module.exports = function (grunt) {

    grunt.initConfig({
        settings: grunt.file.readJSON('settings.json'),
        clean: {
            options: {
                force: true
            },
            fonts: ['<%= settings.dir.assets %>fonts'],
            css: ['<%= settings.dir.assets %>css'],
            scripts: ['<%= settings.dir.assets %>js/*.js'],
            images: ['<%= settings.dir.assets %>img/']
        },
        compass: {
            options: {
                //fontsDir: '<%= settings.dir.dev %>fonts',
                spriteLoadPath: '<%= settings.dir.dev %>img',
                cssDir: '<%= settings.dir.assets %>css',
                sassDir: '<%= settings.dir.dev %>scss',
                imagesDir: '<%= settings.dir.dev %>img',
                relativeAssets: true,
                generatedImagesDir: '../public/assets/backend/img',
                imagesPath: 'img'
            },
            dev: {
                options: {
                    environment: 'development',
                    outputStyle: 'expanded',
                    trace: true,
                    noLineComments: false
                }
            },
            dist: {
                options: {
                    environment: 'production',
                    outputStyle: 'compressed',
                    noLineComments: true
                }
            }
        },
        concat: {
            options: {
                process: function (src, path) {
                    return '\n/** START: "' + path + '" **/\n' + src + '\n/** END: "' + path + '" **/\n';
                }
            },
            appJs: {
                files: {
                    '<%= settings.dir.assets %>js/app.js': [
                        '<%= settings.dir.dev %>js/plugins/*.js',
                    ]
                }
            }
        },
        copy: {
            nonSprites: {
                files: [{
                    expand: true,
                    cwd: '<%= settings.dir.dev %>img/non-sprite',
                    src: ['**'],
                    dest: '<%= settings.dir.assets %>img'
                }]
            },
            fonts: {
                files: [{
                    expand: true,
                    cwd: '<%= settings.dir.dev %>fonts',
                    src: ['**'],
                    dest: '<%= settings.dir.assets %>fonts'
                }]
            }
        },
        uglify: {},
        watch: {
            scss: {
                files: ['<%= settings.dir.dev %>scss/**/*.scss', '<%= settings.dir.dev %>img/**/*.{jpg,png,gif}'],
                tasks: ['compass:dev', 'copy:nonSprites']
            },
            appJs: {
                files: [
                    '<%= settings.dir.dev %>js/plugins/*.js'
                ],
                tasks: ['concat:appJs']
            },
            config: {
                options: {
                    reload: true
                },
                files: ['Gruntfile.js', 'settings.json'],
                tasks: ['clean', 'concat', 'copy', 'compass:dev']
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', ['concat', 'copy', 'compass:dev', 'watch']);
    grunt.registerTask('rebuild', ['clean', 'copy', 'concat', 'compass:dev']);
};