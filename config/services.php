<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => '',
        'secret' => '',
    ],

    'mandrill' => [
        'secret' => '',
    ],

    'ses' => [
        'key' => '',
        'secret' => '',
        'region' => 'us-east-1',
    ],

    'stripe' => [
        'model' => 'App\User',
        'secret' => '',
    ],

  /*  'google' => [
        'client_id' => '961259743805-sbeiqkmhv64jddqkehv6hdb6n2t3q650.apps.googleusercontent.com',
        'client_secret' => 'hsiKbgUJQzW6xCRayYkORsOy',
        'redirect' => '/auth/google/callback',
    ],*/

    'google' => [
        'client_id' => '961259743805-p89a0qn20sp8uge8if2639gg0doa93rt.apps.googleusercontent.com',
        'client_secret' => 'b3x9PmFkDEo-DJf5tfg9XMPb',
        'redirect' => 'http://parsineed.com/auth/google',
    ],

    'github' => [
        'client_id' => '3eb8a931be5e5b312fc6',
        'client_secret' => 'dfc2a075db3a84723e5100a8158e49b21297bee2',
        'redirect' => 'http://parsineed.com/auth/github',
    ],

];
