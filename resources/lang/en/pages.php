<?php

return ['view' =>
    [
        'about' =>
            [
                "Title" => "About",
                "Text1" => '
                    <a name="About us"></a><h1>Studenest</h1>
                    <p><b>Studenest</b>
                        , filiale du groupe Sodevim, initie et gère des projets de résidences locatives destinées
                        principalement aux étudiants, stagiaires et jeunes chercheurs de l’enseignement supérieur.
                        Consciente qu’un environnement confortable, sécurisé et convivial permet aux étudiants de
                        s’impliquer pleinement et sereinement dans leurs études.
                        <br>Studnest entend offrir aux locataires de ses résidences des prestations à la
                        hauteur de leurs attentes. Son opération de Galilée composée de 190 appartements,
                        du Studio au T2, est implantée au cœur du quartier universitaires du secteur de la Cité
                        Descartes à Marne la Vallée.
                        </p>
                ',
                "Text2" => '
                        <a name="Sodevim"></a><h1>Sodevim</h1><p>Depuis plus de 20 ans,
                        <b>groupe Sodevim</b> développe des projets immobiliers et coordonne leur exécution en
                        Ile-de-France et en province. Intervient également comme AMO (Assistance à la
                        Maîtrise d’Ouvrage), et fort de l’expérience de ses dirigeants, réalise des
                        opérations aussi bien en habitat collectif, maisons individuelles, résidence étudiante,
                        résidence pour personnes âgées qu\'en immobilier d\'entreprise. <br>Elle réalise notamment des
                        immeubles en VEFA (Vente en l’État Futur d’Achèvement) pour le compte de bailleurs sociaux et
                        d’investisseurs institutionnels (logements, résidences étudiantes, bureaux).</p>
                        ',


            ],
        'services' =>
            [
                "Title" => "Services",
                "Text1" => '
                    <h1>Services</h1>
                    <p>Quand l’accueil s’habille d’un sourire, que le confort rencontre la convivialité, quand le bien-être est
                        une priorité, c’est le plaisir qui est partagé. Parce que chaque résident est unique, que chaque jour est
                        précieux, nous mettons à la disposition de nos locataires des prestations de qualité, offrant toutes les
                        commodités nécessaires pour rendre leur quotidien extraordinaire.</p>

                ',
                "Text2" => '
                    <h2>PARKING </h2>
                    <p>Des emplacements de parking sous-sol des résidences vous permettent de
                        stationner facilement votre véhicule OU votre vélo.</p>
                        ',
                "Text3" => '

                    <h2>ESPACE DE CONVIVIALITÉ CONNECTÉ</h2>
                    <p>Un salon dédié à la détente et aux échanges, équipé d’une télévision et
                        d’une connexion Internet en Wifi.</p>

                        ',
                "Text4" => '
                            <h2>ACCÈS SÉCURISÉ ET ADAPTÉ À TOUS</h2>
                    <p>Un service d’accueil et un système de badge magnétique assurent la
                        sécurité des résidences dont les accès sont également
                        adaptés aux personnes à mobilité réduite.</p>
                        ',
                "Text5" => '
                    <h2>COMMODITÉS </h2>
                    <p>Pour faciliter le quotidien, une laverie automatique équipée de lave-linge et sèche-linge, ainsi
                        qu\'un service de prêt d\'aspirateur, fer à repasser, linge de lit et kit
                        vaisselle sont mis à la disposition des résidents.</p>
                        ',
                "Text6" => '
                        <h2>ESPACE FITNESS</h2>
                    <p>Souffler, bouger, se dépenser avant, pendant ou après une journée de travail ? </p>
                    <p>Les équipements sportifs de notre espace fitness vous permettent d’entretenir votre forme.</p>

                        ',
                "Text7" => '
                        <h2>ASSURANCE TARIF SPÉCIAL ÉTUDIANT</h2>
                    <p><br>Un partenariat avec la compagnie <b>Dynassurances</b> offre des tarifs préférentiels
                        (-40% sur les tarifs en vigueur) à nos résidents (incendies, dégâts des eaux, vol, vandalisme, responsabilité civile, etc.).<br>
                    <ul class="atgList"><li>1 pièce 	(RDC/ETAGE)	69 € / an TTC
                        </li><li>2 pièces	(RDC/ETAGE)	94 € / an TTC
                        </li><li>3 pièces	(RDC/ETAGE)	105 € / an TTC
                        </li></ul><br><b>Contacte: </b>Christine ANDRE<br><b>e-mail: </b>c.andre@dynassurances.fr<br>
                    <b>Adresse:</b> 15/17 Boulevard Voltaire 94210 La Varenne St Hilaire<br><b>Tel: </b>0148891285<br>
                    <b>Fax:</b>0148838138<br><b>Les heures d\'ouverture : </b><br>Du Lundi au Vendredi : 9h/12h et 14h/18h</p>

                        ',
                "Text8" => '
                    <h2>RESTAURANT LASTRIAL </h2>
                    <p>Petite ou grande faim ? Repas entre amis ? <br>Attenante à la résidence, la cafétéria
                        <b>LASTRIAL</b> a le plaisir de vous accueillir et de vous proposer une gamme
                        de repas soigneusement cuisinés.</p>
                        ',


            ]
    ]
    , 'controller' => [

    ]
    , 'messages' => [
        "err" => "You have problem for saving data! [fr]",
        "errCreate" => "Application problem in create item! [fr]",
    ]
];