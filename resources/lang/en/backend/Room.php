<?php

return ['view' =>
    [
        'index' =>
            [
                "Apartment" => "Apartment",
                "ListRooms" => "List Rooms",
                "ListRoomsRented" => "List Rooms Rented",
                "Type" => "Type",
                "Edit" => "Edit",
                "Delete" => "Delete",
                "Rooms" => "Rooms",
                "Floor" => "Floor",
                "Area" => "Area",
                "RoomCategory" => "Room Category",
                "GenerateRooms" => "Generate Rooms",
            ],
        'listRoomRented' =>
            [
                "UserInfo" => "User Information",
                "Profile" => "User Profile",
                "Booking" => "Bookings",
                "ApartmentInfo" => "Apartment Information",
                "Username" => "User Name",
                "Email" => "Email",
                "Price" => "Price",
                "Duration" => "Duration",
                "CancelRented" => "Click for cancel rent / reserve with booking",
                "Gender" => "Gender",
                "BookingDate" => "Booking Date",
                "Rent" => "Rented",
                "Reserve" => "Reserved",

                "RentStatus" => "Rent Status",
                "Back" => "Back",

                "Title" => "Title",
                "Description" => "Description",
                "Delete" => "Delete",
                "RoomNumber" => "Room Number",
                "RoomPrice" => "Room Price",

                "Apartment" => "Apartment",
                "Category" => "Category",
                "Area" => "Area",
                "Type" => "Type",
                "listRoomRentedUser" => "List Room Rented User",
                "TypeList" => ['room' => 'room', 'parking' => 'parking'],
                "RentStatusList" => ['0' => 'Select', '2' => 'Reserved', '3' => 'Rented'],

            ],
        'create' =>
            [
                "Username" => "User Name",
                "Email" => "Email",
                "Gender" => "Gender",
                "Date" => "Date",
                "Rent" => "Rented",
                "Reserve" => "Reserved",

                "RentStatus" => "Rent Status",
                "Back" => "Back",
                "Wall" => "Wall",
                "Facilities" => "Facilities",
                "Doors" => "Doors",
                "MediaTools" => "Media Tools",
                "Floor" => "Floor",
                "Total" => "Total",
                "Status" => "Room Status",
                "StatusList" => ['good' => 'good', 'dirty' => 'dirty', 'damaged' => 'damaged', 'bad' => 'bad', 'very-bad' => 'very bad'],

                "Title" => "Title",
                "Description" => "Description",
                "LinkPicture" => "Link Picture",
                "Delete" => "Delete",
                "RoomNumber" => "Room Number",
                "RoomPrice" => "Room Price",
                "RoomImage" => "Room Image",
                "Cancel" => "Cancel",
                "SavePictures" => "Save Pictures",
                "Apartment" => "Apartment",
                "Category" => "Category",
                "Area" => "Area",
                "Type" => "Type",
                "UpdateRooms" => "Update Rooms",
                "Upload" => "Upload",
                "UploadFilesTitle" => "Click for upload file picture",
                "titleSaveDesc" => "for save image click on Save Pictures",
                "titleDrag" => "you can drag & drop for upload images, maximum file upload is 15.",


            ]
    ]
    , 'controller' =>
        [
//        "ApartmentEdit" => "Apartment Edit",
        ]
    , 'messages' => [
        "msgInsertPicsCountNotOk" => "count of pictures is not valid, maximum upload for picture is 15.",
        "msgInsertOk" => "Add new rooms.",
        "msgInsertRentNotOk" => "Can not add rend.",
        "msgInsertRentOk" => "Add new rent.",
        "msgInsertReservationNotOk" => "Can not add rend.",
        "msgInsertReservationOk" => "Add new rent.",
        "errMsg" => "You have Error!",
        "errValid" => "This data is not valid, contact to administrator!",
        "msgUpdateOk" => "Update successfully.",
        "msgUpdateNotOk" => "Update unsuccessfully.",
        "msgUpdateReservationNotOk" => "Update reservation unsuccessfully.",
        "msgDelOk" => "Delete successfully.",
        "msgDelNotOk" => "Delete unsuccessfully.",
        "thisRoomNotValid" => "this room is in rent/reservation.",
        "thisRoomInNotReserved" => "this room not reserved.",
        "thisRoomReserved" => "this room reserved.",
        "thisRoomReservedForUser" => "another room reserved for this user, please remove reserved.",
        "thisRoomRented" => "this room rented.",
        "msgCancelRentOk" => "this rent cancel.",

        "msgMailSubjectForReservation" => "A room is booked for you.",
        "msgMailTextForReservation" => "A booked room is now reserved for you, this is certify that this email does not show your rented room.",

        "msgMailSubjectForReservationParking" => "A parking is booked for you.",
        "msgMailTextForReservationParking" => "A booked parking is now reserved for you, this is certify that this email does not show your rented parking.",

        "msgMailSubjectForRent" => "A room is rented for you.",
        "msgMailTextForRent" => "A room is rented for you, if you do not agree please contact with administrator department.",

        "msgMailSubjectForRentParking" => "A parking is rented for you.",
        "msgMailTextForRentParking" => "A parking is rented for you, if you do not agree please contact with administrator department.",

        "msgMailSubjectForCancelRent" => "Your rented room is canceled.",
        "msgMailTextForCancelRent" => "Renting room to you is canceled and deactivate booking because of some proved reasons, if you do not agree please contact with administrator department.",

        "msgMailSubjectForCancelRentParking" => "Your rented parking is canceled.",
        "msgMailTextForCancelRentParking" => "Renting parking to you is canceled and deactivate booking because of some proved reasons, if you do not agree please contact with administrator department.",
    ]
];