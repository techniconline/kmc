<?php

return ['view' =>
    [
        'index' =>
            [
                "Create_New_Room_Category" => "Create New Room Category",
                "List" => "List",
                "Title" => "Title",
                "Short_Description" => "Short Description",
                "Area" => "Area",
                "Rent_Price" => "Rent Price",
                "Type" => "Type",
                "Edit" => "Edit",
                "Details" => "Details",
                "Delete" => "Delete",
            ],
        'create' =>
            [
                "Back" => "Back",
                "Title" => "Title",
                "Short_Description" => "Short Description",
                "Description" => "Description",
                "Upload" => "Upload",
                "Area" => "Area",
                "Type_Category" => "Type Category",
                "File_Upload" => "File Upload",
                "Start_Image_Upload" => "Start Image Upload",
                "Image_List" => "Image List",
                "Image" => "Image",
                "Delete" => "Delete",
                "Link" => "Link",
                "Start_Video_Upload" => "Start Video Upload",
                "Video_List" => "Video List",
                "Cancel" => "Cancel",


            ],
        'details' =>
            [
                "List_Room_Category" => "List Room Category",
                "Apartment_Name" => "Apartment Name",
                "Apartment" => "Apartment",
                "Room_Count" => "Room Count",
                "Floor_Number" => "Floor Number",
                "Update_Category_Details" => "Update Category Details",
                "Delete" => "Delete",
                "Cancel" => "Cancel",
                "Update_Price" => "Update Price",
                "Add_Category_Details" => "Add Category Details",
                "Create_New_Category_Details_For" => "Create New Category Details For",
                "with_Area" => "with Area",

            ]
    ]
    , 'controller' => [
        "Insert_Room_Category" => "Insert Room Category",
        "Create_Room_Category" => "Create Room Category",
        "Edit_Room_Category" => "Edit Room Category",
        "Update_Room_Category" => "Update Room Category",
    ]
    , 'messages' => [
        "errCreate" => "Application problem in create room category!",
        "errNoCatId" => "Category id passed please, contact to administrator!",
        "errPrice" => "You have error for add price, contact to administrator!",
        "msgUpdateOk" => "Update successfully.",
        "msgUpdateNotOk" => "Update unsuccessfully.",
        "msgAddPrice" => "Add price of category.",
        "msgAddPriceWithOld" => "this category price updated. Old price:",
        "msgDelOk" => "Delete successfully.",
        "msgDelNotOk" => "Delete unsuccessfully.",
    ]
];;