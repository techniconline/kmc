<?php

return ['view' =>
    [
        'index' =>
            [
                "List" => "List [en]",
                "Message" => "Message [en]",
                "From" => "From [en]",
                "To" => "To [en]",
                "Date" => "Date [en]",
                "Edit" => 'Edit [en]',
                "Delete" => 'Delete [en]',
                "Answer" => "Answer [en]",
                "Answered" => "Answered [en]"
            ],
        'form' =>
            [
                "Back" => "Back [en]",
                "Answer_To" => "Answer To [en]",
                "Cancel" => "Cancel [en]",
                "Send" => "Send [en]"
            ]
    ],
    'messages' => [
        'fillError' => 'Something Went Wrong!!!! [en]',
        'success' => 'Successfully Your Answer Sent to :email [en]',
        'sentError' => 'Unfortunaly Your Answer does not Sent to :email. Please Try Again [en]'
    ]
];