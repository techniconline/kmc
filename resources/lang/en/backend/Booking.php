<?php

return ['view' =>
    [
        'index' =>
            [
                "Title" => "Title",
                "Edit" => "Edit",
                "Delete" => "Delete",
                "Description" => "Description",
                "IsDisable" => "Is Disable",
                "IsEnable" => "Is Enable",
            ],
        'form' =>
            [
                "Back" => "Back",
                "Title" => "Title",
                "Cancel" => "Cancel",
                "Description" => "Description",

            ],
        'info_users' =>
            [
                "Title" => "Information User",
                "persona" => "persona",
                "education" => "education",
                "institution_type" => "institution type",
                "institution" => "institution",
                "institution_town" => "institution town",
                "current_institution_type" => "current institution type",
                "current_institution" => "current institution",
                "current_institution_town" => "current institution town",
                "parent_guarantor" => "parent guarantor",
                "accuracy_information" => "accuracy information",
                "personal_message" => "personal message",
                "OK" => "OK",
                "NotOK" => "Not Ok",
            ],
        'user_parent' =>
            [
                "Title" => "Information of Parent User",
                "parent_first_name" => "parent first name",
                "parent_last_name" => "parent last name",
                "parent_email" => "parent email",
                "parent_mobile" => "parent mobile",
                "parent_tenant_address" => "parent tenant address",
                "parent_address" => "parent address",
                "parent_postal_code" => "parent postal code",
                "parent_country_name" => "parent country name",
            ],
        'listBooking' =>
            [
                "ListOfUsersBooking" => "List of users booking",
                "ListOfUsersBookingArchive" => "Archive of users booking",
                "SelectRoom" => "Select Room",
                "SelectParking" => "Select Parking",
                "ArchiveBooking" => "Archive Booking",
                "Back" => "Back",
                "Mobile" => "Mobile",
                "Type" => "Type",
                "Room" => "Room",
                "Parking" => "Parking",
                "Description" => "Description",
                "Edit" => "Edit",
                "Delete" => "Delete",
                "Gender" => "Gender",
                "Country" => "Country",
                "Reject" => "Reject",
                "ListAttachment" => "Manage List Attachment",
                "ListAttachmentsUser" => "List Attachment User",
                "CancelRent" => "Cancel Rent",
                "IsOk" => "Is Ok",
                "Search" => "Search",
                "Select" => "Select",
                "SendMail" => "Send Mail",
                "View" => "View",
                "From" => "From",
                "To" => "To",
                "Name" => "Name",
                "Rent" => "Rent",
                "Reservation" => "Reservation",
                "reserved" => "reserved",
                "FirstName" => "First Name",
                "LastName" => "Last Name",
                "Birthday" => "Birth Day",
                "BookingDate" => "Booking Date",
                "RegisterBookingDate" => "Register Booking Date",
                "thisBookingCanceledRent" => "This Booking Canceled Rent",
                "sendContract" => "Send Contract",

                "GeneratePDF" => "Generate PDF of Contract",
                "Cancel" => "Cancel",
                "sendContractMail" => "Send Contract With Mail",
                "ThePriceOfLetters" => "The price of letters",

                "sendContractOk" => "The contract was sent",
                "sendContractNotOk" => "The contract was not sent",
            ]
    ]
    , 'controller' =>
        [

        ]
    , 'messages' => [
        "errCreate" => "Application have problem in create!",
        "errValid" => "This data is not valid, contact to administrator!",
        "msgUpdateOk" => "Update successfully.",
        "msgUpdateNotOk" => "Update unsuccessfully.",
        "msgDelOk" => "Delete successfully.",
        "msgDelNotOk" => "Delete unsuccessfully.",
        "YouBookingIsUnavailable" => "Your Booking is not available.",
        "SendMailOk" => "Send mail successfully.",
        "SendMailNotOk" => "Send mail Unsuccessfully.",
        "changeBookingStatusOk" => "User booking changed status",
        "changeBookingStatusNotOk" => "User booking not changed status",

        "rejectBookingDetailsForRoom" => "Room type reservation for you rejected.",
        "rejectBookingDetailsTextForRoom" => "Your room type reservation in booking on site full rejected, please contact your administrator.",
        "rejectBookingDetailsForParking" => "Parking type reservation for you rejected.",
        "rejectBookingDetailsTextForParking" => "Your parking type reservation in booking on site full rejected, please contact your administrator.",

        "sendContractSubject" => "This message is for the contract between us",
        "sendContractText" => "Complete and read all attachments and then go to department for contract.",

        "FillItems" => "For generate PDF you need to fill items.",
        "GenerateOk" => "file generate successfully.",
        "GenerateNotOk" => "file generate unsuccessfully.",
        "NotFindRentedData" => "Not find parking and room rented for this booking!",
    ]
];