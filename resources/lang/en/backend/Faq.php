<?php

return ['view' =>
    [
        'index' =>
            [
                "Create_New_FAQ" => "Create New FAQ [en]",
                "List" => "List [en]",
                "Question" => "Question [en]",
                "No_Question" => "No_Question [en]",
                "Answer" => "Answer [en]",
                "No_Answer" => "No_Answer [en]",
                "Edit" => 'Edit [en]',
                "Type" => "Type [en]",
                "Delete" => 'Delete [en]'
            ],
        'form' =>
            [
                "Back" => "Back [en]",
                "Edit_FAQ" => "Edit FAQ [en]",
                "Create_FAQ" => "Create FAQ [en]",
                "Question" => "Question [en]",
                "Answer" => "Answer [en]",
                "Update" => "Update [en]",
                "Create" => "Create [en]",
                "Cancel" => "Cancel [en]",
                "Type" => "Type [en]",
            ]
    ]
    , 'controller' =>
        [
        ]
    , 'messages' => [
        "errCreate" => "Application problem in create new Content! [en]",
    ]
];