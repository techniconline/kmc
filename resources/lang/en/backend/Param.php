<?php

return [
    'view' => [
        'index' =>
            [
                "Create_New_Parameter" => "Create New Parameter [en]",
                "List" => "List [en]",
                "Type" => "Type [en]",
                "Name" => "Name [en]",
                "Alias" => "Alias [en]",
                "Value" => "Value [en]",
                "To_Value" => "To Value [en]",
                "Edit" => "Edite [en]",
                "Delete" => "Delete [en]",
            ],
        'form' =>
            [
                'Back' => 'Back [en]',
                'Create_New_Param' => 'Create New Param [en]',
                'Type' => 'Type [en]',
                'Alias' => 'Alias [en]',
                'Name' => 'Name [en]',
                'Image' => 'Image [en]',
                'Value' => 'Value [en]',
                'To_Value' => 'To_Value [en]',
                'Update' => 'Update [en]',
                'Create' => 'Create [en]',
                'Cancel' => 'Cancel [en]',
            ]
    ],
    'messages' => [
        'fillError' => 'Somethings Went Wrong!!! [en]',
        'success' => 'Successfully Create New Parameter [en]',
        'successUpd' => 'Successfully Updated This Parameter [en]',
        'successDel' => 'Successfully Deleted [en]'
    ]
];