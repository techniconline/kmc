<?php

return [
    'view' => [
        'index' =>
            [
                "Calendar" => "Calendar [en]",
                "Add_Reminder" => "Add Reminder [en]",
                "Remove_Reminder" => "Remove Reminder [fr]",
            ],
        'form' =>
            [
                "Title" => "Title [en]",
                "Color" => "Color [en]",
                "Start" => "Start [en]",
                "End" => "End [en]",
                "All_Day" => "All Day [en]",
                "Cancel" => "Cancel [en]",
                "Date" => "Date [en]",
                "Save" => "Save [en]",
                "Delete" => "Delete [fr]",
                "Time" => "Time [en]",
            ]
    ],
    'messages' => [
        'fillError' => 'Somethings Went Wrong!!! [en]',
        'success' => 'Successfully New Reminder Added [en]',
        'successUpd' => 'Successfully Updated This Reminder [en]',
        'successDel' => 'Successfully Deleted [en]'
    ]
];