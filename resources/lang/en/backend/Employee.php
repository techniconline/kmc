<?php

return ['view' =>
    [
        'index' =>
            [
                "Create_New_Employee" => "Create New Employee [en]",
                "List" => "List [en]",
                "Pic" => "Pic [en]",
                "Name" => "Name [en]",
                "Family" => "Family [en]",
                "Email" => "Email [en]",
                "Mobile" => "Mobile [en]",
                "Status" => 'Status [en]',
                "Active" => 'Active [en]',
                "Inactive" => 'Inactive [en]',
                "Edit" => 'Edit [en]',
                "Delete" => 'Delete [en]'
            ],
        'form' =>
            [
                "Back" => "Back [en]",
                "Edit_User" => "Edit User [en]",
                "Avatar" => "Avatar [en]",
                "Name" => "Name [en]",
                "Status" => "Status [en]",
                "Family" => "Family [en]",
                "Email" => "Email [en]",
                "Password" => "Password [en]",
                "Reset_Password" => "Reset Password [en]",
                "Are_You_Sure?" => "Are_You_Sure? [en]",
                "Gender" => "Gender [en]",
                "Mobile" => "Mobile [en]",
                "Fax" => "Fax [en]",
                "Phone" => "Phone [en]",
                "Address" => "Address [en]",
                "Company" => "Company [en]",
                "Paypal" => "Paypal [en]",
                "Postal_Code" => "Postal Code [en]",
                "BirthDate" => "BirthDate [en]",
                "Country" => "Country [en]",
                "Zone" => "Zone [en]",
                "Update" => "Update [en]",
                "Create" => "Create [en]",
                "Cancel" => "Cancel [en]",
            ]
    ]
    , 'controller' =>
        [
        ]
    , 'messages' => [
        "errCreate" => "Application problem in create new Content! [en]",
    ]
];