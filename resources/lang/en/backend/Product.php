<?php

return ['view' =>
    [
        'index' =>
            [
                "CreateNewProduct" => "Create New Product",
                "List" => "List",
                "Title" => "Title",
                "Edit" => "Edit",
                "Delete" => "Delete",
                "Rooms" => "Rooms",
                "Floor" => "Floor",
                "Parking" => "Parking",
                "Address" => "Address",
                "ProductName" => "Product Name",
            ],
        'create' =>
            [
                "Back" => "Back",
                "Title" => "Title",
                "ShortDescription" => "Short Description",
                "Description" => "Description",
                "Upload" => "Upload",
                "FileUpload" => "File Upload",
                "StartImageUpload" => "Start Image Upload",
                "ImageList" => "Image List",
                "Image" => "Image",
                "Delete" => "Delete",
                "Link" => "Link",
                "Map" => "Map",
                "Cancel" => "Cancel",
                "Address" => "Address",
                "StartVideoUpload" => "Start Video Upload",
                "VideoList" => "Video List",
                "FloorCount" => "Floor Count",
                "RoomCount" => "Room Count",
                "ParkingCount" => "Parking Count",
                "IsDefault" => "Is Default",

            ]
    ]
    , 'controller' =>
        [
            "ProductEdit" => "Product Edit",
            "UpdateProduct" => "Update Product",
            "InsertProduct" => "Insert Product",
            "CreateProduct" => "Create Product",
        ]
    , 'messages' => [
        "errCreate" => "Application problem in create product!",
        "errValid" => "This product is not valid, contact to administrator!",
        "msgUpdateOk" => "Update successfully.",
        "msgUpdateNotOk" => "Update unsuccessfully.",
        "msgDelOk" => "Delete successfully.",
        "msgDelNotOk" => "Delete unsuccessfully.",
    ]
];