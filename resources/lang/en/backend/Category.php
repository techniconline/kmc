<?php

return ['view' =>
    [
        'index' =>
            [
                "Categories_List" => "Categories List [en]",
            ],
        'form' =>
            [
                "Back" => "Back",
                "Edit" => "Edit",
                "Name" => "Name",
                "Alias" => "Alias",
                "URL" => "URL",
                "Description" => "Description",
                "Image" => "Image",
                "Thumbnail" => "Thumbnail",
                "Update" => "Update",
                "Cancel" => "Cancel"
            ],
        'subMenus' =>
            [
                "Back" => "Back",
            ]
    ]
    , 'controller' => [
        "Insert_Room_Category" => "Insert Room Category",
        "Create_Room_Category" => "Create Room Category",
        "Edit_Room_Category" => "Edit Room Category",
        "Update_Room_Category" => "Update Room Category",
    ]
    , 'messages' => [
        "errCreate" => "Application problem in create room category!",
        "errNoCatId" => "Category id passed please, contact to administrator!",
        "errPrice" => "You have error for add price, contact to administrator!",
        "msgUpdateOk" => "Update successfully.",
        "msgUpdateNotOk" => "Update unsuccessfully.",
        "msgAddPrice" => "Add price of category.",
        "msgAddPriceWithOld" => "this category price updated. Old price:",
        "msgDelOk" => "Delete successfully.",
        "msgDelNotOk" => "Delete unsuccessfully.",
    ]
];;