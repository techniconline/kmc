<?php

return [
    'view' => [
        'index' => [
            "Contact_Us" => "Contact Us [en]"
        ],
        'info' => [

        ]
    ],
    'controller' => [
        'captcha_error' => 'You Should Fill CAPTCHA [en]'
    ],
    'model' => [
        'Management' => 'Management [en]',
        'Rent' => 'Rent [en]'
    ]
];