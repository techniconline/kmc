<?php

return ['view' =>
    [
        'index' =>
            [


                "TitleLogo" => "Résidence pour étudiants",
                "TitlePageTerms" => "Mentions légales",
                "Reservation" => "Réservation",
                "SelectLang" => "EN",
                "CopyRight" => "&copy; کلیه حقوق این سایت برای نمایندگی 1732 کرمان موتور محفوض است",

                "Profile" => "پروفایل",
                "Change_Password" => "تغییر کلمه عبور",
                "YourMessage" => "پیامهای شما",
                "Logout" => "خروج",
                "Login" => "ورود به سایت",
                "Register" => "عضویت",
                "New" => "جدید",
                "HomePage" => "صفحه اصلی",
                "Sales" => 'از <span>طرح های فروش</span> کرمان موتور دیدن نمائید<',
            ],
        'register' =>
            [

                "Title" => "عضویت",
                "Text" => "برای عضویت کلیه اطلاعات زیر وارد کنید",
                "Fname" => "نام",
                "Lname" => "نام خانوادگی",
                "Email" => "پست الکترونیک",
                "Password" => "کلمه عبور",
                "ConfirmPass" => "تکرار کلمه عبور",
                "Register" => "عضویت",
                "Gender" => "جنسیت",
                "DataGender" => ['male' => 'مرد', 'female' => 'زن'],
            ],
        'login' =>
            [
                "Bonjour" => "خوش آمدید",
                "TextWelcome" => "<p>برای ورود به سایت به روشهای زیر عمل کنید</p>",
                "TitleLogin" => 'ورود کاربر',
                "MSG_USER_EDIT" => '<div>ورود کاربر برای مشاهده پروفایل خود لازم است</div>',

                "Email" => "پست الکترونیک",
                "Password" => "کلمه عبور",
                "Login" => "ورود",
                "TitleForget" => "فراموشی کلمه عبور",
                "TextForget" => "برای بازیابی کلمه عبور از این لینک استفاده کنید",
                "SendPass" => "ارسال کلمه عبور",
                "Remember" => "من را بخاطر بیاور",
                "ProblemLogin" => "در ورود به دچار مشکل شده اید؟",
                "DescAuthStart" => "اگر حساب کاربری ندارید همین حالا, ",
                "DescAuthMiddle" => "عضو شوید",
                "DescAuthEnd" => "و شروع به استفاده نمائید ",
                "DescRegStart" => "رمز عبور خود را فراموش کرده اید? شما می توانید  ",
                "DescRegEnd" => "دهید.. ",
            ]
    ]
    , 'controller' => [

    ]
    , 'messages' => [
        "errTitleLogin" => "خطای ورود!",
        "errLogin" => "نام کاربری یا رمز عبور خود را اشتباه وارد کرده اید...",
        "err" => "You have problem for saving data! [fr]",
        "errCreate" => "Application problem in create item! [fr]",
        "errDate" => "Date is not valid! [fr]",
        "msgCreate" => "Save successful! [fr]",
        "errNoFindData" => "Not find dat [fr]a, contact to administrator! [fr]",
        "msgUpdateOk" => "Update successfully. [fr]",
        "msgUpdateNotChanged" => "No information has been changed! [fr]",
        "msgUpdateNotOk" => "Update unsuccessfully. [fr]",
        "msgDelOk" => "Delete successfully. [fr]",
        "msgDelNotOk" => "Delete unsuccessfully. [fr]",
    ],
    'Footer' => [
        "Info" => [
            'Title' => 'تماس <span class="wlast"> با ما </span>',
            'Tel' => '<span>تلفن:</span> 3 - 88618650 21+ ',
            'Fax' => '<span>فکس:</span> 88618650 21+ ',
            'Email' => '<span>پست الکترونیک:</span> <a href="#">info@1732.ir</a>',
            'Web' => '<span>وب سایت:</span> <a href="#">www.1732.ir</a>',
            'Address' => 'تهران، ونک، ملاصدرا، شیراز شمالی، دانشور شرقی، پلاک 26، طبقه هفتم، واحد E7 '
        ],
        "LastNews" => [
            'Title' => 'آخرین <span class="wlast">خبرها</span>'
        ],
        "NewsLetter" => [
            'Title' => 'عضویت در<span class="wlast">خبرنامه</span>',
            'Content' => 'برای عضویت، آدرس ایمیل خود را وارد کرده و روی دکمه "ثبت" کلیک کنید. بدین ترتیب ایمیل فعال سازی ای برای شما ارسال خواهد شد که با تایید آن اشتراک شما در سایت فعال می شود.',
            'email' => 'پست الکترونیک شما',
            'submit' => 'ثبت'

        ],

    ]
];