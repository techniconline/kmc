<?php

return ['view' =>
    [
        'index' =>
            [
                "Step" => "Étape",
                "compte" => "Créez votre compte",
                "appartement" => "Sélectionnez votre appartement",
                "dossier" => "Complétez votre dossier",
                "candidature" => "Etude de votre candidature",
                "contrat" => "Recevez votre contrat",
            ]
    ]
    , 'controller' => [

    ]
    , 'messages' => [
        "err" => "You have problem for saving data! [fr]",
        "errCreate" => "Application problem in create item! [fr]",
    ]
];