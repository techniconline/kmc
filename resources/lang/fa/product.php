<?php

return ['view' =>
    [
        'index' =>
            [
                "Status" => "وضعیت",
                "Garranty" => "گارانتی",
                "Manufacturer" => "محصول",
                "Specifications" => "مشخصات فنی",
                "Share" => "به اشتراک بگذارید",
                "GalleryAll" => "بدون فیلتر",
                "ShowDetails" => "جزئیات محصول",
            ],
        'info' =>
            [

                "titleComment" => "شما هم می توانید در مورد این محصول نظر دهید",
                "Services" => "خدمات پس از فروش و سرویسها",
                "Ability" => "قابلیت های خودرو",
                "Gallery" => "آلبوم تصاویر",
                "Comments" => "نظرات",
                "all" => "همه",
                "out-side" => "نمای خارجی",
                "in-side" => "نمای داخلی",
                "ShortDescription" => "توضیحات کوتاه",
                "Description" => "توضیحات",
                "Group" => "نام گروه",
                "titleSaveDesc" => "فایل برای متن شما آماده ذخیره سازی است",
                "UploadFileComment" => "بارگذاری فایل",
                "Color" => "رنگ:",

            ]
    ]
    , 'controller' => [
        "Insert_Room_Category" => "Insert Room Category",
    ]
    , 'messages' => [
        "err" => "You have problem for saving data!",
        "errCreate" => "Application problem in create item!",
        "msgCreate" => "Save successful!",
        "errNoFindData" => "Not find data, contact to administrator!",
        "msgUpdateOk" => "Update successfully.",
        "msgUpdateNotChanged" => "No information has been changed!",
        "msgUpdateNotOk" => "Update unsuccessfully.",
        "msgDelOk" => "Delete successfully.",
        "msgDelNotOk" => "Delete unsuccessfully.",
    ]
];