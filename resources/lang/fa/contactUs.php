<?php

return [
    'view' => [
        'index' => [
            'Contact_Us' => 'تماس با ما',
            'Contact_Us_Title' => 'ارسال پیام',
        ],
        'info' => [
            'Info' => 'اطلاعات تماس',
            'Address' => 'تهران، ونک، ملاصدرا، شیراز شمالی، دانشور شرقی، پلاک 26، طبقه هفتم، واحد E7 ',
            'Tel' => ' 3 - 88618650 21+ ',
            'Site' => ' 1732.ir ',
            'Email' => ' info@1732.ir ',
            'Text' => 'تاکنون سعی بر این داشته در راستای رضایتمندی مشتری و تکریم ارباب رجوع قدم برداشته و یکی از اهداف این نمایندگی که همانا جلب رضایت و اعتماد کامل مشتریان است را سرلوحه کار خویش قرارداده ایم.
 <br>
باشد که بتوانیم برای همیشه این راه را ادامه دهیم.',

        ]
    ],
    'controller' => [
        'captcha_error' => 'Vous devez remplir CAPTCHA'
    ],
    'model' => [
        'itemContactUs' => ['Information' => 'Information fr', 'Reservation' => 'Reservation fr', 'Visit' => 'Visit fr', 'Billing' => 'Billing fr'],
        'submit' => 'ارسال',
        'Email' => 'پست الکترونیک*',
        'Tel' => 'تلفن تماس',
        'Message' => 'متن',
        'Name' => 'نام کاربری*',
    ]
];