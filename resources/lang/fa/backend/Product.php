<?php

return ['view' =>
    [
        'index' =>
            [
                "CreateNewProduct" => "ایجاد محصول",
                "List" => "فهرست",
                "Title" => "عنوان",
                "Edit" => "ویرایش",
                "Delete" => "حذف",
                "Rooms" => "اتاق",
                "Floor" => "طبقه",
                "Parking" => "پارکینک",
                "Address" => "آدرس",
                "ProductName" => "نام محصول",
            ],
        'create' =>
            [
                "Back" => "برگشت",
                "Title" => "عنوان",
                "Alias" => "مسیر",
                "Features" => "ویژگی ها",
                "FeatureGroup" => "گروه بندی ویژگی ها",
                "ShortDescription" => "توضیح کوتاه",
                "Description" => "توضیح",
                "Sort" => "ترتیب",
                "ColorFeature" => "رنگ انتخابی",
                "ClassGallery" => "دسته بندی",
                "Upload" => "بارگذاری",
                "FileUpload" => "بارگذاری فایل",
                "StartImageUpload" => "شروع بارگذاری تصویر",
                "ImageList" => "فهرست تصاویر",
                "Image" => "تصویر",
                "Delete" => "حذف",
                "Link" => "پیوند",
                "Map" => "نقشه",
                "Cancel" => "انصراف",
                "Condition" => "وضعیت",
                "new" => "جدید",
                "gift" => "هدیه",
                "StartVideoUpload" => "شروع بارگذاری ویدئو",
                "VideoList" => "فهرست ویدئوها",
                "Is_Active" => "وضعیت نمایش",
                "Active_date" => "تاریخ فعال شدن",
                "Category" => "شاخه",
                "Quantity" => "موجودی",
                "Price" => "قیمت",
                "IsDefault" => "پیش فرض",

            ]
    ]
    , 'controller' =>
        [
            "ProductEdit" => "ویرایش محصول",
            "UpdateProduct" => "بروزرسانی محصول",
            "InsertProduct" => "ذخیره محصول",
            "CreateProduct" => "ایجاد محصول",
        ]
    , 'messages' => [
        "errCreate" => "برای ایجاد محصول سیستم دچار مشکل شده!",
        "errValid" => "این محصول معتبر نیست با مدیر سایت تماس بگیرید!",
        "msgUpdateOk" => "بروز رسانی با موفقیت انجام شد.",
        "msgUpdateNotOk" => "روز رسانی با موفقیت انجام نشد!",
        "msgDelOk" => "حدف با موفقیت انجام شد.",
        "msgDelNotOk" => "حدف با موفقیت انجام نشد!",
    ]
];