<?php

return ['view' =>
    [
        'index' =>
            [

                "CreateGroupOfAttachmentType" => "Create Group Of Attachment Type",
                "CreateAttachmentType" => "Create Attachment Type",
                "ListTypeAttachment" => "List Type Attachment",
                "ListTypeAttachmentGroup" => "List Group Of Type Attachment",
                "Title" => "Title",
                "Alias" => "Alias",
                "Edit" => "Edit",
                "Delete" => "Delete",
                "Description" => "Description",
                "Group" => "Group",
                "IsDisable" => "Is Disable",
                "IsEnable" => "Is Enable",
                "ListUploadedFromUsers" => "List Uploaded From Users",

            ],
        'form' =>
            [
                "Back" => "Back",
                "Title" => "Title",
                "Cancel" => "Cancel",
                "Alias" => "Alias",
                "Description" => "Description",
                "AttachTypeGroup" => "Attach Type Group",

            ],
        'formGroup' =>
            [
                "Back" => "Back",
                "Title" => "Title",
                "Cancel" => "Cancel",
                "Alias" => "Alias",
                "Condition" => "Condition",
                "Description" => "Description",


            ],
        'listUploaded' =>
            [
                "Back" => "Back",
                "Edit" => "Edit",
                "Delete" => "Delete",
                "Gender" => "Gender",
                "Country" => "Country",
                "Reject" => "Reject",
                "IsOk" => "Is Ok",
                "Search" => "Search",
                "Select" => "Select",
                "SendMail" => "Not complete documents",
                "Document" => "Document",
                "View" => "View",
                "GroupAttach" => "Group Attach",
                "From" => "From",
                "To" => "To",
                "Name" => "Name",
                "FirstName" => "First Name",
                "LastName" => "Last Name",
                "Birthday" => "Birth Day",
                "BookingDate" => "Booking Date",
                "RegisterBookingDate" => "Register Booking Date",
                "ListOfUsersAttachment" => "List Of Users Attachment",


            ]
    ]
    , 'controller' =>
        [
            "AttachTypeEdit" => "Attach Type Edit",
            "UpdateAttachType" => "Update Attach Type",
            "InsertAttachType" => "Insert Attach Type",
            "CreateAttachType" => "Create Attach Type",
            "InsertGroup" => "Insert Group",
            "CreateGroup" => "Create Group",
        ]
    , 'messages' => [
        "NotValidUser" => "Not valid user for attachment!",
        "errCreate" => "Application have problem in create!",
        "errValid" => "This data is not valid, contact to administrator!",
        "msgUpdateOk" => "Update successfully.",
        "msgUpdateNotOk" => "Update unsuccessfully.",
        "msgDelOk" => "Delete successfully.",
        "msgDelNotOk" => "Delete unsuccessfully.",
        "msgStatusDocumentIsOk" => "All documents is ok.",
        "msgStatusDocumentRejected" => "All documents rejected.",
        "msgStatusDocumentIsNotComplete" => "Documents is not completed!",
        "msgStatusDocumentNotCheck" => "Documents is not check!",
        "errDuplicateAlias" => "Duplicate this alias, please edit alias.",
        "YouDocIsUnavailable" => "Your documents is not available.",
        "SendMailOk" => "Send mail successfully.",
        "SendMailNotOk" => "Send mail Unsuccessfully.",
        "YouDocRejected" => "All Your Documents reject.",
        "TextYouDocRejected" => "All Documents are not valid , please resending new documents.",
        "YouDocIsOk" => "All Your Documents are available.",
        "TextYouDocIsOk" => "All Documents are valid , please contact to administrator department.",

    ]
];