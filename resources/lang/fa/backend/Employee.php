<?php

return ['view' =>
    [
        'index' =>
            [
                "Create_New_Employee" => "Create New Employee [fr]",
                "List" => "List [fr]",
                "Pic" => "Pic [fr]",
                "Name" => "Name [fr]",
                "Family" => "Family [fr]",
                "Email" => "Email [fr]",
                "Mobile" => "Mobile [fr]",
                "Status" => 'Status [fr]',
                "Active" => 'Active [fr]',
                "Inactive" => 'Inactive [fr]',
                "Edit" => 'Edit [fr]',
                "Delete" => 'Delete [fr]'
            ],
        'form' =>
            [
                "Back" => "Back [fr]",
                "Edit_User" => "Edit User [fr]",
                "Avatar" => "Avatar [fr]",
                "Name" => "Name [fr]",
                "Status" => "Status [fr]",
                "Family" => "Family [fr]",
                "Email" => "Email [fr]",
                "Password" => "Password [fr]",
                "Reset_Password" => "Reset Password [fr]",
                "Are_You_Sure?" => "Are_You_Sure? [fr]",
                "Gender" => "Gender [fr]",
                "Mobile" => "Mobile [fr]",
                "Fax" => "Fax [fr]",
                "Phone" => "Phone [fr]",
                "Address" => "Address [fr]",
                "Company" => "Company [fr]",
                "Paypal" => "Paypal [fr]",
                "Postal_Code" => "Postal Code [fr]",
                "BirthDate" => "BirthDate [fr]",
                "Country" => "Country [fr]",
                "Zone" => "Zone [fr]",
                "Update" => "Update [fr]",
                "Create" => "Create [fr]",
                "Cancel" => "Cancel [fr]",
            ]
    ]
    , 'controller' =>
        [
        ]
    , 'messages' => [
        "errCreate" => "Application problem in create new Content! [fr]",
    ]
];