<?php

return ['view' =>
    [
        'index' =>
            [
                "Add" => "ایجاد",
                "ListAccessGroup" => "فهرست گروه ها",
            ],
        'listAccessGroup' =>
            [

                "GroupName" => "نام گروه",
                "Edit" => "ویرایش",
                "AddUsers" => "افزودن کاربر",
                "AddAccess" => "افزودن دسترسی",
                "Delete" => "حذف",
                "TypeAccess" => "بخش دسترسی",
                "backend" => "مدیریت",
                "frontend" => "کاربری",
                "RegDate" => "Register Date",
                "Search" => "جستجو",
                "LastName" => "نام خانوادگی",

            ],
        'form' =>
            [
                "Back" => "برگشت به عقب",
                "Name" => "نام",
                "UserList" => "فهرست کاربران",
                "actionList" => "فهرست کنترلرها و توابع",
                "TypeAccess" => "بخش دسترسی",
                "backend" => "مدیریت",
                "frontend" => "کاربری",
                "Cancel" => "انصراف",
                "Save" => "دخیره",
                "TitleSave" => "ایجاد گروه جدید",
                "Update" => "بروزرسانی",
                "TitleUpdate" => "بروزرسانی گروه",

            ]
    ]
    , 'controller' =>
        [

        ]
    , 'messages' => [
        "added" => "داده شما دخیره گردید.",
        "errCreateDuplicate" => "This data is duplicate for this rent!",
        "errCreate" => "خطا در سیستم!",
        "errValid" => "داده های شما ناقص بوده ، لطفا تکمیل نمایید!",
        "msgUpdateOk" => "بروزرسانی با موفقیت انجام شد.",
        "msgUpdateNotOk" => "بروزرسانی با موفقیت انجام نشد.",
        "msgDelOk" => "حذف با موفقیت انجام شد.",
        "msgDelNotOk" => "حذف با موفقیت انجام نشد.",

        "SendMailOk" => "Send mail successfully.",
        "SendMailNotOk" => "Send mail Unsuccessfully.",

        "NotValidData" => "داده شما معتبر نیست!",
        "NotValidDataForGenerate" => "This data is not valid for generate list rent!",
        "SuccessGenerate" => "Generate list rent successfully.",

        "Remember" => "Remember",
        "SubjectDueInvoice" => "Declare rental invoice due date",
        "TextDueInvoice" => "Please, refer to financial department to pay rental fee.",

        "SubjectInvoice" => "Invoice for room rent",
        "ThankYou" => "thank you for paid",
        "TextInvoice" => "Your invoice for date",
        "Is" => "is",
        "Euro" => "euro",
    ]
];