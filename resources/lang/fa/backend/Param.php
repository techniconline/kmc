<?php

return [
    'view' => [
        'index' =>
            [
                "Create_New_Parameter" => "Create New Parameter [fr]",
                "List" => "List [fr]",
                "Type" => "Type [fr]",
                "Name" => "Name [fr]",
                "Alias" => "Alias [fr]",
                "Value" => "Value [fr]",
                "To_Value" => "To Value [fr]",
                "Edit" => "Edite [fr]",
                "Delete" => "Delete [fr]",
            ],
        'form' =>
            [
                'Back' => 'Back [fr]',
                'Create_New_Param' => 'Create New Param [fr]',
                'Type' => 'Type [fr]',
                'Alias' => 'Alias [fr]',
                'Name' => 'Name [fr]',
                'Image' => 'Image [fr]',
                'Value' => 'Value [fr]',
                'To_Value' => 'To_Value [fr]',
                'Update' => 'Update [fr]',
                'Create' => 'Create [fr]',
                'Cancel' => 'Cancel [fr]',
            ]
    ],
    'messages' => [
        'fillError' => 'Somethings Went Wrong!!! [fr]',
        'success' => 'Successfully Create New Parameter [fr]',
        'successUpd' => 'Successfully Updated This Parameter [fr]',
        'successDel' => 'Successfully Deleted [fr]'
    ]
];