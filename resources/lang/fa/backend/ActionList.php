<?php

return ['view' =>
    [
        'index' =>
            [
                "Add" => "ایجاد",
                "ListAction" => "فهرست کنترلرها",
            ],
        'listAction' =>
            [

                "Prefix" => "نام محدوده",
                "Controller" => "نام کنترلر",
                "Function" => "نام تابع",
                "Method" => "نوع متد",
                "Edit" => "ویرایش",
                "Delete" => "حذف",
                "RegDate" => "زمان ایجاد",
                "Search" => "جستجو",
                "LastName" => "نام خانوادگی",

            ],
        'form' =>
            [
                "Back" => "برگشت به عقب",
                "Cancel" => "انصراف",
                "Save" => "دخیره",
                "TitleSave" => "ایجاد",
                "Update" => "بروزرسانی",
                "TitleUpdate" => "بروزرسانی",

            ]
    ]
    , 'controller' =>
        [

        ]
    , 'messages' => [
        "added" => "داده شما دخیره گردید.",
        "errCreateDuplicate" => "This data is duplicate for this rent!",
        "errCreate" => "خطا در سیستم!",
        "errValid" => "داده های شما ناقص بوده ، لطفا تکمیل نمایید!",
        "msgUpdateOk" => "بروزرسانی با موفقیت انجام شد.",
        "msgUpdateNotOk" => "بروزرسانی با موفقیت انجام نشد.",
        "msgDelOk" => "حذف با موفقیت انجام شد.",
        "msgDelNotOk" => "حذف با موفقیت انجام نشد.",

        "SendMailOk" => "Send mail successfully.",
        "SendMailNotOk" => "Send mail Unsuccessfully.",

        "NotValidData" => "This data is not valid!",
        "NotValidDataForGenerate" => "This data is not valid for generate list rent!",
        "SuccessGenerate" => "Generate list rent successfully.",

        "Remember" => "Remember",
        "SubjectDueInvoice" => "Declare rental invoice due date",
        "TextDueInvoice" => "Please, refer to financial department to pay rental fee.",

        "SubjectInvoice" => "Invoice for room rent",
        "ThankYou" => "thank you for paid",
        "TextInvoice" => "Your invoice for date",
        "Is" => "is",
        "Euro" => "euro",
    ]
];