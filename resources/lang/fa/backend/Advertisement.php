<?php

return ['view' =>
    [
        'index' =>
            [
                "Create_New" => "ایجاد",
                "List" => "فهرست",
                "URL" => "مسیر",
                "Link_Title" => "عنوان پیوند",
                "Title" => "عنوان ",
                "Content" => "مطلب",
                "ShortContent" => "خلاصه مطلب",
                "ImageContent" => "تصویر مطلب",
                "Access" => "دسترسی",
                "Public" => "عمومی",
                "Members" => "اعضاء",
                "Edit" => 'ویرایش',
                "Delete" => 'حدف'
            ],
        'form' =>
            [
                "Back" => "برگشت",
                "URL" => "مسیر",
                "Title" => "عنوان",
                "Create_New" => "ایجاد",
                "Update" => "بروزرسانی",
                "Create" => "ایجاد",
                "Body_Contents" => "مطلب بدنه",
                "short_description" => "خلاصه مطلب",
                "description" => "توضیحات",
                "slider_option" => "تنطیمات اولیه اسلایدر",
                "File" => "فایل",
                "Access" => "دسترسی",
                "Public" => "عمومی",
                "Members" => "اعضاء",
                "Cancel" => "انصراف",
                "Type" => "نوع",
                "Image" => "تصویر",
                "position" => "محل قرارگیری",
                "right_advert_home" => "سمت راست صفحه اصلی",
                "center_advert_home_footer" => "وسط پایین صفحه اصلی",
                "home_slider" => "اسلایدر صفحه اصلی",
            ]
    ]
    , 'controller' =>
        [
            "Edit" => "ویرایش",
            "Update" => "بروزرسانی",
            "Insert" => "دخیره",
            "Create" => "ایجاد",
        ]
    , 'messages' => [
        "errCreate" => "مشکل در ایجاد",
        "successDelete" => "حدف با موفقیت انجام شد",
        "successInsert" => "اطلاعات با موفقیت دخیره گردید",
        "successUpdate" => "اطلاعات با موفقیت بروزرسانی گردید",
        "successNotUpdate" => "اطلاعات با موفقیت بروزرسانی نشد!",
        "uploadFileSuccess" => "تصویر با موفقیت باگذاری شد",
        "uploadFileNotSuccess" => "تصویر با موفقیت باگذاری نشد",
    ]
];