<?php

return ['view' =>
    [
        'index' =>
            [
                "List" => "فهرست",
                "Author" => "نویسنده",
                "Edit" => 'ویرایش',
                "Delete" => 'حدف'
            ],
        'form' =>
            [
                "Back" => "برگشت",

                "Title" => "عنوان",

                "Update" => "بروزرسانی",
                "Type" => "نوع مطلب",
                "Status" => "وضعیت",
                "Enable" => "فعال",
                "Disable" => "غیرفعال",
            ]
    ]
    , 'controller' =>
        [

        ]
    , 'messages' => [
        "errCreate" => "مشکل در ایجاد مطلب جدید",
        "successDelete" => "حدف با موفقیت انجام شد",
        "successInsert" => "اطلاعات با موفقیت دخیره گردید",
        "successUpdate" => "اطلاعات با موفقیت بروزرسانی گردید",
        "successNotUpdate" => "اطلاعات با موفقیت بروزرسانی نشد!",
        "uploadFileSuccess" => "تصویر با موفقیت باگذاری شد",
        "uploadFileNotSuccess" => "تصویر با موفقیت باگذاری نشد",
    ]
];