<?php

return ['view' =>
    [
        'index' =>
            [
                "Create_New_FAQ" => "Create New FAQ [fr]",
                "List" => "List [fr]",
                "Question" => "Question [fr]",
                "No_Question" => "No_Question [fr]",
                "Answer" => "Answer [fr]",
                "No_Answer" => "No_Answer [fr]",
                "Edit" => 'Edit [fr]',
                "Type" => "Type [fr]",
                "Delete" => 'Delete [fr]'
            ],
        'form' =>
            [
                "Back" => "Back [fr]",
                "Edit_FAQ" => "Edit FAQ [fr]",
                "Create_FAQ" => "Create FAQ [fr]",
                "Question" => "Question [fr]",
                "Answer" => "Answer [fr]",
                "Update" => "Update [fr]",
                "Create" => "Create [fr]",
                "Cancel" => "Cancel [fr]",
                "Type" => "Type [fr]",

            ]
    ]
    , 'controller' =>
        [
        ]
    , 'messages' => [
        "errCreate" => "Application problem in create new Content! [fr]",
    ]
];