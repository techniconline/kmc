<?php

return [
    'view' => [
        'index' =>
            [
                "Calendar" => "تقویم",
                "Add_Reminder" => "افزودن برنامه",
                "Remove_Reminder" => "حذف برنامه",
            ],
        'form' =>
            [
                "Title" => "عنوان",
                "Color" => "رنگ",
                "Start" => "شروع",
                "End" => "پایان",
                "All_Day" => "همه ی روز",
                "Cancel" => "انصراف",
                "Date" => "تاریخ",
                "Save" => "ذخیره",
                "Delete" => "حذف",
                "Time" => "ساعت",
            ]
    ],
    'messages' => [
        'fillError' => 'اطلاعات را کامل پر کنید',
        'success' => 'برنامه کاری ذخیره شد',
        'successUpd' => 'برنامه کاری با موقیت تغییر کرد',
        'successDel' => 'برنامه کاری حذف شد'
    ]
];