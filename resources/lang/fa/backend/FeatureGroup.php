<?php

return ['view' =>
    [
        'index' =>
            [
                "CreateNew" => "ایجاد",
                "List" => "فهرست",
                "Name" => "نام",
                "Edit" => "ویرایش",
                "Delete" => "حذف",
                "Type" => "نوع",
            ],
        'create' =>
            [
                "Back" => "برگشت",
                "Name" => "نام",
                "Type" => "نوع",
                "Cancel" => "انصراف",

                "color" => "رنگ",
                "integer" => "مقدار عددی",
                "text" => "متن",
                "FeatureList" => "لیست ویژگی ها",


            ]
    ]
    , 'controller' =>
        [
            "Edit" => "ویرایش ",
            "Update" => "بروزرسانی ",
            "Insert" => "ذخیره ",
            "Create" => "ایجاد ",
        ]
    , 'messages' => [
        "errCreate" => "برای ایجاد  سیستم دچار مشکل شده!",
        "errValid" => "این ردیف معتبر نیست با مدیر سایت تماس بگیرید!",
        "msgInsertOk" => "دخیره اطلاعات موفقیت انجام شد.",
        "msgUpdateOk" => "بروز رسانی با موفقیت انجام شد.",
        "msgUpdateNotOk" => "بروز رسانی با موفقیت انجام نشد!",
        "msgUpdateNotOkWarningLang" => "اطلاعاتی برای ترجمه ها تغییر نکرده است!",
        "msgDelOk" => "حدف با موفقیت انجام شد.",
        "msgDelNotOk" => "حدف با موفقیت انجام نشد!",
    ]
];