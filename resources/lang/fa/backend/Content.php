<?php

return ['view' =>
    [
        'index' =>
            [
                "Create_New_content" => "ایجاد مطلب جدید",
                "Create_New_news" => "ایجاد خبر",
                "Create_New_weblog" => "ایجاد بلاگ",

                "List" => "فهرست",
                "Author" => "نویسنده",
                "Browser_Title" => "عنوان",
                "Status" => "وضعیت انتشار",
                "Link_Title" => "عنوان پیوند",
                "Content_Title" => "عنوان مطلب",
                "Content" => "مطلب",
                "Cat" => "موضوع",
                "Param" => "کمک کننده پارامتر",
                "Address" => "نشانی مطلب",
                "ShortContent" => "خلاصه مطلب",
                "ImageContent" => "تصویر مطلب",
                "Access" => "دسترسی",
                "Public" => "عمومی",
                "Members" => "اعضاء",
                "Edit" => 'ویرایش',
                "Delete" => 'حدف'
            ],
        'form' =>
            [
                "Back" => "برگشت",
                "Link_Title" => "عنوان پیوند",
                "URL" => "مسیر",
                "TAGS" => "تگ ها",
                "Title" => "عنوان",
                "Create_New_content" => "ایجاد مطلب جدید",
                "Create_New_news" => "ایجاد خبر",
                "Create_New_weblog" => "ایجاد بلاگ",

                "Update" => "بروزرسانی",
                "Create" => "ایجاد",
                "Body_Title" => "عنوان بدنه",
                "Body_Contents" => "مطلب بدنه",
                "Browser_Title" => "عنوان مرورگر",
                "ShortContent" => "خلاصه مطلب",
                "ImageContent" => "تصویر مطلب",
                "Access" => "دسترسی",
                "Public" => "عمومی",
                "Members" => "اعضاء",
                "Author" => "نویسنده",
                "Cancel" => "انصراف",
                "Type" => "نوع مطلب",
                "Sort" => "اولویت",
                "Status" => "وضعیت",
                "Enable" => "فعال",
                "Disable" => "غیرفعال",
                "blog" => "بلاگ",
                "weblog" => "وب لاگ",
                "news" => "خبر",
                "static" => "ثابت",
                "index-list" => "لیست در صفحه اول",
            ]
    ]
    , 'controller' =>
        [
            "ApartmentEdit" => "ویرایش",
            "UpdateApartment" => "برئزرسانی",
            "InsertApartment" => "دخیره",
            "CreateApartment" => "ایجاد",
        ]
    , 'messages' => [
        "errCreate" => "مشکل در ایجاد مطلب جدید",
        "successDelete" => "حدف با موفقیت انجام شد",
        "successInsert" => "اطلاعات با موفقیت دخیره گردید",
        "successUpdate" => "اطلاعات با موفقیت بروزرسانی گردید",
        "successNotUpdate" => "اطلاعات با موفقیت بروزرسانی نشد!",
        "uploadFileSuccess" => "تصویر با موفقیت باگذاری شد",
        "uploadFileNotSuccess" => "تصویر با موفقیت باگذاری نشد",
    ]
];