<?php

return ['view' =>
    [
        'about' =>
            [
                "Title" => "About",
            ],
        'services' =>
            [
                "Title" => "Services",
            ]
    ]
    , 'controller' => [

    ]
    , 'messages' => [
        "err" => "You have problem for saving data! [fr]",
        "errCreate" => "Application problem in create item! [fr]",
    ]
];