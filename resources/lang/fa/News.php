<?php

return [
    'view' => [
        'index' => [
            'PostedBy' => 'نویسنده',
            'RecentPostsNews' => 'آخرین اخبار',
            'RecentPostsNewsText' => 'آخرین اخبار روز خودرو سازها',
            'Contact_Us_Title' => 'ارسال پیام',
            'ReadMore' => 'ادامه مطلب',
            'Entities' => 'تعداد ردیف ها',
            'CategoryList' => 'دسته بندی اخبار',
            'Comments' => 'نظرات:',

        ],
        'info' => [
            'Info' => 'اطلاعات تماس',
            'Address' => 'تهران، ونک، ملاصدرا، شیراز شمالی، دانشور شرقی، پلاک 26، طبقه هفتم، واحد E7 ',
            'Tel' => ' 3 - 88618650 21+ ',
            'Site' => ' 1732.ir ',
            'Email' => ' info@1732.ir ',
            'Text' => 'تاکنون سعی بر این داشته در راستای رضایتمندی مشتری و تکریم ارباب رجوع قدم برداشته و یکی از اهداف این نمایندگی که همانا جلب رضایت و اعتماد کامل مشتریان است را سرلوحه کار خویش قرارداده ایم.
 <br>
باشد که بتوانیم برای همیشه این راه را ادامه دهیم.',

        ]
    ],
    'controller' => [
        'captcha_error' => 'Vous devez remplir CAPTCHA'
    ],
    'model' => [
        'itemContactUs' => ['Information' => 'Information fr', 'Reservation' => 'Reservation fr', 'Visit' => 'Visit fr', 'Billing' => 'Billing fr'],
        'submit' => 'ارسال',
        'Email' => 'پست الکترونیک*',
        'Tel' => 'تلفن تماس',
        'Message' => 'متن',
        'Name' => 'نام کاربری*',
    ],
    'addcomment' => [
        'title' => 'نظر خود را برای ما بنویسید',
        'Email' => 'پست الکترونیک*',
        'Tel' => 'تلفن تماس',
        'Message' => 'متن',
        'Name' => 'نام کاربری*',
        'Reply' => 'پاسخ',
        'Submit' => 'ارسال نظر',
        'SubmitReply' => 'ارسال',
        'Close' => 'بستن',
        'Download' => 'دریافت فایل',
        'Del' => 'حذف',
        'Edit' => 'ویرایش',

    ],
    'comments' => [
        'title' => 'نظرات شما'
    ]
];