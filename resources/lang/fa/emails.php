<?php

return ['view' =>
    [
        'unavailable-document' =>
            [
                "HiDear" => "Hi Dear",

            ]
    ]
    , 'controller' => [
        "" => "",
    ]
    , 'messages' => [
        "err" => "You have problem for saving data!",
        "errCreate" => "Application problem in create item!",
        "errDate" => "Date is not valid!",
        "msgCreate" => "Save successful!",
        "errNoFindData" => "Not find data, contact to administrator!",
        "msgUpdateOk" => "Update successfully.",
        "msgUpdateNotChanged" => "No information has been changed!",
        "msgUpdateNotOk" => "Update unsuccessfully.",
        "msgDelOk" => "Delete successfully.",
        "msgDelNotOk" => "Delete unsuccessfully.",
    ]
];