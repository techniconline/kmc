<?php

return ['view' =>
    [
        'index' =>
            [
                "CreateWebLog" => "ایجاد مطلب جدید",
                "TitleCategory" => "دسته بندی ها",
                "RecentPostsWebLog" => "آخرین مطالب",
                "RecentPostsWebLogText" => "آخرین مطالب در وب لاگ",
            ]
    ]
    , 'controller' => [
        "Save" => "ذخیره",
    ]
    , 'messages' => [
        "err" => "شما در ذخیره اطلاعات مشکل دارید!",
        "errCreate" => "سیستم در ایجاد دارای مشکل است!",
        "msgCreate" => "ذخیره سازی با موفقیت انجام شد",
        "msgNotCreate" => "ذخیره سازی با موفقیت انجام نشد!",
        "errNoFindData" => "داده مرتبط یافت نشد!",
        "msgUpdateOk" => "بروز رسانی با موفقیت انجام شد.",
        "msgUpdateNotOk" => "بروز رسانی با موفقیت انجام نشد.",
        "msgDelOk" => "حدف با موفقیت انجام شد.",
        "msgDelNotOk" => "حدف با موفقیت انجام نشد.",
        "subjectReplyComment" => "پیغامی برای پست شما",
        "HiDear" => "سلام دوست عزیز",
        "PleaseLogIn" => "لطفا برای ثبت وب لاگ در وب سایت ثبت نام کنید!",
    ]
];