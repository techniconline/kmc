<?php

return [
    'view' => [
        'index' =>
            [
                "Calendar" => "Calendar [fr]",
                "Add_Reminder" => "Add Reminder [fr]",
                "Remove_Reminder" => "Remove Reminder [fr]",
            ],
        'form' =>
            [
                "Title" => "Title [fr]",
                "Color" => "Color [fr]",
                "Start" => "Start [fr]",
                "End" => "End [fr]",
                "All_Day" => "All Day [fr]",
                "Cancel" => "Cancel [fr]",
                "Date" => "Date [fr]",
                "Save" => "Save [fr]",
                "Delete" => "Delete [fr]",
                "Time" => "Time [fr]",
            ]
    ],
    'messages' => [
        'fillError' => 'Somethings Went Wrong!!! [fr]',
        'success' => 'Successfully New Reminder Added [fr]',
        'successUpd' => 'Successfully Updated This Reminder [fr]',
        'successDel' => 'Successfully Deleted [fr]'
    ]
];