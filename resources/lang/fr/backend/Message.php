<?php

return ['view' =>
    [
        'index' =>
            [
                "List" => "List [fr]",
                "Message" => "Message [fr]",
                "From" => "From [fr]",
                "To" => "To [fr]",
                "Date" => "Date [fr]",
                "Edit" => 'Edit [fr]',
                "Delete" => 'Delete [fr]',
                "Answer" => "Answer [fr]",
                "Answered" => "Answered [fr]"
            ],
        'form' =>
            [
                "Back" => "Back [fr]",
                "Answer_To" => "Answer To [fr]",
                "Cancel" => "Cancel [fr]",
                "Send" => "Send [fr]"
            ]
    ],
    'messages' => [
        'fillError' => 'Something Went Wrong!!!! [fr]',
        'success' => 'Successfully Your Answer Sent to :email [fr]',
        'sentError' => 'Unfortunaly Your Answer does not Sent to :email. Please Try Again [fr]'
    ]
];