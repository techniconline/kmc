<?php

return ['view' =>
    [
        'index' =>
            [
                "Create_New_Link" => "Créer un nouveau lien",
                "List" => "Liste",
                "URL" => "URL",
                "Title" => "Titre",
                "Desc" => "Description",
                "Edit" => 'éditer',
                "Delete" => 'supprimer'
            ],
        'form' =>
            [
                "Edit_Link" => "Lien Modifier",
                "Back" => "Retour",
                "Title" => "Titre",
                "URL" => "URL",
                "Title" => "Titre",
                "Desc" => "Description",
                "Update" => "mise à jour",
                "Create" => "Créer",
                "Cancel" => "Annuler",
            ]
    ]
    , 'controller' =>
        [
            "ApartmentEdit" => "Apartment Edit",
            "UpdateApartment" => "Update Apartment",
            "InsertApartment" => "Insert Apartment",
            "CreateApartment" => "Create Apartment",
        ]
    , 'messages' => [
        "errCreate" => "Application problem in create new Content! [fr]",
        "errValid" => "This apartment is not valid, contact to administrator!",
        "msgUpdateOk" => "Update successfully.",
        "msgUpdateNotOk" => "Update unsuccessfully.",
        "msgDelOk" => "Delete successfully.",
        "msgDelNotOk" => "Delete unsuccessfully.",
    ]
];