<?php

return ['view' =>
    [
        'index' =>
            [
                "AddNewRent" => "Add Rent Room",
                "ListOfUserRent" => "List of users rent",
                "GenerateListRentFor" => "Generate List Rent For",
                "SendMailRemember" => "Send Mail for remember date ",
                "InvoiceList" => "Invoice List",
                "ExportInvoiceList" => "Export Invoice List",
            ],
        'listBilling' =>
            [
                "Paid" => "Paid",
                "Unpaid" => "Unpaid",
                "Username" => "Username",
                "Edit" => "Edit",
                "Delete" => "Delete",
                "Price" => "Price",
                "Room" => "Room Number",
                "RegDate" => "Register Date",
                "DateRent" => "Rent Date",
                "Search" => "Search",
                "To" => "To",
                "From" => "From",
                "LastName" => "Last name",
                "Email" => "Email",
                "Mobile" => "Mobile",
            ],
        'form' =>
            [
                "Back" => "Back",
                "RoomList" => "Room List",
                "RentDate" => "Rent Date",
                "Price" => "Price",
                "Cancel" => "Cancel",
                "Save" => "Save",
                "TitleSave" => "Add new price of rent",
                "Update" => "Update",
                "TitleUpdate" => "Update price of rent",
                "listBillingStatus" => ['1' => 'Paid', '2' => 'Unpaid'],

            ]
    ]
    , 'controller' =>
        [

        ]
    , 'messages' => [
        "addNewRent" => "This rent added to system.",
        "errCreateDuplicate" => "This data is duplicate for this rent!",
        "errCreate" => "Application have problem in create!",
        "errValid" => "This data is not valid, contact to administrator!",
        "msgUpdateOk" => "Update successfully.",
        "msgUpdateNotOk" => "Update unsuccessfully.",
        "msgDelOk" => "Delete successfully.",
        "msgDelNotOk" => "Delete unsuccessfully.",
        "YouRentIsUnavailable" => "Your Rent is not available.",
        "SendMailOk" => "Send mail successfully.",
        "SendMailNotOk" => "Send mail Unsuccessfully.",

        "NotValidData" => "This data is not valid!",
        "NotValidDataForGenerate" => "This data is not valid for generate list rent!",
        "SuccessGenerate" => "Generate list rent successfully.",

        "Remember" => "Remember",
        "SubjectDueInvoice" => "Declare rental invoice due date",
        "TextDueInvoice" => "Please, refer to financial department to pay rental fee.",

        "SubjectInvoice" => "Invoice for room rent",
        "ThankYou" => "thank you for paid",
        "TextInvoice" => "Your invoice for date",
        "Is" => "is",
        "Euro" => "euro",
    ]
];