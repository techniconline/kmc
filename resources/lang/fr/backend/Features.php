<?php

return ['view' =>
    [
        'index' =>
            [
                "CreateNewFeature" => "ایجاد ویژگی جدید",
                "List" => "فهرست",
                "Name" => "نام",
                "Edit" => "ویرایش",
                "Delete" => "حذف",
                "Type" => "نوع",
            ],
        'create' =>
            [
                "Back" => "برگشت",
                "Name" => "نام",
                "Type" => "نوع",
                "Cancel" => "انصراف",

                "color" => "رنگ",
                "integer" => "مقدار عددی",
                "text" => "متن",


            ]
    ]
    , 'controller' =>
        [
            "FeatureEdit" => "ویرایش ویژگی",
            "UpdateFeature" => "بروزرسانی ویژگی",
            "InsertFeature" => "ذخیره ویژگی",
            "CreateFeature" => "ایجاد ویژگی",
        ]
    , 'messages' => [
        "errCreate" => "برای ایجاد ویژگی سیستم دچار مشکل شده!",
        "errValid" => "این ویژگی معتبر نیست با مدیر سایت تماس بگیرید!",
        "msgUpdateOk" => "بروز رسانی با موفقیت انجام شد.",
        "msgUpdateNotOk" => "روز رسانی با موفقیت انجام نشد!",
        "msgDelOk" => "حدف با موفقیت انجام شد.",
        "msgDelNotOk" => "حدف با موفقیت انجام نشد!",
    ]
];