<?php

return ['view' =>
    [
        'index' =>
            [
                "CreateNewApartment" => "Create New Apartment",
                "List" => "List",
                "Title" => "Title",
                "Edit" => "Edit",
                "Delete" => "Delete",
                "Rooms" => "Rooms",
                "Floor" => "Floor",
                "Parking" => "Parking",
                "Address" => "Address",
                "ApartmentName" => "Apartment Name",
            ],
        'create' =>
            [
                "Back" => "Back",
                "Title" => "Title",
                "ShortDescription" => "Short Description",
                "Description" => "Description",
                "Upload" => "Upload",
                "FileUpload" => "File Upload",
                "StartImageUpload" => "Start Image Upload",
                "ImageList" => "Image List",
                "Image" => "Image",
                "Delete" => "Delete",
                "Link" => "Link",
                "Map" => "Map",
                "Cancel" => "Cancel",
                "Address" => "Address",
                "StartVideoUpload" => "Start Video Upload",
                "VideoList" => "Video List",
                "FloorCount" => "Floor Count",
                "RoomCount" => "Room Count",
                "ParkingCount" => "Parking Count",
                "IsDefault" => "Is Default",

            ]
    ]
    , 'controller' =>
        [
            "ApartmentEdit" => "Apartment Edit",
            "UpdateApartment" => "Update Apartment",
            "InsertApartment" => "Insert Apartment",
            "CreateApartment" => "Create Apartment",
        ]
    , 'messages' => [
        "errCreate" => "Application problem in create apartment!",
        "errValid" => "This apartment is not valid, contact to administrator!",
        "msgUpdateOk" => "Update successfully.",
        "msgUpdateNotOk" => "Update unsuccessfully.",
        "msgDelOk" => "Delete successfully.",
        "msgDelNotOk" => "Delete unsuccessfully.",
    ]
];