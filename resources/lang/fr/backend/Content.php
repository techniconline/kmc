<?php

return ['view' =>
    [
        'index' =>
            [
                "Create_New_content" => "ایجاد مطلب جدید",
                "Create_New_news" => "ایجاد خبر",
                "Create_New_weblog" => "ایجاد بلاگ",
                "List" => "فهرست",
                "Author" => "نویسنده",
                "Browser_Title" => "عنوان صفحه مرورگر",
                "Link_Title" => "عنوان پیوند",
                "Content_Title" => "عنوان مطلب",
                "Content" => "مطلب",
                "ShortContent" => "خلاصه مطلب",
                "ImageContent" => "تصویر مطلب",
                "Access" => "دسترسی",
                "Public" => "عمومی",
                "Members" => "اعضاء",
                "Edit" => 'ویرایش',
                "Delete" => 'حدف'
            ],
        'form' =>
            [
                "Back" => "برگشت",
                "Link_Title" => "عنوان پیوند",
                "URL" => "مسیر",
                "Title" => "عنوان",
                "Create_New_content" => "ایجاد مطلب جدید",
                "Create_New_news" => "ایجاد خبر",
                "Update" => "بروزرسانی",
                "Create" => "ایجاد",
                "Body_Title" => "عنوان بدنه",
                "Body_Contents" => "مطلب بدنه",
                "Browser_Title" => "عنوان مرورگر",
                "Access" => "دسترسی",
                "Public" => "عمومی",
                "Members" => "اعضاء",
                "Author" => "نویسنده",
                "Cancel" => "انصراف",
                "Type" => "نوع مطلب",
                "blog" => "بلاگ",
                "news" => "خبر",
                "static" => "ثابت",
            ]
    ]
    , 'controller' =>
        [
            "ApartmentEdit" => "ویرایش",
            "UpdateApartment" => "برئزرسانی",
            "InsertApartment" => "دخیره",
            "CreateApartment" => "ایجاد",
        ]
    , 'messages' => [
        "errCreate" => "مشکل در ایجاد مطلب جدید",
        "successDelete" => "حدف با موفقیت انجام شد"
    ]
];