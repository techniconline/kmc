<?php

return ['view' =>
    [
        'index' =>
            [
                "ListApartment" => "List Apartment",
                "MoreInformation" => "Plus D'Information",
            ],
        'info' =>
            [
                "GoToProfile" => "GoTo Your Profile For complete reservation",
                "Thanks" => "Thank you for selecting rooms",
                "SelectNewRoom" => "Thank you for selecting rooms",

                "ListApartmentType" => "List Apartment Type",
                "ApartmentInfo" => "Apartment Information",
                "Rooms" => "Chambres",
                "Area" => "Area",
                "MonthlyRent" => "Loyer mensuel",
                "AddRoom" => "Select",
                "SelectedRoom" => "Selected Room",
                "ClickForRemove" => "Click For Remove",
                "GalleryImage" => "Gallery Image",
                "Location" => "Emplacement",
                "Reservation" => "Réservation",

                "pourrez" => "Vous pourrez modifier vos choix à partir de votre",
                "MaxSelection" => "Sélectionnez au maximum 3 logements.",
                "Account" => "Compte",
                "Fois" => "Une fois que vous avez fait votre choix, retournez à la page de votre",
                "FoisCn" => "et nous envoyer les documents demandés.",


                "ApartmentService" => "POUR ÉTUDIER L’ESPRIT TRANQUILLE",
                "ApartmentServiceText" => "<p>La résidence GALILÉE offre de nombreuses prestations qui faciliteront votre quotidien</p>
                    <p>vous permettront d’étudier dans la sérénité et de vous détendre en toute convivialité…</p>
                    <p><b>( * ) = Frais en sus </b>
                    </p>",

                "PlaceParking" => "Place de Parking",
                "Parking" => "Parking (*)",
                "Local" => "Local à vélos",

                "SpaceConnected" => "Espace de Convivialité et Connecté",
                "Salon" => "Salon TV / détente",
                "SalonLecture" => "Salon de lecture",
                "Scanner" => "Scanner",
                "Ordinateurs" => "Ordinateurs en libre accès",
                "Imprimante" => "Imprimante / Photocopieur (*)",
                "Tel" => "Téléphone / Fax",
                "Wifi" => "Connexion Wifi",

                "SecureAccess" => "Accés Securisee et Adapté à Tous",
                "Accueil" => "Accueil",
                "Access" => "Accès adapté aux personnes à mobilité réduite",
                "Control" => "Contrôle d'accès par badge",
                "Ascenseur" => "Ascenseur",

                "Amenities" => "Des Commodités",
                "Aspirateur" => "Aspirateur",
                "Fer" => "Fer à repasser",
                "Service" => "Service de lingerie (draps fournis)",
                "Laverie" => "Laverie (*)",
                "ServiceEnt" => "Service d’entretien (*)",
                "Kit" => "Kit de vaisselle (*)",

                "Fitness" => "Un Espace Fitness",
                "Sale" => "Salle de gym",
                "Sprtif" => "Équipement sportif",


                "Charge" => "charge comprise",

                "Emplacement" => "Residence Emplacement",


            ]
    ]
    , 'controller' => [
        "Insert_Room_Category" => "Insert Room Category",
    ]
    , 'messages' => [
        "err" => "You have problem for saving data!",
        "errCreate" => "Application problem in create item!",
        "msgCreate" => "Save successful!",
        "errNoFindData" => "Not find data, contact to administrator!",
        "msgUpdateOk" => "Update successfully.",
        "msgUpdateNotChanged" => "No information has been changed!",
        "msgUpdateNotOk" => "Update unsuccessfully.",
        "msgDelOk" => "Delete successfully.",
        "msgDelNotOk" => "Delete unsuccessfully.",
    ]
];