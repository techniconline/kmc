<?php

return ['view' =>
    [
        'index' =>
            [
                "TITLE_HEAD" => "FAQ",
                "FAQ_TOTAL_DATA_TYPE" => [
                    'FAQ-General' => 'Générale FAQ'
                    , 'FAQ-Residence' => 'Résidence FAQ'
                    , 'FAQ-Reservation' => 'Réservation FAQ'
                    , 'FAQ-Conditions' => 'Les Conditions D\'admission'
                    , 'FAQ-Fournir' => 'Les Pièces à Fournir'
                    , 'FAQ-Reglement' => 'Règlement Intérieur'
                ],

                "FAQ_DATA_TYPE" => [
                    'FAQ-General' => 'Générale FAQ'
                    , 'FAQ-Residence' => 'Résidence FAQ'
                    , 'FAQ-Reservation' => 'Réservation FAQ'
                ],

                "FAQ_GUIDE_DATA_TYPE" => [
                    'FAQ-Conditions' => 'Les Conditions D\'admission'
                    , 'FAQ-Fournir' => 'Les Pièces à Fournir'
                    , 'FAQ-Reglement' => 'Règlement Intérieur'
                ],
            ]
    ]
    , 'controller' => [

    ]
    , 'messages' => [
        "err" => "You have problem for saving data! [fr]",
        "errCreate" => "Application problem in create item! [fr]",
    ]
];