<?php

return ['view' =>
    [
        'index' =>
            [
                "SelectItemPlease" => "Select top item and continues for renting...",
                "Profile" => "Profile",
                "CompletingYourApplication" => "Completing Your Application",
                "AcceptWithGuideSelection" => "Accept With Guide Selection",
                "LinkAcceptWithGuideSelection" => "click link and view Guide Selection",
                "AcceptWithGuideFAQ" => "Accept With Guide FAQ",
                "LinkAcceptWithGuideFAQ" => "click link and view FAQ",
                "Edit" => "Edit",
                "Details" => "Details",
                "Delete" => "Delete",
                "user" => "My Account",
                "bookingDate" => "Booking Information",
                "info" => "User Info",
                "parent" => "Parent Info",
                "userDocUpload" => "User Documents",
                "userDocUploadGuarantor1" => " Guarantor 1 Documents",
                "userDocUploadGuarantor2" => " Guarantor 2 Documents",
                "UploadFilesTitle" => "Upload files, with Click OR Drag & Drop Files(english)",

            ],
        'partials' =>
            [
                "user" => "My Account",
                "info" => "User Info",
                "parent" => "Parent Info",
                "bookingDate" => "Booking Information",
                "userDocUpload" => "User Documents",
                "userDocUploadGuarantor1" => " Guarantor 1 Documents",
                "userDocUploadGuarantor2" => " Guarantor 2 Documents",
            ],
        'messagesUser' =>
            [
                "ListMessagesUser" => "List your messages",
                "ListMessagesForAttachment" => "List your messages about attachments",
                "From" => "From",
                "Subject" => "Subject",
                "Text" => "Text",
                "Date" => "Date",
                "View" => "View",
                "New" => "New",
                "Read" => "Read",
                "YourMessage" => "Your Message",
                "Assessor" => "Assessor",

            ],
        'DocUpload' =>
            [
                "saveContinue" => "Save and continue",
                "saveSend" => "Save and send all documents",
                "Upload" => "Upload",
                "Link" => "Attach Link",
                "LinkIsOk" => "Attach Link (this document Confirmed.)",
                "ThisDocumentIs" => "This Document Is",
                "StartImageUpload" => "Start Image Upload",
                "titleSaveDesc" => "Click save button after uploaded all files, please. en",
                "Delete" => "Delete",

            ],
        'create' =>
            [
                "Back" => "Back",
            ],
        'bookingDate' =>
            [
                "TheTermRent" => "The term rent",
                "save" => "Ajouter la Date et Continuer",
                "saveApartment" => "Add rooms and save continue",
                "ListReservation" => "List Reservation",
                "PleaseSelectRoom" => "Please select room for reservation",
                "YouCanSelect" => "You can select",
                "YouHaveMadeAllChoice" => "You Have Made All Your Choice",
                "Selected" => "Selected",
                "from" => "from",
                "fromDate" => "From Date",
                "toDate" => "To Date",
            ],
        'user' =>
            [
                "fName" => "First Name",
                "lName" => "Last Name",
                "email" => "Email",
                "City" => "City",
                "birthDate" => "Birth Date",
                "CodePostal" => "Code Postal",
                "Address" => "Address",
                "Country" => "Country",
                "Mobile" => "Mobile",
                "Tel" => "Telephone",
                "save" => "Save and continue",
                "gender" => "Gender",
                "genderItem" => ['male' => 'Male', 'female' => 'Female'],
            ],
        'parent' =>
            [
                "fName" => "First Name",
                "lName" => "Last Name",
                "email" => "Email",
                "CodePostal" => "Code Postal",
                "Address" => "Address",
                "Country" => "Country",
                "Mobile" => "Mobile",
                "save" => "Save and continue",
                "SameTenant" => "Same as the tenant",
                "parentTenant" => ['Oui' => 'Oui', 'Non' => 'Non'],
            ],
        'info' =>
            [
                "save" => "Save and continue",
                "YouAre" => "You Are",
                "PersonalMessage" => "Personal Message",
                "AccuracyInformation" => "I hereby confirm the accuracy of the information provided",
                "YourLevelEducation" => "Your Level Education",
                "Type" => "Type",
                "YourInstitution" => "Your Institution",
                "YourAccommodationDuringYourRental" => "Your Accommodation During Your Rental",
                "FacilityName" => "Facility Name",
                "PropertyTown" => "Property Town",
                "DoYouHaveGuarantorForTheLease" => "Do you have a guarantor for the lease",

                "personaItems" => ['student' => 'student', 'trainee' => 'trainee', 'another' => 'another'],
                "educationItems" => ['Terminale' => 'Terminale', 'Bac Pro' => 'Bac Pro', 'Bac Bac+1' => 'Bac Bac+1', 'Bac+2' => 'Bac+2', 'Bac+3' => 'Bac+3'
                    , 'Bac+4' => 'Bac+4', 'Bac+5' => 'Bac+5', 'Stagiaire' => 'Stagiaire', 'Autre' => 'Autre'],
                "institution_typeItems" => ['BTS IUT' => 'BTS IUT', 'Prépa Commerce' => 'Prépa Commerce', 'Prépa Ingénieur' => 'Prépa Ingénieur', 'Université' => 'Université'
                    , 'Ecole de Commerce' => 'Ecole de Commerce', 'Ecole d`Ingénieur' => 'Ecole d`Ingénieur', 'Entreprise' => 'Entreprise', 'Lycée' => 'Lycée', 'Autre' => 'Autre'],
                "current_institution_typeItems" => ['BTS IUT' => 'BTS IUT', 'Prépa Commerce' => 'Prépa Commerce', 'Prépa Ingénieur' => 'Prépa Ingénieur', 'Université' => 'Université'
                    , 'Ecole de Commerce' => 'Ecole de Commerce', 'Ecole d`Ingénieur' => 'Ecole d`Ingénieur', 'Entreprise' => 'Entreprise', 'Lycée' => 'Lycée', 'Autre' => 'Autre'],
                "parent_guarantorItems" => ['OuiA' => 'Oui 1 personne', 'OuiB' => 'Oui 2 personne', 'Non' => 'Non'],
            ],
        'pass' =>
            [
                'Reset_Password' => 'Reset Password [fr]',
                'Current_Password' => 'Current Password [fr]',
                'New_Password' => 'New Password [fr]',
                'Confirm_Password' => 'Confirm Password [fr]',
                'Save_Password' => 'Save Password [fr]'
            ],
        'avatar' =>
            [
                'Avatar' => 'Avatar [fr]',
                'Change_Avatar' => 'Change Avatar [fr]',
                'Save' => 'Save [fr]',
                'SaveSuceess' => 'New Avatar Saved Succesfully [fr]'
            ]
    ]
    , 'controller' => [
        "Insert_Room_Category" => "Insert Room Category",
    ]
    , 'messages' => [
        "err" => "You have problem for saving data!",
        "errCreate" => "Application problem in create item!",
        "errDate" => "Date is not valid!",
        "errBookingNotValid" => "Your Booking is not valid or not create!",
        "msgCreate" => "Save successful!",
        "errNoFindData" => "Not find data, contact to administrator!",
        "msgUpdateOk" => "Update successfully.",
        "msgUpdateNotChanged" => "No information has been changed!",
        "msgUpdateNotOk" => "Update unsuccessfully.",
        "msgDelOk" => "Delete successfully.",
        "msgDelNotOk" => "Delete unsuccessfully.",
        "Match_Password" => "This Password is Incorrect and Does Not Match [fr]",
        "Wrong" => "Something Was Wrong! Please Try Again [fr]",
        "Sucessfuly" => "Successfully changed password [fr]",
        "AllDocsCompleted" => "Thank you for completed all your documents. Your documents has been sent for review to the relevant department.",
        "DisableTab" => "This tab is disable, Please complete the previous tabs.",
        "errRoom" => "please select room for reservation!",
        "errAccept" => "please accept of Guide and Faq!",
        "YouHaveBookingActive" => "You have a active booking, please contact to administrator!",

    ]
];