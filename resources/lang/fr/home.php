<?php

return ['view' =>
    [
        'index' =>
            [
                "TitleIntro" => "Imaginez un appartement à la fois chic et cosy, mariant simplicité et élégance...
                Imaginez une résidence au service de ses occupants, conçue pour faciliter votre quotidien... Cessez d'imaginer !
                <b>Bienvenu chez vous !! </b>",

                "TitleLogo" => "Résidence pour étudiants",
                "TitlePageTerms" => "Mentions légales",
                "Reservation" => "Réservation",
                "SelectLang" => "EN",
                "CopyRight" => "&copy;2014-2015 Studenest. All Rights Are Reserved.",
                "InfoContactIndex" => "<br><strong>Telephone: 01 77 19 61 14</strong><br><strong>Ouverture : lun-ven 9h - 18h</strong>",

                "Profile" => "Profile [fr]",
                "Change_Password" => "Change Password [fr]",
                "YourMessage" => "Your Message [fr]",
                "Logout" => "Logout [fr]",
                "Login" => "Login [fr]",
                "Register" => "Register [fr]",
                "New" => "New [fr]",
            ],
        'register' =>
            [

                "Title" => "Créer un compte",
                "Text" => "Si vous n'avez pas de compte, vous pouvez en créer un.",
                "Fname" => "First Name",
                "Lname" => "Last Name",
                "Email" => "E-Mail Address",
                "Password" => "Password",
                "ConfirmPass" => "Confirm Password",
                "Register" => "Register",
                "Gender" => "Gender",
                "DataGender" => ['male' => 'Male', 'female' => 'Female'],
            ],
        'login' =>
            [
                "Bonjour" => "Bonjour",
                "TextWelcome" => "<p>La création de votre compte est une étape nécessaire pour procéder à la sélection de votre logement et pour constituer votre dossier de réservation.</p>
        <p>Un courriel de confirmation vous sera envoyé automatiquement et vous permettra d'activer votre compte. En cas de non réception de ce courriel, vérifiez vos courriers indésirables.</p>",
                "TitleLogin" => "<h2>Me connecter</h2>
                <p>Connectez-vous pour voir votre profil.</p>",

                "Email" => "E-mail",
                "Password" => "Password",
                "Login" => "Login user",
                "TitleForget" => "Mot de passe oublié",
                "TextForget" => "Veuillez entrer votre E-mail pour recevoir le lien de réinitialisation de votre mot de passe.",
                "SendPass" => "Send Password",
            ]
    ]
    , 'controller' => [

    ]
    , 'messages' => [
        "err" => "You have problem for saving data! [fr]",
        "errCreate" => "Application problem in create item! [fr]",
        "errDate" => "Date is not valid! [fr]",
        "msgCreate" => "Save successful! [fr]",
        "errNoFindData" => "Not find dat [fr]a, contact to administrator! [fr]",
        "msgUpdateOk" => "Update successfully. [fr]",
        "msgUpdateNotChanged" => "No information has been changed! [fr]",
        "msgUpdateNotOk" => "Update unsuccessfully. [fr]",
        "msgDelOk" => "Delete successfully. [fr]",
        "msgDelNotOk" => "Delete unsuccessfully. [fr]",
    ]
];