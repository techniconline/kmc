<?php

return [
    'view' => [
        'index' => [
            "Contact_Us" => "Contact Us [fr]"
        ],
        'info' => [

        ]
    ],
    'controller' => [
        'captcha_error' => 'You Should Fill CAPTCHA [fr]'
    ],
    'model' => [
        'Management' => 'Management [fr]',
        'Rent' => 'Rent [fr]'
    ]
];