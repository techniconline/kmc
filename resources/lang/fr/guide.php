<?php

return ['view' =>
    [
        'index' =>
            [
                "Step" => "Étape",
                "compte" => "Créez votre compte",
                "appartement" => "Sélectionnez votre appartement",
                "dossier" => "Complétez votre dossier",
                "candidature" => "Etude de votre candidature",
                "contrat" => "Recevez votre contrat",

                "TextStep1" => '
                <p><h2>Créez votre compte</h2> </p><p>Pour accéder à la réservation vous devez créer votre compte.<br>Une fois votre compte créé, vous recevrez un courriel automatique de confirmation.<br>Veuillez cliquer sur le lien mentionné afin de valider la création de votre compte.</p><p>
                <a href="front/profile" class="atgLink" target="_blank" title="Open Créez votre compte">Créez votre compte</a></p>
                                    ',

                "TextStep2" => " <p><h2>À partir de votre compte, sélectionnez le(s) logement(s) de votre choix. (3 choix Maximum)</h2>
                    <p>Selon les disponibilités, le nombre de demandes et l'étude de votre dossier, un appartement équivalent à celui que vous avez sélectionné peut vous être proposé au moment de la réservation.</p>
                ",

                "TextStep3" => "
                    <p><h2>Pièces à joindre au dossier</h2><p>
                    <b>POUR VOTRE RESERVATION EN LIGNE : </b>
                    <br>Ajouter les documents vous concernant ainsi que ceux de votre (vos) garant(s) au format PDF (taille maximale pour chaque document est de 6Mo).</p>
                    ",

                "TextStep4" => "<p><h2>Etude de votre dosseir</h2>
                    <p>Si votre réservation est acceptée, vous recevrez un message dans votre compte.
                        <br>Si votre dossier est incomplet et/ou certaines pièces sont manquantes, elles peuvent vous être demandées
                        par téléphone ou par un message dans votre compte. A ce stade votre réservation est en attente.
                        <br>Nous procéderons à l’étude de votre candidature dès que l’ensemble des justificatifs nous sera parvenu.
                        Vous recevrez une réponse à votre demande dans votre compte.</p>",

                "TextStep5" => '
                        <p><h2> Votre réservation est validée: </h2> </p>
                        <p>Afin d’acter votre réservation l’ensemble des documents suivants devront nous parvenir en version papier et par
                        voie postale à l’adresse indiquée dans le contrat de location (SODEVIM, 30, Rue Péclet, 75015 Paris)<br>
                        <ul class="atgList"><li>Le contrat de location (bail)
                        </li><li>L’acte de caution solidaire (le cas échéant)
                        </li><li>La charte informatique
                        </li><li>Le règlement intérieur de la résidence GALILÉE
                        </li><li>Le Diagnostic de Performance Énergétique (DPE)
                        </li><li>L’Etat des Risques Naturels et Technologiques (ERNT)
                        </li></ul></p><p><b>Imprimez puis complétez, paraphés et signés l’ensemble des documents du dossier</b>
                        <br>Renvoyez les documents accompagnés des justificatifs, d’un Relevé d’Identité Bancaire (RIB) et d’un chèque de caution, dans les 5 jours à l’adresse indiquée dans le contrat.<br>Pour les étrangers, le Relevé d’Identité Bancaire (RIB) doit mentionner l’IBAN (International Bank Account Number).
                    </p><p><br><b>La réservation de votre logement sera effective dès que nous aurons reçu par courrier l’ensemble de votre dossier. un accusé de réception vous sera envoyé. </b></p>
                    <p>Afin de ne pas être pénalisé par les délais postaux, vous pouvez également nous faire parvenir par courriel un scan de votre recommandé postal à
                        l’adresse : reservations@residencegalilée.com</p>
                ',

            ]
    ]
    , 'controller' => [

    ]
    , 'messages' => [
        "err" => "You have problem for saving data! [fr]",
        "errCreate" => "Application problem in create item! [fr]",
    ]
];