<?php

return ['view' =>
    [
        'index' =>
            [
                "Booking" => "Booking",
            ]
    ]
    , 'controller' => [
        "Save" => "Save",
    ]
    , 'messages' => [
        "err" => "You have problem for saving data!",
        "errCreate" => "Application problem in create item!",
        "msgCreate" => "Save successful!",
        "msgCreateDuplicate" => "This record is duplicate!",
        "msgMaxCreate" => "Maximum room is 3 for booking!",
        "msgMaxCreateParking" => "Maximum parking is 1 for booking!",
        "errNoFindData" => "Not find data, contact to administrator!",
        "msgUpdateOk" => "Update successfully.",
        "msgUpdateNotChanged" => "No information has been changed!",
        "msgUpdateNotOk" => "Update unsuccessfully.",
        "msgDelOk" => "Delete successfully.",
        "msgDelNotOk" => "Delete unsuccessfully.",
        "YourSelect" => "You've selected",
        "YouCanChoose" => "You can choose",

        "Room" => "room",
        "Parking" => "parking",
    ]
];