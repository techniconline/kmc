@extends('app')
<?php  $langDetails = app('activeLangDetails');?>
<?php $user = Auth::user()->get() or null ?>

@section('browser_title')
    {!! $content->browser_title !!}
@endsection

@section('styles')
    <link rel="stylesheet" href="{{ asset('/assets/frontend/mis.css') }}" type="text/css" media="all"/>
@endsection

@section('content')

    <div id="main">
        <div class="parallax full-width-bg">
            <div class="container">
                <div class="main-title">
                    {{--<h1>{!! $content->body_title !!}</h1>--}}

                    <div class="breadcrumb">
                        <a href="/">@lang("home.view.index.HomePage")</a>
                        <span class="fa fa-angle-left"></span>
                        {{--<a href="">Blog</a>--}}
                        {{--<span class="fa fa-angle-left"></span>--}}
                        <span class="current">{!! $content->body_title !!}</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="dt-sc-margin100"></div>
        <!-- Container starts-->
        <div class="container">
            <!-- **secondary - Starts** -->
            @include('partials.sidebar')
                    <!-- **secondary - Ends** -->
            <!-- Primary Starts -->
            <section id="primary" class="with-left-sidebar page-with-sidebar">

                <!-- **Blog-post - Starts** -->
                <article class="blog-post type3 base">
                    <!-- **entry-meta - Starts** -->
                    {{--<div class="entry-meta">--}}
                    {{--<div class="date">--}}
                    {{--<p>--}}
                    {{--<span>{{$content->pDay}} <p style="font-size: 12px">{{$content->pDayName}}</p> </span>--}}
                    {{--<span><p style="font-size: 12px">{{$content->pMonthName}}</p> </span>--}}
                    {{--<span><p style="font-size: 12px">{{$content->pYear}}</p> </span>--}}
                    {{--</p>--}}
                    {{--</div>--}}
                    {{--<div class="post-comments">--}}
                    {{--<a href="#"><span class="fa fa-comment"></span> {!!count($comments)!!} </a>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    <!-- **entry-meta - Ends** -->

                    <!-- **entry-detail - Starts** -->
                    <div class="entry-detail widget widget_tag_cloud">
                        <div class="entry-title">
                            <h4><a href="">{!! $content->body_title !!}</a></h4>
                        </div>
                        <!-- **entry-meta-data - Starts** -->
                        <div class="entry-meta-data">
                            <p><span class="fa fa-user"> </span> @lang("News.view.index.PostedBy"): <a
                                        href="#"> {{$content->firstname.' '.$content->lastname}} </a></p>

                            <p>
                                <span class="fa fa-sitemap"> </span> @lang("backend/Product.view.create.Category")
                                : <a
                                        href="#"> {{$content->name_category}} </a></p>


                            <p><span class="fa fa-calendar"> </span>
                                {{$content->pDay}} {{$content->pMonthName}} {{$content->pYear}}
                            </p>

                            <p><a href="#"><span
                                            class="fa fa-comment"></span> @lang("News.view.index.Comments") {!!$content->count_comments!!}
                                </a></p>

                        </div>
                        <!-- **entry-meta-data - Ends** -->
                        {{--<div class="entry-body">--}}
                        {{--<p>{!! $content->short_content !!}</p>--}}
                        {{--</div>--}}

                        <div class="dt-sc-margin10"></div>
                        <blockquote class="type2 right dt-sc-three-fifth">
                            <span class="fa fa-quote-left"></span>
                            <q>{!! $content->short_content !!}</q>
                            <span class="fa fa-quote-right"></span>
                        </blockquote>
                        <!-- **entry-thumb - Starts** -->
                        <div class="column dt-sc-one-third img_cont">
                            <a href="{!! route(($content->type=='blog'?'content':$content->type).'.show',$content->url)  !!}">
                                        @if($content->image_content
                                                    && \Illuminate\Support\Facades\File::exists(base_path().env('PUBLIC_PATH_URL', 'public').$content->image_content.'/'.$content->contents_id.'-160x80.'.$content->extension))
                                                <img src="{{asset($content->image_content).'/'.$content->contents_id.'-160x80.'.$content->extension}}"
                                alt="{!! $content->body_title !!}" title="{!! $content->body_title !!}"/>
                                @else
                                    <img src="{{asset('/images/NoImage.jpg')}}" alt="{!! $content->body_title !!}"/>
                                @endif
                            </a>
                        </div>
                        <!-- **entry-thumb - Ends** -->
                        <div class="dt-sc-hr-invisible-very-small"></div>
                    </div>
                    <div class="dt-sc-margin10"></div>
                    <div class="type2 dt-sc-full-width">
                        <p>{!! $content->body_content !!}</p>

                        <div class="dt-sc-hr-invisible-very-small"></div>
                        <p class="tagcloud"><span class="fa fa-tag"> </span> @lang("backend/Content.view.form.TAGS"):
                            @if($content->tags && $tagsArr=explode(",",$content->tags))
                                @foreach(($tagsArr) as $tag)
                                    @if($tag)
                                        <a href="#"> {!!$tag!!} </a>
                                    @endif
                                @endforeach
                            @endif
                        </p>
                    </div>
                    <!-- **entry-detail - Ends** -->
                </article>
                <!-- **Blog-post - Ends** -->

                <!-- **post-author-details - starts** -->
                {{--<div class="post-author-details">--}}
                {{--<h3>About Author</h3>--}}

                {{--<div class="entry-author-image">--}}
                {{--<img src="http://www.placehold.it/85x85&text=Image" alt="image" title=""/>--}}
                {{--</div>--}}
                {{--<div class="author-desc">--}}
                {{--<div class="author-title">--}}
                {{--<h5><a href="#">James Dean</a></h5><span>/ Admin</span>--}}
                {{--</div>--}}
                {{--<p>The wise man therefore always holds in these matters to this principle of selection: he--}}
                {{--rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse--}}
                {{--pains.</p>--}}
                {{--</div>--}}
                {{--<div class="dt-sc-hr-invisible-small"></div>--}}
                {{--<ul class='dt-sc-social-icons'>--}}
                {{--<li><a class="dt-sc-tooltip-top facebook" href='#' target="_blank" title="Facebook"> <i--}}
                {{--class="fa fa-facebook"></i> </a></li>--}}
                {{--<li><a class="dt-sc-tooltip-top twitter" href='#' target="_blank" title="Twitter"> <i--}}
                {{--class="fa fa-twitter"></i> </a></li>--}}
                {{--<li><a class="dt-sc-tooltip-top google-plus" href='#' target="_blank" title="Google-plus"> <i--}}
                {{--class="fa fa-google-plus"></i> </a></li>--}}
                {{--<li><a class="dt-sc-tooltip-top pinterest" href='#' target="_blank" title="Pinterest"> <i--}}
                {{--class="fa fa-pinterest"></i> </a></li>--}}
                {{--<li><a class="dt-sc-tooltip-top youtube" href='#' target="_blank" title="Youtube"> <i--}}
                {{--class="fa fa-youtube"></i> </a></li>--}}
                {{--<li><a class="dt-sc-tooltip-top" href='#' target="_blank" title="linkedin"> <i--}}
                {{--class="fa fa-linkedin"></i> </a></li>--}}
                {{--<li><a class="dt-sc-tooltip-top" href='#' target="_blank" title="dribbble"> <i--}}
                {{--class="fa fa-dribbble"></i> </a></li>--}}
                {{--<li><a class="dt-sc-tooltip-top" href='#' target="_blank" title="flickr"> <i--}}
                {{--class="fa fa-flickr"></i> </a></li>--}}
                {{--<li><a class="dt-sc-tooltip-top" href='#' target="_blank" title="tumblr"> <i--}}
                {{--class="fa fa-tumblr"></i> </a></li>--}}
                {{--</ul>--}}
                {{--</div>--}}
                <!-- **post-author-details** - Ends -->
                <!-- **commententries** - Starts-->
                <div class="commententries base">
                    <h4> @lang("News.comments.title") </h4>
                    <ul class="commentlist" id="comments-list">
                    @if($comments)
                            @foreach($comments as $item)
                                <li>
                                    <div class="comment">
                                        <header class="comment-author author-image">
                                            <img src="{!! $item->avatar_image !!}" alt="{!! $item->user_name !!}"/>
                                        </header>
                                        <div class="comment-details">
                                            <div class="author-name" data-email="{!! $item->email !!}">
                                                <a href="#">{!! $item->user_name !!}</a>
                                            </div>
                                            <div class="commentmetadata comment-date"> {!!$item->sdate!!} </div>
                                            <div class="comment-body">
                                                <div class="comment-content">
                                                    <p>
                                                        {!!$item->text!!}
                                                    </p>
                                                </div>
                                            </div>
                                            {{--<div class="reply">--}}
                                            {{--<a data-toggle="modal" href="#replyComment"  class="dt-sc-button">@lang("News.addcomment.Reply")</a>--}}
                                            {{--</div>--}}
                                            <div class="like">
                                                <a href="#" data-id="{!!$item->id!!}"
                                                   class="{{isset($user->id)&&$user->id? '_like' : ''}}"><i
                                                            class="fa fa-thumbs-o-up">{!! isset($item->like_value['like'])?$item->like_value['like']:0 !!}</i>
                                                </a>
                                                <a href="#" data-id="{!!$item->id!!}"
                                                   class="{{isset($user->id)&&$user->id? '_dislike' : ''}}"><i
                                                            class="fa fa-thumbs-o-down">{!! isset($item->like_value['dislike'])?$item->like_value['dislike']:0 !!}</i>
                                                </a>

                                                        <span class="loading-like"
                                                              style="background: url('{{ asset('/assets/img/loader.gif') }}') no-repeat;
                                                                      padding: 5px 20px 20px; display: none;float: left; height: 10px; "></span>

                                            </div>
                                        </div>
                                    </div>

                                </li>
                            @endforeach
                    @endif
                    </ul>

                    @if(isset($comments) && $comments)
                        <div class="dataTables_info" id="paginate-comments" role="status" aria-live="polite"
                             style="vertical-align: middle">
                            <div class="pagination">
                                {!!$comments->appends(Input::all())->render()!!}
                            </div>
                            @lang("News.view.index.Entities") {!!$comments->total()!!}

                        </div>
                    @endif

                </div>
                <!-- **commententries- Ends** -->

                <!-- **respond - Starts** -->
                <div id="create-comment" class="base">
                    <h3> @lang("News.addcomment.title") </h3>
                    {!!Form::open(['route' => $locale.'comment.store','class'=>'form-horizontal form-bordered', 'id'=>'commentform'])!!}
                    <input type="hidden" name="item_id" value="{!! $content->id !!}"/>
                    @if($userData = Auth::user()->get())
                        <input type="hidden" value="{!! $userData->id !!}" name="user_id"/>
                    @endif
                    <input type="hidden" value="{!! isset($system)&&$system?$system:'news' !!}" name="system"/>

                    <div class="column dt-sc-one-half first">
                        <p> <span> <input {!! !isset($userData)&&!$userData?'':'disabled' !!} type="text"
                                          placeholder="@lang("News.addcomment.Name")"
                                          name="user_name"
                                          value="{{ !isset($userData)&&!$userData?'': $userData->firstname.' '.$userData->lastname }}"
                                          required/> </span></p>
                    </div>
                    <div class="column dt-sc-one-half">
                        <p> <span> <input {!! !isset($userData)&&!$userData?'':'disabled' !!} type="email"
                                          placeholder="@lang("News.addcomment.Email")"
                                          name="email"
                                          value="{{ !isset($userData)&&!$userData?'':$userData->email }}"
                                          required/> </span></p>
                    </div>
                    <p><textarea placeholder="@lang("News.addcomment.Message")" name="text"></textarea></p>

                    <div class="dt-sc-margin10"></div>
                    <p>
                        <span class="loading"
                              style="background: url('{{ asset('/assets/img/loader.gif') }}') no-repeat; padding: 5px 20px 20px; display: none;float: left;
                                      height: 10px; "></span>
                        <input id="btnSave-Comment" type="submit" value="@lang("News.addcomment.Submit")" name="submit"
                               class="button"/>
                    </p>
                    {!!Form::close()!!}
                </div>
                <!-- **respond- Ends** -->
                <div class="dt-sc-margin80"></div>
            </section>
            <!-- **Primary - Ends** -->

        </div>
        <!-- **container - Ends** -->

    </div>

    {{--<section class="fullscreen" id="contentSite" style="background-color: #FFFFFF">--}}
    {{--<div class="container">--}}
    {{--<div class="panel-heading">--}}
    {{--<img src="{!! asset($content->image_content) !!}" alt=""/><br/>--}}
    {{--<label for="title" class="title">{!! $content->body_title !!}</label>--}}
    {{--<p>{!! $content->short_content !!}</p>--}}
    {{--</div>--}}
    {{--<div class="panel-body">--}}
    {{--{!! $content->body_content !!}--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</section>--}}
@endsection

@section('scripts')
    <script src="{{ asset('/assets/js/front/comments.js') }}"></script>
    <script src="{{ asset('/assets/js/front/likes.js') }}"></script>
    <script src="{{ asset('/assets/js/front/content.js') }}"></script>

@endsection

@section('inline-scripts')



@endsection
