@extends('app')
<?php $user = Auth::user()->get() or null ?>
@section('styles')
    <link id="layer-slider" rel="stylesheet" href="{{ asset('/assets/frontend/mis.css') }}" media="all"/>

    {!!UploaderHelper::loadUploaderJquery()!!}

    @endsection

    @section('content')
    @if($dataProduct)

            <!-- **Main - Starts** -->

    <div class="parallax full-width-bg">
        <div class="container">
            <div class="main-title">
                {{--<h1>{!!$dataProduct->title!!}</h1>--}}

                <div class="breadcrumb">
                    <a href="/">@lang("home.view.index.HomePage")</a>
                    <span class="fa fa-angle-left"></span>
                    <a href="{{URL::route('category.show',['id'=>$dataProduct->category_alias])}}">{!!$dataProduct->category_name!!} </a>
                    <span class="fa fa-angle-left"></span>
                    <span class="current">{!!$dataProduct->title!!}</span>
                </div>
            </div>
        </div>
    </div>

    <div class="full-width-bg">
        <!-- **banner - Starts** -->
        <div class="banner">
            @if(($dataProduct->image) && \Illuminate\Support\Facades\File::exists(base_path().env('PUBLIC_PATH_URL', 'public').$dataProduct->image))
                <img class="" src="{!! asset($dataProduct->image) !!}" alt="Image"/>
            @else
                <img src="http://www.placehold.it/1920x600&text=Banner" alt="banner"/>
            @endif
        </div>
        <!-- **banner - Ends** -->
    </div>

    <!-- Container starts-->
    <div class="container">
        <div id="home">

        </div>
        <!-- Primary Starts -->
        <section id="primary" class="content-full-width header-mean-wrapper">
            <div class="dt-sc-margin50"></div>
            <article class="content">
                <div class="base">
                    <div class="column first image-product">
                        <!-- **recent-gallery-container - Starts** -->
                        <div class="dt-sc-one-column">
                            <div class="recent-gallery-container">
                                <!-- **recent-gallery - Starts** -->
                                @if(($dataProduct->src) && \Illuminate\Support\Facades\File::exists(base_path().env('PUBLIC_PATH_URL', 'public').'/'.$dataProduct->src))
                                    <img data-feature-value="{{$dataProduct->feature_value_selected}}"
                                         id="default-product-image" src="{!! asset($dataProduct->src) !!}" alt="Image"
                                         width="430px" height="215px"/>
                                @else
                                    <img src="http://www.placehold.it/280x280&text=Banner" alt="banner" width="430px"
                                         height="215px"/>
                                @endif

                            </div>
                        </div>
                        <div class="dt-sc-one-column">
                            <div class="dt-sc-margin15"></div>
                            <div class="color-product">
                                <ul>
                                    @foreach($feature_color as $itemC)
                                        <li class="item-color column">
                                            <a class="item-color dt-sc-tooltip-bottom"
                                               data-value="{!! trim($itemC['value']) !!}"
                                               title="{!! $itemC['title'] !!}">

                                                <i style="color: {!! $itemC['value'] !!};" class="fa fa-square"></i>

                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <!-- **recent-gallery-container - Ends** -->
                    </div>
                    <div class="column dt-sc-three-fifth prod_details">
                        <header class="dt-sc-one-column pro-title">
                            <h3>{!!$dataProduct->title!!}</h3>
                        </header>
                        <p></p>

                        <div class="dt-sc-hr-invisible-very-small"></div>
                        <!-- **project-details - Starts** -->
                        <div class="project-details">
                            <p>
                                {!!$dataProduct->short_description!!}
                            </p>

                            {{--<h5><span>@lang("product.view.info.ShortDescription")</span></h5>--}}


                            <div class="dt-sc-hr-invisible-very-small"></div>
                            <h6>@lang("product.view.index.Share")</h6>
                            <!-- **dt-sc-social-icons - Starts** -->
                            <ul class="dt-sc-social-icons">
                                <li><a href="#" title="twitter"> <span class="fa fa-twitter"></span> </a></li>
                                <li><a href="#" title="facebook"> <span class="fa fa-facebook"></span> </a></li>
                                <li><a href="#" title="google"> <span class="fa fa-google"></span> </a></li>
                                <li><a href="#" title="linkedin"> <span class="fa fa-linkedin"></span> </a></li>
                            </ul>
                            <!-- **dt-sc-social-icons - Ends** -->
                        </div>
                        <!-- **project-details - Ends** -->
                    </div>
                </div>

                <div class="dt-sc-margin50"></div>

                <div class="dt-sc-tabs-container type2">
                    <!-- **dt-sc-tabs-frame - Starts** -->
                    <ul class="dt-sc-tabs-frame">
                        <li><a href="javascript:void(0);">@lang("product.view.info.Services")</a></li>
                        <li><a href="javascript:void(0);">@lang("product.view.info.Ability")</a></li>
                        <li><a class="gallery" href="javascript:void(0);">@lang("product.view.info.Gallery")</a></li>
                        <li><a href="javascript:void(0);">@lang("product.view.info.Comments")</a></li>
                    </ul>
                    <!-- **dt-sc-tabs-frame - Ends** -->

                    <!-- **dt-sc-tabs-frame-content - Starts** -->
                    <div class="dt-sc-tabs-frame-content persia">
                        <div class="woocommerce">
                            <table class="shop_table cart">
                                @if(isset($features) && $features)
                                    <?php $fg_id = 0; ?>
                                    @foreach($features as $item)
                                        @if(!$fg_id || $fg_id!=$item['id'])
                                            <thead>
                                            <tr>
                                                {{--<th class="product-thumbnail">@lang("product.view.info.Group")--}}
                                                {{--</th>--}}
                                                <th class="product-name" colspan="2">{!! $item['name_group'] !!}</th>
                                                <?php $fg_id = $item['id']; ?>
                                            </tr>
                                            </thead>
                                        @endif
                                        <tbody>
                                        <tr class="cart_table_item">
                                            <td class="product-name">
                                                {!!$item['name']!!}</td>
                                            <td class="product-name">{!!implode(" , ",$item['feature_values'])!!}
                                            </td>
                                        </tr>
                                        </tbody>

                                    @endforeach
                                @endif
                                <p></p>
                            </table>
                        </div>
                    </div>
                    <!-- **dt-sc-tabs-frame-content - Ends** -->

                    <!-- **dt-sc-tabs-frame-content - Starts** -->
                    <div class="dt-sc-tabs-frame-content persia">
                        {!!$dataProduct->description!!}
                    </div>
                    <!-- **dt-sc-tabs-frame-content - Ends** -->

                    <!-- **dt-sc-tabs-frame-content - Starts** -->
                    <div class="dt-sc-tabs-frame-content">

                        @if($imageProduct)
                            <?php $arrImg = [] ?>
                            <section id="portfolio">
                                <!-- **Full-width-section - Starts** -->
                                <div class="full-width-section">
                                    <div class="dt-sc-margin65"></div>
                                    <div class="container">
                                        <h2 class="aligncenter">@lang('product.view.info.Gallery') {!!$dataProduct->title!!}</h2>

                                        {{--<p class="middle-align">Contrary to popular belief, Lorem Ipsum is not simply random text.--}}
                                        {{--It has roots in a piece of classical<br/> Latin literature from 45 BC, making it over--}}
                                        {{--2000 years old. </p>--}}

                                        <div class="dt-sc-hr-invisible-small"></div>
                                        <div class="sorting-container">
                                            @foreach($imageProduct as $itemImg)
                                                <?php $arrImg[] = $itemImg->class; ?>
                                            @endforeach
                                            <?php $itemImage = array_unique($arrImg) ?>
                                            <a href="#" data-filter=".all-sort"
                                               class="active-sort">@lang('product.view.info.all')</a>
                                            @foreach($itemImage as $item)
                                                @if($item)
                                                    <a href="#"
                                                       data-filter=".{{$item}}-sort">@lang('product.view.info.'.$item)</a>
                                                @endif
                                            @endforeach

                                        </div>
                                        <div class="dt-sc-hr-invisible-small"></div>
                                    </div>
                                    <!-- **portfolio-container - Starts** -->
                                    <div class="portfolio-container no-space">

                                        @foreach($imageProduct as $itemImg)

                                            <div class="portfolio dt-sc-one-fifth no-space column all-sort {{$itemImg->class}}-sort">
                                                <!-- **portfolio-thumb - Starts** -->
                                                <div class="portfolio-thumb">
                                                    <figure>
                                                        <img class="item-image-gallery"
                                                             data-deafult="{{$itemImg->is_default?'yes':'no'}}"
                                                             data-feature-value="{{$itemImg->feature_value_selected?$itemImg->feature_value_selected:''}}"
                                                             src="{{$itemImg->image}}"
                                                             alt="image" style="height: 160px !important;"/>

                                                        <div class="image-overlay">
                                                            <a class="zoom" href="{{$itemImg->image}}"
                                                               data-gal="prettyPhoto[gallery]"><span
                                                                        class="fa fa-search"></span></a>
                                                            <a class="link" href="#"><span
                                                                        class="fa fa-link"></span></a>

                                                            <div class="portfolio-content">
                                                                <h5><a href="#"> {{$itemImg->title}} </a></h5>
                                                                <span class="fa fa-sort-up"></span>
                                                            </div>
                                                        </div>
                                                    </figure>
                                                </div>
                                                <!-- **portfolio-thumb - Ends** -->
                                            </div>

                                        @endforeach

                                    </div>
                                    <!-- **portfolio-container - Ends** -->
                                    <div class="dt-sc-margin65"></div>
                                </div>
                                <!-- **Full-width-section - Ends** -->
                            </section>
                        @endif

                    </div>
                    <!-- **dt-sc-tabs-frame-content - Ends** -->

                    <!-- **dt-sc-tabs-frame-content - Starts** -->
                    <div class="dt-sc-tabs-frame-content">
                        <!-- **respond - Starts** -->
                        <div id="create-comment">
                            <h3> @lang("product.view.info.titleComment") </h3>
                            {!!Form::open(['route' => $locale.'comment.store','class'=>'form-horizontal form-bordered', 'id'=>'commentform'])!!}
                            <input type="hidden" name="item_id" value="{!! $dataProduct->id !!}"/>
                            @if($userData = Auth::user()->get())
                                <input type="hidden" value="{!! $userData->id !!}" name="user_id"/>
                            @endif
                            <input type="hidden" value="{!! isset($system)&&$system?$system:'news' !!}" name="system"/>

                            <div class="column dt-sc-one-half first">
                                <p> <span> <input {!! !isset($userData)&&!$userData?'':'disabled' !!} type="text"
                                                  placeholder="@lang("News.addcomment.Name")"
                                                  name="user_name"
                                                  value="{{ !isset($userData)&&!$userData?'': $userData->firstname.' '.$userData->lastname }}"
                                                  required/> </span></p>
                            </div>
                            <div class="column dt-sc-one-half">
                                <p> <span> <input {!! !isset($userData)&&!$userData?'':'disabled' !!} type="email"
                                                  placeholder="@lang("News.addcomment.Email")"
                                                  name="email"
                                                  value="{{ !isset($userData)&&!$userData?'':$userData->email }}"
                                                  required/> </span></p>
                            </div>
                            <p><textarea placeholder="@lang("News.addcomment.Message")" name="text"></textarea></p>

                            <div class="dt-sc-margin10">
                                {{---Uploader----}}


                                <div class="fi-span-input full clrfx form-group file-uploader"
                                     id="form-uploader-{!!$dataProduct->id!!}"
                                     style="margin-bottom: 1px !important;">
                                    <label class="col-md-2 control-label"></label>

                                    <div class="input-group col-md-6">

                                <span class="col-xs-8">

                                    {{--{!!$item->title!!}--}}

                                    {{--<p style="color: #333333"><b>@lang("product.view.info.UploadFileComment")</b></p>--}}
                                    {!!
                                    UploaderHelper::renderUploaderJquery($idUploader='imageUploader-'.$dataProduct->id,$idStartUploader='startUploadImage'.$dataProduct->id
                                    ,$idMsgUploader='msgUploaderImage'.$dataProduct->id
                                    ,$titleUpload=trans("profile.view.DocUpload.Upload"),$titleStartUpload=trans("profile.view.DocUpload.Upload")
                                    ,trans("profile.view.index.UploadFilesTitle")
                                    ,$urlUploader=URL::route('uploadFile'),$objUploaderName='uploadObjImg'.$dataProduct->id
                                    ,$autoUpload='true',$subFolder='comments'
                                    ,$type='images-docs',$maxFileSize=20,$itemId=$dataProduct->id,$allowedTypes='png,gif,jpg,jpeg,pdf,doc,docx'
                                    ,$showDelete='true',$showDone='false',$fileName='myfile',$titleButtonDrag=trans("product.view.info.UploadFileComment"),''
                                    ,$titleSaveDesc=trans("product.view.info.titleSaveDesc")
                                    ,$controller='product',$controllerDoc='media'
                                    ,$maxCountFileUploaded=1,$multiUpload='false'
                                    )
                                    !!}
                                    <div id="msgUploaderImage{{$dataProduct->id}}"></div>
                                    <span class="input-group-addon" style="display: none">
                                        </span>
                                </span>
                                        <span style="top: 5px"></span>
                                    </div>
                                </div>


                                {{---Uploader----}}
                            </div>
                            <p>
                        <span class="loading"
                              style="background: url('{{ asset('/assets/img/loader.gif') }}') no-repeat; padding: 5px 20px 20px; display: none;float: left;
                                      height: 10px; "></span>
                                <span id="result-uploader-{!!$dataProduct->id!!}"></span>
                                <input id="btnSave-Comment" type="submit" value="@lang("News.addcomment.Submit")"
                                       name="submit"
                                       class="button"/>
                            </p>
                            {!!Form::close()!!}

                        </div>

                        <br/>
                        <!-- **respond- Ends** -->
                        <!-- **commententries** - Starts-->
                        <div class="commententries persia">
                            <div id="edit-panel" class="edit-panel" style="display: none" data-url="{{URL::route($locale.'comment.index')}}">
                                <input type="hidden" name="_method" value="PUT"/>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                <p><textarea style="height: 100px;" placeholder="@lang("News.addcomment.Message")" name="text"></textarea></p>
                                <p><span class="loading" style="background: url('{{ asset('/assets/img/loader.gif') }}') no-repeat;
                                                                        padding: 5px 20px 20px; display: none;float: left;
                                                                        height: 10px; "></span>
                                    <input id="btnClose-Edit-Comment" type="submit" value="@lang("News.addcomment.Close")"
                                           name="submit"  class="button close" style="float: right !important; margin: 2px"/>
                                    <input id="btnSave-Edit-Comment" type="submit" value="@lang("News.addcomment.Submit")"
                                           name="submit"  class="button" style="float: right !important; margin: 2px"/>
                                </p>
                            </div>

                            <h4> @lang("News.comments.title") </h4>
                            <ul class="commentlist" id="comments-list"
                                data-url="{{URL::route($locale.'comment.index')}}">

                                @if(isset($comments) && $comments)

                                    @foreach($comments as $item)
                                        <li class="item" data-id="{!!$item->id!!}">
                                            <div class="comment">
                                                <header class="comment-author author-image">
                                                    <img src="{!! $item->avatar_image !!}"
                                                         alt="{!! $item->user_name !!}"/>
                                                </header>
                                                <div class="comment-details">
                                                    <div class="author-name" data-email="{!! $item->email !!}">
                                                        <a href="#">{!! $item->user_name !!}</a>
                                                        @if(isset($user->id) && $user->id==$item->user_id)
                                                                (<a  style="font-size: 10px" href="#" data-id="{!!$item->id!!}" class="edit-comment">@lang("News.addcomment.Edit")</a>)
                                                        @endif
                                                    </div>
                                                    <div class="commentmetadata comment-date"> {!!$item->sdate!!} </div>
                                                    <div class="comment-body">
                                                        <div class="comment-content" data-id="{!!$item->id!!}">
                                                            <p>
                                                                {!! nl2br($item->text) !!}
                                                            </p>
                                                            <div class="res-edit-panel" style="display: none">

                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="reply">
                                                        <a href="#" data-id="{!!$item->id!!}"
                                                           class="dt-sc-button reply-comment">@lang("News.addcomment.Reply")</a>
                                                    </div>

                                                    @if(isset($user->id) && $user->id==$item->user_id)
                                                        <div class="del">
                                                            <a href="#" data-id="{!!$item->id!!}"
                                                               class="dt-sc-button del-comment">@lang("News.addcomment.Del")</a>
                                                        </div>
                                                    @endif
                                                    <div class="like">
                                                        <a href="#" data-id="{!!$item->id!!}"
                                                           class="{{isset($user->id)&&$user->id? '_like' : ''}}"><i
                                                                    class="fa fa-thumbs-o-up">{!! isset($item->like_value['like'])?$item->like_value['like']:0 !!}</i>
                                                        </a>
                                                        <a href="#" data-id="{!!$item->id!!}"
                                                           class="{{isset($user->id)&&$user->id? '_dislike' : ''}}"><i
                                                                    class="fa fa-thumbs-o-down">{!! isset($item->like_value['dislike'])?$item->like_value['dislike']:0 !!}</i>
                                                        </a>

                                                        <span class="loading-like"
                                                              style="background: url('{{ asset('/assets/img/loader.gif') }}') no-repeat;
                                                                      padding: 5px 20px 20px; display: none;float: left; height: 10px; "></span>

                                                    </div>
                                                </div>
                                                @if(isset($item->mediaComment))
                                                    <div class="media-comment" data-id="{!!$item->id!!}">
                                                        <ul class="media-list">
                                                            @foreach($item->mediaComment as $itemMed)
                                                                <li style="border-style: none !important;">
                                                                    <div class="comment">
                                                                        @if($itemMed->type=='image')
                                                                            <a href="{!! $itemMed->src_media !!}">
                                                                                <img src="{!! $itemMed->src_media !!}"
                                                                                     width="100px"/></a>
                                                                        @else
                                                                            <a class="btn download" target="_blank"
                                                                               href="{!! $itemMed->src_media !!}">
                                                                                @lang("News.addcomment.Download")
                                                                            </a>
                                                                        @endif
                                                                    </div>
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                @endif
                                                <ul class="reply-list" data-id="{!!$item->id!!}">

                                                    @if(isset($item->replyComments))

                                                        @foreach($item->replyComments as $itemRep)
                                                            <li>
                                                                <div class="comment">
                                                                    <header class="comment-author author-image">
                                                                        <img src="{!! $itemRep->avatar_image !!}"
                                                                             alt="{!! $itemRep->user_name !!}"/>
                                                                    </header>
                                                                    <div class="comment-details">
                                                                        <div class="author-name"
                                                                             data-email="{!! $itemRep->email !!}">
                                                                            <a href="#">{!! $itemRep->user_name !!}</a>
                                                                        </div>
                                                                        <div class="commentmetadata comment-date"> {!!$itemRep->sdate!!} </div>
                                                                        <div class="comment-body">
                                                                            <div class="comment-content" data-id="{!!$item->id!!}">
                                                                                <p>
                                                                                    {!!$itemRep->text!!}
                                                                                </p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </li>
                                                        @endforeach

                                                    @endif

                                                </ul>
                                            </div>
                                        </li>
                                    @endforeach

                                    <div id="replyComment" style="padding-top: 50px; display: none ">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">@lang("News.addcomment.Reply")</h4>
                                                </div>
                                                <div class="modal-body" style="height: 380px">
                                                    <div class="scroller" style="" data-always-visible="1"
                                                         data-rail-visible1="1">

                                                        {!!Form::open(['route' => $locale.'comment.store','class'=>'form-horizontal form-bordered', 'id'=>'commentform-reply'])!!}
                                                        <input type="hidden" name="item_id"
                                                               value="{!! $dataProduct->id !!}"/>
                                                        <input type="hidden" name="parent_id" class="parent-id"
                                                               value="0"/>
                                                        @if($userData = Auth::user()->get())
                                                            <input type="hidden" value="{!! $userData->id !!}"
                                                                   name="user_id"/>
                                                        @endif
                                                        <input type="hidden"
                                                               value="{!! isset($system)&&$system?$system:'news' !!}"
                                                               name="system"/>

                                                        <div class="column dt-sc-one-half first">
                                                            <p> <span> <input
                                                                            {!! !isset($userData)&&!$userData?'':'disabled' !!}
                                                                            type="text"
                                                                            placeholder="@lang("News.addcomment.Name")"
                                                                            name="user_name"
                                                                            value="{{ !isset($userData)&&!$userData?'': $userData->firstname.' '.$userData->lastname }}"
                                                                            required/> </span></p>
                                                        </div>
                                                        <div class="column dt-sc-one-half">
                                                            <p> <span> <input
                                                                            {!! !isset($userData)&&!$userData?'':'disabled' !!}
                                                                            type="email"
                                                                            placeholder="@lang("News.addcomment.Email")"
                                                                            name="email"
                                                                            value="{{ !isset($userData)&&!$userData?'':$userData->email }}"
                                                                            required/> </span></p>
                                                        </div>
                                                        <p><textarea style="height: 100px"
                                                                     placeholder="@lang("News.addcomment.Message")"
                                                                     name="text"></textarea></p>

                                                        <div class="dt-sc-margin10"></div>
                                                        <p>
                                        <span class="loading-reply"
                                              style="background: url('{{ asset('/assets/img/loader.gif') }}') no-repeat;
                                                      padding: 5px 20px 20px; display: none;float: left; height: 10px; "></span>
                                                            <input style="margin: 5px" id="btnSave-ReplyComment"
                                                                   type="submit"
                                                                   value="@lang("News.addcomment.SubmitReply")"
                                                                   name="submit"
                                                                   class="button"/>
                                                        </p>
                                                        {!!Form::close()!!}


                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                @endif
                            </ul>
                            @if(isset($comments) && $comments)
                                <div class="dataTables_info" id="paginate-comments" role="status" aria-live="polite"
                                     style="vertical-align: middle">
                                    <div class="pagination">
                                        {!!$comments->appends(Input::all())->render()!!}
                                    </div>
                                    @lang("News.view.index.Entities") {!!$comments->total()!!}

                                </div>
                            @endif

                        </div>
                        <!-- **commententries- Ends** -->
                        <div class="dt-sc-margin10"></div>
                    </div>
                    <!-- **dt-sc-tabs-frame-content - Ends** -->

                </div>
                <!-- **dt-sc-tabs-container - Ends** -->


            </article>
        </section>


    </div>
    <!-- **container - Ends** -->



    @endif


@endsection

@section('scr1')
    <script src="{{ asset('/assets/frontend/js/jquery.jcarousel.min.js') }}" type="text/javascript"></script>
    {{--<script src="{{ asset('/assets/frontend/js/jquery.donutchart.min.js') }}" type="text/javascript"></script>--}}
@endsection

@section('scr2')
    <script src="{{ asset('/assets/frontend/js/jquery.smartresize.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/assets/frontend/js/jquery.scrollTo.js') }}" type="text/javascript"></script>
@endsection
@section('scr-retina')
    <script src="{{ asset('/assets/frontend/js/retina.js') }}" type="text/javascript"></script>
@endsection
@section('scripts')
    <script src="{{ asset('/assets/js/front/comments.js') }}"></script>
    <script src="{{ asset('/assets/js/front/likes.js') }}"></script>
    <script src="{{ asset('/assets/js/front/product.js') }}"></script>

@endsection

@section('inline-scripts')
    <script>
        jQuery(document).ready(function () {

            jQuery('section#primary').on('click', 'a.gallery', function (event) {

                var $portfolio = jQuery('section#portfolio');
                if ($portfolio.length > 0) {
                    $portfolio.find('a.active-sort').click();
                }

            });


        });
    </script>
@endsection
