@extends('app')
@section('styles')
    <link href="{{ asset('/assets/theme/assets/global/plugins/font-awesome/css/font-awesome.min.css') }}"
          rel="stylesheet" type="text/css"/>

@endsection
@section('content')

    @if($data)
        @foreach($data as $item)
            <section class="halfscreen apartmentBox" style="background-image: url('/{!! $item->src !!}')">
                <div class="container">
                    <h1>{!! $item->title !!}</h1>
                    @if($item->countTypeApartment)
                        <a class="button"
                           href="{!!URL::route($locale.'apartment.show',['apartment_id'=>$item->id.'-'.$item->alias])!!}">@lang("apartment.view.index.MoreInformation")</a>
                    @endif


                </div>
            </section>

        @endforeach
    @endif

    <span class="loading"
          style="background: url('{{ asset('/assets/img/loader.gif') }}') no-repeat;padding: 10px 20px 20px; margin-left: 50%; display: none"></span>
    <div class="result"></div>



@endsection
@section('scripts')
    {{--    <script src="{{ asset('/assets/js/front/apartment.js') }}"></script>--}}
    <script src="{{ asset('/assets/js/jquery.fancybox.pack.js') }}"></script>

@endsection
@section('inline-scripts')
    <script>
        $(document).ready(function () {
            /* !FancyBox Initialize */
            $(".atgImageView").fancybox({
                'titlePosition': 'inside',
                'transitionIn': 'fade',
                'transitionOut': 'none',
                'centerOnScroll': 'true',
                'overlayColor': '#000'
            });
        });
    </script>
@endsection
