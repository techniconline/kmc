<!Doctype html>


<html dir="rtl" lang="<?php $langDetails = app('activeLangDetails'); ?>">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0"/>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>

    <title>KMC - @yield('browser_title')</title>

    <meta name="description" content="">
    <meta name="author" content="">

    <link href="images/favicon.png" rel="shortcut icon" type="image/png">
    <link href="images/apple-touch-icon.png" rel="apple-touch-icon">
    <link href="images/apple-touch-icon-72x72.png" rel="apple-touch-icon" sizes="72x72">
    <link href="images/apple-touch-icon-114x114.png" rel="apple-touch-icon" sizes="114x114">
    <link href="images/apple-touch-icon-144x144.png" rel="apple-touch-icon" sizes="144x144">


    {{--    <script src="{{ asset('/assets/frontend/js/jquery-1.10.2.min.js') }}" type="text/javascript"></script>--}}
    {{--<script src="{{ asset('/assets/frontend/js/jquery.min.js') }}" type="text/javascript"></script>--}}



    @if($langDetails['is_rtl'])


{{--        <link href="{{ asset('/assets/frontend/css/font.min.css') }}" rel="stylesheet" type="text/css">--}}
        <link href="{{ asset('/assets/frontend/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('/assets/frontend/css/jquery-ui.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('/assets/frontend/css/animate.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('/assets/frontend/css/css-plugin-collections.css') }}" rel="stylesheet"/>

        <link id="menuzord-menu-skins" href="{{ asset('/assets/frontend/css/menuzord-skins/menuzord-rounded-boxed.css') }}" rel="stylesheet"/>
        <link href="{{ asset('/assets/frontend/css/style-main-rtl.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('/assets/frontend/css/colors/theme-skin-sky-blue.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('/assets/frontend/css/preloader.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('/assets/frontend/css/custom-bootstrap-margin-padding.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('/assets/frontend/css/responsive.css') }}" rel="stylesheet" type="text/css">

        @else

        @endsection


    @endif


    @yield('styles')

    <script src="{{ asset('/assets/frontend/js/jquery-2.1.4.min.js') }}"></script>

    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <script src="{{ asset('/assets/frontend/js/respond/1.4.2/respond.min.js')}}"></script>
    <![endif]-->
</head>

<body class="">



<div id="wrapper">
    <div id="preloader">
        <div id="spinner">
            <img src="{{ asset('/assets/frontend/images/preloaders/1.gif') }}" alt="">
        </div>
        <div id="disable-preloader" class="btn btn-default btn-sm">Disable Preloader</div>
    </div>

    @if (Session::has('messages'))
        @foreach(Session::get('messages') as $itemMsg)
            <div class="alert alert-{{ $itemMsg['level']}}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ $itemMsg['message'] }}
            </div>
            @endforeach

            @endif


                @include('partials.header')


                        <!-- **Main - Starts** -->
                <div class="main-content">

                    @yield('content')

                </div>
                <!-- **Main - Ends** -->

                @include('partials.footer')

    <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
</div>


<!-- **jQuery** -->
<script src="{{ asset('/assets/frontend/js/jquery-ui.min.js') }}"></script>
<script src="{{ asset('/assets/frontend/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/assets/frontend/js/jquery-plugin-collection.js') }}"></script>

@yield('slider-js')
@yield('scr-map')

{{--<script src="{{ asset('/assets/frontend/js/persianumber.min.js') }}" type="text/javascript"></script>--}}
<script src="{{ asset('/assets/frontend/js/calendar-events-data.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/frontend/js/custom.js') }}" type="text/javascript"></script>


@yield('scripts')

@yield('inline-scripts')
</body>
</html>
