@extends('app')

@section('content')
    <section class="fullscreen" id="guidesSection">
        <div class="container">
            <div class="guideBox" id="step1">
                <img src="{{ asset('/assets/frontend/images_page/Guide-Icons-01.svg')}}">

                <p>@lang("guide.view.index.Step") 1</p>

                <h2>@lang("guide.view.index.compte")</h2>
            </div><!--
		-->
            <div class="guideBox" id="step2">
                <img src="{{ asset('/assets/frontend/images_page/Guide-Icons-02.svg')}}">

                <p>@lang("guide.view.index.Step") 2</p>

                <h2>@lang("guide.view.index.appartement")</h2>
            </div><!--
		-->
            <div class="guideBox" id="step3">
                <img src="{{ asset('/assets/frontend/images_page/Guide-Icons-03.svg')}}">

                <p>@lang("guide.view.index.Step") 3</p>

                <h2>@lang("guide.view.index.dossier")</h2>
            </div><!--
		-->
            <div class="guideBox" id="step4">
                <img src="{{ asset('/assets/frontend/images_page/Guide-Icons-04.svg')}}">

                <p>@lang("guide.view.index.Step") 4</p>

                <h2>@lang("guide.view.index.candidature")</h2>
            </div><!--
		-->
            <div class="guideBox last" id="step5">
                <img src="{{ asset('/assets/frontend/images_page/Guide-Icons-05.svg')}}">

                <p>@lang("guide.view.index.Step") 5</p>

                <h2>@lang("guide.view.index.contrat")</h2>
            </div>

            <article class="fill info" id="step1-info">
                <div class="post atgLTR"><a name="Step 1"></a>

                    <h1>@lang("guide.view.index.Step") 1</h1>
                    {!! ParamsHelper::getStaticContent('TextStep1')['body_content'] !!}
                </div>
                <br class="clearfloat">
            </article>

            <article class="fill info" id="step2-info">
                <div class="post atgLTR"><a name="Step 2"></a>

                    <h1>@lang("guide.view.index.Step") 2</h1>
                    {!! ParamsHelper::getStaticContent('TextStep2')['body_content'] !!}
                </div>
                <br class="clearfloat">
            </article>
            <article class="fill info" id="step3-info">
                <div class="post atgLTR"><a name="Step 3"></a>

                    <h1>@lang("guide.view.index.Step") 3</h1>
                    {!! ParamsHelper::getStaticContent('TextStep3')['body_content'] !!}
                </div>
                <br class="clearfloat">
            </article>
            <article class="fill info" id="step4-info">
                <div class="post atgLTR"><a name="Step 4"></a>

                    <h1>@lang("guide.view.index.Step") 4</h1>
                    {!! ParamsHelper::getStaticContent('TextStep4')['body_content'] !!}
                </div>
                <br class="clearfloat">
            </article>
            <article class="fill info" id="step5-info">
                <div class="post atgLTR"><a name="Step 5"></a>

                    <h1>@lang("guide.view.index.Step") 5</h1>
                    {!! ParamsHelper::getStaticContent('TextStep5')['body_content'] !!}
                </div>
                <br class="clearfloat">
            </article>
            <div class="arrowDown scrollToAnchor"></div>
        </div>
    </section>


    <section class="fullscreen" id="guidesFAQ">
        <div class="container fill" id="mainAnchor">
            @foreach($faq_type as $key => $value)
                <div class="faqBox" id="{!!$key!!}">
                    <h2>{!!$value!!}</h2>
                </div>
            @endforeach
            @foreach($faq_type as $key => $value)
                <article id="{!!$key!!}-info" class="fill info">
                    <div class="post atgLTR"><a name="FAQ General"></a>
                        {{--<h1>RÉSERVATION ET CONTRAT</h1>--}}

                        @if(count($faqs[$key])>0)
                            @foreach($faqs[$key] as $faq)
                                <p>
                                <h2>» {!! $faq['question'] !!}</h2>
                                <p>
                                <div class='block'>
                                    {!! $faq['answer'] !!}
                                </div>

                                </p>
                            @endforeach
                        @endif
                    </div>
                </article>
            @endforeach
        </div>
    </section>

@endsection
