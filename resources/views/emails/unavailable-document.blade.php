<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>>@lang("emails.view.unavailable-document.HiDear") {!!$username!!}</title>
</head>
<body>
<div>@lang("emails.view.unavailable-document.HiDear"), {!!$username!!}</div>
<p></p>

<div style="color: #ff0000">{!!$subject!!}</div>
<p></p>

<div>{!!$text!!}</div>
</body>
</html>