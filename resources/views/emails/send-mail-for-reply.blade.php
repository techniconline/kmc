<!doctype html>
<html lang="fa">
<head>
    <meta charset="UTF-8">
    <title>@lang("comments.messages..HiDear") {!!$username!!}</title>
</head>
<body style="direction: rtl">
<div>@lang("comments.messages..HiDear"), {!!$username!!}</div>
<p></p>

<div style="color: green">{!!$subject!!}</div>
<p></p>

<div>{!!$text!!}</div>
</body>
</html>