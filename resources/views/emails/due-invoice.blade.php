<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>@lang("emails.view.unavailable-document.HiDear")</title>
</head>
<body>
<div>@lang("emails.view.unavailable-document.HiDear")</div>
<p></p>

<div style="color: green">{!!$text!!}</div>
<p></p>

<div>{!!$text2!!}</div>
</body>
</html>