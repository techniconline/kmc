{!! trans("backend/Email.view.password.Your_New_Password_Is") !!} : {!! $newPass !!}
{!! trans("backend/Email.view.password.You_Can_Click_This_Link_To_Login_Again") !!} : {!! link_to('http://www.studenest.com/front/profile', 'Studenest') !!}
