<!DOCTYPE html>
<html>
<head lang="fr">
    <title></title>
</head>
<body>


<table style="width: 750px;" align="center">
    <tr>
        <td>

            <table class="header" style="width: 100%">
                <tr>
                    <td>
                        30, Rue Peclet <br>75015 Paris<br>
                        Tel : 01 77 19 61 14<br>
                        Email : info@studenest.com
                    </td>
                    <td style="text-align:right"><img
                                src="{!!base_path().env('PUBLIC_PATH_URL', 'public')!!}/assets/img/MainLogo-Black-studenest.png">
                    </td>
                </tr>
            </table>

        </td>
    </tr>

    <tr>
        <td style="text-align:center">

            <div>
                <h1 style="background-color:#AAAAAA">CONTRAT DE CAUTIONNEMENT SOLIDAIRE <br>ANNÉE 2015 / 2016 </h1> <br>

                <h3>À remplir et faire signer par les parents et à joindre à votre dossier<br>
                    Document à joindre à toute demande d’admission ou de réadmission<br>&#32;<br><br>
                </h3>
            </div>

        </td>
    </tr>

    <tr>
        <td>
            <p>
                <br>Je soussigné(e) :
                .........................................................................................................................(Régime
                matrimonial)<br>
                Né(e) le ........................................................................ à
                ................................................................................... <br>
                Demeurant
                ....................................................................................................................................(Adresse
                complète)<br>&#32;<br>
                Déclare me porter caution solidaire aux conditions et termes du présent acte de : <br>&#32;<br>
                M
                ................................................................................................................................................................................
                <br>
            </p>

        </td>
    </tr>

    <tr>
        <td>

            <p>
                Avec renonciation aux bénéfices de discussion et de division pour les obligations résultant du bail qui
                lui a été consenti par Studenest C /O Sodevim 30, rue Peclet 75015 Paris, pour une durée de 9 mois, à
                compter du 1er Septembre 2015, portant sur la location d’un appartement dans la Résidence Galilée sis 4
                bis, rue Alfred Nobel 77420 Champs-sur-Marne.<br>&#32;<br>&#32;<br>
                Le montant du loyer mensuel s’élève à la somme de <b>{!!$ROOM_PRICE!!} € TTC</b>, ledit loyer étant
                révisé tous les ans selon la variation du dernier indice de référence des loyers (IRL) publié par
                l’INSEE au jour de la signature du contrat de bail.<br>&#32;<br>
                Je reconnais avoir pris connaissance des différentes clauses et condition de ce bail dont un exemplaire
                m’a été remis, et m’engage à garantir le paiement des loyers, indemnités d’occupation, charges,
                réparations locatives, impôts et taxes, frais d’état des lieux et tous frais éventuels de procédure dus
                en vertu de ce bail, le montant maximum de mon engagement étant limité à
                <b>{!!($ROOM_PRICE*9)!!} {!!$ROOM_PRICE_LETTER_9!!} Euros.</b><br>&#32;<br>
                Cet engagement est donné pour la durée du bail, lequel n’est pas reconductible.<br>&#32;

            </p>

        </td>
    </tr>

    <tr>
        <td>

            <p>

                La caution confirme sa connaissance de la nature et de l’étendue de ses obligations en recopiant de sa
                main la mention ci-après : <br>&#32;<br>&#32;<br>
                « Je me porte caution solidaire sans bénéfice de discussion et de division à compter de la date du 1er
                Septembre 2015 pour une durée de 9 mois, pour un montant
                <b>{!!($ROOM_PRICE*9)!!} {!!$ROOM_PRICE_LETTER_9!!} Euros</b>, pour le paiement du loyer s’élevant à ce
                jour à <b>{!!($ROOM_PRICE)!!} € TTC {!!$ROOM_PRICE_LETTER!!} Euros</b>, et de sa révision chaque année
                sur la base de l’IRL du dernier indice publié au jour de la signature du contrat de bail, ainsi que les
                indemnités d’occupation, charges récupérables, réparations locatives, impôts et taxes, frais d’état des
                lieux et frais éventuels de procédure, ces obligations résultant du bail dont j’ai reçu un exemplaire.
                Je confirme avoir une parfaite connaissance de la nature et de l’étendue de mon engagement. Lorsque le
                cautionnement d’obligations résultant d’un contrat de location conclu en application du présent titre ne
                comporte aucune indication de durée ou lorsque la durée du cautionnement est stipulée indéterminée, la
                caution peut le résilier unilatéralement. La résiliation prend effet au terme du contrat de location,
                qu’il s’agisse du contrat initial ou d’un contrat reconduit ou renouvelé, au cours duquel le bailleur
                reçoit notification de la résiliation. »<br>&#32;<br>

            </p>

        </td>
    </tr>

    <tr>
        <td>

            <p>Signature de la caution:</p>

            <p>
                En application des dispositions des articles L341.2 et L341-3 du Code de la consommation, la caution
                doit reproduire de sa main le texte suivant : </b><br>
                « En me portant caution de <b> {!!$GENDER!!} {!!$LASTNAME.' '.$FIRSTNAME!!} </b> dans la limite de la
                somme de <b> {!!($ROOM_PRICE*9)!!} {!!$ROOM_PRICE_LETTER_9!!} Euros </b> couvrant le paiement du
                principal, des intérêts et, le cas échéant, des pénalités ou intérêts de retard et pour la durée de 9
                mois, je m'engage à rembourser au prêteur les sommes dues sur mes revenus et mes biens si
                <b> {!!$GENDER!!} {!!$LASTNAME.' '.$FIRSTNAME!!} </b> n'y satisfait pas lui-même.<br>
                En renonçant au bénéfice de discussion défini à l'article 2298 du code civil et en m'obligeant
                solidairement avec <b> {!!$GENDER!!} {!!$LASTNAME.' '.$FIRSTNAME!!} </b>, je m'engage à rembourser le
                créancier sans pouvoir exiger qu'il poursuive préalablement
                <b> {!!$GENDER!!} {!!$LASTNAME.' '.$FIRSTNAME!!} </b>».<br>&#32;<br>
            <table class="cadre">
                <tr>
                    <td>&#32;</td>
                </tr>
            </table>
            <p>Fait à .......................................... Le ..........................................</p>

            <p>En 2 exemplaires,</p><br>&#32;<br>

            <b>
                <ins>POUR LA CAUTION</ins>
            </b><br>&#32;<br><br>&#32;<br>
            <b>
                <ins>POUR LE BAILLEUR :</ins>
                <br><br>&#32;<br><br>&#32;<br>
                </p>


        </td>

    <tr>
        <td>


        </td>
    </tr>

    <tr>
        <td>
            Copyright © studenest.com
        </td>
    </tr>

</table>

</body>
</html>