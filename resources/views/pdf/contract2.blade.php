<!DOCTYPE html>
<html>
<head lang="fr">
    <title></title>
</head>
<body>


<table style="width: 750px;" align="center">
    <tr>
        <td>

            <table class="header" style="width: 100%">
                <tr>
                    <td>
                        30, Rue Peclet <br>75015 Paris<br>
                        Tel : 01 77 19 61 14<br>
                        Email : info@studenest.com
                    </td>
                    <td style="text-align:right"><img
                                src="{!!base_path().env('PUBLIC_PATH_URL', 'public')!!}/assets/img/MainLogo-Black-studenest.png">
                    </td>
                </tr>
            </table>

        </td>
    </tr>

    <tr>
        <td style="text-align:center">

            <div>

                <h4 style="background-color:#AAAAAA">CONTRAT DE RÉSIDENCE À DURÉE DÉTERMINÉE <br>
                    DE LOCAUX D'HABITATION MEUBLÉE (NON RECONDUCTIBLE)<br>
                    CONDITIONS PARTICULIÈRES</h4>
                <br>

            </div>

        </td>
    </tr>
    <tr>
        <td>
            <h4>Entre les soussignés <br>&#32;<br>
                STUDENEST<br>Immatriculée au RCS de PARIS sous le n° 398 032 078 (SODEVIM) dont le siège social est sis
                30 rue Péclet 75015 Paris, représentée par son Président en exercice,
                <br>&#32;<br><br>
                Dénommée « le Bailleur »
                <br></h4>
        </td>
    </tr>

    <tr>
        <td>
            <b>
                <div>{!!$GENDER!!}: {!!$FIRSTNAME.' '.$LASTNAME!!}</div>
                <div>Né(e)le : {!!$BIRTH_DATE!!} à {!!$CITY!!}</div>
                <div>Résidant : {!!$ADDRESS!!}</div>
                <div>{!!$POSTAL_CODE!!} {!!$CITY!!}</div>
            </b>
        </td>
    </tr>

    <tr>
        <td>
            <p>
                Dénommé(s) « le Preneur »<br>&#32;<br>
                <b>Il est rappelé ce qui suit :<br></b>
                STUDENEST a pris à bail commercial des logements dépendant de la résidence avec services dénommée :<br>&#32;<br>
                Résidence Galilée<br>
                4 BIS RUE ALFRED NOBEL A CHAMPS-SUR-MARNE (77420)<br>&#32;<br>
                A ce titre, STUDENEST exerce dans cette résidence une activité d'exploitation à caractère para-hôtelier
                consistant en la mise à disposition de logements meublés pour des périodes de temps déterminées, avec la
                fourniture de différents services dont les modalités de tarification et les conditions de réservation
                sont affichées dans la résidence.<br>&#32;<br>
                <ins>Informations du Preneur sur les risques naturels et technologiques</ins>
                <br>
                Aux termes des dispositions de l'article L 125-5 du Code de l'Environnement, le Preneur des biens
                immobiliers situés dans les zones couvertes par un plan de prévention des risques technologiques ou par
                un plan de prévention des risques naturels prévisibles, prescrit ou approuvé, ou dans des zones de
                sismicité définies par décret en Conseil d'Etat, est informé par le Bailleur de l'existence des risques
                visés par ce plan ou ce décret.<br>&#32;<br>
                L’ensemble immobilier dont dépend le bien sous-loué objet des présentes n’est pas situé dans le
                périmètre de plans de prévention des risques naturels<br>&#32;<br>
            </p>
        </td>
    </tr>

    <tr>
        <td>
            <b>Article 1</b><br>
            Le présent contrat est régi par les dispositions des articles 1714 à 1762 du code civil, les articles
            L.632-1 et suivants du code de la construction et de l’habitation, le Titre 1er bis et l’article 40 VIII de
            la loi n°89-462 du 6 juillet 1989.<br>
        </td>
    </tr>

    <tr>
        <td>
            <br><b>Article 2</b><br>
            Le Bailleur met à disposition du Preneur, l'appartement meublé <b>{!!$ROOM_NUMBER!!} </b>d’une surface
            habitable de <b> {!!($ROOM_AREA)!!} </b>m²sis Résidence Galilée 4 bis rue Alfred Nobel à CHAMPS-SUR-MARNE
            (77420) comprenant coin cuisine équipée, WC, salle d’eau et mobilier selon descriptif visé dans les lieux et
            un emplacement de parking au sous-sol.<br>
        </td>
    </tr>

    <tr>
        <td>
            <br><b>Article 3</b><br>
            Le présent contrat est consenti et accepté pour une durée de 9 mois soit pour la période du 01/09/2015 au
            01/08/2016 A l'expiration de ce délai, il prendra fin sans possibilité de tacite reconduction.<br>
        </td>
    </tr>

    <tr>
        <td>
            <b>Article 4</b><br>
            Le montant mensuel du loyer, objet du présent contrat, est fixé à :<br>&#32;<br>
            <b>Redevance hors taxe: {!!($ROOM_PRICE - ($ROOM_PRICE*0.2))!!} €<br>
                TVA 20 % :{!!($ROOM_PRICE*0.2)!!} €<br>
                Soit un total mensuel de :{!!($ROOM_PRICE)!!} €<br>
                Parkig :{!!($PARKING_PRICE)!!} €<br>
                Soit un total général de : {!!(($ROOM_PRICE + $PARKING_PRICE))!!} €<br></b><br>
            Le loyer est payable par mois d'avance le premier jour du mois.<br>&#32;<br>
            Le prix du loyer sera révisé de plein droit dans les conditions fixées à l'article 1 des conditions
            générales ou conformément au réajustement prévu à l'article 6 des conditions générales.<br>
            <br>
        </td>
    </tr>

    <tr>
        <td>
            <br><b>Article 5</b><br>
            Le dépôt de garantie faisant l'objet de l'article 9 des conditions générales du contrat est fixé à
            <b>{!!($ROOM_PRICE)!!} € TTC </b>(un mois de loyer)<br>
            <br><b>Article 6</b><br>
            <ins>Consistance du logement :</ins>
            <br>
            - localisation du logement : 4 BIS RUE ALFRED NOBEL A CHAMPS-SUR-MARNE (77420)<br>
            - type d'habitat : immeuble collectif<br>
            - régime juridique de l'immeuble : mono propriété<br>
            - période de construction : 2015<br>
            - surface habitable : {!!($ROOM_AREA)!!}m²<br>
            - nombre de pièces principales : 1 pièce<br>
            - le cas échéant, autres parties du logement : balcon,terrasse<br>
            - le cas échéant, Eléments d'équipements du logement : cuisine équipée, Salle de bain<br>
            - modalité de production chauffage : Collectif<br>
            - modalité de production d'eau chaude sanitaire : Collective<br>&#32;<br>
            <ins>Locaux, parties, équipements et accessoires de l'immeuble à usage commun</ins>
            : <br>Garage à vélo, ascenseur, espaces verts, laverie, local poubelle, accueil, autres prestations et
            services collectifs etc.<br>&#32;<br>
            <ins>Equipement d'accès aux technologies de l'information et de la communication</ins>
            :<br> modalités de raccordement internet en wifi.<br>

        </td>
    </tr>

    <tr>
        <td>
            <b>Article 7</b><br>
            Vos données personnelles sont enregistrées dans notre fichier de clients et peuvent donner lieu à l'exercice
            du droit d'accès et de rectification. Conformément à la loi a informatique et libertés » du 6 janvier 1978
            modifiée en 2004, vous bénéficiez d'un droit d'accès et de rectification aux informations qui vous
            concernent, ou vous opposer au traitement des données vous concernant pour des motifs légitimes que vous
            pouvez exercer en nous adressant un courrier à cet effet.
            <p>Fait à .......................................... Le ..........................................</p><br>
            Le(s)Preneur(s) .......................................
            Le(s)Garant(s)....................................... Le Bailleur
            .......................................<br>&#32;<br><br>&#32;<br>

            En cas de pluralité de Preneurs et/ou de garants: « agissant solidairement et indissociablement <br> « lu et
            approuvé -bon pour accord »

        </td>
    </tr>

    <tr>
        <td style="text-align:center">
            <h4 style="background-color:#AAAAAA">CONTRAT DE RÉSIDENCE À DURÉE DÉTERMINÉE <br>
                DE LOCAUX D'HABITATION MEUBLÉE <br> CONDITIONS GÉNÉRALES<br>
            </h4>
        </td>
    </tr>

    <tr>
        <td>
            <b>ARTICLE 1 - LOYER</b><br>
            Le Preneur s'engage à acquitter mensuellement d'avance le premier de chaque mois, les sommes dues au titre
            du loyer. Le loyer est portable au domicile du Bailleur. Il est convenu que le loyer sera révisée de plein
            droit à la hausse, et sans qu'il soit besoin d'une notification préalable, le 1er janvier de chaque année
            proportionnellement à la variation du dernier indice de référence des foyers (IRL) publié par l'INSEE au
            jour de la signature du contrat.<br>&#32;<br>

        </td>
    </tr>

    <tr>
        <td>

            <b>ARTICLE 2 - DESTINATION DES LIEUX</b><br>
            Les lieux seront affectés à l'usage exclusif d'habitation. Toute affectation à usage professionnel, libéral
            ou commercial est interdite.<br>&#32;<br>

        </td>
    </tr>

    <tr>
        <td>

            <b>ARTICLE 3 - OCCUPATION</b><br>
            Le Preneur s'oblige à fixer son habitation principale dans le logement mis à disposition ainsi qu'à habiter
            les lieux personnellement et à en user paisiblement en bon père de famille. Il s'interdit de les faire
            occuper, même temporairement, par d'autres personnes, ou de céder à titre onéreux ou gratuit les droits
            qu'il tient du présent engagement.<br>
            Le Preneur respectera selon la nature de la location toutes les règles découlant de l'application des
            prescriptions du code civil, du règlement intérieur, et du règlement sanitaire départemental.<br>
            Le Preneur ne devra nuire ni au repos et à la tranquillité des autres résidents ni à la bonne tenue de la
            résidence.<br>
            En cas de sinistre dans les lieux mis à disposition, le Preneur en informera immédiatement le Bailleur même
            en l'absence de dégâts apparents.<br>
            Le Preneur devra permettre la visite des lieux loues par le Bailleur, rendez-vous préalablement pris, sauf
            cas d'urgence et, notamment pour la visite annuelle au titre du contrat de maintenance du mobilier et du
            suivi technique du logement.<br>
            Le Preneur informera le Bailleur des dispositions prises pour permettre l'accès aux lieux en son absence et
            en cas de nécessité ; à défaut le Bailleur pourra faire ouvrir les lieux aux frais du Preneur.<br>&#32;<br>

        </td>
    </tr>

    <tr>
        <td>

            <b>ARTICLE 4 - ENTREE EN JOUISSANCE - CONSERVATION DE LA CHOSE LOUEE -RESTITUTION</b><br>
            Le Bailleur s'oblige à fournir le logement en bon état d'habitabilité et à effectuer les grosses réparations
            et le gros entretien ; toutefois, le Preneur supportera la charge de ces travaux s'ils ont été rendus
            nécessaires par son fait, sa négligence, son imprudence ou du fait de tiers dont il a permis l’entrée dans
            le logement et/ou ses parties communes. Un état des lieux sera dressé contradictoirement à l’entrée dans le
            logement. A défaut, la partie la plus diligente pourra le faire établir par huissier, à frais partagés. A
            défaut d’état des lieux, ou de remise d’un exemplaire à l’autre partie, la présomption posée à l’article
            1731 du code civil ne pourra être invoquée par celle des parties qui a fait obstacle à l’établissement de
            l’acte ou à sa remise à l’une des parties. Le Preneur dispose d’un délai de 10 jours à compter de
            l’établissement de l’état des lieux pour demander au Bailleur de le compléter.<br>&#32;<br>
        </td>
    </tr>

    <tr>
        <td>
            Le Preneur s'oblige à effectuer les menues réparations y compris les remplacements d'éléments assimilables
            aux dites réparations et l'entretien courant du logement. A la fin du contrat, il devra restituer les lieux
            conformes à leur composition initiale et dans un état d'entretien et de propreté correspondant à un usage
            normal.<br>&#32;<br>
            Le Preneur répond de toutes les dégradations survenues pondant sa jouissance, à l'exclusion de celles
            résultant de la vétusté, de malfaçons, de vice de construction ou de la force majeure,<br>&#32;<br>
            Aucune transformation des locaux et équipements mis à disposition ne pourra être effectuée dans les
            lieux.<br>&#32;<br>
            Le Preneur ne pourra prétendre à aucune indemnité pour les travaux d'embellissement et de décor quelconque
            ou pour los installations, appareils qu'il laissera dans les lieux.<br>&#32;<br>
            Le Preneur souffrira sans indemnité l’exécution des travaux ou réparations affectant les parties communes ou
            privatives, et les travaux nécessaires au maintien en état et à l'entretien normal des lieux loués, que le
            Bailleur déciderait de faire exécuter, dès que lors que la durée de ceux-ci sera inférieure à 21 jours.<br>&#32;<br>
            S’il choisit un fournisseur spécifique, le Preneur supportera les frais d'abonnement, branchement et
            remplacement de compteur ou d'installations intérieures pouvant être exiges par les compagnies
            distributrices de l'électricité ou chargées du chauffage. Il prendra à sa charge les consommations
            individuelles des parties privatives ainsi que les taxes locatives.<br>&#32;<br>
            Le Preneur ne pourra rien faire ni laisser faire qui puisse détériorer les biens loués.<br>&#32;<br>
            Le Preneur devra informer immédiatement te Bailleur sans aucun retard de toute atteinte qui serait portée à
            la propriété, en cas de travaux, dégradations et détériorations qui viendraient à se produire dans les biens
            loués et qui rendraient nécessaire l'intervention du Bailleur. A défaut de quoi, il répondra des
            dégradations et pertes survenues pendant la durée du contrat hormis celles survenues du fait de la force
            majeure ou du Bailleur.<br>&#32;<br>
            Le Preneur devra, au minimum 15 jours avant son départ, en faire connaître la date exacte et demander un
            rendez-vous pour l’établissement de l’état des lieux de sortie. Au cas où le Preneur n’assisterait pas au
            rendez-vous fixé, il pourra être procédé à l’état des lieux par voie d’huissier.<br>&#32;<br>


        </td>
    </tr>

    <tr>
        <td>

            <b>ARTICLE 5 — PRESTATIONS— IMPOTS — TAXES</b><br>
            Le Preneur paiera ou remboursera au Bailleur les prestations, impôts, taxes, fournitures individuelles et
            services para-hôteliers indépendamment des impôts et taxes qui pourront être réclamés directement par
            l'administration fiscale au Preneur. Il devra également justifier du paiement régulier de ses contributions
            mobilières et personnelles à toute réquisition de Bailleur.<br>&#32;<br>

        </td>
    </tr>

    <tr>
        <td>

            <b>ARTICLE 6 — RESILIATION —CONGE</b><br>
            Le présent contrat pourra être résilié par le Preneur par lettre recommandée avec accusé de réception ou par
            acte d'huissier moyennant le respect d’un préavis d'un mois.<br>&#32;<br>
            Le délai de congé commence à compter du jour de la réception de la lettre recommandée ou de la notification
            de l'acte extrajudiciaire. Dans le cas où le Preneur quitterait les lieux sans en avertir Bailleur, les
            frais de constat d'huissier ainsi que les travaux de remise en état consécutifs et éventuels seraient mis
            obligatoirement à la charge du Preneur, sans préjudice des indemnités d'occupation, des charges et des
            dommages et intérêts.<br>&#32;<br>
        </td>
    </tr>

    <tr>
        <td>
            <b>ARTICLE 7 —ASSURANCE</b><br>
            Le Preneur s'engage à justifier de la souscription d'une assurance multirisque habitation au Bailleur lors
            de la signature du contrat de résidence et pendant toute la durée de celui-ci.<br>&#32;<br>
        </td>
    </tr>

    <tr>
        <td>
            <b>ARTICLE 8 — RETARD DE PAIEMENT : CLAUSE RESOLUTOIRE ET RESILIATION JUDICIAIRE</b><br>
            8.1. En cas de non-paiement à leur échéance des sommes dues par le Preneur au Bailleur, une majoration de
            cinq (5) pour cent des sommes dues sera automatiquement appliquée â l'expiration d'un délai de quinze jours
            à compter de la date d’échéance de la créance impayée indépendamment de tous autres dommages-intérêts et
            intérêts de retard ainsi que du jeu de la clause résolutoire.<br>&#32;<br>
            8.2. Par dérogation au dernier alinéa de l'article 1184 du Code Civil, à défaut de paiement d'un seul terme
            de loyer ou du montant des charges et accessoires à son échéance, ainsi que du montant du dépôt de garantie,
            ou à défaut de souscription d'une assurance couvrant les risques locatifs, et un mois après la délivrance
            d'un commandement resté infructueux, le bail sera résilié de plein droit, si bon semble au Bailleur, et sans
            aucune formalité judiciaire. Aucune offre ou consignation ultérieure ne pourra arrêter les effets de la
            présente clause.<br>&#32;<br>
            Si le Preneur refusait de quitter les lieux immédiatement et sans délai, ii suffirait pour l'y contraindre,
            d'une ordonnance de référé rendue par M. le Président du Tribunal du lieu de la situation de l’immeuble.<br>&#32;<br>
            8.3 En cas d'inexécution par le Preneur de ses obligations, et indépendamment de la mise en œuvre éventuelle
            de la clause résolutoire ci-dessus, le Bailleur, huit jours après sommation d'exécuter, saisira le Tribunal
            du lieu de situation de l’immeuble, en vue de voir résilier le bail, sans préjudice de tous
            dommages-intérêts pouvant lui être dus.<br>&#32;<br>

        </td>
    </tr>

    <tr>
        <td>

            <b>ARTICLE 9 — DEPOT DE GARANTIE</b><br>
            Pour garantir l'exécution de ses obligations locatives, le Preneur verse le jour de la signature du présent
            contrat, à titre de dépôt de garantie, une somme dont le montant est spécifié dans les conditions
            particulières du présent contrat.<br>&#32;<br>
            Il ne sera l'objet d'aucune révision durant l'exécution du contrat et n'est pas productif d'intérêts ; il
            n'est pas imputable sur les derniers termes de loyer.<br>&#32;<br>
            Le dépôt de garantie est restitué dans un délai de maximal de 2 mois à compter de la restitution des clés
            par le Preneur, ou dans le délai d’un mois si l’état des lieux de sortie est conforme à l’état des lieux
            d’entrée, déduction faite des sommes restant dues au Bailleur et des sommes dont celui-ci pourrait être
            tenu, aux lieu et place du Preneur, sous réserve qu'elles soient dûment justifiées.<br>&#32;<br>

        </td>
    </tr>

    <tr>
        <td>

            <b>ARTICLE 10— ELECTION DE DOMICILE</b><br>
            Le Preneur fait élection de domicile dans les locaux mis à disposition pendant la durée de l'occupation et à
            l'adresse qu'il aura communiqué après son départ, à défaut à la dernière adresse connue. Le Bailleur élit
            domicile à de son siège social où toutes les significations devront lui être faites.<br>&#32;<br>

        </td>
    </tr>

    <tr>
        <td>

            <b>ARTICLE 11 — PIECES ANNEXES</b><br>
            Au présent contrat de résidence sont annexes les documents suivants :<br> - un exemplaire de l'état des
            lieux visé par le Preneur<br>
            - une liste des équipements et du mobilier<br>
            - la liste des réparations et travaux locatifs<br>
            - l'état des risques naturels et technologiques <br>
            - le diagnostic de performance énergétique<br>
            - une notice d’information relative aux droits et obligations du bailleur et du locataire.<br>

        </td>
    </tr>

    <tr>
        <td>
            <p>

            <p>Fait à .......................................... Le ..........................................</p><br>
            Le(s)Preneur(s) .......................................
            Le(s)Garant(s)....................................... Le Bailleur
            .......................................<br>&#32;<br><br>&#32;<br>
            En cas de pluralité de Preneurs et/ou de garants: « agissant solidairement et indissociablement <br> « lu et
            approuvé -bon pour accord »

            </p>

        </td>
    </tr>

    <tr>
        <td>


        </td>
    </tr>

    <tr>
        <td>
            Copyright © studenest.com
        </td>
    </tr>

</table>

</body>
</html>