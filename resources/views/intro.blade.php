<!DOCTYPE HTML>
<html>
<head>
    <!-- -->
    <title>Studenest - @yield('browser_title') </title>
    <meta name="keywords" content="Studenest,Campus,Rental,Dorm,Room,France,Paris,Résidence,étudiants">
    <meta name="description" content="Résidence pour étudiants">


    <meta name="author" content="Made with passion for pretty things.">
    <meta name="language" content="fr_FR">
    <meta name="documentcountrycode" content="fr">
    <meta name="generator" content="ProCEED 2.6.1">
    {!! ParamsHelper::metaTag() !!}
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="initial-scale=1">
    {!! ParamsHelper::favIcon() !!}
    {{--    <link href="{{ asset('/assets/frontend/css/styleATG2.13.0.css') }}" rel="stylesheet">--}}
    <link href="{{ asset('/assets/frontend/css/styleLayout0.2.36.css') }}" rel="stylesheet">

    <link href="{{ asset('/assets/theme/assets/global/css/components.css') }}" rel="stylesheet">
    <link href="{{ asset('/assets/theme/assets/global/plugins/simple-line-icons/simple-line-icons.min.css') }}"
          rel="stylesheet">
    @yield('styles')

            <!-- Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Pacifico|Roboto:400italic,300,700,400|Roboto+Condensed:400,700'
          rel='stylesheet' type='text/css'>
    {{--<script src="{{ asset('/assets/js/front/plugin/jquery.min.js') }}"></script>--}}
    {{--<script src="{{ asset('/assets/js/front/plugin/bootstrap.min.js') }}"></script>--}}

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="{{ asset('/assets/js/front/plugin/html5shiv.min.js') }}"></script>
    <script src="{{ asset('/assets/js/front/plugin/respond.min.js') }}"></script>
    <![endif]-->

    <script src="{{ asset('/assets/frontend/js/JSLib-2.2.0.js') }}"></script>
    <script src="{{ asset('/assets/frontend/js/JSPage0.2.17.js') }}"></script>

    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/backstretch/jquery.backstretch.min.js') }}"></script>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                        (i[r].q = i[r].q || []).push(arguments)
                    }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-2235235-16', 'auto');
        ga('send', 'pageview');

    </script>
</head>

<body>

@yield('content')

<footer>
    @lang("home.view.index.CopyRight")
</footer>

<!-- Scripts -->


@yield('scripts')
@yield('inline-scripts')
<script>
    var sliders = [];
    @foreach($sliders as $slider)
    sliders.push("{!! $slider !!}");
    @endforeach
    $.backstretch(sliders, {
                fade: 1000,
                duration: 4000
            }
    );
</script>
</body>
</html>

