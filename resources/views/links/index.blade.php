@extends('app')

@section('content')
    <section class="fullscreen" id="link">
        <div class="container fill" id="mainAnchor">
            @foreach($links as $link)
                <div class="faqBox" id="">
                    <h2>{!! link_to($link->url,$link->title) !!}</h2>
                </div>
            @endforeach
        </div>
    </section>
@endsection
