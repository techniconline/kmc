@extends('app')

@section('browser_title')
@lang('contactUs.view.index.Contact_Us')
@endsection

@section('styles')

        <!--link id="layer-slider" rel="stylesheet" href="{{ asset('/assets/frontend/css/layerslider.css') }}" media="all"/>

    <!-- **Additional - stylesheets** -->
<link rel="stylesheet" href="{{ asset('/assets/frontend/responsive.css') }}" type="text/css" media="all"/>
<link rel="stylesheet" href="{{ asset('/assets/frontend/css/meanmenu.css') }}" type="text/css" media="all"/>
<link rel="stylesheet" href="{{ asset('/assets/frontend/css/prettyPhoto.css') }}" type="text/css" media="screen"/>
<link rel="stylesheet" href="{{ asset('/assets/frontend/css/animations.css') }}" type="text/css" media="all"/>

<!-- **Font Awesome** -->
<link rel="stylesheet" href="{{ asset('/assets/frontend/css/font-awesome.min.css') }}" type="text/css"/>

@endsection

@section('content')

    <div id="main">
        <div class="parallax full-width-bg">
            <div class="container">
                <div class="main-title">
                    <h1>@lang("contactUs.view.index.Contact_Us")</h1>

                    <div class="breadcrumb">
                        <a href="/">@lang("home.view.index.HomePage")</a>
                        <span class="fa fa-angle-left"></span>
                        <a href="contact-us">@lang("contactUs.view.index.Contact_Us")</a>

                    </div>
                </div>
            </div>
        </div>
        <!-- Primary Starts -->
        <section id="primary" class="content-full-width">
            <div id="map"></div>
            <div class="dt-sc-margin50"></div>
            <div class="full-width-section">
                <div class="dt-sc-margin50"></div>
                <div class="container">
                    <div class="column dt-sc-three-fourth first animate" data-animation="fadeInRight" data-delay="50">
                        <div class="hr-title">
                            <h3>@lang("contactUs.view.index.Contact_Us_Title")</h3>

                            <div class="title-sep">
                            </div>
                        </div>


                        {!! Form::open([ 'route' => [$locale.'contact-us.store']], ['class' => 'dt-sc-contact-form']) !!}
                        {{--<form method="post" class="dt-sc-contact-form" action="php/send.php" name="frmcontact">--}}
                        <div class="column dt-sc-one-third first">
                            <p><span> {!! Form::text('name',NULL,[ 'class' => 'txtFullBig', 'placeholder' => trans('contactUs.model.Name'), 'required'=>'', 'style'=>'height: 30px'])  !!}
                                    {!! $errors->first('email') !!} </span></p>
                        </div>
                        <div class="column dt-sc-one-third">
                            <p><span>  {!! Form::select('to',trans('contactUs.model.itemContactUs'),NULL)  !!} </span>
                            </p>
                        </div>
                        <div class="column dt-sc-one-third">
                            <p><span> @if(!isset(Auth::user()->get()->id))

                                        {!! Form::email('email',NULL,[ 'class' => 'txtFullBig', 'placeholder' =>  trans('contactUs.model.Email'), 'required'=>'', 'style'=>'height: 30px'])  !!}
                                        {!! $errors->first('email') !!}

                                    @else
                                        {!! Form::email('email',Auth::user()->get()->email,['readonly'=>'', 'class' => 'txtFullBig', 'placeholder' =>  trans('contactUs.model.Email'), 'required'=>'', 'style'=>'height: 30px'])  !!}

                                    @endif </span>
                            </p>


                        </div>
                        <p> {!! Form::textarea('message',NULL,['class' => 'txtFullBig', 'placeholder' => trans('contactUs.model.Message'), 'rows'=>4, 'required'=>''])  !!}
                            {!! $errors->first('message') !!}</p>

                        <p>{!! Form::submit(trans('contactUs.model.submit'),[ 'class' => 'submit button', 'style'=>'height: 30px'])  !!}</p>

                        <p>
                            {!! Recaptcha::render(['lang'=>app('activeLangDetails')['lang']]) !!}
                            {!! $errors->first('g-recaptcha-response') !!}


                        </p>
                        {!! Form::close() !!}
                        <div id="ajax_contact_msg">
                            @if (Session::has('flash_notification.message'))
                                <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                                    <button type="button" class="close" data-dismiss="alert"
                                            aria-hidden="true">&times;</button>
                                    {{ Session::get('flash_notification.message') }}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="column dt-sc-one-fourth animate" data-animation="fadeInLeft" data-delay="100">
                        <div class="hr-title">
                            <h3>@lang("contactUs.view.info.Info")</h3>

                            <div class="title-sep">
                            </div>
                        </div>
                        <p class="dt-sc-contact-detail">@lang("contactUs.view.info.Text")</p>
                        <!-- **dt-sc-contact-info - Starts** -->
                        <div class="dt-sc-contact-info">
                            <p><i class="fa fa-location-arrow"></i>@lang("contactUs.view.info.Address")</p>

                            <p><i class="fa fa-phone"></i>@lang("contactUs.view.info.Tel")</p>

                            <p><i class="fa fa-globe"></i> <a href="#">@lang("contactUs.view.info.Site")</a></p>

                            <p><i class="fa fa-envelope"></i> <a href="#">@lang("contactUs.view.info.Email")</a></p>
                        </div>
                        <!-- **dt-sc-contact-info - Ends** -->
                    </div>

                </div>
                <div class="dt-sc-margin80"></div>
            </div>
            <div class="dt-sc-margin80"></div>
        </section>
    </div>

    {{--<section class="fullscreen" id="contactsSection" style="padding-top: 40px">
        <div class="container">
            <article class="fill">
                {!! ParamsHelper::getStaticContent('ContactUsHeader')['body_content'] !!}

    <div class="container-fluid">
        <div class="row" style="width: 500px">
            <div class="col-md-8 col-md-offset-2" style="width: 500px; float: right">
                @if (Session::has('flash_notification.message'))
                    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('flash_notification.message') }}
                    </div>
                @endif
                --}}{{--<div class="panel panel-default" style="width: 500px">--}}{{--
                    --}}{{--<div class="panel-heading" style="width: 500px; padding: 2px ">--}}{{--
--}}{{--                       @lang('contactUs.view.index.Contact_Us')--}}{{--
                    --}}{{--</div>--}}{{--
                    --}}{{--<div class="panel-body" style="width: 100%; padding: 0px; padding-left: 4px">--}}{{--
                        {!! Form::open([ 'route' => [$locale.'contact-us.store']]) !!}
                            --}}{{--<div class="form-group">--}}{{--
                        <label></label>
                            {!! Form::select('to',trans('contactUs.model.itemContactUs'),NULL,[ 'class' => 'form-control ', 'style'=>'height: 30px'])  !!}
                            --}}{{--</div>--}}{{--
                        <label>@lang('contactUs.model.Name')</label>
                            {!! Form::text('name',NULL,[ 'class' => 'txtFullBig', 'placeholder' => trans('contactUs.model.Name'), 'required'=>'', 'style'=>'height: 30px'])  !!}
                            {!! $errors->first('email') !!}
                        <label>@lang('contactUs.model.Email')</label>
                            @if(!isset(Auth::user()->get()->id))
                                --}}{{--<div class="form-group" style="padding: 0px">--}}{{--
                                    {!! Form::email('email',NULL,[ 'class' => 'txtFullBig', 'placeholder' =>  trans('contactUs.model.Email'), 'required'=>'', 'style'=>'height: 30px'])  !!}
                                    {!! $errors->first('email') !!}
                                --}}{{--</div>--}}{{--
                            @else
                            {!! Form::email('email',Auth::user()->get()->email,['readonly'=>'', 'class' => 'txtFullBig', 'placeholder' =>  trans('contactUs.model.Email'), 'required'=>'', 'style'=>'height: 30px'])  !!}
--}}{{--                        {!! Form::hidden('email',Auth::user()->get()->email)  !!}--}}{{--
                            @endif
                            --}}{{--<div class="form-group">--}}{{--
                                <label>@lang('contactUs.model.Message')</label>
                                {!! Form::textarea('message',NULL,['class' => 'txtFullBig', 'placeholder' => trans('contactUs.model.Message'), 'rows'=>4, 'required'=>''])  !!}
                                {!! $errors->first('message') !!}
                            --}}{{--</div>--}}{{--

                            --}}{{--<div class="form-group">--}}{{--
                                <label></label>
                                {!! Form::submit(trans('contactUs.model.submit'),[ 'class' => 'submit button', 'style'=>'height: 30px'])  !!}
                            --}}{{--</div>--}}{{--
                        --}}{{--<div class="form-group">--}}{{--
                            <label></label>

                            {!! Recaptcha::render(['lang'=>app('activeLangDetails')['lang']]) !!}
                            {!! $errors->first('g-recaptcha-response') !!}
                        --}}{{--</div>--}}{{--
                        {!! Form::close() !!}
                    --}}{{--</div>--}}{{--
                --}}{{--</div>--}}{{--
            </div>
        </div>
    </div>

            </article>
        </div>
    </section>--}}
@endsection

@section('scripts')
    <script src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <script src="{{ asset('/assets/frontend/js/jquery.gmap.min.js') }}"></script>
@endsection

@section('inline-scripts')
@endsection
