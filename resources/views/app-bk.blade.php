<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {!! ParamsHelper::metaTag() !!}
    {!! ParamsHelper::favIcon() !!}

    <title>@yield('browser_title')</title>

    <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('/assets/theme/assets/global/css/components.css') }}" rel="stylesheet">
    <link href="{{ asset('/assets/theme/assets/global/plugins/simple-line-icons/simple-line-icons.min.css') }}"
          rel="stylesheet">
    @yield('styles')

            <!-- Fonts -->
    <link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="{{ asset('/assets/js/front/plugin/html5shiv.min.js') }}"></script>
    <script src="{{ asset('/assets/js/front/plugin/respond.min.js') }}"></script>
    <![endif]-->
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Studenest</a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                {{--                {!! MenusHelper::frontEndMenusForHomeBuild($frontend_menus,$contents) !!}--}}
            </ul>

            <ul class="nav navbar-nav navbar-right">
                @if (Auth::user()->guest())
                    <li><a href="{{ url('/front/auth/login') }}">@lang("home.view.index.Login")</a></li>
                    <li><a href="{{ url('/front/auth/register') }}">@lang("home.view.index.Register")</a></li>
                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-expanded="false">{{ Auth::user()->get()->firstname.' '.Auth::user()->get()->lastname }}
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ url('/front/profile') }}">@lang("home.view.index.Profile")</a></li>
                            <li>
                                <a href="{{ route('front.profile.password') }}">@lang("home.view.index.Change_Password")</a>
                            </li>
                            <li><a href="{{ url('/front/profile/messages') }}">@lang("home.view.index.YourMessage")
                                    (@lang("home.view.index.New") {!!\App\Profile::checkMessagesUser(Auth::user()->get()->id)!!}
                                    )</a></li>
                            <li><a href="{{ url('/front/auth/logout') }}">@lang("home.view.index.Logout")</a></li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>

<div id="content">
    @yield('content')
</div>

<!-- Scripts -->

<script src="{{ asset('/assets/js/front/plugin/jquery.min.js') }}"></script>

{{--<script src="{{ asset('/assets/theme/assets/global/plugins/fancybox/source/jquery.fancybox.js') }}"></script>--}}
{{--<script src="{{ asset('/assets/js/front/plugin/jquery.min.js') }}"></script>--}}
{{--<script src="{{ asset('/assets/js/front/plugin/bootstrap.min.js') }}"></script>--}}
{{--<script src="{{ asset('/assets/frontend/js/JSLib-2.2.0.js') }}"></script>--}}
{{--<script src="{{ asset('/assets/frontend/js/JSPage0.2.17.js') }}"></script>--}}

@yield('scripts')
@yield('inline-scripts')

</body>
</html>
