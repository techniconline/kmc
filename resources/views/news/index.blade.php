@extends('app')
<?php  $langDetails = app('activeLangDetails');?>

@section('browser_title')
    {{--    {!! $content->browser_title !!}--}}
@endsection

@section('styles')
    <link rel="stylesheet" href="{{ asset('/assets/frontend/mis.css') }}" media="all"/>
    @endsection
    @section('content')

            <!-- Primary Starts -->

    <div class="parallax full-width-bg persia">
        <div class="container">
            <div class="main-title">
                <h1>@lang('News.view.index.RecentPostsNewsText')</h1>

                <div class="breadcrumb">
                    <a href="index.html">@lang('home.view.index.HomePage')</a>
                    <span class="fa fa-angle-left"></span>
                    <span class="current">@lang('News.view.index.RecentPostsNews')</span>
                </div>
            </div>
        </div>
    </div>
    <div class="dt-sc-margin100"></div>
    <!-- Container starts-->
    <div class="container">
        @if (Session::has('flash_notification.message'))
            <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                {{--<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>--}}
                {{ Session::get('flash_notification.message') }}
            </div>
        @endif
        @if(isset($systemContent)&&$systemContent=='weblog')
            <div>
                <a href="{{URL::route($locale.'weblog.create')}}">@lang('webLog.view.index.CreateWebLog')</a>
            </div>
            @endif

                    <!-- Primary Starts -->
            <section id="primary" class="with-right-sidebar page-with-sidebar">

                <!-- **Blog-post - Starts** -->
                @foreach($news as $new)
                    <article class="blog-post type3 base persia">
                        <!-- **entry-meta - Starts** -->
                        {{--<div class="entry-meta">--}}
                        {{--<div class="date">--}}
                        {{--<p>--}}
                        {{--<span>{{$new->pDay}} <p style="font-size: 12px">{{$new->pDayName}}</p> </span>--}}
                        {{--<span><p style="font-size: 12px">{{$new->pMonthName}}</p> </span>--}}
                        {{--<span><p style="font-size: 12px">{{$new->pYear}}</p> </span>--}}
                        {{--</p>--}}
                        {{--</div>--}}
                        {{--<div class="post-comments">--}}
                        {{--<a href="#"><span class="fa fa-comment"></span> {!!$new->count_comments!!} </a>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        <!-- **entry-meta - Ends** -->
                        <!-- **entry-detail - Starts** -->
                        <div class="dt-sc-full-width">
                            <div class="entry-detail widget widget_tag_cloud">
                                <div class="entry-title">
                                    <h4>
                                        <a href="{!! route(($new->type=='blog'?'content':$new->type).'.show',$new->url)  !!}">{!! $new->link_title !!}</a>
                                    </h4>
                                </div>
                                <div class="entry-meta-data">
                                    <p>
                                        <span class="fa fa-sitemap"> </span> @lang("backend/Product.view.create.Category")
                                        : <a
                                                href="#"> {{$new->name_category}} </a></p>

                                    <p><span class="fa fa-user"> </span> @lang("News.view.index.PostedBy"): <a
                                                href="#"> {{$new->user_name}} </a></p>

                                    <p><span class="fa fa-calendar"> </span>
                                        {{$new->pDay}} {{$new->pMonthName}} {{$new->pYear}}
                                    </p>

                                    <p><a href="#"><span
                                                    class="fa fa-comment"></span> @lang("News.view.index.Comments") {!!$new->count_comments!!}
                                        </a></p>
                                    {{--<p class="tagcloud"><span class="fa fa-tag"> </span>--}}
                                    {{--@if($new->tags && $tagsArr=explode(",",$new->tags))--}}
                                    {{--@foreach(($tagsArr) as $tag)--}}
                                    {{--@if($tag)--}}
                                    {{--<a href="{{URL::route(($new->type=='weblog'?'weblog':'news').'.searchTags',['tag'=>$tag])}}"> {!!$tag!!} </a>--}}
                                    {{--@endif--}}
                                    {{--@endforeach--}}
                                    {{--@endif--}}
                                    {{--</p>--}}
                                </div>
                                <!-- **entry-meta-data - Starts** -->
                                <div class="entry-body">
                                    <div class="right dt-sc-three-fourth">{!!$new->short_content ? nl2br($new->short_content):'&nbsp;'!!}</div>
                                    <!-- **entry-thumb - Starts** -->
                                    <div class="column dt-sc-one-fourth">
                                        <div class="entry-thumb">

                                            <h4>
                                                <a href="{!! route(($new->type=='blog'?'content':$new->type).'.show',$new->url)  !!}">
                                                    @if($new->image_content
                                                    && \Illuminate\Support\Facades\File::exists(base_path().env('PUBLIC_PATH_URL', 'public').$new->image_content.'/'.$new->contents_id.'-160x80.'.$new->extension))
                                                        <img src="{{asset($new->image_content).'/'.$new->contents_id.'-160x80.'.$new->extension}}"
                                                             alt="{!!$new->tags!!}" title=""/>
                                                    @else
                                                        <img src="{{asset('images/no-image.jpg')}}" alt=""/>
                                                    @endif
                                                </a>
                                            </h4>
                                        </div>
                                    </div>
                                    <!-- **entry-thumb - Ends** -->
                                </div>

                                <!-- **entry-meta-data - Ends** -->
                            </div>
                        </div>

                        <!-- **entry-detail - Ends** -->

                    </article>
                @endforeach

                <article class="blog-post type3">
                    <div class="dataTables_info" id="infoData" role="status" aria-live="polite"
                         style="vertical-align: middle">
                        <div class="pagination">
                            {!!$news->appends(Input::all())->render()!!}
                        </div>
                        @lang("News.view.index.Entities") {!!$news->total()!!}

                    </div>
                </article>
                <!-- **Blog-post - Ends** -->

            </section>
            <!-- **Primary - Ends** -->


            <!-- **secondary - Starts** -->
            @include('partials.sidebar')
                    <!-- **secondary - Ends** -->
    </div>


@endsection


@section('scr1')
    <script src="{{ asset('/assets/frontend/js/jquery.jcarousel.min.js') }}" type="text/javascript"></script>
@endsection

@section('scr2')
    <script src="{{ asset('/assets/frontend/js/jquery.smartresize.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/assets/frontend/js/jquery.scrollTo.js') }}" type="text/javascript"></script>
@endsection
@section('scr-retina')
    <script src="{{ asset('/assets/frontend/js/retina.js') }}" type="text/javascript"></script>
@endsection


@section('scripts')
@endsection

@section('inline-scripts')
    <script>
        jQuery.noConflict();
        jQuery(document).ready(function ($) {
            $("div.pagination").children("ul.pagination").removeClass().removeAttr("class");
        });
    </script>
@endsection
