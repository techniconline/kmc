@extends('app')

@section('browser_title')
    @lang("faq.view.index.TITLE_HEAD")
@endsection

@section('styles')

@endsection

@section('content')

    <section class="fullscreen" id="faq">
        <div class="container fill" id="mainAnchor">
            @foreach($faq_type as $key => $value)
                <div class="faqBox" id="{!!$key!!}">
                    <h2>{!!$value!!}</h2>
                </div>
            @endforeach
            @foreach($faq_type as $key => $value)
                <article id="{!!$key!!}-info" class="fill info">
                    <div class="post atgLTR"><a name="FAQ General"></a>
                        {{--<h1>RÉSERVATION ET CONTRAT</h1>--}}

                        @if(count($faqs[$key])>0)
                            @foreach($faqs[$key] as $faq)
                                <p>
                                <h2>» {!! $faq['question'] !!}</h2>
                                <p>
                                <div class='block'>
                                    {!! $faq['answer'] !!}
                                </div>

                                </p>
                            @endforeach
                        @endif
                    </div>
                </article>
            @endforeach
        </div>
    </section>

@endsection

@section('scripts')
@endsection

@section('inline-scripts')
@endsection
