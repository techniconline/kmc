@extends('app')
@section('styles')

    <link href="{{ asset('/assets/frontend/css/profile.css') }}" rel="stylesheet" type="text/css"/>

@endsection
@section('content')
    <section class="fullscreen" id="profileSection">
        <div class="container">
            @if (Session::has('flash_notification.message'))
                <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ Session::get('flash_notification.message') }}
                </div>
            @endif
            <h2>@lang('profile.view.avatar.Change_Avatar')</h2>

            <div class="half fleft pl20">
                {!! Form::open(['route' => 'front.profile.changeAvatar','class'=>'form-horizontal form-bordered', 'files'=>'true']) !!}

                <div class="col-md-6">
                    @if(\Illuminate\Support\Facades\File::exists(base_path().env('PUBLIC_PATH_URL', 'public').'/uploads/images/Users/'.Auth::user()->get()->id.'-200x200.jpg'))
                        {!! Html::image('uploads/images/Users/'.Auth::user()->get()->id.'-200x200.jpg',Auth::user()->get()->lastname,['class'=>'col-md-12 blah']) !!}
                    @else
                        <img class="blah col-md-12" src="#" alt="your image"/>
                    @endif
                </div>

                {!!Form::label('avatar',trans("profile.view.avatar.Avatar"),['class'=>'control-label col-md-2'])!!}
                <div>
                    {!! Form::file('avatar',['onchange'=>"readURL(this)"]) !!}
                    {!!$errors->first('avatar','<span class="error">:message</span>')!!}
                </div>

                <div class="fi-postBtn-right">
                    <input type="submit" value="@lang('profile.view.avatar.Save')" name="submit" class="fi-postBtn"
                           style="width: 100%">
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </section>
@endsection

@section('inline-scripts')
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('.blah')
                            .attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endsection
