<h2>{!!$title!!}</h2>
<form class="form-horizontal" role="form" method="POST"
      action="{!!URL::route('front.profile.update',['user_id'=>$data->id])!!}">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="page" value="{!!$page!!}">

    <div class="form-group">
        <label class="col-md-4 control-label"> @lang("profile.view.user.fName")</label>

        <div class="input-group col-md-6">
            <span class="input-group-addon">
            <i class="fa fa-user"></i>
            </span>
            <input type="text" class="form-control" name="firstname" value="{{$data->firstname}}" required=""
                   placeholder="@lang("profile.view.user.fName")">
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label"> @lang("profile.view.user.lName")</label>

        <div class="input-group col-md-6">
            <span class="input-group-addon">
            <i class="fa fa-user"></i>
            </span>
            <input type="text" class="form-control" name="lastname" value="{{$data->lastname }}" required=""
                   placeholder="@lang("profile.view.user.lName")">
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label"> @lang("profile.view.user.email")</label>

        <div class="input-group col-md-6">
            <span class="input-group-addon">
            <i class="fa fa-envelope"></i>
            </span>
            <input type="text" class="form-control" placeholder="Email Address" name="email" value="{{$data->email}}"
                   required="" placeholder="@lang("profile.view.user.email")">
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">@lang("profile.view.user.gender")</label>

        <div class="input-group col-md-3">
            <span class="input-group-addon">
            <i class="fa fa-user"></i>
            </span>
            {!!Form::select  ('gender', trans("profile.view.user.genderItem"),$data->gender,['class'=>'btn form-control input-small select2me','id'=>'gender','required'=>''])!!}
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-md-4">@lang("profile.view.user.birthDate")</label>

        <div class="input-group col-md-3">
            <span class="input-group-addon">
            <i class="fa fa-user"></i>
            </span>

            <div class="input-group input-small date date-picker" data-date-format="yyyy-mm-dd">
                {!!Form::text('birth_date',$data->birth_date,array('class'=>'form-control','size'=>16,'required'=>'','placeholder'=>trans("profile.view.user.birthDate")))!!}
                <span class="input-group-btn">
                <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                </span>
            </div>
        </div>
    </div>

    <div class="form-group">
        {!!Form::label('address',trans("profile.view.user.Address"),['class'=>'control-label col-md-4'])!!}
        <div class="input-group col-md-6">
            <span class="input-group-addon">
            <i class="fa fa-home"></i>
            </span>
            {!!Form::text('address',$data->address,array('class'=>'form-control','required'=>'','placeholder'=>trans("profile.view.user.Address")))!!}
            {!!$errors->first('address','<span class="error">:message</span>')!!}
        </div>
    </div>

    <div class="form-group">
        {!!Form::label('code_postal',trans("profile.view.user.CodePostal"),['class'=>'control-label col-md-4'])!!}
        <div class="input-group col-md-3">
            <span class="input-group-addon">
            <i class="fa fa-user"></i>
            </span>
            {!!Form::text('code_postal',$data->code_postal,array('class'=>'form-control','required'=>'','placeholder'=>trans("profile.view.user.CodePostal")))!!}
            {!!$errors->first('code_postal','<span class="error">:message</span>')!!}
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">@lang("profile.view.user.Country")</label>

        <div class="input-group col-md-3">
            <span class="input-group-addon">
            <i class="fa fa-user"></i>
            </span>
            {!!Form::select('country_id', $dataCountries,$data->country_id,['class'=>'btn form-control input-small select2me','id'=>'country','required'=>'','style'=>'text-align: left'])!!}
        </div>
    </div>


    <div class="form-group">
        {!!Form::label('mobile',trans("profile.view.user.Mobile"),['class'=>'control-label col-md-4'])!!}
        <div class="input-group col-md-3">
            <span class="input-group-addon">
            <i class="fa fa-mobile"></i>
            </span>
            {!!Form::text('mobile',$data->mobile,array('class'=>'form-control','required'=>'','placeholder'=>trans("profile.view.user.Mobile")))!!}
            {!!$errors->first('mobile','<span class="error">:message</span>')!!}
        </div>
    </div>

    <div class="form-group">
        {!!Form::label('telephone',trans("profile.view.user.Tel"),['class'=>'control-label col-md-4'])!!}
        <div class="input-group col-md-3">
            <span class="input-group-addon">
            <i class="fa fa-phone"></i>
            </span>
            {!!Form::text('telephone',$data->telephone,array('class'=>'form-control','placeholder'=>trans("profile.view.user.Tel")))!!}
            {!!$errors->first('telephone','<span class="error">:message</span>')!!}
        </div>
    </div>


    <div class="form-group">
        <div class="col-md-6 col-md-offset-4">
            <button type="submit" class="btn btn-primary" id="btnSave">
                @lang("profile.view.user.save")
            </button>
            <span class="result">
                <i class="fa fa-error font-yellow"></i>
            </span>
            <span class="loading"
                  style="background: url('{{ asset('/assets/img/loader.gif') }}') no-repeat;padding: 5px 20px 20px; display: none "></span>
        </div>
    </div>
</form>
<script>
    handleDatePickers();
</script>
