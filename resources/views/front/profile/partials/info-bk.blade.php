<h2>{!!$title!!}</h2>
<form class="form-horizontal" role="form" method="POST"
      action="{!!URL::route('front.profile.update',['user_id'=>Auth::user()->get()->id])!!}">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="page" value="{!!$page!!}">

    <div class="form-group">
        <label class="col-md-4 control-label"> @lang("profile.view.info.YouAre")</label>

        <div class="input-group col-md-6">
            <span class="input-group-addon">
            <i class="fa fa-user"></i>
            </span>
            {!!Form::select  ('persona', $otherData['persona'],isset($data->persona)?$data->persona:'',['class'=>'btn form-control input-small select2me','id'=>'persona','required'=>''])!!}
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label"> @lang("profile.view.info.YourLevelEducation")</label>

        <div class="input-group col-md-6">
            <span class="input-group-addon">
            <i class="fa fa-user"></i>
            </span>
            {!!Form::select  ('education', $otherData['education'],isset($data->education)?$data->education:'',['class'=>'btn form-control input-small select2me','id'=>'education','required'=>''])!!}
        </div>
    </div>

    <h4>@lang("profile.view.info.YourInstitution")</h4>

    <div class="form-group">
        <label class="col-md-4 control-label"> @lang("profile.view.info.Type")</label>

        <div class="input-group col-md-6">
            <span class="input-group-addon">
            <i class="fa fa-user"></i>
            </span>
            {!!Form::select  ('institution_type', $otherData['institution_type'],isset($data->institution_type)?$data->institution_type:'',['class'=>'btn form-control input-small select2me','id'=>'institution_type','required'=>''])!!}
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label"> @lang("profile.view.info.FacilityName")</label>

        <div class="input-group col-md-6">
            <span class="input-group-addon">
            <i class="fa fa-user"></i>
            </span>
            <input type="text" class="form-control" name="institution"
                   value="{!!isset($data->institution)?$data->institution:''!!}" required="">
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label"> @lang("profile.view.info.PropertyTown")</label>

        <div class="input-group col-md-6">
            <span class="input-group-addon">
            <i class="fa fa-user"></i>
            </span>
            <input type="text" class="form-control" name="institution_town"
                   value="{!!isset($data->institution_town)?$data->institution_town:''!!}" required="">
        </div>
    </div>


    <h4>@lang("profile.view.info.YourAccommodationDuringYourRental")</h4>

    <div class="form-group">
        <label class="col-md-4 control-label"> @lang("profile.view.info.Type")</label>

        <div class="input-group col-md-6">
            <span class="input-group-addon">
            <i class="fa fa-user"></i>
            </span>
            {!!Form::select  ('current_institution_type', $otherData['current_institution_type'],isset($data->current_institution_type)?$data->current_institution_type:'',['class'=>'btn form-control input-small select2me','id'=>'current_institution_type','required'=>''])!!}
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label"> @lang("profile.view.info.FacilityName")</label>

        <div class="input-group col-md-6">
            <span class="input-group-addon">
            <i class="fa fa-user"></i>
            </span>
            <input type="text" class="form-control" name="current_institution"
                   value="{!!isset($data->current_institution)?$data->current_institution:''!!}" required="">
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label"> @lang("profile.view.info.PropertyTown")</label>

        <div class="input-group col-md-6">
            <span class="input-group-addon">
            <i class="fa fa-user"></i>
            </span>
            <input type="text" class="form-control" name="current_institution_town"
                   value="{!!isset($data->current_institution_town)?$data->current_institution_town:''!!}" required="">
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label"> @lang("profile.view.info.DoYouHaveGuarantorForTheLease")</label>

        <div class="input-group col-md-6">
            <span class="input-group-addon">
            <i class="fa fa-user"></i>
            </span>
            {!!Form::select  ('parent_guarantor', $otherData['parent_guarantor'],isset($data->parent_guarantor)?$data->parent_guarantor:'',['class'=>'btn form-control input-small select2me','id'=>'parent_guarantor','required'=>''])!!}
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label"> @lang("profile.view.info.PersonalMessage")</label>

        <div class="input-group col-md-6">
            <span class="input-group-addon">
            <i class="fa fa-pencil"></i>
            </span>

            <textarea class="form-control" name="personal_message"
                      rows="3">{!!isset($data->personal_message)?$data->personal_message:''!!}</textarea>
        </div>
    </div>

    <div class="form-group">
        <div class="checkbox-list">
            <label class="col-md-10 control-label">
                {!!Form::checkbox ('accuracy_information',1,isset($data->accuracy_information)?$data->accuracy_information:''
                        ,['class'=>'form-control','id'=>'accuracy_information','required'=>''])!!} @lang("profile.view.info.AccuracyInformation")
            </label>
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-6 col-md-offset-4">
            <button type="submit" class="btn btn-primary" id="btnSave">
                @lang("profile.view.user.save")
            </button>
            <span class="result">
                <i class="fa fa-error font-yellow"></i>
            </span>
            <span class="loading"
                  style="background: url('{{ asset('/assets/img/loader.gif') }}') no-repeat;padding: 5px 20px 20px; display: none "></span>
        </div>
    </div>
</form>

