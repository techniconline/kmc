{{--<h2>{!!$title!!}</h2>--}}

{!!Form::open(['route' => [$locale.'front.profile.update',$data->id],'method' => 'PUT','class'=>'form-horizontal form-bordered register-form','files'=> true])!!}
<input type="hidden" name="page" value="{!!$page!!}">


<div class="dt-sc-one-half column first">
    <p class="dt-sc-one-half column first">
        <span class="fi-required">@lang("profile.view.user.fName")</span>
        <input class="col-xs-8" name="firstname"
               value="{{$data->firstname}}" type="text" placeholder="@lang("profile.view.user.fName")" required>
    </p>

    <p class="dt-sc-one-half column">
        <span class="fi-required">@lang("profile.view.user.lName")</span>
        <input class="col-xs-8" name="lastname"
               value="{{$data->lastname}}" type="text" placeholder="@lang("profile.view.user.lName")" required>
    </p>

    <p class="dt-sc-one-column column first">
        <span class="fi-required">@lang("profile.view.user.email")</span>
        <input class="col-xs-8" name="email"
               value="{{$data->email}}" type="text" placeholder="@lang("profile.view.user.email")" required>
    </p>

    <p class="dt-sc-one-column column first">
        <span class="fi-required">@lang("profile.view.user.gender")</span>
        {!!Form::select  ('gender', trans("profile.view.user.genderItem"),$data->gender,['style'=>'text-align: left','class'=>'input-small select2me col-xs-8','id'=>'gender','required'=>''])!!}
    </p>

    <p class="dt-sc-one-half column first">
        <span class="fi-required">@lang("profile.view.user.Mobile")</span>
        {!!Form::text('mobile',$data->mobile,array('class'=>'col-xs-8','required'=>'','placeholder'=>trans("profile.view.user.Mobile")))!!}
    </p>

    <p class="dt-sc-one-half column">
        <span class="fi-required">@lang("profile.view.user.Tel")</span>
        {!!Form::text('telephone',$data->telephone,array('class'=>'col-xs-8','placeholder'=>trans("profile.view.user.Tel")))!!}
    </p>

    {{--<p class="dt-sc-one-half column first">--}}
    {{--<span id="span-date-birth" class="fi-required">@lang("profile.view.user.birthDate")</span>--}}
    {{--<span class="input-group-btn">--}}
    {{--<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>--}}
    {{--{!!Form::text('birth_date',PersianDate::convert_date_M_to_H($data->birth_date)--}}
    {{--,array('style'=>'text-align: center','class'=>'input-small','size'=>16,'required'=>'', 'id'=>'input-date-birth'--}}
    {{--,'placeholder'=>trans("profile.view.user.birthDate")))!!}--}}
    {{--</span>--}}

    {{--</p>--}}


</div>

<div class="dt-sc-one-half column">

    <p class="dt-sc-one-column column first">
        <span class="fi-required">@lang("profile.view.user.Address")</span>
        {!!Form::text('address',$data->address,array('class'=>'col-xs-8','required'=>'','placeholder'=>trans("profile.view.user.Address")))!!}
    </p>

    <p class="dt-sc-one-column column first">
        <span class="fi-required">@lang("profile.view.user.CodePostal")</span>
        {!!Form::text('code_postal',$data->code_postal,array('class'=>'col-xs-8','required'=>'','placeholder'=>trans("profile.view.user.CodePostal")))!!}
    </p>

    <p class="dt-sc-one-column column first">
        <span class="fi-required">@lang("profile.view.user.Country")</span>
        {!!Form::select('country_id', $dataCountries,$data->country_id,['class'=>'input-small select2me col-xs-8','id'=>'country','required'=>'','style'=>'text-align: left'])!!}
    </p>

    <p class="dt-sc-one-column column first">
        <span class="fi-required">@lang("profile.view.user.City")</span>
        {!!Form::text('city',$data->city,array('class'=>'col-xs-8','required'=>'','placeholder'=>trans("profile.view.user.City")))!!}
    </p>


    <hr>
    <div class="fi-postBtn-right dt-sc-one-column column first">
        <input type="submit" value="@lang("profile.view.user.save")" name="submit"
               class="fi-postBtn" id="btnSave" style="width: 100%">
        <span class="result">
            <i class="fa fa-error font-yellow"></i>
        </span>
            <span class="loading"
                  style="background: url('{{ asset('/assets/img/loader.gif') }}') no-repeat;padding: 5px 20px 20px; display: none "></span>
    </div>


{!!Form::close()!!}

