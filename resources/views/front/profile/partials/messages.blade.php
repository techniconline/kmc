<!-- BEGIN SAMPLE TABLE PORTLET-->
<div class="portlet">
    <div class="portlet-title">
        <div class="caption">
            {{--<i class="fa fa-comment "></i>@lang("profile.view.messagesUser.ListMessagesForAttachment")--}}
        </div>
        <div class="tools">
            {{--<a href="javascript:;" class="collapse">--}}
            {{--</a>--}}
            {{--<a href="#portlet-config" data-toggle="modal" class="config">--}}
            {{--</a>--}}
            {{--<a href="javascript:;" class="reload">--}}
            {{--</a>--}}
            {{--<a href="javascript:;" class="remove">--}}
            {{--</a>--}}
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-scrollable">
            <table class="table table-striped table-bordered table-advance table-hover">
                <thead>
                <tr>
                    <th>
                        <i class="fa fa-user"></i>@lang("profile.view.messagesUser.From")
                    </th>
                    <th class="hidden-xs">
                        <i class="fa fa-bullhorn"></i>@lang("profile.view.messagesUser.Subject")
                    </th>
                    <th>
                        <i class="fa fa-calendar"></i>@lang("profile.view.messagesUser.Date")
                    </th>
                    <th>
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($dataMessage as $item)
                    <tr class="tr-row">
                        <td>
                            {!!$item->firstname.' '.$item->lastname!!}
                        </td>
                        <td class="hidden-xs">
                            {!!$item->subject!!}
                        </td>
                        <td>
                            {!!$item->created_at!!}
                            <span class="label label-sm label-success label-mini">
                            <b>{!! (!$item->view_status)?trans("profile.view.messagesUser.New"):trans("profile.view.messagesUser.Read") !!}</b>
                        </span>
                        </td>
                        <td>
                            <a data-toggle="modal"
                               href="{!!URL::route('front.profile.showMessage',['id'=>$item->id])!!}/#basic"
                               data-id="{!!$item->id!!}"
                               class=" btn default btn-xs green-stripe load-msg">
                                <i class="fa fa-eye"></i>
                                @lang("profile.view.messagesUser.View")
                            </a>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>

        <div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title font-green">...</h4>
                        <h6 class="modal-assessor font-gray">...</h6>
                        <span class="loading" style="display: none">
                            <img src="{{ asset('/assets/theme/assets/global/img/loading-spinner-grey.gif') }}" alt=""
                                 class="loading">
                            <span>
                            &nbsp;&nbsp;Loading...
                            </span>
                        </span>
                    </div>
                    <div class="modal-body">
                        ...
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn default" data-dismiss="modal">Close</button>
                        {{--<button type="button" class="btn blue">Save changes</button>--}}
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>


    </div>
</div>
<!-- END SAMPLE TABLE PORTLET-->