<h2>{!!$title!!}</h2>
<form class="form-horizontal" role="form" method="POST"
      action="{!!URL::route('front.profile.update',['user_id'=>Auth::user()->get()->id])!!}">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="page" value="{!!$page!!}">

    <div class="form-group">
        <label class="col-md-4 control-label"> @lang("profile.view.parent.fName")</label>

        <div class="input-group col-md-6">
            <span class="input-group-addon">
            <i class="fa fa-user"></i>
            </span>
            <input type="text" class="form-control" name="parent_first_name"
                   value="{{isset($data->parent_first_name)?$data->parent_first_name:''}}" required="">
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label"> @lang("profile.view.parent.lName")</label>

        <div class="input-group col-md-6">
            <span class="input-group-addon">
            <i class="fa fa-user"></i>
            </span>
            <input type="text" class="form-control" name="parent_last_name"
                   value="{{isset($data->parent_last_name)?$data->parent_last_name:''}}" required="">
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label"> @lang("profile.view.parent.email")</label>

        <div class="input-group col-md-6">
            <span class="input-group-addon">
            <i class="fa fa-envelope"></i>
            </span>
            <input type="text" class="form-control" placeholder="Email Address" name="parent_email"
                   value="{{isset($data->parent_email)?$data->parent_email:''}}" required="">
        </div>
    </div>

    <div class="form-group">
        {!!Form::label('mobile',trans("profile.view.parent.Mobile"),['class'=>'control-label col-md-4'])!!}
        <div class="input-group col-md-3">
            <span class="input-group-addon">
            <i class="fa fa-mobile"></i>
            </span>
            {!!Form::text('parent_mobile',isset($data->parent_mobile)?$data->parent_mobile:'',array('class'=>'form-control','required'=>''))!!}
            {!!$errors->first('parent_mobile','<span class="error">:message</span>')!!}
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">@lang("profile.view.parent.SameTenant")</label>

        <div class="input-group col-md-3">
            <span class="input-group-addon">
            <i class="fa fa-user"></i>
            </span>
            {!!Form::select  ('parent_tenant_address', trans("profile.view.parent.parentTenant"),isset($data->parent_tenant_address)?$data->parent_tenant_address:'',['class'=>'btn form-control input-small select2me','id'=>'gender','required'=>''])!!}
        </div>
    </div>

    <div class="form-group">
        {!!Form::label('parent_address',trans("profile.view.user.Address"),['class'=>'control-label col-md-4'])!!}
        <div class="input-group col-md-6">
            <span class="input-group-addon">
            <i class="fa fa-home"></i>
            </span>
            {!!Form::text('parent_address',isset($data->parent_address)?$data->parent_address:'',array('class'=>'form-control','required'=>''))!!}
            {!!$errors->first('parent_address','<span class="error">:message</span>')!!}
        </div>
    </div>

    <div class="form-group">
        {!!Form::label('parent_postal_code',trans("profile.view.user.CodePostal"),['class'=>'control-label col-md-4'])!!}
        <div class="input-group col-md-3">
            <span class="input-group-addon">
            <i class="fa fa-user"></i>
            </span>
            {!!Form::text('parent_postal_code',isset($data->parent_postal_code)?$data->parent_postal_code:'',array('class'=>'form-control','required'=>''))!!}
            {!!$errors->first('parent_postal_code','<span class="error">:message</span>')!!}
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">@lang("profile.view.user.Country")</label>

        <div class="input-group col-md-3">
            <span class="input-group-addon">
            <i class="fa fa-user"></i>
            </span>
            {!!Form::select  ('parent_country_id', $dataCountries,isset($data->parent_country_id)?$data->parent_country_id:'',['class'=>'btn form-control input-small select2me','id'=>'country','required'=>''])!!}
        </div>
    </div>


    <div class="form-group">
        <div class="col-md-6 col-md-offset-4">
            <button type="submit" class="btn btn-primary" id="btnSave">
                @lang("profile.view.user.save")
            </button>
            <span class="result">
                <i class="fa fa-error font-yellow"></i>
            </span>
            <span class="loading"
                  style="background: url('{{ asset('/assets/img/loader.gif') }}') no-repeat;padding: 5px 20px 20px; display: none "></span>
        </div>
    </div>
</form>

