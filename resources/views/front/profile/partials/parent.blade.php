<h2>{!!$title!!}</h2>
<div class="panel-body">
    <form class="form-horizontal" role="form" method="POST"
          action="{!!URL::route('front.profile.update',['user_id'=>Auth::user()->get()->id])!!}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="page" value="{!!$page!!}">

        <div class="half fleft pr20">

            <div class="fi-span-input full clrfx">
                <span class="fi-required">@lang("profile.view.parent.fName")</span>
                <input type="text" class="fi-input" placeholder="@lang("profile.view.parent.fName")"
                       name="parent_first_name" value="{{isset($data->parent_first_name)?$data->parent_first_name:''}}"
                       required="">
            </div>

            <div class="fi-span-input full clrfx">
                <span class="fi-required">@lang("profile.view.parent.lName")</span>
                <input type="text" class="fi-input" placeholder="@lang("profile.view.parent.lName")"
                       name="parent_last_name" value="{{isset($data->parent_last_name)?$data->parent_last_name:''}}"
                       required="">
            </div>

            <div class="fi-span-input full clrfx">
                <span class="fi-required">@lang("profile.view.parent.email")</span>
                <input type="text" class="fi-input" placeholder="@lang("profile.view.parent.email")" name="parent_email"
                       value="{{isset($data->parent_email)?$data->parent_email:''}}" required="">
            </div>

            <div class="fi-span-input full clrfx">
                <span class="fi-required">@lang("profile.view.parent.Mobile")</span>
                {!!Form::text('parent_mobile',isset($data->parent_mobile)?$data->parent_mobile:'',array('class'=>'fi-input','placeholder'=>trans("profile.view.parent.Mobile"),'required'=>''))!!}
            </div>

        </div>

        <div class="half fright pr20">

            <div class="fi-span-input full clrfx">
                <span class="fi-required">@lang("profile.view.parent.SameTenant")</span>
                {!!Form::select  ('parent_tenant_address', trans("profile.view.parent.parentTenant"),isset($data->parent_tenant_address)?$data->parent_tenant_address:''
                ,['class'=>'btn form-control input-small select2me fi-input','id'=>'gender','required'=>''])!!}
            </div>

            <div class="fi-span-input full clrfx">
                <span class="fi-required">@lang("profile.view.parent.Address")</span>
                {!!Form::text('parent_address',isset($data->parent_address)?$data->parent_address:'',array('class'=>'fi-input','placeholder'=>trans("profile.view.parent.Address"),'required'=>''))!!}
            </div>

            <div class="fi-span-input full clrfx">
                <span class="fi-required">@lang("profile.view.parent.CodePostal")</span>
                {!!Form::text('parent_postal_code',isset($data->parent_postal_code)?$data->parent_postal_code:'',array('class'=>'fi-input','placeholder'=>trans("profile.view.parent.CodePostal"),'required'=>''))!!}
            </div>

            <div class="fi-span-input full clrfx">
                <span class="fi-required">@lang("profile.view.parent.Country")</span>
                {!!Form::select  ('parent_country_id', $dataCountries,isset($data->parent_country_id)?$data->parent_country_id:'',['class'=>'btn form-control input-small select2me fi-input','id'=>'country','required'=>''])!!}
            </div>

        </div>

        <div class="fi-postBtn-right">
            <input type="submit" value="@lang("profile.view.parent.save")" name="submit"
                   class="fi-postBtn" id="btnSave" style="width: 100%">
            <span class="result">
                <i class="fa fa-error font-yellow"></i>
            </span>
            <span class="loading"
                  style="background: url('{{ asset('/assets/img/loader.gif') }}') no-repeat;padding: 5px 20px 20px; display: none "></span>
        </div>

    </form>
</div>
