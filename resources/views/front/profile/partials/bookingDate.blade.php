<h2>{!!$title!!}</h2>
<form class="form-horizontal" role="form" method="POST"
      action="{!!URL::route('front.profile.update',['booking_id'=>isset($data->id)?$data->id:0])!!}">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="page" value="{!!$page!!}">

    <div class="clearfix booking-list">
        <h4 class="block">@lang("profile.view.bookingDate.ListReservation")</h4>
        @if(isset($otherData['dataBookingList']) && $otherData['dataBookingList'])
            <?php $counter = 0; ?>
            @foreach($otherData['dataBookingList'] as $item)
                <?php $counter++; ?>
                <a href="{!!URL::route($locale.'front.booking.destroy',['id'=>$item->booking_details_id])!!}"
                   class="btn btn-xs purple deleteBooking" data-id="{!!$item->id!!}">
                    <i class="fa fa-times"></i> {!!$item->title!!}
                    <span style="font-size: 8px">
                    (@lang("apartment.view.info.ClickForRemove"))
                </span>
                    <span class="loading"
                          style="background: url('{{ asset('/assets/img/loader.gif') }}') no-repeat;padding: 5px 20px 20px; display: none "></span>
                </a>
            @endforeach
            <a href="{!!URL::route($locale.'apartment.index')!!}" class="btn btn-xs yellow">
                <i class="fa fa-link"></i> @lang("profile.view.bookingDate.PleaseSelectRoom")
                (@lang("profile.view.bookingDate.Selected") {!!$counter.' '.trans("profile.view.bookingDate.from").' 3 + 1 '!!}
                )
                <span class="loading"
                      style="background: url('{{ asset('/assets/img/loader.gif') }}') no-repeat;padding: 5px 20px 20px; display: none "></span>
            </a>
        @else
            <a href="{!!URL::route($locale.'apartment.index')!!}" class="btn btn-xs yellow">
                <i class="fa fa-link"></i> @lang("profile.view.bookingDate.PleaseSelectRoom")
                (@lang("profile.view.bookingDate.Selected") 0 @lang("profile.view.bookingDate.from") 3 + 1 )
                <span class="loading"
                      style="background: url('{{ asset('/assets/img/loader.gif') }}') no-repeat;padding: 5px 20px 20px; display: none "></span>
            </a>
        @endif
    </div>

    <div class="form-group">
        <label class="control-label col-md-4">@lang("profile.view.bookingDate.fromDate")</label>

        <div class="input-group col-md-3">
            <span class="input-group-addon">
            <i class="fa fa-user"></i>
            </span>

            <div class="input-group input-small date date-picker" data-date-format="yyyy-mm-dd"
                 data-date-start-date="+0d">
                {!!Form::text('from_date',isset($data->from_date)?$data->from_date:'0000-00-00',array('class'=>'form-control','size'=>16,'required'=>''))!!}
                <span class="input-group-btn">
                <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                </span>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-md-4">@lang("profile.view.bookingDate.toDate")</label>

        <div class="input-group col-md-3">
            <span class="input-group-addon">
            <i class="fa fa-user"></i>
            </span>

            <div class="input-group input-small date date-picker" data-date-format="yyyy-mm-dd"
                 data-date-start-date="+1d">
                {!!Form::text('to_date',isset($data->to_date)?$data->to_date:'0000-00-00',array('class'=>'form-control','size'=>16,'required'=>''))!!}
                <span class="input-group-btn">
                <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                </span>
            </div>
        </div>
    </div>


    <div class="form-group">
        <div class="col-md-6 col-md-offset-4">
            <button type="submit" class="btn btn-primary" id="btnSave">
                @lang("profile.view.user.save")
            </button>
            <span class="result">
                <i class="fa fa-error font-yellow"></i>
            </span>
            <span class="loading"
                  style="background: url('{{ asset('/assets/img/loader.gif') }}') no-repeat;padding: 5px 20px 20px; display: none "></span>
        </div>
    </div>
</form>
<script>
    handleDatePickers();
</script>
