<h2>{!!$title!!}</h2>
<form id="formDocuments" class="form-horizontal" role="form" method="POST"
      action="{!!URL::route($locale.'front.profile.update',['user_id'=>Auth::user()->get()->id])!!}">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="page" value="{!!$page!!}">
    <input type="hidden" name="deleteUrl" id="deleteUrl" value="{!!URL::route('uploadFile.delete')!!}">
    <input type="hidden" name="lastDocument" id="lastDocument" value="{!!$otherData['lastItem']!!}">

    <div class="full fleft pl20" style="width: 100%;">
        @if(isset($data))
            <?php $counter = 0; ?>
            @foreach($data as $item)
                <?php $counter++; ?>

                <div class="fi-span-input full clrfx form-group" id="form-uploader-{!!$item->id!!}" style="margin-bottom: 1px !important
                                                                                ; width: 45%; float: left;">
                    <label class="col-md-2 control-label"></label>

                    <div class="input-group col-md-10" style="width: 90% !important;">

            <span style="width: 90%">

                {{--{!!$item->title!!}--}}
                @if($item->media_id&&$item->src)
                    <p style="color: #a93033; "><b>{!!$counter.'. '.$item->title!!}</b></p>
                    <span class="link">
                        <a class="fa fa-link btn btn-primary" target="_blank" href="{!!$item->linkMedia!!}">
                            @lang("profile.view.DocUpload.Link")
                        </a>
                    <a class="fa fa-times red btn delete" data-id="{!!$item->media_id!!}">
                        @lang("profile.view.DocUpload.Delete")
                    </a>
                    <span class="loading"
                          style="background: url('{{ asset('/assets/img/loader.gif') }}') no-repeat;padding: 10px 20px 20px; margin-left: 50%; display: none"></span>
                    </span>
                    <span class="input-group-addon" style="display: none">
                    </span>
                @elseif($item->valid_document_status==4)
                    <p style="color: #a93033;"><b>{!!$counter.'. '.$item->title!!}</b></p>
                    <span class="link">
                    <a class="fa fa-link btn btn-primary" target="_blank" href="{!!$item->valid_linkMedia!!}">
                        @lang("profile.view.DocUpload.LinkIsOk")
                    </a>
                    </span>
                    <span class="input-group-addon" style="display: none">
                    </span>
                @else
                    <p style="color: #333333"><b>{!!$counter.'. '.$item->title!!}</b></p>
                    {!!
                    UploaderHelper::renderUploaderJquery($idUploader='imageUploader-'.$item->id,$idStartUploader='startUploadImage'.$item->id
                    ,$idMsgUploader='msgUploaderImage'.$item->id
                    ,$titleUpload=trans("profile.view.DocUpload.Upload"),$titleStartUpload=trans("profile.view.DocUpload.Upload")
                    ,trans("profile.view.index.UploadFilesTitle")
                    ,$urlUploader=URL::route('uploadFile'),$objUploaderName='uploadObjImg'.$item->id
                    ,$autoUpload='true',$subFolder='userDocumentUpload'
                    ,$type='images-docs',$maxFileSize=20,$itemId=$item->id,$allowedTypes='png,gif,jpg,jpeg,pdf'
                    ,$showDelete='true',$showDone='false',$fileName='myfile',$titleButtonDrag='',''
                    ,$titleSaveDesc=trans("profile.view.DocUpload.titleSaveDesc")
                    ,$controller='profile',$controllerDoc='attach'
                    ,$maxCountFileUploaded=1,$multiUpload='false'
                    )
                    !!}
                    <span class="input-group-addon" style="display: none">
                    </span>
                @endif
            </span>
                        <span style="top: 5px"></span>
                    </div>
                </div>

            @endforeach
        @endif


        <div class="fi-postBtn-right">
            <input type="submit"
                   value="{!!($otherData['lastItem']=='false')? trans("profile.view.DocUpload.saveContinue") : trans("profile.view.DocUpload.saveSend") !!}"
                   name="submit"
                   class="fi-postBtn" id="btnSaveDoc" style="width: 100%">
            <span class="result">
                <i class="fa fa-error font-yellow"></i>
            </span>
            <span class="loading"
                  style="background: url('{{ asset('/assets/img/loader.gif') }}') no-repeat;padding: 5px 20px 20px; display: none "></span>
        </div>

    </div>
</form>

{{--<script src="{{ asset('/assets/js/front/uploader.js') }}" type="text/javascript"></script>--}}
