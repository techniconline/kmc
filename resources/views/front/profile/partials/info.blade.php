<h2>{!!$title!!}</h2>
<div class="panel-body">
    <form class="form-horizontal" role="form" method="POST"
          action="{!!URL::route('front.profile.update',['user_id'=>Auth::user()->get()->id])!!}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="page" value="{!!$page!!}">

        <div class="half fleft pr20">

            <div class="fi-span-input full clrfx">
                <span class="fi-required">@lang("profile.view.info.YouAre")</span>
                {!!Form::select  ('persona', $otherData['persona'],isset($data->persona)?$data->persona:'',['class'=>'btn form-control input-small select2me fi-input','id'=>'persona','required'=>''])!!}
            </div>

            <div class="fi-span-input full clrfx">
                <span class="fi-required">@lang("profile.view.info.YourLevelEducation")</span>
                {!!Form::select  ('education', $otherData['education'],isset($data->education)?$data->education:'',['class'=>'btn form-control input-small select2me fi-input','id'=>'education','required'=>''])!!}
            </div>

            <h4>@lang("profile.view.info.YourInstitution")</h4>

            <div class="fi-span-input full clrfx">
                <span class="fi-required">@lang("profile.view.info.Type")</span>
                {!!Form::select  ('institution_type', $otherData['institution_type'],isset($data->institution_type)?$data->institution_type:'',['class'=>'btn form-control input-small select2me fi-input','id'=>'institution_type','required'=>''])!!}
            </div>

            <div class="fi-span-input full clrfx">
                <span class="fi-required">@lang("profile.view.info.FacilityName")</span>
                <input placeholder="@lang("profile.view.info.FacilityName")" type="text" class="fi-input"
                       name="institution" value="{!!isset($data->institution)?$data->institution:''!!}" required="">
            </div>

            <div class="fi-span-input full clrfx">
                <span class="fi-required">@lang("profile.view.info.PropertyTown")</span>
                <input placeholder="@lang("profile.view.info.PropertyTown")" type="text" class="fi-input"
                       name="institution_town" value="{!!isset($data->institution_town)?$data->institution_town:''!!}"
                       required="">
            </div>


        </div>

        <div class="half fright pr20">
            <h4>@lang("profile.view.info.YourAccommodationDuringYourRental")</h4>

            <div class="fi-span-input full clrfx">
                <span class="fi-required">@lang("profile.view.info.Type")</span>
                {!!Form::select  ('current_institution_type', $otherData['current_institution_type']
                ,isset($data->current_institution_type)?$data->current_institution_type:'',['class'=>'btn form-control input-small select2me fi-input','id'=>'current_institution_type','required'=>''])!!}
            </div>

            <div class="fi-span-input full clrfx">
                <span class="fi-required">@lang("profile.view.info.FacilityName")</span>
                <input placeholder="@lang("profile.view.info.FacilityName")" type="text" class="fi-input"
                       name="current_institution"
                       value="{!!isset($data->current_institution)?$data->current_institution:''!!}" required="">
            </div>

            <div class="fi-span-input full clrfx">
                <span class="fi-required">@lang("profile.view.info.PropertyTown")</span>
                <input placeholder="@lang("profile.view.info.PropertyTown")" type="text" class="fi-input"
                       name="current_institution_town"
                       value="{!!isset($data->current_institution_town)?$data->current_institution_town:''!!}"
                       required="">
            </div>

            <div class="fi-span-input full clrfx">
                <span class="fi-required">@lang("profile.view.info.DoYouHaveGuarantorForTheLease")</span>
                {!!Form::select  ('parent_guarantor', $otherData['parent_guarantor'],isset($data->parent_guarantor)?$data->parent_guarantor:'',['class'=>'btn form-control input-small select2me fi-input','id'=>'parent_guarantor','required'=>''])!!}
            </div>

            <div class="fi-span-input full clrfx">
                <span>@lang("profile.view.info.PersonalMessage")</span>
                <textarea style="width: 100%; height: 100px" class="fi-input" name="personal_message"
                          rows="3">{!!isset($data->personal_message)?$data->personal_message:''!!}</textarea>
            </div>

            <div class="fi-span-input full clrfx">
            <span style="width: 100%">@lang("profile.view.info.AccuracyInformation")
                {!!Form::checkbox ('accuracy_information',1,isset($data->accuracy_information)?$data->accuracy_information:''
            ,['class'=>'fi-input','id'=>'accuracy_information','required'=>''])!!}
            </span>

            </div>


            <div class="fi-postBtn-right">
                <input type="submit" value="@lang("profile.view.info.save")" name="submit"
                       class="fi-postBtn" id="btnSave" style="width: 100%">
            <span class="result">
                <i class="fa fa-error font-yellow"></i>
            </span>
                <span class="loading"
                      style="background: url('{{ asset('/assets/img/loader.gif') }}') no-repeat;padding: 5px 20px 20px; display: none "></span>
            </div>
        </div>

    </form>
</div>