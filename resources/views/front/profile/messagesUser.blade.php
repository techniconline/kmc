@extends('app')
@section('styles')
    <link href="{{ asset('/assets/theme/assets/global/plugins/font-awesome/css/font-awesome.min.css') }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/theme/assets/global/plugins/bootstrap-datepicker/css/datepicker3.css') }}"
          rel="stylesheet" type="text/css"/>

@endsection
@section('content')
    <section class="fullscreen" id="profileSection">
        <div class="container">
            <div class="half fleft pl20">

                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h2><i class="fa fa-comment "></i>{!!$title!!}</h2>
                                </div>
                                <div class="panel-body">
                                    {{--<span class="loading" style="background: url('{{ asset('/assets/img/loader.gif') }}') no-repeat;padding: 10px 20px 20px; margin-left: 50%; display: none"></span>--}}
                                    <div class="result">
                                        @include('front.profile.partials.messages')
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

@endsection
@section('scripts')
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/js/front/profile.js') }}"></script>

    {{--    {!!UploaderHelper::loadUploaderJquery()!!}--}}
    {{--    <script src="{{ asset('/assets/js/generalUpload.js') }}" type="text/javascript"></script>--}}

@endsection
@section('inline-scripts')

@endsection
