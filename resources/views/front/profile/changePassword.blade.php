@extends('app')
@section('styles')

    <link href="{{ asset('/assets/frontend/css/profile.css') }}"
          rel="stylesheet" type="text/css"/>

@endsection
@section('content')
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <section class="fullscreen" id="profileSection">
        <div class="container">
            <h2>@lang('profile.view.pass.Reset_Password')</h2>

            <div class="half fleft pl20">


                <form class="form-horizontal" role="form" method="POST"
                      action="{{ route('front.profile.changePassword') }}">
                    <div class="fi-span-input full clrfx">
                        <span class="fi-required" style="width: 30%">@lang('profile.view.pass.Current_Password')</span>
                        <input style="width: 70%" type="password" class="fi-input"
                               placeholder="@lang('profile.view.pass.Current_Password')" name="currentPassword"
                               value="">
                    </div>
                    <div class="fi-span-input full clrfx">
                        <span style="width: 30%" class="fi-required">@lang('profile.view.pass.New_Password')</span>
                        <input style="width: 70%" type="password" class="fi-input"
                               placeholder="@lang('profile.view.pass.New_Password')"
                               name="newPassword" value="">
                    </div>

                    {{--<div class="form-group">--}}
                    {{--<label class="col-md-4 control-label">@lang('profile.view.pass.Confirm_Password')</label>--}}
                    {{--<div class="col-md-6">--}}
                    {{--<input type="confirmPassword" class="form-control" name="confirmPassword" value="">--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    <div class="fi-postBtn-right">
                        <input type="submit" value="@lang('profile.view.pass.Save_Password')" name="submit"
                               class="fi-postBtn" style="width: 100%">
                    </div>

                </form>
            </div>

        </div>
    </section>
@endsection
