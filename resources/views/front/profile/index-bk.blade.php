@extends('app')
@section('styles')
    <link href="{{ asset('/assets/theme/assets/global/plugins/font-awesome/css/font-awesome.min.css') }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/theme/assets/global/plugins/bootstrap-datepicker/css/datepicker3.css') }}"
          rel="stylesheet" type="text/css"/>

@endsection
@section('content')
    <section class="fullscreen" id="profileSection">
        <div class="container">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                {{--<label for="title" class="title">@lang("profile.view.index.Profile")</label>--}}
                                <label for="bookingDate">
                                    <a href="{!!URL::route($locale.'front.profile.show',['bookingDate'])!!}"
                                       class="icon-btn">
                                        <i class="fa fa-calendar"></i>

                                        <div>
                                            @lang("profile.view.index.bookingDate")
                                        </div>
                        <span class="badge badge-danger">
                        <i {!!\App\Profile::checkCompleted(Auth::user()->get()->id,'bookingDate')?'class="fa fa-check font-green"':'class="fa fa-warning font-yellow"'!!}></i>
                        </span>
                                    </a>
                                </label>
                                <label for="user">
                                    <a href="{!!URL::route($locale.'front.profile.show',['user'])!!}" class="icon-btn">
                                        <i class="fa fa-user"></i>

                                        <div>
                                            @lang("profile.view.index.user")
                                        </div>
                        <span class="badge badge-danger">
                        <i {!!\App\Profile::checkCompleted(Auth::user()->get()->id,'user')?'class="fa fa-check font-green"':'class="fa fa-warning font-yellow"'!!}></i>
                        </span>
                                    </a>
                                </label>

                                <label for="info">
                                    <a href="{!!URL::route($locale.'front.profile.show',['info'])!!}" class="icon-btn">
                                        <i class="fa fa-info"></i>

                                        <div>
                                            @lang("profile.view.index.info")
                                        </div>
                        <span class="badge badge-danger">
                        <i {!!\App\Profile::checkCompleted(Auth::user()->get()->id,'info')?'class="fa fa-check font-green"':'class="fa fa-warning font-yellow"'!!}></i>
                        </span>
                                    </a>
                                </label>


                                @if(isset($age) && $age<18)
                                    <label for="parent">
                                        <a href="{!!URL::route($locale.'front.profile.show',['parent'])!!}"
                                           class="icon-btn">
                                            <i class="fa fa-users"></i>

                                            <div>
                                                @lang("profile.view.index.parent")
                                            </div>
                        <span class="badge badge-danger">
                        <i {!!\App\Profile::checkCompleted(Auth::user()->get()->id,'parent')?'class="fa fa-check font-green"':'class="fa fa-warning font-yellow"'!!}></i>
                        </span>
                                        </a>
                                    </label>
                                @endif
                                @if(isset($dataGroup))
                                    @foreach($dataGroup as $item)

                                        <label data-id="{!!$item->id!!}" for="userDocUpload{!!$item->id!!}">
                                            <a href="{!!URL::route($locale.'front.profile.show',['userDocUpload'])!!}"
                                               class="icon-btn">
                                                <i class="fa fa-file-image-o"></i>

                                                <div>
                                                    {!!$item->title!!}
                                                </div>
                            <span class="badge badge-danger">
                            <i {!! isset($item->completed_group)&& $item->completed_group ? 'class="fa fa-check font-green"':'class="fa fa-warning font-yellow"' !!}></i>
                            </span>
                                            </a>
                                        </label>

                                    @endforeach
                                @endif
                            </div>
                            <div class="panel-body">
                                <span class="loading"
                                      style="background: url('{{ asset('/assets/img/loader.gif') }}') no-repeat;padding: 10px 20px 20px; margin-left: 50%; display: none"></span>

                                <div class="result"></div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/js/front/profile.js') }}"></script>

    {!!UploaderHelper::loadUploaderJquery()!!}
    {{--    <script src="{{ asset('/assets/js/generalUpload.js') }}" type="text/javascript"></script>--}}

@endsection
@section('inline-scripts')

@endsection
