@extends('app')
<?php $langDetails = app('activeLangDetails'); ?>

@section('styles')
    {{--<link type="text/css" rel="stylesheet" href="{{ asset('/assets/theme/assets/global/plugins/bootstrap-datepicker/css/datepicker.css') }}" />--}}
    <link type="text/css" rel="stylesheet"
          href="{{ asset('/assets/persian-datepicker/css/persianDatepicker-default.css') }}"/>
    <!-- **Font Awesome** -->
    {{--<link rel="stylesheet" href="{{ asset('/assets/frontend/css/font-awesome.min.css') }}" type="text/css"/>--}}

@endsection

@section('fake')
    <header>
        <div class="breadcrumbs-container smocky-white-bg">
            <div class="container">
                <ul class="breadcrumb">
                    @if (Auth::user()->guest())
                        <li><a href="{{ url('/front/auth/login') }}">@lang("home.view.index.Login")
                                / @lang("home.view.index.Register")</a></li>
                    @else
                        <li><a title="{{ Auth::user()->get()->firstname.' '.Auth::user()->get()->lastname }}"
                               href="{{ url('/front/profile/avatar') }}">@lang("backend/User.view.form.Avatar")</a>
                        </li>
                        <li><a href="{{ route('front.profile.password') }}">@lang("home.view.index.Change_Password")</a>
                        </li>
                        <li><a href="{{ url('/front/profile/messages') }}">@lang("home.view.index.YourMessage")
                                (@lang("home.view.index.New") {!!\App\Profile::checkMessagesUser(Auth::user()->get()->id)!!}
                                )</a>
                        </li>
                        <li><a href="{{ url('/front/auth/logout') }}">@lang("home.view.index.Logout")</a>
                        </li>
                    @endif
                </ul>
            </div>
            <!-- /container -->
        </div>
        <!-- /breadcrumbs-container -->
    </header>

@endsection
@section('content')

    <div class="parallax full-width-bg">
        <div class="container">
            <div class="main-title">
                <h1> Register </h1>

                <div class="breadcrumb">
                    <a href="index.html"> Home </a>
                    <span class="fa fa-angle-right"></span>
                    <a href="tabs-accordions.html"> Pages </a>
                    <span class="fa fa-angle-right"></span>
                    <span class="current"> Register </span>
                </div>
            </div>
        </div>
    </div>
    <div class="dt-sc-margin100"></div>
    <!-- Primary Starts -->
    <section id="primary" class="content-full-width">
        <div class="full-width-section">
            <div class="container">
                <div class="page_info">
                    <h3 class="aligncenter"><span> <i class="fa fa-user"></i></span>
                        @lang("profile.view.index.Profile") </h3>
                </div>
                <div class="dt-sc-margin20"></div>
            </div>
        </div>
        <div class="full-width-section parallax full-section-bg">
            <div class="container">
                <div class="dt-sc-clear"></div>
                <div class="form-wrapper register">

                    @include('front.profile.partials.user')


                            <!-- tabs -->

                    <div class="dt-sc-tabs-vertical-container type2">
                        @if(isset($pages)&&$pages)
                                <!-- **dt-sc-tabs-vertical-frame - Starts** -->
                        <ul class="dt-sc-tabs-vertical-frame">
                            <?php $count = 0; ?>
                            @foreach($pages as $itemP)
                                <li class="{{!$count?'active':''}}"><a data-toggle="tab"
                                                                       href="#tab_{{$count}}">{!! $itemP['title'] !!}</a>
                                </li>
                                <?php $count++; ?>
                            @endforeach
                        </ul>
                        <!-- **dt-sc-tabs-vertical-frame - Ends** -->

                        <!-- **dt-sc-tabs-vertical-frame-content - Starts** -->
                        <?php $count = 0; ?>
                        @foreach($pages as $itemP)
                            <div class="dt-sc-tabs-vertical-frame-content" id="tab_{{$count}}">
                                {!! $itemP['page'] !!}
                            </div>
                            <?php $count++; ?>
                            @endforeach
                                    <!-- **dt-sc-tabs-vertical-frame-content - Ends** -->
                            @endif
                    </div>
                    <!-- **dt-sc-tabs-vertical-container - Ends** -->


                    <!-- /tabs -->
                </div>
            </div>
        </div>
    </section>







@endsection
@section('scripts')
    {{--<script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>--}}
    {{--    <script src="{{ asset('/assets/persian-datepicker/js/persianDatepicker.js') }}"></script>--}}
    <script src="{{ asset('/assets/js/front/profile.js') }}"></script>

@endsection
@section('inline-scripts')

    {{--<script>--}}
    {{--//        jQuery.noConflict();--}}
    {{--//        jQuery(document).ready(function($){--}}

    {{--$("#input-date-birth").persianDatepicker(--}}
    {{--{--}}
    {{--fontSize: 11, // by px--}}
    {{--formatDate: "YYYY-0M-0D",--}}
    {{--startDate: "1310/01/01",--}}
    {{--endDate: "1379/12/30"--}}
    {{--}--}}
    {{--);--}}
    {{--//        });--}}
    {{--</script>--}}


@endsection
