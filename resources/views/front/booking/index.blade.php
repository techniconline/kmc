@extends('layouts.default')

@section('content')
    <h3>
        {{link_to('', 'Home')}}
    </h3>
    {{Form::open(['route' => 'booking.store'])}}
    <div>
        {{Form::label('apartment_id', 'Apartment:')}}
        {{Form::select('apartment_id', $dataApartment,'')}}
    </div>
    <div>
        {{Form::submit('Add Category Details')}}
    </div>
    {{Form::close()}}
@stop
@section('scripts')
    {{ HTML::script('assets/js/backend/category.js')}}
@stop
