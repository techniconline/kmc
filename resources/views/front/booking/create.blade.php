@extends('layouts.default')

@section('content')
    <h1>Create User</h1>
    {{Form::open(['route' => 'users.store'])}}
    <div>
        {{Form::label('name', 'Name:')}}
        {{Form::text('name')}}
        {{$errors->first('name','<span>:message</span>')}}
    </div>
    <div>
        {{Form::label('username', 'User Name:')}}
        {{Form::text('username')}}
        {{$errors->first('username','<span>:message</span>')}}
    </div>
    <div>
        {{Form::label('password', 'Password:')}}
        {{Form::password('password')}}
        {{$errors->first('password','<span>:message</span>')}}
    </div>
    <div>
        {{Form::label('email', 'Email:')}}
        {{Form::email('email')}}
        {{$errors->first('email','<span>:message</span>')}}
    </div>
    <div>
        {{Form::label('gender', 'Gender:')}}
        {{Form::select('gender',array('male'=>'male','female'=>'female'))}}
        {{$errors->first('gender','<span>:message</span>')}}
    </div>
    <div>
        {{Form::label('mobile', 'Cell phone:')}}
        {{Form::text('mobile')}}
        {{$errors->first('mobile','<span>:message</span>')}}
    </div>
    <div>
        {{Form::label('tel', 'Tel:')}}
        {{Form::text('tel')}}
        {{$errors->first('tel','<span>:message</span>')}}
    </div>
    <div>
        {{Form::submit('Create User!')}}
    </div>
    {{Form::close()}}



@stop


