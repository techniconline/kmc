<!DOCTYPE HTML>
<html>
<head>
    <!-- -->
    <title>Studenest - @yield('browser_title')</title>
    <meta http-equiv="Cache-control" content="no-cache">
    <meta http-equiv="Expires" content="-1">
    <meta name="keywords" content="Studenest,Campus,Rental,Dorm,Room,France,Paris,Résidence,étudiants">
    <meta name="description" content="Résidence pour étudiants">


    <meta name="author" content="Made with passion for pretty things.">
    <meta name="language" content="fr_FR">
    <meta name="documentcountrycode" content="fr">
    <meta name="generator" content="ProCEED 2.6.1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="initial-scale=1">
    {!! ParamsHelper::metaTag() !!}
    {!! ParamsHelper::favIcon() !!}

    <link href="{{ asset('/css/app.css') }}" rel="stylesheet">

    <link href="{{ asset('/assets/frontend/css/styleATG2.13.0.css') }}" rel="stylesheet">
    <link href="{{ asset('/assets/frontend/css/styleLayout0.2.36.css') }}" rel="stylesheet">

    <link href="{{ asset('/assets/theme/assets/global/css/components.css') }}" rel="stylesheet">
    <link href="{{ asset('/assets/theme/assets/global/plugins/simple-line-icons/simple-line-icons.min.css') }}"
          rel="stylesheet">
    @yield('styles')

            <!-- Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Pacifico|Roboto:400italic,300,700,400|Roboto+Condensed:400,700'
          rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    {{--<script src="{{ asset('/assets/theme/assets/global/plugins/fancybox/source/jquery.fancybox.js') }}"></script>--}}

    <!--[if lt IE 9]>
    <script src="{{ asset('/assets/js/front/plugin/html5shiv.min.js') }}"></script>
    <script src="{{ asset('/assets/js/front/plugin/respond.min.js') }}"></script>
    <![endif]-->

    <script src="{{ asset('/assets/js/front/plugin/jquery.min.js') }}"></script>
    {{--<script src="{{ asset('/assets/frontend/js/JSLib-2.2.0.js') }}"></script>--}}
    <script src="{{ asset('/assets/frontend/js/JSPage0.2.17.js') }}"></script>
    <script src="{{ asset('/assets/js/front/plugin/bootstrap.min.js') }}"></script>


    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                        (i[r].q = i[r].q || []).push(arguments)
                    }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-2235235-16', 'auto');
        ga('send', 'pageview');

    </script>
</head>

<body>

<div class="authOptions">
    @if (Auth::user()->guest())
        <a class="button" href="{{ url('/front/auth/login') }}">@lang("home.view.index.Login")
            / @lang("home.view.index.Register")</a>
        {{--        <a class="button" href="{{ url('/front/auth/register') }}">@lang("home.view.index.Register")</a>--}}
    @else
        {{--<a class="dropdown-toggle" data-toggle="dropdown" role="button"--}}
        {{--aria-expanded="false">--}}
        {{--<span class="caret"></span></a>--}}
        {{--<ul class="dropdown-menu" role="menu">--}}
        <a title="{{ Auth::user()->get()->firstname.' '.Auth::user()->get()->lastname }}"
           class="button" href="{{ url('/front/profile') }}">@lang("home.view.index.Profile")</a>
        {{--<li>--}}
        {{--<a href="{{ route('front.profile.password') }}">@lang("home.view.index.Change_Password")</a>--}}
        {{--</li>--}}
        {{--<li><a href="{{ url('/front/profile/messages') }}">@lang("home.view.index.YourMessage")--}}
        {{--(@lang("home.view.index.New") {!!\App\Profile::checkMessagesUser(Auth::user()->get()->id)!!}--}}
        {{--)</a></li>--}}
        <a class="button" href="{{ url('/front/auth/logout') }}">@lang("home.view.index.Logout")</a>
        {{--</ul>--}}
    @endif
    {{--<a class="button" href="{{ url('/front/profile') }}">@lang("home.view.index.Profile")</a>--}}

</div>

<nav id="mainMenu">
    <a class="selectlanguage"
       href="/{!!strtolower(trans("home.view.index.SelectLang"))!!}">@lang("home.view.index.SelectLang")</a>
    <a href="/" class="MainLogo"></a>

    {!! MenusHelper::frontEndMenusForHomeBuildWithOutLi($frontend_menus,$contents,'item hvr-underline-from-left',true) !!}

    <footer>
        <a href="http://sodevim.com" target="_blank" id="sodevim"></a><br>
        <a href="/terms.html">@lang("home.view.index.TitlePageTerms")</a>
        @lang("home.view.index.CopyRight")
    </footer>
</nav>

<section class="fullscreen" id="appSection">

    @yield('content')

</section>


<!-- Scripts -->

@yield('scripts')
@yield('inline-scripts')

</body>
</html>

