@extends('app')
@section('styles')
        <!--link id="layer-slider" rel="stylesheet" href="{{ asset('/assets/frontend/css/layerslider.css') }}" media="all"/>


@section('content')
@if($dataProduct)

        <!-- **Main - Starts** -->

<div class="parallax full-width-bg">
    <div class="container">
        <div class="main-title">
            <h1>{!!$dataProduct->title!!}</h1>

            <div class="breadcrumb">
                <a href="index.html">@lang("home.view.index.HomePage")</a>
                <span class="fa fa-angle-right"></span>
                <a href="portfolio-4-column-with-space.html">Portfolio </a>
                <span class="fa fa-angle-right"></span>
                <span class="current">{!!$dataProduct->title!!}</span>
            </div>
        </div>
    </div>
</div>


<!-- Container starts-->
<div class="container">
    <div id="home">
        <!-- **banner - Starts** -->
        <div class="banner">
            @if(isset($dataProduct) && ($dataProduct->image) && \Illuminate\Support\Facades\File::exists(base_path().env('PUBLIC_PATH_URL', 'public').$dataProduct->image))
                <img class="" src="{!! asset($dataProduct->image) !!}" alt="Image"/>
            @else
                <img src="http://www.placehold.it/1920x895&text=Banner" alt="banner"/>
            @endif
        </div><!-- **banner - Ends** -->
    </div>
    <!-- Primary Starts -->
    <section id="primary" class="content-full-width header-mean-wrapper">
        <div class="dt-sc-margin50"></div>
        <article class="content">

            <div class="column dt-sc-one-third first">
                <!-- **recent-gallery-container - Starts** -->
                <div class="recent-gallery-container">
                    <!-- **recent-gallery - Starts** -->
                    <img src="http://placehold.it/1200x800&text=Gallery" alt="image"/>
                </div>
                <!-- **recent-gallery-container - Ends** -->
            </div>

            <div class="column dt-sc-two-third">
                <h3>{!!$dataProduct->title!!}</h3>
                <h4>{!!$dataProduct->title!!}</h4>

                <p></p>

                <div class="dt-sc-hr-invisible-very-small"></div>
                <!-- **project-details - Starts** -->
                <div class="project-details">

                    <ul class="client-details">
                        <li>
                            <p><span>@lang("product.view.index.Status")</span> 26 Dec 2013</p>
                        </li>
                        <li>
                            <p><span>@lang("product.view.index.Garranty")</span> Stephen Kinglard</p>
                        </li>
                        <li>
                            <p><span>@lang("product.view.index.Manufacturer")</span> Photoshop, Lightroom</p>
                        </li>
                    </ul>
                    <div class="dt-sc-hr-invisible-very-small"></div>
                    <h6>@lang("product.view.index.Share")</h6>
                    <!-- **dt-sc-social-icons - Starts** -->
                    <ul class="dt-sc-social-icons">
                        <li><a href="#" title="twitter"> <span class="fa fa-twitter"></span> </a></li>
                        <li><a href="#" title="facebook"> <span class="fa fa-facebook"></span> </a></li>
                        <li><a href="#" title="google"> <span class="fa fa-google"></span> </a></li>
                        <li><a href="#" title="linkedin"> <span class="fa fa-linkedin"></span> </a></li>
                    </ul>
                    <!-- **dt-sc-social-icons - Ends** -->
                </div>
                <!-- **project-details - Ends** -->
            </div>


            <div class="dt-sc-margin50"></div>
            <!-- **post-nav-container - Starts** -->
            <div id="menu-container" class="post-nav-container">
                <nav id="main-menu">
                    <ul class="group">
                        <li class="current_page_item"><a href="#home">Services</a></li>
                        <li><a href="#team">Team</a></li>
                        <li><a href="#portfolio">Portfolio</a></li>
                        <li><a href="#blog">Blog</a></li>
                        <li><a href="#contacts">Contact</a></li>
                    </ul>
                </nav>
            </div>
            <!-- **post-nav-container - Ends** -->
            <div class="dt-sc-margin50"></div>
            <div id="spec" class="full-width-section">
                <div class="container">
                    <h6>@lang("product.view.index.Specifications")</h6>

                    <p>{!!$dataProduct->short_description!!}</p>
                </div>
            </div>


            <div class="dt-sc-margin50"></div>
            <div class="dt-sc-hr-invisible"></div>

        </article>
    </section>

    <div id="main">
        <section id="team">
            <div class="full-width-section">
                <div class="dt-sc-margin65"></div>
                <div class="container">
                    <h2 class="aligncenter">Our Team</h2>

                    <p class="middle-align">Contrary to popular belief, Lorem Ipsum is not simply random text. It has
                        roots in a piece of classical<br/> Latin literature from 45 BC, making it over 2000 years old.
                    </p>

                    <div class="dt-sc-hr-invisible-small"></div>

                    <div class="column dt-sc-one-fourth first">
                        <!-- **dt-sc-team - Starts** -->
                        <div class="dt-sc-team">
                            <div class="image"><img src="http://placehold.it/160x160&text=Team" alt="iamge"/></div>
                            <!-- **team-details - Starts** -->
                            <div class="team-details">
                                <h6><a href="#">James Dunlop</a></h6>

                                <p>Head of the Department</p>
                            </div> <!-- **team-details - Ends** -->
                            <!-- **dt-sc-social-icons - Starts** -->
                            <ul class="dt-sc-social-icons">
                                <li><a href="#" title=""> <span class="fa fa-twitter"></span> </a></li>
                                <li><a href="#" title=""> <span class="fa fa-facebook"></span> </a></li>
                                <li><a href="#" title=""> <span class="fa fa-behance"></span> </a></li>
                                <li><a href="#" title=""> <span class="fa fa-dribbble"></span> </a></li>
                                <li><a href="#" title=""> <span class="fa fa-linkedin"></span> </a></li>
                            </ul> <!-- **dt-sc-social-icons - Ends** -->
                        </div><!-- **dt-sc-team - Ends** -->
                    </div> <!-- **column - Ends** -->
                    <!-- **column - Starts** -->
                    <div class="column dt-sc-one-fourth">
                        <!-- **dt-sc-team - Starts** -->
                        <div class="dt-sc-team">
                            <div class="image"><img src="http://placehold.it/160x160&text=Team" alt="iamge"/></div>
                            <!-- **team-details - Starts** -->
                            <div class="team-details">
                                <h6><a href="#">Mick Lousie</a></h6>

                                <p>Head of the Department</p>
                            </div> <!-- **team-details - Ends** -->
                            <!-- **dt-sc-social-icons - Starts** -->
                            <ul class="dt-sc-social-icons">
                                <li><a href="#" title="twitter"> <span class="fa fa-twitter"></span> </a></li>
                                <li><a href="#" title="facebook"> <span class="fa fa-facebook"></span> </a></li>
                                <li><a href="#" title="behance"> <span class="fa fa-behance"></span> </a></li>
                                <li><a href="#" title="dribbble"> <span class="fa fa-dribbble"></span> </a></li>
                                <li><a href="#" title="linkedin"> <span class="fa fa-linkedin"></span> </a></li>
                            </ul> <!-- **dt-sc-social-icons - Ends** -->
                        </div><!-- **dt-sc-team - Ends** -->
                    </div> <!-- **column - Ends** -->
                    <!-- **column - Starts** -->
                    <div class="column dt-sc-one-fourth">
                        <!-- **dt-sc-team - Starts** -->
                        <div class="dt-sc-team">
                            <div class="image"><img src="http://placehold.it/160x160&text=Team" alt="iamge"/></div>
                            <!-- **team-details - Starts** -->
                            <div class="team-details">
                                <h6><a href="#">Jessie Alan</a></h6>

                                <p>Head of the Department</p>
                            </div> <!-- **team-details - Ends** -->
                            <!-- **dt-sc-social-icons - Starts** -->
                            <ul class="dt-sc-social-icons">
                                <li><a href="#" title="twitter"> <span class="fa fa-twitter"></span> </a></li>
                                <li><a href="#" title="facebook"> <span class="fa fa-facebook"></span> </a></li>
                                <li><a href="#" title="behance"> <span class="fa fa-behance"></span> </a></li>
                                <li><a href="#" title="dribbble"> <span class="fa fa-dribbble"></span> </a></li>
                                <li><a href="#" title="linkedin"> <span class="fa fa-linkedin"></span> </a></li>
                            </ul> <!-- **dt-sc-social-icons - Ends** -->
                        </div><!-- **dt-sc-team - Ends** -->
                    </div> <!-- **column - Ends** -->
                    <!-- **column - Starts** -->
                    <div class="column dt-sc-one-fourth">
                        <!-- **dt-sc-team - Starts** -->
                        <div class="dt-sc-team">
                            <div class="image"><img src="http://placehold.it/160x160&text=Team" alt="iamge"/></div>
                            <!-- **team-details - Starts** -->
                            <div class="team-details">
                                <h6><a href="#">Karen Smith</a></h6>

                                <p>Head of the Department</p>
                            </div> <!-- **team-details - Ends** -->
                            <!-- **dt-sc-social-icons - Starts** -->
                            <ul class="dt-sc-social-icons">
                                <li><a href="#" title="twitter"> <span class="fa fa-twitter"></span> </a></li>
                                <li><a href="#" title="facebook"> <span class="fa fa-facebook"></span> </a></li>
                                <li><a href="#" title="behance"> <span class="fa fa-behance"></span> </a></li>
                                <li><a href="#" title="dribbble"> <span class="fa fa-dribbble"></span> </a></li>
                                <li><a href="#" title="linkedin"> <span class="fa fa-linkedin"></span> </a></li>
                            </ul> <!-- **dt-sc-social-icons - Ends** -->
                        </div><!-- **dt-sc-team - Ends** -->
                    </div>
                </div>
                <div class="dt-sc-margin30"></div>
                <div class="hr-line"></div>
            </div>
            <div class="full-width-section">
                <div class="container">
                    <div class="dt-sc-hr-invisible"></div>
                    <div class="column dt-sc-one-column">
                        <h3 class="aligncenter">Frequently Asking Questions</h3>

                        <div class="dt-sc-margin20"></div>
                        <div class="dt-sc-toggle-frame-set-container">
                            <!-- **dt-sc-toggle-frame-set - Starts** -->
                            <div class="dt-sc-toggle-frame-set">
                                <h5 class="dt-sc-toggle-accordion active"><a href="#">How to customize the templates in
                                        order</a></h5>

                                <div class="dt-sc-toggle-content">
                                    <div class="block">
                                        Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots
                                        in a piece of classical Latin literature from 45 BC, making it over 2000 years
                                        old this is the larger one. Phasellus viverra nulla ut metus varius laoreet.
                                        Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur
                                        ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus.Contrary to popular
                                        belief, Lorem Ipsum is not simply random text. It has roots in a piece of
                                        classical Latin literature from 45 BC, making it over 2000 years old this is the
                                        larger one. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum.
                                        Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper
                                        ultricies nisi. Nam eget dui. Etiam rhoncus.
                                    </div>
                                </div>
                                <h5 class="dt-sc-toggle-accordion"><a href="#">How to make boxed width in Priority
                                        Template</a></h5>

                                <div class="dt-sc-toggle-content">
                                    <div class="block">
                                        Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots
                                        in a piece of classical Latin literature from 45 BC, making it over 2000 years
                                        old this is the larger one. Phasellus viverra nulla ut metus varius laoreet.
                                        Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur
                                        ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus.Contrary to popular
                                        belief, Lorem Ipsum is not simply random text. It has roots in a piece of
                                        classical Latin literature from 45 BC, making it over 2000 years old this is the
                                        larger one. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum.
                                        Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper
                                        ultricies nisi. Nam eget dui. Etiam rhoncus.
                                    </div>
                                </div>
                                <h5 class="dt-sc-toggle-accordion"><a href="#">Choose skins and dark skins in HTML
                                        Template</a></h5>

                                <div class="dt-sc-toggle-content">
                                    <div class="block">
                                        Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots
                                        in a piece of classical Latin literature from 45 BC, making it over 2000 years
                                        old this is the larger one. Phasellus viverra nulla ut metus varius laoreet.
                                        Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur
                                        ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus.Contrary to popular
                                        belief, Lorem Ipsum is not simply random text. It has roots in a piece of
                                        classical Latin literature from 45 BC, making it over 2000 years old this is the
                                        larger one. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum.
                                        Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper
                                        ultricies nisi. Nam eget dui. Etiam rhoncus.
                                    </div>
                                </div>
                            </div> <!-- **dt-sc-toggle-frame-set - Ends** -->
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="portfolio">
            <!-- **Full-width-section - Starts** -->
            <div class="full-width-section">
                <div class="dt-sc-margin65"></div>
                <div class="container">
                    <h2 class="aligncenter">Our Portfolio</h2>

                    <p class="middle-align">Contrary to popular belief, Lorem Ipsum is not simply random text. It has
                        roots in a piece of classical<br/> Latin literature from 45 BC, making it over 2000 years old.
                    </p>

                    <div class="dt-sc-hr-invisible-small"></div>
                    <div class="sorting-container">
                        <a href="#" data-filter=".all-sort" class="active-sort">All</a>
                        <a href="#" data-filter=".photography-sort">Photography</a>
                        <a href="#" data-filter=".outdoors-sort">Outdoors</a>
                        <a href="#" data-filter=".fashion-sort">Fashion</a>
                        <a href="#" data-filter=".graphic-sort">Graphic Design</a>
                    </div>
                    <div class="dt-sc-hr-invisible-small"></div>
                </div>
                <!-- **portfolio-container - Starts** -->
                <div class="portfolio-container no-space">
                    <div class="portfolio dt-sc-one-fifth no-space column all-sort outdoors-sort fashion-sort">
                        <!-- **portfolio-thumb - Starts** -->
                        <div class="portfolio-thumb">
                            <figure>
                                <img src="http://placehold.it/1200x800&text=Gallery" alt="image"/>

                                <div class="image-overlay">
                                    <a class="zoom" href="http://placehold.it/1200x800&text=Gallery"
                                       data-gal="prettyPhoto[gallery]"><span class="fa fa-search"></span></a>
                                    <a class="link" href="portfolio-detail-v2.html"><span class="fa fa-link"></span></a>

                                    <div class="portfolio-content">
                                        <h5><a href="portfolio-detail-v2.html"> Dandelion Falls </a></h5>
                                        <span class="fa fa-sort-up"></span>
                                    </div>
                                </div>
                            </figure>
                        </div> <!-- **portfolio-thumb - Ends** -->
                    </div>

                    <div class="portfolio dt-sc-one-fifth no-space column all-sort fashion-sort graphic-sort">
                        <!-- **portfolio-thumb - Starts** -->
                        <div class="portfolio-thumb">
                            <figure>
                                <img src="http://placehold.it/1200x800&text=Gallery" alt="image"/>

                                <div class="image-overlay">
                                    <a class="zoom" href="http://placehold.it/1200x800&text=Gallery"
                                       data-gal="prettyPhoto[gallery]"><span class="fa fa-search"></span></a>
                                    <a class="link" href="portfolio-detail.html"><span class="fa fa-link"></span></a>

                                    <div class="portfolio-content">
                                        <h5><a href="portfolio-detail.html"> Fashion </a></h5>
                                        <span class="fa fa-sort-up"></span>
                                    </div>
                                </div>
                            </figure>
                        </div> <!-- **portfolio-thumb - Ends** -->
                    </div>

                    <div class="portfolio dt-sc-one-fifth no-space column all-sort fashion-sort photography-sort">
                        <!-- **portfolio-thumb - Starts** -->
                        <div class="portfolio-thumb">
                            <figure>
                                <img src="http://placehold.it/1200x800&text=Gallery" alt="image"/>

                                <div class="image-overlay">
                                    <a class="zoom" href="http://placehold.it/1200x800&text=Gallery"
                                       data-gal="prettyPhoto[gallery]"><span class="fa fa-search"></span></a>
                                    <a class="link" href="portfolio-detail-v2.html"><span class="fa fa-link"></span></a>

                                    <div class="portfolio-content">
                                        <h5><a href="portfolio-detail-v2.html"> World is ours </a></h5>
                                        <span class="fa fa-sort-up"></span>
                                    </div>
                                </div>
                            </figure>
                        </div> <!-- **portfolio-thumb - Ends** -->
                    </div>

                    <div class="portfolio dt-sc-one-fifth no-space column all-sort photography-sort outdoors-sort">
                        <!-- **portfolio-thumb - Starts** -->
                        <div class="portfolio-thumb">
                            <figure>
                                <img src="http://placehold.it/1200x800&text=Gallery" alt="image"/>

                                <div class="image-overlay">
                                    <a class="zoom" href="http://placehold.it/1200x800&text=Gallery"
                                       data-gal="prettyPhoto[gallery]"><span class="fa fa-search"></span></a>
                                    <a class="link" href="portfolio-detail.html"><span class="fa fa-link"></span></a>

                                    <div class="portfolio-content">
                                        <h5><a href="portfolio-detail.html"> Love the Life </a></h5>
                                        <span class="fa fa-sort-up"></span>
                                    </div>
                                </div>
                            </figure>
                        </div> <!-- **portfolio-thumb - Ends** -->
                    </div>

                    <div class="portfolio dt-sc-one-fifth no-space column all-sort fashion-sort outdoors-sort">
                        <!-- **portfolio-thumb - Starts** -->
                        <div class="portfolio-thumb">
                            <figure>
                                <img src="http://placehold.it/1200x800&text=Gallery" alt="image"/>

                                <div class="image-overlay">
                                    <a class="zoom" href="http://placehold.it/1200x800&text=Gallery"
                                       data-gal="prettyPhoto[gallery]"><span class="fa fa-search"></span></a>
                                    <a class="link" href="portfolio-detail-v2.html"><span class="fa fa-link"></span></a>

                                    <div class="portfolio-content">
                                        <h5><a href="portfolio-detail-v2.html"> Snow Time </a></h5>
                                        <span class="fa fa-sort-up"></span>
                                    </div>
                                </div>
                            </figure>
                        </div> <!-- **portfolio-thumb - Ends** -->
                    </div>

                    <div class="portfolio dt-sc-one-fifth no-space column all-sort graphic-sort">
                        <!-- **portfolio-thumb - Starts** -->
                        <div class="portfolio-thumb">
                            <figure>
                                <img src="http://placehold.it/1200x800&text=Gallery" alt="image"/>

                                <div class="image-overlay">
                                    <a class="zoom" href="http://placehold.it/1200x800&text=Gallery"
                                       data-gal="prettyPhoto[gallery]"><span class="fa fa-search"></span></a>
                                    <a class="link" href="portfolio-detail.html"><span class="fa fa-link"></span></a>

                                    <div class="portfolio-content">
                                        <h5><a href="portfolio-detail.html"> Black Jade </a></h5>
                                        <span class="fa fa-sort-up"></span>
                                    </div>
                                </div>
                            </figure>
                        </div> <!-- **portfolio-thumb - Ends** -->
                    </div>

                    <div class="portfolio dt-sc-one-fifth no-space column all-sort photography-sort graphic-sort">
                        <!-- **portfolio-thumb - Starts** -->
                        <div class="portfolio-thumb">
                            <figure>
                                <img src="http://placehold.it/1200x800&text=Gallery" alt="image"/>

                                <div class="image-overlay">
                                    <a class="zoom" href="http://placehold.it/1200x800&text=Gallery"
                                       data-gal="prettyPhoto[gallery]"><span class="fa fa-search"></span></a>
                                    <a class="link" href="portfolio-detail-v2.html"><span class="fa fa-link"></span></a>

                                    <div class="portfolio-content">
                                        <h5><a href="portfolio-detail-v2.html"> Photography </a></h5>
                                        <span class="fa fa-sort-up"></span>
                                    </div>
                                </div>
                            </figure>
                        </div> <!-- **portfolio-thumb - Ends** -->
                    </div>

                    <div class="portfolio dt-sc-one-fifth no-space column all-sort graphic-sort">
                        <!-- **portfolio-thumb - Starts** -->
                        <div class="portfolio-thumb">
                            <figure>
                                <img src="http://placehold.it/1200x800&text=Gallery" alt="image"/>

                                <div class="image-overlay">
                                    <a class="zoom" href="http://placehold.it/1200x800&text=Gallery"
                                       data-gal="prettyPhoto[gallery]"><span class="fa fa-search"></span></a>
                                    <a class="link" href="portfolio-detail.html"><span class="fa fa-link"></span></a>

                                    <div class="portfolio-content">
                                        <h5><a href="portfolio-detail.html"> An Adventure </a></h5>
                                        <span class="fa fa-sort-up"></span>
                                    </div>
                                </div>
                            </figure>
                        </div> <!-- **portfolio-thumb - Ends** -->
                    </div>

                    <div class="portfolio dt-sc-one-fifth no-space column all-sort outdoors-sort fashion-sort">
                        <!-- **portfolio-thumb - Starts** -->
                        <div class="portfolio-thumb">
                            <figure>
                                <img src="http://placehold.it/1200x800&text=Gallery" alt="image"/>

                                <div class="image-overlay">
                                    <a class="zoom" href="http://placehold.it/1200x800&text=Gallery"
                                       data-gal="prettyPhoto[gallery]"><span class="fa fa-search"></span></a>
                                    <a class="link" href="portfolio-detail-v2.html"><span class="fa fa-link"></span></a>

                                    <div class="portfolio-content">
                                        <h5><a href="portfolio-detail-v2.html"> Willow Tree </a></h5>
                                        <span class="fa fa-sort-up"></span>
                                    </div>
                                </div>
                            </figure>
                        </div> <!-- **portfolio-thumb - Ends** -->
                    </div>

                    <div class="portfolio dt-sc-one-fifth no-space column all-sort fashion-sort graphic-sort">
                        <!-- **portfolio-thumb - Starts** -->
                        <div class="portfolio-thumb">
                            <figure>
                                <img src="http://placehold.it/1200x800&text=Gallery" alt="image"/>

                                <div class="image-overlay">
                                    <a class="zoom" href="http://placehold.it/1200x800&text=Gallery"
                                       data-gal="prettyPhoto[gallery]"><span class="fa fa-search"></span></a>
                                    <a class="link" href="portfolio-detail.html"><span class="fa fa-link"></span></a>

                                    <div class="portfolio-content">
                                        <h5><a href="portfolio-detail.html"> Man and Women </a></h5>
                                        <span class="fa fa-sort-up"></span>
                                    </div>
                                </div>
                            </figure>
                        </div> <!-- **portfolio-thumb - Ends** -->
                    </div>


                </div> <!-- **portfolio-container - Ends** -->
                <div class="dt-sc-margin65"></div>
            </div> <!-- **Full-width-section - Ends** -->
        </section>
    </div>
</div>
<!-- **container - Ends** -->


@endif


@endsection

@section('scr1')
    <script src="{{ asset('/assets/frontend/js/jquery.jcarousel.min.js') }}" type="text/javascript"></script>
    {{--<script src="{{ asset('/assets/frontend/js/jquery.donutchart.min.js') }}" type="text/javascript"></script>--}}
@endsection

@section('scr2')
    <script src="{{ asset('/assets/frontend/js/jquery.smartresize.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/assets/frontend/js/jquery.scrollTo.js') }}" type="text/javascript"></script>
@endsection
@section('scr-retina')
    <script src="{{ asset('/assets/frontend/js/retina.js') }}" type="text/javascript"></script>
@endsection
@section('scripts')

    {{--    <script src="{{ asset('/assets/frontend/js/pace.min.js') }}" type="text/javascript"></script>--}}




    {{--<script src="{{ asset('/assets/frontend/js/layerslider.transitions.js') }}"></script>--}}
    {{--<script src="{{ asset('/assets/frontend/js/layerslider.kreaturamedia.jquery.js') }}"></script>--}}
    {{--    <script src="{{ asset('/assets/frontend/js/greensock.js') }}"></script>--}}





    {{--    <script src="{{ asset('/assets/frontend/js/custom-farsi.js') }}" type="text/javascript"></script>--}}

@endsection

@section('inline-scripts')

@endsection
