@extends('app')
@section('styles')
    <link rel="stylesheet" href="{{ asset('/assets/frontend/mis.css') }}" media="all"/>
@endsection
@section('data')
    @if(isset($dataCategories)&&$dataCategories)

        <section class="" style="width: 95%;">
            <div class="full-width-section">
                <div class="dt-sc-margin65"></div>
                <div class="container">

                    <?php $counter = 0; ?>
                    @foreach($dataCategories as $item)

                        <?php if ($counter == 3) {
                            $counter = 0;
                        } $counter++; ?>

                        <div class="column dt-sc-one-third {!! $counter==1?'first':'' !!}">
                            <div class="dt-sc-ico-content type7">
                                <div>
                                    @if(\Illuminate\Support\Facades\File::exists(base_path().env('PUBLIC_PATH_URL', 'public').'/uploads/images/Categories/'.$item->id.'-300x100.jpg'))
                                        <img src="{!! asset('/uploads/images/Categories/'.$item->id.'-300x100.jpg') !!}"
                                             alt="{!! $item->name !!}"/>
                                    @else
                                        <img src="images/admin1.png" alt="{!! $item->name !!}"/>
                                    @endif
                                </div>
                                <h4>
                                    <a href="{!!route( 'category.show',[$item->alias.'.html'])!!}"> {!!$item->name!!} </a>
                                </h4>

                                <p> {!!$item->short_content!!}</p>
                            </div>
                        </div>

                        @if($counter==3)
                            <div class="dt-sc-margin30"></div>
                        @endif

                    @endforeach
                </div>
                <div class="dt-sc-margin50"></div>
            </div>
        </section>

    @endif


@endsection
@section('content')





    <div class="dt-sc-margin35"></div>
    <!-- Container starts-->
    <div class="container">

        <!-- Primary Starts -->
        <section id="primary" class="with-right-sidebar page-with-sidebar">
            @if($data)
                @foreach($data as $item)
                    <article class="blog-post type4 base">

                        <!-- **entry-thumb - Starts** -->
                        <div class="column first">

                            <h4>
                                <a href="{!!route( 'product.show',[$item->alias])!!}">
                                    @if(\Illuminate\Support\Facades\File::exists(base_path().env('PUBLIC_PATH_URL', 'public').'/'.$item->src))
                                        <img src="{!! asset($item->src) !!}" alt="{!! $item->title !!}" width="300px"
                                             height="250px"/>
                                    @else
                                        <img src="/images/NoImage.jpg" alt="{!! $item->title !!}"/>
                                    @endif

                                </a>
                            </h4>

                            <div class="color-product">
                                <ul>
                                    {{-- @foreach($feature_color as $itemC)
                                         <li class="item-color column">
                                             <a class="item-color dt-sc-tooltip-bottom" data-value="{!! trim($itemC['value']) !!}" title="@lang('product.view.info.Color') {!! $itemC['title'] !!}">

                                                 <i style="color: {!! $itemC['value'] !!};" class="fa fa-square"></i>

                                             </a>
                                         </li>
                                     @endforeach--}}
                                </ul>
                            </div>
                        </div>
                        <div class="column dt-sc-three-fifth prod_details">

                            <a href="{!!route( 'product.show',[$item->alias])!!}"><h3>{!!$item->title!!}</h3>
                            </a>
                            <p></p>

                            <div class="dt-sc-hr-invisible-very-small"></div>
                            <!-- **project-details - Starts** -->
                            <div class="project-details">
                                <h5><span>@lang("product.view.info.ShortDescription")</span></h5>
                            </div>


                        </div>
                        <p class="entry-detail">

                        <div class="left">
                            <a href="{!!route( 'product.show',[$item->alias])!!}" class="dt-sc-button1 ico-button">@lang("product.view.index.ShowDetails")</a>
                        </div>
                        </p>

                    </article>
                @endforeach
            @endif
        </section>
        <!-- **secondary - Starts** -->
        @include('partials.sidebar')
                <!-- **secondary - Ends** -->
    </div>





@endsection
@section('scripts')

@endsection
@section('inline-scripts')

@endsection
