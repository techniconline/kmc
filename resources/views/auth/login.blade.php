@extends('app')

@section('browser_title')
    {{--    {!! $content->browser_title !!}--}}
@endsection

@section('styles')

@endsection

@section('content')

    <div class="parallax full-width-bg">
        <div class="container">
            <div class="main-title">
                <h1>@lang("home.view.login.TitleLogin")</h1>

                <div class="breadcrumb">
                    <a href="index.html">@lang("home.view.index.HomePage")</a>
                    <span class="fa fa-angle-left"></span>
                    <span class="current">@lang("home.view.index.Login")</span>
                </div>
            </div>
        </div>
    </div>
    <section id="primary" class="content-full-width">
        {{--<div class="full-width-section">--}}
        {{--<div class="container">--}}
        {{--<div class="page_info">--}}
        {{--<h3 class="aligncenter"><span> <i class="fa fa-user"></i></span>--}}
        {{--@lang("home.view.login.Bonjour")</h3>--}}
        {{--</div>--}}
        {{--<div class="full-width-section">--}}
        {{--<div class="container">--}}
        {{--<div class="dt-sc-margin70"></div>--}}
        {{--@if (count($errors) > 0)--}}
        {{--<div class="page_info aligncenter">--}}
        {{--<h4 class="title">@lang("home.view.messages.errTitleLogin")</h4>--}}

        {{--<p>@lang("home.view.messages.errLogin")</p>--}}
        {{--<ul>--}}
        {{--@foreach ($errors->all() as $error)--}}
        {{--<li>{{ $error }}</li>--}}
        {{--@endforeach--}}
        {{--</ul>--}}
        {{--</div>--}}
        {{--@endif--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--<div class="dt-sc-margin20"></div>--}}


        {{--</div>--}}
        {{--</div>--}}
        <div class="full-width-section full-section-bg">

            <div class="column dt-sc-one-half first">
                <div class="full-width-section parallax">
                    <div class="dt-sc-clear"></div>
                    <div class="form-wrapper register">
                        <form method="post" action="{{ url('/front/auth/register') }}"
                              enctype="multipart/form-data"
                              id="reg_form" name="frmRegister">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <p class="dt-sc-one-third column first">


                                <input placeholder="@lang("home.view.register.Fname")"
                                       class="txtFullBig"
                                       name="firstname"
                                       value="{{ old('firstname') }}" id="f_name" type="text">

                            </p>

                            <p class="dt-sc-two-third column">
                                <input placeholder="@lang("home.view.register.Lname")" type="text"
                                       class="txtFullBig"
                                       name="lastname" value="{{ old('lastname') }}" id="l_name" type="text">


                            </p>

                            <p>
                                <input placeholder="@lang("home.view.register.Email")" type="email"
                                       class="txtFullBig"
                                       name="email" value="{{ old('email') }}" id="email_link" type="email">
                            </p>

                            <p>
                                <input placeholder="@lang("home.view.register.Password")" type="password"
                                       class="txtFullBig"
                                       name="password" id="user_pwd" type="password">
                            </p>

                            <p>
                                <input placeholder="@lang("home.view.register.ConfirmPass")" type="password"
                                       class="txtFullBig" name="password_confirmation" name="c_pwd"
                                       type="password">
                            </p>

                            <input class="button" value="@lang("home.view.register.Register")" type="submit">
                        </form>
                    </div>
                </div>

            </div>
            <div class="column dt-sc-one-half">
                <div class="full-width-section parallax">

                    <div class="dt-sc-clear"></div>
                    <div class="form-wrapper login">

                        <form method="post"
                              action="{{ url((app('locale') == '')? '/front/auth/login' : '/'.app('locale').'/front/auth/login') }}"
                              enctype="multipart/form-data" id="loginform" name="frmLogin">
                            <p>
                                <input placeholder="@lang("home.view.login.Email")" id="user_name" name="email"
                                       maxlength="127" autocomplete="off" type="text">
                            </p>

                            <p>
                                <input placeholder="@lang("home.view.login.Password")" id="user_pwd"
                                       name="password" maxlength="64" type="password">
                            </p>

                            <label><input value="forever" id="rememberme" name="rememberme"
                                          type="checkbox">@lang("home.view.login.Remember")</label>
                            <input class="button" value="@lang("home.view.login.Login")" type="submit">
                        </form>
                    </div>
                </div>
                <div class="full-width-section parallax">
                    <div class="dt-sc-clear"></div>
                    <div class="form-wrapper login">

                        <form id="requestResetPassword-target" name="requestResetPassword" method="post"
                              action="{{ url('/front/password/email') }}"
                              enctype="multipart/form-data" style="display:none ;">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <p>@lang("home.view.login.TitleForget")</p>

                            <p>
                                <input placeholder="@lang("home.view.login.Email")" name="email" maxlength="127"
                                       autocomplete="off" type="text">
                            </p>
                            <input class="button" value="@lang("home.view.login.SendPass")" type="submit">
                        </form>
                    </div>
                </div>
            </div>

        </div>
        <div class="full-width-section full-section-bg" style="padding: 20px !important;">
            <div class="container">
                <div class="page_info aligncenter">
                    <h4 class="title">@lang("home.view.login.ProblemLogin")</h4>

                    <p>@lang("home.view.login.DescAuthStart")<a href="#reg_form"
                                                                title="">@lang("home.view.login.DescAuthMiddle")</a> @lang("home.view.login.DescAuthEnd")
                    </p>

                    <p>@lang("home.view.login.DescRegStart")<a id="requestResetPassword"
                                                               href="#requestResetPassword-target"
                                                               class="link panelTrigger">@lang("home.view.login.TitleForget")</a> @lang("home.view.login.DescRegEnd")
                    </p>
                </div>
            </div>
        </div>
        <div class="dt-sc-margin100"></div>

    </section>




@endsection


@section('scripts')
@endsection


@section('inline-scripts')
    <script>
        $(document).ready(function () {

            $('#requestResetPassword').on('click', function () {
                var clicked = $(this);
                var target = $('#requestResetPassword-target');
                target.fadeIn();
            });

        });
    </script>
@endsection
