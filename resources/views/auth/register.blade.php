@extends('app')


@section('browser_title')
{{--    {!! $content->browser_title !!}--}}
@endsection

@section('styles')

        <!--link id="layer-slider" rel="stylesheet" href="{{ asset('/assets/frontend/css/layerslider.css') }}" media="all"/>

    <!-- **Additional - stylesheets** -->
<link rel="stylesheet" href="{{ asset('/assets/frontend/responsive.css') }}" type="text/css" media="all"/>
<link rel="stylesheet" href="{{ asset('/assets/frontend/css/meanmenu.css') }}" type="text/css" media="all"/>
<link rel="stylesheet" href="{{ asset('/assets/frontend/css/prettyPhoto.css') }}" type="text/css" media="screen"/>
<link rel="stylesheet" href="{{ asset('/assets/frontend/css/animations.css') }}" type="text/css" media="all"/>

<!-- **Font Awesome** -->
<link rel="stylesheet" href="{{ asset('/assets/frontend/css/font-awesome.min.css') }}" type="text/css"/>
@endsection

@section('content')

        <!--article class="signup"  style="width: 45%">
		<h2>@lang("home.view.register.Title")</h2>
		<p>@lang("home.view.register.Text")</p>
		<form name="login" method="post" action="{{ url('/front/auth/register') }}" enctype="multipart/form-data">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">

			<div class="form-group">
				<label class="col-md-4 control-label">@lang("home.view.register.Fname")</label>
				<input type="text" placeholder="@lang("home.view.register.Fname")" class="txtFullBig" name="firstname" value="{{ old('firstname') }}">
			</div>

			<div class="form-group">
				<label class="col-md-4 control-label">@lang("home.view.register.Lname")</label>
				<input placeholder="@lang("home.view.register.Lname")"  type="text" class="txtFullBig" name="lastname" value="{{ old('lastname') }}">
			</div>

			<div class="form-group">
				<label class="col-md-4 control-label">@lang("home.view.register.Email")</label>
				<input placeholder="@lang("home.view.register.Email")"  type="email" class="txtFullBig" name="email" value="{{ old('email') }}">
			</div>

			<div class="form-group">
				<label class="col-md-4 control-label">@lang("home.view.register.Gender")</label>
				{!!Form::select('gender', trans("home.view.register.DataGender"),null,['style'=>'border: 1px solid #f0e5cd; height: 40px; margin: 0; width: 60%;','class'=>'btn form-control input-small select2me','id'=>'gender','required'=>''])!!}
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label">@lang("home.view.register.Password")</label>
				<input placeholder="@lang("home.view.register.Password")"  type="password" class="txtFullBig" name="password">
			</div>

			<div class="form-group">
				<label class="col-md-4 control-label">@lang("home.view.register.ConfirmPass")</label>
				<input placeholder="@lang("home.view.register.ConfirmPass")"  type="password" class="txtFullBig" name="password_confirmation">
			</div>

			<div class="form-group">
				<div class="col-md-6 col-md-offset-4">
					<button type="submit" class="submit button">
						@lang("home.view.register.Register")
        </button>
    </div>
</div>
</form>
</article-->
<div id="main">
    <div class="parallax full-width-bg">
        <div class="container">
            <div class="main-title">
                <h1> @lang("home.view.register.Title") </h1>

                <div class="breadcrumb">
                    <a href="index.html"> صفحه اصلی </a>
                    <span class="fa fa-angle-left"></span>
                    <span class="current"> عضویت در سایت </span>
                </div>
            </div>
        </div>
    </div>
    <div class="dt-sc-margin100"></div>
    <section id="primary" class="content-full-width">
        <div class="full-width-section">
            <div class="container">
                <div class="page_info">
                    <h3 class="aligncenter"><span> <i class="fa fa-user"></i></span>
                        @lang("home.view.register.Title") </h3>
                </div>
                <div class="full-width-section">
                    <div class="container">
                        <div class="dt-sc-margin70"></div>
                        @if (count($errors) > 0)
                            <div class="page_info aligncenter">
                                <h4 class="title">خطای ورود!</h4>

                                <p>نام کاربری یا رمز عبور خود را اشتباه وارد کرده اید...</p>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="dt-sc-margin20"></div>
            </div>
        </div>
        <div class="full-width-section parallax full-section-bg">
            <div class="container">
                <div class="dt-sc-clear"></div>
                <div class="form-wrapper register">
                    <form method="post" action="{{ url('/front/auth/register') }}" enctype="multipart/form-data"
                          id="reg_form" name="frmRegister">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <p class="dt-sc-one-half column first">
                            <input placeholder="@lang("home.view.register.Fname")" class="txtFullBig" name="firstname"
                                   value="{{ old('firstname') }}" id="f_name" type="text">
                        </p>

                        <p class="dt-sc-one-half column">
                            <input placeholder="@lang("home.view.register.Lname")" type="text" class="txtFullBig"
                                   name="lastname" value="{{ old('lastname') }}" id="l_name" type="text">
                        </p>

                        <p class="dt-sc-one-half column first">
                            <input placeholder="@lang("home.view.register.Email")" type="email" class="txtFullBig"
                                   name="email" value="{{ old('email') }}" id="email_link" type="email">
                        </p>

                        <p class="dt-sc-one-half column">
                            <input placeholder="Web Address" id="web_link" name="web" autocomplete="off" type="text">
                        </p>

                        <p class="dt-sc-one-half column first">
                            <input placeholder="@lang("home.view.register.Password")" type="password" class="txtFullBig"
                                   name="password" id="user_pwd" type="password">
                        </p>

                        <p class="dt-sc-one-half column">
                            <input placeholder="@lang("home.view.register.ConfirmPass")" type="password"
                                   class="txtFullBig" name="password_confirmation" name="c_pwd" type="password">
                        </p>

                        <input class="button" value="@lang("home.view.register.Register")" type="submit">
                    </form>
                </div>
            </div>
        </div>

    </section>
</div>
<!--div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Register</div>
				<div class="panel-body">
					@if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
								@endforeach
        </ul>
    </div>
@endif

        <form class="form-horizontal" role="form" method="POST" action="{{ url('/front/auth/register') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="form-group">
                            <label class="col-md-4 control-label">First Name</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="firstname" value="{{ old('firstname') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Last Name</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="lastname" value="{{ old('lastname') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">E-Mail Address</label>
                            <div class="col-md-6">
                                <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Gender</label>
                            <div class="col-md-6">

                                <select name="gender" id="gender" class="form-control">
                                    <option value="male">Male</option>
                                    <option value="female">Female</option>
                                </select>

                            </div>
                        </div>

						<div class="form-group">
							<label class="col-md-4 control-label">Password</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Confirm Password</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password_confirmation">
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									Register
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div-->
@endsection
