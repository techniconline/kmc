@extends('app')

@section('browser_title')
{{--    {!! $content->browser_title !!}--}}
@endsection

@section('styles')

        <!--link id="layer-slider" rel="stylesheet" href="{{ asset('/assets/frontend/css/layerslider.css') }}" media="all"/>

    <!-- **Additional - stylesheets** -->
<link rel="stylesheet" href="{{ asset('/assets/frontend/responsive.css') }}" type="text/css" media="all"/>
<link rel="stylesheet" href="{{ asset('/assets/frontend/css/meanmenu.css') }}" type="text/css" media="all"/>
<link rel="stylesheet" href="{{ asset('/assets/frontend/css/prettyPhoto.css') }}" type="text/css" media="screen"/>
<link rel="stylesheet" href="{{ asset('/assets/frontend/css/animations.css') }}" type="text/css" media="all"/>

<!-- **Font Awesome** -->
<link rel="stylesheet" href="{{ asset('/assets/frontend/css/font-awesome.min.css') }}" type="text/css"/>
@endsection

@section('content')
    <div id="main">
        <div class="parallax full-width-bg">
            <div class="container">
                <div class="main-title">
                    <h1>  @lang("home.view.login.TitleLogin") </h1>

                    <div class="breadcrumb">
                        <a href="index.html">@lang("home.view.index.HomePage")</a>
                        <span class="fa fa-angle-left"></span>
                        <span class="current">@lang("home.view.index.Login")</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="dt-sc-margin100"></div>
        <section id="primary" class="content-full-width">
            <div class="full-width-section">
                <div class="container">
                    <div class="page_info">
                        <h3 class="aligncenter"><span> <i class="fa fa-user"></i></span>
                            @lang("home.view.login.Bonjour")</h3>
                    </div>
                    <div class="full-width-section">
                        <div class="container">
                            <div class="dt-sc-margin70"></div>
                            @if (count($errors) > 0)
                                <div class="page_info aligncenter">
                                    <h4 class="title">@lang("home.view.messages.errTitleLogin")</h4>

                                    <p>@lang("home.view.messages.errLogin")</p>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="dt-sc-margin20"></div>


                </div>
            </div>
            <div class="full-width-section parallax full-section-bg">
                <div class="container">
                    <div class="dt-sc-clear"></div>
                    <div class="form-wrapper login">

                        <form method="post"
                              action="{{ url((app('locale') == '')? '/front/auth/login' : '/'.app('locale').'/front/auth/login') }}"
                              enctype="multipart/form-data" id="loginform" name="frmLogin">
                            <p>
                                <input placeholder="@lang("home.view.login.Email")" id="user_name" name="email"
                                       maxlength="127" autocomplete="off" type="text">
                            </p>

                            <p>
                                <input placeholder="@lang("home.view.login.Password")" id="user_pwd" name="password"
                                       maxlength="64" type="password">
                            </p>

                            <label><input value="forever" id="rememberme" name="rememberme"
                                          type="checkbox">@lang("home.view.login.Remember")</label>
                            <input class="button" value="@lang("home.view.login.Login")" type="submit">
                        </form>
                    </div>
                </div>
            </div>
            <div class="full-width-section parallax full-section-bg">
                <div class="container">
                    <div class="dt-sc-clear"></div>
                    <div class="form-wrapper login">

                        <form id="requestResetPassword-target" name="requestResetPassword" method="post"
                              action="{{ url('/front/password/email') }}"
                              enctype="multipart/form-data" style="display: none;">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <p>@lang("home.view.login.TitleForget")</p>

                            <p>
                                <input placeholder="@lang("home.view.login.Email")" name="email" maxlength="127"
                                       autocomplete="off" type="text">
                            </p>
                            <input class="button" value="@lang("home.view.login.SendPass")" type="submit">
                        </form>
                    </div>
                </div>
            </div>

            <div class="full-width-section">
                <div class="container">
                    <div class="dt-sc-margin70"></div>
                    <div class="page_info aligncenter">
                        <h4 class="title">@lang("home.view.login.ProblemLogin")</h4>

                        <p>@lang("home.view.login.DescLoginStart")<a href="register.html"
                                                                     title="">@lang("home.view.login.DescLoginMiddle")</a> @lang("home.view.login.DescLoginEnd")
                        </p>

                        <p>@lang("home.view.login.DescRegStart")<a id="requestResetPassword"
                                                                   class="link panelTrigger">@lang("home.view.login.TitleForget")</a> @lang("home.view.login.DescRegEnd")
                        </p>
                    </div>
                </div>
            </div>
            <div class="dt-sc-margin100"></div>

        </section>
    </div>
    <!--section class="fullscreen" id="profileSection">
        <div class="container">

            <h1>@lang("home.view.login.Bonjour")</h1>
            <div class="post atgLTR"><a name="Profile Welcome"></a>@lang("home.view.login.TextWelcome")</div><br class="clearfloat">
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
                        @endforeach
            </ul>
        </div>
    @endif

            <article class="login" style="width: 45%">

                @lang("home.view.login.TitleLogin")
            <form name="login" method="post" action="{{ url((app('locale') == '')? '/front/auth/login' : '/'.app('locale').'/front/auth/login') }}" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <label>@lang("home.view.login.Email")</label>
                    <input type="email" placeholder="@lang("home.view.login.Email") " name="email" maxlength="127" class="txtFullBig" required value="{{ old('email') }}">
                    <label>@lang("home.view.login.Password")</label>
                    <input type="password" placeholder="@lang("home.view.login.Password")" name="password" maxlength="64" class="txtFullBig" required>
                    <label>&nbsp;</label>
                    <button type="submit" class="submit button">@lang("home.view.login.Login")</button>

                </form>
                <p><a id="requestResetPassword" class="link panelTrigger">@lang("home.view.login.TitleForget")</a></p>
                <form id="requestResetPassword-target" name="requestResetPassword" method="post" action="{{ url('/front/password/email') }}"
                      enctype="multipart/form-data" style="display: none;">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <p>@lang("home.view.login.TextForget")</p>
                    <label>@lang("home.view.login.Email")</label>
                    <input type="email" placeholder="@lang("home.view.login.Email")" name="email" maxlength="127" class="txtFullBig" required>
                    <label>&nbsp;</label>
                    <button type="submit" class="submit button">
                        @lang("home.view.login.SendPass")
            </button>
        </form>

    </article>


    <article class="signup"  style="width: 45%">
        <h2>@lang("home.view.register.Title")</h2>
                <p>@lang("home.view.register.Text")</p>
                <form name="login" method="post" action="{{ url('/front/auth/register') }}" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="form-group">
                        <label class="col-md-4 control-label">@lang("home.view.register.Fname")</label>
                            <input type="text" placeholder="@lang("home.view.register.Fname")" class="txtFullBig" name="firstname" value="{{ old('firstname') }}">
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">@lang("home.view.register.Lname")</label>
                            <input placeholder="@lang("home.view.register.Lname")"  type="text" class="txtFullBig" name="lastname" value="{{ old('lastname') }}">
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">@lang("home.view.register.Email")</label>
                            <input placeholder="@lang("home.view.register.Email")"  type="email" class="txtFullBig" name="email" value="{{ old('email') }}">
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">@lang("home.view.register.Gender")</label>
                            {!!Form::select('gender', trans("home.view.register.DataGender"),null,['style'=>'border: 1px solid #f0e5cd; height: 40px; margin: 0; width: 60%;','class'=>'btn form-control input-small select2me','id'=>'gender','required'=>''])!!}
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label">@lang("home.view.register.Password")</label>
                            <input placeholder="@lang("home.view.register.Password")"  type="password" class="txtFullBig" name="password">
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">@lang("home.view.register.ConfirmPass")</label>
                            <input placeholder="@lang("home.view.register.ConfirmPass")"  type="password" class="txtFullBig" name="password_confirmation">
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="submit button">
                                @lang("home.view.register.Register")
            </button>
        </div>
    </div>
</form>
</article>


</div>
</section-->




@endsection


@section('scripts')
@endsection


@section('inline-scripts')
    <script>
        $(document).ready(function () {

            $('#requestResetPassword').on('click', function () {
                var clicked = $(this);
                var target = $('#requestResetPassword-target');
                target.fadeIn();
            });

        });
    </script>
@endsection
