@extends('app')
@section('styles')
    <link id="layer-slider" rel="stylesheet" href="{{ asset('/assets/frontend/css/layerslider.css') }}" media="all"/>

    <!-- **Additional - stylesheets** -->
    <link rel="stylesheet" href="{{ asset('/assets/frontend/responsive.css') }}" type="text/css" media="all"/>
    <link rel="stylesheet" href="{{ asset('/assets/frontend/css/meanmenu.css') }}" type="text/css" media="all"/>
    <link rel="stylesheet" href="{{ asset('/assets/frontend/css/prettyPhoto.css') }}" type="text/css" media="screen"/>
    <link rel="stylesheet" href="{{ asset('/assets/frontend/css/animations.css') }}" type="text/css" media="all"/>

    <!-- **Font Awesome** -->
    <link rel="stylesheet" href="{{ asset('/assets/frontend/css/font-awesome.min.css') }}" type="text/css"/>
@endsection
@section('content')

    <div class="TestPage">


        این صفحه برای تست است.


    </div>

@endsection
@section('scripts')
    <script src="{{ asset('/assets/frontend/js/pace.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/assets/frontend/js/jquery.tabs.min.js') }}"></script>
    <script src="{{ asset('/assets/frontend/js/jquery.tipTip.minified.js') }}"></script>
    <script src="{{ asset('/assets/frontend/js/jquery-easing-1.3.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/assets/frontend/js/jquery.carouFredSel-6.2.1-packed.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/frontend/js/jquery.parallax-1.1.3.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/assets/frontend/js/jquery.inview.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/assets/frontend/js/jquery.nav.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/assets/frontend/js/layerslider.transitions.js') }}"></script>
    <script src="{{ asset('/assets/frontend/js/layerslider.kreaturamedia.jquery.js') }}"></script>
    <script src="{{ asset('/assets/frontend/js/greensock.js') }}"></script>
@endsection
@section('inline-scripts')
    <script data-cfasync="false" type="text/javascript">var lsjQuery = jQuery;</script>
@endsection
@section('footer-scripts')

    <script src="{{ asset('/assets/frontend/js/jquery.jcarousel.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/assets/frontend/js/jquery.isotope.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/assets/frontend/js/jquery.prettyPhoto.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/assets/frontend/js/masonry.pkgd.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/assets/frontend/js/jquery.smartresize.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/assets/frontend/js/responsive-nav.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/assets/frontend/js/jquery.meanmenu.min.js') }}" type="text/javascript"></script>
    <!--<script src="http://maps.google.com/maps/api/js?sensor=false"></script>-->
    <script src="{{ asset('/assets/frontend/js/jquery.gmap.min.js') }}"></script>
    <!-- **Sticky Nav** -->
    <script src="{{ asset('/assets/frontend/js/jquery.sticky.js') }}" type="text/javascript"></script>
    <!-- **To Top** -->
    <script src="{{ asset('/assets/frontend/js/jquery.ui.totop.min.js') }}" type="text/javascript"></script>
    <!--<script type="text/javascript" src="{{ asset('/assets/frontend/js/twitter/jquery.tweet.min.js') }}"></script>-->
    <script src="{{ asset('/assets/frontend/js/jquery.viewport.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset('/assets/frontend/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('/assets/frontend/js/retina.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/assets/frontend/js/jquery.nicescroll.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/assets/frontend/js/custom.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/assets/frontend/js/custom-farsi.js') }}" type="text/javascript"></script>

@endsection
