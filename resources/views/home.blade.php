@extends('app')
<?php  $langDetails = app('activeLangDetails');?>

@section('styles')
    <link  href="{{ asset('/assets/frontend/js/revolution-slider/css/settings.css') }}" rel="stylesheet" type="text/css"/>
    <link  href="{{ asset('/assets/frontend/js/revolution-slider/css/layers.css') }}" rel="stylesheet" type="text/css"/>
    <link  href="{{ asset('/assets/frontend/js/revolution-slider/css/navigation.css') }}" rel="stylesheet" type="text/css"/>

{{--    <link rel="stylesheet" href="{{ asset('/assets/frontend/mis.css') }}" media="all"/>--}}
@endsection
@section('content')

    @include('partials.slider')
    @include('partials.welcome')
    @include('partials.about')
{{--    @include('partials.divider')--}}
    @include('partials.expart')
{{--    @include('partials.carousel')--}}
    @include('partials.news')


@endsection
@section('slider-js')
    <script src="{{ asset('/assets/frontend/js/revolution-slider/js/jquery.themepunch.tools.min.js') }}"></script>
    <script src="{{ asset('/assets/frontend/js/revolution-slider/js/jquery.themepunch.revolution.min.js') }}"></script>
    <script src="{{ asset('/assets/frontend/js/revolution-slider/js/extensions/revolution.extension.actions.min.js') }}"></script>
    <script src="{{ asset('/assets/frontend/js/revolution-slider/js/extensions/revolution.extension.carousel.min.js') }}"></script>
    <script src="{{ asset('/assets/frontend/js/revolution-slider/js/extensions/revolution.extension.kenburn.min.js') }}"></script>
    <script src="{{ asset('/assets/frontend/js/revolution-slider/js/extensions/revolution.extension.layeranimation.min.js') }}"></script>
    <script src="{{ asset('/assets/frontend/js/revolution-slider/js/extensions/revolution.extension.migration.min.js') }}"></script>
    <script src="{{ asset('/assets/frontend/js/revolution-slider/js/extensions/revolution.extension.navigation.min.js') }}"></script>
    <script src="{{ asset('/assets/frontend/js/revolution-slider/js/extensions/revolution.extension.parallax.min.js') }}"></script>
    <script src="{{ asset('/assets/frontend/js/revolution-slider/js/extensions/revolution.extension.slideanims.min.js') }}"></script>
    <script src="{{ asset('/assets/frontend/js/revolution-slider/js/extensions/revolution.extension.video.min.js') }}"></script>

@endsection
@section('inline-scripts')
    <script>
        $(document).ready(function(e) {
            var revapi = $(".rev_slider").revolution({
                sliderType:"standard",
                jsFileLocation: "/assets/frontend/js/revolution-slider/js/",
                sliderLayout: "fullscreen",
                dottedOverlay: "none",
                delay: 5000,
                navigation: {
                    keyboardNavigation: "off",
                    keyboard_direction: "horizontal",
                    mouseScrollNavigation: "off",
                    onHoverStop: "off",
                    touch: {
                        touchenabled: "on",
                        swipe_threshold: 75,
                        swipe_min_touches: 1,
                        swipe_direction: "horizontal",
                        drag_block_vertical: false
                    },
                    arrows: {
                        style: "gyges",
                        enable: true,
                        hide_onmobile: false,
                        hide_onleave: true,
                        hide_delay: 200,
                        hide_delay_mobile: 1200,
                        tmp: '',
                        left: {
                            h_align: "left",
                            v_align: "center",
                            h_offset: 0,
                            v_offset: 0
                        },
                        right: {
                            h_align: "right",
                            v_align: "center",
                            h_offset: 0,
                            v_offset: 0
                        }
                    },
                    bullets: {
                        enable: true,
                        hide_onmobile: true,
                        hide_under: 800,
                        style: "hebe",
                        hide_onleave: false,
                        direction: "horizontal",
                        h_align: "center",
                        v_align: "bottom",
                        h_offset: 0,
                        v_offset: 30,
                        space: 5,
                        tmp: '<span class="tp-bullet-image"></span><span class="tp-bullet-imageoverlay"></span><span class="tp-bullet-title"></span>'
                    }
                },
                responsiveLevels: [1240, 1024, 778],
                visibilityLevels: [1240, 1024, 778],
                gridwidth: [1170, 1024, 778, 480],
                gridheight: [710, 768, 960, 720],
                lazyType: "none",
                parallax:"mouse",
                parallaxBgFreeze:"off",
                parallaxLevels:[2,3,4,5,6,7,8,9,10,1],
                shadow: 0,
                spinner: "off",
                stopLoop: "on",
                stopAfterLoops: 0,
                stopAtSlide: -1,
                shuffle: "off",
                autoHeight: "off",
                fullScreenAutoWidth: "off",
                fullScreenAlignForce: "off",
                fullScreenOffsetContainer: "",
                fullScreenOffset: "0",
                hideThumbsOnMobile: "off",
                hideSliderAtLimit: 0,
                hideCaptionAtLimit: 0,
                hideAllCaptionAtLilmit: 0,
                debugMode: false,
                fallbacks: {
                    simplifyAll: "off",
                    nextSlideOnWindowFocus: "off",
                    disableFocusListener: false,
                }
            });
        });
    </script>
@endsection