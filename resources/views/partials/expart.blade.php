<section class="divider parallax layer-overlay overlay-dark-8" id="services" data-bg-img="images/bg/bg2.jpg" data-parallax-ratio="0.7">
    <div class="container">
        <div class="section-title text-center">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <h2 class="text-uppercase title">Special <span class="text-white font-weight-300"> Services</span></h2>
                    <p class="text-uppercase letter-space-4">Join our gymzone club and be healthy.</p>
                </div>
            </div>
        </div>
        <div class="section-content">
            <div class="row">
                <div class="col-sm-6 col-md-6">
                    <div class="icon-box p-0 mb-30">
                        <a class="icon icon-xl pull-left border-5px" href="#">
                            <img class="img-fullwidth" src="images/services/m1.jpg" alt="">
                        </a>
                        <div class="ml-140">
                            <h4 class="icon-box-title mt-15 text-uppercase letter-space-2 mb-0 text-theme-colored">Design & style</h4>
                            <p class="text-white">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae officiis ex, illo cumque quis totam saepe quisquam dicta hic eos aliquid est odit itaque, earum id, voluptatibus!</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6">
                    <div class="icon-box p-0 mb-30">
                        <a class="icon icon-xl pull-left border-5px" href="#">
                            <img class="img-fullwidth" src="images/services/m2.jpg" alt="">
                        </a>
                        <div class="ml-140">
                            <h4 class="icon-box-title mt-15 text-uppercase letter-space-2 mb-0 text-theme-colored">Dimension color</h4>
                            <p class="text-white">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae officiis ex, illo cumque quis totam saepe quisquam dicta hic eos aliquid est odit itaque, earum id, voluptatibus!</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6">
                    <div class="icon-box p-0 mb-30">
                        <a class="icon icon-xl pull-right border-5px" href="#">
                            <img class="img-fullwidth" src="images/services/m3.jpg" alt="">
                        </a>
                        <div class="mr-140 text-right flip">
                            <h4 class="icon-box-title mt-15 text-uppercase letter-space-2 mb-0 text-theme-colored">Highlights full head</h4>
                            <p class="text-white">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae officiis ex, illo cumque quis totam saepe quisquam dicta hic eos aliquid est odit itaque, earum id, voluptatibus!</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6">
                    <div class="icon-box p-0 mb-30">
                        <a class="icon icon-xl pull-right border-5px" href="#">
                            <img class="img-fullwidth" src="images/services/m4.jpg" alt="">
                        </a>
                        <div class="mr-140 text-right flip">
                            <h4 class="icon-box-title mt-15 text-uppercase letter-space-2 mb-0 text-theme-colored">Highlights partial</h4>
                            <p class="text-white">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae officiis ex, illo cumque quis totam saepe quisquam dicta hic eos aliquid est odit itaque, earum id, voluptatibus!</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6">
                    <div class="icon-box p-0 mb-30">
                        <a class="icon icon-xl pull-left border-5px" href="#">
                            <img class="img-fullwidth" src="images/services/m5.jpg" alt="">
                        </a>
                        <div class="ml-140">
                            <h4 class="icon-box-title mt-15 text-uppercase letter-space-2 mb-0 text-theme-colored">Base Perm</h4>
                            <p class="text-white">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae officiis ex, illo cumque quis totam saepe quisquam dicta hic eos aliquid est odit itaque, earum id, voluptatibus!</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6">
                    <div class="icon-box p-0 mb-30">
                        <a class="icon icon-xl pull-left border-5px" href="#">
                            <img class="img-fullwidth" src="images/services/m6.jpg" alt="">
                        </a>
                        <div class="ml-140">
                            <h4 class="icon-box-title mt-15 text-uppercase letter-space-2 mb-0 text-theme-colored">Hair Tinsel</h4>
                            <p class="text-white">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae officiis ex, illo cumque quis totam saepe quisquam dicta hic eos aliquid est odit itaque, earum id, voluptatibus!</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="bg-theme-colored">
    <div class="container pt-30 pb-30">
        <div class="section-content">
            <div class="row">
                <div class="col-md-8">
                    <h3 class="font-weight-400 text-white font-raleway letter-space-2">Not every woman has time to go to a salon and have her hair blow-dried every day.</h3>
                </div>
                <div class="col-md-4">
                    <a class="btn btn-dark btn-flat mt-30 hvr-bounce-to-right no-border pull-right" href="ajax-load/reservation-form.html" data-target="#BSParentModal" data-toggle="modal">Online Reservation</a>
                </div>
            </div>
        </div>
    </div>
</section>