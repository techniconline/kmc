<section class="about">
    <div class="container pt-0">
        <div class="section-content">
            <div class="row">
                <div class="col-md-6 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
                    <h3 class="text-uppercase text-theme-colored">چرا 1732</h3>
                    {{--<h2 class="text-uppercase">obcaecati ad tempora vitae quidem Dolores.</h2>--}}
                    <p>
                        مجموعه نمایندگی فروش 1732 کرمان موتور در اسفند 1388 با کسب مجوز نمایندگی کرمان موتور با گسترش فعالیت خود و با داشتن کادری مجرب و متعهد در امر فروش، موفق گردید در سال 1390 عنوان نماینده برتر فروش کرمان موتور را از آن خود نماید.
                    </p>
                    <p>
                        تاکنون سعی بر این داشته در راستای رضایتمندی مشتری و تکریم ارباب رجوع قدم برداشته و یکی از اهداف این نمایندگی که همانا جلب رضایت و اعتماد کامل مشتریان است را سرلوحه کار خویش قرارداده است.
                    </p>
                    <a href="default.htm" class="btn btn-theme-colored btn-flat mt-10"> Read More</a>
                </div>
                <div class="col-md-6 wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.3s">
                    <img class="img-fullwidth" src="images/about/1.png" alt="">
                </div>
            </div>
        </div>
    </div>
</section>