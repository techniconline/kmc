<section id="secondary-right" class="secondary-sidebar secondary-has-right-sidebar">

    {{--<div class="dt-sc-margin35"></div>--}}
    <aside class="widget">
        <form action="{{URL::route('searchContentText')}}" class="mailchimp-form">

            {{--<span class="fa fa-search"></span>--}}
            <input type="text" name="searchText" class="text"/>

            <input type="submit" value="search" class="dt-sc-button">

        </form>
    </aside>
    <aside class="widget">
        {!! ParamsHelper::getStaticContent('SideAdvert')['body_content'] !!}
    </aside>
    {{--<div class="dt-sc-margin35"></div>--}}

    {{--    {{\App\Providers\Helpers\Category\CategoryHelper::createTreeCategories($roots, $translatedName, 0, $locale, trans("News.view.index.CategoryList"), $systemContent)}}--}}


    {{--<aside class="widget widget_categories shbox">--}}
    {{--<header><h4 class="widgettitle">Blog Categories</h4></header>--}}
    {{--<ul>--}}
    {{--<li><a href="#">Internet Trends</a></li>--}}
    {{--<li><a href="#">Web Design</a></li>--}}
    {{--<li><a href="#">Wordpress</a></li>--}}
    {{--<li><a href="#">Web Development</a></li>--}}
    {{--<li><a href="#">Illustration</a></li>--}}
    {{--<li><a href="#">UI Design</a></li>--}}
    {{--<li><a href="#">App Development</a></li>--}}
    {{--</ul>--}}
    {{--</aside>--}}

    {{--<aside class="widget widget_categories">--}}
    {{--{!! ParamsHelper::getStaticContent('SideAdv')['body_content'] !!}--}}
    {{--</aside>--}}
    {{--@if($lastNews= \App\Providers\Helpers\Params\ParamsHelper::getLastNewsContent(5))--}}
    {{--<aside class="widget widget_recent_entries" id="my_recent_posts-3">--}}
    {{--<h4 class="widgettitle"><span>@lang("News.view.index.RecentPostsNews")</span></h4>--}}

    {{--<div class="recent-posts-widget">--}}
    {{--<ul>--}}
    {{--@foreach($lastNews as $itmNews)--}}
    {{--<li>--}}
    {{--<div class="thumb">--}}
    {{--<a href="{!! route($locale.'news.show',$itmNews->url)  !!}" title=""><div class="cm">--}}
    {{--<span class="fa fa-comment"> </span> 16 --}}
    {{--</div>--}}
    {{--@if($itmNews->image_content--}}
    {{--&& \Illuminate\Support\Facades\File::exists(base_path().env('PUBLIC_PATH_URL', 'public').$itmNews->image_content.'/'.$itmNews->contents_id.'-160x80.'.$itmNews->extension))--}}
    {{--<img src="{{asset($itmNews->image_content).'/'.$itmNews->contents_id.'-160x80.'.$itmNews->extension}}" alt="{!!$itmNews->tags!!}" title=""/>--}}
    {{--@else--}}
    {{--<img src="images/no-image.jpg" alt="no-image"/>--}}
    {{--@endif--}}
    {{--</a>--}}
    {{--</div>--}}
    {{--<h4><a href="{!! route($locale.'news.show',$itmNews->url)  !!}">{!! $itmNews->link_title !!}</a></h4>--}}
    {{--<!-- **entry-meta-data - Starts** -->--}}
    {{--<div class="entry-meta-data">--}}
    {{--<p><span class="fa fa-calendar"> </span> {!! $itmNews->pDay !!} {!! $itmNews->pMonthName !!} {!! $itmNews->pYear !!} </p>--}}

    {{--@if($itmNews->tags && $tagsArr=explode(",",$itmNews->tags))--}}
    {{--@foreach(($tagsArr) as $tag)--}}
    {{--@if($tag && $tagsList[]=$tag) @endif--}}
    {{--@endforeach--}}
    {{--@endif--}}

    {{--</div>--}}
    {{--<!-- **entry-meta-data - Ends** -->--}}
    {{--</li>--}}
    {{--@endforeach--}}
    {{--</ul>--}}
    {{--</div>--}}
    {{--</aside>--}}
    {{--@endif--}}
    {{--<aside class="widget widget_text">--}}
    {{--<h4 class="widgettitle">Special Features</h4>--}}

    {{--<div class="textwidget">--}}
    {{--<!-- **dt-sc-toggle-frame-set - Starts** -->--}}
    {{--<div class="dt-sc-toggle-frame-set">--}}
    {{--<h5 class="dt-sc-toggle-accordion active"><a href="#">Various Options &amp; Features</a></h5>--}}

    {{--<div class="dt-sc-toggle-content">--}}
    {{--<div class="block">--}}
    {{--Maecenas nec odio et ante tincidunt al say tempus. Donec vitae sapien ut libero vene natis--}}
    {{--faucibus. Nullam quis ante.vitae sapien ut libero vene natis faucibus.--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<h5 class="dt-sc-toggle-accordion"><a href="#">Exclusive Documentation</a></h5>--}}

    {{--<div class="dt-sc-toggle-content">--}}
    {{--<div class="block">--}}
    {{--Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of--}}
    {{--classical Latin literature from 45 BC.--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<h5 class="dt-sc-toggle-accordion"><a href="#">Easy Customization</a></h5>--}}

    {{--<div class="dt-sc-toggle-content">--}}
    {{--<div class="block">--}}
    {{--Maecenas nec odio et ante tincidunt al say tempus. Donec vitae sapien ut libero vene natis--}}
    {{--faucibus. Nullam quis ante.vitae sapien ut libero vene natis faucibus.--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<!-- **dt-sc-toggle-frame-set - Ends** -->--}}
    {{--</div>--}}
    {{--</aside>--}}
    {{--@if($tagsList)--}}
    {{--<aside class="widget widget_tag_cloud">--}}
    {{--<h4 class="widgettitle">@lang("backend/Content.view.form.TAGS")</h4>--}}

    {{--<div class="tagcloud">--}}
    {{--@foreach($tagsList as $itemTag)--}}
    {{--<a href="#">{!!$itemTag!!}</a>--}}
    {{--@endforeach--}}
    {{--</div>--}}
    {{--</aside>--}}
    {{--@endif--}}
    {{--<aside class="widget tweetbox">--}}
    {{--<h4 class="widgettitle">Latest Tweets</h4>--}}

    {{--<div class="tweet_list"></div>--}}
    {{--</aside>--}}

</section> <!-- **secondary - Ends** -->