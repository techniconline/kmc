<div class="full-width-section grey">
    <div class="dt-sc-margin50"></div>
    <div class="container">
        <h2 class="aligncenter">Our Portfolio</h2>

        <div class="sorting-container">
            <a data-filter=".all-sort" class="active-sort" href="#">All</a>
            <a data-filter=".photography-sort" href="#">Photography</a>
            <a href="#" data-filter=".outdoors-sort">Outdoors</a>
            <a href="#" data-filter=".fashion-sort">Fashion</a>
            <a data-filter=".graphic-sort" href="#">Graphic Design</a>
        </div>
        <div class="dt-sc-hr-invisible-small"></div>
        <!-- **portfolio-container - Starts** -->
        <div class="portfolio-container no-space">
            <div class="portfolio dt-sc-one-fourth no-space column all-sort outdoors-sort fashion-sort">
                <!-- **portfolio-thumb - Starts** -->
                <div class="portfolio-thumb">
                    <figure>
                        <img src="images/no-image.jpg" alt="image"/>

                        <div class="image-overlay">
                            <a class="zoom" href="#/1200x800&text=Gallery"
                               data-gal="prettyPhoto[gallery]"><span class="fa fa-search"></span></a>
                            <a class="link" href="portfolio-detail-v2.html"><span class="fa fa-link"></span></a>

                            <div class="portfolio-content">
                                <h5><a href="portfolio-detail-v2.html"> Dandelion Falls </a></h5>
                                <span class="fa fa-sort-up"></span>
                            </div>
                        </div>
                    </figure>
                </div>
                <!-- **portfolio-thumb - Ends** -->
            </div>

            <div class="portfolio dt-sc-one-fourth no-space column all-sort fashion-sort graphic-sort">
                <!-- **portfolio-thumb - Starts** -->
                <div class="portfolio-thumb">
                    <figure>
                        <img src="images/no-image.jpg" alt="image"/>

                        <div class="image-overlay">
                            <a class="zoom" href="#/1200x800&text=Gallery"
                               data-gal="prettyPhoto[gallery]"><span class="fa fa-search"></span></a>
                            <a class="link" href="portfolio-detail.html"><span
                                        class="fa fa-link"></span></a>

                            <div class="portfolio-content">
                                <h5><a href="portfolio-detail.html"> Fashion </a></h5>
                                <span class="fa fa-sort-up"></span>
                            </div>
                        </div>
                    </figure>
                </div>
                <!-- **portfolio-thumb - Ends** -->
            </div>

            <div class="portfolio dt-sc-one-fourth no-space column all-sort fashion-sort photography-sort">
                <!-- **portfolio-thumb - Starts** -->
                <div class="portfolio-thumb">
                    <figure>
                        <img src="images/no-image.jpg" alt="image"/>

                        <div class="image-overlay">
                            <a class="zoom" href="#/1200x800&text=Gallery"
                               data-gal="prettyPhoto[gallery]"><span class="fa fa-search"></span></a>
                            <a class="link" href="portfolio-detail-v2.html"><span class="fa fa-link"></span></a>

                            <div class="portfolio-content">
                                <h5><a href="portfolio-detail-v2.html"> World is ours </a></h5>
                                <span class="fa fa-sort-up"></span>
                            </div>
                        </div>
                    </figure>
                </div>
                <!-- **portfolio-thumb - Ends** -->
            </div>

            <div class="portfolio dt-sc-one-fourth no-space column all-sort photography-sort outdoors-sort">
                <!-- **portfolio-thumb - Starts** -->
                <div class="portfolio-thumb">
                    <figure>
                        <img src="images/no-image.jpg" alt="image"/>

                        <div class="image-overlay">
                            <a class="zoom" href="#/1200x800&text=Gallery"
                               data-gal="prettyPhoto[gallery]"><span class="fa fa-search"></span></a>
                            <a class="link" href="portfolio-detail.html"><span
                                        class="fa fa-link"></span></a>

                            <div class="portfolio-content">
                                <h5><a href="portfolio-detail.html"> Love the Life </a></h5>
                                <span class="fa fa-sort-up"></span>
                            </div>
                        </div>
                    </figure>
                </div>
                <!-- **portfolio-thumb - Ends** -->
            </div>

            <div class="portfolio dt-sc-one-fourth no-space column all-sort fashion-sort outdoors-sort">
                <!-- **portfolio-thumb - Starts** -->
                <div class="portfolio-thumb">
                    <figure>
                        <img src="images/no-image.jpg" alt="image"/>

                        <div class="image-overlay">
                            <a class="zoom" href="#/1200x800&text=Gallery"
                               data-gal="prettyPhoto[gallery]"><span class="fa fa-search"></span></a>
                            <a class="link" href="portfolio-detail-v2.html"><span class="fa fa-link"></span></a>

                            <div class="portfolio-content">
                                <h5><a href="portfolio-detail-v2.html"> Snow Time </a></h5>
                                <span class="fa fa-sort-up"></span>
                            </div>
                        </div>
                    </figure>
                </div>
                <!-- **portfolio-thumb - Ends** -->
            </div>

            <div class="portfolio dt-sc-one-fourth no-space column all-sort graphic-sort">
                <!-- **portfolio-thumb - Starts** -->
                <div class="portfolio-thumb">
                    <figure>
                        <img src="images/no-image.jpg" alt="image"/>

                        <div class="image-overlay">
                            <a class="zoom" href="#/1200x800&text=Gallery"
                               data-gal="prettyPhoto[gallery]"><span class="fa fa-search"></span></a>
                            <a class="link" href="portfolio-detail.html"><span
                                        class="fa fa-link"></span></a>

                            <div class="portfolio-content">
                                <h5><a href="portfolio-detail.html"> Black Jade </a></h5>
                                <span class="fa fa-sort-up"></span>
                            </div>
                        </div>
                    </figure>
                </div>
                <!-- **portfolio-thumb - Ends** -->
            </div>

            <div class="portfolio dt-sc-one-fourth no-space column all-sort photography-sort graphic-sort">
                <!-- **portfolio-thumb - Starts** -->
                <div class="portfolio-thumb">
                    <figure>
                        <img src="images/no-image.jpg" alt="image"/>

                        <div class="image-overlay">
                            <a class="zoom" href="#/1200x800&text=Gallery"
                               data-gal="prettyPhoto[gallery]"><span class="fa fa-search"></span></a>
                            <a class="link" href="portfolio-detail-v2.html"><span class="fa fa-link"></span></a>

                            <div class="portfolio-content">
                                <h5><a href="portfolio-detail-v2.html"> Photography </a></h5>
                                <span class="fa fa-sort-up"></span>
                            </div>
                        </div>
                    </figure>
                </div>
                <!-- **portfolio-thumb - Ends** -->
            </div>

            <div class="portfolio dt-sc-one-fourth no-space column all-sort graphic-sort">
                <!-- **portfolio-thumb - Starts** -->
                <div class="portfolio-thumb">
                    <figure>
                        <img src="images/no-image.jpg" alt="image"/>

                        <div class="image-overlay">
                            <a class="zoom" href="#/1200x800&text=Gallery"
                               data-gal="prettyPhoto[gallery]"><span class="fa fa-search"></span></a>
                            <a class="link" href="portfolio-detail.html"><span
                                        class="fa fa-link"></span></a>

                            <div class="portfolio-content">
                                <h5><a href="portfolio-detail.html"> An Adventure </a></h5>
                                <span class="fa fa-sort-up"></span>
                            </div>
                        </div>
                    </figure>
                </div>
                <!-- **portfolio-thumb - Ends** -->
            </div>
        </div>
        <!-- **portfolio-container - Ends** -->
        <div class="dt-sc-hr-invisible"></div>

    </div>
</div>