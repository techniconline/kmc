<div class="full-width-section grey">
    <div class="dt-sc-hr-invisible"></div>
    <div class="container">
        <!-- **dt-sc-partner-carousel--wrapper - Starts** -->
        <div class="dt-sc-partner-carousel-wrapper">
            <ul class="dt-sc-partner-carousel">
                {{\App\Providers\Helpers\Advert\AdvertHelper::showAdverts('center_advert_home_footer')}}
                {{--<li><a href="#"><img src="images/no-image.jpg" alt="image"/></a></li>--}}
                {{--<!--141*142-->--}}
                {{--<li><a href="#"><img src="images/no-image.jpg" alt="image"/></a></li>--}}
                {{--<li><a href="#"><img src="images/no-image.jpg" alt="image"/></a></li>--}}
                {{--<li><a href="#"><img src="images/no-image.jpg" alt="image"/></a></li>--}}
                {{--<li><a href="#"><img src="images/no-image.jpg" alt="image"/></a></li>--}}
            </ul>
        </div>
        <!-- **dt-sc-partner-carousel-wrapper - Starts** -->
    </div>
    <div class="dt-sc-hr-invisible-small"></div>
</div>