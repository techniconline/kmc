<div class="full-width-section">
    <div class="dt-sc-margin50"></div>
    <div class="container">
        <div class="column dt-sc-three-fourth first animate" data-animation="fadeInRight" data-delay="100">
            <div class="hr-title">
                <h3>تماس با ما</h3>

                <div class="title-sep">
                </div>
            </div>
            <form method="post" class="dt-sc-contact-form" action="php/send.php" name="frmcontact">
                <div class="column dt-sc-one-third first">
                    <p><span> <input type="text" placeholder="نام کاربری*" name="txtname" value=""
                                     required/> </span></p>
                </div>
                <div class="column dt-sc-one-third">
                    <p><span> <input type="email" placeholder="پست الکترونیک*" name="txtemail" value=""
                                     required/> </span>
                    </p>
                </div>
                <div class="column dt-sc-one-third">
                    <p><span> <input type="text" placeholder="تلفن*" name="txtphone" value=""/> </span></p>
                </div>
                <p><textarea placeholder="متن" name="txtmessage" required></textarea></p>

                <p><input type="submit" value="ارسال پیام" name="submit"/></p>
            </form>
            <div id="ajax_contact_msg"></div>
        </div>

        <div class="column dt-sc-one-fourth animate" data-animation="fadeInLeft" data-delay="100">
            <div class="hr-title">
                <h3>اطلاعات تماس</h3>

                <div class="title-sep">
                </div>
            </div>
            <p class="dt-sc-contact-detail"> We are located in Melbourne, serving clients worldwide. Shoot
                us an email, give us a call, or fill out our Project Brief if you're interested in getting
                started. </p>
            <!-- **dt-sc-contact-info - Starts** -->
            <div class="dt-sc-contact-info">
                <p><i class="fa fa-location-arrow"></i>تهران، ونک، ملاصدرا، شیراز شمالی، دانشور شرقی، پلاک 26، طبقه
                    هفتم، واحد E7 </p>

                <p><i class="fa fa-phone"></i> 3 - 88618650 21+ </p>

                <p><i class="fa fa-globe"></i> <a href="#"> 1732.ir </a></p>

                <p><i class="fa fa-envelope"></i> <a href="#"> info@1732.ir </a></p>
            </div>
            <!-- **dt-sc-contact-info - Ends** -->
        </div>

    </div>
    <div class="dt-sc-margin80"></div>
</div>