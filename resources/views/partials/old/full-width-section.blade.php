<div class="full-width-section persia">
    <div class="dt-sc-margin65"></div>
    <div class="container">
        <?php $contentsList = \App\Providers\Helpers\Params\ParamsHelper::getStaticContentsByType('index-list'); $counter = 0; ?>
        @if($contentsList)
            @foreach($contentsList as $item)
                <?php if ($counter == 3) {
                    $counter = 0;
                } $counter++; ?>

                <div class="column dt-sc-one-third {!! $counter==1?'first':'' !!}">
                    <div class="dt-sc-ico-content type7">
                        <div class="icon animate" data-animation="fadeInRight" data-delay="100">
                            <h4><a href="{!!route( 'content.show',[$item->url])!!}" title=" {!!$item->link_title!!}">
                                    @if(isset($item->image_content) && \Illuminate\Support\Facades\File::exists(base_path().env('PUBLIC_PATH_URL', 'public').$item->image_content.'/'.$item->id.'-160x80.'.$item->extension))
                                        {!! Html::image($item->image_content.'/'.$item->id.'-160x80.'.$item->extension) !!}

                                    @else
                                        <img src="images/admin1.png" alt="admin"/>
                                    @endif
                                </a></h4>
                        </div>
                        {{--<h4><a href="{!!route( 'content.show',[$item->url])!!}"> {!!$item->link_title!!} </a></h4>--}}

                        {{--<p> {!!$item->short_content!!}</p>--}}
                    </div>
                </div>

                @if($counter==3)
                    <div class="dt-sc-margin30"></div>
                @endif

            @endforeach

        @endif


    </div>
    <div class="dt-sc-margin50"></div>
</div>