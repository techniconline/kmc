<div class="top-bar type6">
    <div class="container top-left">

        <ul class="top-menu">
            <li> {!! ParamsHelper::getStaticContent('ContactUs_HomePage_Top')['body_content'] !!}</li>

        </ul>
        <ul class="dt-sc-social-icons">
            {!! ParamsHelper::socialIcon() !!}
            {!! ParamsHelper::rssIcon() !!}
        </ul>

        <div class="top-right">
            <ul>
                {{--<li>به 1732 خوش امدید</li>--}}
                @if (Auth::user()->guest())
                    <li><i class="fa fa-sign-in"></i>
                        <a href="{{ url('/front/auth/login') }}"
                           title="Design Themes">@lang("home.view.index.Login")</a>
                    </li>
                @else
                    <li><i class="fa fa-edit"></i>
                        <a title="{{ Auth::user()->get()->firstname.' '.Auth::user()->get()->lastname }}"
                           href="{{ url('/front/profile/avatar') }}">
                            {{ Auth::user()->get()->firstname.' '.Auth::user()->get()->lastname }}
                        </a>
                    </li>
                    {{--<li><i class="fa fa-lock"></i>--}}
                    {{--<a href="{{ route('front.profile.password') }}">@lang("home.view.index.Change_Password")</a>--}}
                    {{--</li>--}}
                    {{--<li><i class="fa fa-inbox"></i>--}}
                    {{--<a href="{{ url('/front/profile/messages') }}">@lang("home.view.index.YourMessage")--}}
                    {{--(@lang("home.view.index.New") {!!\App\Profile::checkMessagesUser(Auth::user()->get()->id)!!}--}}
                    {{--)</a>--}}
                    {{--</li>--}}
                    <li><i class="fa fa-sign-out"></i>
                        <a href="{{ url('/front/auth/logout') }}">@lang("home.view.index.Logout")</a>
                    </li>
                @endif
            </ul>
        </div>

    </div>
</div>

