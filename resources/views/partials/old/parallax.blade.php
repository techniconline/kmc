<div class="parallax full-width-section fullwidth-testimonial">
    <div class="container">
        <div class="dt-sc-testimonial-wrapper type2">
            <ul class="dt-sc-testimonial-carousel">
                <li>
                    <div class="dt-sc-testimonial">
                        <h5>" This template is composed of colors. A great deal if you want a colorful &amp;
                            vibrant website for your business or portfolio.<br/>I highly recommend it."</h5>
                        <span>Sarah Mitchell</span>

                        <div class="dt-sc-margin30"></div>
                        <div class="author">
                            <img src="images/no-image.jpg" alt="image"/>
                        </div>
                    </div>
                </li>

                <li>
                    <div class="dt-sc-testimonial">
                        <h5>" This template is composed of colors. A great deal if you want a colorful &amp;
                            vibrant website for your business or portfolio.<br/>I highly recommend it. A
                            great deal."</h5>
                        <span> Sean Bean </span>

                        <div class="dt-sc-margin30"></div>
                        <div class="author">
                            <img src="images/no-image.jpg" alt="image"/> <!--80*80-->
                        </div>
                    </div>
                </li>

                <li>
                    <div class="dt-sc-testimonial">
                        <h5>" This template is composed of colors. A great deal if you want a colorful &amp;
                            vibrant website for your business or portfolio.<br/>I highly recommend it.
                            Website for your business."</h5>
                        <span> Mathew Braveheart </span>

                        <div class="dt-sc-margin30"></div>
                        <div class="author">
                            <img src="images/no-image.jpg" alt="image"/>
                        </div>
                    </div>
                </li>

            </ul>
            <div class="slider-controls">
                <div class="pager">
                    <a href="#"> <span> 1 </span> </a>
                    <a href="#"> <span> 2 </span> </a>
                    <a href="#"> <span> 3 </span> </a>
                </div>

            </div>
        </div>
    </div>
</div>