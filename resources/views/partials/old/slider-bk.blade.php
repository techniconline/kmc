<div class="banner">
    <!-- **Slider Section** -->
    <div id="layerslider_9" class="ls-wp-container"
         style="width:100%;height:645px;max-width:1920px;margin:0 auto;margin-bottom: 0px;">
        <div class="ls-slide" data-ls="slidedelay:10000;transition2d:4;">
            <img src="{{ asset('/images/sliders/lifan-x50.jpg') }}" class="ls-bg" alt="bg3"/>

            <div class="ls-l"
                 style="top:125px;right:45px;font-weight:normal; z-index:5;padding-left:0px;font-size:30px;line-height:46px;color:#fff;white-space: nowrap;"
                 data-ls="offsetxin:0;offsetyin:-100;durationin:2000;delayin:2500;transformoriginin:left 50% 0;offsetxout:0;rotateyout:-90;transformoriginout:left 50% 0;">
                لیفان<span style="font-weight:300;padding-right:20px">ایکس50</span></div>
            <div class="ls-l"
                 style="top:85px;right:45px;font-weight:700; z-index:3;font-size:15px;line-height:26px;color:#E74C3C;white-space: nowrap;"
                 data-ls="offsetxin:-100;durationin:2000;delayin:1500;">راهی جدید، همراهی جدید
            </div>
            <p class="ls-l" style="top:470px;right:20px;white-space: nowrap;"
               data-ls="offsetxin:0;offsetyin:100;delayin:7000;"><a href="#"
                                                                    class="dt-sc-button1 ico-button">اطلاعات بیشتر
                    ...</a></p>

            <p class="ls-l" style="top:470px;right:280px;white-space: nowrap;"
               data-ls="offsetxin:-150;delayin:7500;"><a href="#" class="dt-sc-button2">سفارش در لحظه</a></p>
        </div>
        <div class="ls-slide" data-ls="slidedelay:10000;transition2d:4;">
            <img src="{{ asset('/images/sliders/jac-s5-2.jpg') }}" class="ls-bg" alt="bg3"/>


            <div class="ls-l"
                 style="top:230px;right:830px;font-weight:700; z-index:3;font-size:30px;line-height:26px;color:#01a8df;white-space: nowrap;"
                 data-ls="offsetxin:0;offsetyin:50;durationin:2000;delayin:3000;">جک اس 5
            </div>
            <div class="ls-l" style="top:135px;right:832px;white-space: nowrap;"
                 data-ls="offsetxin:0;offsetyin:100;delayin:3000;"><span
                        style="background: #00a5df; color: #fff; display: inline-block; font-size: 15px; font-weight: 300; line-height: 24px; margin: 10px 0 0; padding: 0 7px 0 7px; position: relative; text-decoration: none; border-radius: 3px; -webkit-border-radius: 3px; -moz-border-radius: 3px; -ms-border-radius: 3px; -o-border-radius: 3px; ">شاسی بلند قدرتمند</span>
            </div>

        </div>
        <div class="ls-slide" data-ls="slidedelay:10000;transition2d:4;">
            <img src="{{ asset('/images/sliders/jac-j5.jpg') }}" class="ls-bg" alt="bg3"/>

            <div class="ls-l"
                 style="top:196px;right:55px;font-weight:300; z-index:5;padding-left:0;font-size:30px;line-height:46px;color:#fff;white-space: nowrap;"
                 data-ls="offsetxin:0;offsetyin:200;durationin:2500;delayin:1500;transformoriginin:left 50% 0;offsetxout:0;rotateyout:-90;transformoriginout:left 50% 0;">
                جی 5 <span style="color:#21c2f8; font-weight:900">اتوماتیک</span> (1800 سی سی)
            </div>
            <div class="ls-l"
                 style="top:296px;right:55px;font-weight:300; z-index:3;font-size:30px;line-height:26px;color:#fff;white-space: nowrap;"
                 data-ls="offsetxin:-100;durationin:2000;delayin:2500;">طراحی <span
                        style="color:#21c2f8;">پینین فارینا ایتالیا</span></div>


        </div>
        <div class="ls-slide" data-ls="slidedelay:10000;transition2d:4;">
            <img src="{{ asset('/images/sliders/lifan-820.jpg') }}" class="ls-bg" alt="bg3"/>

            <div class="ls-l"
                 style="top:150px;left:55px;font-weight:normal; z-index:5;padding-left:0;font-size:30px;line-height:46px;color:#fff;white-space: nowrap;"
                 data-ls="offsetxin:0;offsetyin:-100;durationin:2000;delayin:2500;transformoriginin:left 50% 0;offsetxout:0;rotateyout:-90;transformoriginout:left 50% 0;">
                لیفان<span style="font-weight:300;padding-right:15px">820</span></div>
            <div class="ls-l"
                 style="top:325px;left:55px;font-weight:700; z-index:3;font-size:15px;line-height:26px;color:#AECC70;white-space: nowrap;"
                 data-ls="offsetxin:-100;durationin:2000;delayin:1500;">بزودی ...
            </div>

        </div>
    </div>
</div>