@if($lastNews= \App\Providers\Helpers\Params\ParamsHelper::getLastNewsContent(3))
    <div class="full-width-section persia">
        <div class="dt-sc-margin50"></div>
        <div class="container">
            <h2 class="aligncenter">@lang("News.view.index.RecentPostsNews")</h2>

            <p class="middle-align">@lang("News.view.index.RecentPostsNewsText")</p>

            <div class="dt-sc-hr-invisible-small"></div>
            <?php $counter = 0; $class = ''; $firstClass = ''; ?>
            @foreach($lastNews as $itmNews)
                @if(!fmod($counter,4))
                    <?php $firstClass = 'first'; ?>
                @else
                    <?php $firstClass = ''; ?>
                @endif
                @if(!fmod($counter,2))
                    <?php $class = 'fadeInUp'; ?>
                @else
                    <?php $class = 'fadeInDown'; ?>
                @endif

                <div class="column dt-sc-one-third {!! $firstClass !!} animate" data-animation="{!! $class !!}"
                     data-delay="100">
                    <!-- **entry-post - Starts** -->
                    <article class="entry-post">
                        <div class="entry-meta">
                            <div class="date">
                                <p><span>{!! $itmNews->pDay !!}</span> {!! $itmNews->pMonthName !!}
                                    <br/> {!! $itmNews->pYear !!} </p>
                            </div>
                            <div class="post-comments">
                                <a href="#"><span class="fa fa-comment"></span> 12</a>
                            </div>
                        </div>
                        <div class="entry-post-content">
                            <div class="entry-thumb">
                                <a href="{!! route('news.show',$itmNews->url)  !!}">
                                    @if($itmNews->image_content
                                            && \Illuminate\Support\Facades\File::exists(base_path().env('PUBLIC_PATH_URL', 'public').$itmNews->image_content.'/'.$itmNews->contents_id.'-160x80.'.$itmNews->extension))
                                        <img src="{{asset($itmNews->image_content).'/'.$itmNews->contents_id.'-160x80.'.$itmNews->extension}}"
                                             alt="{!!$itmNews->tags!!}" title=""/>
                                    @else
                                        <img src="images/no-image.jpg" alt="no-image"/>
                                    @endif
                                </a>
                            </div>
                            <div class="entry-detail">
                                <h5>
                                    <a href="{!! route('news.show',$itmNews->url) !!}">{!! str_limit($itmNews->link_title, $limit = 30, $end = '...') !!}</a>
                                </h5>

                                {{--<p><span> {!! str_limit($itmNews->short_content, $limit = 40, $end = '...') !!}</span></p>--}}
                                <a class="dt-sc-button small"
                                   href="{!! route('news.show',$itmNews->url)  !!}">@lang("News.view.index.ReadMore")
                                    <span class="fa fa-long-arrow-left"></span> </a>
                            </div>
                        </div>
                    </article>
                    <!-- **entry-post - Ends** -->
                </div>
                <?php $counter++ ?>
            @endforeach

            @if($counter==3)
                <div class="dt-sc-margin70"></div>
            @endif

        </div>
    </div>
@endif