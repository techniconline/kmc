<section id="welcome">
    <div class="container">
        <div class="section-content">
            <div class="row equal-height-inner">
                <div class="col-sm-12 col-md-6 p-0 p-xs-10 sm-height-auto mt-sm-0" data-margin-top="-200px">
                    <div class="text-black sm-height-auto" data-bg-img="{{ asset('/assets/frontend/images/about/1.jpg') }}">
                        <div class="p-20 pl-50 pr-50">
                            <h3 class="text-white text-uppercase">Join Member Now! <br>Get 40% Off</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas odit unde dolor inventore autem quod vero incidunt labore sunt reprehenderit.</p>
                            <a href="#" class="btn btn-dark btn-md btn-flat mt-10 hvr-bounce-to-left no-border">Read More</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 p-0 p-xs-10 sm-height-auto mt-sm-0" data-margin-top="-200px">
                    <div class="text-black mt-sm-10 sm-height-auto" data-bg-img="{{ asset('/assets/frontend/images/about/2.jpg') }}">
                        <div class="p-20 pl-50 pr-50">
                            <h3 class="text-white text-uppercase">Opening Hours! <br>Check Our Class Here</h3>
                            <div class="line-bottom mb-10"></div>
                            <div class="opening-hours">
                                <p><span class="text-white">Monday To Friday: 9:00 am to 9:00 pm</span> </p>
                                <p><span class="text-white">Saturday: 9:00 am to 5:00 pm</span> </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>