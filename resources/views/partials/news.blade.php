@if($lastNews= \App\Providers\Helpers\Params\ParamsHelper::getLastNewsContent(3))
<section id="news">
    <div class="container">
        <div class="section-content">
            <div class="row">
                <div class="col-md-4 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
                    <div class="news-title bg-black p-40" data-bg-img="images/about/2.jpg">
                        <h2 class="text-uppercase text-theme-colored"> News & <span class="text-white">Blog</span></h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic amet tenetur quis voluptatem, eum ipsam ea temporibus quas ratione necessitatibus! Ea dicta tempore?</p>
                        <a href="#" class="btn btn-theme-colored btn-flat btn-lg text-uppercase mt-20">View All</a>
                    </div>
                </div>
                <div class="col-md-8 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s">
                    <div class="owl-carousel-2col owl-carousel">
                        <?php $counter = 0; $class = ''; $firstClass = ''; ?>
                        @foreach($lastNews as $itmNews)
                            @if(!fmod($counter,4))
                                <?php $firstClass = 'first'; ?>
                            @else
                                <?php $firstClass = ''; ?>
                            @endif
                            @if(!fmod($counter,2))
                                <?php $class = 'fadeInUp'; ?>
                            @else
                                <?php $class = 'fadeInDown'; ?>
                            @endif
                        <div class="item">
                            <article class="post clearfix maxwidth600 mb-sm-30">
                                <div class="entry-header">
                                    <div class="post-thumb thumb">

                                        {{--<img class="img-responsive img-fullwidth" alt="" src="images/news/1.jpg">--}}
                                        @if($itmNews->image_content
                                                && \Illuminate\Support\Facades\File::exists(base_path().env('PUBLIC_PATH_URL', 'public').$itmNews->image_content.'/'.$itmNews->contents_id.'-160x80.'.$itmNews->extension))
                                            <img src="{{asset($itmNews->image_content).'/'.$itmNews->contents_id.'-160x80.'.$itmNews->extension}}"
                                                 alt="{!!$itmNews->title!!}" title="{!! $itmNews->link_title !!}" class="img-responsive img-fullwidth"/>
                                        @else
                                            <img src="images/no-image.jpg" alt="{!!$itmNews->title!!}" title="{!! $itmNews->link_title !!}" class="img-responsive img-fullwidth"/>
                                        @endif
                                    </div>
                                    <div class="entry-meta meta-absolute text-center pl-15 pr-15">
                                        <div class="display-table">
                                            <div class="display-table-cell">
                                                <ul >
                                                    {{--<li><a href="#" class="text-white"><i class="fa fa-thumbs-o-up"></i> 265 <br>--}}
                                                            {{--Likes</a></li>--}}
                                                    <li class="mt-20"><a href="#" class="text-white"><i class="fa fa-comments-o"></i> 72 <br>
                                                            comments</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="entry-content border-1px p-20">
                                    <h5 class="entry-title mt-0 pt-0"><a href="{!! route('news.show',$itmNews->url) !!}" title="{!! str_limit($itmNews->link_title) !!}">{!! str_limit($itmNews->link_title, $limit = 30, $end = '...') !!}</a></h5>
                                    <p class="text-left mb-20 mt-15 font-13">{!! str_limit($itmNews->short_content, $limit = 40, $end = '...') !!}</p>
                                    <a href="{!! route('news.show',$itmNews->url) !!}" class="btn btn-flat btn-dark btn-theme-colored btn-sm pull-left">@lang("News.view.index.ReadMore")</a>
                                    <ul class="list-inline entry-date pull-right font-12 mt-5">
                                        {{--<li><a href="#">Admin |</a></li>--}}
                                        <li>{!! $itmNews->pDay !!} {!! $itmNews->pMonthName !!} {!! $itmNews->pYear !!}</li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                            </article>
                        </div>

                                <?php $counter++ ?>
                            @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
    @endif