<section class="divider bg-lighter">
    <div class="container pt-30 pb-30">
        <div class="section-content">
            <div class="row">
                <div class="owl-carousel-6col clients-logo carousel wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
                    <div class="item"> <a href="#"><img src="images/clients/1.jpg" alt=""></a> </div>
                    <div class="item"> <a href="#"><img src="images/clients/2.jpg" alt=""></a> </div>
                    <div class="item"> <a href="#"><img src="images/clients/3.jpg" alt=""></a> </div>
                    <div class="item"> <a href="#"><img src="images/clients/4.jpg" alt=""></a> </div>
                    <div class="item"> <a href="#"><img src="images/clients/5.jpg" alt=""></a> </div>
                    <div class="item"> <a href="#"><img src="images/clients/6.jpg" alt=""></a> </div>
                    <div class="item"> <a href="#"><img src="images/clients/2.jpg" alt=""></a> </div>
                    <div class="item"> <a href="#"><img src="images/clients/4.jpg" alt=""></a> </div>
                </div>
            </div>
        </div>
    </div>
</section>