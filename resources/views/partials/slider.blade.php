
<section id="home">
    <div class="container-fluid p-0">

        <!-- Slider Revolution Start -->
        <div class="rev_slider_wrapper" dir="ltr">
            <div class="rev_slider" data-version="5.0">
                <ul>

                    <!-- SLIDE 1 -->
                    <li data-index="rs-1" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="images/bg/bg10.jpg" data-rotate="0" data-saveperformance="off" data-title="Web Show" data-description="">
                        <!-- MAIN IMAGE -->
                        <img src="{{ asset('/images/sliders/jac-j5.jpg') }}"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="6" data-no-retina>
                        <!-- LAYERS -->

                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption tp-resizeme text-theme-colored text-uppercase font-playfair font-weight-600"
                             id="rs-1-layer-1"

                             data-x="['right']"
                             data-hoffset="['30']"
                             data-y="['middle']"
                             data-voffset="['-160']"
                             data-fontsize="['100']"
                             data-lineheight="['114']"
                             data-width="none"
                             data-height="none"
                             data-whitespace="nowrap"
                             data-transform_idle="o:1;s:500"
                             data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                             data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                             data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                             data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                             data-start="1000"
                             data-splitin="none"
                             data-splitout="none"
                             data-responsive_offset="on"
                             style="z-index: 5; white-space: nowrap; font-weight:700;">Modern
                        </div>

                        <!-- LAYER NR. 2 -->
                        <div class="tp-caption tp-resizeme font-playfair font-48 text-white font-weight-400 m-0"
                             id="rs-1-layer-2"

                             data-x="['right']"
                             data-hoffset="['30']"
                             data-y="['middle']"
                             data-voffset="['-80']"
                             data-fontsize="['48']"
                             data-lineheight="['78']"
                             data-width="none"
                             data-height="none"
                             data-whitespace="nowrap"
                             data-transform_idle="o:1;s:500"
                             data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                             data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                             data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                             data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                             data-start="1000"
                             data-splitin="none"
                             data-splitout="none"
                             data-responsive_offset="on"
                             style="z-index: 5; white-space: nowrap; font-weight:700;">Hairstyle & Fashion
                        </div>

                        <!-- LAYER NR. 3 -->
                        <div class="tp-caption tp-resizeme text-right font-raleway text-white"
                             id="rs-1-layer-3"

                             data-x="['right']"
                             data-hoffset="['30']"
                             data-y="['middle']"
                             data-voffset="['0']"
                             data-fontsize="['18']"
                             data-lineheight="['34']"
                             data-width="none"
                             data-height="none"
                             data-whitespace="nowrap"
                             data-transform_idle="o:1;s:500"
                             data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                             data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                             data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                             data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                             data-start="1400"
                             data-splitin="none"
                             data-splitout="none"
                             data-responsive_offset="on"
                             style="z-index: 5; white-space: nowrap; font-weight:300;">We Provide Our best opportunity and qualified trainers<br> for best workout yourself as you want
                        </div>

                        <!-- LAYER NR. 4 -->
                        <div class="tp-caption tp-resizeme"
                             id="rs-1-layer-4"

                             data-x="['right']"
                             data-hoffset="['30']"
                             data-y="['middle']"
                             data-voffset="['80']"

                             data-width="none"
                             data-height="none"
                             data-whitespace="nowrap"
                             data-transform_idle="o:1;s:500"
                             data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                             data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                             data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                             data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                             data-start="1600"
                             data-splitin="none"
                             data-splitout="none"
                             data-responsive_offset="on"
                             style="z-index: 5; white-space: nowrap; letter-spacing:1px;"><a class="btn btn-default btn-transparent btn-circled mr-10" href="ajax-load/reservation-form.html" data-target="#BSParentModal" data-toggle="modal">Booking Now</a>
                        </div>
                    </li>

                    <!-- SLIDE 2 -->
                    <li data-index="rs-2" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="images/bg/bg1.jpg" data-rotate="0" data-saveperformance="off" data-title="Web Show" data-description="">
                        <!-- MAIN IMAGE -->
                        <img src="{{ asset('/images/sliders/jac-s5-2.jpg') }}"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="6" data-no-retina>
                        <!-- LAYERS -->

                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption tp-resizeme text-uppercase font-playfair text-center text-white"
                             id="rs-2-layer-1"

                             data-x="['center']"
                             data-hoffset="['0']"
                             data-y="['middle']"
                             data-voffset="['-80']"
                             data-fontsize="['64','64','54','24']"
                             data-lineheight="['95']"

                             data-width="none"
                             data-height="none"
                             data-whitespace="nowrap"
                             data-transform_idle="o:1;s:500"
                             data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                             data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                             data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                             data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                             data-start="1000"
                             data-splitin="none"
                             data-splitout="none"
                             data-responsive_offset="on"
                             style="z-index: 5; white-space: nowrap; font-weight:700;">Best Hair Saloon
                        </div>

                        <!-- LAYER NR. 2 -->
                        <div class="tp-caption tp-resizeme text-center font-raleway text-white"
                             id="rs-2-layer-2"

                             data-x="['center']"
                             data-hoffset="['0']"
                             data-y="['middle']"
                             data-voffset="['0']"
                             data-fontsize="['18']"
                             data-lineheight="['34']"

                             data-width="none"
                             data-height="none"
                             data-whitespace="nowrap"
                             data-transform_idle="o:1;s:500"
                             data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                             data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                             data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                             data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                             data-start="1200"
                             data-splitin="none"
                             data-splitout="none"
                             data-responsive_offset="on"
                             style="z-index: 5; white-space: nowrap; font-weight:500;">We Provide Our best opportunity and qualified trainers<br> for best workout yourself as you want
                        </div>

                        <!-- LAYER NR. 3 -->
                        <div class="tp-caption tp-resizeme"
                             id="rs-2-layer-3"

                             data-x="['center']"
                             data-hoffset="['0']"
                             data-y="['middle']"
                             data-voffset="['80']"

                             data-width="none"
                             data-height="none"
                             data-whitespace="nowrap"
                             data-transform_idle="o:1;s:500"
                             data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                             data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                             data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                             data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                             data-start="1400"
                             data-splitin="none"
                             data-splitout="none"
                             data-responsive_offset="on"
                             style="z-index: 5; white-space: nowrap; letter-spacing:1px;"><a class="btn btn-default btn-lg btn-circled mr-10" href="#">Signup Now</a> <a class="btn btn-colored btn-lg btn-circled btn-theme-colored" href="#">View Details</a>
                        </div>
                    </li>

                    <!-- SLIDE 3 -->
                    <li data-index="rs-3" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="images/bg/bg16.jpg" data-rotate="0" data-saveperformance="off" data-title="Web Show" data-description="">
                        <!-- MAIN IMAGE -->
                        <img src="{{ asset('/images/sliders/jac-s5.jpg') }}"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="6" data-no-retina>
                        <!-- LAYERS -->

                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption tp-resizeme text-black-333 text-uppercase font-playfair font-weight-600"
                             id="rs-3-layer-1"

                             data-x="['left']"
                             data-hoffset="['30']"
                             data-y="['middle']"
                             data-voffset="['-160']"
                             data-fontsize="['100']"
                             data-lineheight="['114']"
                             data-width="none"
                             data-height="none"
                             data-whitespace="nowrap"
                             data-transform_idle="o:1;s:500"
                             data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                             data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                             data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                             data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                             data-start="1000"
                             data-splitin="none"
                             data-splitout="none"
                             data-responsive_offset="on"
                             style="z-index: 5; white-space: nowrap; font-weight:700;">Men<span class="text-theme-colored">'s</span>
                        </div>

                        <!-- LAYER NR. 2 -->
                        <div class="tp-caption tp-resizeme font-playfair font-48 text-black-333 font-weight-400 m-0"
                             id="rs-3-layer-2"

                             data-x="['left']"
                             data-hoffset="['30']"
                             data-y="['middle']"
                             data-voffset="['-80']"
                             data-fontsize="['48']"
                             data-lineheight="['78']"
                             data-width="none"
                             data-height="none"
                             data-whitespace="nowrap"
                             data-transform_idle="o:1;s:500"
                             data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                             data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                             data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                             data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                             data-start="1000"
                             data-splitin="none"
                             data-splitout="none"
                             data-responsive_offset="on"
                             style="z-index: 5; white-space: nowrap; font-weight:700;">Fashionable Haircuts
                        </div>

                        <!-- LAYER NR. 3 -->
                        <div class="tp-caption tp-resizeme font-raleway text-black-333"
                             id="rs-3-layer-3"

                             data-x="['left']"
                             data-hoffset="['30']"
                             data-y="['middle']"
                             data-voffset="['0']"
                             data-fontsize="['18']"
                             data-lineheight="['34']"
                             data-width="none"
                             data-height="none"
                             data-whitespace="nowrap"
                             data-transform_idle="o:1;s:500"
                             data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                             data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                             data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                             data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                             data-start="1400"
                             data-splitin="none"
                             data-splitout="none"
                             data-responsive_offset="on"
                             style="z-index: 5; white-space: nowrap; font-weight:300;">We Provide Our best opportunity and qualified trainers<br> for best workout yourself as you want
                        </div>

                        <!-- LAYER NR. 4 -->
                        <div class="tp-caption tp-resizeme"
                             id="rs-3-layer-4"

                             data-x="['left']"
                             data-hoffset="['30']"
                             data-y="['middle']"
                             data-voffset="['80']"

                             data-width="none"
                             data-height="none"
                             data-whitespace="nowrap"
                             data-transform_idle="o:1;s:500"
                             data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                             data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                             data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                             data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                             data-start="1600"
                             data-splitin="none"
                             data-splitout="none"
                             data-responsive_offset="on"
                             style="z-index: 5; white-space: nowrap; letter-spacing:1px;"><a class="btn btn-theme-colored btn-circled mr-10" href="#">Booking Now</a>
                        </div>
                    </li>

                </ul>
            </div><!-- end .rev_slider -->
        </div>
        <!-- end .rev_slider_wrapper -->

        <!-- Slider Revolution Ends -->
    </div>
</section>