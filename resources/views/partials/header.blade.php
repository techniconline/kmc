<header id="header" class="header">
    <div class="header-nav navbar-fixed-top navbar-dark navbar-transparent navbar-sticky-animated animated-active">
        <div class="header-nav-wrapper">
            <div class="container">
                <nav id="menuzord-right" class="menuzord">
                    <a class="menuzord-brand pull-left flip" href="/">
                        <img src="{{ asset('/assets/frontend/images/logo.png') }}" alt="logo"/>
                    </a>

                        {!! MenusHelper::frontEndMenusMegaBuild($frontend_menus,2,true) !!}
                </nav>
            </div>
        </div>
    </div>
</header>
