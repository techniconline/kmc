<footer id="footer" class="footer divider parallax layer-overlay overlay-dark-9" data-bg-img="{{ asset('/assets/frontend/images/bg/bg7.jpg') }}" data-parallax-ratio="0.7">
    <div class="container pt-70 pb-40">
        <div class="row border-bottom-black">
            <div class="col-sm-6 col-md-3">
                <div class="widget dark">
                    {{--{!! ParamsHelper::getStaticContent('ContactFooter')['body_content'] !!}--}}
                    <img class="mt-10 mb-20" alt="" src="{{ asset('/assets/frontend/images/logo-wide-white.png') }}">
                    <p>203, Envato Labs, Behind Alis Steet, Melbourne, Australia.</p>
                    <ul class="list-inline mt-5">
                        <li class="m-0 pl-10 pr-10"> <i class="fa fa-phone text-theme-colored mr-5"></i> <a class="text-gray" href="#">123-456-789</a> </li>
                        <li class="m-0 pl-10 pr-10"> <i class="fa fa-envelope-o text-theme-colored mr-5"></i> <a class="text-gray" href="#">contact@yourdomain.com</a> </li>
                        <li class="m-0 pl-10 pr-10"> <i class="fa fa-globe text-theme-colored mr-5"></i> <a class="text-gray" href="#">www.yourdomain.com</a> </li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                @if($lastNews= \App\Providers\Helpers\Params\ParamsHelper::getLastNewsContent(3))
                <div class="widget dark">
                    <h5 class="widget-title line-bottom">@lang("home.Footer.LastNews.Title")</h5>
                    <ul class="list angle-double-right list-border">
                        @foreach($lastNews as $itmNews)
                        <li><a href="{!! route('news.show',$itmNews->url)  !!}" title="{!! str_limit($itmNews->link_title) !!}">{!! str_limit($itmNews->link_title, $limit = 35 , $end = '...') !!}</a></li>
                        @endforeach
                    </ul>
                </div>
                    @endif
            </div>
            <div class="col-sm-12 col-md-6">
                <div class="widget dark">
                    <h5 class="widget-title line-bottom">راهنمای 1732</h5>
                    <div class="opening-hourse">
                        <ul class="list-border">
                            <li class="clearfix"><a class="value pull-right" href="#" title="">چگونه در سایت مشارکت داشته باشیم؟</a></li>
                            <li class="clearfix"><a class="value pull-right" href="#" title="">در وب نوشته ها چه مطالبی قابل ثبت است؟</a></li>
                            <li class="clearfix"><a class="value pull-right" href="#" title="">نظرات خود را چگونه مطرح کنیم؟</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-30">
            <div class="col-md-5">
                <div class="widget dark">
                    <h5 class="widget-title mb-10">Subscribe Us</h5>
                    <!-- Mailchimp Subscription Form Starts Here -->
                    <form id="mailchimp-subscription-form-footer" class="newsletter-form">
                        <div class="input-group">
                            <input type="email" value="" name="EMAIL" placeholder="Your Email" class="form-control input-lg font-16" data-height="45px" id="mce-EMAIL-footer" style="height: 45px;">
                <span class="input-group-btn">
                  <button data-height="45px" class="btn btn-colored btn-theme-colored btn-xs m-0 font-14" type="submit">Subscribe</button>
                </span>
                        </div>
                    </form>

                </div>
            </div>
            <div class="col-md-3 col-md-offset-1">
                <div class="widget dark">
                    <h5 class="widget-title mb-10">Call Us Now</h5>
                    <div class="text-gray">
                        +61 3 1234 5678 <br>
                        +12 3 1234 5678
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="widget dark">
                    <h5 class="widget-title mb-10">Connect With Us</h5>
                    <ul class="social-icons icon-dark icon-circled icon-sm">
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-skype"></i></a></li>
                        <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom bg-black-333">
        <div class="container pt-20 pb-20">
            <div class="row">
                <div class="col-md-6">
                    <p class="font-11 text-black-777 m-0">Copyright &copy;2015 ThemeMascot. All Rights Reserved</p>
                </div>
                <div class="col-md-6 text-right">
                    <div class="widget no-border m-0">
                        <ul class="list-inline sm-text-center mt-5 font-12">
                            <li>
                                <a href="#">FAQ</a>
                            </li>
                            <li>|</li>
                            <li>
                                <a href="#">Help Desk</a>
                            </li>
                            <li>|</li>
                            <li>
                                <a href="#">Support</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>