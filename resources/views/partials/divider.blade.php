<section class="divider parallax layer-overlay overlay-black" data-bg-img="images/bg/bg2.jpg" data-parallax-ratio="0.7">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
                <div class="section-content text-center">
                    <h2 class="text-white letter-space-4 font-36">Your Favourite <span class="text-theme-colored">Items</span></h2>
                    <h3 class="text-white text-uppercase font-weight-300 mt-0">I have no regrets about launching Salon. for the life of me, I can't imagine doing anything else</h3>
                    <a href="default.htm" class="btn btn-theme-colored btn-flat mt-10 btn-xl text-uppercase"> Contact Us</a>
                </div>
            </div>
        </div>
    </div>
</section>