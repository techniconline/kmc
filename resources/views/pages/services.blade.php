@extends('app')

@section('browser_title')
    @lang("pages.view.services.Title")
@endsection

@section('styles')
@endsection

@section('content')

    <section class="fullscreen" id="servicesSection">

        <div class="articleImage" id="Service1-image"
             style="background-image: url('{{ asset('/assets/frontend/images_page/serviceBack-parking.jpg')}}');"></div>
        <div class="articleImage" id="Service2-image"
             style="background-image: url('{{ asset('/assets/frontend/images_page/serviceBack-tv-B.jpg')}}');"></div>
        <div class="articleImage" id="Service3-image"
             style="background-image: url('{{ asset('/assets/frontend/images_page/serviceBack-security-B.jpg')}}');"></div>
        <div class="articleImage" id="Service4-image"
             style="background-image: url('{{ asset('/assets/frontend/images_page/serviceBack-comfort-B.jpg')}}');"></div>
        <div class="articleImage" id="Service5-image"
             style="background-image: url('{{ asset('/assets/frontend/images_page/serviceBack-gym.jpg')}}');"></div>
        <div class="articleImage" id="Service6-image"
             style="background-image: url('{{ asset('/assets/frontend/images_page/serviceBack-insurrance.jpg')}}');"></div>
        <div class="articleImage" id="Service7-image"
             style="background-image: url('{{ asset('/assets/frontend/images_page/serviceBack-restaurant-B.jpg')}}');"></div>

        <div class="container">
            <a class="item serviceBox" id="Service1">
                <svg class="picto pictoShow" id="serviceIconA" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50"
                     enable-background="new 0 0 50 50">
                    <g stroke="#231F20" stroke-width="1.44" stroke-linecap="round" stroke-linejoin="round"
                       stroke-miterlimit="22.926" fill="none">
                        <path d="M36.7 30v-10.4M13.3 19.6v8.6"/>
                        <path d="M41 18.1l-.1.2.1.1-2.4 2.7-13.6-9.6-13.6 9.6-2.4-2.7.1-.1-.1-.2 15.9-11.6.1.1.1-.1 15.9 11.6zM12.2 39.8c.8 1.2 2.1 1.9 3.5 1.9s2.7-.7 3.5-1.8h12.5c.8 1.1 2.1 1.8 3.5 1.8s2.7-.7 3.5-1.9l1.8-.2c.5 0 .8-.4.9-.8l.8-5.6c0-.3 0-.5-.2-.8-.4-.5-2.1-1.7-10.3-3-2.8-2.5-4.9-3.3-5.7-3.4-1.1-.1-2.9-.2-4.7-.2-1 0-2.9 0-3.8.2-1.7.4-5.2 3-6.3 3.9l-2.5.5c-.5.1-.9.6-.8 1.1l.8 7.3c.1.5.4.8.9.9l2.6.1zM17.8 37.9c0-1.2-1-2.1-2.1-2.1-1.2 0-2.1 1-2.1 2.1M15.9 31.5h12.5c.2 0 .4-.1.5-.3.1-.2 0-.4-.1-.6-2.1-1.7-3.9-2.2-4-2.2M37.2 37.9c0-1.2-1-2.1-2.1-2.1-1.2 0-2.1 1-2.1 2.1"/>
                    </g>
                </svg>
            </a><!--
			--><a class="item serviceBox" id="Service2">
                <svg class="picto pictoShow" id="serviceIconB" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50"
                     enable-background="new 0 0 50 50">
                    <g stroke="#231F20" stroke-width="1.44" stroke-linecap="round" stroke-linejoin="round"
                       stroke-miterlimit="22.926" fill="none">
                        <path d="M31.1 22.9l-9.1-5.4v10.6l9.1-5.2zm10.2-10.5h-29.5v20.8h29.5v-20.8zm0 25.2h-29.5"/>
                        <path d="M18 36.4c.6 0 1.1.5 1.1 1.1 0 .6-.5 1.1-1.1 1.1-.6 0-1.1-.5-1.1-1.1.1-.6.6-1.1 1.1-1.1z"/>
                    </g>
                </svg>
            </a><!--
			--><a class="item serviceBox" id="Service3">
                <svg class="picto pictoShow" id="serviceIconC" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50"
                     enable-background="new 0 0 50 50">
                    <g stroke="#231F20" stroke-width="1.44" stroke-linecap="round" stroke-linejoin="round"
                       stroke-miterlimit="22.926" fill="none">
                        <path d="M25 40.8c-8.5-3.7-14.2-10.6-14.2-18.6v-12.3c1.3 1.5 3.8 2.5 6.8 2.5 3.5 0 6.5-1.5 7.4-3.4.9 2 3.8 3.4 7.4 3.4 2.9 0 5.5-1 6.8-2.5v12.3c0 8-5.7 14.9-14.2 18.6zM19 24l5.5 6.4 7.8-11.9"/>
                    </g>
                </svg>
            </a><!--
			--><a class="item serviceBox" id="Service4">
                <svg class="picto pictoShow" id="serviceIconD" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50"
                     enable-background="new 0 0 50 50">
                    <g stroke="#000" stroke-width="1.44" stroke-linecap="round" stroke-linejoin="round"
                       stroke-miterlimit="22.926" fill="none">
                        <path d="M36.7 35l-6-16.4c-2.8-7.9-7.9-10.4-12.1-9.6-7.7 1.3-9.4 10-7.3 16.1"/>
                        <path d="M14.4 33.3c1 0 1.9.8 1.9 1.9 0 1-.8 1.9-1.9 1.9-1 0-1.9-.8-1.9-1.9.1-1 .9-1.9 1.9-1.9zM36.8 34.8l5.9.2c.2 0 .5.1.6.3l.3.8c.1.2-.1.5-.3.6l-10.4 3.4c-.2.1-.5-.1-.6-.3l-.3-.8c-.1-.2.1-.4.3-.6l4.5-3.6zM9.7 31.6c-.5-2.6-.2-4.4 1.5-6.6 7.8-6.2 20.2 4.6 20.2 9.1 0 4.4-8.4 4.4-11.7 4.4h-.9"/>
                        <path d="M14.4 30.3c2.7 0 4.9 2.2 4.9 4.9s-2.2 4.9-4.9 4.9-4.9-2.2-4.9-4.9c.1-2.7 2.2-4.9 4.9-4.9z"/>
                    </g>
                </svg>
            </a><!--
			--><a class="item serviceBox" id="Service5">
                <svg class="picto pictoShow" id="serviceIconE" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50"
                     enable-background="new 0 0 50 50">
                    <g stroke="#231F20" stroke-width="1.44" stroke-linecap="round" stroke-linejoin="round"
                       stroke-miterlimit="22.926" fill="none">
                        <path d="M12.6 40h21.3M15.2 12.4l3 7.9M37 11.2l-5.2 15.9M34.1 11.3h7.3l1.9-4.5M19.5 12.5h-9.5"/>
                        <path d="M19.7 26.3c1.5 0 2.8 1.3 2.8 2.8s-1.3 2.8-2.8 2.8c-1.5 0-2.8-1.3-2.8-2.8s1.2-2.8 2.8-2.8zM21.2 31.5l3.4 3.7M21.5 35.4h6.3"/>
                        <path d="M19.8 20.4c2.5 0 4.8.9 6.5 2.4 1.1 1 7.6 6.5 8.1 6.9.9.8 1.5 2 1.5 3.3 0 1.3-.6 2.4-1.5 3.3-.9.8-2.2 1.4-3.6 1.4h-11c-2.6 0-4.9-1-6.6-2.5-1.7-1.6-2.7-3.7-2.7-6.1 0-2.4 1-4.5 2.7-6.1 1.7-1.6 4-2.6 6.6-2.6z"/>
                    </g>
                </svg>
            </a><!--
			--><a class="item serviceBox" id="Service6">
                <svg class="picto pictoShow" id="serviceIconF" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50"
                     enable-background="new 0 0 50 50">
                    <g stroke="#231F20" stroke-width="1.44" stroke-linecap="round" stroke-linejoin="round"
                       stroke-miterlimit="22.926" fill="none">
                        <path d="M30.7 27.1c3.2 0 5.8 2.6 5.8 5.8 0 3.2-2.6 5.8-5.8 5.8-3.2 0-5.8-2.6-5.8-5.8 0-3.2 2.6-5.8 5.8-5.8zM41 18.8v13.7c0 .6-.5 1.2-1.2 1.2l-2.9.1m-12.5 0l-14.2-.1c-.6 0-1.2-.5-1.2-1.2v-20.2c0-.6.5-1.2 1.2-1.2h23.2m7.6 7.7h-6.5c-.6 0-1.2-.5-1.2-1.2v-6.5l7.7 7.7zm-27.9-2.5h16.1m-16.1 3.7h16.1m-16.1 3.6h16.1"/>
                        <path d="M30.7 30.1c1.5 0 2.8 1.2 2.8 2.8s-1.2 2.8-2.8 2.8c-1.5 0-2.8-1.2-2.8-2.8s1.2-2.8 2.8-2.8zM25.7 35.8l-2.7 4.9 5 2.4 2.7-4.1 2.7 4.1 5.1-2.4-2.7-5.2"/>
                    </g>
                </svg>
            </a><!--
			--><a class="item serviceBox last" id="Service7">
                <svg class="picto pictoShow" id="serviceIconG" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50"
                     enable-background="new 0 0 50 50">
                    <g stroke="#231F20" stroke-width="1.44" stroke-linecap="round" stroke-linejoin="round"
                       stroke-miterlimit="22.926" fill="none">
                        <path d="M36.2 30.6c2-.1 5.9-1 6.1-3.2h-34c.3 2.2 4.2 3.1 6.1 3.2h21.8zM10.7 27.1v-.4c0-18.4 29.2-18.4 29.2 0v.4M34.8 40.6l-10.4 2.9M31.7 40.4c.4-1.3.4-2.3 2-4.9.5-.8 2.1-2.4 2.5-3.2 1.5-2-2.5-1.5-3.6 0-.6.8-.8 1.4-1.5 1.8-2.4 1.3-2.6-.4-4.8-2.7-1.5-1.5-4.3-.3-3.1.8M25.1 34.3l-2.8-2.9c-1.5-1.5-3.8.1-2.5 1.3M22.8 35.2l-3.1-2.6-1-1c-1.8-1.7-3.9.3-1.5 2.2l4.1 3.3c.4.3 5.3 3.7 5.2 5.8M25.2 15.4c5.3 0 10.6 2.9 11.7 8.8"/>
                        <path d="M25.3 9c.8 0 1.4.7 1.4 1.5s-.6 1.5-1.4 1.5-1.4-.7-1.4-1.5.7-1.5 1.4-1.5z"/>
                    </g>
                </svg>
            </a>

            <article class="fill info first">
                <div class="post atgLTR">
                    <a name="Services"></a>
                    {!! ParamsHelper::getStaticContent('ServicesText1')['body_content'] !!}


                </div>
                <br class="clearfloat"></article>

            <article id="Service1-info" class="fill info">
                <div class="post atgLTR">
                    <a name="Service Box 1"></a>
                    {!! ParamsHelper::getStaticContent('ServicesText2')['body_content'] !!}

                </div>
                <br class="clearfloat"></article>

            <article id="Service2-info" class="fill info">
                <div class="post atgLTR">
                    <a name="Service Box 2"></a>
                    {!! ParamsHelper::getStaticContent('ServicesText3')['body_content'] !!}

                </div>
                <br class="clearfloat"></article>

            <article id="Service3-info" class="fill info">
                <div class="post atgLTR">
                    <a name="Service Box 3"></a>
                    {!! ParamsHelper::getStaticContent('ServicesText4')['body_content'] !!}

                </div>
                <br class="clearfloat"></article>

            <article id="Service4-info" class="fill info">
                <div class="post atgLTR">
                    <a name="Service Box 4"></a>
                    {!! ParamsHelper::getStaticContent('ServicesText5')['body_content'] !!}

                </div>
                <br class="clearfloat"></article>

            <article id="Service5-info" class="fill info">
                <div class="post atgLTR">
                    <a name="Service Box 5"></a>
                    {!! ParamsHelper::getStaticContent('ServicesText6')['body_content'] !!}

                </div>
                <br class="clearfloat"></article>

            <article id="Service6-info" class="fill info">
                <div class="post atgLTR">
                    <a name="Service Box 6"></a>
                    {!! ParamsHelper::getStaticContent('ServicesText7')['body_content'] !!}
                </div>
                <br class="clearfloat"></article>

            <article id="Service7-info" class="fill info">
                <div class="post atgLTR">
                    <a name="Service Box 7"></a>
                    {!! ParamsHelper::getStaticContent('ServicesText8')['body_content'] !!}

                </div>
                <br class="clearfloat"></article>

        </div>
    </section>


@endsection

@section('scripts')
@endsection

@section('inline-scripts')
@endsection
