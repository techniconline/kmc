@extends('app')

@section('browser_title')
    @lang("pages.view.about.Title")
@endsection

@section('styles')
@endsection

@section('content')


    <section class="fullscreen" id="aboutusSection">
        <div class="container">
            <article class="fill" style="width: 45%">
                <div class="post atgLTR">
                    {!! ParamsHelper::getStaticContent('AboutText1')['body_content'] !!}

                </div>
                <br class="clearfloat">
            </article>
            <article class="fill" style="width: 45%">
                <div class="post atgLTR">
                    {!! ParamsHelper::getStaticContent('AboutText2')['body_content'] !!}

                </div>
                <br class="clearfloat">

            </article>
        </div>
    </section>


@endsection

@section('scripts')
@endsection

@section('inline-scripts')
@endsection
