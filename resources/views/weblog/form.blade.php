@extends('app')
<?php  $langDetails = app('activeLangDetails');?>

@section('browser_title')
{{--    {!! $content->browser_title !!}--}}
@endsection

@section('styles')
@if($langDetails['is_rtl'])

@else

@endif

@endsection
@section('content')

        <!-- Primary Starts -->
<div id="main">
    <div class="parallax full-width-bg">
        <div class="container">
            <div class="main-title">
                <h1>@lang('webLog.view.index.CreateWebLog')</h1>

            </div>
        </div>
    </div>
    <div class="dt-sc-margin100"></div>
    <!-- Container starts-->
    <div class="container">
        @if (Session::has('flash_notification.message'))
            <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                {{--<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>--}}
                {{ Session::get('flash_notification.message') }}
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXTRAS PORTLET-->
                <div class="portlet box blue-hoki">
                    <div class="portlet-body form">
                        @if(isset($content))
                            {!!Form::open(['route' => [$locale.'weblog.update',$content['id']],'method' => 'PUT','class'=>'form-horizontal form-bordered','files'=> true])!!}
                            {!!Form::hidden('content_id',$content['id'] or null,['id'=>'content-id'])!!}
                        @else
                            {!!Form::open(['route' => $locale.'weblog.store','class'=>'form-horizontal form-bordered','files'=> true])!!}
                        @endif
                        <div class="form-body">
                            <div class="form-group">
                                {!!Form::label('link_title',trans("backend/Content.view.form.Link_Title"),['class'=>'control-label col-md-2'])!!}
                                <div class="col-md-10">
                                    {!!Form::text('link_title',isset($content['link_title'])?$content['link_title']:null,array('class'=>'form-control'))!!}
                                    {!!$errors->first('link_title','<span class="error">:message</span>')!!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!!Form::label('image_content',trans("backend/Content.view.form.ImageContent"),['class'=>'control-label col-md-2'])!!}
                                <div class="col-md-4">
                                    {!! Form::file('image_content',['onchange'=>"readURL(this)"]) !!}
                                    {!!$errors->first('image_content','<span class="error">:message</span>')!!}
                                </div>
                                <div class="col-md-4">
                                    @if(isset($content) && \Illuminate\Support\Facades\File::exists(base_path().env('PUBLIC_PATH_URL', 'public').$content['image_content'].'/'.$content['id'].'-160x80.'.$content['extension']))
                                        {!! Html::image($content['image_content'].'/'.$content['id'].'-160x80.'.$content['extension']) !!}
                                    @else
                                        <img class="image_content col-md-12" src="#" alt=""/>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                {!!Form::label('category_id',trans("backend/Product.view.create.Category"),['class'=>'control-label col-md-2'])!!}
                                <div class="col-md-10">
                                    {!! Form::select('categories[]',$catList,null,array('class'=>'form-control input-small')) !!}

                                    {!!$errors->first('categories','<span class="error">:message</span>')!!}
                                </div>
                            </div>


                            <div class="form-group">
                                {!!Form::label('body_content',trans("backend/Content.view.form.Body_Contents"),['class'=>'control-label col-md-2'])!!}
                                <div class="col-md-10">
                                    {{--                                    {!!Form::editor('body_content',isset($content['body_content'])?$content['body_content']:null,'')!!}--}}
                                    <textarea class="ckeditor form-control" name="body_content" rows="6"></textarea>
                                    {{--<textarea name="body_content" class="wysihtml5 form-control" rows="11">{!! isset($content['body_content'])?$content['body_content']:null !!}</textarea>--}}
                                    {!!$errors->first('body_content','<span class="error">:message</span>')!!}
                                </div>
                            </div>

                            {{--<div class="form-group">--}}

                            {{--{!!Form::label('type',trans("backend/Content.view.form.Type"),['class'=>'control-label col-md-2'])!!}--}}
                            {{--<div class="col-md-3">--}}
                            {{--{!! Form::select('type',[--}}
                            {{--'weblog'=>trans("backend/Content.view.form.weblog")--}}
                            {{--],isset($content['type'])?$content['type']:'weblog',array('class'=>'form-control input-small')) !!}--}}
                            {{--{!!$errors->first('type','<span class="error">:message</span>')!!}--}}
                            {{--</div>--}}

                            {{--</div>--}}

                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-6">
                                    {{--                                    {!!Form::select('lang', $listLang,app('getLocaleDefault'),['class'=>'btn form-control input-small select2me'])!!}--}}
                                    <button type="submit" class=""
                                            style="color: #000000;">{!! isset($content)? trans("backend/Content.view.form.Update") : trans("backend/Content.view.form.Create") !!}</button>
                                    <button type="button" class=""
                                            style="color: #000000;">@lang("backend/Content.view.form.Cancel")</button>
                                </div>
                            </div>
                        </div>

                        {!!Form::close()!!}

                    </div>
                </div>
            </div>
        </div>


    </div>
</div>

@endsection


@section('scr1')
    <script src="{{ asset('/assets/frontend/js/jquery.jcarousel.min.js') }}" type="text/javascript"></script>
@endsection

@section('scr2')
    <script src="{{ asset('/assets/frontend/js/jquery.smartresize.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/assets/frontend/js/jquery.scrollTo.js') }}" type="text/javascript"></script>
@endsection
@section('scr-retina')
    <script src="{{ asset('/assets/frontend/js/retina.js') }}" type="text/javascript"></script>
@endsection

@section('scripts')

    {{--    <script src="{{ asset('/assets/theme/assets/admin/pages/scripts/components-editors.js') }}" type="text/javascript"></script>--}}
    <script src="{{ asset('/assets/theme/assets/global/plugins/ckeditor/ckeditor.js') }}"
            type="text/javascript"></script>
    {{--    <script src="{{ asset('/assets/js/backend/category.js') }}" type="text/javascript"></script>--}}

@endsection

@section('inline-scripts')

    <script>
        jQuery(document).ready(function () {

//            CategoriesList.init();

//            ComponentsEditors.init();
//            ComponentsEditors.notePad('short_content', 150);
//            $('.tags').tagsInput({
//                width: 'auto',
//                'onAddTag': function () {
//                    //alert(1);
//                }
//            });
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('.image_content').attr('src', e.target.result)
                            .css("width", "150px")
                            .css("height", "70px");
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>

@endsection
