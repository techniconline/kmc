@extends('app')
<?php  $langDetails = app('activeLangDetails');?>

@section('browser_title')
{{--    {!! $content->browser_title !!}--}}
@endsection

@section('styles')

@endsection
@section('content')

        <!-- Primary Starts -->
<div id="main">
    <div class="parallax full-width-bg">
        <div class="container">
            <div class="main-title">
                <h1>@lang('webLog.view.index.RecentPostsWebLogText')</h1>

                <div class="breadcrumb">
                    <a href="/">@lang('home.view.index.HomePage')</a>
                    <span class="fa fa-angle-left"></span>
                    <span class="current">@lang('webLog.view.index.RecentPostsWebLog')</span>
                </div>
            </div>
        </div>
    </div>
    <div class="dt-sc-margin100"></div>
    <!-- Container starts-->
    <div class="container">
        @if (Session::has('flash_notification.message'))
            <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                {{--<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>--}}
                {{ Session::get('flash_notification.message') }}
            </div>
        @endif
        <div>
            <a href="{{URL::route($locale.'weblog.create')}}">@lang('webLog.view.index.CreateWebLog')</a>

        </div>
        <!-- **secondary - Starts** -->
        @include('partials.sidebar')
                <!-- **secondary - Ends** -->
        <!-- Primary Starts -->
        <section id="primary" class="with-left-sidebar page-with-sidebar">

            <!-- **Blog-post - Starts** -->
            @foreach($webLog as $bLog)
                <article class="blog-post type3">
                    <!-- **entry-meta - Starts** -->
                    <div class="entry-meta">
                        <div class="date">
                            <p>
                                <span>{{$bLog->pDay}} <p style="font-size: 12px">{{$bLog->pDayName}}</p> </span>
                            <span><p style="font-size: 12px">{{$bLog->pMonthName}}</p> </span>
                            <span><p style="font-size: 12px">{{$bLog->pYear}}</p> </span>
                            </p>
                        </div>
                        <div class="post-comments">
                            <a href="#"><span
                                        class="fa fa-comment"></span> {!!$bLog->count_comments?$bLog->count_comments:0!!}
                            </a>
                        </div>
                    </div>
                    <!-- **entry-meta - Ends** -->
                    <!-- **entry-thumb - Starts** -->
                    <div class="entry-thumb">
                        <h4><a href="{!! route($locale.'weblog.show',$bLog->url)  !!}">
                                @if($bLog->image_content
                                && \Illuminate\Support\Facades\File::exists(base_path().env('PUBLIC_PATH_URL', 'public').$bLog->image_content.'/'.$bLog->contents_id.'-160x80.'.$bLog->extension))
                                    <img src="{{asset($bLog->image_content).'/'.$bLog->contents_id.'-160x80.'.$bLog->extension}}"
                                         alt="{!!$bLog->tags!!}" title=""/>
                                @else
                                    <img src="images/no-image.jpg" alt=""/>
                                @endif
                            </a>
                        </h4>
                    </div>
                    <!-- **entry-thumb - Ends** -->
                    <!-- **entry-detail - Starts** -->
                    <div class="entry-detail widget widget_tag_cloud">
                        <div class="entry-title">
                            <h4>
                                <a href="{!! route($locale.'weblog.show',$bLog->url)  !!}">{!! $bLog->link_title !!}</a>
                            </h4>
                        </div>
                        <!-- **entry-meta-data - Starts** -->
                        <div class="entry-body">
                            <p>{!!str_limit($bLog->body_content, $limit = 50, $end = '...')!!}</p>
                        </div>
                        <div class="entry-meta-data">
                            <p><span class="fa fa-sitemap"> </span> @lang("backend/Product.view.create.Category"): <a
                                        href="#"> {{$bLog->name_category}} </a></p>

                            <p><span class="fa fa-user"> </span> @lang("News.view.index.PostedBy"): <a
                                        href="#"> {{$bLog->user_name}} </a></p>

                            <p class="tagcloud"><span class="fa fa-tag"> </span>
                                @if($bLog->tags && $tagsArr=explode(",",$bLog->tags))
                                    @foreach(($tagsArr) as $tag)
                                        @if($tag)
                                            <a href="{{URL::route('weblog.searchTags',['tag'=>$tag])}}"> {!!$tag!!} </a>
                                        @endif
                                    @endforeach
                                @endif
                            </p>
                        </div>
                        <!-- **entry-meta-data - Ends** -->

                    </div>
                    <!-- **entry-detail - Ends** -->
                </article>
            @endforeach

            @if(count($webLog))
                <article class="blog-post type3">
                    <div class="dataTables_info" id="infoData" role="status" aria-live="polite"
                         style="vertical-align: middle">
                        {!!$webLog->appends(Input::all())->render()!!}
                        @lang("News.view.index.Entities") {!!$webLog->total()!!}
                    </div>
                </article>
                @endif

                        <!-- **Blog-post - Ends** -->

        </section>
        <!-- **Primary - Ends** -->

    </div>
</div>

@endsection


@section('scr1')
    <script src="{{ asset('/assets/frontend/js/jquery.jcarousel.min.js') }}" type="text/javascript"></script>
@endsection

@section('scr2')
    <script src="{{ asset('/assets/frontend/js/jquery.smartresize.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/assets/frontend/js/jquery.scrollTo.js') }}" type="text/javascript"></script>
@endsection
@section('scr-retina')
    <script src="{{ asset('/assets/frontend/js/retina.js') }}" type="text/javascript"></script>
@endsection


@section('scripts')
@endsection

@section('inline-scripts')
@endsection
