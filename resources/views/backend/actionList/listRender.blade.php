@if($data)
    <?php $counter = $data->firstItem() - 1; ?>
    @foreach($data as $item)
        <?php $counter++; ?>
        <tr data-id="{!!$item->id!!}" class="row-item">
            <td>
                <a href="#" class="row-details">
                    <?php echo $counter; ?>
                </a>
            </td>
            <td>
                <span class="prefix">{!!$item->prefix!!}</span>
            </td>
            <td>
                <span class="controller">{!!$item->controller!!}</span>
            </td>
            <td style="text-align: center">
                {!!$item->function!!}
            </td>
            <td style="text-align: center">
                {!!$item->method!!}
            </td>

            <td class="row-action">
                <a href="{!!URL::route($locale.'backend.actionList.edit',['id'=>$item->id])!!}"
                   class="btn default btn-xs purple list-attachment" target="_blank">
                    <i class="fa fa-edit"></i> @lang("backend/ActionList.view.listAction.Edit")
                </a>

            <span class="loading"
                  style="background: url('{{ asset('/assets/img/loader.gif') }}') no-repeat;padding: 5px 20px 20px; display: none "></span>
                <span class="result"></span>
            </td>

        </tr>

    @endforeach

    <tr class="paginate">
        <td colspan="8">
            <div class="row">
                <div class="col-md-5 col-sm-12">
                    <div class="dataTables_info" id="infoData" role="status" aria-live="polite"
                         style="vertical-align: middle">
                        {!!$data->appends(Input::all())->render()!!}
                        <span class="loading"
                              style="background: url('{{ asset('/assets/img/loader.gif') }}') no-repeat;padding: 5px 20px 20px; display: none "></span>

                        <div>
                            Showing
                            {!!$data->firstItem()!!} to {!!$data->lastItem()!!}
                            of {!!$data->total()!!} entries
                        </div>
                    </div>
                </div>
            </div>
        </td>
    </tr>

    <script>
        //    console.log(arrayId);
        jQuery(document).ready(function () {
            fnSelect2();
        });
    </script>

@endif



