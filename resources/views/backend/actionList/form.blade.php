@extends('backend.layouts.default')
@section('styles-plugin')
    <link href="{{ asset('/assets/theme/assets/global/plugins/bootstrap-datepicker/css/datepicker.css') }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/theme/assets/global/plugins/select2/select2.css') }}"
          rel="stylesheet" type="text/css"/>

@endsection
@section('styles')

@endsection
@section('content')

    <a href="{!! URL::route($locale.'backend.actionList.index') !!}" class="btn btn-lg red">
        <i class="fa fa-backward"></i>
        @lang("backend/ActionList.view.form.Back")
    </a>
    @if(isset($error))

        @foreach($error as $item)
            <div class="errors">{!!$item!!}</div>
        @endforeach

    @else

        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXTRAS PORTLET-->
                <div class="portlet box blue-hoki">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cogs"></i>{!! $title !!}
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">
                            </a>
                            <a href="#portlet-config" data-toggle="modal" class="config">
                            </a>
                            <a href="javascript:;" class="reload">
                            </a>
                            <a href="javascript:;" class="remove">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body form">

                        @if(isset($data))
                            {!!Form::open(['route' => [$locale.'backend.actionList.update',$data['id']],'method' => 'PUT','class'=>'form-horizontal form-bordered'])!!}
                        @else
                            {!!Form::open(['route' => $locale.'backend.actionList.store','class'=>'form-horizontal form-bordered'])!!}
                        @endif

                        <div class="form-body">

                            <div class="form-group">
                                <label for="short_description" class="control-label col-md-2">
                                    @lang("backend/ActionList.view.listAction.Prefix")
                                </label>

                                <div class="col-md-2">
                                    <div class="input-group">
                                        {!!Form::text('prefix',isset($data['prefix'])?$data['prefix']:null
                                        ,array('class'=>'form-control input-large','placeholder'=>trans("backend/ActionList.view.listAction.Prefix")))!!}
                                        {!!$errors->first('prefix','<span class="error">:message</span>')!!}
                                    </div>
                                </div>

                            </div>
                            <div class="form-group">
                                <label for="short_description" class="control-label col-md-2">
                                    @lang("backend/ActionList.view.listAction.Controller")
                                </label>

                                <div class="col-md-2">
                                    <div class="input-group">
                                        {!!Form::text('controller',isset($data['controller'])?$data['controller']:null
                                        ,array('class'=>'form-control input-large','placeholder'=>trans("backend/ActionList.view.listAction.Controller")))!!}
                                        {!!$errors->first('controller','<span class="error">:message</span>')!!}
                                    </div>
                                </div>

                            </div>
                            <div class="form-group">
                                <label for="short_description" class="control-label col-md-2">
                                    @lang("backend/ActionList.view.listAction.Function")
                                </label>

                                <div class="col-md-2">
                                    <div class="input-group">
                                        {!!Form::text('function',isset($data['function'])?$data['function']:null
                                        ,array('class'=>'form-control input-large','placeholder'=>trans("backend/ActionList.view.listAction.Function")))!!}
                                        {!!$errors->first('function','<span class="error">:message</span>')!!}
                                    </div>
                                </div>

                            </div>

                            <div class="form-group">
                                <label for="alias" class="control-label col-md-2">
                                    @lang("backend/ActionList.view.listAction.Method")</label>

                                <div class="col-md-2">
                                    {!!Form::select('method', [\App\ActionsList::METHOD_GET=>\App\ActionsList::METHOD_GET
                                    ,\App\ActionsList::METHOD_POST=>\App\ActionsList::METHOD_POST
                                    ,\App\ActionsList::METHOD_PUT=>\App\ActionsList::METHOD_PUT
                                    ,\App\ActionsList::METHOD_DELETE=>\App\ActionsList::METHOD_DELETE
                                    ] ,isset($data['method'])?$data['method']:null
                                        ,['id'=>'select2-type','class'=>'form-control select2 input-small'])!!}
                                </div>
                            </div>


                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-6">
                                    <button type="submit" class="btn purple"><i
                                                class="fa fa-check"></i>{!! $btnTitle !!}</button>
                                    <button type="button"
                                            class="btn default">@lang("backend/ActionList.view.form.Cancel")</button>
                                </div>
                            </div>
                        </div>
                        {!!Form::close()!!}

                    </div>
                </div>
            </div>
        </div>

    @endif


@endsection
@section('scripts-plugin')
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootbox/bootbox.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/select2/select2.min.js') }}"
            type="text/javascript"></script>
@endsection
@section('scripts')
    {{--    <script src="{{ asset('/assets/theme/assets/admin/pages/scripts/components-editors.js') }}" type="text/javascript"></script>--}}

    <script src="{{ asset('/assets/js/backend/billing.js') }}" type="text/javascript"></script>

@endsection
@section('scripts-inline')
    <script>
        //    console.log(arrayId);
        jQuery(document).ready(function () {
            fnSelect2();

            var initPickers = function () {
                //init date pickers
                $('.date-picker').datepicker({
                    rtl: Metronic.isRTL(),
                    autoclose: true
                });
            }

            initPickers();

        });
    </script>
    <script>
        jQuery(document).ready(function () {
            // initiate layout and plugins

        });
    </script>
@endsection

