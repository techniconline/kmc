<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-bell-o"></i>@lang("backend/Features.view.index.List")
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>
                    <a href="#portlet-config" data-toggle="modal" class="config">
                    </a>
                    <a href="javascript:;" class="reload">
                    </a>
                    <a href="javascript:;" class="remove">
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-scrollable">
                    <table class="table table-striped table-bordered table-advance table-hover">
                        <thead>
                        <tr>
                            <th>
                                @lang("backend/Features.view.createValue.Title")
                            </th>
                            <th class="hidden-xs">
                                @lang("backend/Features.view.createValue.Value")
                            </th>
                            <th>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($DataValue as $item)
                            <tr class="row-item">
                                <td class="highlight">
                                    <div class="success">
                                    </div>
                                    <a href="{!! URL::route('backend.feature.editFeatureValue',['feature_id'=>$item->feature_id,'feature_value_id'=>$item->id]) !!}">
                                        {!! (($item->title)?$item->title:'No Title' ) !!}
                                    </a>
                                </td>
                                <td class="hidden-xs"
                                    style="{{$item->type=='color'? 'color:'.$item->value.'; direction: ltr' :'' }}; text-align: center">
                                    {!! (($item->value)?$item->value:'') !!}
                                </td>
                                <td>
                                    <a href="{!! URL::route('backend.feature.editFeatureValue',['feature_id'=>$item->feature_id,'feature_value_id'=>$item->id]) !!}"
                                       class="btn default btn-xs purple">
                                        <i class="fa fa-edit"></i> @lang("backend/Features.view.index.Edit") </a>

                                    {{--<a href="{!! URL::route('backend.feature.destroyFeatureValue',['feature_id'=>$item->feature_id,'feature_value_id'=>$item->id]) !!}" class="btn default btn-xs red delete">--}}
                                    {{--<i class="fa fa-times"></i> @lang("backend/Features.view.index.Delete") </a>--}}

                                </td>
                            </tr>
                        @endforeach


                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- END SAMPLE TABLE PORTLET-->
    </div>
</div>

