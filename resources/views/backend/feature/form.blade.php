@extends('backend.layouts.default')
<?php  $langDetails = app('activeLangDetails');?>

@section('styles-plugin')
    @if($langDetails['is_rtl'])

    @else

    @endif
@endsection
@section('styles')

@endsection
@section('content')
    <a href="{!! URL::route($locale.'backend.feature.index') !!}" class="btn btn-lg red">
        <i class="fa fa-backward"></i>
        @lang("backend/Features.view.create.Back")
    </a>


    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXTRAS PORTLET-->
            <div class="portlet box blue-hoki">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>{!! $title !!}
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse">
                        </a>
                        <a href="#portlet-config" data-toggle="modal" class="config">
                        </a>
                        <a href="javascript:;" class="reload">
                        </a>
                        <a href="javascript:;" class="remove">
                        </a>
                    </div>
                </div>
                <div class="portlet-body form">
                    @if(isset($data))
                        {!!Form::open(['route' => [$locale.'backend.feature.update',$data['id']],'method' => 'PUT','class'=>'form-horizontal form-bordered'])!!}

                    @else
                        {!!Form::open(['route' => $locale.'backend.feature.store','class'=>'form-horizontal form-bordered'])!!}
                    @endif
                    {{--<form class="form-horizontal form-bordered">--}}
                    <div class="form-body">
                        <div class="form-group">
                            {!!Form::label('name',trans("backend/Features.view.create.Name"),['class'=>'control-label col-md-2'])!!}
                            <div class="col-md-10">
                                {!!Form::text('name',isset($data['name'])?$data['name']:null,array('class'=>'form-control'))!!}
                                {!!$errors->first('name','<span class="error">:message</span>')!!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!!Form::label('type',trans("backend/Features.view.create.Type"),['class'=>'control-label col-md-2'])!!}
                            <div class="col-md-3">
                                {!!Form::select('type', [ "text"=>trans("backend/Features.view.create.text")
                                                        ,"integer"=>trans("backend/Features.view.create.integer")
                                                        ,"color"=>trans("backend/Features.view.create.color") ],isset($data['type'])?$data['type']:null,['class'=>'btn form-control input-small select2me'])!!}
                                {!!$errors->first('type','<span class="error">:message</span>')!!}
                            </div>

                            {!!Form::label('type',trans("backend/Features.view.create.Custom"),['class'=>'control-label col-md-2'])!!}
                            <div class="col-md-3">
                                {!!Form::checkbox('custom',1,isset($data['custom'])?$data['custom']:null,array('class'=>'form-control'))!!}
                                {!!$errors->first('custom','<span class="error">:message</span>')!!}
                            </div>
                        </div>

                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-6">
                                {!!Form::select('lang', $listLang,$setLang,['class'=>'btn form-control input-small select2me'])!!}
                                <button type="submit" class="btn purple"><i class="fa fa-check"></i>{!! $btnTitle !!}
                                </button>
                                <button type="button"
                                        class="btn default">@lang("backend/Features.view.create.Cancel")</button>
                            </div>
                        </div>
                    </div>
                    {!!Form::close()!!}

                </div>
            </div>
        </div>
    </div>


@endsection
@section('scripts-plugin')


@endsection
@section('scripts')

@endsection
@section('scripts-inline')

    <script>
        jQuery(document).ready(function () {

        });
    </script>
@endsection