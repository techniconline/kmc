@extends('backend.layouts.default')
<?php  $langDetails = app('activeLangDetails');?>

@section('styles-plugin')
    @if($data['type']=='color')
        <link href="{{ asset('/assets/theme/assets/global/plugins/bootstrap-colorpicker/css/colorpicker.css') }}"
              rel="stylesheet" type="text/css"/>
    @endif

    @if($langDetails['is_rtl'])

    @else

    @endif
@endsection
@section('styles')

@endsection
@section('content')
    <a href="{!! URL::route($locale.'backend.feature.index') !!}" class="btn btn-lg red">
        <i class="fa fa-backward"></i>
        @lang("backend/Features.view.create.Back")
    </a>


    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXTRAS PORTLET-->
            <div class="portlet box blue-hoki">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>{!! $title !!}
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse">
                        </a>
                        <a href="#portlet-config" data-toggle="modal" class="config">
                        </a>
                        <a href="javascript:;" class="reload">
                        </a>
                        <a href="javascript:;" class="remove">
                        </a>
                    </div>
                </div>
                <div class="portlet-body form">
                    @if(isset($data)&&isset($dataFeatureValue))
                        {!!Form::open(['route' => ['backend.feature.updateFeatureValue',$dataFeatureValue['feature_id'],$dataFeatureValue['id']]
                                ,'method' => 'PUT','class'=>'form-horizontal form-bordered'])!!}
                    @else
                        {!!Form::open(['route' => ['backend.feature.storeFeatureValue',$data['id']],'class'=>'form-horizontal form-bordered'])!!}
                    @endif
                    @if(!$data['custom'])

                        <div class="form-body">
                            <div class="form-group">
                                {!!Form::label('name',trans("backend/Features.view.create.Name"),['class'=>'control-label col-md-2'])!!}
                                <div class="col-md-3">
                                    {!!Form::text('name',isset($data['name'])?$data['name']:null,array('class'=>'form-control input-medium', 'disabled'=>''))!!}
                                    {!!$errors->first('name','<span class="error">:message</span>')!!}
                                </div>
                                {!!Form::label('type',trans("backend/Features.view.create.Type"),['class'=>'control-label col-md-2'])!!}
                                <div class="col-md-3">
                                    {!!Form::text('type',isset($data['type'])?$data['type']:null,array('class'=>'form-control input-medium', 'disabled'=>''))!!}
                                    {!!$errors->first('type','<span class="error">:message</span>')!!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!!Form::label('title',trans("backend/Features.view.createValue.Title"),['class'=>'control-label col-md-2'])!!}
                                <div class="col-md-3">
                                    {!!Form::text('title',isset($dataFeatureValue['title'])?$dataFeatureValue['title']:null,array('class'=>'form-control input-medium'))!!}
                                    {!!$errors->first('title','<span class="error">:message</span>')!!}
                                </div>
                                {!!Form::label('value',trans("backend/Features.view.createValue.Value"),['class'=>'control-label col-md-2'])!!}

                                <div class="col-md-4">
                                    <div class="input-group" style="text-align:left">
                                        {!!Form::text('value',isset($dataFeatureValue['value'])?$dataFeatureValue['value']:null,array('class'=>'form-control '.($data['type']=='color'? 'colorPicker':'').' input-medium'))!!}
                                        @if($data['type']=='color')
                                            <span class="input-group-btn">
                                                <a class="btn " id="btn-color"
                                                   style="background-color: {{isset($dataFeatureValue['value'])?$dataFeatureValue['value']:''}};">
                                                    <i class="fa "></i>
                                                </a>
                                            </span>
                                        @endif
                                        {!!$errors->first('value','<span class="error">:message</span>')!!}

                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-6">
                                    {!!Form::select('lang', $listLang,$setLang,['class'=>'btn form-control input-small select2me'])!!}
                                    <button type="submit" class="btn purple"><i
                                                class="fa fa-check"></i>{!! $btnTitle !!}</button>
                                    <button type="button"
                                            class="btn default">@lang("backend/Features.view.create.Cancel")</button>
                                </div>
                            </div>
                        </div>

                    @endif
                    {!!Form::close()!!}

                </div>
            </div>
        </div>
    </div>

    @if(isset($data) && isset($DataValue))

        @include('backend.feature.indexValue')

    @endif


@endsection
@section('scripts-plugin')
    @if($data['type']=='color')
        <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js') }}"
                type="text/javascript"></script>
    @endif

@endsection
@section('scripts')

@endsection
@section('scripts-inline')

    <script>
        jQuery(document).ready(function () {
            @if($data['type']=='color')
            $('.colorPicker').colorpicker();
            @endif


        });
    </script>
@endsection