@extends('backend.layouts.default')

@section('content')
    @if (Session::has('flash_notification.message'))
        <div class="alert alert-{{ Session::get('flash_notification.level') }}">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

            {{ Session::get('flash_notification.message') }}
        </div>
    @endif
    <a href="{!! URL::route($locale.'backend.messages.index') !!}" class="btn btn-lg red">
        <i class="fa fa-backward"></i>
        @lang("backend/Message.view.form.Back")
    </a>
    @if(isset($error))
        @foreach($error as $item)
            <div class="errors">{!!$item!!}</div>
        @endforeach
    @else
        <div class="row">
            <div class="col-md-12">
                <div class="portlet box blue-hoki">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cogs"></i>@lang("backend/Message.view.form.Answer_To") {!! $message->from !!}
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">
                            </a>
                            <a href="#portlet-config" data-toggle="modal" class="config">
                            </a>
                            <a href="javascript:;" class="reload">
                            </a>
                            <a href="javascript:;" class="remove">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        {!!Form::model($message,['route' => [$locale.'backend.messages.store'],'class'=>'form-horizontal form-bordered'])!!}
                        {!! Form::hidden('id',$message->id) !!}
                        {!! Form::hidden('from',$message->from) !!}
                        {!! Form::hidden('to',$message->to) !!}
                        <div class="form-body">
                            <div class="form-group">
                                {!!Form::label('message',trans("backend/Message.view.index.Message"),['class'=>'control-label col-md-2'])!!}
                                <div class="col-md-10">
                                    {!! $message->message !!}
                                </div>
                                {!!$errors->first('message','<span class="error">:message</span>')!!}
                            </div>
                            <div class="form-group">
                                {!!Form::label('answer',trans("backend/Message.view.index.Answer"),['class'=>'control-label col-md-2'])!!}
                                <div class="col-md-10">
                                    {!!Form::textarea('answer',null,['class'=>'form-control', 'rows'=> 20])!!}
                                    {!!$errors->first('answer','<span class="error">:message</span>')!!}
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-6">
                                    <button type="submit"
                                            class="btn green">@lang("backend/Message.view.form.Send")</button>
                                    <a href="{!! route($locale.'backend.messages.index') !!}"
                                       class="btn default">@lang("backend/Message.view.form.Cancel")</a>
                                </div>
                            </div>
                        </div>
                        {!!Form::close()!!}
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection

@section('scripts-plugin')
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-markdown/lib/markdown.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-summernote/summernote.min.js') }}"
            type="text/javascript"></script>
@endsection

@section('scripts')
    <script src="{{ asset('/assets/theme/assets/admin/pages/scripts/components-editors.js') }}"
            type="text/javascript"></script>
@endsection

@section('scripts-inline')
    <script>
        jQuery(document).ready(function () {
            ComponentsEditors.init();
        });
    </script>
@endsection