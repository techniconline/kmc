@extends('backend.layouts.default')
@section('styles-plugin')

@endsection
@section('styles')

@endsection
@section('content')
    @if (Session::has('flash_notification.message'))
        <div class="alert alert-{{ Session::get('flash_notification.level') }}">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

            {{ Session::get('flash_notification.message') }}
        </div>
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-bell-o"></i>@lang("backend/Message.view.index.List")
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse">
                        </a>
                        <a href="#portlet-config" data-toggle="modal" class="config">
                        </a>
                        <a href="javascript:;" class="reload">
                        </a>
                        <a href="javascript:;" class="remove">
                        </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-scrollable">
                        <table class="table table-striped table-bordered table-advance table-hover">
                            <thead>
                            <tr>
                                <th>
                                    <i class="fa fa-info"></i>@lang("backend/Message.view.index.From")
                                </th>
                                <th>
                                    <i class="fa fa-info"></i>@lang("backend/Message.view.index.Name")
                                </th>
                                <th>
                                    <i class="fa fa-header"></i>@lang("backend/Message.view.index.To")
                                </th>
                                <th class="hidden-xs">
                                    <i class="fa fa-file-text"></i>@lang("backend/Message.view.index.Message")
                                </th>
                                <th>
                                    <i class="fa fa-globe"></i>@lang("backend/Message.view.index.Date")
                                </th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($messages as $message)
                                <tr class="row-item">
                                    <td>
                                        {!! $message->from !!}
                                    </td>
                                    <td>
                                        {!! $message->name !!}
                                    </td>
                                    <td>
                                        {!! $message->to !!}
                                    </td>
                                    <td>
                                        {!! $message->message !!}
                                    </td>
                                    <td>
                                        {!! $message->created_at !!}
                                    </td>
                                    <td>
                                        @if($message->status == 0)
                                            <a href="{!! route($locale.'backend.messages.edit',$message->id) !!}"
                                               class="btn btn-warning btn-xs answer">
                                                <i class="fa fa-mail-reply"></i> @lang("backend/Message.view.index.Answer")
                                            </a>
                                        @else
                                            <a href="#answer" class="btn btn-success btn-xs answered"
                                               data-toggle="modal" data-id="{!! $message->id !!}"
                                               data-href="{!! route($locale.'backend.messages.edit',$message->id) !!}">
                                                <i class="fa fa-search"></i> @lang("backend/Message.view.index.Answered")
                                            </a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    {!! $messages->render() !!}
                </div>
            </div>

        </div>
    </div>
    <div class="modal fade bs-modal-lg tab-pane active fontawesome-demo" id="answer" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">to: <span></span></h4>
                    <h4 class="modal-date">2015-07-10 11:00:52</h4>
                </div>
                <div class="modal-body">
                    <h4 class="modal-answer"></h4>
                </div>
                <div class="modal-footer">
                    <h4 class="modal-btn">
                        <a href="" class="btn btn-info" data-dismiss="modal">
                            OK
                        </a>
                    </h4>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts-plugin')

    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-markdown/lib/markdown.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-summernote/summernote.min.js') }}"
            type="text/javascript"></script>


@endsection
@section('scripts')
    <script src="{{ asset('/assets/theme/assets/admin/pages/scripts/components-editors.js') }}"
            type="text/javascript"></script>
@endsection
@section('scripts-inline')

    <script>
        jQuery(document).ready(function () {
            ComponentsEditors.init();
        });

        $('.answered').on('click', function () {
            var $modal = $('.modal');
            $.ajax({
                url: $(this).data('href'),
                method: 'GET',
                success: function (data) {
                    $modal.find('.modal-title span').html(data.to);
                    $modal.find('.modal-date').html(data.created_at);
                    $modal.find('.modal-answer').html(data.message);
                }
            });
        });

    </script>
@endsection