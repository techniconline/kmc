@extends('backend.layouts.default')
@section('styles-plugin')

@endsection
@section('styles')

@endsection
@section('content')
    <a class="btn btn-lg blue" href="{!! URL::route($locale.'backend.roomCategory.create') !!}">
        <i class="fa fa-file-o"></i>
        @lang("backend/roomCategory.view.index.Create_New_Room_Category")
    </a>

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN SAMPLE TABLE PORTLET-->
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-bell-o"></i>@lang("backend/roomCategory.view.index.List")
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse">
                        </a>
                        <a href="#portlet-config" data-toggle="modal" class="config">
                        </a>
                        <a href="javascript:;" class="reload">
                        </a>
                        <a href="javascript:;" class="remove">
                        </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-scrollable">
                        <table class="table table-striped table-bordered table-advance table-hover">
                            <thead>
                            <tr>
                                <th>
                                    <i class="fa fa-building"></i>@lang("backend/roomCategory.view.index.Title")
                                </th>
                                <th class="hidden-xs">
                                    <i class="fa fa-keyboard-o"></i>@lang("backend/roomCategory.view.index.Short_Description")
                                </th>
                                <th>
                                    <i class="fa fa-cube"></i>@lang("backend/roomCategory.view.index.Area") (m2)
                                </th>
                                <th>
                                    <i class="fa fa-eur"></i>@lang("backend/roomCategory.view.index.Rent_Price")
                                </th>
                                <th>
                                    <i class="fa fa-location-arrow"></i> @lang("backend/roomCategory.view.index.Type")
                                </th>

                                <th>
                                </th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($data as $item)
                                <tr class="row-category" data-id="{!!$item->id!!}">
                                    <td class="highlight">
                                        <div class="success">
                                        </div>
                                        <a href="{!! URL::route($locale.'backend.roomCategory.edit',['room_category_id'=>$item->id]) !!}">
                                            {!! (($item->title)?$item->title:'No Title' ) !!}
                                        </a>
                                    </td>
                                    <td class="hidden-xs">
                                        {!! (($item->short_description)?$item->short_description:'0') !!}
                                    </td>
                                    <td>
                                        {!! (($item->area)?$item->area:'0') !!} ~
                                        {!! (($item->to_area)?$item->to_area:'0') !!}
                                    </td>
                                    <td>
                                        {!! (($item->rent_price)?$item->rent_price:'0') !!} ~
                                        {!! (($item->to_rent_price)?$item->to_rent_price:'0') !!}
                                    </td>
                                    <td>
                                        {!! (($item->type)?$item->type:'0') !!}
                                    </td>

                                    <td data-id="{!!$item->id!!}">
                                        <a href="{!! URL::route($locale.'backend.roomCategory.edit',['room_category_id'=>$item->id]) !!}"
                                           class="btn default btn-xs purple">
                                            <i class="fa fa-edit"></i> @lang("backend/roomCategory.view.index.Edit")
                                        </a>
                                        <a href="{!! URL::route('backend.roomCategory.details',['category'=>$item->id]) !!}"
                                           class="btn default btn-xs green">
                                            <i class="fa fa-cubes"></i> @lang("backend/roomCategory.view.index.Details")
                                        </a>
                                        <a href="{!! URL::route($locale.'backend.roomCategory.destroy',['category'=>$item->id]) !!}"
                                           class="btn default btn-xs red delete">
                                            <i class="fa fa-times"></i> @lang("backend/roomCategory.view.index.Delete")
                                        </a>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END SAMPLE TABLE PORTLET-->
        </div>
    </div>

@endsection
@section('scripts-plugin')
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootbox/bootbox.min.js') }}"
            type="text/javascript"></script>
@endsection
@section('scripts')
    <script src="{{ asset('/assets/js/backend/category.js') }}" type="text/javascript"></script>

@endsection
@section('scripts-inline')
    <script>
        jQuery(document).ready(function () {

        });
    </script>
@endsection