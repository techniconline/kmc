@extends('backend.layouts.default')
@section('styles-plugin')
    {{--<link href="{{ asset('/assets/theme/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css"/>--}}
    {{--<link href="{{ asset('/assets/theme/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css"/>--}}
    {{--<link href="{{ asset('/assets/theme/assets/global/plugins/jquery-tags-input/jquery.tagsinput.css') }}" rel="stylesheet" type="text/css"/>--}}
    {{--<link href="{{ asset('/assets/theme/assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css') }}" rel="stylesheet" type="text/css"/>--}}
    {{--<link href="{{ asset('/assets/theme/assets/global/plugins/typeahead/typeahead.css') }}" rel="stylesheet" type="text/css"/>--}}
@endsection
@section('styles')

@endsection
@section('content')
    <a href="{!! URL::route($locale.'backend.roomCategory.index') !!}" class="btn btn-lg red">
        <i class="fa fa-backward"></i>
        Back
    </a>
    @if(($error))

        @foreach($error as $item)
            <div class="errors">{!!$item!!}</div>
        @endforeach

    @else


        @if(($dataDetails))
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN SAMPLE TABLE PORTLET-->
                    <div class="portlet">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-bell-o"></i>@lang("backend/roomCategory.view.details.List_Room_Category")
                                <b>{!!$dataCategory['title']!!}</b>
                            </div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse">
                                </a>
                                <a href="#portlet-config" data-toggle="modal" class="config">
                                </a>
                                <a href="javascript:;" class="reload">
                                </a>
                                <a href="javascript:;" class="remove">
                                </a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="table-scrollable">
                                <table class="table table-striped table-bordered table-advance table-hover">
                                    <thead>
                                    <tr>
                                        <th>
                                            <i class="fa fa-building"></i>@lang("backend/roomCategory.view.details.Apartment_Name")
                                        </th>
                                        <th class="hidden-xs">
                                            <i class="fa fa-bank"></i> @lang("backend/roomCategory.view.details.Room_Count")
                                        </th>
                                        <th>
                                            <i class="fa fa-bank"></i> @lang("backend/roomCategory.view.details.Floor_Number")
                                        </th>

                                        <th>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($dataDetails as $data)
                                        {!!Form::open(['route' => ['backend.roomCategory.updateDetails',$dataCategory['id'],$data->id],'method' => 'PUT'])!!}
                                        <tr class="row-details">
                                            <td>
                                                {!!Form::select  ('apartment_id', $dataApartment,isset($data->apartment_id)?$data->apartment_id:'',['class'=>'form-control input-large select2me'])!!}
                                            </td>
                                            <td>
                                                {!!Form::text('room_number',isset($data->room_number)?$data->room_number:'',array('class'=>'form-control input-small room-number-cat'))!!}
                                            </td>
                                            <td>
                                                {!!Form::text('floor_number',isset($data->floor_number)?$data->floor_number:'',array('class'=>'form-control input-small floor-number-cat'))!!}
                                            </td>
                                            <td class="item-details" data-id="{!!$data->id!!}">
                                                <button type="submit" class="btn purple"><i
                                                            class="fa fa-check"></i>@lang("backend/roomCategory.view.details.Update_Category_Details")
                                                </button>
                                                <button type="button"
                                                        class="btn default btn-delete">@lang("backend/roomCategory.view.details.Delete")</button>

                                            </td>
                                        </tr>
                                        {!!Form::close()!!}
                                    @endforeach

                                    <tr class="price-item" data-id="{!!$dataCategory['id']!!}">
                                        <td>
                                            {!!Form::label('price','Price',['class'=>'control-label col-md-2'])!!}
                                            {!!Form::text('price',isset($data->rent_price)?$data->rent_price:0,array('class'=>'form-control input-medium price-cat','placeholder'=>'Price'))!!}
                                            {!!$errors->first('price','<span class="error">:message</span>')!!}
                                        </td>
                                        <td colspan="2">
                                            {!!Form::label('to_price','To Price',['class'=>'control-label col-md-3'])!!}
                                            {!!Form::text('to_price',isset($data->to_rent_price)?$data->to_rent_price:0,array('class'=>'form-control input-medium to-price-cat','placeholder'=>'To Price'))!!}
                                            {!!$errors->first('to_price','<span class="error">:message</span>')!!}
                                        </td>

                                        <td class="item-details" data-id="{!!$data->id!!}">
                                            <button type="submit" class="btn purple btn-update-price"><i
                                                        class="fa fa-check"></i>@lang("backend/roomCategory.view.details.Update_Price")
                                            </button>
                                        </td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- END SAMPLE TABLE PORTLET-->
                </div>
            </div>
        @endif


        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXTRAS PORTLET-->
                <div class="portlet box blue-hoki">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cogs"></i>@lang("backend/roomCategory.view.details.Create_New_Category_Details_For")
                            <b>{!!$dataCategory['title']!!}</b>
                            - @lang("backend/roomCategory.view.details.with_Area"): <b>{!!$dataCategory['area']!!}
                                ~ {!!$dataCategory['area_to']!!}</b>
                            - @lang("backend/roomCategory.view.create.Type_Category"):
                            <b>{!!$dataCategory['type']!!}</b>
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">
                            </a>
                            <a href="#portlet-config" data-toggle="modal" class="config">
                            </a>
                            <a href="javascript:;" class="reload">
                            </a>
                            <a href="javascript:;" class="remove">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body form">

                        {!!Form::open(['route' => ['backend.roomCategory.addDetails',$dataCategory['id']],'method' => 'POST','class'=>'form-horizontal form-bordered'])!!}

                        <div class="form-body">

                            <div class="form-group">
                                {!!Form::label('floor_number',trans("backend/roomCategory.view.details.Floor_Number"),['class'=>'control-label col-md-2'])!!}
                                <div class="col-md-10">
                                    {!!Form::text('floor_number',null,array('class'=>'form-control'))!!}
                                    {!!$errors->first('floor_number','<span class="error">:message</span>')!!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!!Form::label('room_number',trans("backend/roomCategory.view.details.Room_Count"),['class'=>'control-label col-md-2'])!!}
                                <div class="col-md-10">
                                    {!!Form::text('room_number',null,array('class'=>'form-control'))!!}
                                    {!!$errors->first('room_number','<span class="error">:message</span>')!!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!!Form::label('apartment_id',trans("backend/roomCategory.view.details.Apartment"),['class'=>'control-label col-md-2'])!!}
                                <div class="col-md-10">
                                    {!!Form::select  ('apartment_id', $dataApartment,'',['class'=>'form-control input-large select2me'])!!}
                                    {!!$errors->first('apartment_id','<span class="error">:message</span>')!!}
                                </div>
                            </div>

                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-6">
                                    <button type="submit" class="btn purple"><i
                                                class="fa fa-check"></i>@lang("backend/roomCategory.view.details.Add_Category_Details")
                                    </button>
                                    <button type="button"
                                            class="btn default">@lang("backend/roomCategory.view.details.Cancel")</button>
                                </div>
                            </div>
                        </div>
                        {!!Form::close()!!}

                    </div>
                </div>
            </div>
        </div>

    @endif
@stop
@section('scripts-plugin')

@endsection
@section('scripts')
    <script src="{{ asset('/assets/js/backend/category.js') }}" type="text/javascript"></script>

@endsection
@section('scripts-inline')
    <script>
        jQuery(document).ready(function () {

//            ComponentsFormTools.init();
        });
    </script>
@endsection