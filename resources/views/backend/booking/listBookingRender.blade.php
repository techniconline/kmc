@foreach($dataBooking as $item)
    <tr data-id="{!!$item->id!!}" class="row-item">
        <td>
            <a href="#" class="row-details">
                <i class="fa fa-plus-square-o"></i>
            </a>
        </td>
        <td>
            <span class="username">{!!$item->firstname.' '.$item->lastname!!}</span>
            <span class="email" data-user="{!!$item->user_id!!}">
                {!!$item->email!!}
            </span>

            <div class="mobile" data-user="{!!$item->user_id!!}">
                <b>@lang("backend/Booking.view.listBooking.Mobile"):</b> {!!$item->mobile!!}
            </div>
            <div class="register_date_user" data-user="{!!$item->user_id!!}">
                <b>@lang("backend/User.view.index.RegDate"):</b>

                <p>{!!$item->register_date_user!!}</p>
            </div>
        </td>
        <td>
            {!!$item->birth_date!!}
        </td>
        <td>
            {!!trans("backend/Booking.view.listBooking.From").': '.$item->from_date.' '.trans("backend/Booking.view.listBooking.To").' : '.$item->to_date!!}
        </td>
        <td>
            {!!$item->gender!!}
        </td>
        <td>
            {!!$item->country_name!!}
        </td>
        <td>
            {!!$item->created_at!!}
        </td>
        <td class="row-action">
            <a href="{!!URL::route('backend.attachment.listUploadedSingleUser',['user_id'=>$item->user_id,'booking_id'=>$item->id])!!}"
               class="btn default btn-xs purple list-attachment" target="_blank">
                <i class="fa fa-link"></i> @lang("backend/Booking.view.listBooking.ListAttachment")
            </a>
            @if($item->send_contract)
                <p style="color: #006400; margin-top: 5px"><i
                            class="fa fa-check font-green"></i>@lang("backend/Booking.view.listBooking.sendContractOk")
                </p>
            @else
                <p style="color: red; margin-top: 5px"><i
                            class="fa fa-warning font-yellow"></i>@lang("backend/Booking.view.listBooking.sendContractNotOk")
                </p>
            @endif
            {{--<a href="{!!URL::route('backend.booking.rejectBookingUser', ['user_id'=>$item->user_id,'booking_id'=>$item->id])!!}"--}}
            {{--class="btn default btn-xs red reject">--}}
            {{--<i class="fa fa-times"></i> @lang("backend/Booking.view.listBooking.Reject")--}}
            {{--</a>--}}
            <a href="{!!URL::route('backend.booking.sendContract', ['booking_id'=>$item->id])!!}"
               class="btn default btn-xs green send-contract">
                <i class="fa fa-check"></i> @lang("backend/Booking.view.listBooking.sendContract")
            </a>
            <a style="display: none" class="item show-modal-contract" data-toggle="modal"
               href="#modal-contract-{!!$item->id!!}">modal</a>
            <!-- /.modal -->
            <div class="modal fade bs-modal-lg" id="modal-contract-{!!$item->id!!}" tabindex="-1" role="dialog"
                 aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title">{!!$item->firstname.' '.$item->lastname!!} - {!!$item->email!!}</h4>
                        </div>
                        <div class="modal-body">
                            <input type="hidden" name="booking_id" value="{!!$item->id!!}"/>

                            <div class="price-room">
                                @lang("backend/Booking.view.listBooking.ThePriceOfLetters")
                                (@lang("backend/Booking.view.listBooking.Room")):
                                <span class="price"><b>...</b> <i class="fa fa-euro"></i></span>
                                <input placeholder="@lang("backend/Booking.view.listBooking.ThePriceOfLetters")"
                                       type="text" class="form-control input-sm price_letters_room"
                                       name="price_letters_room">
                            </div>
                            <div class="price-room-9">
                                @lang("backend/Booking.view.listBooking.ThePriceOfLetters")
                                (@lang("backend/Booking.view.listBooking.Room")):
                                <span class="price"><b>...</b> (X * 9) <i class="fa fa-euro"></i></span>
                                <input placeholder="@lang("backend/Booking.view.listBooking.ThePriceOfLetters")"
                                       type="text" class="form-control input-sm price_letters_room_9"
                                       name="price_letters_room_9">
                            </div>
                            <div class="price-parking">
                                @lang("backend/Booking.view.listBooking.ThePriceOfLetters")
                                (@lang("backend/Booking.view.listBooking.Parking")):
                                <span class="price"><b>...</b> <i class="fa fa-euro"></i></span>
                                <input placeholder="@lang("backend/Booking.view.listBooking.ThePriceOfLetters")"
                                       type="text" class="form-control input-sm price_letters_parking"
                                       name="price_letters_parking">
                            </div>
                            <div class="price-parking-9">
                                @lang("backend/Booking.view.listBooking.ThePriceOfLetters")
                                (@lang("backend/Booking.view.listBooking.Parking")):
                                <span class="price"><b>...</b> (X * 9) <i class="fa fa-euro"></i></span>
                                <input placeholder="@lang("backend/Booking.view.listBooking.ThePriceOfLetters")"
                                       type="text" class="form-control input-sm price_letters_parking_9"
                                       name="price_letters_parking_9">
                            </div>

                        </div>
                        <div class="modal-footer">
                            <input type="hidden" class="file_contract1" name="file_contract1" value=""/>
                            <input type="hidden" class="file_contract2" name="file_contract2" value=""/>
                            <a class="btn default"
                               data-dismiss="modal">@lang("backend/Booking.view.listBooking.Cancel")</a>
                            <a style="display: none;"
                               href="{!!URL::route('backend.booking.generateContract', ['booking_id'=>$item->id])!!}"
                               class="btn green pdf-modal-generate-contract">@lang("backend/Booking.view.listBooking.GeneratePDF")</a>

                            <a style="display: none;"
                               href="{!!URL::route('backend.booking.sendMailContract', ['booking_id'=>$item->id])!!}"
                               class="btn green pdf-modal-send-mail-contract">@lang("backend/Booking.view.listBooking.sendContractMail")</a>

                            <span class="loading"
                                  style="background: url('{{ asset('/assets/img/loader.gif') }}') no-repeat;padding: 5px 20px 20px; display: none "></span>
                            <span class="result"></span>

                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->

            <span class="loading"
                  style="background: url('{{ asset('/assets/img/loader.gif') }}') no-repeat;padding: 5px 20px 20px; display: none "></span>
            <span class="result"></span>
        </td>

    </tr>
    <tr data-id="{!!$item->id!!}" class="details" style="display: none">
        <td class="details" colspan="8">
            <table style="width: 100%">
                <tbody>
                @if(isset($item->details))
                    <?php $typeRoom = ''; $typeCat = []; ?>
                    @foreach($item->details as $itemD)
                        @if($typeRoom!=$itemD->type)
                            <?php $typeCat[] = $typeRoom = $itemD->type; ?>
                            <tr data-id-map="{!!$itemD->id!!}"
                                style="background: #AAAAAA; font-weight: bold; padding: 2px; border: 1px solid #222222">
                                <td>@lang("backend/Booking.view.listBooking.Type"):</td>
                                <td>{!!$itemD->type!!} </td>
                                <td></td>
                            </tr>
                        @endif

                        <tr data-idD="{!!$itemD->booking_details_id!!}"
                            style="padding: 2px; border: 1px solid #555555">
                            <td>@lang("backend/Booking.view.listBooking.Description"):</td>
                            <td class="hidden-xs">{!!$itemD->title!!} ({!!$itemD->area!!}~{!!$itemD->to_area!!} m2)
                                ( {!!$itemD->rent_price!!}~{!!$itemD->to_rent_price!!}<i class="fa fa-eur"></i>)
                                / {!!$itemD->short_description!!}</td>
                            <td>
                            </td>
                        </tr>
                    @endforeach

                    @if($item->status==1 || $item->status==3)
                        @if($typeCat && in_array('room',$typeCat) && ($item->rentStatus['room']!=5))
                            <tr style="background: #EEEEEE; font-weight: bold; padding: 2px; border: 1px solid #222222"
                                class="details-action">
                                <td>@lang("backend/Booking.view.listBooking.SelectRoom"):</td>
                                <td style="padding: 5px">

                                    @if(isset($item->roomReserved->rent_status))
                                        {!!Form::select('room_id', $dataListRooms,isset($item->roomReserved->room_id)?$item->roomReserved->room_id:0,['disabled'=>'disabled','id'=>'select2-'.$item->id,'class'=>'form-control select2 item-room-select'])!!}
                                    @else
                                        {!!Form::select('room_id', $dataListRooms,isset($item->roomReserved->room_id)?$item->roomReserved->room_id:0,['id'=>'select2-'.$item->id,'class'=>'form-control select2 item-room-select'])!!}
                                    @endif

                                </td>
                                <td>
                                    @if(!isset($item->roomReserved->rent_status))
                                        <a href="{!!URL::route($locale.'backend.room.index')!!}"
                                           class="btn default btn-xs yellow-stripe reservation">
                                            <i class="fa fa-book"></i>
                                            @lang("backend/Booking.view.listBooking.Reservation")
                                        </a>
                                        <a href="{!!URL::route('backend.booking.rejectBookingDetailsType',['booking_id'=>$item->id,'type'=>'room'])!!}"
                                           class="btn default btn-xs red-stripe reject-booking-details">
                                            <i class="fa fa-times"></i>
                                            @lang("backend/Booking.view.listBooking.Reject")
                                        </a>
                                    @endif

                                    @if(isset($item->roomReserved->rent_status)&&$item->roomReserved->rent_status==2)
                                        <a href="{!!URL::route($locale.'backend.room.index')!!}"
                                           class="btn default btn-xs green-stripe rent">
                                            <i class="fa fa-check"></i>
                                            @lang("backend/Booking.view.listBooking.Rent")
                                        </a>
                                    @endif

                                    @if(isset($item->roomReserved->rent_status))
                                        <a href="{!!URL::route('backend.room.cancelRentRoom'
                                ,['rent_id'=>$item->roomReserved->id,'user_id'=>$item->roomReserved->user_id])!!}"
                                           class="btn default btn-xs red-stripe cancel-rent">
                                            <i class="fa fa-times"></i>
                                            @lang("backend/Booking.view.listBooking.CancelRent")
                                        </a>
                                    @endif

                                    <span class="loading"
                                          style="background: url('{{ asset('/assets/img/loader.gif') }}') no-repeat;padding: 5px 20px 20px; display: none "></span>
                                    <span class="result"></span>
                                </td>
                            </tr>
                            <script>
                                arrayId.push({!!$item->id!!});
                            </script>
                        @endif
                        @if($typeCat && in_array('parking',$typeCat) && ($item->rentStatus['parking']!=5))
                            <tr style="background: #EEEEEE; font-weight: bold; padding: 2px; border: 1px solid #222222;"
                                class="details-action">
                                <td>@lang("backend/Booking.view.listBooking.SelectParking"):</td>
                                <td style="padding: 5px">

                                    @if(isset($item->parkingReserved->rent_status))
                                        {!!Form::select('room_id', $dataListParking,isset($item->parkingReserved->room_id)?$item->parkingReserved->room_id:0,['disabled'=>'disabled','id'=>'select2-'.$item->id.'-park','class'=>'form-control select2 item-room-select'])!!}
                                    @else
                                        {!!Form::select('room_id', $dataListParking,isset($item->parkingReserved->room_id)?$item->parkingReserved->room_id:0,['id'=>'select2-'.$item->id.'-park','class'=>'form-control select2 item-room-select'])!!}
                                    @endif

                                </td>
                                <td>
                                    @if(!isset($item->parkingReserved->rent_status))
                                        <a href="{!!URL::route($locale.'backend.room.index')!!}"
                                           class="btn default btn-xs yellow-stripe reservation">
                                            <i class="fa fa-book"></i>
                                            @lang("backend/Booking.view.listBooking.Reservation")
                                        </a>
                                        <a href="{!!URL::route('backend.booking.rejectBookingDetailsType',['booking_id'=>$item->id,'type'=>'parking'])!!}"
                                           class="btn default btn-xs red-stripe reject-booking-details">
                                            <i class="fa fa-times"></i>
                                            @lang("backend/Booking.view.listBooking.Reject")
                                        </a>
                                    @endif

                                    @if(isset($item->parkingReserved->rent_status)&&$item->parkingReserved->rent_status==2)
                                        <a href="{!!URL::route($locale.'backend.room.index')!!}"
                                           class="btn default btn-xs green-stripe rent">
                                            <i class="fa fa-check"></i>
                                            @lang("backend/Booking.view.listBooking.Rent")
                                        </a>
                                    @endif

                                    @if(isset($item->parkingReserved->rent_status))
                                        <a href="{!!URL::route('backend.room.cancelRentRoom'
                                ,['rent_id'=>$item->parkingReserved->id,'user_id'=>$item->parkingReserved->user_id])!!}"
                                           class="btn default btn-xs red-stripe cancel-rent">
                                            <i class="fa fa-times"></i>
                                            @lang("backend/Booking.view.listBooking.CancelRent")
                                        </a>
                                    @endif

                                    <span class="loading"
                                          style="background: url('{{ asset('/assets/img/loader.gif') }}') no-repeat;padding: 5px 20px 20px; display: none "></span>
                                    <span class="result"></span>
                                </td>
                            </tr>
                            <script>
                                arrayId.push({!!$item->id!!}+'-park');
                            </script>
                        @endif
                    @else
                        <tr style="background: #EEEEEE; font-weight: bold; padding: 2px; border: 1px solid #222222; color: #ff0000;">
                            <td colspan="3"> @lang("backend/Booking.view.listBooking.thisBookingCanceledRent") </td>
                        </tr>
                    @endif
                @endif
                </tbody>
            </table>
        </td>
    </tr>
    <tr data-id="{!!$item->id!!}" class="details"
        style="display: none; background: #999999; font-weight: bold; padding: 2px; border: 1px solid #111111">
        <td colspan="2">@lang("backend/Booking.view.listBooking.ListAttachmentsUser")</td>
        <td colspan="6" class="row-action" style="background-color: #ffffff">
            @if($item->status!=3)
                <a href="{!!URL::route('backend.attachment.sendMailToUser')!!}"
                   class="btn default btn-xs purple send-mail">
                    <i class="fa fa-edit"></i> @lang("backend/AttachType.view.listUploaded.SendMail")
                </a>
                <a href="{!!URL::route('backend.attachment.rejectDocumentsUser', ['user_id'=>$item->user_id,'booking_id'=>$item->id])!!}"
                   class="btn default btn-xs red reject">
                    <i class="fa fa-times"></i> @lang("backend/AttachType.view.listUploaded.Reject")
                </a>
                <a href="{!!URL::route('backend.attachment.isOkDocumentsUser', ['user_id'=>$item->user_id,'booking_id'=>$item->id])!!}"
                   class="btn default btn-xs green is-ok">
                    <i class="fa fa-check"></i> @lang("backend/AttachType.view.listUploaded.IsOk")
                </a>
                <span class="loading"
                      style="background: url('{{ asset('/assets/img/loader.gif') }}') no-repeat;padding: 5px 20px 20px; display: none "></span>
                <span class="result"></span>
            @endif
            <span style="color: #8b0000">{!!$item->text_status_document!!}</span>
        </td>
    </tr>
    <tr data-id="{!!$item->id!!}" class="details" style="display: none">
        <td class="details" colspan="8">
            <table style="width: 100%">
                <tbody>
                <?php $group_title = '';?>
                @if(isset($item->documents))
                    @foreach($item->documents as $itemDoc)

                        <?php
                        if ($itemDoc->title_group != $group_title) {
                        $group_title = $itemDoc->title_group;
                        ?>
                        <tr style="background: #AAAAAA; font-weight: bold; padding: 2px; border: 1px solid #222222">
                            <td>@lang("backend/AttachType.view.listUploaded.GroupAttach"):</td>
                            <td>{!!$group_title!!}</td>
                            <td></td>
                        </tr>
                        <?php
                        }
                        ?>

                        <tr data-idDoc="{!!$itemDoc->attach_document_id!!}"
                            style="padding: 2px; border: 1px solid #555555">
                            <td>@lang("backend/AttachType.view.listUploaded.Document"):</td>
                            <td class="hidden-xs">{!!$itemDoc->title!!}</td>
                            <td>
                                <a target="_blank" href="/{!!$itemDoc->src!!}" class="btn default btn-xs green-stripe">
                                    @lang("backend/AttachType.view.listUploaded.View") </a>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </td>
    </tr>
    @if($item->persona)
        <tr data-id="{!!$item->id!!}" class="details"
            style="display: none; background: #999999; font-weight: bold; padding: 2px; border: 1px solid #111111">
            <td colspan="8">@lang("backend/Booking.view.info_users.Title")</td>
        </tr>
        <tr data-id="{!!$item->id!!}" class="details" style="display: none">
            <td class="details" colspan="8">
                <table class="user-info" style="width: 100%">
                    <tr style="background: #AAAAAA; font-weight: bold; padding: 2px; border: 1px solid #222222">
                        <td style="border: 1px solid #999999">@lang("backend/Booking.view.info_users.persona")</td>
                        <td style="border: 1px solid #999999">@lang("backend/Booking.view.info_users.education")</td>
                        <td style="border: 1px solid #999999">@lang("backend/Booking.view.info_users.institution_type")</td>
                        <td style="border: 1px solid #999999">@lang("backend/Booking.view.info_users.institution")</td>
                    </tr>
                    <tr>
                        <td style="border: 1px solid #999999">{!!$item->persona!!}</td>
                        <td style="border: 1px solid #999999">{!!$item->education!!}</td>
                        <td style="border: 1px solid #999999">{!!$item->institution_type!!}</td>
                        <td style="border: 1px solid #999999">{!!$item->institution!!}</td>
                    </tr>
                    <tr style="background: #AAAAAA; font-weight: bold; padding: 2px; border: 1px solid #222222">
                        <td style="border: 1px solid #999999">@lang("backend/Booking.view.info_users.institution_town")</td>
                        <td style="border: 1px solid #999999">@lang("backend/Booking.view.info_users.current_institution_type")</td>
                        <td style="border: 1px solid #999999">@lang("backend/Booking.view.info_users.current_institution")</td>
                        <td style="border: 1px solid #999999">@lang("backend/Booking.view.info_users.current_institution_town")</td>
                    </tr>
                    <tr>
                        <td style="border: 1px solid #999999">{!!$item->institution_town!!}</td>
                        <td style="border: 1px solid #999999">{!!$item->current_institution_type!!}</td>
                        <td style="border: 1px solid #999999">{!!$item->current_institution!!}</td>
                        <td style="border: 1px solid #999999">{!!$item->current_institution_town!!}</td>
                    </tr>
                    <tr style="background: #AAAAAA; font-weight: bold; padding: 2px; border: 1px solid #222222">
                        <td colspan="3"
                            style="border: 1px solid #999999">@lang("backend/Booking.view.info_users.parent_guarantor")</td>
                        <td style="border: 1px solid #999999">@lang("backend/Booking.view.info_users.accuracy_information")</td>
                    </tr>
                    <tr>
                        <td colspan="3" style="border: 1px solid #999999">{!!$item->parent_guarantor!!}</td>
                        <td style="border: 1px solid #999999">{!!$item->accuracy_information?trans("backend/Booking.view.info_users.OK"):trans("backend/Booking.view.info_users.NotOK")!!}</td>
                    </tr>
                    <tr style="background: #AAAAAA; font-weight: bold; padding: 2px; border: 1px solid #222222">
                        <td colspan="4"
                            style="border: 1px solid #999999">@lang("backend/Booking.view.info_users.personal_message")</td>
                    </tr>
                    <tr>
                        <td colspan="4" style="border: 1px solid #999999">{!!$item->personal_message!!}</td>
                    </tr>
                </table>
            </td>
        </tr>
    @endif
    @if($item->parent_first_name)
        <tr data-id="{!!$item->id!!}" class="details"
            style="display: none; background: #999999; font-weight: bold; padding: 2px; border: 1px solid #111111">
            <td colspan="8">@lang("backend/Booking.view.user_parent.Title")</td>
        </tr>
        <tr data-id="{!!$item->id!!}" class="details" style="display: none">
            <td class="details" colspan="8">
                <table class="user-info" style="width: 100%">
                    <tr style="background: #AAAAAA; font-weight: bold; padding: 2px; border: 1px solid #222222">
                        <td style="border: 1px solid #999999">@lang("backend/Booking.view.user_parent.parent_first_name")</td>
                        <td style="border: 1px solid #999999">@lang("backend/Booking.view.user_parent.parent_last_name")</td>
                        <td style="border: 1px solid #999999">@lang("backend/Booking.view.user_parent.parent_email")</td>
                        <td style="border: 1px solid #999999">@lang("backend/Booking.view.user_parent.parent_mobile")</td>
                    </tr>
                    <tr>
                        <td style="border: 1px solid #999999">{!!$item->parent_first_name!!}</td>
                        <td style="border: 1px solid #999999">{!!$item->parent_last_name!!}</td>
                        <td style="border: 1px solid #999999">{!!$item->parent_email!!}</td>
                        <td style="border: 1px solid #999999">{!!$item->parent_mobile!!}</td>
                    </tr>
                    <tr style="background: #AAAAAA; font-weight: bold; padding: 2px; border: 1px solid #222222">
                        <td style="border: 1px solid #999999">@lang("backend/Booking.view.user_parent.parent_tenant_address")</td>
                        <td style="border: 1px solid #999999">@lang("backend/Booking.view.user_parent.parent_address")</td>
                        <td style="border: 1px solid #999999">@lang("backend/Booking.view.user_parent.parent_postal_code")</td>
                        <td style="border: 1px solid #999999">@lang("backend/Booking.view.user_parent.parent_country_name")</td>
                    </tr>
                    <tr>
                        <td style="border: 1px solid #999999">{!!$item->parent_tenant_address!!}</td>
                        <td style="border: 1px solid #999999">{!!$item->parent_address!!}</td>
                        <td style="border: 1px solid #999999">{!!$item->parent_postal_code!!}</td>
                        <td style="border: 1px solid #999999">{!!$item->parent_country_name!!}</td>
                    </tr>

                </table>
            </td>
        </tr>
    @endif
@endforeach
<tr class="paginate">
    <td colspan="8">
        <div class="row">
            <div class="col-md-5 col-sm-12">
                <div class="dataTables_info" id="infoData" role="status" aria-live="polite"
                     style="vertical-align: middle">
                    {!!$dataBooking->appends(Input::all())->render()!!}
                    <span class="loading"
                          style="background: url('{{ asset('/assets/img/loader.gif') }}') no-repeat;padding: 5px 20px 20px; display: none "></span>
                    Showing
                    {!!$dataBooking->firstItem()!!} to {!!$dataBooking->count()!!}
                    of {!!$dataBooking->total()!!} entries
                </div>
            </div>
        </div>
    </td>
</tr>
<script>
    //    console.log(arrayId);
    jQuery(document).ready(function () {
        fnSelect2();
    });
</script>

