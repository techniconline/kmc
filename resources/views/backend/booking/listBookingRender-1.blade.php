@foreach($dataBooking as $item)
    <tr data-id="{!!$item->id!!}" class="row-item">
        <td>
            <a href="#" class="row-details">
                <i class="fa fa-plus-square-o"></i>
            </a>
        </td>
        <td>
            <span class="username">{!!$item->firstname.' '.$item->lastname!!}</span>
            <span class="email" data-user="{!!$item->user_id!!}">
                {!!$item->email!!}
            </span>
        </td>
        <td>
            {!!$item->birth_date!!}
        </td>
        <td>
            {!!trans("backend/AttachType.view.listUploaded.From").': '.$item->from_date.' '.trans("backend/AttachType.view.listUploaded.To").' : '.$item->to_date!!}
        </td>
        <td>
            {!!$item->gender!!}
        </td>
        <td>
            {!!$item->country_name!!}
        </td>
        <td>
            {!!$item->created_at!!}
        </td>
        <td class="row-action">
            <a href="{!!URL::route('backend.attachment.sendMailToUser')!!}"
               class="btn default btn-xs purple send-mail">
                <i class="fa fa-edit"></i> @lang("backend/AttachType.view.listUploaded.SendMail")
            </a>
            <a href="{!!URL::route('backend.attachment.rejectDocumentsUser', ['user_id'=>$item->user_id,'booking_id'=>$item->id])!!}"
               class="btn default btn-xs red reject">
                <i class="fa fa-times"></i> @lang("backend/AttachType.view.listUploaded.Reject")
            </a>
            <a href="{!!URL::route('backend.attachment.isOkDocumentsUser', ['user_id'=>$item->user_id,'booking_id'=>$item->id])!!}"
               class="btn default btn-xs green is-ok">
                <i class="fa fa-check"></i> @lang("backend/AttachType.view.listUploaded.IsOk")
            </a>
            <span class="loading"
                  style="background: url('{{ asset('/assets/img/loader.gif') }}') no-repeat;padding: 5px 20px 20px; display: none "></span>
            <span class="result"></span>
        </td>

    </tr>
    <tr data-id="{!!$item->id!!}" class="details" style="display: none">
        <td class="details" colspan="8">
            <table>
                <tbody>
                <?php $group_title = '';?>
                @if(isset($item->documents))
                    @foreach($item->documents as $itemDoc)

                        <?php
                        if ($itemDoc->title_group != $group_title) {
                        $group_title = $itemDoc->title_group;
                        ?>
                        <tr style="background: #AAAAAA; font-weight: bold; padding: 2px; border: 1px solid #222222">
                            <td>@lang("backend/AttachType.view.listUploaded.GroupAttach"):</td>
                            <td>{!!$group_title!!}</td>
                            <td></td>
                        </tr>
                        <?php
                        }
                        ?>

                        <tr data-idDoc="{!!$itemDoc->attach_document_id!!}"
                            style="padding: 2px; border: 1px solid #555555">
                            <td>@lang("backend/AttachType.view.listUploaded.Document"):</td>
                            <td class="hidden-xs">{!!$itemDoc->title!!}</td>
                            <td>
                                <a target="_blank" href="/{!!$itemDoc->src!!}" class="btn default btn-xs green-stripe">
                                    @lang("backend/AttachType.view.listUploaded.View") </a>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </td>
    </tr>
@endforeach
<tr class="paginate">
    <td colspan="8">
        <div class="row">
            <div class="col-md-5 col-sm-12">
                <div class="dataTables_info" id="infoData" role="status" aria-live="polite"
                     style="vertical-align: middle">
                    {!!$dataBooking->render()!!}
                    <span class="loading"
                          style="background: url('{{ asset('/assets/img/loader.gif') }}') no-repeat;padding: 5px 20px 20px; display: none "></span>
                    Showing
                    {!!$dataBooking->firstItem()!!} to {!!$dataBooking->count()!!}
                    of {!!$dataBooking->total()!!} entries
                </div>
            </div>
        </div>
    </td>
</tr>