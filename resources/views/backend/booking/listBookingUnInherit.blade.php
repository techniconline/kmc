<p></p>

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXTRAS PORTLET-->
        <div class="portlet box red-intense">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-globe"></i>@lang("backend/Booking.view.listBooking.ListOfUsersBooking")
                </div>
                <div class="actions">
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="table-advance">
                    <thead>
                    <tr role="row" class="heading">
                        <th class="table-checkbox " style="width: 14px;"></th>
                        <th>
                            @lang("backend/Booking.view.listBooking.Name")
                        </th>
                        <th>
                            @lang("backend/Booking.view.listBooking.Birthday")
                        </th>
                        <th class="hidden-xs">
                            @lang("backend/Booking.view.listBooking.BookingDate")
                        </th>
                        <th class="hidden-xs">
                            @lang("backend/Booking.view.listBooking.Gender")
                        </th>
                        <th class="hidden-xs">
                            @lang("backend/Booking.view.listBooking.Country")
                        </th>
                        <th class="hidden-xs">
                            @lang("backend/Booking.view.listBooking.RegisterBookingDate")
                        </th>
                        <th class="hidden-xs">
                        </th>
                    </tr>
                    @if(!isset($notShowFilter))
                        <tr role="row" class="filter">
                            <td>
                                <input type="hidden" class="form-control form-filter input-sm" name="search" value="1">
                            </td>
                            <td colspan="2">
                                <input placeholder="@lang("backend/Booking.view.listBooking.FirstName")" type="text"
                                       class="form-control form-filter input-sm first_name" name="first_name">
                                <input placeholder="@lang("backend/Booking.view.listBooking.LastName")" type="text"
                                       class="form-control form-filter input-sm last_name" name="last_name">
                            </td>
                            {{--<td>--}}
                            {{--</td>--}}
                            <td>
                                <div class="input-group date date-picker margin-bottom-5" data-date-format="yyyy-mm-dd">
                                    <input type="text" class="form-control form-filter input-sm date_from"
                                           name="date_from"
                                           placeholder="@lang("backend/Booking.view.listBooking.From")">
											<span class="input-group-btn">
											<button class="btn btn-sm default" type="button"><i
                                                        class="fa fa-calendar"></i></button>
											</span>
                                </div>
                                <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                                    <input type="text" class="form-control form-filter input-sm date_to" name="date_to"
                                           placeholder="@lang("backend/Booking.view.listBooking.To")">
											<span class="input-group-btn">
											<button class="btn btn-sm default" type="button"><i
                                                        class="fa fa-calendar"></i></button>
											</span>
                                </div>
                            </td>
                            <td>
                                {!!Form::select  ('gender', [''=>trans("backend/Booking.view.listBooking.Select")]+trans("profile.view.user.genderItem"),null,['class'=>'btn form-control input-xsmall select2me','id'=>'gender','required'=>''])!!}
                            </td>
                            <td>
                                <input type="text" class="form-control form-filter input-sm" name="country_name"
                                       placeholder="@lang("backend/Booking.view.listBooking.Country")">
                            </td>
                            <td>
                                <div class="input-group date date-picker margin-bottom-5" data-date-format="yyyy-mm-dd">
                                    <input type="text" class="form-control form-filter input-sm reg_date_from"
                                           name="reg_date_from"
                                           placeholder="@lang("backend/Booking.view.listBooking.From")">
											<span class="input-group-btn">
											<button class="btn btn-sm default" type="button"><i
                                                        class="fa fa-calendar"></i></button>
											</span>
                                </div>
                                <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                                    <input type="text" class="form-control form-filter input-sm reg_date_to"
                                           name="reg_date_to"
                                           placeholder="@lang("backend/Booking.view.listBooking.To")">
											<span class="input-group-btn">
											<button class="btn btn-sm default" type="button"><i
                                                        class="fa fa-calendar"></i></button>
											</span>
                                </div>
                            </td>
                            <td>
                                <div class="margin-bottom-5 search"
                                     data-url="{!!URL::route('backend.booking.search')!!}">
                                    <button class="btn btn-sm yellow filter-submit margin-bottom btn-search"><i
                                                class="fa fa-search"></i> @lang("backend/Booking.view.listBooking.Search")
                                    </button>
                                    <span class="loading"
                                          style="background: url('{{ asset('/assets/img/loader.gif') }}') no-repeat;padding: 5px 20px 20px; display: none "></span>
                                </div>
                            </td>
                        </tr>
                    @endif
                    </thead>
                    <script>
                        var arrayId = [];
                    </script>
                    <tbody id="result-data-table">
                    {!!$pageData!!}
                    </tbody>
                </table>

            </div>
        </div>

    </div>
</div>

