@extends('backend.layouts.default')
<?php  $langDetails = app('activeLangDetails');?>

@section('styles-plugin')

    <link href="{{ asset('/assets/theme/assets/global/plugins/select2/select2.css') }}" rel="stylesheet"
          type="text/css"/>

    @if($langDetails['is_rtl'])
        <link href="{{ asset('/assets/theme/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5-rtl.css') }}"
              rel="stylesheet" type="text/css"/>
        {{--        <link href="{{ asset('/assets/theme/assets/global/plugins/bootstrap-select/bootstrap-select-rtl.min.css') }}" rel="stylesheet" type="text/css"/>--}}
        <link href="{{ asset('/assets/theme/assets/global/plugins/jquery-multi-select/css/multi-select-rtl.css') }}"
              rel="stylesheet" type="text/css"/>

    @else
        <link href="{{ asset('/assets/theme/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css') }}"
              rel="stylesheet" type="text/css"/>
        {{--        <link href="{{ asset('/assets/theme/assets/global/plugins/bootstrap-select/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css"/>--}}
        <link href="{{ asset('/assets/theme/assets/global/plugins/jquery-multi-select/css/multi-select.css') }}"
              rel="stylesheet" type="text/css"/>
    @endif
    <link href="{{ asset('/assets/theme/assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css') }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/theme/assets/global/plugins/bootstrap-summernote/summernote.css') }}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('/assets/theme/assets/global/plugins/jquery-nestable/jquery.nestable.css') }}" rel="stylesheet"
          type="text/css"/>
    {!!Html::loadEditor()!!}

    {{--<link href="{{ asset('/assets/theme/assets/global/plugins/jstree/dist/themes/default/style.min.css') }}" rel="stylesheet" type="text/css"/>--}}
@endsection
@section('styles')

@endsection
@section('content')
    <a href="{!! URL::route($locale.'backend.product.index') !!}" class="btn btn-lg red">
        <i class="fa fa-backward"></i>
        @lang("backend/Product.view.create.Back")
    </a>
    @if(($error))

        @foreach($error as $item)
            <div class="errors">{!!$item!!}</div>
        @endforeach

    @else

        {{--        {!! App\Providers\Helpers\Category\CategoryHelper::treeRenderJsonTree($roots,$translatedName,count($translatedName)) !!}--}}

        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXTRAS PORTLET-->
                <div class="portlet box blue-hoki">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cogs"></i>{!! $title !!}
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">
                            </a>
                            <a href="#portlet-config" data-toggle="modal" class="config">
                            </a>
                            <a href="javascript:;" class="reload">
                            </a>
                            <a href="javascript:;" class="remove">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        @if(isset($data))
                            {!!Form::open(['route' => [$locale.'backend.product.update',$data['id']],'method' => 'PUT','class'=>'form-horizontal form-bordered','files'=> true])!!}
                            {!!Form::hidden('product_id',isset($data['id'])?$data['id']:null,['id'=>'product-id'])!!}

                        @else
                            {!!Form::open(['route' => $locale.'backend.product.store','class'=>'form-horizontal form-bordered','files'=> true])!!}
                        @endif
                        {{--<form class="form-horizontal form-bordered">--}}
                        <div class="form-body">
                            <div class="form-group">
                                {!!Form::label('title',trans("backend/Product.view.create.Title"),['class'=>'control-label col-md-2'])!!}
                                <div class="col-md-10">
                                    {!!Form::text('title',isset($data['title'])?$data['title']:null,array('class'=>'form-control'))!!}
                                    {!!$errors->first('title','<span class="error">:message</span>')!!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!!Form::label('alias',trans("backend/Product.view.create.Alias"),['class'=>'control-label col-md-2'])!!}
                                <div class="col-md-10">
                                    {!!Form::text('alias',isset($data['alias'])?$data['alias']:null,array('class'=>'form-control'))!!}
                                    {!!$errors->first('alias','<span class="error">:message</span>')!!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!!Form::label('image_product',trans("backend/Content.view.form.ImageContent"),['class'=>'control-label col-md-2'])!!}
                                <div class="col-md-4">
                                    {!! Form::file('image_product',['onchange'=>"readURL(this)"]) !!}
                                    {!!$errors->first('image_product','<span class="error">:message</span>')!!}
                                </div>
                                <div class="col-md-4">
                                    @if(isset($data) && ($data['image']) && \Illuminate\Support\Facades\File::exists(base_path().env('PUBLIC_PATH_URL', 'public').$data['image']))
                                        <img class="image_content col-md-12" src="{!! asset($data['image']) !!}"
                                             alt="Image" width="120" height="60"/>
                                    @else
                                        <img class="image_content col-md-12" src="#" alt="Image" width="120"
                                             height="60"/>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                {!!Form::label('category_id',trans("backend/Product.view.create.Category"),['class'=>'control-label col-md-2'])!!}
                                <div class="col-md-10">

                                    {!! CategoryHelper::createBoxTreeForNestable($roots,$translatedName,0,'category',$locale
                                        , trans("backend/Product.view.create.Category"), false, true ,false,true,isset($categoriesProduct)?$categoriesProduct:[]) !!}

                                    {!!$errors->first('categories','<span class="error">:message</span>')!!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!!Form::label('features',trans("backend/Product.view.create.Features"),['class'=>'control-label col-md-2'])!!}
                                <div class="col-md-10">


                                    <div class="portlet box blue">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="fa fa-gift"></i>@lang("backend/Product.view.create.FeatureGroup")
                                            </div>
                                            <div class="tools">
                                                <a href="javascript:;" class="collapse">
                                                </a>
                                                <a href="#portlet-config" data-toggle="modal" class="config">
                                                </a>
                                            </div>
                                        </div>
                                        <div class="portlet-body">
                                            <div class="row">
                                                <div class="col-md-3 col-sm-3 col-xs-3">
                                                    <ul class="nav nav-tabs tabs-left">
                                                        @if($dataFG)
                                                            <?php $count = 0; ?>
                                                            @foreach($dataFG as $itemFG)
                                                                <li class="{{$count?'':'active'}}">
                                                                    <a {{$count?'':'aria-expanded="true"'}}
                                                                       href="#tab_{!! $itemFG->id !!}"
                                                                       data-toggle="tab">{!! $itemFG->name !!}</a>
                                                                </li>
                                                                <?php $count++; ?>
                                                            @endforeach
                                                        @endif
                                                    </ul>
                                                </div>
                                                <div class="col-md-9 col-sm-9 col-xs-9">
                                                    <div class="tab-content">
                                                        @if($allDataFG)
                                                            <?php $count = 0; ?>
                                                            @foreach($allDataFG as $itemFG)
                                                                <div class="tab-pane {{$count?'':'active'}}"
                                                                     id="tab_{!! $itemFG->id !!}">
                                                                    @if(isset($itemFG->features_array))
                                                                        @foreach($itemFG->features_array as $itemF)
                                                                            <div class="col-md-4">
                                                                                {!! $itemF->name !!}
                                                                                @if(isset($itemF->feature_values) && !$itemF->custom )
                                                                                    {!!Form::select ('features['.$itemF->id.'][]', $itemF->feature_values
                                                                                            ,isset($itemF->feature_value_selected)?$itemF->feature_value_selected:null
                                                                                            ,['class'=>'form-control select2', 'multiple'=>'', 'placeholder'=>$itemF->name])!!}
                                                                                @elseif($itemF->custom)
                                                                                    {!!Form::text('new_features_value['.$itemF->id.']'
                                                                                    ,isset($itemF->feature_value_selected)?$itemF->feature_value_selected:null
                                                                                    ,array('class'=>'btn form-control input-small', 'placeholder'=>$itemF->name))!!}
                                                                                @endif
                                                                            </div>
                                                                        @endforeach
                                                                    @endif
                                                                </div>
                                                                <?php $count++; ?>

                                                            @endforeach
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    {!!$errors->first('features','<span class="error">:message</span>')!!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="short_description"
                                       class="control-label col-md-2">@lang("backend/Product.view.create.ShortDescription")</label>

                                <div class="col-md-10">
                                    <textarea name="short_description" class="wysihtml5 form-control"
                                              rows="3">{!! isset($data['short_description'])?$data['short_description']:null !!}</textarea>
                                    {!!$errors->first('short_description','<span class="error">:message</span>')!!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="description"
                                       class="control-label col-md-2">@lang("backend/Product.view.create.Description")</label>

                                <div class="col-md-10">
                                    {!!Form::editor('description',isset($data['description'])?$data['description']:null,'')!!}

                                    {{--<textarea name="description" class="wysihtml5 form-control" rows="6">{!! isset($data['description'])?$data['description']:null !!}</textarea>--}}
                                    {!!$errors->first('description','<span class="error">:message</span>')!!}
                                </div>
                            </div>

                            @if(isset($data))
                                <div class="form-group">
                                    <label for="formatted_address"
                                           class="control-label col-md-2">@lang("backend/Product.view.create.FileUpload")</label>

                                    <div class="col-md-10">

                                        <div id="imageUploader">@lang("backend/Product.view.create.Upload")</div>
                                        <div id="startUploadImage"
                                             class="ajax-file-upload-green">@lang("backend/Product.view.create.StartImageUpload")</div>
                                        <div id="msgUploaderImage" style="border: 1px solid #CD3F3F"></div>
                                        @if(isset($images))
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <!-- BEGIN SAMPLE TABLE PORTLET-->
                                                    <div class="portlet">
                                                        <div class="portlet-title">
                                                            <div class="caption">
                                                                <i class="fa fa-bell-o"></i>@lang("backend/Product.view.create.ImageList")
                                                            </div>
                                                            <div class="tools">
                                                                <a href="javascript:;" class="collapse">
                                                                </a>
                                                                <a href="#portlet-config" data-toggle="modal"
                                                                   class="config">
                                                                </a>
                                                                <a href="javascript:;" class="reload">
                                                                </a>
                                                                <a href="javascript:;" class="remove">
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="portlet-body">
                                                            <div class="table-scrollable">
                                                                <table data-url="{!! URL::route('backend.product.isDefault') !!}"
                                                                       class="table table-striped table-bordered table-advance table-hover">
                                                                    <thead>
                                                                    <tr>
                                                                        <th>
                                                                            <i class="fa fa-file-photo-o"></i> @lang("backend/Product.view.create.Title")
                                                                        </th>
                                                                        <th>
                                                                            <i class="fa fa-file-image-o "></i> @lang("backend/Product.view.create.Image")
                                                                        </th>
                                                                        <th>
                                                                            <i class="fa fa-times "></i> @lang("backend/Product.view.create.Delete")
                                                                        </th>
                                                                        <th>
                                                                            <i class="fa fa-edit "></i> @lang("backend/Product.view.create.IsDefault")
                                                                        </th>
                                                                        <th>
                                                                            <i class="fa fa-edit "></i> @lang("backend/Product.view.create.Sort")
                                                                        </th>
                                                                        <th>
                                                                            <i class="fa fa-edit "></i> @lang("backend/Product.view.create.ClassGallery")
                                                                        </th>
                                                                        <th>
                                                                            <i class="fa fa-edit "></i> @lang("backend/Product.view.create.ColorFeature")
                                                                        </th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    @foreach($images as $img)
                                                                        <tr>
                                                                            <td class="highlight">
                                                                                <div class="success">
                                                                                </div>
                                                                                <div>
                                                                                    <input class="form-control"
                                                                                           type="checkbox"
                                                                                           value="{!!$img->media_id!!}"
                                                                                           name="image[]">
                                                                                    <input class="form-control input-xsmall"
                                                                                           type="text"
                                                                                           value="{!!$img->title?$img->title:'No Title!'!!}"
                                                                                           name="image_title[{!!$img->media_id!!}]"
                                                                                           placeholder="type title...">
                                                                                </div>
                                                                            </td>
                                                                            <td class="hidden-xs">
                                                                                <img width="50" height="33"
                                                                                     src="{!!$img->image!!}" alt=""/>
                                                                            </td>
                                                                            <td>
                                                                                <input type="checkbox"
                                                                                       value="{!!$img->media_id!!}"
                                                                                       name="media_del[]">
                                                                                {{--<a href="#{!!$img->media_id!!}" class="btn default btn-xs red">--}}
                                                                                {{--<i class="fa fa-times"></i> Delete </a>--}}
                                                                            </td>
                                                                            <td>
                                                                                <input class="media-default"
                                                                                       type="radio"
                                                                                       value="{!!$img->media_id!!}"
                                                                                       {!! $img->is_default?'checked':'' !!}
                                                                                       name="image_default">
                                                                            </td>
                                                                            <td>
                                                                                <input class="item-image-sort form-control input-xsmall"
                                                                                       type="text"
                                                                                       name="sort[{!!$img->id!!}]"
                                                                                       value="{!!$img->sort?$img->sort:1!!}"/>
                                                                            </td>
                                                                            <td>
                                                                                <input class="item-image-class form-control input-small"
                                                                                       type="text"
                                                                                       name="class[{!!$img->id!!}]"
                                                                                       value="{!!$img->class?$img->class:''!!}"/>
                                                                            </td>
                                                                            <td>
                                                                                {!!Form::select ('feature_value_selected['.$img->id.']', $color_selected ,isset($img->feature_value_selected)&&$img->feature_value_selected?$img->feature_value_selected:''
                                                                                    ,['class'=>'form-control input-medium',  'placeholder'=>''])!!}
                                                                            </td>
                                                                        </tr>
                                                                    @endforeach


                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- END SAMPLE TABLE PORTLET-->
                                                </div>
                                            </div>
                                        @endif
                                        <div id="videoUploader">@lang("backend/Product.view.create.Upload")</div>
                                        <div id="startUploadVideo"
                                             class="ajax-file-upload-green">@lang("backend/Product.view.create.StartVideoUpload")</div>
                                        <div id="msgUploaderVideo" style="border: 1px solid #CD3F3F"></div>
                                        @if(isset($videos))
                                            <div class="row">
                                                <div class="col-md-10">
                                                    <!-- BEGIN SAMPLE TABLE PORTLET-->
                                                    <div class="portlet">
                                                        <div class="portlet-title">
                                                            <div class="caption">
                                                                <i class="fa fa-bell-o"></i>@lang("backend/Product.view.create.VideoList")
                                                            </div>
                                                            <div class="tools">
                                                                <a href="javascript:;" class="collapse">
                                                                </a>
                                                                <a href="#portlet-config" data-toggle="modal"
                                                                   class="config">
                                                                </a>
                                                                <a href="javascript:;" class="reload">
                                                                </a>
                                                                <a href="javascript:;" class="remove">
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="portlet-body">
                                                            <div class="table-scrollable">
                                                                <table class="table table-striped table-bordered table-advance table-hover">
                                                                    <thead>
                                                                    <tr>
                                                                        <th>
                                                                            <i class="fa fa-file-video-o"></i>@lang("backend/Product.view.create.Title")
                                                                        </th>
                                                                        <th>
                                                                            <i class="fa fa-link"></i>@lang("backend/Product.view.create.Link")
                                                                        </th>
                                                                        <th>
                                                                            <i class="fa fa-times "></i>@lang("backend/Product.view.create.Delete")
                                                                        </th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    @foreach($videos as $vdo)
                                                                        <tr>
                                                                            <td class="highlight">
                                                                                <div class="success">
                                                                                </div>
                                                                                @if(!$img->title)
                                                                                    <div>
                                                                                        <input type="checkbox"
                                                                                               value="{!!$vdo->media_id!!}"
                                                                                               name="video[]">
                                                                                        <input type="text" value=""
                                                                                               name="video_title[{!!$vdo->media_id!!}]"
                                                                                               placeholder="type title...">
                                                                                    </div>
                                                                                @else
                                                                                    <a href="{!!$vdo->video!!}">
                                                                                        {!!$vdo->title?$vdo->title:'No Title!'!!}
                                                                                    </a>
                                                                                @endif
                                                                            </td>
                                                                            <td class="hidden-xs">
                                                                                <a class="btn btn-sm grey-cascade"
                                                                                   href="{!!$vdo->video!!}">
                                                                                    View
                                                                                    <i class="fa fa-link"></i>
                                                                                </a>
                                                                            </td>
                                                                            <td>
                                                                                <input type="checkbox"
                                                                                       value="{!!$vdo->media_id!!}"
                                                                                       name="media_del[]">
                                                                            </td>
                                                                        </tr>
                                                                    @endforeach


                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- END SAMPLE TABLE PORTLET-->
                                                </div>
                                            </div>
                                        @endif

                                    </div>
                                </div>
                            @endif

                            <div class="form-group">
                                <label for="floor_number"
                                       class="control-label col-md-2">@lang("backend/Product.view.create.Price")</label>

                                <div class="col-md-2">
                                    <input type="text" value="{!! isset($data['price'])?$data['price']:null !!}"
                                           class="form-control" maxlength="10" name="price" id="mask_number">
                                    {!!$errors->first('price','<span class="error">:message</span>')!!}
                                </div>

                                <label for="quantity"
                                       class="control-label col-md-2">@lang("backend/Product.view.create.Quantity")</label>

                                <div class="col-md-2">
                                    <input type="text" value="{!! isset($data['quantity'])?$data['quantity']:null !!}"
                                           class="form-control" maxlength="5" name="quantity" id="mask_quantity">
                                    {!!$errors->first('quantity','<span class="error">:message</span>')!!}
                                </div>

                                <label for="condition"
                                       class="control-label col-md-2">@lang("backend/Product.view.create.Condition")</label>

                                <div class="col-md-2">
                                    {!!Form::select('condition',
                                    [
                                    'new'=>trans("backend/Product.view.create.new"),
                                    'gift'=>trans("backend/Product.view.create.gift"),
                                    ],isset($data['condition'])?$data['condition']:null,['class'=>'btn form-control input-small'])!!}
                                    {!!$errors->first('condition','<span class="error">:message</span>')!!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="active_date" class="control-label col-md-2"><i
                                            class="fa fa-calendar"></i> @lang("backend/Product.view.create.Active_date")
                                </label>

                                <div class="col-md-3" id="active-date">
                                    <?php $dateArr = isset($data['active_date'])&&$data['active_date'] ? explode(" ", $data['active_date']) : '';  ?>
                                    {{--<input type="text"--}}
                                           {{--value="{!! isset($data['active_date'])&&$data['active_date']&&$dateArr?--}}
                                               {{--App\Providers\Helpers\PersianDate\PersianDate::convert_date_M_to_H($dateArr[0]).' '.$dateArr[1]--}}
                                               {{--:App\Providers\Helpers\PersianDate\PersianDate::convert_date_M_to_H(date('Y-m-d')).' '.date('H:i:s') !!}"--}}
                                           {{--class="form-control input-lg" id="input-date" maxlength="5"--}}
                                           {{--name="active_date" style="text-align: left; direction: ltr">--}}

                                        {!! App\Providers\Helpers\PersianDate\PersianDate::renderCalenderSelector('active_date','active_date','active-date',1394,1395
                                            ,isset($data['active_date'])&&$data['active_date']&&$dateArr?
                                               App\Providers\Helpers\PersianDate\PersianDate::convert_date_M_to_H($dateArr[0])
                                               :App\Providers\Helpers\PersianDate\PersianDate::convert_date_M_to_H(date('Y-m-d'))
                                               ,App\Providers\Helpers\PersianDate\PersianDate::convert_date_M_to_H(date('Y-m-d')),true
                                               ,(isset($dateArr[1])&&$dateArr[1]?$dateArr[1]:'12:00:00')) !!}

                                    {!!$errors->first('active_date','<span class="error">:message</span>')!!}
                                </div>
                            </div>

                            {{--<input type="hidden" value="{!! isset($data['lat'])?$data['lat']:null !!}"--}}
                            {{--class="form-control lat-apt" maxlength="15" name="lat" id="mask_number">--}}
                            {{--<input type="hidden" value="{!! isset($data['lng'])?$data['lng']:null !!}"--}}
                            {{--class="form-control lng-apt" maxlength="15" name="lng" id="mask_number">--}}
                            {{--{!!$errors->first('lng','<span class="error">:message</span>')!!}--}}
                            {{--{!!$errors->first('lat','<span class="error">:message</span>')!!}--}}


                            {{--<div class="form-group last">--}}
                            {{--<label for="map"--}}
                            {{--class="control-label col-md-2">@lang("backend/Product.view.create.Map")</label>--}}

                            {{--<div class="col-md-10">--}}
                            {{--<div id="map-canvas"></div>--}}
                            {{--</div>--}}

                            {{--</div>--}}


                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-6">
                                    {!!Form::select('lang', $listLang,$setLang,['class'=>'btn form-control input-small select2me'])!!}
                                    <button type="submit" class="btn purple"><i
                                                class="fa fa-check"></i>{!! $btnTitle !!}</button>
                                    <button type="button"
                                            class="btn default">@lang("backend/Product.view.create.Cancel")</button>
                                </div>
                            </div>
                        </div>
                        {!!Form::close()!!}

                    </div>
                </div>
            </div>
        </div>


    @endif
@endsection
@section('scripts-plugin')

    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-markdown/lib/markdown.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-summernote/summernote.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/jquery-nestable/jquery.nestable.js') }}"
            type="text/javascript"></script>

    {{--    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-select/bootstrap-select.min.js') }}" type="text/javascript"></script>--}}
    <script src="{{ asset('/assets/theme/assets/global/plugins/select2/select2.min.js') }}"
            type="text/javascript"></script>
    {{--    <script src="{{ asset('/assets/theme/assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js') }}" type="text/javascript"></script>--}}


@endsection
@section('scripts')
    <script src="{{ asset('/assets/theme/assets/admin/pages/scripts/components-editors.js') }}"
            type="text/javascript"></script>
    {{--    <script src="{{ asset('/assets/theme/assets/admin/pages/scripts/ui-nestable.js') }}" type="text/javascript"></script>--}}


    @if(!($error))
        <script>
        {{--var dbLat = 52;--}}
        {{--var dbLng = 37;--}}
        {{--var zoom = 6;--}}
        {{--@if(isset($data['lat']) && isset($data['lng']) && $data['lat'] && $data['lng'])--}}
        {{--dbLat = {!! $data['lat'] !!};--}}
        {{--dbLng = {!! $data['lng'] !!};--}}
        {{--zoom = 16;--}}
        {{--@endif--}}
        var baseUrl = '{!!URL::route('uploadFile')!!}';
        @if(isset($data))
        var direct = 'product';
        var maxFileSizeImg = 1024 * 2 * 1024;//2M
        var maxFileSizeVideo = 1024 * 30 * 1024;//30M
        @endif
        </script>

        {{--<script src="{{ asset('https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places') }}"--}}
        {{--type="text/javascript"></script>--}}
        {{--<link href="{{ asset('/assets/css/backend/map.css') }}" rel="stylesheet" type="text/css"/>--}}
        {{--<script src="{{ asset('/assets/js/backend/map.js') }}" type="text/javascript"></script>--}}

        {{--        <script src="{{ asset('/assets/theme/assets/global/plugins/jstree/dist/jstree.min.js') }}" type="text/javascript"></script>--}}
        {{--        <script src="{{ asset('/assets/theme/assets/admin/pages/scripts/ui-tree.js') }}" type="text/javascript"></script>--}}
        <script src="{{ asset('/assets/js/backend/category.js') }}" type="text/javascript"></script>
        {{--{!! Html::script('https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places')!!}--}}
        {{--{!! Html::style('assets/css/backend/map.css')!!}--}}
        {{--{!! Html::script('assets/js/backend/map.js')!!}--}}
        @if(isset($data))
            <link href="{{ asset('/assets/css/uploadfile.min.css') }}" rel="stylesheet" type="text/css"/>
            <script src="{{ asset('/assets/js/jquery.uploadfile.min.js') }}" type="text/javascript"></script>
            <script src="{{ asset('/assets/js/generalUpload.js') }}" type="text/javascript"></script>
            {{--{!! Html::style('assets/css/uploadfile.min.css')!!}--}}
            {{--{!! Html::script('assets/js/jquery.uploadfile.min.js')!!}--}}
            {{--{!! Html::script('assets/js/generalUpload.js')!!}--}}
        @endif
    @endif
@endsection
@section('scripts-inline')

    <script>

        $('.select2').select2({
            placeholder: "features",
            allowClear: true
        });

        jQuery(document).ready(function () {
            // initiate layout and plugins
//            Metronic.init(); // init metronic core components
//            Layout.init(); // init current layout
//            QuickSidebar.init(); // init quick sidebar
//            Demo.init(); // init demo features
            ComponentsEditors.init();
//            CategoriesTree.init();
            CategoriesList.init();

        });
        $(function () {
            $("#input-date").persianDatepicker(
                    {
                        fontSize: 11, // by px
                        formatDate: "YYYY-0M-0D hh:mm:ss"
//                        startDate: "1310/01/01",
//                        endDate:"1379/12/30"
                    }
            );
        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('.image_product').attr('src', e.target.result)
                            .css("width", "120px")
                            .css("height", "60px");
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>

@endsection