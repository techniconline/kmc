@foreach($dataRoomRented as $item)
    <tr data-id="{!!$item->booking_id!!}" class="row-item">
        <td>
            {{--<a href="#" class="row-details">--}}
            {{--<i class="fa fa-plus-square-o"></i>--}}
            {{--</a>--}}
        </td>
        <td style="width: 200px">
            <span class="username">{!!$item->firstname.' '.$item->lastname!!}</span>
            <span class="email" data-user="{!!$item->user_id!!}">
                {!!$item->email!!}
            </span>

            <div class="mobile" data-user="{!!$item->user_id!!}">
                <b>@lang("backend/Booking.view.listBooking.Mobile"):</b> {!!$item->mobile!!}
            </div>
            <div class="register_date_user" data-user="{!!$item->user_id!!}">
                <b>@lang("backend/User.view.index.RegDate"):</b>

                <p>{!!$item->register_date_user!!}</p>
            </div>
        </td>
        <td style="width: 250px">
            <div><b>@lang("backend/Room.view.listRoomRented.RoomNumber"):</b>{!!$item->room_number!!}</div>
            <div><b>@lang("backend/Room.view.listRoomRented.Apartment"):</b>{!!$item->apartment_title!!}</div>
            <div><b>@lang("backend/Room.view.listRoomRented.Category"):</b>{!!$item->category_title!!}</div>
            <div><b>@lang("backend/Room.view.listRoomRented.Area"):</b>{!!$item->area!!}~{!!$item->to_area!!}</div>
        </td>
        <td>
            {!!trans("backend/Booking.view.listBooking.From").': '.$item->from_date.' '.trans("backend/Booking.view.listBooking.To").' : '.$item->to_date!!}
        </td>
        <td>
            {!!$item->gender!!}
        </td>
        <td>
            {!!$item->type!!}
        </td>
        <td>
            @if($item->rent_status==2)
                @lang("backend/Room.view.listRoomRented.Reserve")
            @elseif($item->rent_status==3)
                @lang("backend/Room.view.listRoomRented.Rent")
            @endif
        </td>
        <td class="row-action">
            @if(isset($showActionRoomRented)&&$showActionRoomRented)
                <a href="{!!URL::route('backend.room.cancelRentRoom', ['rent_id'=>$item->id,'user_id'=>$item->user_id])!!}"
                   class="btn default btn-xs red cancelRent"
                   title="@lang("backend/Room.view.listRoomRented.CancelRented")">
                    <i class="fa fa-times"></i> @lang("backend/Room.view.create.Cancel")
                </a>
                {{--<a href="{!!URL::route('backend.booking.sendContract', ['booking_id'=>$item->id])!!}"--}}
                {{--class="btn default btn-xs green send-contract">--}}
                {{--<i class="fa fa-check"></i> @lang("backend/Booking.view.listBooking.sendContract")--}}
                {{--</a>--}}

                <span class="loading"
                      style="background: url('{{ asset('/assets/img/loader.gif') }}') no-repeat;padding: 5px 20px 20px; display: none "></span>
                <span class="result"></span>
            @endif
        </td>

    </tr>
@endforeach
<tr class="paginate">
    <td colspan="8">
        <div class="row">
            <div class="col-md-5 col-sm-12">
                <div class="dataTables_info" id="infoData" role="status" aria-live="polite"
                     style="vertical-align: middle">
                    {!!$dataRoomRented->appends(Input::all())->render()!!}
                    <span class="loading"
                          style="background: url('{{ asset('/assets/img/loader.gif') }}') no-repeat;padding: 5px 20px 20px; display: none "></span>
                    Showing
                    {!!$dataRoomRented->firstItem()!!} to {!!$dataRoomRented->count()!!}
                    of {!!$dataRoomRented->total()!!} entries
                </div>
            </div>
        </div>
    </td>
</tr>


