@extends('backend.layouts.default')
@section('styles-plugin')

@endsection
@section('styles')

@endsection
@section('content')

    <a class="btn btn-lg purple" href="{!!URL::route('backend.room.listRoomRented')!!}">
        <i class="fa fa-list"></i>
        @lang("backend/Room.view.index.ListRoomsRented")
    </a>

    <p></p>
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN SAMPLE TABLE PORTLET-->
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-bell-o"></i>@lang("backend/Room.view.index.ListRooms")
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse">
                        </a>
                        <a href="#portlet-config" data-toggle="modal" class="config">
                        </a>
                        <a href="javascript:;" class="reload">
                        </a>
                        <a href="javascript:;" class="remove">
                        </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-scrollable">
                        <table class="table table-striped table-bordered table-advance table-hover">
                            <thead>
                            <tr>
                                <th>
                                    <i class="fa fa-building"></i>@lang("backend/Room.view.index.Apartment")
                                </th>
                                <th class="hidden-xs">
                                    <i class="fa fa-keyboard-o"></i>@lang("backend/Room.view.index.RoomCategory")
                                </th>
                                <th>
                                    <i class="fa fa-cube"></i> @lang("backend/Room.view.index.Area") (m2)
                                </th>
                                <th>
                                    <i class="fa fa-location-arrow"></i> @lang("backend/Room.view.index.Type")
                                </th>
                                <th>
                                    <i class="fa fa-home"></i> @lang("backend/Room.view.index.Rooms")
                                </th>
                                <th>
                                    <i class="fa fa-tree"></i> @lang("backend/Room.view.index.Floor")
                                </th>
                                <th>
                                </th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($data as $item)
                                <tr class="row-item" data-id="{!!$item->id!!}">
                                    <td class="highlight">
                                        <div class="success">
                                        </div>
                                        <a href="{!! URL::route($locale.'backend.apartment.edit',['apartment_id'=>$item->apartment_id]) !!}">
                                            {!! (($item->apartment_title)?$item->apartment_title:'No Title' ) !!}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="{!! URL::route($locale.'backend.roomCategory.edit',['room_category_id'=>$item->room_category_id]) !!}">
                                            {!! (($item->category_title)?$item->category_title:'No Title') !!}
                                        </a>
                                    </td>
                                    <td>
                                        {!! (($item->area)?$item->area:'0') !!} ~
                                        {!! (($item->area)?$item->to_area:'0') !!}
                                    </td>
                                    <td>
                                        {!! (($item->type)?$item->type:'0') !!}
                                    </td>
                                    <td>
                                        {!! (($item->room_number)?$item->room_number:'0') !!}
                                    </td>
                                    <td>
                                        {!! (($item->floor_number)?$item->floor_number:'0') !!}
                                    </td>

                                    <td data-id="{!!$item->id!!}" class="row-action">
                                        @if($item->Count_Room==$item->room_number)
                                            <a href="{!!URL::route($locale.'backend.room.edit', ['apartment_room_category_map_id'=>$item->id])!!}"
                                               class="btn default btn-xs purple edit">
                                                <i class="fa fa-edit"></i> @lang("backend/Room.view.index.Edit") </a>
                                            <a href="{!!URL::route($locale.'backend.room.destroy', ['apartment_room_category_map_id'=>$item->id])!!}"
                                               class="btn default btn-xs red delete">
                                                <i class="fa fa-times"></i> @lang("backend/Room.view.index.Delete") </a>
                                        @else
                                            <a href="{!!URL::route('backend.room.generateRooms', ['apartment_room_category_map_id'=>$item->id])!!}"
                                               class="btn default btn-xs green generate">
                                                <i class="fa fa-cubes"></i> @lang("backend/Room.view.index.GenerateRooms")
                                            </a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END SAMPLE TABLE PORTLET-->
        </div>
    </div>

@endsection
@section('scripts-plugin')
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootbox/bootbox.min.js') }}"
            type="text/javascript"></script>
@endsection
@section('scripts')
    <script src="{{ asset('/assets/js/backend/category.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/assets/js/backend/room.js') }}" type="text/javascript"></script>

@endsection
@section('scripts-inline')
    <script>
        jQuery(document).ready(function () {

        });
    </script>
@endsection

