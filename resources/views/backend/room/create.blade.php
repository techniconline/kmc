@extends('backend.layouts.default')
@section('styles-plugin')

@endsection
@section('styles')
    {!!UploaderHelper::loadUploaderJquery()!!}

@endsection
@section('content')
    <a href="{!! URL::route($locale.'backend.room.index') !!}" class="btn btn-lg red">
        <i class="fa fa-backward"></i>
        @lang("backend/Room.view.create.Back")
    </a>

    @if(isset($result))
        {!!$result!!}
    @endif
    @if($data_category)

        <div class="row">
            <div class="col-md-10">
                <!-- BEGIN SAMPLE TABLE PORTLET-->
                <div class="portlet">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-bell-o"></i>@lang("backend/Room.view.create.Apartment"):
                            <b>{!!$data_category->apartment_title!!}</b>
                            , @lang("backend/Room.view.create.Category"): <b>{!!$data_category->category_title!!}</b>
                            , @lang("backend/Room.view.create.Area"): <b>{!!$data_category->area!!}</b>
                            , @lang("backend/Room.view.create.Type"): <b>{!!$data_category->type!!}</b>
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">
                            </a>
                            <a href="#portlet-config" data-toggle="modal" class="config">
                            </a>
                            <a href="javascript:;" class="reload">
                            </a>
                            <a href="javascript:;" class="remove">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-scrollable">
                            <table class="table table-striped table-bordered table-advance table-hover">
                                <thead>
                                <tr>
                                    <th>
                                        <i class="fa fa-home"></i>@lang("backend/Room.view.create.RoomNumber")
                                    </th>
                                    <th>
                                        <i class="fa fa-euro"></i>@lang("backend/Room.view.create.RoomPrice")
                                    </th>
                                    <th>
                                        <i class="fa fa-image"></i>@lang("backend/Room.view.create.RoomImage")
                                    </th>

                                    <th>
                                    </th>
                                </tr>
                                </thead>

                                <tbody>
                                {!!Form::open(['route' => [$locale.'backend.room.update',$data_category->id],'method' => 'PUT'])!!}
                                <?php $counter = 0; ?>
                                @foreach($data as $item)
                                    <tr class="row-item" data-id="{!!$item->id!!}">
                                        <td class="highlight" style="width: 100px">
                                            <div class="col-md-3">
                                                <div class="input-icon">
                                                    <i class="fa fa-home"></i>
                                                    {!!Form::text('room_number['.$item->id.']',$item->room_number,array('class'=>'form-control input-small room-number-map'))!!}
                                                    {!!$errors->first('room_number','<span class="error">:message</span>')!!}
                                                </div>
                                            </div>
                                        </td>
                                        <td class="highlight">
                                            <div class="col-md-3">
                                                <div class="input-icon">
                                                    <i class="fa fa-euro"></i>
                                                    {!!Form::text('room_price['.$item->id.']',$item->price?$item->price:$data_category->default_price,array('class'=>'form-control input-xsmall room-price-map'))!!}
                                                    {!!$errors->first('room_price','<span class="error">:message</span>')!!}
                                                </div>
                                            </div>
                                        </td>

                                        <td data-id="{!!$item->id!!}" class="row-pic col-md-8">
                                            <?php $counterPic = 0; ?>
                                            @if($item->pictures)
                                                <div class="portlet-body">
                                                    @foreach($item->pictures as $itemPic)
                                                        <?php $counterPic++; ?>
                                                        <label class="checkbox-inline link"
                                                               style="border-bottom: 1px solid #BBBBBB"> {!!$counterPic.'. '.$itemPic->title!!}
                                                            <a class="btn btn-xs btn-primary" target="_blank"
                                                               href="{!!$itemPic->linkMedia!!}">
                                                                <i class="fa fa-link"></i>
                                                                @lang("backend/Room.view.create.LinkPicture")
                                                            </a>
                                                            <a href="{!!URL::route('backend.room.delPictures',['room_id'=>$item->id,'media_id'=>$itemPic->media_id])!!}"
                                                               class="btn btn-xs red delete-pic"
                                                               data-id="{!!$itemPic->media_id!!}">
                                                                <i class="fa fa-times"></i>
                                                                @lang("backend/Room.view.create.Delete")
                                                            </a>
                                                            <span class="loading"
                                                                  style="background: url('{{ asset('/assets/img/loader.gif') }}') no-repeat;padding: 10px 20px 20px; margin-left: 50%; display: none"></span>
                                                        </label>
                                                    @endforeach
                                                </div>

                                            @endif

                                            @if($counterPic<15)
                                                <div class="form-group" id="form-uploader-{!!$item->id!!}"
                                                     style="margin-bottom: 1px !important;">
                                                    <label class="col-md-2 control-label"></label>

                                                    <div class="input-group col-md-10">

                                                <span class="input-group-addon">
                                                    <i class="fa fa-file-image-o"></i>
                                                    {!!
                                                    UploaderHelper::renderUploaderJquery($idUploader='imageUploader-'.$item->id
                                                    ,$idStartUploader='startUploadImage'.$item->id
                                                    ,$idMsgUploader='msgUploaderImage'.$item->id
                                                    ,$titleUpload=trans("backend/Room.view.create.Upload")
                                                    ,$titleStartUpload=trans("backend/Room.view.create.Upload")
                                                    ,trans("backend/Room.view.create.UploadFilesTitle")
                                                    ,$urlUploader=URL::route('uploadFile')
                                                    ,$objUploaderName='uploadObjImg'.$item->id
                                                    ,$autoUpload='true'
                                                    ,$subFolder='picRoomsUpload'
                                                    ,$type='images'
                                                    ,$maxFileSize=20,$itemId=$item->id,$allowedTypes='png,gif,jpg,jpeg'
                                                    ,$showDelete='true'
                                                    ,$showDone='false'
                                                    ,$fileName='myfile'
                                                    ,$titleButtonDrag=trans("backend/Room.view.create.titleDrag")
                                                    ,$counter+=1
                                                    ,$titleSaveDesc=trans("backend/Room.view.create.titleSaveDesc")
                                                    ,$controller='room',$controllerDoc='room'
                                                    ,$maxCountFileUploaded=15
                                                    ,$multiUpload='true'
                                                    )
                                                    !!}

                                                    <div class="success"></div>
                                                </span>
                                                        <span style="top: 5px"></span>
                                                    </div>
                                                </div>
                                            @endif

                                        </td>
                                        <td data-id="{!!$item->id!!}" class="row-action">
                                            @if($counterPic<15)
                                                <button type="button"
                                                        data-url="{!!URL::route('backend.room.savePictures',['room_id'=>$item->id])!!}"
                                                        class="btn green save-pictures">@lang("backend/Room.view.create.SavePictures")</button>
                                                <span class="result"><i class="fa fa-error font-yellow"></i></span>
                                            @endif
                                            <span class="loading"
                                                  style="background: url('{{ asset('/assets/img/loader.gif') }}') no-repeat;padding: 5px 20px 20px; display: none "></span>
                                        </td>
                                    </tr>

                                    @if($item->room_status>1 && $item->dataRent)

                                        <tr class="row-item-rent-status"
                                            style="background-color: #cccccc; border-bottom: 2px solid #000000">
                                            <td>
                                                <h4>@lang("backend/Room.view.create.RentStatus")</h4>
                                                <h6>@lang("backend/Room.view.create.RoomNumber") {!!$item->room_number!!}</h6>
                                            </td>
                                            <td colspan="3" style="vertical-align: middle">
                                                <div class="col-md-10">
                                                    <div class="input-icon">
                                                        <i class="fa fa-edit"></i>

                                                        <p><b>@lang("backend/Room.view.create.Username")
                                                                :</b> {!!$item->dataRent->firstname.' '.$item->dataRent->lastname!!}
                                                            | <b>@lang("backend/Room.view.create.Gender")
                                                                :</b> {!!$item->dataRent->gender!!}
                                                            | <b>@lang("backend/Room.view.create.Email")
                                                                :</b> {!!$item->dataRent->email!!}</p>

                                                        <p><b>@lang("backend/Room.view.create.Apartment")
                                                                :</b> {!!$item->dataRent->apartment_title!!}
                                                            | <b>@lang("backend/Room.view.create.Category")
                                                                :</b> {!!$item->dataRent->category_title!!}</p>

                                                        <h3>@if($item->room_status==2)
                                                                <b style="color: #990000">@lang("backend/Room.view.create.Reserve")</b>
                                                            @elseif($item->room_status==3)
                                                                <b style="color: #008000">@lang("backend/Room.view.create.Rent")</b>
                                                            @endif

                                                            | <b>@lang("backend/Room.view.create.Date")
                                                                :</b> {!!$item->dataRent->from_date!!}
                                                            ~ {!!$item->dataRent->to_date!!}
                                                        </h3>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>

                                    @endif
                                    @if($data_category->type=='room')
                                        <tr class="row-item-status"
                                            style="background-color: #AAAAAA; border-bottom: 1px solid #000000">
                                            <td>
                                                <h4>@lang("backend/Room.view.create.Status")</h4>
                                                <h6>@lang("backend/Room.view.create.RoomNumber") {!!$item->room_number!!}</h6>
                                            </td>
                                            <td colspan="3" style="vertical-align: middle">

                                                <table>
                                                    <tr>
                                                        <td>@lang("backend/Room.view.create.Wall")</td>
                                                        <td>@lang("backend/Room.view.create.Facilities")</td>
                                                        <td>@lang("backend/Room.view.create.Doors")</td>
                                                        <td>@lang("backend/Room.view.create.MediaTools")</td>
                                                        <td>@lang("backend/Room.view.create.Floor")</td>
                                                        <td>@lang("backend/Room.view.create.Total")</td>
                                                    </tr>
                                                    <tr>
                                                        <td>{!! Form::select  ('wall_status['.$item->id.']', trans("backend/Room.view.create.StatusList"),$item->wall_status,['class'=>'btn form-control input-small select2me'])!!}</td>
                                                        <td>{!! Form::select  ('facilities_status['.$item->id.']', trans("backend/Room.view.create.StatusList"),$item->facilities_status,['class'=>'btn form-control input-small select2me'])!!}</td>
                                                        <td>{!! Form::select  ('doors_status['.$item->id.']', trans("backend/Room.view.create.StatusList"),$item->doors_status,['class'=>'btn form-control input-small select2me'])!!}</td>
                                                        <td>{!! Form::select  ('media_tools_status['.$item->id.']', trans("backend/Room.view.create.StatusList"),$item->media_tools_status,['class'=>'btn form-control input-small select2me'])!!}</td>
                                                        <td>{!! Form::select  ('floor_status['.$item->id.']', trans("backend/Room.view.create.StatusList"),$item->floor_status,['class'=>'btn form-control input-small select2me'])!!}</td>
                                                        <td>{!! Form::select  ('total_status['.$item->id.']', trans("backend/Room.view.create.StatusList"),$item->total_status,['class'=>'btn form-control input-small select2me'])!!}</td>
                                                    </tr>
                                                </table>

                                            </td>
                                        </tr>
                                    @endif
                                    <tr class="row-item-desc"
                                        style="background-color: #999999; border-bottom: 2px solid #000000">
                                        <td>
                                            <h4>@lang("backend/Room.view.create.Description")</h4>
                                            <h6>@lang("backend/Room.view.create.RoomNumber") {!!$item->room_number!!}</h6>
                                        </td>
                                        <td colspan="3" style="vertical-align: middle">
                                            <div class="col-md-10">
                                                <div class="input-icon">
                                                    <i class="fa fa-edit"></i>
                                                    {!!Form::textarea('room_desc['.$item->id.']',$item->description?$item->description:'',array('class'=>'form-control room-description-map','rows'=>2
                                                        ,'placeholder'=>trans("backend/Room.view.create.Description")))!!}
                                                    {!!$errors->first('room_desc','<span class="error">:message</span>')!!}
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                <tr class="row-item">
                                    <td colspan="3">
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-6">
                                                    <button type="submit" class="btn purple"><i class="fa fa-check"></i>
                                                        @lang("backend/Room.view.create.UpdateRooms")</button>
                                                    <button type="button"
                                                            class="btn default">@lang("backend/Room.view.create.Cancel")</button>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                {!!Form::close()!!}
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
                <!-- END SAMPLE TABLE PORTLET-->
            </div>
        </div>

    @endif
@endsection
@section('scripts-plugin')

@endsection
@section('scripts')
    <script src="{{ asset('/assets/js/backend/category.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/assets/js/backend/room.js') }}" type="text/javascript"></script>

@endsection
@section('scripts-inline')
    <script>
        jQuery(document).ready(function () {

            $('body').find('div.ajax-file-upload').css('width', '100%');
            $('body').find('div.ajax-upload-dragdrop').css('width', '100%');

        });
    </script>
@endsection

