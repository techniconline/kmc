<p></p>

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXTRAS PORTLET-->
        <div class="portlet box red-intense">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-globe"></i>@lang("backend/Room.view.listRoomRented.listRoomRentedUser")
                </div>
                <div class="actions">
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="table-advance">
                    <thead>
                    <tr role="row" class="heading">
                        <th class="table-checkbox " style="width: 14px;"></th>
                        <th>
                            @lang("backend/Room.view.listRoomRented.Username")
                        </th>
                        <th>
                            @lang("backend/Room.view.listRoomRented.RoomNumber")
                        </th>
                        <th class="hidden-xs">
                            @lang("backend/Room.view.listRoomRented.BookingDate")
                        </th>
                        <th class="hidden-xs">
                            @lang("backend/Room.view.listRoomRented.Gender")
                        </th>
                        <th class="hidden-xs">
                            @lang("backend/Room.view.listRoomRented.Type")
                        </th>
                        <th class="hidden-xs">
                            @lang("backend/Room.view.listRoomRented.RentStatus")
                        </th>
                        <th class="hidden-xs">
                        </th>
                    </tr>
                    <tr role="row" class="filter">
                        <td>
                            <input type="hidden" class="form-control form-filter input-sm" name="search" value="1">
                        </td>
                        <td>
                            <input placeholder="@lang("backend/Booking.view.listBooking.FirstName")" type="text"
                                   class="form-control form-filter input-sm first_name" name="first_name">
                            <input placeholder="@lang("backend/Booking.view.listBooking.LastName")" type="text"
                                   class="form-control form-filter input-sm last_name" name="last_name">
                        </td>
                        <td>
                            <input placeholder="@lang("backend/Room.view.listRoomRented.RoomNumber")" type="text"
                                   class="form-control form-filter input-sm first_name" name="room_number">
                        </td>
                        <td>
                            <div class="input-group date date-picker margin-bottom-5" data-date-format="yyyy-mm-dd">
                                <input type="text" class="form-control form-filter input-sm date_from" name="date_from"
                                       placeholder="@lang("backend/Booking.view.listBooking.From")">
											<span class="input-group-btn">
											<button class="btn btn-sm default" type="button"><i
                                                        class="fa fa-calendar"></i></button>
											</span>
                            </div>
                            <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                                <input type="text" class="form-control form-filter input-sm date_to" name="date_to"
                                       placeholder="@lang("backend/Booking.view.listBooking.To")">
											<span class="input-group-btn">
											<button class="btn btn-sm default" type="button"><i
                                                        class="fa fa-calendar"></i></button>
											</span>
                            </div>
                        </td>
                        <td>
                            {!!Form::select  ('gender', [''=>trans("backend/Booking.view.listBooking.Select")]+trans("profile.view.user.genderItem"),null,['class'=>'btn form-control input-xsmall select2me','id'=>'gender','required'=>''])!!}
                        </td>
                        <td>
                            {!!Form::select  ('type', [''=>trans("backend/Booking.view.listBooking.Select")]+trans("backend/Room.view.listRoomRented.TypeList"),null,['class'=>'btn form-control input-xsmall select2me','id'=>'type','required'=>''])!!}
                        </td>
                        <td>
                            {!!Form::select  ('rent_status', trans("backend/Room.view.listRoomRented.RentStatusList"),null,['class'=>'btn form-control input-xsmall select2me','id'=>'rent_status','required'=>''])!!}
                        </td>
                        <td>
                            <div class="margin-bottom-5 search"
                                 data-url="{!!URL::route('backend.room.listRoomRentedInDashboard')!!}">
                                <button class="btn btn-sm yellow filter-submit margin-bottom btn-search"><i
                                            class="fa fa-search"></i> @lang("backend/Booking.view.listBooking.Search")
                                </button>
                                <span class="loading"
                                      style="background: url('{{ asset('/assets/img/loader.gif') }}') no-repeat;padding: 5px 20px 20px; display: none "></span>
                            </div>
                        </td>
                    </tr>
                    </thead>
                    <tbody id="result-data-table">
                    {!!$pageData!!}
                    </tbody>
                </table>

            </div>
        </div>

    </div>
</div>
