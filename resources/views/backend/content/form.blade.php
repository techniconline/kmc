@extends('backend.layouts.default')
<?php  $langDetails = app('activeLangDetails');?>

@section('styles-plugin')
    @if($langDetails['is_rtl'])
        <link href="{{ asset('/assets/theme/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5-rtl.css') }}"
              rel="stylesheet" type="text/css"/>
        <link href="{{ asset('/assets/theme/assets/global/plugins/jquery-tags-input/jquery.tagsinput-rtl.css') }}"
              rel="stylesheet" type="text/css"/>
    @else
        <link href="{{ asset('/assets/theme/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css') }}"
              rel="stylesheet" type="text/css"/>
        <link href="{{ asset('/assets/theme/assets/global/plugins/jquery-tags-input/jquery.tagsinput.css') }}"
              rel="stylesheet" type="text/css"/>
    @endif
    <link href="{{ asset('/assets/theme/assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css') }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/theme/assets/global/plugins/bootstrap-summernote/summernote.css') }}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('/assets/theme/assets/global/plugins/jquery-nestable/jquery.nestable.css') }}" rel="stylesheet"
          type="text/css"/>

    {!!Html::loadEditor()!!}
    {!!UploaderHelper::loadUploaderJquery()!!}

@endsection
@section('styles')

@endsection
@section('content')
    <a href="{!! URL::route($locale.'backend.'.$type.'.index') !!}" class="btn btn-lg red">
        <i class="fa fa-backward"></i>
        @lang("backend/Content.view.form.Back")
    </a>
    @if(isset($error))

        @foreach($error as $item)
            <div class="errors">{!!$item!!}</div>
        @endforeach

    @else
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXTRAS PORTLET-->
                <div class="portlet box blue-hoki">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cogs"></i>@lang("backend/Content.view.form.Create_New_".$type)
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">
                            </a>
                            <a href="#portlet-config" data-toggle="modal" class="config">
                            </a>
                            <a href="javascript:;" class="reload">
                            </a>
                            <a href="javascript:;" class="remove">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        @if(isset($content))
                            {!!Form::open(['route' => [$locale.'backend.'.$type.'.update',$content['id']],'method' => 'PUT','class'=>'form-horizontal form-bordered','files'=> true])!!}
                            {!!Form::hidden('content_id',$content['id'] or null,['id'=>'content-id'])!!}
                        @else
                            {!!Form::open(['route' => $locale.'backend.'.$type.'.store','class'=>'form-horizontal form-bordered','files'=> true])!!}
                        @endif
                        <div class="form-body">
                            <div class="form-group">
                                {!!Form::label('link_title',trans("backend/Content.view.form.Link_Title"),['class'=>'control-label col-md-2'])!!}
                                <div class="col-md-10">
                                    {!!Form::text('link_title',isset($content['link_title'])?$content['link_title']:null,array('class'=>'form-control'))!!}
                                    {!!$errors->first('link_title','<span class="error">:message</span>')!!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!!Form::label('image_content',trans("backend/Content.view.form.ImageContent"),['class'=>'control-label col-md-2'])!!}
                                <div class="col-md-4">
                                    {!! Form::file('image_content',['onchange'=>"readURL(this)"]) !!}
                                    {!!$errors->first('image_content','<span class="error">:message</span>')!!}
                                </div>
                                <div class="col-md-4">
                                    @if(isset($content) && \Illuminate\Support\Facades\File::exists(base_path().env('PUBLIC_PATH_URL', 'public').$content['image_content'].'/'.$content['id'].'-160x80.'.$content['extension']))
                                        {!! Html::image($content['image_content'].'/'.$content['id'].'-160x80.'.$content['extension']) !!}
                                    @else
                                        <img class="image_content col-md-12" src="#" alt="Image content"/>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                {!!Form::label('url',trans("backend/Content.view.form.URL"),['class'=>'control-label col-md-2'])!!}
                                <div class="col-md-10">
                                    {!!Form::text('url',isset($content['url'])?$content['url']:null,array('class'=>'form-control'))!!}
                                    {!!$errors->first('url','<span class="error">:message</span>')!!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!!Form::label('tags',trans("backend/Content.view.form.TAGS"),['class'=>'control-label col-md-2'])!!}
                                <div class="col-md-10">
                                    {!!Form::textarea('tags',isset($content['tags'])?$content['tags']:null,array('class'=>'form-control tags'))!!}
                                    {!!$errors->first('tags','<span class="error">:message</span>')!!}
                                </div>
                            </div>


                            <div class="form-group">
                                {!!Form::label('category_id',trans("backend/Product.view.create.Category"),['class'=>'control-label col-md-2'])!!}
                                <div class="col-md-10">

                                    {!! CategoryHelper::createBoxTreeForNestable($roots,$translatedName,0,'category',$locale
                                        , trans("backend/Product.view.create.Category"), false, true ,false,true,isset($categoriesContent)?$categoriesContent:[]) !!}

                                    {!!$errors->first('category_id','<span class="error">:message</span>')!!}
                                </div>
                            </div>


                            <div class="form-group">
                                {!!Form::label('body_title',trans("backend/Content.view.form.Body_Title"),['class'=>'control-label col-md-2'])!!}
                                <div class="col-md-10">
                                    {!!Form::text('body_title',isset($content['body_title'])?$content['body_title']:null,array('class'=>'form-control'))!!}
                                    {!!$errors->first('body_title','<span class="error">:message</span>')!!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="short_content"
                                       class="control-label col-md-2">@lang("backend/Content.view.form.ShortContent")</label>

                                <div class="col-md-10">
                                    <textarea id="short_content" name="short_content" class="wysihtml5 form-control"
                                              rows="3">{!! isset($content['short_content'])?$content['short_content']:null !!}</textarea>
                                    {!!$errors->first('short_content','<span class="error">:message</span>')!!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!!Form::label('body_content',trans("backend/Content.view.form.Body_Contents"),['class'=>'control-label col-md-2'])!!}
                                <div class="col-md-10">
                                    {!!Form::editor('body_content',isset($content['body_content'])?$content['body_content']:null,'')!!}
                                    {{--<textarea name="body_content" class="wysihtml5 form-control" rows="11">{!! isset($content['body_content'])?$content['body_content']:null !!}</textarea>--}}
                                    {!!$errors->first('body_content','<span class="error">:message</span>')!!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!!Form::label('browser_title',trans("backend/Content.view.form.Browser_Title"),['class'=>'control-label col-md-2'])!!}
                                <div class="col-md-10">
                                    {!! Form::text('browser_title',isset($content['browser_title'])?$content['browser_title']:null,array('class'=>'form-control')) !!}
                                    {!!$errors->first('browser_title','<span class="error">:message</span>')!!}
                                </div>
                            </div>
                            <div class="form-group">
                                @if($type == 'news')
                                    {!!Form::label('login',trans("backend/Content.view.form.Access"),['class'=>'control-label col-md-2'])!!}
                                    <div class="col-md-2">
                                        {!! Form::select('login',[0=>trans("backend/Content.view.form.Public"),1=>trans("backend/Content.view.form.Members")],isset($content['login'])?$content['login']:0,array('class'=>'form-control input-small')) !!}
                                        {!!$errors->first('login','<span class="error">:message</span>')!!}
                                    </div>

                                @else
                                    {!!Form::label('type',trans("backend/Content.view.form.Type"),['class'=>'control-label col-md-2'])!!}
                                    <div class="col-md-2">
                                        {!! Form::select('type',['blog'=>trans("backend/Content.view.form.blog")
                                        ,'static'=>trans("backend/Content.view.form.static")
                                        ,'weblog'=>trans("backend/Content.view.form.weblog")
                                        ,'index-list'=>trans("backend/Content.view.form.index-list")
                                        ],isset($content['type'])?$content['type']:'blog',array('class'=>'form-control input-small')) !!}
                                        {!!$errors->first('type','<span class="error">:message</span>')!!}
                                    </div>
                                @endif
                                {!!Form::label('sort',trans("backend/Content.view.form.Sort"),['class'=>'control-label col-md-2'])!!}
                                <div class="col-md-2">
                                    {!! Form::text('sort',isset($content['sort'])?$content['sort']:1,array('class'=>'form-control')) !!}
                                    {!!$errors->first('sort','<span class="error">:message</span>')!!}
                                </div>
                                {!!Form::label('sort',trans("backend/Content.view.form.Status"),['class'=>'control-label col-md-2'])!!}
                                <div class="col-md-2">
                                    {!! Form::select('status',['1'=>trans("backend/Content.view.form.Enable")
                                     ,'2'=>trans("backend/Content.view.form.Disable")
                                     ],isset($content['status'])?$content['status']:0,array('class'=>'form-control input-small')) !!}
                                    {!!$errors->first('status','<span class="error">:message</span>')!!}
                                </div>

                            </div>

                            @if(isset($content)&&$content)
                                <div class="form-group">
                                    {!!Form::label('browser_title',trans("backend/Product.view.create.FileUpload"),['class'=>'control-label col-md-2'])!!}
                                    <div class="col-md-10">

                                        <div class="file-uploader"
                                             id="form-uploader-{!!$content->id!!}"
                                             style="margin-bottom: 1px !important;">
                                             {!!
                                            UploaderHelper::renderUploaderJquery($idUploader='imageUploader-'.$content->id
                                            ,$idStartUploader='startUploadImage'.$content->id
                                            ,$idMsgUploader='msgUploaderImage'.$content->id
                                            ,$titleUpload=trans("backend/Product.view.create.Upload")
                                            ,$titleStartUpload=trans("backend/Product.view.create.Upload")
                                            ,trans("profile.view.index.UploadFilesTitle")
                                            ,$urlUploader=URL::route('uploadFile'),$objUploaderName='uploadObjImg'.$content->id
                                            ,$autoUpload='false',$subFolder='content_gallery'
                                            ,$type='images',$maxFileSize=20,$itemId=$content->id,$allowedTypes='png,gif,jpg,jpeg'
                                            ,$showDelete='true',$showDone='false',$fileName='myfile'
                                            ,$titleButtonDrag=trans("backend/Product.view.create.StartImageUpload"),''
                                            ,$titleSaveDesc=trans("product.view.info.titleSaveDesc")
                                            ,$controller='content',$controllerDoc='media'
                                            ,$maxCountFileUploaded=10
                                            ,$multiUpload='true'
                                            ,$useController=true
                                            ,$prefix='backend'
                                            )
                                            !!}
                                            <div id="msgUploaderImage{{$content->id}}"></div>
                                            <span class="input-group-addon" style="display: none"></span>
                                            <span style="top: 5px"></span>
                                        </div>


                                        @if(isset($images))
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <!-- BEGIN SAMPLE TABLE PORTLET-->
                                                    <div class="portlet">
                                                        <div class="portlet-title">
                                                            <div class="caption">
                                                                <i class="fa fa-bell-o"></i>@lang("backend/Product.view.create.ImageList")
                                                            </div>
                                                            <div class="tools">
                                                                <a href="javascript:;" class="collapse">
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="portlet-body">
                                                            <div class="table-scrollable">
                                                                <table data-url="{!! URL::route('backend.product.isDefault') !!}"
                                                                       class="table table-striped table-bordered table-advance table-hover">
                                                                    <thead>
                                                                    <tr>
                                                                        <th>
                                                                            <i class="fa fa-file-photo-o"></i> @lang("backend/Product.view.create.Title")
                                                                        </th>
                                                                        <th>
                                                                            <i class="fa fa-file-image-o "></i> @lang("backend/Product.view.create.Image")
                                                                        </th>
                                                                        <th>
                                                                            <i class="fa fa-times "></i> @lang("backend/Product.view.create.Delete")
                                                                        </th>
                                                                        <th>
                                                                            <i class="fa fa-edit "></i> @lang("backend/Product.view.create.IsDefault")
                                                                        </th>
                                                                        <th>
                                                                            <i class="fa fa-edit "></i> @lang("backend/Product.view.create.Sort")
                                                                        </th>
                                                                        <th>
                                                                            <i class="fa fa-edit "></i> @lang("backend/Product.view.create.ClassGallery")
                                                                        </th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    @foreach($images as $img)
                                                                        <tr>
                                                                            <td class="highlight">

                                                                                <div style="width: 100%">
                                                                                    <input class="form-control"
                                                                                           type="checkbox"
                                                                                           value="{!!$img->media_id!!}"
                                                                                           name="image[]">
                                                                                    <input class="form-control input-medium"
                                                                                           type="text"
                                                                                           value="{!!$img->title?$img->title:'No Title!'!!}"
                                                                                           name="image_title[{!!$img->media_id!!}]"
                                                                                           placeholder="type title...">
                                                                                </div>
                                                                            </td>
                                                                            <td class="hidden-xs">
                                                                                <img width="50" height="33"
                                                                                     src="{!!$img->image!!}" alt=""/>
                                                                            </td>
                                                                            <td>
                                                                                <input type="checkbox"
                                                                                       value="{!!$img->media_id!!}"
                                                                                       name="media_del[]">
                                                                                {{--<a href="#{!!$img->media_id!!}" class="btn default btn-xs red">--}}
                                                                                {{--<i class="fa fa-times"></i> Delete </a>--}}
                                                                            </td>
                                                                            <td>
                                                                                <input class="media-default"
                                                                                       type="radio"
                                                                                       value="{!!$img->media_id!!}"
                                                                                        {!! $img->is_default?'checked':'' !!}
                                                                                       name="image_default">
                                                                            </td>
                                                                            <td>
                                                                                <input class="item-image-sort form-control input-xsmall"
                                                                                       type="text"
                                                                                       name="sort[{!!$img->id!!}]"
                                                                                       value="{!!$img->sort?$img->sort:1!!}"/>
                                                                            </td>
                                                                            <td>
                                                                                <input class="item-image-class form-control input-small"
                                                                                       type="text"
                                                                                       name="class[{!!$img->id!!}]"
                                                                                       value="{!!$img->alias?$img->alias:''!!}"/>
                                                                            </td>
                                                                        </tr>
                                                                    @endforeach
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- END SAMPLE TABLE PORTLET-->
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            @endif

                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-6">
                                    {!!Form::select('lang', $listLang,app('getLocaleDefault'),['class'=>'btn form-control input-small select2me'])!!}
                                    <button type="submit" class="btn purple"><i
                                                class="fa fa-check"></i>{!! isset($content)? trans("backend/Content.view.form.Update") : trans("backend/Content.view.form.Create") ; !!}
                                    </button>
                                    <button type="button"
                                            class="btn default">@lang("backend/Content.view.form.Cancel")</button>
                                </div>
                            </div>
                        </div>

                        {!!Form::close()!!}

                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection

@section('scripts-plugin')
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js') }}"
            type="text/javascript"></script>
    {{--<script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-markdown/lib/markdown.js') }}" type="text/javascript"></script>--}}
    {{--<script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js') }}" type="text/javascript"></script>--}}
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-summernote/summernote.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/jquery-tags-input/jquery.tagsinput.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/jquery-nestable/jquery.nestable.js') }}"
            type="text/javascript"></script>
@endsection

@section('scripts')
    <script src="{{ asset('/assets/theme/assets/admin/pages/scripts/components-editors.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/js/backend/category.js') }}" type="text/javascript"></script>
@endsection

@section('scripts-inline')

    <script>
        jQuery(document).ready(function () {

            CategoriesList.init();

//            ComponentsEditors.init();
            ComponentsEditors.notePad('short_content', 150);
            $('.tags').tagsInput({
                width: 'auto',
                'onAddTag': function () {
                    //alert(1);
                }
            });
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('.image_content').attr('src', e.target.result)
                            .css("width", "150px")
                            .css("height", "70px");
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>

@endsection