@extends('backend.layouts.default')
@section('styles-plugin')

@endsection
@section('styles')

@endsection
@section('content')
    @if (Session::has('flash_notification.message'))
        <div class="alert alert-{{ Session::get('flash_notification.level') }}">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

            {{ Session::get('flash_notification.message') }}
        </div>
    @endif
    <a class="btn btn-lg blue" href="{!! URL::route($locale.'backend.'.$type.'.create') !!}">
        <i class="fa fa-file-o"></i>
        @lang("backend/Content.view.index.Create_New_".$type)
    </a>
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN SAMPLE TABLE PORTLET-->
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-bell-o"></i>@lang("backend/Content.view.index.List")
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse">
                        </a>
                        <a href="#portlet-config" data-toggle="modal" class="config">
                        </a>
                        <a href="javascript:;" class="reload">
                        </a>
                        <a href="javascript:;" class="remove">
                        </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-scrollable">
                        <table class="table table-striped table-bordered table-advance table-hover">
                            <thead>
                            <tr>
                                <th>
                                    @lang("backend/Content.view.index.Content_Title")
                                </th>
                                {{--<th class="hidden-xs">--}}
                                {{--<i class="fa fa-file-text"></i>@lang("backend/Content.view.index.Content")--}}
                                {{--</th>--}}
                                <th>
                                    @lang("backend/Content.view.index.Status")
                                </th>
                                {{--<th>--}}
                                    {{--@lang("backend/Content.view.index.Browser_Title")--}}
                                {{--</th>--}}

                                @if($type == 'news')
                                    <th>
                                        @lang("backend/Content.view.index.Access")
                                    </th>
                                @else
                                    <th>
                                        @lang("backend/Content.view.form.Type")
                                    </th>
                                @endif
                                <th>
                                    @lang("backend/Content.view.index.Author")
                                </th>
                                <th>@lang("backend/Content.view.index.Cat")</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($contents as $content)
                                <tr class="row-item">

                                    <td>

                                        <p>{!! $content->updated_at !!} -
                                        <a href="{!! URL::route($locale.'backend.'.$type.'.edit',$content->contents_id) !!}">
                                            {!! $content->body_title  or 'No Title' !!}
                                        </a></p>
                                        <p>
                                            <code>
                                                @lang("backend/Content.view.index.Param"): {!! $content->browser_title !!}
                                            </code>
                                        </p>

                                    </td>
                                    <td class="highlight">
                                        @if( $content->status == 1 )
                                            {{--{!! Form::open(['route'=> [$locale.'backend.'.$type.'.ch_status',$content->contents_id]]) !!}--}}
                                            {{--<button type="submit" class="btn default btn-xs red delete">--}}
                                                {{--<i class="fa fa-times"></i></a>--}}
                                            {{--</button>--}}
                                            {{--{!! Form::close() !!}--}}
                                            @elseif( $content->status == 0)
                                            <div class="fail">
                                                منشتر نشده
                                            </div>
                                            @endif


                                    </td>
                                    {{--<td class="hidden-xss">--}}
                                    {{--{!! $content->body_content or 'No Content' !!}--}}
                                    {{--</td>--}}
                                    {{--<td>--}}
                                        {{--{!! $content->browser_title or 'No Browser Title' !!}--}}
                                    {{--</td>--}}
                                    @if($type == 'news')
                                        <td>
                                            {!! ($content->login == 0) ? trans("backend/Content.view.form.Public") : trans("backend/Content.view.form.Members")  !!}
                                        </td>
                                    @else
                                        <td>
                                            {!!  trans("backend/Content.view.form.".$content->type)  !!}
                                        </td>
                                    @endif
                                    <td>
                                        {!! $content->lastname or '0' !!}
                                    </td>
                                    <td>
                                        {!! $content->lang_id or '-' !!}
                                    </td>
                                    <td>
                                        <a href="{!! URL::route($locale.'backend.'.$type.'.edit',$content->contents_id) !!}"
                                           class="btn default btn-xs purple">
                                            <i class="fa fa-edit"></i></a>
                                        {!! Form::open(['route'=> [$locale.'backend.'.$type.'.destroy',$content->contents_id], 'method' => 'delete']) !!}
                                        <button type="submit" class="btn default btn-xs red delete">
                                            <i class="fa fa-times"></i></a>
                                        </button>
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END SAMPLE TABLE PORTLET-->
        </div>
    </div>

@endsection
@section('scripts-plugin')

@endsection
@section('scripts')
    {{--<script src="{{ asset('/assets/js/backend/apartment.js') }}" type="text/javascript"></script>--}}

@endsection
@section('scripts-inline')
    {{--<script>--}}
    {{--jQuery(document).ready(function() {--}}

    {{--});--}}
    {{--</script>--}}
@endsection
