@extends('backend.layouts.default')

@section('styles-plugin')
    <link href="{{ asset('/assets/theme/assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css') }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/theme/assets/global/plugins/fullcalendar/fullcalendar.min.css') }}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('/assets/theme/assets/global/plugins/bootstrap-datepicker/css/datepicker.css') }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/theme/assets/global/plugins/bootstrap-colorpicker/css/colorpicker.css') }}"
          rel="stylesheet" type="text/css"/>
@endsection
@section('styles')
    <link href="{{ asset('/assets/theme/assets/admin/pages/css/tasks.css') }}" rel="stylesheet" type="text/css"/>
@endsection
@section('content')

    @include('backend.dashboard.partials.pageContent')

@endsection
@section('scripts-plugin')

    <script src="{{ asset('/assets/theme/assets/global/plugins/flot/jquery.flot.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/flot/jquery.flot.resize.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/flot/jquery.flot.categories.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/jquery.pulsate.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-daterangepicker/moment.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/fullcalendar/fullcalendar.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/jquery.sparkline.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-datepicker/js/locales/bootstrap-datepicker.fr.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js') }}"
            type="text/javascript"></script>
@endsection

@section('scripts')

    <script src="{{ asset('/assets/theme/assets/admin/pages/scripts/index.js') }}" type="text/javascript"></script>
    {{--<script src="{{ asset('/assets/theme/assets/admin/pages/scripts/tasks.js') }}" type="text/javascript"></script>--}}

    <script src="{{ asset('/assets/js/backend/dashboard.js') }}" type="text/javascript"></script>

@endsection

@section('scripts-inline')
    <script>
        jQuery(document).ready(function () {
            var initPickers = function () {
                //init date pickers
                $('.date-picker').datepicker({
                    rtl: Metronic.isRTL(),
                    autoclose: true
                });
            }
            initPickers();

            var reminders = {!! json_encode($reminders) !!};
            {{--@foreach($reminders as $reminder)--}}
            {{--reminders.push("{!! $reminder !!}");--}}
            {{--@endforeach--}}
            $('.date-picker').datepicker();
            $('.colorPicker').colorpicker();

            Index.initCalendar(reminders); // init index page's custom scripts
//            Index.initCharts(); // init index page's custom scripts
//            Index.initChat();
//            Index.initMiniCharts();
//            Tasks.initDashboardWidget();
        });
    </script>
@endsection