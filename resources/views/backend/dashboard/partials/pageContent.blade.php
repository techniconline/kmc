<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
                Widget settings form goes here
            </div>
            <div class="modal-footer">
                <button type="button" class="btn blue">Save changes</button>
                <button type="button" class="btn default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

<!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
    @lang("backend/dashboard.Dashboard")
    <small>@lang("backend/dashboard.reports & statistics")</small>
</h3>

<!-- END CALENDAR -->
<div class="clearfix">
</div>
<div class="row ">
    <div class="col-md-6 col-sm-6">
        <!-- BEGIN PORTLET-->
        <div class="portlet box blue-madison calendar">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-calendar"></i>@lang('backend/Calendar.view.index.Calendar')
                </div>
            </div>
            <div class="portlet-body light-grey">
                <div id="calendar"></div>
            </div>
            <div class="portlet-body light-grey">
                <a href="#addReminder" class=""
                   data-toggle="modal">+ @lang('backend/Calendar.view.index.Add_Reminder')</a>
            </div>
            <div class="portlet-body light-grey">
                <a href="#removeReminder" class=""
                   data-toggle="modal">- @lang('backend/Calendar.view.index.Remove_Reminder')</a>
            </div>
        </div>
        <!-- END PORTLET-->
    </div>
</div>
<div class="modal fade bs-modal-lg tab-pane active fontawesome-demo col-md-12" id="addReminder" tabindex="-1"
     role="dialog">
    {!! Form::open(['route' => $locale.'backend.calendar.store', 'method' => 'post']) !!}
    <div class="modal-dialog modal-lg">
        <div class="modal-content form">
            <div class="modal-header col-md-12">
                <h3 class="modal-title">@lang('backend/Calendar.view.index.Add_Reminder')</h3>
            </div>
            <div class="modal-body col-md-12">
                <div class="col-md-6">
                    <label class="control-label">@lang('backend/Calendar.view.form.Title')</label>
                    <input type="text" class="form-control" name="title">
                </div>
                <div class="col-md-6">
                    <label class="control-label">@lang('backend/Calendar.view.form.Color')</label>
                    <input type="text" class="form-control colorPicker" name="color">
                </div>

                <input type="checkbox" name="all_day" value="1" id="all_day">
                <label for="all_day">@lang('backend/Calendar.view.form.All_Day')</label>
            </div>
            <div class="modal-body col-md-12 form-body">
                <div class="col-md-6">
                    <div class="form-group">
                        <h4>@lang('backend/Calendar.view.form.Start')</h4>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">@lang('backend/Calendar.view.form.Date')</label>

                        <div class="col-md-9">
                            <div class="input-group date date-picker" data-date-format="yyyy-mm-dd"
                                 data-date-language="{!! app('activeLangDetails')['lang'] !!}">
                                <input type="text" class="form-control" name="start">
                                        <span class="input-group-btn">
                                            <button class="btn default" type="button"><i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">@lang('backend/Calendar.view.form.Time')</label>

                        <div class="col-md-9">
                            <input type="text" class="form-control" name="startTime">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <h4>@lang('backend/Calendar.view.form.End')</h4>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">@lang('backend/Calendar.view.form.Date')</label>

                        <div class="col-md-9">
                            <div class="input-group date date-picker" data-date-format="yyyy-mm-dd"
                                 data-date-language="{!! app('activeLangDetails')['lang'] !!}">
                                <input type="text" class="form-control" name="end">
                                        <span class="input-group-btn">
                                            <button class="btn default" type="button"><i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">@lang('backend/Calendar.view.form.Time')</label>

                        <div class="col-md-9">
                            <input type="text" class="form-control" name="endTime">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <h4 class="modal-btn">
                    <a href="" class="btn btn-default"
                       data-dismiss="modal">@lang('backend/Calendar.view.form.Cancel')</a>
                    <input type="submit" class="btn btn-success" value="@lang('backend/Calendar.view.form.Save')">
                </h4>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>

<div class="modal fade bs-modal-lg tab-pane active fontawesome-demo col-md-12" id="removeReminder" tabindex="-1"
     role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header col-md-12">
                <h3 class="modal-title">@lang('backend/Calendar.view.index.Remove_Reminder')</h3>
            </div>
            <div class="modal-body col-md-12">
                <div class="portlet-body">
                    <div class="table-scrollable">
                        <table class="table table-striped table-bordered table-advance table-hover">
                            <thead>
                            <tr>
                                <th>
                                    <i class="fa fa-info"></i>@lang("backend/Calendar.view.form.Title")
                                </th>
                                <th>
                                    <i class="fa fa-header"></i>@lang("backend/Calendar.view.form.Start")
                                </th>
                                <th>
                                    <i class="fa fa-header"></i>@lang("backend/Calendar.view.form.End")
                                </th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($reminders as $reminder)
                                <tr class="row-item" style="background-color: {!! $reminder['color'] !!}">
                                    <td>
                                        {!! $reminder['title'] !!}
                                    </td>
                                    <td>
                                        {!! $reminder['start'] !!}
                                    </td>
                                    <td>
                                        {!! $reminder['end'] !!}
                                    </td>
                                    <td>
                                        {!! Form::open(['route'=> [$locale.'backend.calendar.destroy',$reminder['id']], 'method' => 'delete']) !!}
                                        <button title="@lang("backend/Calendar.view.form.Delete")" type="submit"
                                                class="btn default btn-xs red delete">
                                            <i class="fa fa-times"></i></a>@lang("backend/Calendar.view.form.Delete")
                                        </button>
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <h4 class="modal-btn">
                    <a href="" class="btn btn-default" data-dismiss="modal">Ok</a>
                </h4>
            </div>
        </div>
    </div>
</div>