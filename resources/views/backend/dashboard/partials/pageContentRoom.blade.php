<div class="fc fc-ltr fc-unthemed">
    <div class="fc-toolbar">
        <div class="fc-left"><h2></h2></div>
        <div class="fc-right">
            {{--<div class="fc-button-group">--}}
            {{--<button class="fc-prev-button fc-button fc-state-default fc-corner-left" type="button"><span--}}
            {{--class="fc-icon fc-icon-left-single-arrow"></span></button>--}}
            {{--<button class="fc-next-button fc-button fc-state-default" type="button"><span--}}
            {{--class="fc-icon fc-icon-right-single-arrow"></span></button>--}}
            {{--</div>--}}
        </div>
        <div class="fc-center"></div>
        <div class="fc-clear"></div>
    </div>
    <div class="fc-view-container" style="">
        <div class="fc-view fc-month-view fc-basic-view" style="">
            <table>
                <thead>
                <tr>
                    <td class="fc-widget-header">
                        <div class="fc-row fc-widget-header">

                        </div>
                    </td>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>
                        <div class="fc-day-grid-container" style="">
                            <div class="fc-day-grid">
                                <div style="width: 100%; position: relative; padding: 5px" id="result-data-room-table">
                                    {!!$pageData!!}
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>