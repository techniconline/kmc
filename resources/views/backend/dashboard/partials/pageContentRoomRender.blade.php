@foreach($dataRoomRented as $item)
    <?php
    $remainDays = 9999999;
    if ($item->rent_status == 3) {
        $now = time(); // or your date as well
        $your_date = strtotime($item->to_date);
        $datediff = $your_date - $now;
        $remainDays = floor($datediff / (60 * 60 * 24));
    }
    $backGround = '#a9302a';
    if ($remainDays <= 60) {
        $backGround = 'orange';
    } elseif ($item->rent_status == 2 && $remainDays > 60) {
        $backGround = 'greenyellow';
    } elseif ($item->rent_status == 3 && $remainDays > 60) {
        $backGround = '#008000';
    }
    ?>
    <a class="atgRoomView item show-modal-room" rel="roomList" data-toggle="modal"
       href="#modal-room-{!!$item->id_room!!}" style="color: #eeeeee">
        <div style="padding:4px; height: 40px; width: 40px; float: left; border: 1px solid #999999; margin: 2px; background-color: {!!$backGround!!}; "
             title="{!!'to date:'.$item->to_date.' - '.$remainDays!!}">
            <b>{!!$item->room_number!!}</b>
            <span>
            @if($item->rent_status==2)
                    @lang("backend/Room.view.listRoomRented.Reserve")
                @elseif($item->rent_status==3)
                    @lang("backend/Room.view.listRoomRented.Rent")
                @endif
            </span>
        </div>
    </a>

    <!-- /.modal -->
    <div class="modal fade bs-modal-sm" id="modal-room-{!!$item->id_room!!}" tabindex="-1" role="dialog"
         aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="z-index: 999999 !important;">
                <div class="modal-header">
                    <p></p>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <div><h4>@lang("backend/Room.view.listRoomRented.Apartment"):{!!$item->apartment_title!!}</h4></div>

                    @if($item->rent_status>1)
                        <div data-user="{!!$item->user_id!!}"><b>@lang("backend/Room.view.listRoomRented.Username")
                                :</b>{!!$item->firstname.' '.$item->lastname!!}
                        </div>
                        <div><b>@lang("backend/Room.view.listRoomRented.Email"):</b> {!!$item->email!!}</div>
                        <div><b>@lang("backend/Room.view.listRoomRented.Username"):</b> {!!$item->apartment_title!!}
                        </div>

                        <div class="mobile" data-user="{!!$item->user_id!!}">
                            <b>@lang("backend/Booking.view.listBooking.Mobile"):</b> {!!$item->mobile!!}
                        </div>
                        <div class="register_date_user" data-user="{!!$item->user_id!!}">
                            <b>@lang("backend/User.view.index.RegDate"):</b> {!!$item->register_date_user!!}
                        </div>
                        <div>
                            <b>@lang("backend/Room.view.listRoomRented.BookingDate"):</b>
                            {!!$item->from_date!!} ~ {!!$item->to_date!!}
                        </div>
                        <div class="duration" data-user="{!!$item->user_id!!}">
                            <b>@lang("backend/Room.view.listRoomRented.Duration"):</b> {!!$item->register_date_user!!}
                        </div>
                        <div>
                            <b>@lang("backend/Room.view.listRoomRented.RentStatus"):</b>
                            @if($item->rent_status==2)
                                @lang("backend/Room.view.listRoomRented.Reserve")
                            @elseif($item->rent_status==3)
                                @lang("backend/Room.view.listRoomRented.Rent")
                            @endif
                        </div>
                        <div>
                            <a class="btn green"
                               href="{!!URL::route($locale.'backend.users.edit',['user_id'=>$item->user_id])!!}"
                               target="_blank">@lang("backend/Room.view.listRoomRented.Profile")</a>
                            <a class="btn red"
                               href="{!!URL::route('backend.users.showData',['user_id'=>$item->user_id])!!}"
                               target="_blank">@lang("backend/Room.view.listRoomRented.Booking")</a>
                        </div>
                    @endif
                </div>
                <div class="modal-body">
                    <div><h4>@lang("backend/Room.view.listRoomRented.ApartmentInfo"):</h4></div>
                    <div><b>@lang("backend/Room.view.listRoomRented.RoomNumber"):</b>{!!$item->room_number!!}</div>
                    <div><b>@lang("backend/Room.view.listRoomRented.Apartment"):</b>{!!$item->apartment_title!!}</div>
                    <div><b>@lang("backend/Room.view.listRoomRented.Price"):</b>{!!$item->price!!}</div>
                    <div><b>@lang("backend/Room.view.listRoomRented.Category"):</b>{!!$item->category_title!!}</div>
                    <div><b>@lang("backend/Room.view.listRoomRented.Area"):</b>{!!$item->area!!}~{!!$item->to_area!!}
                    </div>
                </div>
                <div class="modal-footer">
                    {{--<a class="btn default"  data-dismiss="modal"></a>--}}
                    {{--<a class="btn green" ></a>--}}
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

@endforeach

<div class="row" data-url="{!!URL::route('backend.home.listRoomRentedDashboard')!!}">
    <div class="col-md-5 col-sm-12" style="width: 100%">
        <div class="dataTables_info" id="infoData" role="status" aria-live="polite"
             style="vertical-align: middle">
            {!!$dataRoomRented->appends(Input::all())->render()!!}
            <span class="loading"
                  style="background: url('{{ asset('/assets/img/loader.gif') }}') no-repeat;padding: 5px 20px 20px; display: none "></span>
            {{--Showing--}}
            {{--{!!$dataRoomRented->firstItem()!!} to {!!$dataRoomRented->count()!!}--}}
            {{--of {!!$dataRoomRented->total()!!} entries--}}
        </div>
    </div>
</div>
