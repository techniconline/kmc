<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
                Widget settings form goes here
            </div>
            <div class="modal-footer">
                <button type="button" class="btn blue">Save changes</button>
                <button type="button" class="btn default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
<!-- BEGIN STYLE CUSTOMIZER -->
{{--<div class="theme-panel hidden-xs hidden-sm">--}}
{{--<div class="toggler"></div>--}}
{{--<div class="toggler-close"></div>--}}
{{--<div class="theme-options">--}}
{{--<div class="theme-option theme-colors clearfix">--}}
{{--<span>THEME COLOR </span>--}}
{{--<ul>--}}
{{--<li class="color-default current tooltips" data-style="default" data-container="body" data-original-title="Default">--}}
{{--</li>--}}
{{--<li class="color-darkblue tooltips" data-style="darkblue" data-container="body" data-original-title="Dark Blue">--}}
{{--</li>--}}
{{--<li class="color-blue tooltips" data-style="blue" data-container="body" data-original-title="Blue">--}}
{{--</li>--}}
{{--<li class="color-grey tooltips" data-style="grey" data-container="body" data-original-title="Grey">--}}
{{--</li>--}}
{{--<li class="color-light tooltips" data-style="light" data-container="body" data-original-title="Light">--}}
{{--</li>--}}
{{--<li class="color-light2 tooltips" data-style="light2" data-container="body" data-html="true" data-original-title="Light 2">--}}
{{--</li>--}}
{{--</ul>--}}
{{--</div>--}}
{{--<div class="theme-option">--}}
{{--<span>Theme Style </span>--}}
{{--<select class="layout-style-option form-control input-sm">--}}
{{--<option value="square" selected="selected">Square corners</option>--}}
{{--<option value="rounded">Rounded corners</option>--}}
{{--</select>--}}
{{--</div>--}}
{{--<div class="theme-option">--}}
{{--<span>Layout </span>--}}
{{--<select class="layout-option form-control input-sm">--}}
{{--<option value="fluid" selected="selected">Fluid</option>--}}
{{--<option value="boxed">Boxed</option>--}}
{{--</select>--}}
{{--</div>--}}
{{--<div class="theme-option">--}}
{{--<span>Header </span>--}}
{{--<select class="page-header-option form-control input-sm">--}}
{{--<option value="fixed" selected="selected">Fixed</option>--}}
{{--<option value="default">Default</option>--}}
{{--</select>--}}
{{--</div>--}}
{{--<div class="theme-option">--}}
{{--<span>Top Menu Dropdown</span>--}}
{{--<select class="page-header-top-dropdown-style-option form-control input-sm">--}}
{{--<option value="light" selected="selected">Light</option>--}}
{{--<option value="dark">Dark</option>--}}
{{--</select>--}}
{{--</div>--}}
{{--<div class="theme-option">--}}
{{--<span>Sidebar Mode</span>--}}
{{--<select class="sidebar-option form-control input-sm">--}}
{{--<option value="fixed">Fixed</option>--}}
{{--<option value="default" selected="selected">Default</option>--}}
{{--</select>--}}
{{--</div>--}}
{{--<div class="theme-option">--}}
{{--<span>Sidebar Menu </span>--}}
{{--<select class="sidebar-menu-option form-control input-sm">--}}
{{--<option value="accordion" selected="selected">Accordion</option>--}}
{{--<option value="hover">Hover</option>--}}
{{--</select>--}}
{{--</div>--}}
{{--<div class="theme-option">--}}
{{--<span>Sidebar Style </span>--}}
{{--<select class="sidebar-style-option form-control input-sm">--}}
{{--<option value="default" selected="selected">Default</option>--}}
{{--<option value="light">Light</option>--}}
{{--</select>--}}
{{--</div>--}}
{{--<div class="theme-option">--}}
{{--<span>Sidebar Position </span>--}}
{{--<select class="sidebar-pos-option form-control input-sm">--}}
{{--<option value="left" selected="selected">Left</option>--}}
{{--<option value="right">Right</option>--}}
{{--</select>--}}
{{--</div>--}}
{{--<div class="theme-option">--}}
{{--<span>--}}
{{--Footer </span>--}}
{{--<select class="page-footer-option form-control input-sm">--}}
{{--<option value="fixed">Fixed</option>--}}
{{--<option value="default" selected="selected">Default</option>--}}
{{--</select>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
<!-- END STYLE CUSTOMIZER -->
<!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
    @lang("backend/dashboard.Dashboard")
    <small>@lang("backend/dashboard.reports & statistics")</small>
</h3>
{{--<div class="page-bar">--}}
{{--<ul class="page-breadcrumb">--}}
{{--<li>--}}
{{--<i class="fa fa-home"></i>--}}
{{--<a href="index.html">Home</a>--}}
{{--<i class="fa fa-angle-right"></i>--}}
{{--</li>--}}
{{--<li>--}}
{{--<a href="#">Dashboard</a>--}}
{{--</li>--}}
{{--</ul>--}}
{{--<div class="page-toolbar">--}}
{{--<div id="dashboard-report-range" class="pull-right tooltips btn btn-fit-height grey-salt" data-placement="top" data-original-title="Change dashboard date range">--}}
{{--<i class="icon-calendar"></i>&nbsp; <span class="thin uppercase visible-lg-inline-block"></span>&nbsp; <i class="fa fa-angle-down"></i>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
<!-- END PAGE HEADER-->
<!-- BEGIN CALENDAR -->
<div class="row ">
    <div class="col-md-6 col-sm-6">
        <!-- BEGIN PORTLET-->
        <div class="portlet box blue-madison calendar">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-calendar"></i>@lang('backend/Calendar.view.index.Calendar')
                </div>
            </div>
            <div class="portlet-body light-grey">
                <div id="calendar"></div>
            </div>
            <div class="portlet-body light-grey">
                <a href="#addReminder" class=""
                   data-toggle="modal">+ @lang('backend/Calendar.view.index.Add_Reminder')</a>
            </div>
            <div class="portlet-body light-grey">
                <a href="#removeReminder" class=""
                   data-toggle="modal">- @lang('backend/Calendar.view.index.Remove_Reminder')</a>
            </div>
        </div>
        <!-- END PORTLET-->
    </div>

    <div class="col-md-6 col-sm-6">
        <!-- BEGIN PORTLET-->
        <div class="portlet solid bordered grey-cararra">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-bar-chart-o"></i>Site Visits
                </div>
                <div class="actions">
                    <div class="btn-group" data-toggle="buttons">
                        <label class="btn grey-steel btn-sm active">
                            <input type="radio" name="options" class="toggle" id="option1">New</label>
                        <label class="btn grey-steel btn-sm">
                            <input type="radio" name="options" class="toggle" id="option2">Returning</label>
                    </div>
                </div>
            </div>
            <div class="portlet-body">
                <div id="site_statistics_loading">
                    <img src="{{ asset('/assets/theme/assets/admin/layout/img/loading.gif') }}" alt="loading"/>
                </div>
                <div id="site_statistics_content" class="display-none">
                    <div id="site_statistics" class="chart">
                    </div>
                </div>
            </div>
        </div>
        <!-- END PORTLET-->
    </div>

</div>
<!-- END CALENDAR -->
<div class="clearfix">
</div>

<div class="modal fade bs-modal-lg tab-pane active fontawesome-demo col-md-12" id="addReminder" tabindex="-1"
     role="dialog">
    {!! Form::open(['route' => $locale.'backend.calendar.store', 'method' => 'post']) !!}
    <div class="modal-dialog modal-lg">
        <div class="modal-content form">
            <div class="modal-header col-md-12">
                <h3 class="modal-title">@lang('backend/Calendar.view.index.Add_Reminder')</h3>
            </div>
            <div class="modal-body col-md-12">
                <div class="col-md-6">
                    <label class="control-label">@lang('backend/Calendar.view.form.Title')</label>
                    <input type="text" class="form-control" name="title">
                </div>
                <div class="col-md-6">
                    <label class="control-label">@lang('backend/Calendar.view.form.Color')</label>
                    <input type="text" class="form-control colorPicker" name="color">
                </div>

                <input type="checkbox" name="all_day" value="1" id="all_day">
                <label for="all_day">@lang('backend/Calendar.view.form.All_Day')</label>
            </div>
            <div class="modal-body col-md-12 form-body">
                <div class="col-md-6">
                    <div class="form-group">
                        <h4>@lang('backend/Calendar.view.form.Start')</h4>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">@lang('backend/Calendar.view.form.Date')</label>

                        <div class="col-md-9">
                            <div class="input-group date date-picker" data-date-format="yyyy-mm-dd"
                                 data-date-language="{!! app('activeLangDetails')['lang'] !!}">
                                <input type="text" class="form-control" name="start">
                                        <span class="input-group-btn">
                                            <button class="btn default" type="button"><i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">@lang('backend/Calendar.view.form.Time')</label>

                        <div class="col-md-9">
                            <input type="text" class="form-control" name="startTime">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <h4>@lang('backend/Calendar.view.form.End')</h4>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">@lang('backend/Calendar.view.form.Date')</label>

                        <div class="col-md-9">
                            <div class="input-group date date-picker" data-date-format="yyyy-mm-dd"
                                 data-date-language="{!! app('activeLangDetails')['lang'] !!}">
                                <input type="text" class="form-control" name="end">
                                        <span class="input-group-btn">
                                            <button class="btn default" type="button"><i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">@lang('backend/Calendar.view.form.Time')</label>

                        <div class="col-md-9">
                            <input type="text" class="form-control" name="endTime">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <h4 class="modal-btn">
                    <a href="" class="btn btn-default"
                       data-dismiss="modal">@lang('backend/Calendar.view.form.Cancel')</a>
                    <input type="submit" class="btn btn-success" value="@lang('backend/Calendar.view.form.Save')">
                </h4>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>

<div class="modal fade bs-modal-lg tab-pane active fontawesome-demo col-md-12" id="removeReminder" tabindex="-1"
     role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header col-md-12">
                <h3 class="modal-title">@lang('backend/Calendar.view.index.Remove_Reminder')</h3>
            </div>
            <div class="modal-body col-md-12">
                <div class="portlet-body">
                    <div class="table-scrollable">
                        <table class="table table-striped table-bordered table-advance table-hover">
                            <thead>
                            <tr>
                                <th>
                                    <i class="fa fa-info"></i>@lang("backend/Calendar.view.form.Title")
                                </th>
                                <th>
                                    <i class="fa fa-header"></i>@lang("backend/Calendar.view.form.Start")
                                </th>
                                <th>
                                    <i class="fa fa-header"></i>@lang("backend/Calendar.view.form.End")
                                </th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($reminders as $reminder)
                                <tr class="row-item" style="background-color: {!! $reminder['color'] !!}">
                                    <td>
                                        {!! $reminder['title'] !!}
                                    </td>
                                    <td>
                                        {!! $reminder['start'] !!}
                                    </td>
                                    <td>
                                        {!! $reminder['end'] !!}
                                    </td>
                                    <td>
                                        {!! Form::open(['route'=> [$locale.'backend.calendar.destroy',$reminder['id']], 'method' => 'delete']) !!}
                                        <button title="@lang("backend/Calendar.view.form.Delete")" type="submit"
                                                class="btn default btn-xs red delete">
                                            <i class="fa fa-times"></i></a>@lang("backend/Calendar.view.form.Delete")
                                        </button>
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <h4 class="modal-btn">
                    <a href="" class="btn btn-default" data-dismiss="modal">Ok</a>
                </h4>
            </div>
        </div>
    </div>
</div>