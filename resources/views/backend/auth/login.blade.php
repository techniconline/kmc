<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title>{!!Config::get('app.webSiteName')!!} | Admin Dashboard | Login </title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta content="{!!Config::get('app.descriptionWebSite')!!}" name="description"/>
    <meta content="" name="author"/>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('/assets/theme/assets/global/plugins/font-awesome/css/font-awesome.min.css') }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/theme/assets/global/plugins/simple-line-icons/simple-line-icons.min.css') }}"
          rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('/assets/theme/assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('/assets/theme/assets/global/plugins/uniform/css/uniform.default.css') }}" rel="stylesheet"
          type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="{{ asset('/assets/theme/assets/global/plugins/select2/select2.css') }}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('/assets/theme/assets/admin/pages/css/login-soft.css') }}" rel="stylesheet" type="text/css"/>
    <!-- END PAGE LEVEL SCRIPTS -->
    <!-- BEGIN THEME STYLES -->
    <link href="{{ asset('/assets/theme/assets/global/css/components.css') }}" rel="stylesheet"
          id="style_components type=" text
    /css"/>
    <link href="{{ asset('/assets/theme/assets/global/css/plugins.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/theme/assets/admin/layout/css/layout.css') }}" rel="stylesheet" type="text/css"/>
    <link id="style_color" href="{{ asset('/assets/theme/assets/admin/layout/css/themes/darkblue.css') }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/theme/assets/admin/layout/css/custom.css') }}" rel="stylesheet" type="text/css"/>
    <!-- END THEME STYLES -->
    {!! ParamsHelper::favIcon() !!}
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="login">
<!-- BEGIN LOGO -->
<div class="logo">
    <a href="{!!url('backend')!!}" style="margin-top: 10px; color: #fff; font-size: 14pt;">
        @lang('backend/Cp.view.index.NameFa')
        {{--<img src="{{ asset('/assets/img/logo/logo-backend.png') }}" alt="logo"--}}
        {{--class="logo-default" style="margin-top: 9px"/>--}}
    </a>
</div>
<!-- END LOGO -->
<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
<div class="menu-toggler sidebar-toggler">
</div>
<!-- END SIDEBAR TOGGLER BUTTON -->
<!-- BEGIN LOGIN -->
<div class="content">
    <!-- BEGIN LOGIN FORM -->
    <form class="form-horizontal" role="form" method="POST"
          action="{{ url((app('locale') == '')? '/backend/auth/login' : '/'.app('locale').'/backend/auth/login') }}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <h3 class="form-title">Login to your account</h3>
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <button class="close" data-close="alert"></button>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        {{--<div class="alert alert-danger display-hide">--}}
        {{--<button class="close" data-close="alert"></button>--}}
        {{--<span>--}}
        {{--Enter any username and password. </span>--}}
        {{--</div>--}}
        <div class="form-group">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9">Username</label>

            <div class="input-icon">
                <i class="fa fa-user"></i>
                <input class="form-control placeholder-no-fix" type="email" autocomplete="off" placeholder="Username"
                       name="email" value="{{ old('email') }}"/>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Password</label>

            <div class="input-icon">
                <i class="fa fa-lock"></i>
                <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Password"
                       name="password"/>
            </div>
        </div>
        <div class="form-actions">
            {{--<label class="checkbox">--}}
            {{--<input type="checkbox" name="remember" value="1"/> Remember me </label>--}}
            {{--<input type="submit" class="btn blue pull-right" value="Login"/>--}}
            <button type="submit" class="btn blue pull-right">
                Login <i class="m-icon-swapright m-icon-white"></i>
            </button>
        </div>
        <div class="forget-password">
            <h4>Forgot your password ?</h4>

            <p>
                no worries, click <a href="javascript:;" id="forget-password">
                    here </a>
                to reset your password.
            </p>
        </div>
        {{--<div class="create-account">--}}
        {{--<p>--}}
        {{--Don't have an account yet ?&nbsp; <a href="javascript:;" id="register-btn">--}}
        {{--Create an account </a>--}}
        {{--</p>--}}
        {{--</div>--}}
    </form>
    <!-- END LOGIN FORM -->
    <!-- BEGIN FORGOT PASSWORD FORM -->
    <form class="forget-form" id="forget-form" method="post">
        <h3>Forget Password ?</h3>

        <p>
            Enter your e-mail address below to reset your password.
        </p>

        <div class="form-group">
            <div class="input-icon">
                <i class="fa fa-envelope"></i>
                <input data-action="/backend/password/mail" class="form-control placeholder-no-fix" type="text"
                       autocomplete="off" placeholder="Email" name="forgotEmail"/>
            </div>
        </div>
        <div class="form-actions">
            <button type="button" id="back-btn" class="btn">
                <i class="m-icon-swapleft"></i> Back
            </button>
            <button type="submit" class="btn blue pull-right">
                Submit <i class="m-icon-swapright m-icon-white"></i>
            </button>
        </div>
    </form>
    <!-- END FORGOT PASSWORD FORM -->
</div>
<!-- END LOGIN -->
<!-- BEGIN COPYRIGHT -->
<div class="copyright">
    2015 - {{date('Y')+1}} &copy; {!!Config::get('app.webSiteName')!!} by techniconline.com
</div>
<!-- END COPYRIGHT -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="{{ asset('/assets/theme/assets/global/plugins/respond.min.js') }}"></script>
<script src="{{ asset('/assets/theme/assets/global/plugins/excanvas.min.js') }}'"></script>
<![endif]-->
<script src="{{ asset('/assets/theme/assets/global/plugins/jquery.min.js') }}"></script>
<script src="{{ asset('/assets/theme/assets/global/plugins/jquery-migrate.min.js') }}"></script>
<script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/assets/theme/assets/global/plugins/jquery.blockui.min.js') }}"></script>
<script src="{{ asset('/assets/theme/assets/global/plugins/uniform/jquery.uniform.min.js') }}"></script>
<script src="{{ asset('/assets/theme/assets/global/plugins/jquery.cokie.min.js') }}"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{ asset('/assets/theme/assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('/assets/theme/assets/global/plugins/backstretch/jquery.backstretch.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/assets/theme/assets/global/plugins/select2/select2.min.js') }}"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{ asset('/assets/theme/assets/global/scripts/metronic.js') }}"></script>
<script src="{{ asset('/assets/theme/assets/admin/layout/scripts/layout.js') }}"></script>
<script src="{{ asset('/assets/theme/assets/admin/layout/scripts/demo.js') }}"></script>
<script src="{{ asset('/assets/theme/assets/admin/pages/scripts/login-soft.js') }}"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
    jQuery(document).ready(function () {
        Metronic.init(); // init metronic core components
        Layout.init(); // init current layout
        Login.init();
        Demo.init();
        // init background slide images
        $.backstretch([
                    "{{ asset('/assets/theme/assets/admin/pages/media/bg/1.jpg') }}",
                    "{{ asset('/assets/theme/assets/admin/pages/media/bg/3.jpg') }}"
                ], {
                    fade: 1000,
                    duration: 8000
                }
        );
    });
</script>

<script>
    $('#forget-form').on('submit', function (e) {
        var $this = $(this);
        console.log($this.find('input[name="forgotEmail"]').data('action'));
        $.ajax({
            method: 'POST',
            url: $this.find('input[name="forgotEmail"]').data('action'),
            data: {
                forgotEmail: $this.find('input[name="forgotEmail"]').val()
            },
            success: function (data) {
                if (data == true) {
                    alert('Sucessfully sent new password to your mail!');
                } else {
                    alert('this email does not exist');
                }
            }
        });
    });
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>