@extends('backend.layouts.default')
<?php  $langDetails = app('activeLangDetails');?>

@section('styles-plugin')

@endsection
@section('styles')

@endsection
@section('content')
    <a class="btn btn-lg blue" href="{!! URL::route($locale.'backend.featureGroup.create') !!}">
        <i class="fa fa-file-o"></i>
        @lang("backend/FeatureGroup.view.index.CreateNew")
    </a>
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN SAMPLE TABLE PORTLET-->
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-bell-o"></i>@lang("backend/FeatureGroup.view.index.List")
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse">
                        </a>
                        <a href="#portlet-config" data-toggle="modal" class="config">
                        </a>
                        <a href="javascript:;" class="reload">
                        </a>
                        <a href="javascript:;" class="remove">
                        </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-scrollable">
                        <table class="table table-striped table-bordered table-advance table-hover">
                            <thead>
                            <tr>
                                <th>
                                    <i class="fa "></i>@lang("backend/FeatureGroup.view.index.Name")
                                </th>
                                <th>
                                    <i class="fa "></i>@lang("backend/FeatureGroup.view.create.FeatureList")
                                </th>

                                <th>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $item)
                                <tr class="row-item">
                                    <td class="highlight">
                                        <div class="success">
                                        </div>
                                        <a href="{!! URL::route($locale.'backend.featureGroup.edit',['feature_group_id'=>$item->id]) !!}">
                                            {!! (($item->name)?$item->name:'No Name' ) !!}
                                        </a>
                                    </td>
                                    <td class="highlight">
                                        {!! (($item->list_features)?$item->list_features:'No Feature' ) !!}
                                    </td>
                                    <td>
                                        <a href="{!! URL::route($locale.'backend.featureGroup.edit',['feature_group_id'=>$item->id]) !!}"
                                           class="btn default btn-xs purple">
                                            <i class="fa fa-edit"></i> @lang("backend/FeatureGroup.view.index.Edit")
                                        </a>

                                        <a href="{!! URL::route($locale.'backend.featureGroup.destroy',['product_id'=>$item->id]) !!}"
                                           class="btn default btn-xs red delete">
                                            <i class="fa fa-times"></i> @lang("backend/FeatureGroup.view.index.Delete")
                                        </a>

                                    </td>
                                </tr>
                            @endforeach


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END SAMPLE TABLE PORTLET-->
        </div>
    </div>

@endsection
@section('scripts-plugin')
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootbox/bootbox.min.js') }}"
            type="text/javascript"></script>
@endsection
@section('scripts')
    <script src="{{ asset('/assets/js/backend/product.js') }}" type="text/javascript"></script>

@endsection
@section('scripts-inline')
    <script>
        jQuery(document).ready(function () {

        });
    </script>
@endsection
