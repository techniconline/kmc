@extends('backend.layouts.default')
<?php  $langDetails = app('activeLangDetails');?>

@section('styles-plugin')
    @if($langDetails['is_rtl'])

    @else

    @endif
@endsection
@section('styles')

@endsection
@section('content')
    <a href="{!! URL::route($locale.'backend.featureGroup.index') !!}" class="btn btn-lg red">
        <i class="fa fa-backward"></i>
        @lang("backend/FeatureGroup.view.create.Back")
    </a>


    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXTRAS PORTLET-->
            <div class="portlet box blue-hoki">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>{!! $title !!}
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse">
                        </a>
                        <a href="#portlet-config" data-toggle="modal" class="config">
                        </a>
                        <a href="javascript:;" class="reload">
                        </a>
                        <a href="javascript:;" class="remove">
                        </a>
                    </div>
                </div>
                <div class="portlet-body form">
                    @if(isset($data))
                        {!!Form::open(['route' => [$locale.'backend.featureGroup.update',$data['id']],'method' => 'PUT','class'=>'form-horizontal form-bordered'])!!}

                    @else
                        {!!Form::open(['route' => $locale.'backend.featureGroup.store','class'=>'form-horizontal form-bordered'])!!}
                    @endif
                    {{--<form class="form-horizontal form-bordered">--}}
                    <div class="form-body">
                        <div class="form-group">
                            {!!Form::label('name',trans("backend/FeatureGroup.view.create.Name"),['class'=>'control-label col-md-2'])!!}
                            <div class="col-md-10">
                                {!!Form::text('name',isset($data['name'])?$data['name']:null,array('class'=>'form-control'))!!}
                                {!!$errors->first('name','<span class="error">:message</span>')!!}
                            </div>
                        </div>
                        @if(isset($data) && isset($dataFeature))

                            <div class="row">
                                <div class="col-md-12">
                                    <!-- BEGIN PORTLET-->
                                    <div class="portlet box green-haze">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="fa fa-gift"></i>@lang("backend/FeatureGroup.view.create.FeatureList")
                                            </div>
                                        </div>
                                        <div class="portlet-body form">
                                            <!-- BEGIN FORM-->
                                            <div class="form-body">
                                                @if($dataFeature)

                                                    <?php $counter = 0; ?>
                                                    @foreach($dataFeature as $item)

                                                        @if(!$counter)
                                                            <div class="form-group">
                                                                @endif

                                                                <label class="control-label col-md-2">{!!$item->name!!}</label>

                                                                <div class="col-md-2">
                                                                    <input type="checkbox" name="features[]"
                                                                           @if(isset($featureGroup) && in_array($item->id,$featureGroup)) checked
                                                                           @endif
                                                                           value="{!!$item->id!!}" class="make-switch"
                                                                           data-size="small">
                                                                </div>

                                                                <?php $counter++; ?>
                                                                @if($counter==3)
                                                                    <?php $counter = 0; ?>
                                                            </div>
                                                        @endif
                                                    @endforeach

                                                @endif

                                            </div>
                                            <!-- END FORM-->
                                        </div>
                                    </div>
                                    <!-- END PORTLET-->
                                </div>
                            </div>

                        @endif

                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-6">
                                {!!Form::select('lang', $listLang,$setLang,['class'=>'btn form-control input-small select2me'])!!}
                                <button type="submit" class="btn purple"><i class="fa fa-check"></i>{!! $btnTitle !!}
                                </button>
                                <button type="button"
                                        class="btn default">@lang("backend/FeatureGroup.view.create.Cancel")</button>
                            </div>
                        </div>
                    </div>

                    {!!Form::close()!!}

                </div>
            </div>
        </div>
    </div>


@endsection
@section('scripts-plugin')

@endsection
@section('scripts')

@endsection
@section('scripts-inline')

    <script>
        jQuery(document).ready(function () {

        });
    </script>
@endsection