@extends('backend.layouts.default')
@section('styles-plugin')
    <link href="{{ asset('/assets/theme/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css') }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/theme/assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css') }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/theme/assets/global/plugins/bootstrap-summernote/summernote.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection
@section('styles')

@endsection
@section('content')
    @if(isset($error))
        @foreach($error as $item)
            <div class="errors">{!!$item!!}</div>
        @endforeach
    @else
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXTRAS PORTLET-->
                <div class="portlet box blue-hoki">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cogs"></i>@lang("backend/User.view.form.Edit_User")
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">
                            </a>
                            <a href="#portlet-config" data-toggle="modal" class="config">
                            </a>
                            <a href="javascript:;" class="reload">
                            </a>
                            <a href="javascript:;" class="remove">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        {!!Form::open(['route' => [$locale.'backend.profile.update',$employee['id']],'method' => 'PUT','class'=>'form-horizontal form-bordered','files'=> true])!!}
                        {!!Form::hidden('employee_id',$employee['id'] or null,['id'=>'employee-id'])!!}
                        <div class="form-body">
                            <div class="form-group">
                                {!!Form::label('avatar',trans("backend/User.view.form.Avatar"),['class'=>'control-label col-md-2'])!!}
                                <div class="col-md-10">
                                    @if(isset($employee) && \Illuminate\Support\Facades\File::exists(base_path().env('PUBLIC_PATH_URL', 'public').'/uploads/images/Employees/'.$employee->id.'-200x200.jpg'))
                                        {!! Html::image('uploads/images/Employees/'.$employee->id.'-200x200.jpg') !!}
                                    @endif
                                    {!! Form::file('avatar') !!}
                                    {{--                                        {!! link_to('delete-avatar')trans() !!}--}}
                                    {!!$errors->first('avatar','<span class="error">:message</span>')!!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!!Form::label('firstname',trans("backend/User.view.form.Name"),['class'=>'control-label col-md-2'])!!}
                                <div class="col-md-10">
                                    {!!Form::text('firstname',isset($employee['firstname'])?$employee['firstname']:null,array('class'=>'form-control'))!!}
                                    {!!$errors->first('firstname','<span class="error">:message</span>')!!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!!Form::label('lastname',trans("backend/User.view.form.Family"),['class'=>'control-label col-md-2'])!!}
                                <div class="col-md-10">
                                    {!!Form::text('lastname',isset($employee['lastname'])?$employee['lastname']:null,array('class'=>'form-control'))!!}
                                    {!!$errors->first('lastname','<span class="error">:message</span>')!!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!!Form::label('email',trans("backend/User.view.form.Email"),['class'=>'control-label col-md-2'])!!}
                                <div class="col-md-10">
                                    {!!Form::text('email',isset($employee['email'])?$employee['email']:null,array('class'=>'form-control'))!!}
                                    {!!$errors->first('email','<span class="error">:message</span>')!!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!!Form::label('currentPassword',trans("backend/User.view.form.Current_Password"),['class'=>'control-label col-md-2'])!!}
                                <div class="col-md-10">
                                    {!!Form::password('currentPassword',array('class'=>'form-control'))!!}
                                    {!!$errors->first('currentPassword','<span class="error">:message</span>')!!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!!Form::label('newPassword',trans("backend/User.view.form.New_Password"),['class'=>'control-label col-md-2'])!!}
                                <div class="col-md-10">
                                    {!!Form::password('newPassword',array('class'=>'form-control'))!!}
                                    {!!$errors->first('newPassword','<span class="error">:message</span>')!!}
                                </div>
                            </div>
                            {{--<div class="form-group">--}}
                            {{--{!!Form::label('passwordconfirm',trans("backend/User.view.form.Password_Confirm"),['class'=>'control-label col-md-2'])!!}--}}
                            {{--<div class="col-md-10">--}}
                            {{--{!!Form::password('passwordconfirm',array('class'=>'form-control'))!!}--}}
                            {{--{!!$errors->first('passwordconfirm','<span class="error">:message</span>')!!}--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            <div class="form-group">
                                {!!Form::label('gender',trans("backend/User.view.form.Gender"),['class'=>'control-label col-md-2'])!!}
                                <div class="col-md-10">
                                    {!!Form::select('gender',['male'=>'male','female'=>'female'],isset($employee['gender'])?$employee['gender']:null,array('class'=>'form-control'))!!}
                                    {!!$errors->first('gender','<span class="error">:message</span>')!!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!!Form::label('mobile',trans("backend/User.view.form.Mobile"),['class'=>'control-label col-md-2'])!!}
                                <div class="col-md-10">
                                    {!!Form::text('mobile',isset($employee['mobile'])?$employee['mobile']:null,array('class'=>'form-control'))!!}
                                    {!!$errors->first('mobile','<span class="error">:message</span>')!!}
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-6">
                                    <button type="submit" class="btn purple"><i
                                                class="fa fa-check"></i>{!! isset($employee)? trans("backend/User.view.form.Update") : trans("backend/User.view.form.Create") ; !!}
                                    </button>
                                    <button type="button"
                                            class="btn default">@lang("backend/User.view.form.Cancel")</button>
                                </div>
                            </div>
                        </div>

                        {!!Form::close()!!}

                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection

@section('scripts-plugin')
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-markdown/lib/markdown.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-summernote/summernote.min.js') }}"
            type="text/javascript"></script>
@endsection

@section('scripts')

    <script src="{{ asset('/assets/theme/assets/admin/pages/scripts/components-editors.js') }}"
            type="text/javascript"></script>
@endsection

@section('scripts-inline')

    <script>
        jQuery(document).ready(function () {

            ComponentsEditors.init();
        });

        $('.resetPass').on('click', function (e) {
            if (!confirm("{!! trans("backend/User.view.form.Are_You_Sure?") !!}")) {
                e.preventDefault();
            }
        });
    </script>
@endsection