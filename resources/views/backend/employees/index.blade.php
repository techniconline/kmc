@extends('backend.layouts.default')
@section('styles-plugin')

@endsection
@section('styles')

@endsection
@section('content')
    <a class="btn btn-lg blue" href="{!! URL::route($locale.'backend.employees.create') !!}">
        <i class="fa fa-file-o"></i>
        @lang("backend/Employee.view.index.Create_New_Employee")
    </a>
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN SAMPLE TABLE PORTLET-->
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-bell-o"></i>@lang("backend/Employee.view.index.List")
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse">
                        </a>
                        <a href="#portlet-config" data-toggle="modal" class="config">
                        </a>
                        <a href="javascript:;" class="reload">
                        </a>
                        <a href="javascript:;" class="remove">
                        </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-scrollable">
                        <table class="table table-striped table-bordered table-advance table-hover">
                            <thead>
                            <tr>
                                <th>
                                    <i class="fa fa-user"></i>
                                </th>
                                <th>
                                    <i class="fa fa-user"></i> @lang("backend/Employee.view.index.Name")
                                    / @lang("backend/Employee.view.index.Family")
                                </th>
                                <th>
                                    <i class="fa fa-envelope"></i> @lang("backend/Employee.view.index.Email")
                                </th>
                                <th class="hidden-xs">
                                    <i class="fa fa-mobile"></i> @lang("backend/Employee.view.index.Mobile")
                                </th>
                                <th>
                                    <i class="fa fa-circle"></i> @lang("backend/Employee.view.index.Status")
                                </th>
                                {{--<th></th>--}}
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($employees as $employee)
                                <tr class="row-item">
                                    <td>
                                        <a href="{!! URL::route($locale.'backend.employees.edit',$employee->id) !!}">
                                            @if(\Illuminate\Support\Facades\File::exists(base_path().env('PUBLIC_PATH_URL', 'public').'/uploads/images/Employees/'.$employee->id.'-200x200.jpg'))
                                                {!! Html::image('uploads/images/Employees/'.$employee->id.'-200x200.jpg',$employee->lastname,['width'=>'30']) !!}
                                            @else
                                                {!! Html::image('assets/theme/assets/admin/layout/img/avatar.png','avatar',['width'=>'30']) !!}
                                            @endif
                                        </a>
                                    </td>
                                    <td>
                                        <a href="{!! URL::route($locale.'backend.employees.edit',$employee->id) !!}">
                                            {!! $employee->firstname .' '. $employee->lastname !!}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="{!! URL::route($locale.'backend.employees.edit',$employee->id) !!}">
                                            {!! $employee->email  or 'No Title' !!}
                                        </a>
                                    </td>
                                    <td class="hidden-xss">
                                        {!! $employee->mobile or 'No Mobile' !!}
                                    </td>
                                    <td>
                                        {!! ( $employee->status == 1) ? trans("backend/Employee.view.index.Active") : trans("backend/Employee.view.index.Inactive") !!}
                                    </td>
                                    {{--<td>--}}
                                    {{--<a title="edit" href="{!! URL::route($locale.'backend.employees.edit',$employee->id) !!}"  class="btn default btn-xs purple">--}}
                                    {{--<i class="fa fa-edit"></i>--}}
                                    {{--</a>--}}
                                    {{--<a title="delete" href="{!! URL::route($locale.'backend.employees.destroy',$employee->id) !!}" class="btn default btn-xs red delete">--}}
                                    {{--<i class="fa fa-times"></i>--}}
                                    {{--</a>--}}
                                    {{--</td>--}}
                                </tr>
                            @endforeach


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END SAMPLE TABLE PORTLET-->
        </div>
    </div>

@endsection
@section('scripts-plugin')

@endsection
@section('scripts')

@endsection
@section('scripts-inline')
    <script>
        jQuery(document).ready(function () {

        });
    </script>
@endsection
