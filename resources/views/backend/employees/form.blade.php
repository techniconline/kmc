@extends('backend.layouts.default')
@section('styles-plugin')
    <link href="{{ asset('/assets/theme/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css') }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/theme/assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css') }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/theme/assets/global/plugins/bootstrap-summernote/summernote.css') }}" rel="stylesheet"
          type="text/css"/>

    {!!Html::loadEditor()!!}

@endsection
@section('styles')

@endsection
@section('content')
    <a href="{!! URL::route($locale.'backend.employees.index') !!}" class="btn btn-lg red">
        <i class="fa fa-backward"></i>
        @lang("backend/User.view.form.Back")
    </a>
    @if(isset($error))
        @foreach($error as $item)
            <div class="errors">{!!$item!!}</div>
        @endforeach
    @else
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXTRAS PORTLET-->
                <div class="portlet box blue-hoki">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cogs"></i>@lang("backend/User.view.form.Edit_User")
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">
                            </a>
                            <a href="#portlet-config" data-toggle="modal" class="config">
                            </a>
                            <a href="javascript:;" class="reload">
                            </a>
                            <a href="javascript:;" class="remove">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        @if(isset($employee))
                            {!!Form::open(['route' => [$locale.'backend.employees.update',$employee['id']],'method' => 'PUT','class'=>'form-horizontal form-bordered','files'=> true])!!}
                            {!!Form::hidden('employee_id',$employee['id'] or null,['id'=>'employee-id'])!!}
                        @else
                            {!!Form::open(['route' => $locale.'backend.employees.store','class'=>'form-horizontal form-bordered','files'=> true])!!}
                        @endif
                        <div class="form-body">
                            <div class="form-group">
                                {!!Form::label('avatar',trans("backend/User.view.form.Avatar"),['class'=>'control-label col-md-2'])!!}
                                <div class="col-md-10">
                                    @if(isset($employee) && \Illuminate\Support\Facades\File::exists(base_path().env('PUBLIC_PATH_URL', 'public').'/uploads/images/Employees/'.$employee->id.'-200x200.jpg'))
                                        {!! Html::image('uploads/images/Employees/'.$employee->id.'-200x200.jpg') !!}
                                    @endif
                                    {!! Form::file('avatar') !!}
                                    {{--                                        {!! link_to('delete-avatar')trans() !!}--}}
                                    {!!$errors->first('avatar','<span class="error">:message</span>')!!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!!Form::label('status',trans("backend/User.view.form.Status"),['class'=>'control-label col-md-2'])!!}
                                <div class="col-md-10">
                                    {!! Form::select('status',[1=>'active',0=>'inactive'],isset($employee['status'])?$employee['status']:null,array('class'=>'form-control input-small')) !!}
                                    {!!$errors->first('status','<span class="error">:message</span>')!!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!!Form::label('firstname',trans("backend/User.view.form.Name"),['class'=>'control-label col-md-2'])!!}
                                <div class="col-md-10">
                                    {!!Form::text('firstname',isset($employee['firstname'])?$employee['firstname']:null,array('class'=>'form-control'))!!}
                                    {!!$errors->first('firstname','<span class="error">:message</span>')!!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!!Form::label('lastname',trans("backend/User.view.form.Family"),['class'=>'control-label col-md-2'])!!}
                                <div class="col-md-10">
                                    {!!Form::text('lastname',isset($employee['lastname'])?$employee['lastname']:null,array('class'=>'form-control'))!!}
                                    {!!$errors->first('lastname','<span class="error">:message</span>')!!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!!Form::label('email',trans("backend/User.view.form.Email"),['class'=>'control-label col-md-2'])!!}
                                <div class="col-md-10">
                                    {!!Form::text('email',isset($employee['email'])?$employee['email']:null,array('class'=>'form-control'))!!}
                                    {!!$errors->first('email','<span class="error">:message</span>')!!}
                                </div>
                            </div>
                            @if(isset($employee))
                                <div class="form-group">
                                    {!!Form::label('password',trans("backend/User.view.form.Password"),['class'=>'control-label col-md-2'])!!}
                                    <div class="col-md-10">
                                        {!! link_to('backend/password/reset/'.$employee->id, trans("backend/User.view.form.Reset_Password"),['class' => 'resetPass']) !!}
                                    </div>
                                </div>
                            @else
                                <div class="form-group">
                                    {!!Form::label('password',trans("backend/User.view.form.Password"),['class'=>'control-label col-md-2'])!!}
                                    <div class="col-md-10">
                                        {!!Form::password('password',array('class'=>'form-control'))!!}
                                        {!!$errors->first('password','<span class="error">:message</span>')!!}
                                    </div>
                                </div>
                                {{--<div class="form-group">--}}
                                {{--{!!Form::label('passwordconfirm',trans("backend/User.view.form.Password_Confirm"),['class'=>'control-label col-md-2'])!!}--}}
                                {{--<div class="col-md-10">--}}
                                {{--{!!Form::password('passwordconfirm',array('class'=>'form-control'))!!}--}}
                                {{--{!!$errors->first('passwordconfirm','<span class="error">:message</span>')!!}--}}
                                {{--</div>--}}
                                {{--</div>--}}
                            @endif
                            <div class="form-group">
                                {!!Form::label('gender',trans("backend/User.view.form.Gender"),['class'=>'control-label col-md-2'])!!}
                                <div class="col-md-10">
                                    {!!Form::select('gender',['male'=>trans("backend/User.view.form.Male"),'female'=>trans("backend/User.view.form.Female")],isset($employee['gender'])?$employee['gender']:null
                                    ,array('class'=>'form-control  input-small'))!!}
                                    {!!$errors->first('gender','<span class="error">:message</span>')!!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!!Form::label('type_employee',trans("backend/User.view.form.TypeEmployee"),['class'=>'control-label col-md-2'])!!}
                                <div class="col-md-10">
                                    {!!Form::select('type_employee',['employee'=>trans("backend/User.view.form.Employee")
                                    ,'teacher'=>trans("backend/User.view.form.Teacher")]
                                    ,isset($employee['type_employee'])?$employee['type_employee']:null,array('class'=>'form-control input-small'))!!}
                                    {!!$errors->first('type_employee','<span class="error">:message</span>')!!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!!Form::label('mobile',trans("backend/User.view.form.Mobile"),['class'=>'control-label col-md-2'])!!}
                                <div class="col-md-10">
                                    {!!Form::text('mobile',isset($employee['mobile'])?$employee['mobile']:null,array('class'=>'form-control'))!!}
                                    {!!$errors->first('mobile','<span class="error">:message</span>')!!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!!Form::label('short_description',trans("backend/Employee.view.form.Short_Description"),['class'=>'control-label col-md-2'])!!}
                                <div class="col-md-10">
                                    {!!Form::editor('short_description',isset($employee['short_description'])?$employee['short_description']:null,'')!!}
                                    {!!$errors->first('short_description','<span class="error">:message</span>')!!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!!Form::label('description',trans("backend/Employee.view.form.Description"),['class'=>'control-label col-md-2'])!!}
                                <div class="col-md-10">
                                    {!!Form::editor('description',isset($employee['description'])?$employee['description']:null,'')!!}
                                    {!!$errors->first('description','<span class="error">:message</span>')!!}
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-6">
                                    <button type="submit" class="btn purple"><i
                                                class="fa fa-check"></i>{!! isset($employee)? trans("backend/User.view.form.Update") : trans("backend/User.view.form.Create") ; !!}
                                    </button>
                                    <button type="button"
                                            class="btn default">@lang("backend/User.view.form.Cancel")</button>
                                </div>
                            </div>
                        </div>

                        {!!Form::close()!!}

                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection

@section('scripts-plugin')
    {{--    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js') }}" type="text/javascript"></script>--}}
    {{--    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js') }}" type="text/javascript"></script>--}}
    {{--<script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-markdown/lib/markdown.js') }}" type="text/javascript"></script>--}}
    {{--<script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js') }}" type="text/javascript"></script>--}}
    {{--<script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-summernote/summernote.min.js') }}" type="text/javascript"></script>--}}
@endsection

@section('scripts')

@endsection

@section('scripts-inline')

    <script>
        jQuery(document).ready(function () {
//            ComponentsEditors.init();
        });

        $('.resetPass').on('click', function (e) {
            if (!confirm("{!! trans("backend/User.view.form.Are_You_Sure?") !!}")) {
                e.preventDefault();
            }
        });
    </script>
@endsection