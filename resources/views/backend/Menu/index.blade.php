@extends('backend.layouts.default')
@section('styles-plugin')
@endsection
@section('styles')
    <link href="{{ asset('/assets/theme/assets/global/plugins/jquery-nestable/jquery.nestable.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection
@section('content')
    {!! CategoryHelper::createBoxTreeForNestable($roots,$translatedName,0,'menu',$locale, trans("backend/Menu.view.index.Menus_List"), true, false,false) !!}
@endsection
@section('scripts-plugin')
    <script src="{{ asset('/assets/theme/assets/global/plugins/jquery-nestable/jquery.nestable.js') }}"
            type="text/javascript"></script>
@endsection
@section('scripts')
    <script src="{{ asset('/assets/theme/assets/admin/pages/scripts/ui-nestable.js') }}"
            type="text/javascript"></script>

@endsection
@section('scripts-inline')
    <script>
        jQuery(document).ready(function () {

            UINestable.init();
        });
    </script>
@endsection