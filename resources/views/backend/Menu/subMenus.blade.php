@extends('backend.layouts.default')
@section('styles-plugin')

@endsection
@section('styles')
    <link href="{!! asset('/assets/theme/assets/global/plugins/jquery-nestable/jquery.nestable.css') !!}"
          rel="stylesheet" type="text/css"/>
@endsection

@section('content')
    {!! CategoryHelper::createBoxTreeForNestable($subMenus,$translatedName,$myRoot->root_id,'menu',$locale) !!}

    {!! link_to_route($locale.'backend.menu.index', trans("backend/Menu.view.subMenus.Back"))  !!}

@endsection
@section('scripts-plugin')

    <script src="{!! asset('/assets/theme/assets/global/plugins/jquery-nestable/jquery.nestable.js') !!}"
            type="text/javascript"></script>

@endsection
@section('scripts')

    <script src="{!! asset('/assets/theme/assets/admin/pages/scripts/ui-nestable.js') !!}"
            type="text/javascript"></script>

    <script src="{!! asset('/assets/js/backend/menu.js') !!}" type="text/javascript"></script>

@endsection
@section('scripts-inline')
    <script>
        jQuery(document).ready(function () {

        });
    </script>
@endsection
