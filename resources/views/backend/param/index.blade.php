@extends('backend.layouts.default')
@section('styles-plugin')

@endsection
@section('styles')

@endsection
@section('content')
    @if (Session::has('flash_notification.message'))
        <div class="alert alert-{{ Session::get('flash_notification.level') }}">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{ Session::get('flash_notification.message') }}
        </div>
    @endif
    <a class="btn btn-lg blue" href="{!! URL::route($locale.'backend.params.create') !!}">
        <i class="fa fa-file-o"></i>
        @lang("backend/Param.view.index.Create_New_Parameter")
    </a>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-bell-o"></i>@lang("backend/Param.view.index.List")
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse">
                        </a>
                        <a href="javascript:;" class="remove">
                        </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-scrollable">
                        <table class="table table-striped table-bordered table-advance table-hover">
                            <thead>
                            <tr>
                                <th>
                                    <i class="fa fa-info"></i>@lang("backend/Param.view.index.Type")
                                </th>
                                <th>
                                    <i class="fa fa-header"></i>@lang("backend/Param.view.index.Name")
                                </th>
                                <th>
                                    <i class="fa fa-header"></i>@lang("backend/Param.view.index.Alias")
                                </th>
                                <th>
                                    <i class="fa fa-file-text"></i>@lang("backend/Param.view.index.Value")
                                </th>
                                <th>
                                    <i class="fa fa-globe"></i>@lang("backend/Param.view.index.To_Value")
                                </th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($params as $param)
                                <tr class="row-item">
                                    <td>
                                        {!! $param->type !!}
                                    </td>
                                    <td>
                                        @if($param->type != 'image')
                                            <a href="{!! URL::route($locale.'backend.params.edit',$param->id) !!}">
                                                {!! $param->name !!}
                                            </a>
                                        @else
                                            {!! $param->name !!}
                                        @endif
                                    </td>
                                    <td class="hidden-xss">
                                        {!! $param->alias !!}
                                    </td>
                                    <td>
                                        {!! $param->value !!}
                                    </td>
                                    <td>
                                        {!! $param->to_value or '---' !!}
                                    </td>
                                    <td>
                                        @if($param->type != 'image')
                                            <a title="@lang("backend/Param.view.index.Edit")"
                                               href="{!! URL::route($locale.'backend.params.edit',$param->id) !!}"
                                               class="btn default btn-xs purple" style="float: left;">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                        @endif
                                        {!! Form::open(['route'=> [$locale.'backend.params.destroy',$param->id], 'method' => 'delete']) !!}
                                        <button title="@lang("backend/Param.view.index.Delete")" type="submit"
                                                class="btn default btn-xs red delete">
                                            <i class="fa fa-times"></i></a>
                                        </button>
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('scripts-plugin')

@endsection
@section('scripts')

    <script src="{{ asset('/assets/js/backend/apartment.js') }}" type="text/javascript"></script>

@endsection
@section('scripts-inline')
    <script>
        jQuery(document).ready(function () {

        });
    </script>
@endsection
