@extends('backend.layouts.default')
@section('styles-plugin')
    <link href="{{ asset('/assets/theme/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css') }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/theme/assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css') }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/theme/assets/global/plugins/bootstrap-summernote/summernote.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection
@section('styles')

@endsection
@section('content')

    @if (Session::has('flash_notification.message'))
        <div class="alert alert-{{ Session::get('flash_notification.level') }}">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{ Session::get('flash_notification.message') }}
        </div>
    @endif

    <a href="{!! URL::route($locale.'backend.params.index') !!}" class="btn btn-lg red">
        <i class="fa fa-backward"></i>
        @lang("backend/Param.view.form.Back")
    </a>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue-hoki">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>@lang("backend/Param.view.form.Create_New_Param")
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                        <a href="javascript:;" class="remove"></a>
                    </div>
                </div>
                <div class="portlet-body form">
                    @if(isset($param))
                        {!!Form::model($param,['route' => [$locale.'backend.params.update',$param['id']],'method' => 'PUT','class'=>'form-horizontal form-bordered', 'files'=>'true'])!!}
                    @else
                        {!!Form::open(['route' => $locale.'backend.params.store','class'=>'form-horizontal form-bordered', 'files'=>'true'])!!}
                    @endif
                    <div class="form-body">
                        <div class="form-group">
                            {!!Form::label('type',trans("backend/Param.view.form.Type"),['class'=>'control-label col-md-2'])!!}
                            <div class="col-md-10">
                                {!! Form::select('type',$types,isset($param['type'])?$param['type']:1,array('class'=>'form-control type')) !!}
                                {!!$errors->first('type','<span class="error">:message</span>')!!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!!Form::label('name',trans("backend/Param.view.form.Name"),['class'=>'control-label col-md-2'])!!}
                            <div class="col-md-10">
                                {!!Form::text('name',NULL,array('class'=>'form-control'))!!}
                                {!!$errors->first('name','<span class="error">:message</span>')!!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!!Form::label('alias',trans("backend/Param.view.form.Alias"),['class'=>'control-label col-md-2'])!!}
                            <div class="col-md-10">
                                {!!Form::text('alias',NULL,array('class'=>'form-control'))!!}
                                {!!$errors->first('alias','<span class="error">:message</span>')!!}
                            </div>
                        </div>
                        <div class="form-group img" style="display: none;">
                            {!!Form::label('img',trans("backend/Param.view.form.Image"),['class'=>'control-label col-md-2'])!!}
                            <div class="col-md-5">
                                {!! Form::file('img',['onchange'=>"readURL(this)"]) !!}
                                {!!$errors->first('img','<span class="error">:message</span>')!!}
                            </div>
                            {{--<div class="col-md-5">--}}
                            {{--<img id="blah" src="#" alt="your image" class="col-md-12"/>--}}
                            {{--</div>--}}
                            <div class="col-md-5">
                                @if(isset($param) && $param->type == 'image')
                                    @if(\Illuminate\Support\Facades\File::exists(public_path().'/uploads/images/Params/'.$param->value.'.jpg'))
                                        {!! Html::image('uploads/images/Params/'.$param->value.'.jpg',$param->name,['class'=>'col-md-12']) !!}
                                    @endif
                                @else
                                    <img id="blah" src="#" alt="your image" class="col-md-12"/>
                                @endif
                            </div>
                        </div>
                        <div class="form-group value">
                            {!!Form::label('value',trans("backend/Param.view.form.Value"),['class'=>'control-label col-md-2'])!!}
                            <div class="col-md-10">
                                {!!Form::textarea('value',NULL,array('class'=>'form-control'))!!}
                                {!!$errors->first('value','<span class="error">:message</span>')!!}
                            </div>
                        </div>
                        <div class="form-group to_value">
                            {!!Form::label('to_value',trans("backend/Param.view.form.To_Value"),['class'=>'control-label col-md-2'])!!}
                            <div class="col-md-10">
                                {!!Form::textarea('to_value',NULL,array('class'=>'form-control'))!!}
                                {!!$errors->first('to_value','<span class="error">:message</span>')!!}
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-6">
                                <button type="submit" class="btn purple"><i
                                            class="fa fa-check"></i>{!! isset($param)? trans("backend/Param.view.form.Update") : trans("backend/Param.view.form.Create") ; !!}
                                </button>
                                <button type="button"
                                        class="btn default">@lang("backend/Param.view.form.Cancel")</button>
                            </div>
                        </div>
                    </div>
                    {!!Form::close()!!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts-plugin')
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-markdown/lib/markdown.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-summernote/summernote.min.js') }}"
            type="text/javascript"></script>
@endsection

@section('scripts')
    <script src="{{ asset('/assets/theme/assets/admin/pages/scripts/components-editors.js') }}"
            type="text/javascript"></script>
@endsection

@section('scripts-inline')

    <script>
        jQuery(document).ready(function () {
            ComponentsEditors.init();
        });

        if ($('.type').val() == 'image') {
            $('.img').slideDown();
            $('.value, .to_value').slideUp();
            $('.value textarea').val('');
            $('.to_value textarea').val('');
        } else {
            $('.img').slideUp();
            $('.img input').val('');
            $('.img img').attr('src', '');
            $('.value, .to_value').slideDown();
        }

        $('.type').on('change', function () {
            var val = $(this).val();
            if (val == 'image') {
                $('.img').slideDown();
                $('.value, .to_value').slideUp();
                $('.value textarea').val('');
                $('.to_value textarea').val('');
            } else {
                $('.img').slideUp();
                $('.img input').val('');
                $('.img img').attr('src', '');
                $('.value, .to_value').slideDown();
            }
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#blah')
                            .attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endsection