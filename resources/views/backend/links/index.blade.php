@extends('backend.layouts.default')
@section('styles-plugin')

@endsection
@section('styles')

@endsection
@section('content')
    <a class="btn btn-lg blue" href="{!! URL::route($locale.'backend.links.create') !!}">
        <i class="fa fa-file-o"></i>
        @lang("backend/Link.view.index.Create_New_Link")
    </a>
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN SAMPLE TABLE PORTLET-->
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-bell-o"></i>@lang("backend/Link.view.index.List")
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse">
                        </a>
                        <a href="#portlet-config" data-toggle="modal" class="config">
                        </a>
                        <a href="javascript:;" class="reload">
                        </a>
                        <a href="javascript:;" class="remove">
                        </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-scrollable">
                        <table class="table table-striped table-bordered table-advance table-hover">
                            <thead>
                            <tr>
                                <th>
                                    <i class="fa fa-title"></i>@lang("backend/Link.view.index.Title")
                                </th>
                                <th>
                                    <i class="fa fa-desc"></i>@lang("backend/Link.view.index.URL")
                                </th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($links as $link)
                                <tr class="row-item">
                                    <td>
                                        <a href="{!! URL::route($locale.'backend.links.edit',$link->id) !!}">
                                            {!! $link->title  or 'No '.trans("backend/Link.view.index.Title") !!}
                                        </a>
                                    </td>
                                    <td class="hidden-xss">
                                        {!! $link->url or 'No URL' !!}
                                    </td>
                                    <td>
                                        <a title="edit"
                                           href="{!! URL::route($locale.'backend.links.edit',$link->id) !!}"
                                           class="btn default btn-xs purple">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <a title="delete"
                                           href="{!! URL::route($locale.'backend.links.destroy',$link->id) !!}"
                                           class="btn default btn-xs red delete">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END SAMPLE TABLE PORTLET-->
        </div>
    </div>

@endsection
@section('scripts-plugin')

@endsection
@section('scripts')

@endsection
@section('scripts-inline')
    <script>
        jQuery(document).ready(function () {

        });
    </script>
@endsection
