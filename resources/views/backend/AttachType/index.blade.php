@extends('backend.layouts.default')
@section('styles-plugin')

@endsection
@section('styles')

@endsection
@section('content')
    <a class="btn btn-lg green" href="{!! URL::route('backend.attachment.createGroup') !!}">
        <i class="fa fa-file-o"></i>
        @lang("backend/AttachType.view.index.CreateGroupOfAttachmentType")
    </a>
    <a class="btn btn-lg blue" href="{!! URL::route($locale.'backend.attachment.create') !!}">
        <i class="fa fa-file-o"></i>
        @lang("backend/AttachType.view.index.CreateAttachmentType")
    </a>

    <a class="btn btn-lg purple" href="{!! URL::route('backend.attachment.listUploaded') !!}">
        <i class="fa fa-file-o"></i>
        @lang("backend/AttachType.view.index.ListUploadedFromUsers")
    </a>


    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN SAMPLE TABLE GROUP-->
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-bell-o"></i>@lang("backend/AttachType.view.index.ListTypeAttachmentGroup")
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse">
                        </a>
                        <a href="#portlet-config" data-toggle="modal" class="config">
                        </a>
                        <a href="javascript:;" class="reload">
                        </a>
                        <a href="javascript:;" class="remove">
                        </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-scrollable">
                        <table class="table table-striped table-bordered table-advance table-hover">
                            <thead>
                            <tr>
                                <th>
                                    <i class="fa fa-building"></i>@lang("backend/AttachType.view.index.Title")
                                </th>
                                <th class="hidden-xs">
                                    <i class="fa fa-keyboard-o"></i>@lang("backend/AttachType.view.index.Alias")
                                </th>
                                <th class="hidden-xs">
                                    <i class="fa fa-keyboard-o"></i>@lang("backend/AttachType.view.index.Description")
                                </th>
                                <th>
                                </th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($dataGroup as $item)
                                <tr class="row-item-group" data-id="{!!$item->id!!}">
                                    <td class="highlight">
                                        <div class="success">
                                        </div>
                                        {!! ($item->title) !!}
                                    </td>
                                    <td>
                                        {!! ($item->alias) !!}
                                    </td>
                                    <td>
                                        {!! ($item->description) !!}
                                    </td>

                                    <td data-id="{!!$item->id!!}" class="row-action">
                                        <a href="{!!URL::route('backend.attachment.editGroup', ['group_id'=>$item->id])!!}"
                                           class="btn default btn-xs purple edit">
                                            <i class="fa fa-edit"></i> @lang("backend/AttachType.view.index.Edit") </a>
                                        <a href="{!!URL::route('backend.attachment.destroyGroup', ['group_id'=>$item->id])!!}"
                                           class="btn default btn-xs red deleteGroup">
                                            <i class="fa fa-times"></i> @lang("backend/AttachType.view.index.Delete")
                                        </a>
                                        <a data-status="{!!$item->group_status!!}"
                                           href="{!!URL::route('backend.attachment.activeGroup', ['group_id'=>$item->id])!!}"
                                           class="btn default btn-xs green activateGroup">
                                            <i class="fa fa-link"></i> @if($item->group_status) @lang("backend/AttachType.view.index.IsEnable") @else @lang("backend/AttachType.view.index.IsDisable") @endif
                                        </a>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END SAMPLE TABLE GROUP-->
            <!-- BEGIN SAMPLE TABLE TYPE-->
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-bell-o"></i>@lang("backend/AttachType.view.index.ListTypeAttachment")
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse">
                        </a>
                        <a href="#portlet-config" data-toggle="modal" class="config">
                        </a>
                        <a href="javascript:;" class="reload">
                        </a>
                        <a href="javascript:;" class="remove">
                        </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-scrollable">
                        <table class="table table-striped table-bordered table-advance table-hover">
                            <thead>
                            <tr>
                                <th>
                                    <i class="fa fa-building"></i>@lang("backend/AttachType.view.index.Title")
                                </th>
                                <th class="hidden-xs">
                                    <i class="fa fa-keyboard-o"></i>@lang("backend/AttachType.view.index.Alias")
                                </th>
                                <th class="hidden-xs">
                                    <i class="fa fa-keyboard-o"></i>@lang("backend/AttachType.view.index.Description")
                                </th>
                                <th class="hidden-xs">
                                    <i class="fa fa-keyboard-o"></i>@lang("backend/AttachType.view.index.Group")
                                </th>
                                <th>
                                </th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($data as $item)
                                <tr class="row-item" data-id="{!!$item->id!!}">
                                    <td class="highlight">
                                        <div class="success">
                                        </div>
                                        {!! ($item->title) !!}
                                    </td>
                                    <td>
                                        {!! ($item->alias) !!}
                                    </td>
                                    <td>
                                        {!! ($item->description) !!}
                                    </td>
                                    <td>
                                        {!! ($item->title_group) !!}
                                    </td>

                                    <td data-id="{!!$item->id!!}" class="row-action">
                                        <a href="{!!URL::route($locale.'backend.attachment.edit', ['attach_type_id'=>$item->id])!!}"
                                           class="btn default btn-xs purple edit">
                                            <i class="fa fa-edit"></i> @lang("backend/AttachType.view.index.Edit") </a>
                                        <a href="{!!URL::route($locale.'backend.attachment.destroy', ['attach_type_id'=>$item->id])!!}"
                                           class="btn default btn-xs red delete">
                                            <i class="fa fa-times"></i> @lang("backend/AttachType.view.index.Delete")
                                        </a>
                                        <a data-status="{!!$item->type_status!!}"
                                           href="{!!URL::route('backend.attachment.active', ['attach_type_id'=>$item->id])!!}"
                                           class="btn default btn-xs green activate">
                                            <i class="fa fa-link"></i> @if($item->type_status) @lang("backend/AttachType.view.index.IsEnable") @else @lang("backend/AttachType.view.index.IsDisable") @endif
                                        </a>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END SAMPLE TABLE TYPE-->
        </div>
    </div>

@endsection
@section('scripts-plugin')

@endsection
@section('scripts')
    <script src="{{ asset('/assets/js/backend/attach-type.js') }}" type="text/javascript"></script>

@endsection
@section('scripts-inline')
    <script>
        jQuery(document).ready(function () {

        });
    </script>
@endsection

