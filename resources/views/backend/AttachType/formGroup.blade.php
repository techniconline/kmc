@extends('backend.layouts.default')
@section('styles-plugin')

@endsection
@section('styles')

@endsection
@section('content')
    <a href="{!! URL::route($locale.'backend.attachment.index') !!}" class="btn btn-lg red">
        <i class="fa fa-backward"></i>
        @lang("backend/AttachType.view.formGroup.Back")
    </a>
    @if(($error))

        @foreach($error as $item)
            <div class="errors">{!!$item!!}</div>
        @endforeach

    @else

        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXTRAS PORTLET-->
                <div class="portlet box blue-hoki">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cogs"></i>{!! $title !!}
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">
                            </a>
                            <a href="#portlet-config" data-toggle="modal" class="config">
                            </a>
                            <a href="javascript:;" class="reload">
                            </a>
                            <a href="javascript:;" class="remove">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body form">

                        @if(isset($data))
                            {!!Form::open(['route' => ['backend.attachment.updateGroup',$data['id']],'method' => 'PUT','class'=>'form-horizontal form-bordered'])!!}
                        @else
                            {!!Form::open(['route' => 'backend.attachment.storeGroup','class'=>'form-horizontal form-bordered'])!!}
                        @endif

                        <div class="form-body">

                            <div class="form-group">
                                <label for="short_description" class="control-label col-md-2">
                                    @lang("backend/AttachType.view.formGroup.Title")
                                </label>

                                <div class="col-md-10">
                                    {!!Form::text('title',isset($data['title'])?$data['title']:null,array('class'=>'form-control'))!!}
                                    {!!$errors->first('title','<span class="error">:message</span>')!!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="alias" class="control-label col-md-2">
                                    @lang("backend/AttachType.view.formGroup.Alias")</label>

                                <div class="col-md-10">
                                    <input type="text" value="{!! isset($data['alias'])?$data['alias']:null !!}"
                                           class="form-control" name="alias">
                                    {!!$errors->first('alias','<span class="error">:message</span>')!!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="condition" class="control-label col-md-2">
                                    @lang("backend/AttachType.view.formGroup.Condition")</label>

                                <div class="col-md-10">
                                    <input type="text" value="{!! isset($data['condition'])?$data['condition']:null !!}"
                                           class="form-control" name="condition">
                                    {!!$errors->first('condition','<span class="error">:message</span>')!!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="short_description" class="control-label col-md-2">
                                    @lang("backend/AttachType.view.formGroup.Description")
                                </label>

                                <div class="col-md-10">
                                    {!!Form::textarea('description',isset($data['description'])?$data['description']:null,array('class'=>'form-control', 'rows'=>'2'))!!}
                                    {!!$errors->first('description','<span class="error">:message</span>')!!}
                                </div>
                            </div>

                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-6">
                                    <button type="submit" class="btn purple"><i
                                                class="fa fa-check"></i>{!! $btnTitle !!}</button>
                                    <button type="button"
                                            class="btn default">@lang("backend/AttachType.view.formGroup.Cancel")</button>
                                    {!!Form::select  ('lang', $listLang,$setLang,['class'=>'btn form-control input-small select2me'])!!}
                                </div>
                            </div>
                        </div>
                        {!!Form::close()!!}

                    </div>
                </div>
            </div>
        </div>

    @endif


@endsection
@section('scripts-plugin')

@endsection
@section('scripts')
    <script src="{{ asset('/assets/theme/assets/admin/pages/scripts/components-editors.js') }}"
            type="text/javascript"></script>

@endsection
@section('scripts-inline')

    <script>
        jQuery(document).ready(function () {

        });
    </script>
@endsection

