@extends('backend.layouts.default')
@section('styles-plugin')
    <link href="{{ asset('/assets/theme/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css') }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/theme/assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css') }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/theme/assets/global/plugins/bootstrap-summernote/summernote.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection
@section('styles')

@endsection
@section('content')
    <a href="{!! URL::route($locale.'backend.apartment.index') !!}" class="btn btn-lg red">
        <i class="fa fa-backward"></i>
        @lang("backend/Apartment.view.create.Back")
    </a>
    @if(($error))

        @foreach($error as $item)
            <div class="errors">{!!$item!!}</div>
        @endforeach

    @else

        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXTRAS PORTLET-->
                <div class="portlet box blue-hoki">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cogs"></i>{!! $title !!}
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">
                            </a>
                            <a href="#portlet-config" data-toggle="modal" class="config">
                            </a>
                            <a href="javascript:;" class="reload">
                            </a>
                            <a href="javascript:;" class="remove">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        @if(isset($data))
                            {!!Form::open(['route' => [$locale.'backend.apartment.update',$data['id']],'method' => 'PUT','class'=>'form-horizontal form-bordered'])!!}
                            {!!Form::hidden('apartment_id',isset($data['id'])?$data['id']:null,['id'=>'apartment-id'])!!}

                        @else
                            {!!Form::open(['route' => $locale.'backend.apartment.store','class'=>'form-horizontal form-bordered'])!!}
                        @endif
                        {{--<form class="form-horizontal form-bordered">--}}
                        <div class="form-body">
                            <div class="form-group">
                                {!!Form::label('title',trans("backend/Apartment.view.create.Title"),['class'=>'control-label col-md-2'])!!}
                                <div class="col-md-10">
                                    {!!Form::text('title',isset($data['title'])?$data['title']:null,array('class'=>'form-control'))!!}
                                    {!!$errors->first('title','<span class="error">:message</span>')!!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="short_description"
                                       class="control-label col-md-2">@lang("backend/Apartment.view.create.ShortDescription")</label>

                                <div class="col-md-10">
                                    <textarea name="short_description" class="wysihtml5 form-control"
                                              rows="3">{!! isset($data['short_description'])?$data['short_description']:null !!}</textarea>
                                    {!!$errors->first('short_description','<span class="error">:message</span>')!!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="description"
                                       class="control-label col-md-2">@lang("backend/Apartment.view.create.Description")</label>

                                <div class="col-md-10">
                                    <textarea name="description" class="wysihtml5 form-control"
                                              rows="6">{!! isset($data['description'])?$data['description']:null !!}</textarea>
                                    {!!$errors->first('description','<span class="error">:message</span>')!!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="formatted_address"
                                       class="control-label col-md-2">@lang("backend/Apartment.view.create.Address")</label>

                                <div class="col-md-10">
                                    <input type="text"
                                           value="{!! isset($data['formatted_address'])?$data['formatted_address']:null !!}"
                                           class="form-control address-apt" maxlength="255" name="formatted_address">
                                    {!!$errors->first('formatted_address','<span class="error">:message</span>')!!}
                                </div>
                            </div>

                            @if(isset($data))
                                <div class="form-group">
                                    <label for="formatted_address"
                                           class="control-label col-md-2">@lang("backend/Apartment.view.create.FileUpload")</label>

                                    <div class="col-md-10">

                                        <div id="imageUploader">@lang("backend/Apartment.view.create.Upload")</div>
                                        <div id="startUploadImage"
                                             class="ajax-file-upload-green">@lang("backend/Apartment.view.create.StartImageUpload")</div>
                                        <div id="msgUploaderImage" style="border: 1px solid #CD3F3F"></div>
                                        @if(isset($images))
                                            <div class="row">
                                                <div class="col-md-10">
                                                    <!-- BEGIN SAMPLE TABLE PORTLET-->
                                                    <div class="portlet">
                                                        <div class="portlet-title">
                                                            <div class="caption">
                                                                <i class="fa fa-bell-o"></i>@lang("backend/Apartment.view.create.ImageList")
                                                            </div>
                                                            <div class="tools">
                                                                <a href="javascript:;" class="collapse">
                                                                </a>
                                                                <a href="#portlet-config" data-toggle="modal"
                                                                   class="config">
                                                                </a>
                                                                <a href="javascript:;" class="reload">
                                                                </a>
                                                                <a href="javascript:;" class="remove">
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="portlet-body">
                                                            <div class="table-scrollable">
                                                                <table data-url="{!! URL::route('backend.apartment.isDefault') !!}"
                                                                       class="table table-striped table-bordered table-advance table-hover">
                                                                    <thead>
                                                                    <tr>
                                                                        <th>
                                                                            <i class="fa fa-file-photo-o"></i>@lang("backend/Apartment.view.create.Title")
                                                                        </th>
                                                                        <th>
                                                                            <i class="fa fa-file-image-o "></i>@lang("backend/Apartment.view.create.Image")
                                                                        </th>
                                                                        <th>
                                                                            <i class="fa fa-times "></i>@lang("backend/Apartment.view.create.Delete")
                                                                        </th>
                                                                        <th>
                                                                            <i class="fa fa-edit "></i>@lang("backend/Apartment.view.create.IsDefault")
                                                                        </th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    @foreach($images as $img)
                                                                        <tr>
                                                                            <td class="highlight">
                                                                                <div class="success">
                                                                                </div>
                                                                                @if(!$img->title)
                                                                                    <div>
                                                                                        <input type="checkbox"
                                                                                               value="{!!$img->media_id!!}"
                                                                                               name="image[]">
                                                                                        <input type="text" value=""
                                                                                               name="image_title[{!!$img->media_id!!}]"
                                                                                               placeholder="type title...">
                                                                                    </div>
                                                                                @else
                                                                                    <a href="#">
                                                                                        {!!$img->title?$img->title:'No Title!'!!}
                                                                                    </a>
                                                                                @endif
                                                                            </td>
                                                                            <td class="hidden-xs">
                                                                                <img width="50" height="33"
                                                                                     src="{!!$img->image!!}" alt=""/>
                                                                            </td>
                                                                            <td>
                                                                                <input type="checkbox"
                                                                                       value="{!!$img->media_id!!}"
                                                                                       name="media_del[]">
                                                                                {{--<a href="#{!!$img->media_id!!}" class="btn default btn-xs red">--}}
                                                                                {{--<i class="fa fa-times"></i> Delete </a>--}}
                                                                            </td>
                                                                            <td>
                                                                                <input class="media-default"
                                                                                       type="radio"
                                                                                       value="{!!$img->media_id!!}"
                                                                                       {!! $img->is_default?'checked':'' !!} name="image_default">
                                                                            </td>
                                                                        </tr>
                                                                    @endforeach


                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- END SAMPLE TABLE PORTLET-->
                                                </div>
                                            </div>
                                        @endif
                                        <div id="videoUploader">@lang("backend/Apartment.view.create.Upload")</div>
                                        <div id="startUploadVideo"
                                             class="ajax-file-upload-green">@lang("backend/Apartment.view.create.StartVideoUpload")</div>
                                        <div id="msgUploaderVideo" style="border: 1px solid #CD3F3F"></div>
                                        @if(isset($videos))
                                            <div class="row">
                                                <div class="col-md-10">
                                                    <!-- BEGIN SAMPLE TABLE PORTLET-->
                                                    <div class="portlet">
                                                        <div class="portlet-title">
                                                            <div class="caption">
                                                                <i class="fa fa-bell-o"></i>@lang("backend/Apartment.view.create.VideoList")
                                                            </div>
                                                            <div class="tools">
                                                                <a href="javascript:;" class="collapse">
                                                                </a>
                                                                <a href="#portlet-config" data-toggle="modal"
                                                                   class="config">
                                                                </a>
                                                                <a href="javascript:;" class="reload">
                                                                </a>
                                                                <a href="javascript:;" class="remove">
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="portlet-body">
                                                            <div class="table-scrollable">
                                                                <table class="table table-striped table-bordered table-advance table-hover">
                                                                    <thead>
                                                                    <tr>
                                                                        <th>
                                                                            <i class="fa fa-file-video-o"></i>@lang("backend/Apartment.view.create.Title")
                                                                        </th>
                                                                        <th>
                                                                            <i class="fa fa-link"></i>@lang("backend/Apartment.view.create.Link")
                                                                        </th>
                                                                        <th>
                                                                            <i class="fa fa-times "></i>@lang("backend/Apartment.view.create.Delete")
                                                                        </th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    @foreach($videos as $vdo)
                                                                        <tr>
                                                                            <td class="highlight">
                                                                                <div class="success">
                                                                                </div>
                                                                                @if(!$img->title)
                                                                                    <div>
                                                                                        <input type="checkbox"
                                                                                               value="{!!$vdo->media_id!!}"
                                                                                               name="video[]">
                                                                                        <input type="text" value=""
                                                                                               name="video_title[{!!$vdo->media_id!!}]"
                                                                                               placeholder="type title...">
                                                                                    </div>
                                                                                @else
                                                                                    <a href="{!!$vdo->video!!}">
                                                                                        {!!$vdo->title?$vdo->title:'No Title!'!!}
                                                                                    </a>
                                                                                @endif
                                                                            </td>
                                                                            <td class="hidden-xs">
                                                                                <a class="btn btn-sm grey-cascade"
                                                                                   href="{!!$vdo->video!!}">
                                                                                    View
                                                                                    <i class="fa fa-link"></i>
                                                                                </a>
                                                                            </td>
                                                                            <td>
                                                                                <input type="checkbox"
                                                                                       value="{!!$vdo->media_id!!}"
                                                                                       name="media_del[]">
                                                                            </td>
                                                                        </tr>
                                                                    @endforeach


                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- END SAMPLE TABLE PORTLET-->
                                                </div>
                                            </div>
                                        @endif

                                    </div>
                                </div>
                            @endif

                            <div class="form-group">
                                <label for="floor_number"
                                       class="control-label col-md-2">@lang("backend/Apartment.view.create.FloorCount")</label>

                                <div class="col-md-2">
                                    <input type="text"
                                           value="{!! isset($data['floor_number'])?$data['floor_number']:null !!}"
                                           class="form-control" maxlength="5" name="floor_number" id="mask_number">
                                    {!!$errors->first('floor_number','<span class="error">:message</span>')!!}
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="room_number"
                                       class="control-label col-md-2">@lang("backend/Apartment.view.create.RoomCount")</label>

                                <div class="col-md-2">
                                    <input type="text"
                                           value="{!! isset($data['room_number'])?$data['room_number']:null !!}"
                                           class="form-control" maxlength="5" name="room_number" id="mask_number">
                                    {!!$errors->first('room_number','<span class="error">:message</span>')!!}
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="parking_number"
                                       class="control-label col-md-2">@lang("backend/Apartment.view.create.ParkingCount")</label>

                                <div class="col-md-2">
                                    <input type="text"
                                           value="{!! isset($data['parking_number'])?$data['parking_number']:null !!}"
                                           class="form-control" maxlength="5" name="parking_number" id="mask_number">
                                    {!!$errors->first('parking_number','<span class="error">:message</span>')!!}
                                </div>
                            </div>

                            <input type="hidden" value="{!! isset($data['lat'])?$data['lat']:null !!}"
                                   class="form-control lat-apt" maxlength="15" name="lat" id="mask_number">
                            <input type="hidden" value="{!! isset($data['lng'])?$data['lng']:null !!}"
                                   class="form-control lng-apt" maxlength="15" name="lng" id="mask_number">
                            {!!$errors->first('lng','<span class="error">:message</span>')!!}
                            {!!$errors->first('lat','<span class="error">:message</span>')!!}


                            <div class="form-group last">
                                <label for="map"
                                       class="control-label col-md-2">@lang("backend/Apartment.view.create.Map")</label>

                                <div class="col-md-10">
                                    <div id="map-canvas"></div>
                                </div>

                            </div>


                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-6">
                                    {!!Form::select('lang', $listLang,$setLang,['class'=>'btn form-control input-small select2me'])!!}
                                    <button type="submit" class="btn purple"><i
                                                class="fa fa-check"></i>{!! $btnTitle !!}</button>
                                    <button type="button"
                                            class="btn default">@lang("backend/Apartment.view.create.Cancel")</button>
                                </div>
                            </div>
                        </div>
                        {!!Form::close()!!}

                    </div>
                </div>
            </div>
        </div>


    @endif
@endsection
@section('scripts-plugin')

    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-markdown/lib/markdown.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-summernote/summernote.min.js') }}"
            type="text/javascript"></script>


@endsection
@section('scripts')
    <script src="{{ asset('/assets/theme/assets/admin/pages/scripts/components-editors.js') }}"
            type="text/javascript"></script>


@endsection
@section('scripts-inline')

    <script>
        jQuery(document).ready(function () {

            ComponentsEditors.init();
        });
    </script>
@endsection