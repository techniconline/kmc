@extends('backend.layouts.default')
@section('styles-plugin')

@endsection
@section('styles')

@endsection
@section('content')
    @if (Session::has('flash_notification.message'))
        <div class="alert alert-{{ Session::get('flash_notification.level') }}">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

            {{ Session::get('flash_notification.message') }}
        </div>
    @endif

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN SAMPLE TABLE PORTLET-->
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-bell-o"></i>@lang("backend/Comment.view.index.List")
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse">
                        </a>
                        <a href="#portlet-config" data-toggle="modal" class="config">
                        </a>
                        <a href="javascript:;" class="reload">
                        </a>
                        <a href="javascript:;" class="remove">
                        </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-scrollable">
                        <table class="table table-striped table-bordered table-advance table-hover">
                            <thead>
                            <tr>
                                <th>
                                    <i class="fa fa-info"></i>@lang("backend/Comment.view.form.Title")
                                </th>
                                <th>
                                    <i class="fa fa-globe"></i>@lang("backend/Comment.view.form.Type")
                                </th>

                                <th>
                                    <i class="fa fa-user"></i>@lang("backend/Comment.view.index.Author")
                                </th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $comment)
                                <tr class="row-item">
                                    <td class="highlight">
                                        {{--<a href="{!! URL::route($locale.'backend.comments.edit',$comment->id) !!}">--}}
                                            {!! $comment->id  or 'No Text' !!}.
                                            {!! $comment->text  or 'No Text' !!}
                                        {{--</a>--}}
                                    </td>

                                    <td>
                                        <a href="{!! $comment->url !!}" target="_blank">{!! $comment->system  or 'No System' !!}</a>
                                    </td>

                                    <td>
                                        {!! $comment->user_name or null !!}<br/>
                                        {!! $comment->email or null !!}
                                    </td>
                                    <td>
                                        <a href="{!! URL::route($locale.'backend.comments.destroy',$comment->id) !!}"
                                           class="btn default btn-xs red">
                                            <i class="fa fa-times"></i>@lang("backend/Comment.view.index.Delete")</a>
                                    </td>
                                </tr>
                            @endforeach

                            <tr>
                                <td colspan="2">
                                    {!!$data->render()!!}
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END SAMPLE TABLE PORTLET-->
        </div>
    </div>

@endsection
@section('scripts-plugin')

@endsection
@section('scripts')
    {{--<script src="{{ asset('/assets/js/backend/apartment.js') }}" type="text/javascript"></script>--}}

@endsection
@section('scripts-inline')
    {{--<script>--}}
    {{--jQuery(document).ready(function() {--}}

    {{--});--}}
    {{--</script>--}}
@endsection
