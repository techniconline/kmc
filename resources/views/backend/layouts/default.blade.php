<!DOCTYPE html>
<?php  $langDetails = app('activeLangDetails');?>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title>{!!Config::get('app.webSiteName')!!} | Admin Dashboard</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta content="{!!Config::get('app.descriptionWebSite')!!}" name="description"/>
    <meta content="" name="author"/>
    {!! ParamsHelper::favIcon() !!}
    <script src="{{ asset('/assets/theme/assets/global/plugins/jquery.min.js') }}" type="text/javascript"></script>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    {{--<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>--}}
    <link href="{{ asset('/assets/css/styleCustom.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/theme/assets/global/plugins/font-awesome/css/font-awesome.min.css') }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/theme/assets/global/plugins/simple-line-icons/simple-line-icons.min.css') }}"
          rel="stylesheet" type="text/css"/>
    @if($langDetails['is_rtl'])
        <link href="{{ asset('/assets/theme/assets/global/plugins/bootstrap/css/bootstrap-rtl.min.css') }}"
              rel="stylesheet" type="text/css"/>
    @else
        <link href="{{ asset('/assets/theme/assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet"
              type="text/css"/>
    @endif
    <link href="{{ asset('/assets/theme/assets/global/plugins/uniform/css/uniform.default.css') }}" rel="stylesheet"
          type="text/css"/>
    <link type="text/css" rel="stylesheet"
          href="{{ asset('/assets/persian-datepicker/css/persianDatepicker-default.css') }}"/>

    @if($langDetails['is_rtl'])
        <link href="{{ asset('/assets/theme/assets/global/plugins/bootstrap-switch/css/bootstrap-switch-rtl.min.css') }}"
              rel="stylesheet" type="text/css"/>
    @else
        <link href="{{ asset('/assets/theme/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}"
              rel="stylesheet" type="text/css"/>
    @endif
    <link href="{{ asset('/assets/theme/assets/global/plugins/icheck/skins/all.css') }}" rel="stylesheet"
          type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL PLUGIN STYLES -->

    @yield('styles-plugin')

            <!-- END PAGE LEVEL PLUGIN STYLES -->
    <!-- BEGIN PAGE STYLES -->

    @yield('styles')

            <!-- END PAGE STYLES -->
    <!-- BEGIN THEME STYLES -->
    <!-- DOC: To use 'rounded corners' style just load 'components-rounded.css') }}' stylesheet instead of 'components.css') }}' in the below style tag -->
    @if($langDetails['is_rtl'])
        <link href="{{ asset('/assets/theme/assets/global/css/components-rtl.css') }}" id="style_components"
              rel="stylesheet" type="text/css"/>
        <link href="{{ asset('/assets/theme/assets/global/css/plugins-rtl.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('/assets/theme/assets/admin/layout/css/layout-rtl.css') }}" rel="stylesheet"
              type="text/css"/>
        <link href="{{ asset('/assets/theme/assets/admin/layout/css/themes/darkblue-rtl.css') }}" rel="stylesheet"
              type="text/css" id="style_color"/>
        <link href="{{ asset('/assets/theme/assets/admin/layout/css/custom-rtl.css') }}" rel="stylesheet"
              type="text/css"/>
    @else
        <link href="{{ asset('/assets/theme/assets/global/css/components.css') }}" id="style_components"
              rel="stylesheet" type="text/css"/>
        <link href="{{ asset('/assets/theme/assets/global/css/plugins.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('/assets/theme/assets/admin/layout/css/layout.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('/assets/theme/assets/admin/layout/css/themes/darkblue.css') }}" rel="stylesheet"
              type="text/css" id="style_color"/>
        <link href="{{ asset('/assets/theme/assets/admin/layout/css/custom.css') }}" rel="stylesheet" type="text/css"/>
        @endif

                <!-- END THEME STYLES -->
        @yield('styles')

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->

<body class="page-header-fixed page-quick-sidebar-over-content page-style-square">
<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner">
        <!-- BEGIN LOGO -->
        <div class="page-logo">
            <a href="{!!url('backend')!!}">
                <img src="{{ asset('/assets/img/logo/logo-backend.png') }}" alt="logo"
                     class="logo-default" style="margin-top: 9px"/>
            </a>

            <div class="menu-toggler sidebar-toggler hide">
                <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
            </div>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse"
           data-target=".navbar-collapse">
        </a>
        <!-- END RESPONSIVE MENU TOGGLER -->
        <!-- BEGIN TOP NAVIGATION MENU -->
        <div class="top-menu">
            <ul class="nav navbar-nav pull-right">
                <!-- BEGIN NOTIFICATION DROPDOWN -->
                <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                <li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar"
                    style="display: none">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                       data-close-others="true">
                        <i class="icon-bell"></i>
					<span class="badge badge-default">
					7 </span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="external">
                            <h3><span class="bold">12 pending</span> notifications</h3>
                            <a href="extra_profile.html">view all</a>
                        </li>
                        <li>
                            <ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283">
                                <li>
                                    <a href="javascript:;">
                                        <span class="time">just now</span>
									<span class="details">
									<span class="label label-sm label-icon label-success">
									<i class="fa fa-plus"></i>
									</span>
									New user registered. </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:;">
                                        <span class="time">3 mins</span>
									<span class="details">
									<span class="label label-sm label-icon label-danger">
									<i class="fa fa-bolt"></i>
									</span>
									Server #12 overloaded. </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:;">
                                        <span class="time">10 mins</span>
									<span class="details">
									<span class="label label-sm label-icon label-warning">
									<i class="fa fa-bell-o"></i>
									</span>
									Server #2 not responding. </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:;">
                                        <span class="time">14 hrs</span>
									<span class="details">
									<span class="label label-sm label-icon label-info">
									<i class="fa fa-bullhorn"></i>
									</span>
									Application error. </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:;">
                                        <span class="time">2 days</span>
									<span class="details">
									<span class="label label-sm label-icon label-danger">
									<i class="fa fa-bolt"></i>
									</span>
									Database overloaded 68%. </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:;">
                                        <span class="time">3 days</span>
									<span class="details">
									<span class="label label-sm label-icon label-danger">
									<i class="fa fa-bolt"></i>
									</span>
									A user IP blocked. </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:;">
                                        <span class="time">4 days</span>
									<span class="details">
									<span class="label label-sm label-icon label-warning">
									<i class="fa fa-bell-o"></i>
									</span>
									Storage Server #4 not responding dfdfdfd. </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:;">
                                        <span class="time">5 days</span>
									<span class="details">
									<span class="label label-sm label-icon label-info">
									<i class="fa fa-bullhorn"></i>
									</span>
									System Error. </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:;">
                                        <span class="time">9 days</span>
									<span class="details">
									<span class="label label-sm label-icon label-danger">
									<i class="fa fa-bolt"></i>
									</span>
									Storage server failed. </span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <!-- END NOTIFICATION DROPDOWN -->
                <!-- BEGIN INBOX DROPDOWN -->
                <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                <li class="dropdown dropdown-extended dropdown-inbox" id="header_inbox_bar" style="display: none">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                       data-close-others="true">
                        <i class="icon-envelope-open"></i>
					<span class="badge badge-default">
					4 </span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="external">
                            <h3>You have <span class="bold">7 New</span> Messages</h3>
                            <a href="page_inbox.html">view all</a>
                        </li>
                        {{--<li>--}}
                        {{--<ul class="dropdown-menu-list scroller" style="height: 275px;" data-handle-color="#637283">--}}
                        {{--<li>--}}
                        {{--<a href="inbox.html?a=view">--}}
                        {{--<span class="photo">--}}
                        {{--<img src="{{ asset('/assets/theme/assets/admin/layout3/img/avatar2.jpg') }}"--}}
                        {{--class="img-circle" alt="">--}}
                        {{--</span>--}}
                        {{--<span class="subject">--}}
                        {{--<span class="from">--}}
                        {{--Lisa Wong </span>--}}
                        {{--<span class="time">Just Now </span>--}}
                        {{--</span>--}}
                        {{--<span class="message">--}}
                        {{--Vivamus sed auctor nibh congue nibh. auctor nibh auctor nibh... </span>--}}
                        {{--</a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                        {{--<a href="inbox.html?a=view">--}}
                        {{--<span class="photo">--}}
                        {{--<img src="{{ asset('/assets/theme/assets/admin/layout3/img/avatar3.jpg') }}"--}}
                        {{--class="img-circle" alt="">--}}
                        {{--</span>--}}
                        {{--<span class="subject">--}}
                        {{--<span class="from">--}}
                        {{--Richard Doe </span>--}}
                        {{--<span class="time">16 mins </span>--}}
                        {{--</span>--}}
                        {{--<span class="message">--}}
                        {{--Vivamus sed congue nibh auctor nibh congue nibh. auctor nibh auctor nibh... </span>--}}
                        {{--</a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                        {{--<a href="inbox.html?a=view">--}}
                        {{--<span class="photo">--}}
                        {{--<img src="{{ asset('/assets/theme/assets/admin/layout3/img/avatar1.jpg') }}"--}}
                        {{--class="img-circle" alt="">--}}
                        {{--</span>--}}
                        {{--<span class="subject">--}}
                        {{--<span class="from">--}}
                        {{--Bob Nilson </span>--}}
                        {{--<span class="time">2 hrs </span>--}}
                        {{--</span>--}}
                        {{--<span class="message">--}}
                        {{--Vivamus sed nibh auctor nibh congue nibh. auctor nibh auctor nibh... </span>--}}
                        {{--</a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                        {{--<a href="inbox.html?a=view">--}}
                        {{--<span class="photo">--}}
                        {{--<img src="{{ asset('/assets/theme/assets/admin/layout3/img/avatar2.jpg') }}"--}}
                        {{--class="img-circle" alt="">--}}
                        {{--</span>--}}
                        {{--<span class="subject">--}}
                        {{--<span class="from">--}}
                        {{--Lisa Wong </span>--}}
                        {{--<span class="time">40 mins </span>--}}
                        {{--</span>--}}
                        {{--<span class="message">--}}
                        {{--Vivamus sed auctor 40% nibh congue nibh... </span>--}}
                        {{--</a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                        {{--<a href="inbox.html?a=view">--}}
                        {{--<span class="photo">--}}
                        {{--<img src="{{ asset('/assets/theme/assets/admin/layout3/img/avatar3.jpg') }}"--}}
                        {{--class="img-circle" alt="">--}}
                        {{--</span>--}}
                        {{--<span class="subject">--}}
                        {{--<span class="from">--}}
                        {{--Richard Doe </span>--}}
                        {{--<span class="time">46 mins </span>--}}
                        {{--</span>--}}
                        {{--<span class="message">--}}
                        {{--Vivamus sed congue nibh auctor nibh congue nibh. auctor nibh auctor nibh... </span>--}}
                        {{--</a>--}}
                        {{--</li>--}}
                        {{--</ul>--}}
                        {{--</li>--}}
                    </ul>
                </li>
                <!-- END INBOX DROPDOWN -->
                <!-- BEGIN TODO DROPDOWN -->
                <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                <li class="dropdown dropdown-extended dropdown-tasks" id="header_task_bar" style="display: none">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                       data-close-others="true">
                        <i class="icon-calendar"></i>
                        <span class="badge badge-default">3</span>
                    </a>
                    <ul class="dropdown-menu extended tasks">
                        <li class="external">
                            <h3>You have <span class="bold">12 pending</span> tasks</h3>
                            <a href="page_todo.html">view all</a>
                        </li>
                        <li>
                            <ul class="dropdown-menu-list scroller" style="height: 275px;" data-handle-color="#637283">
                                <li>
                                    <a href="javascript:;">
									<span class="task">
									<span class="desc">New release v1.2 </span>
									<span class="percent">30%</span>
									</span>
									<span class="progress">
									<span style="width: 40%;" class="progress-bar progress-bar-success"
                                          aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"><span
                                                class="sr-only">40% Complete</span></span>
									</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:;">
									<span class="task">
									<span class="desc">Application deployment</span>
									<span class="percent">65%</span>
									</span>
									<span class="progress">
									<span style="width: 65%;" class="progress-bar progress-bar-danger"
                                          aria-valuenow="65" aria-valuemin="0" aria-valuemax="100"><span
                                                class="sr-only">65% Complete</span></span>
									</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:;">
									<span class="task">
									<span class="desc">Mobile app release</span>
									<span class="percent">98%</span>
									</span>
									<span class="progress">
									<span style="width: 98%;" class="progress-bar progress-bar-success"
                                          aria-valuenow="98" aria-valuemin="0" aria-valuemax="100"><span
                                                class="sr-only">98% Complete</span></span>
									</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:;">
									<span class="task">
									<span class="desc">Database migration</span>
									<span class="percent">10%</span>
									</span>
									<span class="progress">
									<span style="width: 10%;" class="progress-bar progress-bar-warning"
                                          aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"><span
                                                class="sr-only">10% Complete</span></span>
									</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:;">
									<span class="task">
									<span class="desc">Web server upgrade</span>
									<span class="percent">58%</span>
									</span>
									<span class="progress">
									<span style="width: 58%;" class="progress-bar progress-bar-info" aria-valuenow="58"
                                          aria-valuemin="0" aria-valuemax="100"><span
                                                class="sr-only">58% Complete</span></span>
									</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:;">
									<span class="task">
									<span class="desc">Mobile development</span>
									<span class="percent">85%</span>
									</span>
									<span class="progress">
									<span style="width: 85%;" class="progress-bar progress-bar-success"
                                          aria-valuenow="85" aria-valuemin="0" aria-valuemax="100"><span
                                                class="sr-only">85% Complete</span></span>
									</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:;">
									<span class="task">
									<span class="desc">New UI release</span>
									<span class="percent">38%</span>
									</span>
									<span class="progress progress-striped">
									<span style="width: 38%;" class="progress-bar progress-bar-important"
                                          aria-valuenow="18" aria-valuemin="0" aria-valuemax="100"><span
                                                class="sr-only">38% Complete</span></span>
									</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <!-- END TODO DROPDOWN -->
                <!-- BEGIN LANGUAGE BAR -->
                <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                <li class="dropdown dropdown-language">
                    <a href="{!!URL::to($langDetails['lang'].'/backend')!!}" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        <img alt="" src="{{ asset('/assets/theme/assets/global/img/flags/'.($langDetails['lang']=='en'?'us':$langDetails['lang']).'.png') }}">
					<span class="langname">
					{!! strtoupper($langDetails['lang']) !!} </span>
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-default">
                        @foreach(Config::get('app.locales') as $key => $value)
                            <li>
                                <a href="{!!URL::to($key.'/backend')!!}">
                                    <img alt="" src="{{ asset('/assets/theme/assets/global/img/flags/'.($key=='en'?'us':$key).'.png') }}"> {{$value}} </a>
                            </li>
                        @endforeach
                    </ul>
                </li>
                <!-- END LANGUAGE BAR -->
                <!-- BEGIN USER LOGIN DROPDOWN -->
                <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                <li class="dropdown dropdown-user">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                       data-close-others="true">
                        @if(\Illuminate\Support\Facades\File::exists(base_path().env('PUBLIC_PATH_URL', 'public').'/uploads/images/Employees/'.Auth::employee()->get()->id.'-200x200.jpg'))
                            {!! Html::image('uploads/images/Employees/'.Auth::employee()->get()->id.'-200x200.jpg',Auth::employee()->get()->lastname,['width'=>'30','class'=>'img-circle']) !!}
                        @else
                            {!! Html::image('assets/theme/assets/admin/layout/img/avatar.png','avatar',['width'=>'30','class'=>'img-circle']) !!}
                        @endif
                        <span class="username username-hide-on-mobile">
					{{ Auth::employee()->get()->firstname.' '.Auth::employee()->get()->lastname }} </span>
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-default">
                        <li>
                            <a href="{!! route((app('locale')==''?'':app('locale').'.').'backend.profile.edit',Auth::employee()->get()->id) !!}">
                                <i class="icon-user"></i> My Profile </a>
                        </li>
                        <li style="display: none">
                            <a href="page_calendar.html">
                                <i class="icon-calendar"></i> My Calendar </a>
                        </li>
                        <li style="display: none">
                            <a href="inbox.html">
                                <i class="icon-envelope-open"></i> My Inbox <span class="badge badge-danger">
							3 </span>
                            </a>
                        </li>
                        <li style="display: none">
                            <a href="page_todo.html">
                                <i class="icon-rocket"></i> My Tasks <span class="badge badge-success">
							7 </span>
                            </a>
                        </li>
                        <li class="divider">
                        </li>
                        <li style="display: none">
                            <a href="extra_lock.html">
                                <i class="icon-lock"></i> Lock Screen </a>
                        </li>
                        <li>
                            <a href="{{ url('/backend/auth/logout') }}">
                                <i class="icon-key"></i> Log Out </a>
                        </li>
                    </ul>
                </li>
                <!-- END USER LOGIN DROPDOWN -->
                <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                {{--<li class="dropdown dropdown-quick-sidebar-toggler">--}}
                {{--<a href="javascript:;" class="dropdown-toggle">--}}
                {{--<i class="icon-logout"></i>--}}
                {{--</a>--}}
                {{--</li>--}}
                <!-- END QUICK SIDEBAR TOGGLER -->
            </ul>
        </div>
        <!-- END TOP NAVIGATION MENU -->
    </div>
    <!-- END HEADER INNER -->
</div>
<!-- END HEADER -->
<div class="clearfix">
</div>

<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN SIDEBAR -->
    <div class="page-sidebar-wrapper">
        <div class="page-sidebar navbar-collapse collapse">
            <!-- BEGIN SIDEBAR MENU -->
            @include('backend.layouts.partials.sidebarMenu')
                    <!-- END SIDEBAR MENU -->
        </div>
    </div>
    <!-- END SIDEBAR -->
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT -->
        <div class="page-content">
            @if (Session::has('messages'))
                @foreach(Session::get('messages') as $itemMsg)
                    <div class="alert alert-{{ $itemMsg['level']}}">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ $itemMsg['message'] }}
                    </div>
                @endforeach

            @endif
            @if (Session::has('flash_notification.message'))
                <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ Session::get('flash_notification.message') }}
                </div>
            @endif

            @yield('content')
        </div>
        <!-- END CONTENT -->
    </div>
    <!-- END CONTENT -->

    <!-- BEGIN QUICK SIDEBAR -->
    {{--    @include('backend.layouts.partials.quickSidebar')--}}
    <!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="page-footer-inner">
        2015 &copy; KMC by techniconline.com
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]-->
<script src="{{ asset('/assets/theme/assets/global/plugins/respond.min.js') }}"></script>
<script src="{{ asset('/assets/theme/assets/global/plugins/excanvas.min.js') }}"></script>
<!--[endif]-->
<script src="{{ asset('/assets/theme/assets/global/plugins/jquery-migrate.min.js') }}" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->

<script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-daterangepicker/moment.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/theme/assets/global/plugins/carousel-owl-carousel/assets/js/bootstrap-transition.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/theme/assets/global/plugins/carousel-owl-carousel/assets/js/bootstrap-collapse.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/theme/assets/global/plugins/jquery-ui/jquery-ui.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap/js/bootstrap.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/theme/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/theme/assets/global/plugins/jquery.blockui.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/theme/assets/global/plugins/jquery.cokie.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/theme/assets/global/plugins/uniform/jquery.uniform.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}"
        type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{ asset('/assets/persian-datepicker/js/persianDatepicker.js') }}"></script>


@yield('scripts-plugin')

        <!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{ asset('/assets/theme/assets/global/scripts/metronic.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/theme/assets/admin/layout/scripts/layout.js') }}" type="text/javascript"></script>

@yield('scripts')

        <!-- END PAGE LEVEL SCRIPTS -->
<script>
    jQuery(document).ready(function () {
        Metronic.init(); // init metronic core componets
        Layout.init(); // init layout
    });
</script>
@yield('scripts-inline')

        <!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>