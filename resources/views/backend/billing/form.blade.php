@extends('backend.layouts.default')
@section('styles-plugin')
    <link href="{{ asset('/assets/theme/assets/global/plugins/bootstrap-datepicker/css/datepicker.css') }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/theme/assets/global/plugins/select2/select2.css') }}"
          rel="stylesheet" type="text/css"/>

@endsection
@section('styles')

@endsection
@section('content')
    @if (Session::has('flash_notification.message'))
        <div class="alert alert-{{ Session::get('flash_notification.level') }}">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

            {{ Session::get('flash_notification.message') }}
        </div>
    @endif

    <a href="{!! URL::route($locale.'backend.billing.index') !!}" class="btn btn-lg red">
        <i class="fa fa-backward"></i>
        @lang("backend/AttachType.view.form.Back")
    </a>
    @if(isset($error))

        @foreach($error as $item)
            <div class="errors">{!!$item!!}</div>
        @endforeach

    @else

        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXTRAS PORTLET-->
                <div class="portlet box blue-hoki">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cogs"></i>{!! $title !!}
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">
                            </a>
                            <a href="#portlet-config" data-toggle="modal" class="config">
                            </a>
                            <a href="javascript:;" class="reload">
                            </a>
                            <a href="javascript:;" class="remove">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body form">

                        @if(isset($data))
                            {!!Form::open(['route' => [$locale.'backend.billing.update',$data['id']],'method' => 'PUT','class'=>'form-horizontal form-bordered'])!!}
                        @else
                            {!!Form::open(['route' => $locale.'backend.billing.store','class'=>'form-horizontal form-bordered'])!!}
                        @endif

                        <div class="form-body">

                            <div class="form-group">
                                <label for="short_description" class="control-label col-md-2">
                                    @lang("backend/Billing.view.form.RentDate")
                                </label>

                                <div class="col-md-10">
                                    <div class="input-group date date-picker margin-bottom-5 col-md-2"
                                         data-date-format="yyyy-mm" data-date-start-view="months"
                                         data-date-min-view-mode="months">
                                        <input type="text" class="form-control form-filter input-sm rent-date"
                                               name="rent_date"
                                               placeholder="@lang("backend/Billing.view.form.RentDate")"
                                               value="{!! isset($data['year']) && isset($data['month'])?$data['year'].'-'.$data['month']:null!!}">
											<span class="input-group-btn">
											<button class="btn btn-sm default" type="button"><i
                                                        class="fa fa-calendar"></i></button>
											</span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="alias" class="control-label col-md-2">
                                    @lang("backend/Billing.view.form.RoomList")</label>

                                <div class="col-md-10">
                                    {!!Form::select('rent_id', $listRentedRooms,isset($data['rent_id'])?$data['rent_id']:null
                                        ,['id'=>'select2-rooms','class'=>'form-control select2 item-room-select'])!!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="short_description" class="control-label col-md-2">
                                    @lang("backend/Billing.view.form.Price")
                                </label>

                                <div class="col-md-2">
                                    <div class="input-group">
                                        {!!Form::text('price',isset($data['price'])?$data['price']:null,array('class'=>'form-control input-sm','placeholder'=>trans("backend/Billing.view.form.Price")))!!}
                                        <span class="input-group-btn">
											<button class="btn btn-sm default" type="button" disabled><i
                                                        class="fa fa-euro"></i></button>
                                    </span>
                                        {!!$errors->first('title','<span class="error">:message</span>')!!}
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-6">
                                    {!!Form::select('billing_status', trans("backend/Billing.view.form.listBillingStatus"),isset($data['billing_status'])?$data['billing_status']:null,['class'=>'btn form-control input-small select2me'])!!}
                                    <button type="submit" class="btn purple"><i
                                                class="fa fa-check"></i>{!! $btnTitle !!}</button>
                                    <button type="button"
                                            class="btn default">@lang("backend/AttachType.view.form.Cancel")</button>
                                </div>
                            </div>
                        </div>
                        {!!Form::close()!!}

                    </div>
                </div>
            </div>
        </div>

    @endif


@endsection
@section('scripts-plugin')
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootbox/bootbox.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/select2/select2.min.js') }}"
            type="text/javascript"></script>
@endsection
@section('scripts')

    <script src="{{ asset('/assets/js/backend/billing.js') }}" type="text/javascript"></script>

@endsection
@section('scripts-inline')
    <script>
        //    console.log(arrayId);
        jQuery(document).ready(function () {
            fnSelect2();

            var initPickers = function () {
                //init date pickers
                $('.date-picker').datepicker({
                    rtl: Metronic.isRTL(),
                    autoclose: true
                });
            }

            initPickers();

        });
    </script>
    <script>
        jQuery(document).ready(function () {
            // initiate layout and plugins
        });
    </script>
@endsection

