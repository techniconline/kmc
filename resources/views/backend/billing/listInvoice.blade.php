@extends('backend.layouts.default')
@section('styles-plugin')

@endsection
@section('styles')

    <link rel="stylesheet" type="text/css"
          href="{{ asset('/assets/theme/assets/global/plugins/select2/select2.css') }}"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('/assets/theme/assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css') }}"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('/assets/theme/assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css') }}"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('/assets/theme/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css') }}"/>


@endsection
@section('content')
    <a class="btn btn-lg red" href="{!! URL::route($locale.'backend.billing.index') !!}">
        <i class="fa fa-backward"></i>
        @lang("backend/AttachType.view.form.Back")
    </a>

    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box blue-hoki">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-globe"></i>Datatable with TableTools
                    </div>
                    <div class="tools">
                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover" id="sample_1">
                        <thead>
                        <tr>
                            <th>
                                Rendering engine
                            </th>
                            <th>
                                Browser
                            </th>
                            <th>
                                Platform(s)
                            </th>
                            <th>
                                Engine version
                            </th>
                            <th>
                                CSS grade
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                Trident
                            </td>
                            <td>
                                Internet Explorer 4.0
                            </td>
                            <td>
                                Win 95+
                            </td>
                            <td>
                                4
                            </td>
                            <td>
                                X
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->

        </div>
    </div>
    <!-- END PAGE CONTENT-->

@endsection
@section('scripts-plugin')
    {{--<script src="{{ asset('/assets/theme/assets/global/plugins/select2/select2.min.js') }}" type="text/javascript"></script>--}}
    {{--<script src="{{ asset('/assets/theme/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js') }}" type="text/javascript"></script>--}}
    {{--<script src="{{ asset('/assets/theme/assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js') }}" type="text/javascript"></script>--}}
    {{--<script src="{{ asset('/assets/theme/assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js') }}" type="text/javascript"></script>--}}
    {{--<script src="{{ asset('/assets/theme/assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js') }}" type="text/javascript"></script>--}}
    {{--<script src="{{ asset('/assets/theme/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js') }}" type="text/javascript"></script>--}}


@endsection
@section('scripts')


@endsection
@section('scripts-inline')
    <script>
        jQuery(document).ready(function () {

        });
    </script>
@endsection

