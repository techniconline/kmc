<?php $counter = 0; ?>
@foreach($dataBilling as $item)
    <?php $counter++; ?>
    <tr data-id="{!!$item->id!!}" class="row-item">
        <td>
            <a href="#" class="row-details">
                <?php echo $counter; ?>
            </a>
        </td>
        <td>
            <span class="username">{!!$item->firstname.' '.$item->lastname!!}</span>

            <div class="email" data-user="{!!$item->user_id!!}">
                @lang("backend/Billing.view.listBilling.Email"):{!!$item->email!!}
            </div>
            <div class="mobile" data-user="{!!$item->user_id!!}">
                @lang("backend/Billing.view.listBilling.Mobile"): {!!$item->mobile!!}
            </div>
        </td>
        <td style="text-align: center">
            {!!$item->room_number!!}
        </td>
        <td style="text-align: center">
            {!!$item->year.' / '.$item->month!!}
        </td>
        <td style="text-align: center">
            {!!$item->price!!}<i class="fa fa-euro"></i>
        </td>
        <td>
            {!!$item->created_at!!}
        </td>
        <td class="row-action">
            <a href="{!!URL::route($locale.'backend.billing.edit',['rent_id'=>$item->id])!!}"
               class="btn default btn-xs purple list-attachment" target="_blank">
                <i class="fa fa-edit"></i> @lang("backend/Billing.view.listBilling.Edit")
            </a>

            @if($item->billing_status==1)
                <span
                        class="btn default btn-xs green status-paid">
                <i class="fa fa-check"></i> @lang("backend/Billing.view.listBilling.Paid")
            </span>
            @else
                <span class="btn default btn-xs yellow status-paid">
                <i class="fa fa-warning"></i> @lang("backend/Billing.view.listBilling.Unpaid")
            </span>
            @endif

            <span class="loading"
                  style="background: url('{{ asset('/assets/img/loader.gif') }}') no-repeat;padding: 5px 20px 20px; display: none "></span>
            <span class="result"></span>
        </td>

    </tr>

@endforeach
<tr class="paginate">
    <td colspan="8">
        <div class="row">
            <div class="col-md-5 col-sm-12">
                <div class="dataTables_info" id="infoData" role="status" aria-live="polite"
                     style="vertical-align: middle">
                    {!!$dataBilling->appends(Input::all())->render()!!}
                    <span class="loading"
                          style="background: url('{{ asset('/assets/img/loader.gif') }}') no-repeat;padding: 5px 20px 20px; display: none "></span>
                    Showing
                    {!!$dataBilling->firstItem()!!} to {!!$dataBilling->count()!!}
                    of {!!$dataBilling->total()!!} entries
                </div>
            </div>
        </div>
    </td>
</tr>
<script>
    //    console.log(arrayId);
    jQuery(document).ready(function () {
        fnSelect2();
    });
</script>

