@extends('backend.layouts.default')
@section('styles-plugin')
    <link href="{{ asset('/assets/theme/assets/global/plugins/select2/select2.css') }}" rel="stylesheet"
          type="text/css"/>
    {{--    <link href="{{ asset('/assets/theme/assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css') }}" rel="stylesheet" type="text/css"/>--}}
    <link href="{{ asset('/assets/theme/assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css') }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/theme/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css') }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/theme/assets/global/plugins/bootstrap-datepicker/css/datepicker.css') }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/theme/assets/global/plugins/select2/select2.css') }}"
          rel="stylesheet" type="text/css"/>
    <script src="{{ asset('/assets/theme/assets/global/plugins/select2/select2.min.js') }}"
            type="text/javascript"></script>
@endsection
@section('styles')

@endsection
@section('content')
    @if(isset($error)&&($error))

        @foreach($error as $item)
            <div class="errors">{!!$item!!}</div>
        @endforeach

    @else

        @if (Session::has('flash_notification.message'))
            <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

                {{ Session::get('flash_notification.message') }}
            </div>
        @endif
        <div class="list-button">
            <a class="btn btn-sm purple" href="{!!URL::route($locale.'backend.billing.create')!!}">
                <i class="fa fa-plus"></i>
                @lang("backend/Billing.view.index.AddNewRent")
            </a>
            <a id="generate-list" class="btn btn-sm green"
               href="{!!URL::route('backend.billing.generateListRent',['date'=>date("Y-m", strtotime(date("Y-m") . " + 1 month"))])!!}">
                <i class="fa fa-list "></i>
                @lang("backend/Billing.view.index.GenerateListRentFor")
                : {!!date("Y-m", strtotime(date("Y-m") . " + 1 month"))!!}
            </a>
            <a id="remember-list" class="btn btn-sm yellow"
               href="{!!URL::route('backend.billing.sendRememberInvoice',['date'=>date("Y-m")])!!}">
                <i class="fa fa-reply"></i>
                @lang("backend/Billing.view.index.SendMailRemember"): {!!date("Y-m")!!}
            </a>
            <a id="export-list" class="btn btn-sm purple"
               href="{!!URL::route($locale.'backend.billing.show',['date'=>date("Y-m")])!!}">
                <i class="fa fa-file-excel-o"></i>
                @lang("backend/Billing.view.index.ExportInvoiceList")
            </a>
            <span class="loading"
                  style="background: url('{{ asset('/assets/img/loader.gif') }}') no-repeat;padding: 5px 20px 20px; display: none "></span>
            <span class="result"></span>
        </div>


        <p></p>

        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXTRAS PORTLET-->
                <div class="portlet box red-intense">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-globe"></i>@lang("backend/Billing.view.index.ListOfUserRent")
                        </div>
                        <div class="actions">
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover" id="table-advance">
                            <thead>
                            <tr role="row" class="heading">
                                <th class="table-checkbox " style="width: 14px;"></th>
                                <th>
                                    @lang("backend/Billing.view.listBilling.Username")
                                </th>
                                <th>
                                    @lang("backend/Billing.view.listBilling.Room")
                                </th>
                                <th class="hidden-xs">
                                    @lang("backend/Billing.view.listBilling.DateRent")
                                </th>
                                <th class="hidden-xs">
                                    @lang("backend/Billing.view.listBilling.Price")
                                </th>
                                <th class="hidden-xs">
                                    @lang("backend/Billing.view.listBilling.RegDate")
                                </th>
                                <th class="hidden-xs">
                                </th>
                            </tr>
                            @if(!isset($notShowFilter))
                                <tr role="row" class="filter">
                                    <td>
                                        <input type="hidden" class="form-control form-filter input-sm" name="search"
                                               value="1">
                                    </td>
                                    <td>
                                        <input placeholder="@lang("backend/Billing.view.listBilling.Email")" type="text"
                                               class="form-control form-filter input-sm email" name="email">
                                        <input placeholder="@lang("backend/Billing.view.listBilling.LastName")"
                                               type="text" class="form-control form-filter input-sm last_name"
                                               name="last_name">
                                    </td>
                                    <td>
                                        <input placeholder="@lang("backend/Billing.view.listBilling.Room")" type="text"
                                               class="form-control form-filter input-sm room-number" name="room_number">
                                    </td>
                                    <td>
                                        <div class="input-group date date-picker margin-bottom-5"
                                             data-date-format="yyyy-mm" data-date-start-view="months"
                                             data-date-min-view-mode="months">
                                            <input type="text" class="form-control form-filter input-sm date_from"
                                                   name="date_from"
                                                   placeholder="@lang("backend/Billing.view.listBilling.From")">
											<span class="input-group-btn">
											<button class="btn btn-sm default" type="button"><i
                                                        class="fa fa-calendar"></i></button>
											</span>
                                        </div>
                                        <div class="input-group date date-picker" data-date-format="yyyy-mm"
                                             data-date-start-view="months" data-date-min-view-mode="months">
                                            <input type="text" class="form-control form-filter input-sm date_to"
                                                   name="date_to"
                                                   placeholder="@lang("backend/Billing.view.listBilling.To")">
											<span class="input-group-btn">
											<button class="btn btn-sm default" type="button"><i
                                                        class="fa fa-calendar"></i></button>
											</span>
                                        </div>
                                    </td>
                                    <td>
                                        <input placeholder="@lang("backend/Billing.view.listBilling.Price")" type="text"
                                               class="form-control form-filter input-sm price" name="price">
                                    </td>
                                    <td>
                                        <div class="input-group date date-picker margin-bottom-5"
                                             data-date-format="yyyy-mm-dd">
                                            <input type="text" class="form-control form-filter input-sm reg_date_from"
                                                   name="reg_date_from"
                                                   placeholder="@lang("backend/Billing.view.listBilling.From")">
											<span class="input-group-btn">
											<button class="btn btn-sm default" type="button"><i
                                                        class="fa fa-calendar"></i></button>
											</span>
                                        </div>
                                        <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                                            <input type="text" class="form-control form-filter input-sm reg_date_to"
                                                   name="reg_date_to"
                                                   placeholder="@lang("backend/Billing.view.listBilling.To")">
											<span class="input-group-btn">
											<button class="btn btn-sm default" type="button"><i
                                                        class="fa fa-calendar"></i></button>
											</span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="margin-bottom-5 search"
                                             data-url="{!!URL::route('backend.billing.search')!!}">
                                            <button class="btn btn-sm yellow filter-submit margin-bottom btn-search"><i
                                                        class="fa fa-search"></i>
                                                @lang("backend/Billing.view.listBilling.Search")</button>
                                            <span class="loading"
                                                  style="background: url('{{ asset('/assets/img/loader.gif') }}') no-repeat;padding: 5px 20px 20px; display: none "></span>
                                        </div>
                                    </td>
                                </tr>
                            @endif
                            </thead>

                            <tbody id="result-data-table">
                            {!!$pageData!!}
                            </tbody>
                        </table>

                    </div>
                </div>

            </div>
        </div>

    @endif


@endsection
@section('scripts-plugin')
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootbox/bootbox.min.js') }}"
            type="text/javascript"></script>

@endsection
@section('scripts')
    <script src="{{ asset('/assets/js/backend/billing.js') }}" type="text/javascript"></script>

@endsection
@section('scripts-inline')

    <script>
        jQuery(document).ready(function () {
            var initPickers = function () {
                //init date pickers
                $('.date-picker').datepicker({

                    rtl: Metronic.isRTL(),
                    autoclose: true
                });
            }


            initPickers();


        });
    </script>
@endsection

