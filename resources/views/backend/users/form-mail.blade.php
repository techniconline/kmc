@extends('backend.layouts.default')
@section('styles-plugin')
    <link href="{{ asset('/assets/theme/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css') }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/theme/assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css') }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/theme/assets/global/plugins/bootstrap-summernote/summernote.css') }}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('/assets/theme/assets/global/plugins/select2/select2.css') }}"
          rel="stylesheet" type="text/css"/>
    <script src="{{ asset('/assets/theme/assets/global/plugins/select2/select2.min.js') }}"
            type="text/javascript"></script>
@endsection
@section('styles')

@endsection
@section('content')
    <a href="{!! URL::route($locale.'backend.users.index') !!}" class="btn btn-lg red">
        <i class="fa fa-backward"></i>
        @lang("backend/User.view.form.Back")
    </a>

    @if(isset($error))
        @foreach($error as $item)
            <div class="errors">{!!$item!!}</div>
        @endforeach
    @else
        @if (Session::has('flash_notification.message'))
            <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ Session::get('flash_notification.message') }}
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXTRAS PORTLET-->
                <div class="portlet box blue-hoki">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cogs"></i>@lang("backend/User.view.index.Send_Email_to_GROUP")
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">
                            </a>
                            <a href="#portlet-config" data-toggle="modal" class="config">
                            </a>
                            <a href="javascript:;" class="reload">
                            </a>
                            <a href="javascript:;" class="remove">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body form">

                        {!!Form::open(['route' => 'backend.users.sendMailGroup','class'=>'form-horizontal form-bordered','files'=> true])!!}

                        <div class="col-md-12">
                            <div class="form-body">
                                {{--<div class="form-group">--}}
                                {{--<label class="control-label col-md-2">Tags Support List</label>--}}
                                {{--<div class="col-md-6">--}}
                                {{--<input name="list_users" type="hidden" id="select2_users" class="form-control select2" value="">--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                @if(isset($templateList))
                                    <div id="select-items" class="form-group"
                                         data-url="{!!URL::route('backend.users.formSendMailGroup')!!}">
                                        {!!Form::label('subject',trans('backend/User.view.index.template'),['class'=>'control-label col-md-2'])!!}
                                        <div class="col-md-6">
                                            {!!Form::select('template_id', $templateList,$templateId,['class'=>'form-control input-xxlarge template-items'])!!}
                                            {!!$errors->first('subject','<span class="error">:message</span>')!!}
                                            <span class="loading-select"
                                                  style="background: url('{{ asset('/assets/img/loader.gif') }}') no-repeat;padding: 5px 20px 20px; display: none "></span>

                                        </div>
                                        <div class="col-md-4">
                                            <button data-action="1"
                                                    data-url="{{URL::route('backend.users.storeTemplate')}}"
                                                    type="button"
                                                    class="btn green save-template"><i
                                                        class="fa fa-plus"></i>@lang('backend/User.view.index.SaveForTemplate')
                                            </button>
                                        </div>
                                    </div>
                                @endif

                                <div class="form-group">
                                    <label class="control-label col-md-2">@lang("backend/User.view.index.To")</label>

                                    <div class="col-md-10">
                                        <select name="user_list[]" id="select2_users_list" class="form-control select2"
                                                multiple>
                                            <option value="">&nbsp;</option>
                                            @foreach($users as $user)
                                                <option value="{!!$user->id!!}">{!!$user->firstname!!} {!!$user->lastname!!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    {!!Form::label('subject',trans("backend/User.view.index.Subject"),['class'=>'control-label col-md-2'])!!}
                                    <div class="col-md-10">
                                        {!!Form::text('subject',isset($resultTemplate->title)?$resultTemplate->title:null,array('class'=>'form-control'))!!}
                                        {!!$errors->first('subject','<span class="error">:message</span>')!!}
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label for="message"
                                           class="control-label col-md-2">@lang("backend/User.view.index.Message")</label>

                                    <div class="col-md-10">
                                        <textarea name="message" class="wysihtml5 form-control"
                                                  rows="6">{{isset($resultTemplate->text)?$resultTemplate->text:null}}</textarea>
                                        {!!$errors->first('message','<span class="error">:message</span>')!!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    {!!Form::label('attach',trans("backend/User.view.index.Attach"),['class'=>'control-label col-md-2'])!!}
                                    <div class="col-md-10">
                                        {!! Form::file('attach') !!}
                                        {!!$errors->first('attach','<span class="error">:message</span>')!!}
                                    </div>
                                </div>

                            </div>

                        </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-6">
                                    <button type="submit" class="btn purple"><i
                                                class="fa fa-check"></i>{!! trans("backend/User.view.index.Send"); !!}
                                    </button>
                                    <button type="button"
                                            class="btn default">@lang("backend/User.view.form.Cancel")</button>
                                </div>
                            </div>
                        </div>

                        {!!Form::close()!!}

                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection

@section('scripts-plugin')
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-markdown/lib/markdown.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-summernote/summernote.min.js') }}"
            type="text/javascript"></script>

    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootbox/bootbox.min.js') }}"
            type="text/javascript"></script>

@endsection

@section('scripts')
    <script src="{{ asset('/assets/theme/assets/admin/pages/scripts/components-editors.js') }}"
            type="text/javascript"></script>
    {{--    <script src="{{ asset('/assets/js/backend/booking.js') }}" type="text/javascript"></script>--}}
    <script src="{{ asset('/assets/js/backend/users.js') }}" type="text/javascript"></script>

@endsection

@section('scripts-inline')

    <script>

        jQuery(document).ready(function () {

            $("#select2_users").select2({
                tags: ["red", "green", "blue", "yellow", "pink"]
            });

            $('#select2_users_list').select2({
                placeholder: "Select Users",
                allowClear: true
            });


            ComponentsEditors.init();
        });

        $('.resetPass').on('click', function (e) {
            if (!confirm("{!! trans("backend/User.view.form.Are_You_Sure?") !!}")) {
                e.preventDefault();
            }
        });
    </script>
@endsection