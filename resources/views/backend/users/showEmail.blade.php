<div class="modal-content">
    <div class="modal-body">
        <h4 class="modal-answer">
            <h4>@lang('backend/User.view.index.Subject'):</h4>

            <p>{!! $mail->subject !!}</p>
            <h4>@lang('backend/User.view.index.Message'):</h4>

            <p>{!! $mail->message !!}</p>
        </h4>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default default" data-dismiss="modal">OK</button>
    </div>
</div>
