@extends('backend.layouts.default')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN SAMPLE TABLE PORTLET-->
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-bell-o"></i>@lang("backend/User.view.index.List")
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse">
                        </a>
                        <a href="javascript:;" class="remove">
                        </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-scrollable">
                        <table class="table table-striped table-bordered table-advance table-hover">
                            <thead>
                            <tr>
                                <th>
                                    <i class="fa fa-google"></i>@lang("backend/User.view.index.Subject")
                                </th>
                                <th>
                                    <i class="fa fa-google"></i>@lang("backend/User.view.index.Attach")
                                </th>
                                <th>
                                    <i class="fa fa-mobile"></i>@lang("backend/User.view.index.Date")
                                </th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($emails as $email)
                                <tr class="row-item">
                                    <td>
                                        {!! $email->subject or 'No Subject' !!}
                                    </td>
                                    <td>
                                        @if($email->src != '')
                                            <a href="{!! asset($email->src) !!}">
                                                @lang("backend/User.view.index.Attach")
                                            </a>
                                        @else
                                            ---
                                        @endif
                                    </td>
                                    <td>
                                        {!! $email->created_at !!}
                                    </td>
                                    <td>
                                        <a title="@lang('backend/User.view.index.View')" class="btn btn-sm btn-warning"
                                           href="{!! route('backend.users.showEmail',['id' => $email->id]) !!}"
                                           data-target="#ajax" data-toggle="modal" id="ajaxClick">
                                            @lang('backend/User.view.index.View')
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade in" id="ajax" role="basic" aria-hidden="false">
        <div class="modal-backdrop fade in" style="height: 401px;"></div>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <img src="{!! asset('/assets/theme/assets/global/img/loading-spinner-grey.gif') !!}" alt=""
                         class="loading">
                    <span>&nbsp;&nbsp;Loading...</span>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('scripts')

@endsection
@section('scripts-inline')
    <script>
        jQuery(document).ready(function () {

        });

        $('#mailAll').on('submit', function (e) {
            e.preventDefault();
            $.ajax({
                method: 'POST',
                url: $(this).attr('action'),
                data: $(this).serialize(),
                beforeSend: function () {
                    $('input[type="submit"]').addClass('disabled').val('@lang('backend/User.view.index.Please_Wait')');
                },
                complete: function () {
                    $('input[type="submit"]').removeAttr('type').addClass('btn btn-default green-stripe').removeClass('disabled default btn-success').val('@lang('backend/User.view.index.Sent')').attr("data-dismiss", "modal");
                },
                success: function (data) {
                    console.log(data);
                }
            });
        });


    </script>
@endsection
