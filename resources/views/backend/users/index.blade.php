@extends('backend.layouts.default')

@section('styles')
    {!!UploaderHelper::loadUploaderJquery()!!}
@endsection

@section('content')
    <a class="btn btn-primary cmodal" data-toggle="modal" href="#all" data-id="0">
        <i class="fa fa-mail-reply-all"></i>
        @lang("backend/User.view.index.Send_Email_to_All")
    </a>
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN SAMPLE TABLE PORTLET-->
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-bell-o"></i>@lang("backend/User.view.index.List")
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse">
                        </a>
                        <a href="javascript:;" class="remove">
                        </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-scrollable">
                        <table class="table table-striped table-bordered table-advance table-hover">
                            <thead>
                            <tr>
                                <th>
                                    <i class="fa fa-user"></i>
                                </th>
                                <th>
                                    <i class="fa fa-barcode"></i>@lang("backend/User.view.index.Name")
                                </th>
                                <th>
                                    <i class="fa fa-google"></i>@lang("backend/User.view.index.Family")
                                </th>
                                <th>
                                    <i class="fa fa-google"></i>@lang("backend/User.view.index.Email")
                                </th>
                                <th class="hidden-xs">
                                    <i class="fa fa-mobile"></i>@lang("backend/User.view.index.Mobile")
                                </th>
                                <th>
                                    <i class="fa fa-phone"></i>@lang("backend/User.view.index.Telephone")
                                </th>
                                <th>
                                    <i class="fa fa-globe"></i>@lang("backend/User.view.index.Country")
                                </th>
                                <th>
                                    <i class="fa fa-circle"></i>@lang("backend/User.view.index.Status")
                                </th>
                                <th>
                                    <i class="fa fa-calendar"></i>@lang("backend/User.view.index.RegDate")
                                </th>
                                <th>
                                    <i class="fa fa-dropbox"></i>
                                </th>
                            </tr>
                            {!! Form::open(['route' => $locale.'backend.users.index','class'=>'searchClass']) !!}
                            <tr role="row" class="filter">
                                <td>
                                    {!! Form::hidden('filtering',1) !!}
                                </td>
                                <td colspan="2">
                                    <input placeholder="@lang("backend/User.view.index.LastName")" type="text"
                                           class="form-control form-filter input-sm lastname" name="lastname">
                                </td>
                                <td>
                                    <input placeholder="@lang("backend/User.view.index.Email")" type="text"
                                           class="form-control form-filter input-sm email" name="email">
                                </td>
                                <td colspan="2">
                                    <input placeholder="@lang("backend/User.view.index.Mobile")" type="text"
                                           class="form-control form-filter input-sm mobile" name="mobile">
                                </td>
                                <td colspan="2">
                                    <input placeholder="@lang("backend/User.view.index.Country")" type="text"
                                           class="form-control form-filter input-sm country" name="country">
                                </td>
                                <td>
                                    <div class="margin-bottom-5 search" data-url="">
                                        <button type="submit"
                                                class="btn btn-sm yellow filter-submit margin-bottom btn-search"><i
                                                    class="fa fa-search"></i> Search
                                        </button>
                                        <span class="loading"
                                              style="background: url('{{ asset('/assets/img/loader.gif') }}') no-repeat;padding: 5px 20px 20px; display: none "></span>
                                    </div>
                                </td>
                            </tr>
                            {!! Form::close() !!}
                            </thead>
                            <tbody>
                            <tbody id="result-data-table">
                            {!! $users !!}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade bs-modal-lg tab-pane active fontawesome-demo" id="all" tabindex="-1" role="dialog">
        {!! Form::open(['route' => ['backend.users.mail'], 'method' => 'POST', 'id' => 'mailAll', 'files' => 'true']) !!}
        {!! Form::hidden('id',0,['id'=>'cmodalId']) !!}
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <h4 class="modal-answer">
                        <h4>@lang('backend/User.view.index.Subject'):</h4>
                        {!! Form::text('subject',NULL,['class'=>"form-control"]) !!}
                        <h4>@lang('backend/User.view.index.Attach'):</h4>

                        <div class="form-group" id="form-uploader-1" style="margin-bottom: 1px !important;">
                            {!!
                                UploaderHelper::renderUploaderJquery($idUploader='imageUploader-1',$idStartUploader='startUploadImage1'
                                ,$idMsgUploader='msgUploaderImage1'
                                ,$titleUpload=trans("profile.view.DocUpload.Upload"),$titleStartUpload=trans("profile.view.DocUpload.Upload")
                                ,trans("profile.view.index.UploadFilesTitle")
                                ,$urlUploader=URL::route('uploadFile'),$objUploaderName='uploadObjImg1'
                                ,$autoUpload='true',$subFolder='emailAttach'
                                ,$type='images-docs-zip',$maxFileSize=20,$itemId=1,$allowedTypes='rar,zip,png,gif,jpg,jpeg,pdf'
                                ,$showDelete='true',$showDone='false',$fileName='myfile',$titleButtonDrag='attach',$counter=1
                                ,$titleSaveDesc=trans("profile.view.DocUpload.titleSaveDesc")
                                ,$controller='',$controllerDoc=''
                                ,$maxCountFileUploaded=1,$multiUpload='false'
                                )
                            !!}
                        </div>
                        <h4>@lang('backend/User.view.index.Message'):</h4>
                        {!! Form::textarea('message',NULL,['class'=>'form-control']) !!}
                    </h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default default"
                            data-dismiss="modal">@lang('backend/User.view.index.Cancel')</button>
                    <input type="submit" class="btn btn-success" value="@lang('backend/User.view.index.Send')">
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>

@endsection
@section('scripts')

@endsection
@section('scripts-inline')
    <script>
        jQuery(document).ready(function () {

        });

        $('#mailAll').on('submit', function (e) {
            e.preventDefault();
            $.ajax({
                method: 'POST',
                url: $(this).attr('action'),
                data: $(this).serialize(),
                beforeSend: function () {
                    $('input[type="submit"]').addClass('disabled').val('@lang('backend/User.view.index.Please_Wait')');
                },
                complete: function () {
                    $('input[type="submit"]').removeAttr('type').addClass('btn btn-default green-stripe').removeClass('disabled default btn-success').val('@lang('backend/User.view.index.Sent')').attr("data-dismiss", "modal");
                },
                success: function (data) {
                    location.reload();
                }
            });
        });

        $('.cmodal').on('click', function () {
            $('#cmodalId').val($(this).data('id'));
        });

        $('.filter-submit').on('click', function (e) {
            e.preventDefault();
            var $form = $('.searchClass');
            $.ajax({
                method: 'get',
                url: $form.attr('action'),
                data: $form.serialize(),
                beforeSend: function () {
                    $('.loading').fadeIn();
                },
                success: function (data) {
                    $('.loading').fadeOut();
//                    console.log(data);
                    $('#result-data-table').html(data);
                }
            });
        });

        $('#result-data-table').on('click', 'ul.pagination a', function (event) {
            event.preventDefault();
            var clicked = $(this);
            var url = clicked.attr('href');
            var parent = clicked.parents('tr.paginate').first();
            var loading = parent.find('span.loading');
            var resultDataTable = clicked.parents('#result-data-table');

            $.ajax({
                url: url,
                method: 'GET',
                beforeSend: function () {
                    $('.loading').fadeIn();
                },
                success: function (data) {
                    $('.loading').fadeOut();
                    resultDataTable.html(data);
                }
            });

        });


    </script>
@endsection
