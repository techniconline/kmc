@foreach($users as $user)
    <tr class="row-item">
        <td>
            <a href="{!! URL::route($locale.'backend.users.edit',$user->id) !!}">
                @if(\Illuminate\Support\Facades\File::exists(base_path().env('PUBLIC_PATH_URL', 'public').'/uploads/images/Users/'.$user->id.'-200x200.jpg'))
                    {!! Html::image('uploads/images/Users/'.$user->id.'-200x200.jpg',$user->lastname,['width'=>'30']) !!}
                @else
                    {!! Html::image('assets/theme/assets/admin/layout/img/avatar.png','avatar',['width'=>'30']) !!}
                @endif
            </a>
        </td>
        <td>
            {!! $user->firstname  or 'No Name' !!}
        </td>
        <td>
            <a href="{!! URL::route($locale.'backend.users.edit',$user->id) !!}">
                {!! $user->lastname  or 'No Family' !!}
            </a>
        </td>
        <td>
            <a href="{!! URL::route($locale.'backend.users.edit',$user->id) !!}">
                {!! $user->email  or 'No Title' !!}
            </a>
        </td>
        <td class="hidden-xss">
            {!! $user->mobile or 'No Mobile' !!}
        </td>
        <td>
            {!! $user->telephone or 'No Telephone' !!}
        </td>
        <td>
            {!! $user->name !!}
        </td>
        <td>
            {!! ( $user->status == 1) ? trans("backend/User.view.index.Active") : trans("backend/User.view.index.Inactive") !!}
        </td>
        <td>
            {!! ( $user->created_at) !!}
        </td>
        <td>
            <a href="#all" title="@lang('backend/User.view.index.New_Email')" class="btn btn-icon-only green cmodal"
               data-id="{!! $user->id !!}" data-toggle="modal">
                <i class="fa fa-mail-forward"></i>
            </a>
            <a href="{!! route($locale.'backend.users.show',$user->id) !!}"
               title="@lang('backend/User.view.index.History')" class="btn btn-icon-only yellow">
                <i class="fa fa-search"></i>
            </a>
            <a href="{!! URL::route($locale.'backend.users.edit',$user->id) !!}"
               title="@lang('backend/User.view.index.View')" class="btn btn-icon-only purple">
                <i class="fa fa-edit"></i>
            </a>
        </td>
    </tr>
@endforeach

<tr class="paginate">
    <td colspan="8">
        <div class="row">
            <div class="col-md-5 col-sm-12">
                <div class="dataTables_info" id="infoData" role="status" aria-live="polite"
                     style="vertical-align: middle">
                    {!!$users->appends(Input::all())->render()!!}
                    <span class="loading"
                          style="background: url('{{ asset('/assets/img/loader.gif') }}') no-repeat;padding: 5px 20px 20px; display: none "></span>
                </div>
            </div>
        </div>
    </td>
</tr>


