@extends('backend.layouts.default')
@section('styles-plugin')
    <link href="{{ asset('/assets/theme/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css') }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/theme/assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css') }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/theme/assets/global/plugins/bootstrap-summernote/summernote.css') }}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('/assets/theme/assets/global/plugins/select2/select2.css') }}"
          rel="stylesheet" type="text/css"/>
    <script src="{{ asset('/assets/theme/assets/global/plugins/select2/select2.min.js') }}"
            type="text/javascript"></script>
@endsection
@section('styles')

@endsection
@section('content')
    <a href="{!! URL::route($locale.'backend.users.index') !!}" class="btn btn-lg red">
        <i class="fa fa-backward"></i>
        @lang("backend/User.view.form.Back")
    </a>

    @if(isset($error))
        @foreach($error as $item)
            <div class="errors">{!!$item!!}</div>
        @endforeach
    @else
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXTRAS PORTLET-->
                <div class="portlet box blue-hoki">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cogs"></i>@lang("backend/User.view.form.Edit_User")
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">
                            </a>
                            <a href="#portlet-config" data-toggle="modal" class="config">
                            </a>
                            <a href="javascript:;" class="reload">
                            </a>
                            <a href="javascript:;" class="remove">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        @if(isset($user))
                            {!!Form::open(['route' => [$locale.'backend.users.update',$user['id']],'method' => 'PUT','class'=>'form-horizontal form-bordered','files'=> true])!!}
                            {!!Form::hidden('user_id',$user['id'] or null,['id'=>'user-id'])!!}
                        @else
                            {!!Form::open(['route' => $locale.'backend.users.store','class'=>'form-horizontal form-bordered','files'=> true])!!}
                        @endif
                        <div class="col-md-6">
                            <div class="form-body">
                                <div class="form-group">
                                    {!!Form::label('avatar',trans("backend/User.view.form.Avatar"),['class'=>'control-label col-md-2'])!!}
                                    <div class="col-md-10">
                                        @if(isset($user) && \Illuminate\Support\Facades\File::exists(base_path().env('PUBLIC_PATH_URL', 'public').'/uploads/images/Users/'.$user->id.'-200x200.jpg'))
                                            {!! Html::image('uploads/images/Users/'.$user->id.'-200x200.jpg') !!}
                                        @endif
                                        {!! Form::file('avatar') !!}
                                        {!!$errors->first('avatar','<span class="error">:message</span>')!!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    {!!Form::label('status',trans("backend/User.view.form.Status"),['class'=>'control-label col-md-2'])!!}
                                    <div class="col-md-10">
                                        {!! Form::select('status',[1=>'active',0=>'inactive'],isset($user['status'])?$user['status']:null,array('class'=>'form-control')) !!}
                                        {!!$errors->first('status','<span class="error">:message</span>')!!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    {!!Form::label('firstname',trans("backend/User.view.form.Name"),['class'=>'control-label col-md-2'])!!}
                                    <div class="col-md-10">
                                        {!!Form::text('firstname',isset($user['firstname'])?$user['firstname']:null,array('class'=>'form-control'))!!}
                                        {!!$errors->first('firstname','<span class="error">:message</span>')!!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    {!!Form::label('lastname',trans("backend/User.view.form.Family"),['class'=>'control-label col-md-2'])!!}
                                    <div class="col-md-10">
                                        {!!Form::text('lastname',isset($user['lastname'])?$user['lastname']:null,array('class'=>'form-control'))!!}
                                        {!!$errors->first('lastname','<span class="error">:message</span>')!!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    {!!Form::label('email',trans("backend/User.view.form.Email"),['class'=>'control-label col-md-2'])!!}
                                    <div class="col-md-10">
                                        {!!Form::text('email',isset($user['email'])?$user['email']:null,array('class'=>'form-control'))!!}
                                        {!!$errors->first('email','<span class="error">:message</span>')!!}
                                    </div>
                                </div>
                                @if(isset($user))
                                    <div class="form-group">
                                        {!!Form::label('password',trans("backend/User.view.form.Password"),['class'=>'control-label col-md-2'])!!}
                                        <div class="col-md-10">
                                            {!! link_to('front/password/reset/'.$user->id, trans("backend/User.view.form.Reset_Password"),['class' => 'resetPass']) !!}
                                        </div>
                                    </div>
                                @endif
                                <div class="form-group">
                                    {!!Form::label('gender',trans("backend/User.view.form.Gender"),['class'=>'control-label col-md-2'])!!}
                                    <div class="col-md-10">
                                        {!!Form::select('gender',['male'=>'male','female'=>'female'],isset($user['gender'])?$user['gender']:null,array('class'=>'form-control'))!!}
                                        {!!$errors->first('gender','<span class="error">:message</span>')!!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    {!!Form::label('city',trans("backend/User.view.form.City"),['class'=>'control-label col-md-2'])!!}
                                    <div class="col-md-10">
                                        {!!Form::text('city',isset($user['city'])?$user['city']:null,array('class'=>'form-control'))!!}
                                        {!!$errors->first('city','<span class="error">:message</span>')!!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    {!!Form::label('mobile',trans("backend/User.view.form.Mobile"),['class'=>'control-label col-md-2'])!!}
                                    <div class="col-md-10">
                                        {!!Form::text('mobile',isset($user['mobile'])?$user['mobile']:null,array('class'=>'form-control'))!!}
                                        {!!$errors->first('mobile','<span class="error">:message</span>')!!}
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-6">

                            <div class="form-body">

                                <div class="form-group">
                                    {!!Form::label('telephone',trans("backend/User.view.form.Phone"),['class'=>'control-label col-md-2'])!!}
                                    <div class="col-md-10">
                                        {!!Form::text('telephone',isset($user['telephone'])?$user['telephone']:null,array('class'=>'form-control'))!!}
                                        {!!$errors->first('telephone','<span class="error">:message</span>')!!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    {!!Form::label('fax',trans("backend/User.view.form.Fax"),['class'=>'control-label col-md-2'])!!}
                                    <div class="col-md-10">
                                        {!!Form::text('fax',isset($user['fax'])?$user['fax']:null,array('class'=>'form-control'))!!}
                                        {!!$errors->first('fax','<span class="error">:message</span>')!!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    {!!Form::label('birth_date',trans("backend/User.view.form.BirthDate"),['class'=>'control-label col-md-2'])!!}
                                    <div class="col-md-10">
                                        {!!Form::text('birth_date',isset($user['birth_date'])?$user['birth_date']:null,array('class'=>'form-control'))!!}
                                        {!!$errors->first('birth_date','<span class="error">:message</span>')!!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    {!!Form::label('birth_place',trans("home.view.register.birth_place"),['class'=>'control-label col-md-2'])!!}
                                    <div class="col-md-10">
                                        {!!Form::text('birth_place',isset($user['birth_place'])?$user['birth_place']:null,array('class'=>'form-control'))!!}
                                        {!!$errors->first('birth_place','<span class="error">:message</span>')!!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    {!!Form::label('type_personal',trans("home.view.register.TypePersonal"),['class'=>'control-label col-md-2'])!!}
                                    <div class="col-md-10">
                                        {!!Form::select('type_personal'
                                        ,['legal'=>trans("home.view.register.legal"),'real'=>trans("home.view.register.real")]
                                        ,isset($user['type_personal'])?$user['type_personal']:null
                                        ,array('class'=>'form-control'))!!}

                                        {!!$errors->first('type_personal','<span class="error">:message</span>')!!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    {!!Form::label('address',trans("backend/User.view.form.Address"),['class'=>'control-label col-md-2'])!!}
                                    <div class="col-md-10">
                                        {!!Form::textarea('address',isset($user['address'])?$user['address']:null,array('class'=>'form-control', 'rows'=>3))!!}
                                        {!!$errors->first('address','<span class="error">:message</span>')!!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    {!!Form::label('code_postal',trans("backend/User.view.form.Postal_Code"),['class'=>'control-label col-md-2'])!!}
                                    <div class="col-md-10">
                                        {!!Form::text('code_postal',isset($user['code_postal'])?$user['code_postal']:null,array('class'=>'form-control'))!!}
                                        {!!$errors->first('code_postal','<span class="error">:message</span>')!!}
                                    </div>
                                </div>
                                {{--<div class="form-group">--}}
                                {{--{!!Form::label('company',trans("backend/User.view.form.Company"),['class'=>'control-label col-md-2'])!!}--}}
                                {{--<div class="col-md-10">--}}
                                {{--{!!Form::text('company',isset($user['company'])?$user['company']:null,array('class'=>'form-control'))!!}--}}
                                {{--{!!$errors->first('company','<span class="error">:message</span>')!!}--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                <div class="form-group">
                                    {!!Form::label('paypal',trans("backend/User.view.form.Paypal"),['class'=>'control-label col-md-2'])!!}
                                    <div class="col-md-10">
                                        {!!Form::text('paypal',isset($user['paypal'])?$user['paypal']:null,array('class'=>'form-control'))!!}
                                        {!!$errors->first('paypal','<span class="error">:message</span>')!!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    {!!Form::label('country_id',trans("backend/User.view.form.Country"),['class'=>'control-label col-md-2'])!!}
                                    <div class="col-md-10">
                                        {!! Form::select('country_id',$countries,isset($user['country_id'])?$user['country_id']:null,array('class'=>'form-control')) !!}
                                        {!!$errors->first('country_id','<span class="error">:message</span>')!!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    {!!Form::label('zone_id',trans("backend/User.view.form.Zone"),['class'=>'control-label col-md-2'])!!}
                                    <div class="col-md-10">
                                        {!! Form::text('zone_id',isset($user['zone_id'])?$user['zone_id']:0,array('class'=>'form-control')) !!}
                                        {!!$errors->first('zone_id','<span class="error">:message</span>')!!}
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-6">
                                    <button type="submit" class="btn purple"><i
                                                class="fa fa-check"></i>{!! isset($user)? trans("backend/User.view.form.Update") : trans("backend/User.view.form.Create") ; !!}
                                    </button>
                                    <button type="button"
                                            class="btn default">@lang("backend/User.view.form.Cancel")</button>
                                </div>
                            </div>
                        </div>

                        {!!Form::close()!!}

                    </div>
                </div>
            </div>
        </div>


    @endif
@endsection

@section('scripts-plugin')
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-markdown/lib/markdown.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-summernote/summernote.min.js') }}"
            type="text/javascript"></script>

    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootbox/bootbox.min.js') }}"
            type="text/javascript"></script>

@endsection

@section('scripts')
    <script src="{{ asset('/assets/theme/assets/admin/pages/scripts/components-editors.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/js/backend/booking.js') }}" type="text/javascript"></script>

@endsection

@section('scripts-inline')

    <script>
        jQuery(document).ready(function () {

            ComponentsEditors.init();
        });

        $('.resetPass').on('click', function (e) {
            if (!confirm("{!! trans("backend/User.view.form.Are_You_Sure?") !!}")) {
                e.preventDefault();
            }
        });
    </script>
@endsection