@extends('backend.layouts.default')
@section('styles-plugin')
    <link href="{{ asset('/assets/theme/assets/global/plugins/select2/select2.css') }}" rel="stylesheet"
          type="text/css"/>
    {{--    <link href="{{ asset('/assets/theme/assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css') }}" rel="stylesheet" type="text/css"/>--}}
    <link href="{{ asset('/assets/theme/assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css') }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/theme/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css') }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/theme/assets/global/plugins/bootstrap-datepicker/css/datepicker.css') }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/theme/assets/global/plugins/select2/select2.css') }}"
          rel="stylesheet" type="text/css"/>
    <script src="{{ asset('/assets/theme/assets/global/plugins/select2/select2.min.js') }}"
            type="text/javascript"></script>
@endsection
@section('styles')

@endsection
@section('content')
    @if(isset($error)&&($error))

        @foreach($error as $item)
            <div class="errors">{!!$item!!}</div>
        @endforeach

    @else

        <div class="list-button">
            <a class="btn btn-sm purple" href="{!!URL::route($locale.'backend.accessGroup.create')!!}">
                <i class="fa fa-plus"></i>
                @lang("backend/AccessGroup.view.index.Add")
            </a>
            <span class="loading"
                  style="background: url('{{ asset('/assets/img/loader.gif') }}') no-repeat;padding: 5px 20px 20px; display: none "></span>
            <span class="result"></span>
        </div>


        <p></p>

        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXTRAS PORTLET-->
                <div class="portlet box red-intense">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-globe"></i>@lang("backend/AccessGroup.view.index.ListAccessGroup")
                        </div>
                        <div class="actions">
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover" id="table-advance">
                            <thead>
                            <tr role="row" class="heading">
                                <th class="table-checkbox " style="width: 14px;"></th>
                                <th>
                                    @lang("backend/AccessGroup.view.listAccessGroup.GroupName")
                                </th>
                                <th>
                                    @lang("backend/AccessGroup.view.listAccessGroup.TypeAccess")
                                </th>
                                <th class="hidden-xs">
                                </th>
                            </tr>
                            @if(!isset($notShowFilter))
                                <tr role="row" class="filter">
                                    <td>
                                        <input type="hidden" class="form-control form-filter input-sm" name="search"
                                               value="1">
                                    </td>
                                    <td>
                                        <input placeholder="@lang("backend/AccessGroup.view.listAccessGroup.GroupName")"
                                               type="text" class="form-control form-filter input-sm name" name="name">
                                    </td>
                                    <td>
                                        {!!Form::select('type', ['backend'=>trans('backend/AccessGroup.view.form.backend')
                                        ,'frontend'=>trans('backend/AccessGroup.view.form.frontend')] ,isset($data['type'])?$data['type']:null
                                            ,['id'=>'select2-type','class'=>'form-control select2 input-small'])!!}
                                    </td>
                                    <td>
                                        <div class="margin-bottom-5 search"
                                             data-url="{!!URL::route('backend.accessGroup.search')!!}">
                                            <button class="btn btn-sm yellow filter-submit margin-bottom btn-search"><i
                                                        class="fa fa-search"></i>
                                                @lang("backend/AccessGroup.view.listAccessGroup.Search")</button>
                                            <span class="loading"
                                                  style="background: url('{{ asset('/assets/img/loader.gif') }}') no-repeat;padding: 5px 20px 20px; display: none "></span>
                                        </div>
                                    </td>
                                </tr>
                            @endif
                            </thead>

                            <tbody id="result-data-table">
                            {!!$pageData!!}
                            </tbody>
                        </table>

                    </div>
                </div>

            </div>
        </div>

    @endif


@endsection
@section('scripts-plugin')
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootbox/bootbox.min.js') }}"
            type="text/javascript"></script>

@endsection
@section('scripts')

    <script src="{{ asset('/assets/js/backend/billing.js') }}" type="text/javascript"></script>

@endsection
@section('scripts-inline')

    <script>
        jQuery(document).ready(function () {
            var initPickers = function () {
                //init date pickers
                $('.date-picker').datepicker({

                    rtl: Metronic.isRTL(),
                    autoclose: true
                });
            }

            initPickers();


        });
    </script>
@endsection

