<?php $counter = 0; ?>
@if($data)
    @foreach($data as $item)
        <?php $counter++; ?>
        <tr data-id="{!!$item->id!!}" class="row-item">
            <td>
                <a href="#" class="row-details">
                    <?php echo $counter; ?>
                </a>
            </td>
            <td>
                <span class="username">{!!$item->name!!}</span>
            </td>
            <td style="text-align: center">
                {!! trans('backend/AccessGroup.view.listAccessGroup.'.$item->type) !!}
            </td>
            <td class="row-action">
                <a href="{!!URL::route($locale.'backend.accessGroup.edit',['id'=>$item->id])!!}"
                   class="btn default btn-xs purple edit-group" target="">
                    <i class="fa fa-edit"></i> @lang("backend/AccessGroup.view.listAccessGroup.Edit")
                </a>
                <a href="{!!URL::route($locale.'backend.accessGroup.show',['id'=>$item->id])!!}"
                   class="btn default btn-xs green list-users" target="">
                    <i class="fa fa-group"></i> @lang("backend/AccessGroup.view.listAccessGroup.AddUsers")
                </a>
                <a href="{!!URL::route('backend.accessGroup.accessList',['id'=>$item->id])!!}"
                   class="btn default btn-xs green-haze list-access" target="">
                    <i class="fa fa-plus"></i> @lang("backend/AccessGroup.view.listAccessGroup.AddAccess")
                </a>

            <span class="loading"
                  style="background: url('{{ asset('/assets/img/loader.gif') }}') no-repeat;padding: 5px 20px 20px; display: none "></span>
                <span class="result"></span>
            </td>

        </tr>

    @endforeach
@endif
<tr class="paginate">
    <td colspan="8">
        <div class="row">
            <div class="col-md-5 col-sm-12">
                <div class="dataTables_info" id="infoData" role="status" aria-live="polite"
                     style="vertical-align: middle">
                    {!!$data->appends(Input::all())->render()!!}
                    <span class="loading"
                          style="background: url('{{ asset('/assets/img/loader.gif') }}') no-repeat;padding: 5px 20px 20px; display: none "></span>
                    Showing
                    {!!$data->firstItem()!!} to {!!$data->lastItem()!!}
                    of {!!$data->total()!!} entries
                </div>
            </div>
        </div>
    </td>
</tr>
<script>
    //    console.log(arrayId);
    jQuery(document).ready(function () {
        fnSelect2();
    });
</script>

