@extends('backend.layouts.default')
@section('styles-plugin')
    <link href="{{ asset('/assets/theme/assets/global/plugins/bootstrap-datepicker/css/datepicker.css') }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/theme/assets/global/plugins/select2/select2.css') }}"
          rel="stylesheet" type="text/css"/>

@endsection
@section('styles')

@endsection
@section('content')

    <a href="{!! URL::route($locale.'backend.accessGroup.index') !!}" class="btn btn-lg red">
        <i class="fa fa-backward"></i>
        @lang("backend/AttachType.view.form.Back")
    </a>
    @if(isset($error))

        @foreach($error as $item)
            <div class="errors">{!!$item!!}</div>
        @endforeach

    @else

        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXTRAS PORTLET-->
                <div class="portlet box blue-hoki">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cogs"></i>{!! $title !!}
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">
                            </a>
                            <a href="#portlet-config" data-toggle="modal" class="config">
                            </a>
                            <a href="javascript:;" class="reload">
                            </a>
                            <a href="javascript:;" class="remove">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body form">

                        @if(isset($data))
                            {!!Form::open(['route' => [$locale.'backend.accessGroup.update',$data['id']],'method' => 'PUT','class'=>'form-horizontal form-bordered'])!!}
                        @else
                            {!!Form::open(['route' => $locale.'backend.accessGroup.store','class'=>'form-horizontal form-bordered'])!!}
                        @endif

                        <div class="form-body">

                            <div class="form-group">
                                <label for="short_description" class="control-label col-md-2">
                                    @lang("backend/AccessGroup.view.form.Name")
                                </label>

                                <div class="col-md-2">
                                    <div class="input-group">
                                        {!!Form::text('name',isset($data['name'])?$data['name']:null
                                        ,array('class'=>'form-control input-medium','placeholder'=>trans("backend/AccessGroup.view.form.Name")))!!}
                                        {!!$errors->first('name','<span class="error">:message</span>')!!}
                                    </div>
                                </div>

                            </div>

                            <div class="form-group">
                                <label for="alias" class="control-label col-md-2">
                                    @lang("backend/AccessGroup.view.form.TypeAccess")</label>

                                <div class="col-md-2">
                                    {!!Form::select('type', ['backend'=>trans('backend/AccessGroup.view.form.backend')
                                    ,'frontend'=>trans('backend/AccessGroup.view.form.frontend')] ,isset($data['type'])?$data['type']:null
                                        ,['id'=>'select2-type','class'=>'form-control select2 input-small'])!!}
                                </div>
                            </div>

                            @if(isset($userList)&&$userList)

                                <div class="row">
                                    <div class="col-md-12">
                                        <!-- BEGIN PORTLET-->
                                        <div class="portlet box green-haze">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>@lang("backend/AccessGroup.view.form.UserList")
                                                </div>
                                            </div>
                                            <div class="portlet-body form">
                                                <!-- BEGIN FORM-->
                                                <div class="form-body">

                                                    <?php $counter = 0; ?>
                                                    @foreach($userList as $item)

                                                        @if(!$counter)
                                                            <div class="form-group">
                                                                @endif

                                                                <div class="col-md-1">
                                                                    <input type="checkbox" name="user_ids[]"
                                                                           @if(isset($userActiveList) && in_array($item->id,$userActiveList)) checked
                                                                           @endif
                                                                           value="{!!$item->id!!}" class="make-switch"
                                                                           data-size="small">
                                                                </div>
                                                                <label class="control-label col-md-2">
                                                                    {!!$item->email!!} <br/>
                                                                    <b>{!!$item->firstname.' '.$item->lastname!!}</b>
                                                                </label>


                                                                <?php $counter++; ?>
                                                                @if($counter==4)
                                                                    <?php $counter = 0; ?>
                                                            </div>
                                                        @endif
                                                    @endforeach


                                                </div>
                                                <!-- END FORM-->
                                            </div>
                                        </div>
                                        <!-- END PORTLET-->
                                    </div>
                                </div>


                            @endif


                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-6">
                                    <button type="submit" class="btn purple"><i
                                                class="fa fa-check"></i>{!! $btnTitle !!}</button>
                                    <button type="button"
                                            class="btn default">@lang("backend/AttachType.view.form.Cancel")</button>
                                </div>
                            </div>
                        </div>
                        {!!Form::close()!!}

                    </div>
                </div>
            </div>
        </div>

    @endif


@endsection
@section('scripts-plugin')
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootbox/bootbox.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/select2/select2.min.js') }}"
            type="text/javascript"></script>
@endsection
@section('scripts')
    {{--    <script src="{{ asset('/assets/theme/assets/admin/pages/scripts/components-editors.js') }}" type="text/javascript"></script>--}}

    <script src="{{ asset('/assets/js/backend/billing.js') }}" type="text/javascript"></script>

@endsection
@section('scripts-inline')
    <script>
        //    console.log(arrayId);
        jQuery(document).ready(function () {
            fnSelect2();

            var initPickers = function () {
                //init date pickers
                $('.date-picker').datepicker({
                    rtl: Metronic.isRTL(),
                    autoclose: true
                });
            }

//            initPickers();

        });
    </script>
    <script>
        jQuery(document).ready(function () {

        });
    </script>
@endsection

