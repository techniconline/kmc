@extends('backend.layouts.default')
@section('styles-plugin')
    <link href="{{ asset('/assets/theme/assets/global/plugins/bootstrap-datepicker/css/datepicker.css') }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/theme/assets/global/plugins/select2/select2.css') }}"
          rel="stylesheet" type="text/css"/>

@endsection
@section('styles')

@endsection
@section('content')

    <a href="{!! URL::route($locale.'backend.accessGroup.index') !!}" class="btn btn-lg red">
        <i class="fa fa-backward"></i>
        @lang("backend/AttachType.view.form.Back")
    </a>
    @if(isset($error))

        @foreach($error as $item)
            <div class="errors">{!!$item!!}</div>
        @endforeach

    @else

        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXTRAS PORTLET-->
                <div class="portlet box blue-hoki">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cogs"></i>{!! $title !!}
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">
                            </a>
                            <a href="#portlet-config" data-toggle="modal" class="config">
                            </a>
                            <a href="javascript:;" class="reload">
                            </a>
                            <a href="javascript:;" class="remove">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body form">

                        @if(isset($data))
                            {!!Form::open(['route' => ['backend.accessGroup.updateAccessList',$data['id']],'method' => 'PUT','class'=>'form-horizontal form-bordered'])!!}

                            <div class="form-body">

                                <div class="form-group">
                                    <label for="short_description" class="control-label col-md-2">
                                        @lang("backend/AccessGroup.view.form.Name")
                                    </label>

                                    <div class="col-md-3">
                                        <div class="input-group">
                                            {{$data['name']}}
                                        </div>
                                    </div>

                                    <label for="alias" class="control-label col-md-2">
                                        @lang("backend/AccessGroup.view.form.TypeAccess")</label>

                                    <div class="col-md-3">
                                        {!!trans('backend/AccessGroup.view.form.'.$data['type'])!!}
                                    </div>
                                </div>


                                @if(isset($actionList)&&$actionList)

                                    <div class="row">
                                        <div class="col-md-12">
                                            <!-- BEGIN PORTLET-->
                                            <div class="portlet box green-haze">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <i class="fa fa-gift"></i>@lang("backend/AccessGroup.view.form.actionList")
                                                    </div>
                                                </div>
                                                <div class="portlet-body form">
                                                    <!-- BEGIN FORM-->
                                                    <div class="form-body">

                                                        <?php $counter = 0; $countT = 0; ?>
                                                        @foreach($actionList as $item)
                                                            <?php $countT++; ?>

                                                            @if(!$counter)
                                                                <div class="form-group">
                                                                    @endif

                                                                    <div class="col-md-1"> {{$countT}}.
                                                                        <input type="checkbox" name="action_ids[]"
                                                                               @if(isset($actionActiveList) && in_array($item->id,$actionActiveList)) checked
                                                                               @endif
                                                                               value="{!!$item->id!!}"
                                                                               class="make-switch" data-size="small">
                                                                    </div>
                                                                    <label class="control-label col-md-2"
                                                                           style="direction: ltr">
                                                                        {!!$item->prefix!!}, {!!$item->controller!!}
                                                                        <br/>
                                                                        <b>{!!$item->function.' ('.$item->method.') '!!}</b>
                                                                    </label>


                                                                    <?php $counter++; ?>
                                                                    @if($counter==4)
                                                                        <?php $counter = 0; ?>
                                                                </div>
                                                            @endif
                                                        @endforeach


                                                    </div>
                                                    <!-- END FORM-->
                                                </div>
                                            </div>
                                            <!-- END PORTLET-->
                                        </div>
                                    </div>


                                @endif


                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-6">
                                        <button type="submit" class="btn purple"><i
                                                    class="fa fa-check"></i>{!! $btnTitle !!}</button>
                                        <button type="button"
                                                class="btn default">@lang("backend/AttachType.view.form.Cancel")</button>
                                    </div>
                                </div>
                            </div>
                            {!!Form::close()!!}
                        @endif

                    </div>
                </div>
            </div>
        </div>

    @endif


@endsection
@section('scripts-plugin')
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootbox/bootbox.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/select2/select2.min.js') }}"
            type="text/javascript"></script>
@endsection
@section('scripts')
    <script src="{{ asset('/assets/js/backend/billing.js') }}" type="text/javascript"></script>

@endsection
@section('scripts-inline')
    <script>
        //    console.log(arrayId);
        jQuery(document).ready(function () {
            fnSelect2();

            var initPickers = function () {
                //init date pickers
                $('.date-picker').datepicker({
                    rtl: Metronic.isRTL(),
                    autoclose: true
                });
            }

//            initPickers();

        });
    </script>
    <script>
        jQuery(document).ready(function () {

        });
    </script>
@endsection

