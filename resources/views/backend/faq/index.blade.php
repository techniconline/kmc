@extends('backend.layouts.default')
@section('styles-plugin')

@endsection
@section('styles')

@endsection
@section('content')
    <a class="btn btn-lg blue" href="{!! URL::route($locale.'backend.faq.create') !!}">
        <i class="fa fa-file-o"></i>
        @lang("backend/Faq.view.index.Create_New_FAQ")
    </a>
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN SAMPLE TABLE PORTLET-->
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-bell-o"></i>@lang("backend/Faq.view.index.List")
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse">
                        </a>
                        <a href="#portlet-config" data-toggle="modal" class="config">
                        </a>
                        <a href="javascript:;" class="reload">
                        </a>
                        <a href="javascript:;" class="remove">
                        </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-scrollable">
                        <table class="table table-striped table-bordered table-advance table-hover">
                            <thead>
                            <tr>
                                <th>
                                    <i class="fa fa-question-circle"></i>@lang("backend/Faq.view.index.Question")
                                </th>
                                <th>
                                    <i class="fa fa-question-circle"></i>@lang("backend/Faq.view.index.Type")
                                </th>
                                <th>
                                    <i class="fa fa-check"></i>@lang("backend/Faq.view.index.Answer")
                                </th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($faqs as $faq)
                                <tr class="row-item">
                                    <td>
                                        <a href="{!! URL::route($locale.'backend.faq.edit',$faq->id) !!}">
                                            {!! $faq->question  or trans("backend/Faq.view.index.No_Question") !!}
                                        </a>
                                    </td>
                                    <td class="hidden-xss">
                                        {!! $faq->type !!}
                                    </td>
                                    <td class="hidden-xss">
                                        {!! $faq->answer or trans("backend/Faq.view.index.No_Answer") !!}
                                    </td>
                                    <td>
                                        <a title="@lang("backend/Faq.view.index.Edit")"
                                           href="{!! URL::route($locale.'backend.faq.edit',$faq->id) !!}"
                                           class="btn default btn-xs purple">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        {!! Form::open(['route' => [$locale.'backend.faq.destroy',$faq->id],'method' => 'DELETE','style'=>'display:inline']) !!}
                                        <button type="submit" title="@lang("backend/Faq.view.index.Delete")"
                                                class="btn default btn-xs red delete">
                                            <i class="fa fa-times"></i>
                                        </button>
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END SAMPLE TABLE PORTLET-->
        </div>
    </div>

@endsection
@section('scripts-plugin')

@endsection
@section('scripts')

@endsection
@section('scripts-inline')
    <script>
        jQuery(document).ready(function () {

        });
    </script>
@endsection
