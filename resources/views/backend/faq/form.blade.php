@extends('backend.layouts.default')
@section('styles-plugin')
    <link href="{{ asset('/assets/theme/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css') }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/theme/assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css') }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/theme/assets/global/plugins/bootstrap-summernote/summernote.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection
@section('styles')

@endsection
@section('content')
    <a href="{!! URL::route($locale.'backend.faq.index') !!}" class="btn btn-lg red">
        <i class="fa fa-backward"></i>
        @lang("backend/Faq.view.form.Back")
    </a>
    @if(isset($error))
        @foreach($error as $item)
            <div class="errors">{!!$item!!}</div>
        @endforeach
    @else
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXTRAS PORTLET-->
                <div class="portlet box blue-hoki">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cogs"></i>
                            @if(isset($faq))
                                @lang("backend/Faq.view.form.Edit_FAQ")
                            @else
                                @lang("backend/Faq.view.form.Create_FAQ")
                            @endif
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">
                            </a>
                            <a href="#portlet-config" data-toggle="modal" class="config">
                            </a>
                            <a href="javascript:;" class="reload">
                            </a>
                            <a href="javascript:;" class="remove">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        @if(isset($faq))
                            {!!Form::open(['route' => [$locale.'backend.faq.update',$faq['id']],'method' => 'PUT','class'=>'form-horizontal form-bordered'])!!}
                            {!!Form::hidden('faq_id',$faq['id'] or null,['id'=>'faq_id'])!!}
                        @else
                            {!!Form::open(['route' => $locale.'backend.faq.store','class'=>'form-horizontal form-bordered'])!!}
                        @endif
                        <div class="form-body">
                            <div class="form-group">
                                {!!Form::label('question',trans("backend/Faq.view.form.Question"),['class'=>'control-label col-md-2'])!!}
                                <div class="col-md-10">
                                    {!! Form::text('question',isset($faq['question'])?$faq['question']:null,array('class'=>'form-control')) !!}
                                    {!!$errors->first('question','<span class="error">:message</span>')!!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!!Form::label('type',trans("backend/Faq.view.form.Type"),['class'=>'control-label col-md-2'])!!}
                                <div class="col-md-10">
                                    {!!Form::select('type', trans("faq.view.index.FAQ_TOTAL_DATA_TYPE"),isset($faq['type'])?$faq['type']:null
                                            ,['class'=>'btn form-control input-small select2me','id'=>'type','required'=>'','style'=>'text-align:left'])!!}
                                    {!!$errors->first('type','<span class="error">:message</span>')!!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!!Form::label('answer',trans("backend/Faq.view.form.Answer"),['class'=>'control-label col-md-2'])!!}
                                <div class="col-md-10">
                                    {!!Form::textarea('answer',isset($faq['answer'])?$faq['answer']:null,array('class'=>'form-control'))!!}
                                    {!!$errors->first('answer','<span class="error">:message</span>')!!}
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-6">
                                    {!!Form::select('lang', $listLang,app('activeLangDetails')['lang'],['class'=>'btn form-control input-small select2me'])!!}
                                    <button type="submit" class="btn purple"><i
                                                class="fa fa-check"></i>{!! isset($faq)? trans("backend/Faq.view.form.Update") : trans("backend/Faq.view.form.Create") ; !!}
                                    </button>
                                    <button type="button"
                                            class="btn default">@lang("backend/Faq.view.form.Cancel")</button>
                                </div>
                            </div>
                        </div>

                        {!!Form::close()!!}

                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection

@section('scripts-plugin')
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-markdown/lib/markdown.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-summernote/summernote.min.js') }}"
            type="text/javascript"></script>
@endsection

@section('scripts')
    <script src="{{ asset('/assets/theme/assets/admin/pages/scripts/components-editors.js') }}"
            type="text/javascript"></script>
@endsection

@section('scripts-inline')

    <script>
        jQuery(document).ready(function () {
            ComponentsEditors.init();
        });
    </script>
@endsection