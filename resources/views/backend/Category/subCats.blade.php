@extends('backend.layouts.default')
@section('styles-plugin')

@endsection
@section('styles')
    <link href="{{ asset('/assets/theme/assets/global/plugins/jquery-nestable/jquery.nestable.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('content')
    <a href="{!! URL::route($locale.'backend.category.index') !!}" class="btn btn-lg red">
        <i class="fa fa-backward"></i>
        Back
    </a>

    {!! CategoryHelper::createBoxTreeForNestable($subCats,$translatedName,$myRoot->root_id,'category',$locale) !!}


@endsection
@section('scripts-plugin')

    <script src="{{ asset('/assets/theme/assets/global/plugins/jquery-nestable/jquery.nestable.js') }}"
            type="text/javascript"></script>

@endsection
@section('scripts')

    <script src="{{ asset('/assets/theme/assets/admin/pages/scripts/ui-nestable.js') }}"
            type="text/javascript"></script>

@endsection
@section('scripts-inline')

    {{--{!! Html::script('assets/backend/js/sortable.js') !!}--}}
    <script>
        jQuery(document).ready(function () {

            UINestable.init();
        });
    </script>

@endsection

