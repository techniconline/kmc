@extends('backend.layouts.default')
<?php  $langDetails = app('activeLangDetails');?>

@section('styles-plugin')
    @if($langDetails['is_rtl'])
        <link href="{{ asset('/assets/theme/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5-rtl.css') }}"
              rel="stylesheet" type="text/css"/>
    @else
        <link href="{{ asset('/assets/theme/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css') }}"
              rel="stylesheet" type="text/css"/>
    @endif
    <link href="{{ asset('/assets/theme/assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css') }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/theme/assets/global/plugins/bootstrap-summernote/summernote.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection
@section('styles')

@endsection
@section('content')
    <a href="{!! URL::route($locale.'backend.category.index') !!}" class="btn btn-lg red-sunglo">
        <i class="fa fa-backward"></i>
        @lang("backend/Product.view.create.Back")
    </a>
    @if(isset($error))

        @foreach($error as $item)
            <div class="errors">{!!$item!!}</div>
        @endforeach

    @else
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXTRAS PORTLET-->
                <div class="portlet box blue-hoki">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cogs"></i>{!! $category->name !!}
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">
                            </a>
                            <a href="#portlet-config" data-toggle="modal" class="config">
                            </a>
                            <a href="javascript:;" class="reload">
                            </a>
                            <a href="javascript:;" class="remove">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        {!!Form::open(['route' => [$locale.'backend.category.update',$category['id']],'method' => 'PUT','class'=>'form-horizontal form-bordered','files'=> true ])!!}
                        <div class="form-body">
                            <div class="form-group">
                                {!!Form::label('name','Name',['class'=>'control-label col-md-2'])!!}
                                <div class="col-md-10">
                                    {!!Form::text('name',$category['name'],['class'=>'form-control'])!!}
                                    {!!$errors->first('name','<span class="error">:message</span>')!!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!!Form::label('alias','Alias',['class'=>'control-label col-md-2'])!!}
                                <div class="col-md-10">
                                    {!!Form::text('alias',$category['alias'],['class'=>'form-control'])!!}
                                    {!!$errors->first('alias','<span class="error">:message</span>')!!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!!Form::label('desc','Description',['class'=>'control-label col-md-2'])!!}
                                <div class="col-md-10">
                                    {!! Form::textarea('desc', $category['desc'], ['class' => 'wysihtml5 form-control', 'rows'=>6]) !!}
                                    {!!$errors->first('desc','<span class="error">:message</span>')!!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!!Form::label('image','Image',['class'=>'control-label col-md-2'])!!}
                                <div class="col-md-10">
                                    @if(\Illuminate\Support\Facades\File::exists(base_path().env('PUBLIC_PATH_URL', 'public').'/uploads/images/Categories/'.$category->id.'-300x100.jpg'))
                                        <img src="{{asset('/uploads/images/Categories/'. $category->id .'-300x100.jpg')}}"/>
                                    @endif
                                    {!! Form::file('image') !!}
                                    {!!$errors->first('image','<span class="error">:message</span>')!!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!!Form::label('thumbnail','Thumbnail',['class'=>'control-label col-md-2'])!!}
                                <div class="col-md-10">
                                    {!! Form::text('thumbnail', $category['thumbnail'], ['class' => 'form-control', 'rows'=>6]) !!}
                                    {!!$errors->first('thumbnail','<span class="error">:message</span>')!!}
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-6">
                                    {!! Form::select('lang', $listLang,app('activeLangDetails')['lang'],['class'=>'btn form-control input-small select2me']) !!}
                                    <button type="submit" class="btn green-meadow"><i class="fa fa-check"></i>Update
                                    </button>
                                    <button type="button" class="btn default">Cancel</button>
                                </div>
                            </div>
                        </div>
                        {!!Form::close()!!}
                    </div>
                </div>
            </div>
        </div>


    @endif
@endsection
@section('scripts-plugin')

    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-markdown/lib/markdown.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-summernote/summernote.min.js') }}"
            type="text/javascript"></script>


@endsection
@section('scripts')
    <script src="{{ asset('/assets/theme/assets/admin/pages/scripts/components-editors.js') }}"
            type="text/javascript"></script>

@endsection
@section('scripts-inline')

    <script>
        jQuery(document).ready(function () {

            ComponentsEditors.init();
        });
    </script>
@endsection