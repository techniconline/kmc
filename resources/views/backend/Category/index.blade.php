@extends('backend.layouts.default')
@section('styles-plugin')

@endsection
@section('styles')
    <link href="{{ asset('/assets/theme/assets/global/plugins/jquery-nestable/jquery.nestable.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection
@section('content')

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXTRAS PORTLET-->
            <div class="portlet box green-meadow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>Create New Category
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse">
                        </a>
                        <a href="#portlet-config" data-toggle="modal" class="config">
                        </a>
                        <a href="javascript:;" class="reload">
                        </a>
                        <a href="javascript:;" class="remove">
                        </a>
                    </div>
                </div>
                <div class="portlet-body form">

                    {!! Form::open(['route' => $locale.'backend.category.store','class'=>'form-horizontal form-bordered']) !!}
                    <div class="form-body">

                        <div class="form-group">
                            {!!Form::label('title','New Category',['class'=>'control-label col-md-2'])!!}
                            <div class="col-md-6">
                                {!! Form::text('newCat','',['placeholder' => 'New Category' , 'id' => 'newCat','class'=>'form-control']) !!}
                                {!!$errors->first('newCat','<span class="error">:message</span>')!!}
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-6">
                                <button type="submit" class="btn green-meadow"><i class="fa fa-check"></i>Save</button>
                                <button type="button" class="btn default">Cancel</button>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
    {!! CategoryHelper::createBoxTreeForNestable($roots,$translatedName,0,'category',$locale, trans("backend/Category.view.index.Categories_List"), true, false,false) !!}

@endsection
@section('scripts-plugin')

    <script src="{{ asset('/assets/theme/assets/global/plugins/jquery-nestable/jquery.nestable.js') }}"
            type="text/javascript"></script>

@endsection
@section('scripts')

    <script src="{{ asset('/assets/theme/assets/admin/pages/scripts/ui-nestable.js') }}"
            type="text/javascript"></script>

@endsection
@section('scripts-inline')
    <script>
        jQuery(document).ready(function () {

            UINestable.init();


            $('form').fadeIn();

        });
    </script>
    <script>
        $('#autoAlias').on('change', function () {
            if (this.checked) {
                $("#alias").remove();
            } else {
                $("form").append('{!! Form::text('alias','',['placeholder' => 'Alias' , 'id' => 'alias']) !!}');
            }
        });
    </script>

@endsection