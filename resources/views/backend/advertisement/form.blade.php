@extends('backend.layouts.default')
<?php  $langDetails = app('activeLangDetails');?>

@section('styles-plugin')
    @if($langDetails['is_rtl'])
        <link href="{{ asset('/assets/theme/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5-rtl.css') }}"
              rel="stylesheet" type="text/css"/>
    @else
        <link href="{{ asset('/assets/theme/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css') }}"
              rel="stylesheet" type="text/css"/>
    @endif
    <link href="{{ asset('/assets/theme/assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css') }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/theme/assets/global/plugins/bootstrap-summernote/summernote.css') }}" rel="stylesheet"
          type="text/css"/>

    {{--    {!!Html::loadEditor()!!}--}}

@endsection
@section('styles')

@endsection
@section('content')
    <a href="{!! URL::route($locale.'backend.advert.index') !!}" class="btn btn-lg red">
        <i class="fa fa-backward"></i>
        @lang("backend/Advertisement.view.form.Back")
    </a>
    @if(isset($error))

        @foreach($error as $item)
            <div class="errors">{!!$item!!}</div>
        @endforeach

    @else
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXTRAS PORTLET-->
                <div class="portlet box blue-hoki">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cogs"></i>@lang("backend/Advertisement.view.form.Create_New")
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">
                            </a>
                            <a href="#portlet-config" data-toggle="modal" class="config">
                            </a>
                            <a href="javascript:;" class="reload">
                            </a>
                            <a href="javascript:;" class="remove">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        @if(isset($advertisement))
                            {!!Form::open(['route' => [$locale.'backend.advert.update',$advertisement['id']],'method' => 'PUT','class'=>'form-horizontal form-bordered','files'=> true])!!}
                            {!!Form::hidden('content_id',$advertisement['id'] or null,['id'=>'content-id'])!!}
                        @else
                            {!!Form::open(['route' => $locale.'backend.advert.store','class'=>'form-horizontal form-bordered','files'=> true])!!}
                        @endif
                        <div class="form-body">
                            <div class="form-group">
                                {!!Form::label('title',trans("backend/Advertisement.view.form.Title"),['class'=>'control-label col-md-2'])!!}
                                <div class="col-md-10">
                                    {!!Form::text('title',isset($advertisement['title'])?$advertisement['title']:null,array('class'=>'form-control'))!!}
                                    {!!$errors->first('title','<span class="error">:message</span>')!!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!!Form::label('file_path',trans("backend/Advertisement.view.form.File"),['class'=>'control-label col-md-2'])!!}
                                <div class="col-md-4">
                                    {!! Form::file('file_path',['onchange'=>"readURL(this)"]) !!}
                                    {!!$errors->first('file_path','<span class="error">:message</span>')!!}
                                </div>
                                <div class="col-md-4">
                                    @if(isset($advertisement) && \Illuminate\Support\Facades\File::exists(base_path().env('PUBLIC_PATH_URL', 'public').$advertisement['file_path']))
                                        <img class="file_path col-md-12" src="{{$advertisement['file_path']}}"
                                             alt="Image" width="160px" height="80px"/>
                                    @else
                                        <img class="file_path col-md-12" src="#" alt="Image"/>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                {!!Form::label('url',trans("backend/Advertisement.view.form.URL"),['class'=>'control-label col-md-2'])!!}
                                <div class="col-md-10">
                                    {!!Form::text('url',isset($advertisement['url'])?$advertisement['url']:null,array('class'=>'form-control'))!!}
                                    {!!$errors->first('url','<span class="error">:message</span>')!!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="short_description"
                                       class="control-label col-md-2">@lang("backend/Advertisement.view.form.short_description")</label>

                                <div class="col-md-10">
                                    <textarea name="short_description" id="short_description"
                                              class="wysihtml5 form-control"
                                              rows="2">{!! isset($advertisement['short_description'])?$advertisement['short_description']:null !!}</textarea>
                                    {!!$errors->first('short_description','<span class="error">:message</span>')!!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="description"
                                       class="control-label col-md-2">@lang("backend/Advertisement.view.form.description")</label>

                                <div class="col-md-10">
                                    <textarea name="description" class="form-control" id="description"
                                              rows="4">{!! isset($advertisement['description'])?$advertisement['description']:null !!}</textarea>
                                    {!!$errors->first('description','<span class="error">:message</span>')!!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!!Form::label('position',trans("backend/Advertisement.view.form.position"),['class'=>'control-label col-md-2'])!!}
                                <div class="col-md-2">
                                    {!! Form::select('position'
                                    ,['home_slider'=>trans("backend/Advertisement.view.form.home_slider")
                                        ,'right_advert_home'=>trans("backend/Advertisement.view.form.right_advert_home")
                                        ,'center_advert_home_footer'=>trans("backend/Advertisement.view.form.center_advert_home_footer")
                                        ]
                                        ,isset($advertisement['position'])?$advertisement['position']:'home_slider',array('class'=>'form-control input-medium')) !!}
                                    {!!$errors->first('position','<span class="error">:message</span>')!!}
                                </div>

                                {!!Form::label('group_show',trans("backend/Advertisement.view.form.Access"),['class'=>'control-label col-md-2'])!!}
                                <div class="col-md-2">
                                    {!! Form::select('group_show',['public'=>trans("backend/Advertisement.view.form.Public"),'member'=>trans("backend/Advertisement.view.form.Members")],isset($advertisement['group_show'])?$advertisement['group_show']:'public',array('class'=>'form-control input-small')) !!}
                                    {!!$errors->first('group_show','<span class="error">:message</span>')!!}
                                </div>

                                {!!Form::label('type',trans("backend/Advertisement.view.form.Type"),['class'=>'control-label col-md-2'])!!}
                                <div class="col-md-2">
                                    {!! Form::select('type',['image'=>trans("backend/Advertisement.view.form.Image")]
                                    ,isset($advertisement['type'])?$advertisement['type']:'image',array('class'=>'form-control input-small')) !!}
                                    {!!$errors->first('type','<span class="error">:message</span>')!!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!!Form::label('slider_option',trans("backend/Advertisement.view.form.slider_option"),['class'=>'control-label col-md-2'])!!}
                                <div class="col-md-10">
                                    {!!Form::text('slider_option',isset($advertisement['slider_option'])?$advertisement['slider_option']:null,array('class'=>'form-control'))!!}
                                    {!!$errors->first('slider_option','<span class="error">:message</span>')!!}
                                </div>
                            </div>

                            <div class="form-group" id="body_contents">
                                <label for="body_content"
                                       class="control-label col-md-2"><a
                                            class="btn btn-circle-bottom green add_body_content">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                    @lang("backend/Advertisement.view.form.Body_Contents")
                                </label>


                                <div class="col-md-10 body_contents">
                                    @if(isset($advertisement['body_content']))
                                        <?php $body_content = json_decode($advertisement['body_content']); $counter = 0; ?>
                                        @foreach($body_content as $key => $value)
                                            <?php $counter++; ?>
                                            <div>
                                    <textarea name="body_content[]"
                                              class="form-control"
                                              id="body_content_{{$counter}}">{!! $value !!}</textarea>
                                                <a class="btn btn-bottom red remove_body_content"><i
                                                            class="fa fa-times"></i></a>
                                            </div>
                                            <script>
                                                jQuery(document).ready(function () {
                                                    ComponentsEditors.notePad('body_content_{{$counter}}', 100);
                                                })
                                            </script>
                                        @endforeach
                                    @else
                                        <div>
                                    <textarea name="body_content[]"
                                              class="form-control" id="body_content_1"></textarea>
                                        </div>
                                    @endif
                                </div>
                            </div>

                            {{--<div class="form-group">--}}
                            {{--{!!Form::label('body_content',trans("backend/Advertisement.view.form.Body_Contents"),['class'=>'control-label col-md-2'])!!}--}}
                            {{--<div class="col-md-10">--}}
                            {{--{!!Form::editor('body_content',isset($advertisement['body_content'])?$advertisement['body_content']:null,'')!!}--}}
                            {{--<textarea name="body_content" class="wysihtml5 form-control" rows="11">{!! isset($advertisement['body_content'])?$advertisement['body_content']:null !!}</textarea>--}}
                            {{--{!!$errors->first('body_content','<span class="error">:message</span>')!!}--}}
                            {{--</div>--}}
                            {{--</div>--}}

                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-6">
                                    {!!Form::select('lang', $listLang,app('getLocaleDefault'),['class'=>'btn form-control input-small select2me'])!!}
                                    <button type="submit" class="btn purple"><i
                                                class="fa fa-check"></i>{!! isset($advertisement)? trans("backend/Advertisement.view.form.Update") : trans("backend/Advertisement.view.form.Create")  !!}
                                    </button>
                                    <button type="button"
                                            class="btn default">@lang("backend/Advertisement.view.form.Cancel")</button>
                                </div>
                            </div>
                        </div>

                        {!!Form::close()!!}

                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection

@section('scripts-plugin')
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js') }}"
            type="text/javascript"></script>
    {{--    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-markdown/lib/markdown.js') }}" type="text/javascript"></script>--}}
    {{--<script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js') }}" type="text/javascript"></script>--}}
    <script src="{{ asset('/assets/theme/assets/global/plugins/bootstrap-summernote/summernote.min.js') }}"
            type="text/javascript"></script>
@endsection

@section('scripts')
    <script src="{{ asset('/assets/theme/assets/admin/pages/scripts/components-editors.js') }}"
            type="text/javascript"></script>
@endsection

@section('scripts-inline')

    <script>
        jQuery(document).ready(function () {
            // initiate layout and plugins

//            ComponentsEditors.init();
            ComponentsEditors.notePad('short_description', 100);
            ComponentsEditors.notePad('description', 150);
            @if(!isset($advertisement['body_content']))
            ComponentsEditors.notePad('body_content_1', 100);
            @endif

        $('div#body_contents').on('click', 'a.add_body_content', function (event) {
                event.preventDefault();
                var clicked = $(this);
                var parent = clicked.parents('#body_contents').first();
                var countTextArea = parent.find('textarea');
                console.log(countTextArea.length);
                countTextArea = countTextArea.length
                var $textArea = $('<div><textarea name="body_content[]"  class="form-control" id="body_content_' + countTextArea + '"></textarea><a class="btn btn-bottom red remove_body_content"><i class="fa fa-times"></i></a></div>')
                parent.find('div.body_contents').append($textArea);
                ComponentsEditors.notePad('body_content_' + countTextArea, 100);

            });

            $('div#body_contents').on('click', 'a.remove_body_content', function (event) {
                event.preventDefault();
                var clicked = $(this);
                var parent = clicked.parents('div').first();
                parent.remove();

            });


        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('.file_path').attr('src', e.target.result)
                            .css("width", "160px")
                            .css("height", "80px");
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>

@endsection