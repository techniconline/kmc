@extends('backend.layouts.default')
@section('styles-plugin')

@endsection
@section('styles')

@endsection
@section('content')
    {{--@if (Session::has('flash_notification.message'))--}}
    {{--<div class="alert alert-{{ Session::get('flash_notification.level') }}">--}}
    {{--<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>--}}

    {{--{{ Session::get('flash_notification.message') }}--}}
    {{--</div>--}}
    {{--@endif--}}
    <a class="btn btn-lg blue" href="{!! URL::route($locale.'backend.advert.create') !!}">
        <i class="fa fa-file-o"></i>
        @lang("backend/Advertisement.view.index.Create_New")
    </a>
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN SAMPLE TABLE PORTLET-->
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-bell-o"></i>@lang("backend/Advertisement.view.index.List")
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse">
                        </a>
                        <a href="#portlet-config" data-toggle="modal" class="config">
                        </a>
                        <a href="javascript:;" class="reload">
                        </a>
                        <a href="javascript:;" class="remove">
                        </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-scrollable">
                        <table class="table table-striped table-bordered table-advance table-hover">
                            <thead>
                            <tr>
                                <th>
                                    <i class="fa fa-info"></i> @lang("backend/Advertisement.view.index.Title")
                                </th>

                                <th>
                                    <i class="fa fa-globe"></i> @lang("backend/Advertisement.view.index.URL")
                                </th>

                                <th>
                                    <i class="fa fa-ban"></i> @lang("backend/Advertisement.view.index.Access")
                                </th>

                                <th>
                                    <i class="fa fa-list"></i> @lang("backend/Advertisement.view.form.Type")
                                </th>

                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($advertisements as $content)
                                <tr class="row-item">
                                    <td class="highlight">
                                        <div class="success">
                                        </div>
                                        <a href="{!! URL::route($locale.'backend.advert.edit',$content->advertisement_id) !!}">
                                            {!! $content->title  or 'No Title' !!}
                                        </a>
                                    </td>

                                    <td>
                                        {!! $content->url or 'No Url' !!}
                                    </td>

                                    <td>
                                        {!! ($content->group_show)  !!}
                                    </td>

                                    <td>
                                        {!! ($content->type )  !!}
                                    </td>

                                    <td>
                                        <a href="{!! URL::route($locale.'backend.advert.edit',$content->advertisement_id) !!}"
                                           class="btn default btn-xs purple">
                                            <i class="fa fa-edit"></i></a>
                                        {!! Form::open(['route'=> [$locale.'backend.advert.destroy',$content->advertisement_id], 'method' => 'delete']) !!}
                                        <button type="submit" class="btn default btn-xs red delete">
                                            <i class="fa fa-times"></i></a>
                                        </button>
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END SAMPLE TABLE PORTLET-->
        </div>
    </div>

@endsection
@section('scripts-plugin')

@endsection
@section('scripts')
    <script src="{{ asset('/assets/js/backend/apartment.js') }}" type="text/javascript"></script>

@endsection
@section('scripts-inline')
    <script>
        jQuery(document).ready(function () {

        });
    </script>
@endsection
