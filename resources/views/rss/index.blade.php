<rss xmlns:a10="http://www.w3.org/2005/Atom" version="2.0">
    <channel>
        <title>KMC Feed</title>
        <link>
        http://www.kmc.com</link>
        <updated>{!! \Carbon\Carbon::now()->toATOMString() !!}</updated>
        <language>{!! app('activeLangDetails')['lang'] !!}</language>

        @foreach($news as $new)
            <item>
                <title>{!! $new->link_title !!}</title>
                <link>{!! route('news.show',$new->url) !!}</link>
                <description>{!! $new->body_title !!}</description>
            </item>
        @endforeach

    </channel>
</rss>