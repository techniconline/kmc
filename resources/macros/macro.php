<?php

Form::macro('editor', function ($id, $value, $atts) {
    if (!$value) {
        $value = Form::getValueAttribute($id);
    }

    $attribute = '';
    if ($atts) {
        foreach ($atts as $key => $val) {
            $attribute .= $key . '=' . $val;
        }
    }

    //$dom = Form::textarea($id,$value,$atts);
    $dom = '<textarea name="' . $id . '" id="' . $id . '" cols="50" rows="10" ' . $attribute . '>' . $value . '</textarea>';
    $dom .= '<script>
            jQuery(document).ready(function() {
                CKEDITOR.replace(' . $id . ');
            });
            </script>';
    return $dom;
});

Html::macro('loadEditor', function () {
    return Html::script('/assets/ck/ckeditor.js');
});