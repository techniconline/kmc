<?php

/**
 * Created by PhpStorm.
 * User: Hamid
 * Date: 13/01/2015
 * Time: 09:03 AM
 */
namespace App;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;

class AttachDocument extends BaseModel
{

    protected $table = 'attach_documents';
    protected $primaryKey = 'id';
    protected $fillable = ['media_id', 'attach_type_id', 'booking_id', 'document_status', 'user_id', 'status'];

    protected $hidden = ['remember_token'];
    public $errors;
    public $rules = ['media_id' => 'required|numeric', 'attach_type_id' => 'required|numeric', 'booking_id' => 'required|numeric'];

    /**
     * @return bool
     */
    public function isValid()
    {
        $validation = Validator::make($this->attributes, $this->rules);
        if ($validation->fails()) {
            $this->errors = $validation->messages();
            return false;
        }
        return true;
    }

    public function insertDocument($insertArr)
    {
        $result = DB::table($this->table)->insert(
            $insertArr
        );

        return $result;
    }

    public function updateDocuments($params)
    {
        $user_id = $params['user_id'];
        $attach_type_ids = $params['attach_type_ids'];
        $booking_id = $params['booking_id'];
        if (!$user_id || !$attach_type_ids || !$booking_id)
            return false;

        $result = DB::table($this->table)
            ->whereIn('attach_type_id', $attach_type_ids)
            ->where('user_id', '=', $user_id)
            ->where('booking_id', '=', $booking_id)
            ->update(
                [
                    'status' => 0
                    , 'document_status' => 0
                    , 'updated_at' => date('Y-m-d H:i:s')
                ]
            );

        return $result;

    }

    public function getCountOfAttachTypeInGroup($user_id = 0, $booking_id = 0, $groupId = 0)
    {

        $results = DB::table($this->table)
            ->join('attach_type', 'attach_documents.attach_type_id', '=', 'attach_type.id')
            ->join('attach_type_group', 'attach_type.group_id', '=', 'attach_type_group.id')
            ->select(DB::raw('count(attach_type.id) as countUploaded, attach_type_group.id'))
            ->where('attach_type.status', '=', 1)
            ->where('attach_documents.status', '=', 1)
            ->whereIn('attach_documents.document_status', [1, 4])
            ->where('attach_type_group.status', '=', 1)
            ->where(function ($query) use ($booking_id) {
                if ($booking_id && $query) {
                    $query->where('attach_documents.booking_id', '=', $booking_id);
                }
            })
            ->where(function ($query) use ($user_id) {
                if ($user_id && $query) {
                    $query->where('attach_documents.user_id', '=', $user_id);
                }
            })
            ->where(function ($query) use ($groupId) {
                if ($groupId && $query) {
                    $query->where('attach_type.group_id', '=', $groupId);
                }
            })
            ->groupBy('attach_type_group.id')
            ->orderBy('group_id')
            ->orderBy('attach_documents.booking_id')
            ->get();


        return $results;
    }

    public function addCompletedGroupAttach($dataAttachGroup, $dataCount)
    {
        foreach ($dataAttachGroup as &$item) {
            if ($item->max_upload) {
                foreach ($dataCount as $itemCount) {
                    if ($item->id == $itemCount->id && $item->max_upload <= $itemCount->countUploaded) {
                        $item->completed_group = 1;
                    }
                }
            }
        }

        return $dataAttachGroup;
    }

} 