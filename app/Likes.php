<?php

/**
 * Created by PhpStorm.
 * User: Hamid
 * Date: 13/01/2015
 * Time: 09:03 AM
 */
namespace App;

use App\Providers\Helpers\PersianDate\PersianDate;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\File;

class Likes extends BaseModel
{

    protected $table = 'likes';
    protected $fillable = [
        'system',
        'item_id',
        'user_id',
        'status',
    ];
//
    public $errors;
    public $rules = [
        'item_id' => 'required',
        'system' => 'required',
        'status' => 'required',
    ];


    public function getLikesItem($system = 'comment', $item_id)
    {

        $result = $this->where('system', $system)
            ->where('item_id', $item_id)
            ->select(DB::raw('CONCAT_WS(":",IF(status=1,"like","dislike"),COUNT(id)) as counter'))
            ->groupBy('status')
            ->get();

        $newReturn = [];
        foreach ($result as $item) {
            $arr = explode(":", $item->counter);
            $newReturn[$arr[0]] = $arr[1];
        }

        return $newReturn;

    }


} 