<?php

/**
 * Created by PhpStorm.
 * User: Hamid
 * Date: 13/01/2015
 * Time: 09:03 AM
 */
namespace App;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;

class RoomCategory extends BaseModel
{

    protected $table = 'room_category';
    protected $primaryKey = 'id';
    protected $fillable = ['area', 'type', 'user_id', 'status'
        , 'room_number', 'floor_number', 'room_category_id', 'apartment_id', 'price', 'to_area', 'number_room' /* <- tedade otagh khab ha hastesh*/
    ];

    protected $hidden = ['remember_token'];
    public $errors;
    public $rules = ['area' => 'required|numeric'
        , 'to_area' => 'required|numeric'
        , 'type' => 'required'
    ];

    public $rulesDetails = ['room_number' => 'required|numeric'
        , 'floor_number' => 'required|numeric'
        , 'room_category_id' => 'required|numeric'
//        , 'price' => 'required|numeric'
        , 'apartment_id' => 'required|numeric'];

    public function isValid()
    {
        $validation = Validator::make($this->attributes, $this->rules);
        if ($validation->fails()) {
            $this->errors = $validation->messages();
            return false;
        }
        return true;
    }


    public function isValidDetails()
    {
        $validation = Validator::make($this->attributes, $this->rulesDetails);
        if ($validation->fails()) {
            $this->errors = $validation->messages();
            return false;
        }
        return true;
    }

    public function saveDetails($data)
    {
        $user_id = Auth::employee()->get()->id ? Auth::employee()->get()->id : 1;
        if (!$user_id || !$data->room_category_id || !$data->apartment_id)
            return 0;

        $date = date('Y-m-d H:i:s');
        $insertItem = '';
        $insertItem = ['floor_number' => $data->floor_number, 'room_number' => $data->room_number
            , 'apartment_id' => $data->apartment_id, 'room_category_id' => $data->room_category_id
            , 'created_at' => $date, 'user_id' => $user_id, 'status' => 1];

        $resultPrice = '';
        if ($insertItem) {
            $id = DB::table('apartment_room_category_map')->insertGetId($insertItem);
            return array('apartment_room_category_map_id' => $id, 'resultPrice' => $resultPrice);
        }

        return false;

    }

    public function updateDetails($apartment_room_category_map_id, $data)
    {
        $fill = $this->fill($data);
        $isValid = $fill->isValidDetails();
        if (!$isValid) {
            return array('action' => false, 'message' => $this->errors);
        }

        $Input['floor_number'] = $data['floor_number'];
        $Input['room_number'] = $data['room_number'];
        $Input['apartment_id'] = $data['apartment_id'];
        $Input['user_id'] = Auth::employee()->get()->id;
        $Input['updated_at'] = date('Y-m-d H:i:s');
        $result = DB::table('apartment_room_category_map')
            ->where('id', $apartment_room_category_map_id)
            ->where('status', 1)
            ->update($Input);

        if ($result) {
            return array('action' => true, 'message' => Lang::get('backend/roomCategory.messages.msgUpdateOk'));
        } else {
            return array('action' => false, 'message' => Lang::get('backend/roomCategory.messages.msgUpdateNotOk'));
        }

    }

    public function getPrice($room_category_id)
    {
        $result = DB::table('apartment_room_price')
            ->where('room_category_id', '=', $room_category_id)
            ->where('price_status', 1)
            ->where('status', 1)
            ->orderBy('id', 'DESC')
            ->limit(1)
            ->get();

        if ($result)
            return $result;

        return false;
    }

    public function addPrice($room_category_id, $price, $to_price)
    {
        $user_id = Auth::employee()->get()->id;
        if (!$user_id)
            return 0;

        $date = date('Y-m-d H:i:s');
        $Input['price_status'] = 0;
        $Input['updated_at'] = $date;
        $Input['user_id'] = $user_id;
        $resultPrice = $this->getPrice($room_category_id)[0];
        $currentPrice = isset($resultPrice->rent_price) ? $resultPrice->rent_price : 0;

        $result = DB::table('apartment_room_price')
            ->where('room_category_id', $room_category_id)
            ->where('price_status', 1)
            ->where('rent_price', '!=', $price)
            ->where('to_rent_price', '!=', $to_price)
            ->where('status', 1)
            ->update($Input);

        $insertItem = '';
        $insertItem = ['room_category_id' => $room_category_id, 'rent_price' => $price, 'to_rent_price' => $to_price
            , 'price_status' => 1, 'created_at' => $date, 'user_id' => $user_id, 'status' => 1];

        if (($insertItem) && ($currentPrice != $price) && ($price) && ($room_category_id)) {
            $id = DB::table('apartment_room_price')->insertGetId($insertItem);
            $message = Lang::get('backend/roomCategory.messages.msgAddPrice');
            if ($result) {
                $message = Lang::get('backend/roomCategory.messages.msgAddPriceWithOld') . $currentPrice;
            }
            return ['valid' => true, 'apartment_room_price_id' => $id, 'message' => $message];
        }
        $message = Lang::get('backend/roomCategory.messages.errPrice');
        return ['valid' => false, 'apartment_room_price_id' => 0, 'message' => $message];
    }

    public function getApartmentRoomCategoryMap($apartment_room_category_map_id = 0, $apartment_id = 0, $room_category_id = 0)
    {
        $language = new Language();
        $lang = $language->getLanguage(app('getLocale'));

        $whereApartment = '';
        $whereCategory = '';
        $whereMap = '';
        if ($apartment_id) {
            $whereApartment = 'AND a.id=' . $apartment_id;
        }

        if ($room_category_id) {
            $whereCategory = 'AND rc.id=' . $room_category_id;
        }

        if ($apartment_room_category_map_id) {
            $whereMap = 'AND arcm.id=' . $apartment_room_category_map_id;
        }

        $result = DB::select('
        SELECT
          COUNT(r.id) AS Count_Room,
          arcm.*,
          al.title AS apartment_title,
          rcl.title AS category_title,
          rc.area,
          rc.to_area,
          rc.type,
          (SELECT rent_price FROM apartment_room_price WHERE room_category_id=rc.id ORDER BY id DESC LIMIT 1) as default_price
        FROM
          apartment_room_category_map AS arcm
          INNER JOIN apartments AS a ON arcm.apartment_id = a.id
          INNER JOIN apartments_lang AS al ON a.id = al.id
          INNER JOIN room_category AS rc ON arcm.room_category_id = rc.id
          INNER JOIN room_category_lang AS rcl ON rcl.id = rc.id
          LEFT JOIN rooms AS r ON r.apartment_room_category_map_id=arcm.id
          AND r.room_status=1 AND r.status=1
        WHERE a.status = 1
          AND rc.status = 1
          AND rc.status = 1
          AND al.lang_id = ' . $lang["lang_id"] . '
          AND rcl.lang_id = ' . $lang["lang_id"] . '
          AND arcm.status = 1
          AND a.status = 1
          ' . $whereMap . '
          ' . $whereCategory . '
          ' . $whereApartment . '
          GROUP BY arcm.id
          ');

        return $result;

    }

    public function del($room_category_id)
    {

        if (!$room_category_id)
            return 0;

        $Input['user_id'] = Auth::employee()->get()->id;
        $Input['updated_at'] = date('Y-m-d H:i:s');
        $Input['status'] = 0;
        $Input['price_status'] = 0;

        //-----price
        $result = DB::table('apartment_room_price')
            ->where('room_category_id', $room_category_id)
            ->where('status', 1)
            ->update($Input);
        unset($Input['price_status']);
        //----------

        $result = DB::table('apartment_room_category_map')
            ->where('room_category_id', $room_category_id)
            ->where('status', 1)
            ->update($Input);

        $result = DB::table('rooms')
            ->where('room_category_id', $room_category_id)
            ->where('status', 1)
            ->update($Input);


        $result = DB::table('room_category')
            ->where('id', $room_category_id)
            ->where('status', 1)
            ->update($Input);

        if ($result) {
            return array('action' => true, 'message' => Lang::get('backend/roomCategory.messages.msgDelOk'));
        } else {
            return array('action' => false, 'message' => Lang::get('backend/roomCategory.messages.msgDelNotOk'));
        }

    }

    public function delDetails($apartment_room_category_map_id)
    {

        if (!$apartment_room_category_map_id)
            return 0;

        $Input['user_id'] = Auth::employee()->get()->id;
        $Input['updated_at'] = date('Y-m-d H:i:s');
        $Input['status'] = 0;
        $result = DB::table('apartment_room_category_map')
            ->where('id', $apartment_room_category_map_id)
            ->where('status', 1)
            ->update($Input);

        if ($result) {
            return array('action' => true, 'message' => Lang::get('backend/roomCategory.messages.msgDelOk'));
        } else {
            return array('action' => false, 'message' => Lang::get('backend/roomCategory.messages.msgDelNotOk'));
        }

    }

    public function getCategoryDetails($room_category_id)
    {

        $result = DB::table('apartment_room_category_map')
            ->leftJoin('apartment_room_price', function ($join) {
                $join->on('apartment_room_category_map.room_category_id', '=', 'apartment_room_price.room_category_id')
                    ->where('apartment_room_price.price_status', '=', 1)
                    ->where('apartment_room_price.status', '=', 1);
            })
            ->where('apartment_room_category_map.room_category_id', '=', $room_category_id)
            ->where('apartment_room_category_map.status', '=', 1)
            ->select(['apartment_room_category_map.*', 'apartment_room_price.rent_price', 'apartment_room_price.to_rent_price'])
            ->orderBy('apartment_room_price.rent_price', 'DESC')
            ->orderBy('apartment_room_category_map.id')
            ->get();
        return $result;
    }

    public function updateCategory($room_category_id, $data, $mediaArr = 0, $mediaDelArr = 0)
    {
        $fill = $this->fill($data);
        $isValid = $fill->isValid();
        if (!$isValid) {
            return array('action' => false, 'message' => $this->errors);
        }

        $Input['area'] = $data['area'];
        $Input['to_area'] = $data['to_area'];
        $Input['number_room'] = $data['number_room'];
        $Input['type'] = $data['type'];
        $Input['user_id'] = Auth::employee()->get()->id;
        $Input['status'] = 1;
        $result = RoomCategory::where('id', $room_category_id)->update($Input);

        $InputLang['title'] = $data['title'];
        $InputLang['lang_id'] = $data['lang_id'];
        $InputLang['description'] = $data['description'];
        $InputLang['short_description'] = $data['short_description'];
        $InputLang['id'] = $room_category_id;
        $resultLang = $this->updateRoomCategoryLang($InputLang);

        if ($result) {
            $mediaModel = new Media();
            if ($mediaArr['images']) {
                $subMediaArr = array('ids' => $mediaArr['images'], 'titles' => $mediaArr['images_title']);
                $mediaModel->addMediaApartment($subMediaArr, 0, $room_category_id);
            }

            if ($mediaArr['videos']) {
                $subMediaArr = array('ids' => $mediaArr['videos'], 'titles' => $mediaArr['videos_title']);
                $mediaModel->addMediaApartment($subMediaArr, 0, $room_category_id);
            }

            if ($mediaDelArr) {
                $mediaModel->removeMedia($mediaDelArr);
            }

            return array('action' => true, 'message' => Lang::get('backend/roomCategory.messages.msgUpdateOk'));
        } else {
            return array('action' => false, 'message' => Lang::get('backend/roomCategory.messages.msgUpdateNotOk'));
        }

    }

    public function getListOfRoomCategory()
    {

        $language = new Language();
        $lang = $language->getLanguage(app('getLocale'));

        $result = DB::table('room_category')
            ->join('room_category_lang', 'room_category.id', '=', 'room_category_lang.id')
            ->leftjoin('apartment_room_price', function ($join) {
                $join->on('room_category.id', '=', 'apartment_room_price.room_category_id')
                    ->where('apartment_room_price.price_status', '=', 1)
                    ->where('apartment_room_price.status', '=', 1);
            })
            ->select('room_category.*', 'room_category_lang.title', 'room_category_lang.short_description'
                , 'room_category_lang.description', 'apartment_room_price.rent_price', 'apartment_room_price.to_rent_price')
            ->where('room_category.status', '=', 1)
            ->where('room_category_lang.lang_id', '=', $lang['lang_id'])
            ->get();

        return $result;

    }

    public function saveRoomCategoryLang($params)
    {
        $lang_id = $params['lang_id'];
        $id = $params['id'];
        $title = $params['title'];
        $description = $params['description'];
        $short_description = $params['short_description'];
        $result = DB::table('room_category_lang')->insert(
            [
                'id' => $id
                , 'title' => $title
                , 'description' => $description
                , 'short_description' => $short_description
                , 'lang_id' => $lang_id
            ]
        );

        return $result;

    }

    public function updateRoomCategoryLang($params)
    {
        $lang_id = $params['lang_id'];
        $id = $params['id'];
        $title = $params['title'];
        $description = $params['description'];
        $short_description = $params['short_description'];
        $result = DB::table('room_category_lang')
            ->where('id', '=', $id)
            ->where('lang_id', '=', $lang_id)
            ->update(
                [
                    'title' => $title
                    , 'description' => $description
                    , 'short_description' => $short_description
                ]
            );

        return $result;

    }


    public function getRoomCategoryMapDetails($apartment_room_category_map_id)
    {

        $result = DB::table('apartment_room_category_map')
            ->join('room_category', 'apartment_room_category_map.room_category_id', '=', 'room_category.id')
            ->leftJoin('apartment_room_price', function ($join) {
                $join->on('apartment_room_category_map.room_category_id', '=', 'apartment_room_price.room_category_id')
                    ->where('apartment_room_price.price_status', '=', 1)
                    ->where('apartment_room_price.status', '=', 1);
            })
            ->where('apartment_room_category_map.id', '=', $apartment_room_category_map_id)
            ->where('apartment_room_category_map.status', '=', 1)
            ->select(['apartment_room_category_map.*', 'apartment_room_price.rent_price', 'apartment_room_price.to_rent_price', 'room_category.type'])
            ->orderBy('apartment_room_category_map.id', 'DESC')
            ->first();

        return $result;
    }

} 