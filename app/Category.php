<?php namespace App;

use Baum\Node;
use Illuminate\Support\Facades\DB;

class Category extends Node
{

    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'categories';

    //////////////////////////////////////////////////////////////////////////////

    //
    // Below come the default values for Baum's own Nested Set implementation
    // column names.
    //
    // You may uncomment and modify the following fields at your own will, provided
    // they match *exactly* those provided in the migration.
    //
    // If you don't plan on modifying any of these you can safely remove them.
    //

    // /**
    //  * Column name which stores reference to parent's node.
    //  *
    //  * @var string
    //  */
    // protected $parentColumn = 'parent_id';

    // /**
    //  * Column name for the left index.
    //  *
    //  * @var string
    //  */
    // protected $leftColumn = 'lft';

    // /**
    //  * Column name for the right index.
    //  *
    //  * @var string
    //  */
    // protected $rightColumn = 'rgt';

    // /**
    //  * Column name for the depth field.
    //  *
    //  * @var string
    //  */
    // protected $depthColumn = 'depth';

    // /**
    //  * Column to perform the default sorting
    //  *
    //  * @var string
    //  */
    // protected $orderColumn = null;

    // /**
    // * With Baum, all NestedSet-related fields are guarded from mass-assignment
    // * by default.
    // *
    // * @var array
    // */
    // protected $guarded = array('id', 'parent_id', 'lft', 'rgt', 'depth');

    //
    // This is to support "scoping" which may allow to have multiple nested
    // set trees in the same database table.
    //
    // You should provide here the column names which should restrict Nested
    // Set queries. f.ex: company_id, etc.
    //

    // /**
    //  * Columns which restrict what we consider our Nested Set list
    //  *
    //  * @var array
    //  */
    protected $scoped = array('root_id');

    //////////////////////////////////////////////////////////////////////////////

    //
    // Baum makes available two model events to application developers:
    //
    // 1. `moving`: fired *before* the a node movement operation is performed.
    //
    // 2. `moved`: fired *after* a node movement operation has been performed.
    //
    // In the same way as Eloquent's model events, returning false from the
    // `moving` event handler will halt the operation.
    //
    // Please refer the Laravel documentation for further instructions on how
    // to hook your own callbacks/observers into this events:
    // http://laravel.com/docs/5.0/eloquent#model-events
    public function getDataCategory($root_id = 0, $parentId = 0)
    {
        $roots = $this->roots()
            ->join('categories_lang', 'categories.id', '=', 'categories_lang.categories_id')
            ->where('categories_lang.lang_id', '=', app('activeLangDetails')['lang_id'])
            ->where(function ($query) use ($root_id, $parentId) {
                if ($root_id) {
                    $query->where('root_id', $root_id);
                }
                if ($parentId) {
                    $query->where('parent_id', $parentId);
                }
            })
            ->get();

        $catIds = Category::where(function ($query) use ($root_id, $parentId) {
            if ($root_id) {
                $query->where('root_id', $root_id);
            }
            if ($parentId) {
                $query->where('parent_id', $parentId);
            }
        })->lists('id');

        $translated = CategoryLang::where('lang_id', app('activeLangDetails')['lang_id'])
            ->whereIn('categories_id', $catIds)
            ->get();
        $translatedLang = [];
        foreach ($translated as $item) {
            $translatedLang[$item->categories_id] = [
                'name' => $item->name
                , 'desc' => $item->desc
                , 'tag' => $item->tag];
        }

        return ['roots' => $roots, 'translatedLang' => $translatedLang];

    }

    public function getListCategories($root_id = 0, $lang_id = 1, $category_id = 0)
    {
        $result = DB::table('categories as c')->join('categories_lang as cl', 'c.id', '=', 'cl.categories_id')
            ->leftJoin('categories as c1', 'c1.id', '=', 'c.parent_id')
            ->where(function ($query) use ($category_id, $root_id) {
                if ($category_id) {
                    $query->orWhere('c.id', $category_id);
                    $query->orWhere('c.parent_id', $category_id);
                }
                if ($root_id)
                    $query->where('c.root_id', $root_id);

            })
            ->select(['c.id', 'c.root_id', 'c.parent_id'
                , 'c.alias', 'c.image', 'c.lft', 'c.rgt', 'c.depth'
                , 'cl.name', 'cl.desc', 'cl.tag'
                , DB::raw('(SELECT name FROM categories_lang WHERE categories_id=c.parent_id AND lang_id=' . $lang_id . ' limit 1) AS parent_name')
                , DB::raw('(SELECT GROUP_CONCAT(id) FROM categories WHERE parent_id=c.id) AS child')
            ])
            ->where('cl.lang_id', '=', $lang_id)
            ->orderBy('c.lft')
            ->get();

        return $result;

    }

}
