<?php

/**
 * Created by PhpStorm.
 * User: Hamid
 * Date: 13/01/2015
 * Time: 09:03 AM
 */
namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;

class Feature extends BaseModel
{

    protected $table = 'features';
    protected $primaryKey = 'id';
    protected $fillable = ['type', 'position', 'custom', 'status'];

    protected $hidden = ['remember_token'];
    public $errors;
    public $rules = ['type' => 'required'];


    public function del($feature_id)
    {

        if (!$feature_id)
            return 0;


        $result = DB::table($this->table)
            ->where('id', $feature_id)
            ->delete();

        $result = DB::table('features_lang')
            ->where('feature_id', $feature_id)
            ->delete();

        $ids = DB::table('feature_value')
            ->where('feature_id', $feature_id)
            ->lists('id');

        $result = DB::table('feature_value')
            ->where('feature_id', $feature_id)
            ->delete();

        $result = DB::table('feature_value_lang')
            ->whereIn('feature_value_id', $ids)
            ->delete();

        $result = DB::table('feature_product')
            ->where('feature_id', $feature_id)
            ->delete();

        $result = DB::table('feature_group_relation')
            ->where('feature_id', $feature_id)
            ->delete();


        if ($result) {
            return array('action' => true, 'message' => Lang::get('backend/Features.messages.msgDelOk'));
        } else {
            return array('action' => false, 'message' => Lang::get('backend/Features.messages.msgDelNotOk'));
        }

    }

} 