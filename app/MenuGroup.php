<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuGroup extends Model
{

    protected $table = 'menu_groups';
    protected $fillable = ['title', 'type', 'status'];

    public $rules = ['title' => 'required'];
    public $errors;

    public function isValid()
    {
        $validation = Validator::make($this->attributes, $this->rules);
        if ($validation->fails()) {
            $this->errors = $validation->messages();
            return false;
        }
        return true;
    }

}
