<?php namespace App;

class Calendar extends BaseModel
{

    protected $table = 'calendar';
    protected $fillable = ['employee_id', 'title', 'start', 'end', 'all_day', 'color'];

    public $errors;
    public $rules = [
        'title' => 'required',
        'start' => 'required',
        'end' => 'required',
    ];


} 