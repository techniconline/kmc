<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class Faq extends BaseModel
{

    protected $table = 'faq';
    protected $fillable = ['type'];

    public function selectJoinLang($field)
    {
        return $this->where(key($field), $field)->join('faq_lang', 'faq_id', '=', 'id')
            ->where('faq_lang.lang_id', '=', app('activeLangDetails')['lang_id']);
    }

}