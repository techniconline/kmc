<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('backend.layouts.partials.sidebarMenu', 'App\Http\Composers\MenusComposer@build');
        view()->composer('app', 'App\Http\Composers\MenusComposer@front_menus');
        view()->composer('intro', 'App\Http\Composers\MenusComposer@slider');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

}
