<?php namespace App\Providers;

use App\Language;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Config;

class AppServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton('locale', function () {
            if (strlen(App::getLocale()) > 2) {
                return '';
            } else {
                return App::getLocale();
            }
        });

        $this->app->singleton('getPrefix', function (Request $request) {
            $locale = $request->segment(1);

            if (strlen($locale) > 2) {
                return $locale;
            } else {
                $locale = $request->segment(2);
                return $locale;
            }
        });

        $this->app->singleton('getLocale', function () {
            return app('locale') == '' ? Config::get('app.fallback_locale') : app('locale');
        });

        $this->app->singleton('getLocaleDefault', function () {
            return Config::get('app.fallback_locale');
        });

        $this->app->singleton('activeLangDetails', function () {
            $language = new Language();
            return $language->getLanguage(app('getLocale'));
        });
    }

    /**
     * Register any application services.
     *
     * This service provider is a great spot to register your various container
     * bindings with the application. As you can see, we are registering our
     * "Registrar" implementation here. You can add your own bindings too!
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'Illuminate\Contracts\Auth\Registrar',
            'App\Services\Registrar'
        );
    }

}
