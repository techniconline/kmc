<?php namespace App\Providers\Helpers\Category;

use App\Category;
use App\MenuLang;
use Form;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Input;

class CategoryHelper
{

    private static $getID = array();


    public static function treeRender($roots)
    {
        if (strlen($roots) > 2) {
            echo "<ul class='has-liststyle'>";
            foreach ($roots as $key => $root) {
                echo "<li>" .
                    link_to_route('backend.categories.show', $root->name, $parameters = array($root->name), $attributes = array()) .
                    "<a href='#' data-rootid='" . $root->root_id . "' data-id='" . $root->id . "' data-parentId='" . $root->parent_id . "' id='newCat'>+</a>";

                if ($root['children']) {
                    self::treeRender($root['children']);
                }
                echo "</li>";
            }
            echo "</ul>";
        }
    }

    public static function rootsRender($roots)
    {
        echo "<div><ul>";
        foreach ($roots as $key => $root) {
            echo "<li>" .
                link_to_route('backend.category.show', $root->name, $parameters = array($root->name), $attributes = array()) .
                "</li>";
        }
        echo "</ul></div>";

    }

    public static function treeRenderJustID($roots)
    {
        if (strlen($roots) > 2) {
            foreach ($roots as $key => $root) {
                self::$getID[] = $root->id;

                if ($root['children']) {
                    self::treeRenderJustID($root['children']);
                }
            }
        }
        return self::$getID;
    }

    public static function hierarchy($cats, $countDash = 0, &$catsLists = [])
    {
        foreach ($cats as $key => $cat) {
            $catsLists[$cat->id] = str_repeat("-", $countDash) . $cat->name;
            if (strlen($cat->children) > 2) {
                $countDash++;
                self::hierarchy($cat->children, $countDash, $catsLists);
                $countDash--;
            }
        }
        return $catsLists;
    }

    public static function hierarchyCats($name)
    {
        $cats = Category::where('name', '=', $name)->first()->getDescendants()->toHierarchy();
        return self::hierarchy($cats);
    }

    public static function createBoxTreeForNestable($roots, $translatedName, $root_id, $controller, $locale, $title = null, $tools = true, $drag = true, $childTools = true, $checkbox = false, $listChecked = [])
    {
        echo '
    <div class="row">
        <div class="col-md-8">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-comments"></i>' . ($title ? $title : 'List') . '
                    </div>
                    ';
        if ($tools) {
            echo '<div class="tools">
                        <a href="javascript:;" class="collapse">
                        </a>
                        <a href="#portlet-config" data-toggle="modal" class="config">
                        </a>
                        <a href="javascript:;" class="reload">
                        </a>
                        <a href="javascript:;" class="remove">
                        </a>
                    </div>
                    ';
        }
        echo '
                </div>
                <div class="portlet-body">
                <div class="dd" id="' . ($drag ? 'nestable_list' : 'nestable_list_noDrag') . '" data-route="' . $locale . $controller . '" data-root="' . $root_id . '"  >
                ';

        self::treeRenderForSortableCategory($roots, $controller, $root_id, $locale, $translatedName, $childTools, $checkbox, $listChecked);

        echo '
                </div>
                </div>
            </div>
        </div>
    </div>
    ';

    }

    public static function treeRenderForSortableCategory($roots, $controller, $root_id, $locale, $translatedName, $childTools = true, $checkbox = false, $listChecked = [])
    {
        if (strlen($roots) > 2) {
            echo '<ol class="dd-list">';
            foreach ($roots as $key => $root) {
                if (isset($root->root_id) && (!$root_id || $root->root_id == $root_id)) {


                    echo '
                            <li class="dd-item dd3-item" data-id="' . $root->id . '">';
                    if (!$checkbox) {
                        echo '<div class="dd-handle dd3-handle"></div>';
                    }
                    echo '<div class="dd3-content">
                                    ' . link_to_route($locale . 'backend.' . $controller . '.show', $translatedName[$root->id]['name'], $parameters = array($root->id . '-' . $root->alias), $attributes = array()) . '
                                    ';
                    if ($childTools) {
                        echo '
                            <div style="float: right;margin-top: -2px;">
                                <a class="catLeft catadd btn btn-xs green-meadow"><i class="fa fa-plus"></i> Add Child</a>
                                <a class="catLeft catedit btn btn-xs purple-plum" href="' . route($locale . 'backend.' . $controller . '.edit', $root->id) . '" data-id="' . $root->id . '"><i class="fa fa-edit"></i> Edit</a>
                                <a class="catRight catremove btn btn-xs red-sunglo"><i class="fa fa-times"></i> Deleted</a>
                            </div>
                                    ';
                    }

                    if ($checkbox && $root->parent_id) {
                        echo ' <div style="float: right;padding-left: 2px;"> <input ' . (in_array($root->id, $listChecked) ? 'checked' : '') . ' type="checkbox" value="' . $root->id . '" name="categories[]"/> </div> ';
                    }

                    echo '</div>';

                    if ($root['children']) {
                        self::treeRenderForSortableCategory($root['children'], $controller, $root_id, $locale, $translatedName, $childTools, $checkbox, $listChecked);
                    }
                    echo '</li>';

                }

            }
            echo '</ol>';


        }
    }


    public static function createTreeCategories($roots, $translatedName, $root_id, $locale, $title = null, $system = 'news')
    {
        echo '
    <aside class="widget widget_categories shbox">
        <header><h4 class="widgettitle">' . ($title ? $title : 'List') . '</h4></header>
                    ';

        self::treeRenderCategories($roots, $root_id, $locale, $translatedName, $system);

        echo '</aside>';

    }

    public static function treeRenderCategories($roots, $root_id, $locale, $translatedName, $system)
    {
        if (strlen($roots) > 2) {
            echo '<ul>';
            foreach ($roots as $key => $root) {
                if (isset($root->root_id) && (!$root_id || $root->root_id == $root_id)) {
                    echo '
                            <li class="" data-id="' . $root->id . '">';
                    if ($root->parent_id) {
                        echo link_to_route($system . '.searchCategory', $translatedName[$root->id]['name'], [$root->alias], []);
                    }
                    if ($root['children']) {
                        self::treeRenderCategories($root['children'], $root_id, $locale, $translatedName, $system);
                    }
                    echo '</li>';
                }

            }
            echo '</ul>';


        }
    }

    public static function treeRenderJsonTree($roots, $translatedName, $countTrans, $counter = 0, array $data = null, $isChild = false)
    {
//        dd($roots,$translatedName);
        if (strlen($roots) > 2) {
            foreach ($roots as $key => $root) {
                $counter++;
                if (!$isChild) {
                    $data[$root->id] = ['text' => $translatedName[$root->id]['name'], 'state' => ['selected' => false]];
                } else {
                    $data[$root->parent_id]['children'][] = ['text' => $translatedName[$root->id]['name'], 'state' => ['selected' => false, 'opened' => true], 'icon' => "fa fa-folder icon-state-default"];
                }

//                echo $translatedName[$root->id]['name'].'<br>';

                if ($root['children']) {
                    self::treeRenderJsonTree($root['children'], $translatedName, $countTrans, $counter, $data, true);
                }

            }

        }

        if ($countTrans == $counter) {
            dd($data);
            return $data;
        }

    }


    public static function createAlias($string)
    {
        $string = trim($string);

        if (ctype_digit($string)) {
            return $string;
        } else {
            // replace accented chars
//            /^([\x{0600}-\x{06EF}])+$/    ^[^\x{600}-\x{6FF}]+$
            $accents = '/&([A-Za-z پچجحخهعغفقثصیضشسیبلاتنمکگویئدذرزطظ ]{1,2})(grave|acute|circ|cedil|uml|lig);/';
            $string_encoded = htmlentities($string, ENT_COMPAT, 'UTF-8');

            $string = preg_replace($accents, '$1', $string_encoded);

            // clean out the rest
            //Type A: Remove all duplicated character placements like "this-is-idollobicom"

            $replace = array('([\40])', '([^a-zA-Z0-9- پچجحخهعغفقثصضشییسیبلاتنمکگوئدذرزطظ ])', '(-{2,})');
            $with = array('-', '', '-');

            // Type B: reserve all character placements like "this-is--idollobi-com-"

            $replace = array('([\40])', '([^a-zA-Z0-9- پچجحخهعغفقثیصضشسییبلاتنمکگوئدذرزطظ ])');
            $with = array('-', '-');
            $string = preg_replace($replace, $with, $string);


            $string = str_replace(array(" ", "/", ".", "?", "#"), array("-"), $string);
            $string = str_replace('--', "", $string);
        }

        if (!$string) {
            $string = 'No-Title-' . rand(1111111, 9999999);
        }

        return strtolower($string);

    }

    public static function createTags($string)
    {
        $string = trim($string);

        if (ctype_digit($string)) {
            return $string;
        } else {
            // replace accented chars
//            /^([\x{0600}-\x{06EF}])+$/    ^[^\x{600}-\x{6FF}]+$
            $accents = '/&([A-Za-z پچجحخهیعغفقثصضشسیبلاتنمکگیوئدذرزطظ ]{1,2})(grave|acute|circ|cedil|uml|lig);/';
            $string_encoded = htmlentities($string, ENT_COMPAT, 'UTF-8');

            $string = preg_replace($accents, '$1', $string_encoded);

            // clean out the rest
            //Type A: Remove all duplicated character placements like "this-is-idollobicom"

            $replace = array('([\40])', '([^a-zA-Z0-9- یپچجحخهعغیفقثصضشسیبلاتنمکگوئدذرزطظ ])', '(-{2,})');
            $with = array(',', '', ',');

            // Type B: reserve all character placements like "this-is--idollobi-com-"

            $replace = array('([\40])', '([^a-zA-Z0-9- پچجحخهعیغفیقثصضشسیبلاتنمکگوئدذرزطظ ])');
            $with = array(',', ',');
            $string = preg_replace($replace, $with, $string);


            $string = str_replace(array(" ", "/", ".", "?", "#"), array(","), $string);
            $string = str_replace(',,', "", $string);
        }

        return strtolower($string);

    }


}