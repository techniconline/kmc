<?php namespace App\Providers\Helpers\Category;

use Illuminate\Support\Facades\Facade;

class CategoryHelperFacade extends Facade
{

    protected static function getFacadeAccessor()
    {
        return 'category';
    }

}