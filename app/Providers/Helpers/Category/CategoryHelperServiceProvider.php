<?php namespace App\Providers\Helpers\Category;

use Illuminate\Support\ServiceProvider;

class CategoryHelperServiceProvider extends ServiceProvider
{

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerCategoryBuilder();

        $this->app->alias('category', 'App\Providers\Helpers\Category');
    }

    /**
     * Register the Category builder instance.
     *
     * @return void
     */
    protected function registerCategoryBuilder()
    {
        $this->app->bindShared('category', function () {
            return new CategoryHelper();
        });
    }

}
