<?php namespace App\Providers\Helpers\Uploader;

use Illuminate\Support\ServiceProvider;

class UploaderHelperServiceProvider extends ServiceProvider
{

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerUploaderBuilder();

        $this->app->alias('uploader', 'App\Providers\Helpers\Uploader');
    }

    /**
     * Register the Category builder instance.
     *
     * @return void
     */
    protected function registerUploaderBuilder()
    {
        $this->app->bindShared('uploader', function () {
            return new UploaderHelper();
        });
    }

}
