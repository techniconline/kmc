<?php namespace App\Providers\Helpers\Uploader;

use Form;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Input;

class UploaderHelper
{

    public static function loadUploaderJquery()
    {
        echo '
        <link href="' . asset('/assets/css/uploadfile.min.css') . '" rel="stylesheet" type="text/css"/>
        <script src="' . asset('/assets/js/jquery.uploadfile.min.js') . '" type="text/javascript"></script>
        ';
    }

    public static function renderUploaderJquery($idUploader, $idStartUploader, $idMsgUploader, $titleUpload, $titleStartUpload, $dragDropStr
        , $urlUploader, $objUploaderName, $autoUpload = 'false', $subFolder = ''
        , $type = 'images', $maxFileSize = 20, $itemId = 0, $allowedTypes = 'png,gif,jpg,jpeg'
        , $showDelete = 'true', $showDone = 'false', $fileName = 'myfile', $titleButtonDrag = '', $counter = 1
        , $titleSaveDesc = 'Click save button after uploaded all files, please.'
        , $controller = 'profile', $controllerDoc = 'attach'
        , $maxCountFileUploaded = 1, $multiUpload = 'false'
        , $useController=false
        , $prefix='backend'
    )
    {

        $typeParam = '';
        $withType = "?";
        if ($type) {
            $typeParam = "?type=" . $type;
            $withType="&";
        }
        if($useController){
            $typeParam.= $withType."ctrl=".$controller."&ctrlDoc=".$controllerDoc."&prefix=".$prefix."&itemId=".$itemId;
        }
        $maxFileSize = 1024 * $maxFileSize * 1024;// x Mb
        echo '
            <div id="' . $idUploader . '"><i class="fa fa-plus"></i><i class="fa fa-file"></i></div>';
//        echo '
//            <div id="'.$idUploader.'"><i class="fa fa-file"></i>'.$dragDropStr.'</div>';
        if ($autoUpload == 'false') {
            echo '
            <div id="' . $idStartUploader . '" class="ajax-file-upload-green">' . $titleStartUpload . '</div>';
        }
        echo '
            <div id="' . $idMsgUploader . '" style="border: 1px solid #CD3F3F"></div>

            <script>
            jQuery(document).ready(function () {
                var multipleUpload' . $itemId . ' = ' . $multiUpload . ';
                jQuery("#' . $idMsgUploader . '").hide();
                var formUploader' . $itemId . ' = jQuery("#form-uploader-"+' . $itemId . ');
                var resultUploader' . $itemId . ' = jQuery("#result-uploader-"+' . $itemId . ');
                var ' . $objUploaderName . ' = jQuery("#' . $idUploader . '").uploadFile({
                url: "' . $urlUploader . '"+"' . $typeParam . '",
                method:"POST",
                allowedTypes:"' . $allowedTypes . '",
                formData:{sf:"' . $subFolder . '"},
                fileName:"' . $fileName . '",
                multiple:"' . $multiUpload . '",
                maxFileCount:"' . $maxCountFileUploaded . '",
                autoSubmit:' . $autoUpload . ',
                maxFileSize:' . $maxFileSize . ',
                dragDropStr: "<span><b>' . ''/*$dragDropStr*/ . '</b><p>' . $counter . ' ' . $titleButtonDrag . '</p></span>",
                showDelete: ' . $showDelete . ',
                showDone: ' . $showDone . ',
                deleteCallback: function (data, pd) {
                ';
        if ($multiUpload == 'false') {
            echo 'resultUploader' . $itemId . '.fadeOut(); ';
        }
        echo '
                dataDel=jQuery.parseJSON(data);
                //console.log(data);
                jQuery.post("' . $urlUploader . '"+"/delete",
                {op: "delete"
                ,id: dataDel.id
                ,prefix: "' . $prefix . '"
                ,item_id: "' . $itemId . '"
                ,controller: "' . $controller . '",
                controllerDoc: "' . $controllerDoc . '"},
                    function (resp,textStatus, jqXHR) {
                        //Show Message
                        var dataDeleted=jQuery.parseJSON(resp);
                        //console.log(resp,textStatus);
                        if(textStatus=="success"){
                            if(dataDeleted.valid){
                        console.log("File Deleted");
                        ';
        if ($multiUpload == 'false') {
            echo 'formUploader' . $itemId . '.find("div.ajax-upload-dragdrop").first().fadeIn(); resultUploader' . $itemId . '.html(""); ';
        }
        echo '
                            }
                        }
                    });
                pd.statusbar.hide(); //You choice.
                },
                onSuccess:function(files,data,xhr)
                {
                    var countEle' . $itemId . ' = formUploader' . $itemId . '.find("div.ajax-file-upload-statusbar").length;
                    var resultTarget' . $itemId . ' = formUploader' . $itemId . '.find("div.ajax-file-upload-statusbar").first();
                    var progressBar' . $itemId . ' = formUploader' . $itemId . '.find("div.ajax-file-upload-progress").first();
                    var fileName' . $itemId . ' = formUploader' . $itemId . '.find("div.ajax-file-upload-filename").first();
                    data=jQuery.parseJSON(data);
                    if(data.valid){

                        //if(countEle' . $itemId . '>1){
                           //formUploader' . $itemId . '.find("div.ajax-file-upload-statusbar").last().remove();
                        //}

                        //inputs=[\'<label><input class="icheck" data-checkbox="icheckbox_square-grey" type="checkbox" checked value="\'+data.id+\'" name="' . $type . '[]"></label>\'];
                        //if(multipleUpload' . $itemId . '){
                        //inputHidden=[\'<label><input class="value-media" data-item="' . $itemId . '" type="hidden" value="\'+data.id+\'" name="' . $type . '[]"></label>\'];
                        //}else{
                        inputHidden=[\'<label style="width:0px !important;"><input class="value-media" data-item="' . $itemId . '" type="hidden" value="\'+data.id+\'" name="' . $type . '[]"></label>\'];
                        //}
                        //inputsTitle=[\'<input placeholder="Title, example:\'+files+\'" type="text" class="form-control input-medium" value="" name="image_title[\'+data.id+\']">\'];
                        //$("#' . $idMsgUploader . '").append(\'<p>\'+data.msg+\' \'+inputs+\' (save) \'+inputsTitle+\' </p>\').show();
                        //resultTarget' . $itemId . '.append(\'<i class="fa fa-check">\'+data.msg+\'  \'+inputHidden+\'  (save) </i>\');

                        //console.log(resultUploader' . $itemId . ');
                        //resultUploader' . $itemId . '.append(\'<i class="fa fa-check" style="color:green; width: 80%">\'+data.msg+\'  \'+inputHidden+\'  (' . $titleSaveDesc . ') </i>\');
                        resultUploader' . $itemId . '.append(\'<i class="fa fa-check" style="color:green; width: 80%">\'+inputHidden+\'  (' . $titleSaveDesc . ') </i>\');

                    ';
        if ($multiUpload == 'false') {
            echo 'formUploader' . $itemId . '.find("div.ajax-upload-dragdrop").first().fadeOut();';
        }
        echo '
                    }else{
                        jQuery("#' . $idMsgUploader . '").append(\'<p>\'+data.msg+\'</p>\').show();
                    }
                }
            });

            formUploader' . $itemId . '.find("div.input-group").first();
            ';

        if ($autoUpload == 'false') {
            echo '
            jQuery("#' . $idStartUploader . '").click(function()
            {
                ' . $objUploaderName . '.startUpload();
            });
            ';
        }
        echo '
            });</script>
        ';

    }

}