<?php namespace App\Providers\Helpers\Uploader;

use Illuminate\Support\Facades\Facade;

class UploaderHelperFacade extends Facade
{

    protected static function getFacadeAccessor()
    {
        return 'uploader';
    }

}