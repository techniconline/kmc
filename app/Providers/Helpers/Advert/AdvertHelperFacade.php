<?php namespace App\Providers\Helpers\Advert;

use Illuminate\Support\Facades\Facade;

class AdvertHelperFacade extends Facade
{

    protected static function getFacadeAccessor()
    {
        return 'advert';
    }

}