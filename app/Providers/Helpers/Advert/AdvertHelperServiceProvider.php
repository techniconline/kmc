<?php namespace App\Providers\Helpers\Advert;

use Illuminate\Support\ServiceProvider;

class AdvertHelperServiceProvider extends ServiceProvider
{

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerAdvertBuilder();

        $this->app->alias('advert', 'App\Providers\Helpers\Advert');
    }

    /**
     * Register the Category builder instance.
     *
     * @return void
     */
    protected function registerAdvertBuilder()
    {
        $this->app->bindShared('advert', function () {
            return new AdvertHelper();
        });
    }

}
