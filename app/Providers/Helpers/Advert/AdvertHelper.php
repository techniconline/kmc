<?php namespace App\Providers\Helpers\Advert;

use App\Advertisement;
use App\MenuLang;
use Form;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;


class AdvertHelper
{

    public static function showSliderHome()
    {
        $modelAdvert = new Advertisement();
        $advertisements = $modelAdvert->getAdverts('home_slider');

        $advertHtml = '';
        foreach ($advertisements as $item) {
            $advertHtml .= '<div class="ls-slide" data-ls="' . ($item->slider_option ? $item->slider_option : 'slidedelay:10000;transition2d:4;') . '">';
            $advertHtml .= '<img src="' . asset($item->file_path) . '" class="ls-bg" alt="' . $item->title . '" />';
            $bodyContent = $item->body_content ? json_decode($item->body_content) : null;
            foreach ($bodyContent as $key => $value) {
                $advertHtml .= $value;
            }
            $advertHtml .= '</div>';
        }

        echo $advertHtml;
    }

    public static function showAdverts($position = 'center_advert_home_footer', $classImg = '')
    {
        $modelAdvert = new Advertisement();
        $advertisements = $modelAdvert->getAdverts($position);

        $advertHtml = '';
        foreach ($advertisements as $item) {
            $advertHtml .= '<li><a href="' . $item->url . '">';
            $advertHtml .= '<img src="' . asset($item->file_path) . '" class="' . $classImg . '" alt="' . $item->title . '" />';
            $advertHtml .= '</a></li>';
        }

        echo $advertHtml;
    }


}