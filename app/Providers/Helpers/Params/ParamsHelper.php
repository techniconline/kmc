<?php namespace App\Providers\Helpers\Params;

use App\Param;
use App\Content;
use App\Providers\Helpers\PersianDate\PersianDate;

class ParamsHelper
{

    public static function favIcon()
    {
        $favicon = Param::where('alias', '=', 'favicon')->where('status', 1)->where('type', 'image')->first();
        if ($favicon != '') {
            $faviconTag = '<link rel="icon" type="image" href="' . asset('/uploads/images/Params/' . $favicon->value . '.jpg') . '" />';
            return $faviconTag;
        }
    }

    public static function socialIcon()
    {
        $socials = Param::where('type', 'system')->where('alias', 'social')->where('status', 1)->get();
        if ($socials != '') {
            $socialsTag = '';
            foreach ($socials as $social) {
                $socialsTag .= '<li>';
                $socialsTag .= '<a href="' . $social->value . '" title="' . $social->to_value . '"> <i class="fa fa-' . $social->to_value . '"></i> </a>';
                $socialsTag .= '</li>';
            }
            echo $socialsTag;
        }
    }

    public static function getParam($alias = null, $type = null, $isLink = false, $withTag = '', $isArray = false)
    {
        $data = Param::where('status', 1)
            ->where(function ($query) use ($alias, $type) {

                if ($alias)
                    $query->where('alias', $alias);

                if ($type)
                    $query->where('type', $type);

            })
            ->get();
        if ($data) {
            $dataTag = [];
            $counter = 0;
            foreach ($data as $item) {

                if ($withTag && !$isArray)
                    $dataTag[$counter] .= '<' . $withTag . '>';

                if ($isLink) {
                    $dataTag[$counter] .= '<a href="' . $item->value . '" title="' . $item->to_value . '"> ' . $item->to_value ? '<i class="fa fa-' . $item->to_value . '"></i>' : '' . ' </a>';
                } else {
                    $dataTag[$counter] .= $item->value;
                }

                if ($withTag && !$isArray)
                    $dataTag[$counter] .= '</' . $withTag . '>';

                if (!$isArray)
                    echo $dataTag[$counter];

                $counter++;
            }

            return $dataTag;
        }
    }

    public static function getImages($alias = '', $withTagImg = true, $outArray = false, $link = null, $classLink = null)
    {
        $sliders = Param::where('alias', '=', $alias)->where('status', 1)->where('type', 'image')->get();
        $counter = 0;
        foreach ($sliders as $item) {
            $sliders[$counter] = '';
            if ($withTagImg) {
                if ($link) {
                    $sliders[$counter] .= '<a href="' . $link . '" class="' . $classLink . '">';
                }
                $sliders[$counter] .= '<img src="' . asset('/uploads/images/Params/' . $item->value) . '" alt="' . $item->name . '"/>';
                if ($link) {
                    $sliders[$counter] .= '</a>';
                }
            } else {
                $sliders[$counter] = asset('/uploads/images/Params/' . $item->value);
            }
            if (!$outArray) {
                echo $sliders[$counter];
            }
            $counter++;
        }

        if ($outArray)
            return $sliders;


    }

    public static function rssIcon()
    {
        $rss = Param::where('type', 'system')->where('alias', 'rss')->where('status', 1)->first();
        if ($rss != '') {
            $rssTag = '<li>';
            $rssTag .= '<a href="' . $rss->value . '" title="' . $rss->to_value . '"> <i class="fa fa-' . $rss->to_value . '"></i> </a>';
            $rssTag .= '</li>';
            return $rssTag;
        }
    }

    public static function metaTag()
    {
        $metas = Param::where('type', 'system')->where('alias', 'metatag')->where('status', 1)->get();
        if ($metas != '') {
            $metaTags = '';
            foreach ($metas as $meta) {
                $metaTags .= '<meta name="' . $meta->name . '" content="' . $meta->value . '" />';
            }
            return $metaTags;
        }
    }

    public static function getStaticContent($alias)
    {

        $content = Content::join('contents_lang', 'contents_id', '=', 'contents.id')
            ->join('employee', 'contents.author_id', '=', 'employee.id')
            ->where('contents_lang.lang_id', '=', app('activeLangDetails')['lang_id'])
            ->whereIn('type', ['blog', 'static'])
            ->where('contents.status', 1)
            ->where('contents.url', $alias)
            ->first();

        return $content;

    }

    public static function getStaticContentsByType($type = 'index-list')
    {

        $contents = Content::join('contents_lang', 'contents_id', '=', 'contents.id')
            ->join('employee', 'contents.author_id', '=', 'employee.id')
            ->select(['contents_lang.*', 'contents.*', 'employee.firstname', 'employee.lastname'])
            ->where('contents_lang.lang_id', '=', app('activeLangDetails')['lang_id'])
            ->where('type', $type)
            ->where('contents.status', 1)
            ->orderBy('sort')
            ->get();

        if (count($contents))
            return $contents;

        return false;

    }

    public static function getLastNewsContent($number = 5)
    {

        $news = Content::join('contents_lang', 'contents_id', '=', 'contents.id')
            ->join('employee', 'contents.author_id', '=', 'employee.id')
            ->select(['employee.firstname', 'employee.lastname', 'contents.*', 'contents_lang.*'])
            ->where('contents_lang.lang_id', '=', app('activeLangDetails')['lang_id'])
            ->whereIn('type', ['news'])
            ->where('contents.status', 1)
            ->orderBy('contents.id', 'DESC')
            ->take($number)->get();

        foreach ($news as &$item) {

            $item->pDayName = PersianDate::$pdateWeekNameWithKey[strtolower(date("D", strtotime($item->created_at)))];
            $item->pDate = PersianDate::convert_date_M_to_H(date("Y-m-d", strtotime($item->created_at)));
            $arrDate = explode("-", $item->pDate);
            $item->pMonthName = PersianDate::$pdateMonthName[(int)$arrDate[1]];
            $item->pMonth = $arrDate[1];
            $item->pDay = $arrDate[2];
            $item->pYear = $arrDate[0];

        }

        return $news;

    }


}