<?php namespace App\Providers\Helpers\Params;

use Illuminate\Support\ServiceProvider;

class ParamsHelperServiceProvider extends ServiceProvider
{

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerParamsHelperBuilder();

        $this->app->alias('paramshelper', 'App\Providers\Helpers\ParamsHelper');
    }

    /**
     * Register the ParamsHelper builder instance.
     *
     * @return void
     */
    protected function registerParamsHelperBuilder()
    {
        $this->app->bindShared('paramshelper', function () {
            return new ParamsHelper();
        });
    }

}
