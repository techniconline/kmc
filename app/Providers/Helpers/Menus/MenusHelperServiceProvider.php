<?php namespace App\Providers\Helpers\Menus;

use Illuminate\Support\ServiceProvider;

class MenusHelperServiceProvider extends ServiceProvider
{

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerMenusHelperBuilder();

        $this->app->alias('menushelper', 'App\Providers\Helpers\MenusHelper');
    }

    /**
     * Register the MenusHelper builder instance.
     *
     * @return void
     */
    protected function registerMenusHelperBuilder()
    {
        $this->app->bindShared('menushelper', function () {
            return new MenusHelper();
        });
    }

}
