<?php namespace App\Providers\Helpers\Menus;

use App\ContentLang;
use App\Http\Controllers\backend\CategoryController;
use App\MenuLang;
use Illuminate\Support\Facades\Request;
use Form;
use App\Category;
use Illuminate\Support\Facades\URL;

class MenusHelper
{

    static $menu = "";


    public static function backEndMenusBuild($menus)
    {
        $translatedName = MenuLang::where('lang_id', app('activeLangDetails')['lang_id'])->lists('name', 'menus_id');
        $active = explode("/", Request::path());
        empty($active[1]) ? $active[1] = 'dashboard' : $active[1] = $active[1];
        foreach ($menus as $menu) {
            if ($menu->parent_id == 1) {
                echo '
                        <li class="heading">
                            <h3 class="uppercase">' . $translatedName[$menu->id] . '</h3>' .
                    '</li>';
                if (strlen($menu->children) > 2) {
                    self::backEndchildren($menu->children, $translatedName);
                }
            }
        }
    }

    public static function backEndchildren($children, $translatedName, $arrow = False)
    {
        foreach ($children as $key => $child) {
            if (strlen($child->children) > 2) {
                echo '<li>';
                echo '<a href="javascript:void(0);">
                        <i class="' . $child->thumbnail . '"></i>
                        <span class="title">' . $translatedName[$child->id] . '</span>
                        <span class="arrow "></span>
                        </a>';
                echo '<ul class="dropdown">';
                self::backEndchildren($child->children, $translatedName, TRUE);
                echo '</ul>';
                echo '</li>';
            } else {
                echo '<li>';
                echo '<a href="' . (app('locale') == '' ? '/' . $child->url : '/' . app('locale') . '/' . $child->url) . '">
                        <i class="' . $child->thumbnail . '"></i>
                        <span class="title">' . $translatedName[$child->id] . '</span>
                        </a>';
                echo '</li>';
            }
        }
    }

    public static function returnUrl($url)
    {

        if (strpos($url, 'http') !== false || strpos($url, 'www') !== false) {
            return $url;
        } else {
            $url = (app('locale') == '' ? '' : '/' . app('locale')) . '/' . $url;
            return $url;
        }
    }

    public static function frontEndMenusMegaBuild($menus, $root_id = 2, $category = false)
    {

        $activeCategory = false;
        if ($category) {
            $modelCategory = new Category();
            $dataCategory = $modelCategory->getDataCategory(1);
            $roots = $dataCategory['roots'];
            $translatedLangCat = $dataCategory['translatedLang'];

            if ($roots && $translatedLangCat)
                $activeCategory = true;

        }

        $translated = MenuLang::join('menus', 'menus.id', '=', 'menus_lang.menus_id')
            ->where('menus_lang.lang_id', app('activeLangDetails')['lang_id'])
            ->where('menus.root_id', $root_id)
            ->select(['menus_lang.*'])
            ->get();
        $translatedLang = [];
        foreach ($translated as $item) {
            $translatedLang[$item->menus_id] = ['name' => $item->name, 'desc' => $item->desc];
        }

        if (!$menus && !$activeCategory)
            return false;

        $active = explode("/", Request::path());
        empty($active[1]) ? $active[1] = 'dddddsss' : $active[1] = $active[1];
        if ($menus || $activeCategory)
            echo '<ul class="menuzord-menu">';


        foreach ($menus as $menu) {
            if ($menu->parent_id == $root_id) {
                echo '
                    <li class="dropdown">
                        <a href="' . $menu->url . '">' . $translatedLang[$menu->id]['name'] . '</a>';
                if (strlen($menu->children) > 2) {
                    echo '<ul class="dropdown">';
                    self::frontEndChildren($menu->children, $translatedLang);
                    echo '</ul>';

                }
                echo '</li>';
            }
        }


        if ($activeCategory) {
            foreach ($roots as $root) {
                if (!$root->parent_id) {
                    echo '
                    <li class="dropdown">
                        <a href="#">' . $translatedLangCat[$root->id]['name'] . '</a>';
                    if (strlen($root->children) > 2) {
                        echo '<ul class="dropdown">';
                        self::frontEndChildren($root->children, $translatedLangCat, false, true);
                        echo '</ul>';

                    }
                    echo '</li>';
                }
            }
        }

        if ($menus || $activeCategory)
            echo '</ul>';
    }

    public static function frontEndChildren($children, $translatedLang, $arrow = False, $category = false)
    {
        foreach ($children as $key => $child) {
            if (strlen($child->children) > 2) {
                echo '<li class="menu-item-with-widget-area">';
                echo '<a href="' . (!$category ? self::returnUrl($child->url) : URL::route('category.show', $child->alias)) . '">
                        <i class="' . $child->thumbnail . '"></i> ' . $translatedLang[$child->id]['name'] . '</a>';
                echo '<p>' . $translatedLang[$child->id]['desc'] . '</p>';
                if ($child->image == 'true') {
                    echo '<img src="' . asset(($category ? '/uploads/images/Categories/' . $child->id . '-300x100.jpg' : '/uploads/images/Menus/' . $child->id . '-300x100.jpg')) . '" />';
                }

                echo '<div class="menu-item-widget-area-container"> <ul> <li class="widget widget_text"> <div class="textwidget"> <ul> ';
                self::frontEndChildren($child->children, $translatedLang, TRUE, $category);
                echo '</ul> </div> </li> </ul> </div>';
                echo '</li>';
            } else {
                echo '<li ' . ((isset($arrow) && $arrow) ? "" : "class='menu-item-with-widget-area'") . '>';
                echo '<a href="' . (!$category ? self::returnUrl($child->url) : URL::route('category.show', $child->alias)) . '">
                        <i class="' . $child->thumbnail . '"></i> ' . $translatedLang[$child->id]['name'] . '</a>';
                echo '<p>' . $translatedLang[$child->id]['desc'] . '</p>';
                if ($child->image == 'true') {
                    echo '<img src="' . asset(($category ? '/uploads/images/Categories/' . $child->id . '-300x100.jpg' : '/uploads/images/Menus/' . $child->id . '-300x100.jpg')) . '" />';
                }
                echo '</li>';
            }
        }
    }

    public static function frontEndMenusForHomeBuild($menus, $contents)
    {
        $translatedMenusName = MenuLang::where('lang_id', app('activeLangDetails')['lang_id'])->lists('name', 'menus_id');
        $translatedContentsName = ContentLang::where('lang_id', app('activeLangDetails')['lang_id'])->lists('link_title', 'contents_id');
//        $active = explode("/", Request::path());
//        empty($active[1]) ? $active[1] = 'dashboard' : $active[1] = $active[1];
        foreach ($menus as $menu) {
            echo '
                <li>
                    <a href="' . url(app('locale') . '/' . $menu->url) . '">' . $translatedMenusName[$menu->id] . '</a>' .
                '</li>';
        }
        foreach ($contents as $content) {
            echo '
                <li>
                    <a href="' . url(app('locale') . '/content/' . $content->url) . '">' . $translatedContentsName[$content->id] . '</a>' .
                '</li>';
        }
    }

    public static function frontEndMenusForHomeBuildWithOutLi($menus, $contents, $class = '', $span = false)
    {
        $translatedMenusName = MenuLang::where('lang_id', app('activeLangDetails')['lang_id'])->lists('name', 'menus_id');
        $translatedContentsName = ContentLang::where('lang_id', app('activeLangDetails')['lang_id'])->lists('link_title', 'contents_id');
//        $active = explode("/", Request::path());
//        empty($active[1]) ? $active[1] = 'dashboard' : $active[1] = $active[1];
        foreach ($menus as $menu) {
            echo '<a ' . ($class ? 'class="' . $class . '"' : '') . ' href="' . url(app('locale') . '/' . $menu->url) . '">' . $translatedMenusName[$menu->id] . ($span ? '<span></span>' : '') . '</a>';
        }
        foreach ($contents as $content) {
            echo '<a ' . ($class ? 'class="' . $class . '"' : '') . ' href="' . url(app('locale') . '/content/' . $content->url) . '">' . $translatedContentsName[$content->id] . ($span ? '<span></span>' : '') . '</a>';
        }
    }

//    public static function builder($class ,$menus, $groups){
//        echo "<div id='nav-wrapper' class='menu-{$class} clrfx'>";
//        foreach ($groups as $key => $value) {
//            echo "<div class='$value packet-menu'>
//                          <h3>$value</h3>
//                          <ul class='sub-menu light-gray'>";
//            self::submenu($menus, $key);
//            echo "</ul></div>";
//        }
//        echo "</div>";
//    }
//
//    public static function submenu($menus, $group){
//        $active = explode("/",Request::path());
//        empty($active[1]) ? $active[1]='dashboard' : $active[1]=$active[1] ;
//        foreach($menus as $menu){
//            if($menu->group_id == $group){
//                $url = $menu->url == '#' ? '#' : url('backend') . '/' . $menu->url ;
//                $jsonClass = (isset(json_decode($menu->attr)->class)) ? json_decode($menu->attr)->class : "";
//                $jsonIcon = (isset(json_decode($menu->attr)->icon)) ? json_decode($menu->attr)->icon : "circle";
//                echo "<li class='$jsonClass ".($active[1] == $menu->url ? "active" : "") . "'>".
//                        link_to_route('backend.menu.edit',$menu->name,$menu->id);
//                if(strlen($menu->children) > 2){
//                    echo "<ul>";
//                    self::submenu($menu->children, $group);
//                    echo "</ul>";
//                }
//                echo "</li>";
//            }
//        }
//    }
//
//    public static function menuManager($menus, $stat = 0){
//        foreach ($menus as $key => $value) {
//            if ($value->status == 1) {
//                $status = 'btn-danger';
//                $statusText = 'Unpublished';
//            }else{
//                $status = 'btn-success';
//                $statusText = 'Published';
//            }
//            self::$menu .=   "<tr>
//                                <td><input type='checkbox' data-id='$value->id' class='checkbox1' name='checkbox[]' value='$value->id'></td>
//                                <td><a href='' data-id='$value->id'  class='notify $status'>$statusText</a></td>
//                                <td>";
//            for ($i=0; $i < $stat; $i++) {
//                self::$menu .= " &#8594 ";
//            }
//            self::$menu .=           " $value->title</td>";
//            if(strlen($value->children) > 2){
//                $newStat = $stat+1;
//                self::menuManager($value->children , $newStat);
//            }
//            self::$menu .= "</tr>";
//        }
//        return self::$menu;
//    }
//
//    public static function frontMenuForNewWidget($menus,$maps="")
//    {
//        if(!isset($strmenu)){
//            $strmenu = '';
//        }
//        $strmenu .= "<ul>";
//        foreach($menus as $key => $menu){
//            $strmenu .= "<li>". Form::checkbox("menus[]", $menu->id,(is_array($maps) && in_array($menu->id, $maps) ? TRUE : FALSE)).$menu->name."</li>";
//            if(strlen($menu->children) > 2){
//                self::frontMenuForNewWidget($menu->children,$maps);
//            }
//        }
//        $strmenu .= "</ul>";
////        dd(DB::getQueryLog());
//        return $strmenu;
//    }
}