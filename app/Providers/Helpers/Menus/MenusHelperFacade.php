<?php
/**
 * Created by PhpStorm.
 * User: kharrazi
 * Date: 4/7/15
 * Time: 9:23 AM
 */

namespace App\Providers\Helpers\Menus;

use Illuminate\Support\Facades\Facade;

class MenusHelperFacade extends Facade
{

    protected static function getFacadeAccessor()
    {
        return 'menushelper';
    }

}