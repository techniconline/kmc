<?php

/**
 * Created by PhpStorm.
 * User: Hamid
 * Date: 13/01/2015
 * Time: 09:03 AM
 */
namespace App;

use App\Http\Controllers\backend\FeatureController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;

class Product extends BaseModel
{

    protected $table = 'products';
    protected $primaryKey = 'id';
    protected $fillable = ['is_active', 'active_date', 'category_id'
        , 'price', 'quantity', 'condition', 'image'
        , 'lat', 'lng'
        , 'user_id', 'status'];

    protected $hidden = ['remember_token'];
    public $errors;
    public $rules = [
        'category_id' => 'required|numeric',
        'title' => 'required',
        'alias' => 'required|unique:products_lang,alias',
    ];
    public $rules_upd = [
        'category_id' => 'required|numeric',
        'title' => 'required',
    ];

    public function getDataFeatures($lang_id, $featuresProduct , $search=false , $public_add=false, $search_advance=false)
    {

        $dataF = DB::table('features')
            ->join('features_lang', 'features_lang.feature_id', '=', 'features.id')
            ->where('features.status', '=', 1)
            ->where(function($query)use($search,$public_add,$search_advance){

                if($search){
                    $query->where('search',1);
                }

                if($public_add){
                    $query->where('public_add',1);
                }

                if($search_advance){
                    $query->where('search_advance',1);
                }

            })
            ->where('features_lang.lang_id', '=', $lang_id)
            ->select([
                'features_lang.name'
                , 'features.*'
//                , DB::raw('
//                (SELECT GROUP_CONCAT(CONCAT_WS("|",id,title,value)) FROM feature_value AS fv
//                INNER JOIN feature_value_lang AS fvl ON fv.id=fvl.feature_value_id AND lang_id=' . $lang_id . '
//                WHERE fv.feature_id=features.id) AS feature_value_list
//                ')
            ])
            ->get();

        $dataFV = DB::table('feature_value')
            ->join('feature_value_lang','feature_value.id','=','feature_value_lang.feature_value_id')
            ->where('lang_id',$lang_id)->get();

        $dataFV_new = [];
        foreach($dataFV as $item){
            $dataFV_new[$item->feature_id][] = ['id'=>$item->id, 'title'=>$item->title, 'value'=>$item->value];
        }

        $newDataFeature = [];
        $color_selected = [];
        foreach ($dataF as &$itemF) {

            if (isset($dataFV_new[$itemF->id]) && $dataFV_new[$itemF->id]) {
                $fvArr = $dataFV_new[$itemF->id];

                $cnt = 0;
                $feature_values = [];
                $feature_values_selected = '';
                $is_color = false;
                if ($itemF->type == 'color') {
                    $is_color = true;
                }
                foreach ($fvArr as $itemFV) {
                    $feature_values[$itemFV['id']] = $itemFV['title'] ? $itemFV['title'] : $itemFV['value'];
                    $fp = isset($featuresProduct[$itemFV['value']]) ? $featuresProduct[$itemFV['value']] : [];
                    if (in_array($itemFV['id'], $fp)) {
                        $feature_values_selected[] = !$itemF->custom ?$itemFV['id'] : $itemFV['value'];
                        if ($is_color) {
                            $color_selected[$itemFV['value']] = $itemFV['title'];
                        }
                    }
                    $cnt++;
                }
                if (isset($feature_values_selected) && count($feature_values_selected) > 1) {
                    $itemF->feature_value_selected = $feature_values_selected;

                } elseif (isset($feature_values_selected)) {
                    $itemF->feature_value_selected = isset($feature_values_selected[0]) ? $feature_values_selected[0] : 0;
                }

                $itemF->feature_values = $feature_values;


            }

            $newDataFeature[$itemF->id] = $itemF;

        }

        $fgIds=null;
        if($search){
            $featureIds = DB::table('features')->where('status',1)->where('search',$search)->lists('id');
            if($featureIds){
                $fgIds = DB::table('feature_group_relation')
                    ->whereIn('feature_id',$featureIds)->lists('feature_group_id');

                $fgIds = array_unique($fgIds);
            }

        }

        $dataFG = DB::table('feature_group')
            ->join('feature_group_lang', 'feature_group_lang.feature_group_id', '=', 'feature_group.id')
            ->where('feature_group.status', '=', 1)
            ->where(function($queryf)use($fgIds,$search){
                if($fgIds){
                    $queryf->whereIn('feature_group.id',$fgIds);
                }
            })
            ->where('feature_group_lang.lang_id', '=', $lang_id)
            ->select([
                'feature_group_lang.name'
                , 'feature_group.*'
                , DB::raw('(SELECT GROUP_CONCAT(feature_id) FROM feature_group_relation WHERE feature_group_id=feature_group.id) as list_features')
            ])
            ->get();

        $allDataFG = [];
        foreach ($dataFG as &$itemFG) {
            if ($itemFG->list_features) {
                $listArr = explode(",", $itemFG->list_features);
                $listFeature = [];
                foreach ($listArr as $itemLi) {
                    $listFeature[] = isset($newDataFeature[$itemLi]) ? $newDataFeature[$itemLi] : null;
                }

                $itemFG->features_array = $listFeature;
            }

            $allDataFG[] = $itemFG;

        }

        return [
            'dataFG' => $dataFG
            , 'allDataFG' => $allDataFG
            , 'color_selected' => $color_selected
        ];

    }

    public function getFeaturesProduct($product_id, $lang_id = 3)
    {

        $result = DB::table('feature_product AS fp')
            ->join('feature_value_lang AS fvl', 'fp.feature_value_id', '=', 'fvl.feature_value_id')
            ->join('features_lang AS fl', 'fp.feature_id', '=', 'fl.feature_id')
            ->join('features AS f', 'fl.feature_id', '=', 'f.id')
            ->join('feature_group_relation AS fgr', 'fp.feature_id', '=', 'fgr.feature_id')
            ->join('feature_group_lang AS fgl', 'fgr.feature_group_id', '=', 'fgl.feature_group_id')
            ->join('feature_group AS fg', 'fg.id', '=', 'fgl.feature_group_id')
            ->select(['fvl.value', 'fvl.title', 'fl.name', 'f.type', 'fl.feature_id', 'fgl.name AS name_group', 'fg.id'])
            ->where('fvl.lang_id', $lang_id)
            ->where('fgl.lang_id', $lang_id)
            ->where('fl.lang_id', $lang_id)
            ->where('fp.product_id', $product_id)
            ->orderBy('fg.position')
            ->orderBy('fg.id')
            ->orderBy('fl.feature_id')
            ->get();

        return $result;


    }

    public function saveProductCategories($product_id, $categories = [])
    {

        if (!count($categories))
            return false;

        $resDel = DB::table('product_categories')->where('product_id', $product_id)->delete();

        $insertItems = [];
        foreach ($categories as $item) {
            $insertItems[] = ['product_id' => $product_id, 'categories_id' => $item, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')];
        }

        $result = DB::table('product_categories')->insert(
            $insertItems
        );

        return $result;

    }

    public function saveProductLang($params)
    {
        $lang_id = $params['lang_id'];
        $id = $params['id'];
        $title = $params['title'];
        $alias = $params['alias'];
        $resAlias = DB::table('products_lang')->where('alias', $alias)->where('id', '!=', $id)->first();
        if ($resAlias) {
            $alias .= $id;
        }
        $description = $params['description'];
        $short_description = $params['short_description'];
        $result = DB::table('products_lang')->insert(
            [
                'id' => $id
                , 'title' => $title
                , 'alias' => $alias
                , 'description' => $description
                , 'short_description' => $short_description
                , 'lang_id' => $lang_id
            ]
        );

        return $result;

    }

    public function updateProductLang($params)
    {
        $lang_id = $params['lang_id'];
        $id = $params['id'];
        $title = $params['title'];
        $description = $params['description'];
        $alias = $params['alias'];
        $resAlias = DB::table('products_lang')->where('alias', $alias)->where('id', '!=', $id)->first();
        if ($resAlias) {
            $alias .= $id;
        }
        $short_description = $params['short_description'];
        $result = DB::table('products_lang')
            ->where('id', '=', $id)
            ->where('lang_id', '=', $lang_id)
            ->update(
                [
                    'title' => $title
                    , 'alias' => $alias
                    , 'description' => $description
                    , 'short_description' => $short_description
                ]
            );

        return $result;

    }

    public function updateProduct($product_id, $data, $mediaArr = 0, $mediaDelArr = 0)
    {
        unset($data['image']);
        $product = $this->find($product_id);
        $fill = $product->fill($data);
        $isValid = $product->validationData($data, $this->rules_upd);
        if (!$isValid) {
            return array('action' => false, 'message' => $this->errors);
        }


        $image_default = isset($data['image_default']) && $data['image_default'] ? $data['image_default'] : 0;

        $InputLang['title'] = $data['title'];
        $InputLang['alias'] = $data['alias'];
        $InputLang['lang_id'] = $data['lang_id'];
        $InputLang['description'] = $data['description'];
        $InputLang['short_description'] = $data['short_description'];
        $InputLang['id'] = $product_id;

        $result = $product->save();
        $resultLang = $this->updateProductLang($InputLang);

        //create New Feature Value
        $fvModel = new FeatureValue();
        // $input['features'] => ['feature_id'=>'feature_value_id']
        $features = isset($data['features'])&&$data['features']?$data['features']:null;
        $new_features_value = isset($data['new_features_value'])&&$data['new_features_value']?$data['new_features_value']:null;
        $resSaveFeatureValue = null;
        foreach ($new_features_value as $key => $value) {
            $input = ['value' => $value, 'title' => ''];
            $input['feature_id'] = $key;
            $input['lang_id'] = $data['lang_id'];
            $resSaveFeatureValue = $fvModel->storeFeatureValues($input);
            if ($resSaveFeatureValue['valid']) {
                $features[$key] = $resSaveFeatureValue['feature_value_id'];
            }
        }

        $resFeatureAdd = $fvModel->saveFeatureValueProduct($product_id, $features);

        $mediaModel = new Media();
        if ($mediaArr['images']) {
            $subMediaArr = array('ids' => $mediaArr['images'], 'titles' => $mediaArr['images_title']);
            $mediaModel->addMediaProduct($subMediaArr, $product_id);
        }

        if ($mediaArr['videos']) {
            $subMediaArr = array('ids' => $mediaArr['videos'], 'titles' => $mediaArr['videos_title']);
            $mediaModel->addMediaProduct($subMediaArr, $product_id);
        }

        $date = date('Y-m-d H:i:s');
        if ($mediaArr['images_class']) {
            foreach ($mediaArr['images_class'] as $k => $v) {
                $updData = [
                    'product_id' => $product_id
                    , 'class' => $v
                    , 'updated_at' => $date
                ];
                if ($mediaArr['images_sort'][$k]) {
                    $updData = array_merge($updData, ['sort' => $mediaArr['images_sort'][$k]]);
                }
                if ($mediaArr['images_feature_select'][$k]) {
                    $updData = array_merge($updData, ['feature_value_selected' => $mediaArr['images_feature_select'][$k]]);
                }

                $mediaModel->updateRoomMedia($k, $updData);
            }
        }


        if ($mediaDelArr) {
            $mediaModel->removeMedia($mediaDelArr);
        }

        return array('action' => true, 'message' => Lang::get('backend/Product.messages.msgUpdateOk'));


    }


    public function del($product_id)
    {

        if (!$product_id)
            return 0;

        $Input['user_id'] = Auth::employee()->get()->id;
        $Input['updated_at'] = date('Y-m-d H:i:s');
        $Input['status'] = 0;

        $result = DB::table('products')
            ->where('id', $product_id)
            ->where('status', 1)
            ->update($Input);


        if ($result) {
            return array('action' => true, 'message' => Lang::get('backend/Product.messages.msgDelOk'));
        } else {
            return array('action' => false, 'message' => Lang::get('backend/Product.messages.msgDelNotOk'));
        }

    }

    public function isDefault($product_id, $media_id)
    {

        if (!$product_id || !$media_id)
            return 0;

        $Input['user_id'] = Auth::employee()->get()->id;
        $Input['updated_at'] = date('Y-m-d H:i:s');

        $result = DB::table('product_media')
            ->where('product_id', $product_id)
            ->where('status', 1)
            ->update($Input);

        $Input['is_default'] = 1;

        $result = DB::table('product_media')
            ->where('product_id', $product_id)
            ->where('media_id', $media_id)
            ->where('status', 1)
            ->update($Input);

        if ($result) {
            return array('action' => true, 'message' => Lang::get('backend/Product.messages.msgUpdateOk'));
        } else {
            return array('action' => false, 'message' => Lang::get('backend/Product.messages.msgUpdateNotOk'));
        }

    }

    public function getProductCategories($product_id, $list = false)
    {
        $result = DB::table('product_categories')
            ->where('product_categories.product_id', '=', $product_id)
            ->get();

        if ($list) {

            $resultNew = [];
            foreach ($result as $item) {
                $resultNew[] = $item->categories_id;
            }

            return $resultNew;
        }

        return $result;
    }

} 