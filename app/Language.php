<?php

/**
 * Created by PhpStorm.
 * User: Hamid
 * Date: 13/01/2015
 * Time: 09:03 AM
 */
namespace App;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;

class Language extends BaseModel
{

    protected $table = 'language';
    protected $primaryKey = 'id';
    protected $fillable = ['name', 'is_code', 'language_code', 'date_format', 'date_format_full', 'active', 'is_rtl', 'status'];

    protected $hidden = ['remember_token'];
    public $errors;
    public $rules = ['name' => 'required'
        , 'is_code' => 'required'
        , 'language_code' => 'required'
        , 'date_format' => 'required'
        , 'date_format_full' => 'required'
        , 'active' => 'required'
        , 'is_rtl' => 'required'];

    /**
     * @return bool
     */
    public function isValid()
    {
        $validation = Validator::make($this->attributes, $this->rules);
        if ($validation->fails()) {
            $this->errors = $validation->messages();
            return false;
        }
        return true;
    }

    public function getLanguage($lang = 'fr')
    {

        $langDB = DB::table($this->table)->where('is_code', '=', $lang)->select(['id', 'is_rtl'])->first();

        if (!$langDB)
            return false;

        return ['lang' => $lang, 'lang_id' => $langDB->id, 'is_rtl' => $langDB->is_rtl];

    }

} 