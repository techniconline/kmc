<?php

/**
 * Created by PhpStorm.
 * User: Hamid
 * Date: 13/01/2015
 * Time: 09:03 AM
 */
namespace App;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;

class ContentLang extends BaseModel
{

    protected $table = 'contents_lang';
    public $timestamps = false;
    protected $fillable = ['contents_id', 'lang_id', 'link_title', 'browser_title', 'body_title', 'short_content', 'body_content', 'tags'];
//
    public $errors;
    public $rules = [
        'browser_title' => 'required',
        'link_title' => 'required',
        'body_title' => 'required',
        'body_content' => 'required'
    ];

    public static function updateLang($params)
    {
        $result = DB::table('contents_lang')
            ->where('contents_id', '=', $params['contents_id'])
            ->where('lang_id', '=', $params['lang_id'])
            ->update(
                [
                    'link_title' => $params['link_title'],
                    'browser_title' => $params['browser_title'],
                    'body_title' => $params['body_title'],
                    'tags' => $params['tags'],
                    'short_content' => $params['short_content'],
                    'body_content' => $params['body_content']
                ]
            );

        return $result;
    }


} 