<?php namespace App;

/**
 * Created by PhpStorm.
 * User: Hamid
 * Date: 13/01/2015
 * Time: 09:03 AM
 */
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class BaseModel extends Model
{

    public function getSystemModel($name = 'content')
    {
        if($name=='weblog' || $name=='news')
            $name = 'content';

        $model = "App\\".ucfirst($name);
        $objModel = new $model();

        if($objModel)
            return $objModel;

        return false;

    }

    function removeDirectory($directory)
    {
        foreach(glob("{$directory}/*") as $file)
        {
            if(is_dir($file)) {
                $this->removeDirectory($file);
            } else {
                unlink($file);
            }
        }
        $result = rmdir($directory);

        return $result;
    }

    public function checkPermission($request, $user_id)
    {

//        dd($request['currentRoute']->getActionName(), $user_id);

        if ($request['prefix'] == 'backend') {
//            return true;
//
//            $routeAddress = Route::getRoutes()->getByName($request['prefix'].
//                '.'.$request['controller'].
//                '.'.(isset($request['functionCalled'])&&$request['functionCalled']?$request['functionCalled']:'index'))->getActionName();

            $routeAddress = $request['currentRoute']->getActionName();
            $routeArr = explode("@", $routeAddress);
            $function = end($routeArr);
            $routeArr = explode("\\", $routeArr[0]);
            $controller = end($routeArr);

            $userGroups = UserAccessGroup::where('employee_id', $user_id)->where('status', 1)->lists('access_group_id');
            $actionCall = ActionsList::where('prefix', $request['prefix'])
                ->where('controller', $controller)
                ->where('function', $function)
                ->where('method', strtolower($request['method']))->first();

            if (!$actionCall)
                return false;

            $userAccess = AccessList::whereIn('access_group_id', $userGroups)
                ->where('action_id', $actionCall->id)
                ->where('status', 1)
                ->where('access', 1)->first();

            if ($userAccess)
                return true;

            return false;
        } else {
            //for front and public
            return true;
        }

    }

    const level_success = 'success';
    const level_warning = 'warning';
    const level_danger = 'danger';

    public function isValid()
    {
        $validation = Validator::make($this->attributes, $this->rules);
        if ($validation->fails()) {
            $this->errors = $validation->messages();
            return false;
        }
        return true;
    }

    public function validationData($attributes, $rules)
    {
        $validation = Validator::make($attributes, $rules);
        if ($validation->fails()) {
            $this->errors = $validation->messages();
            return false;
        }
        return true;
    }

    public function getMediaUpload($params){

        $system = isset($params['ctrl']) && $params['ctrl'] ? $params['ctrl'] : null;
        $systemDoc = isset($params['ctrlDoc']) && $params['ctrlDoc'] ? $params['ctrlDoc'] : null;
        $itemId = isset($params['itemId']) && $params['itemId'] ? $params['itemId'] : null;
        $type = isset($params['type']) && $params['type'] ? $params['type'] : 'image';

        if (!$system || !$itemId)
            return ['action' => false];


        $results = DB::table('modules_media')
            ->join('media', 'modules_media.media_id', '=', 'media.id')
            ->select('modules_media.*','media.src','media.type')
            ->where('media.type', '=', $type)
            ->where('modules_media.system', '=', $system)
            ->where('modules_media.item_id', '=', $itemId)
            ->where('media.status', '=', 1)
            ->where('modules_media.status', '=', 1)
            ->orderBy('modules_media.sort')
            ->get();

        foreach ($results as &$item) {
            if ($item->type == 'video') {
                $item->video = asset($item->src);
            } elseif($item->type == 'image'){
                $item->image = asset($item->src);
            } else {
                $item->doc = asset($item->src);
            }
        }

        return $results;


    }

    public function saveMediaUpload($params)
    {
        $system = isset($params['ctrl']) && $params['ctrl'] ? $params['ctrl'] : null;
        $systemDoc = isset($params['ctrlDoc']) && $params['ctrlDoc'] ? $params['ctrlDoc'] : null;
        $itemId = isset($params['itemId']) && $params['itemId'] ? $params['itemId'] : null;
        $mediaId = isset($params['media_id']) && $params['media_id'] ? $params['media_id'] : null;
        $mediaTitle = isset($params['media_title']) && $params['media_title'] ? $params['media_title'] : null;
        $mediaTitles = isset($params['media_titles']) && $params['media_titles'] ? $params['media_titles'] : null;
        $mediaIds = isset($params['media_ids']) && $params['media_ids'] ? $params['media_ids'] : null;
        $userId = isset($params['user_id']) && $params['user_id'] ? $params['user_id'] : null;
        $employeeId = isset($params['employee_id']) && $params['employee_id'] ? $params['employee_id'] : null;

        if (!$system || !$itemId || (!$mediaId && $mediaIds))
            return ['action' => false];

        $insertValue=null;
        if($mediaIds){
            foreach($mediaIds as $item){
                $insertValue[] = ['media_id'=>$item, 'system'=>$system
                    , 'title'=> $mediaIds[key($item)]
                    , 'user_id'=>$userId , 'employee_id'=>$employeeId
                    , 'item_id'=>$itemId, 'status'=>1 , 'created_at'=>date('Y-m-d h:i:s')];
            }
        }elseif($mediaId){
            $insertValue = ['media_id'=>$mediaId, 'system'=>$system
                , 'title'=>$mediaTitle
                , 'user_id'=>$userId , 'employee_id'=>$employeeId
                , 'item_id'=>$itemId , 'status'=>1 , 'created_at'=>date('Y-m-d h:i:s')];
        }

        if(!$insertValue)
            return ['action' => false];

        $res = DB::table('modules_media')->insertGetId($insertValue);

        return ['action' => true, 'res'=>$res];

    }

    public function deleteMediaUpload($params)
    {
        $system = isset($params['ctrl']) && $params['ctrl'] ? $params['ctrl'] : null;
        $systemDoc = isset($params['ctrlDoc']) && $params['ctrlDoc'] ? $params['ctrlDoc'] : null;
        $itemId = isset($params['itemId']) && $params['itemId'] ? $params['itemId'] : null;
        $mediaId = isset($params['media_id']) && $params['media_id'] ? $params['media_id'] : null;
        $mediaIds = isset($params['media_ids']) && $params['media_ids'] ? $params['media_ids'] : null;
        $userId = isset($params['user_id']) && $params['user_id'] ? $params['user_id'] : 0;
        $employeeId = isset($params['employee_id']) && $params['employee_id'] ? $params['employee_id'] : 0;

        if (!$system || !$itemId || (!$mediaId && $mediaIds))
            return ['action' => false];

        $updateItems = [
            'status'=>0
            , 'updated_at'=>date('Y-m-d h:i:s')
        ];

        if($employeeId){
            $updateItems = array_merge($updateItems,['employee_id'=> $employeeId]);
        }
        if($userId){
            $updateItems = array_merge($updateItems,['user_id'=> $userId]);
        }

        $res = DB::table('modules_media')
            ->where('system',$system)
            ->where('status',1)
            ->where('item_id',$itemId)
            ->where(function($query)use($mediaIds,$mediaId){
                if($mediaId){
                    $query->where('media_id',$mediaId);
                }
                if($mediaIds){
                    $query->whereIn('media_id',$mediaIds);
                }
            })
            ->update($updateItems);

        return ['action' => true, 'res'=>$res];

    }

    public function saveItemLang($params)
    {

        $table = $params['table'];
        $arrItems = $params['items']; //['id' => $id, , 'title' => $title , 'lang_id' => $lang_id]

        if (!$table || !$arrItems)
            return false;

        $result = DB::table($table)->insert($arrItems);

        return $result;

    }

    public function updateItemLang($params)
    {
        $lang_id = $params['lang_id'];
        $id = $params['id'];
        $name_id_field = isset($params['name_id_field']) ? $params['name_id_field'] : 'id';
        $table = $params['table'];
        $arrItems = $params['items']; //['title' => $title ]

        if (!$lang_id || !$id || !$table || !$arrItems)
            return false;

        $result = DB::table($table)
            ->where($name_id_field, '=', $id)
            ->where('lang_id', '=', $lang_id)
            ->update($arrItems);
        return $result;

    }


    public function createSiteMap()
    {


        // create new sitemap object
        $sitemap = App::make("sitemap");

        // set cache (key (string), duration in minutes (Carbon|Datetime|int), turn on/off (boolean))
        // by default cache is disabled
//    $sitemap->setCache('laravel.sitemap', 1); //TODO

        // check if there is cached sitemap and build new only if is not
        if (!$sitemap->isCached())
        {
            // add item to the sitemap (url, date, priority, freq)
            $sitemap->add(URL::to('/'), '2013-08-25T20:10:00+02:00', '1.0', 'daily');
            $sitemap->add(URL::to('/category'), '2015-11-26T12:30:00+02:00', '0.9', 'monthly');

            // get all posts from db, with image relations
            $posts = DB::table('contents')
                ->join('contents_lang', 'contents.id','=','contents_lang.contents_id')
                ->where('status', 1)
                ->whereIn('type', ['news','blog','weblog'])
                ->orderBy('id', 'DESC')->get();

            // add every post to the sitemap
            foreach ($posts as &$post)
            {
                // get all images for the current post
                $images = [];
//            foreach ($post->images as $image) {
                $imageUrl = asset( $post->image_content . $post->id . '-600x400.' . $post->extension);
                $images[] = array(
                    'url' => $imageUrl,
                    'title' => $post->link_title,
                    'caption' => $post->browser_title
                );
//            }

                $post->url = route(($post->type=='blog'?'content':$post->type).'.show',$post->url);

                $sitemap->add($post->url, $post->created_at, $post->sort, $post->link_title, $images);
            }

            // get all product from db, with image relations
            $products = DB::table('products')
                ->join('products_lang', 'products.id','=','products_lang.id')
                ->where('status', 1)
                ->orderBy('products.id', 'DESC')->get();

            foreach ($products as &$item)
            {
                // get all images for the current post
                $images = [];
                $images[] = array(
                    'url' => asset($item->image),
                    'title' => $item->title,
                    'caption' => $item->title
                );

                $item->alias =route('product.show',[$item->alias]);

                $sitemap->add($item->alias, $item->created_at, 1, $item->title, $images);
            }

        }

        // show your sitemap (options: 'xml' (default), 'html', 'txt', 'ror-rss', 'ror-rdf')
        return $sitemap->render('xml');


    }


}