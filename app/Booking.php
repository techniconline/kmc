<?php

/**
 * Created by PhpStorm.
 * User: Hamid
 * Date: 13/01/2015
 * Time: 09:03 AM
 */
namespace App;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;

class Booking extends BaseModel
{

    protected $table = 'booking';
    protected $primaryKey = 'id';
    protected $fillable = ['user_id', 'from_date', 'to_date', 'accepted_terms', 'accepted_house_rules', 'status'];

    protected $hidden = ['remember_token'];
    public $errors;
    public $rules = [
        'user_id' => 'required|numeric'
        , 'from_date' => 'required'
        , 'to_date' => 'required'
    ];
    public $rulesAccept = [
        'user_id' => 'required|numeric'
        , 'accepted_house_rules' => 'required'
        , 'accepted_terms' => 'required'
    ];

    public function isValid()
    {
        $validation = Validator::make($this->attributes, $this->rules);
        if ($validation->fails()) {
            $this->errors = $validation->messages();
            return false;
        }
        return true;
    }

    public function isValidAccept()
    {
        $validation = Validator::make($this->attributes, $this->rulesAccept);
        if ($validation->fails()) {
            $this->errors = $validation->messages();
            return false;
        }
        return true;
    }

    public function getBookingDetails($booking_id, $lastData = false, $type = 'room')
    {

        $result = DB::table('booking_details')
            ->join('booking', 'booking_details.booking_id', '=', 'booking.id')
            ->where('booking.id', '=', $booking_id)
            ->where('booking.status', '=', 1)
            ->where('booking_details.status', '=', 1)
            ->where(function ($query) use ($type) {
                $query->where('booking_details.type', '=', $type);
            })
            ->select(['booking_details.*', 'booking.from_date', 'booking.to_date'])
            ->orderBy('booking_details.id', 'DESC');

        if ($lastData) {
            $result = $result->first();
        } else {
            $result = $result->get();
        }

        return $result;
    }

    public function getCountBookingDetails($booking_id, $type = 'room')
    {

        $result = DB::table('booking_details')
            ->where('booking_details.booking_id', '=', $booking_id)
            ->where('booking_details.status', '=', 1)
            ->where(function ($query) use ($type) {
                $query->where('booking_details.type', '=', $type);
            });

        $result = $result->count();

        return $result;
    }

    public function checkValidationBookingDetails($booking_id, $apartment_room_category_map_id, $user_id)
    {

        $result = DB::table('booking_details')
            ->where('booking_details.booking_id', '=', $booking_id)
            ->where('booking_details.apartment_room_category_map_id', '=', $apartment_room_category_map_id)
            ->where('booking_details.user_id', '=', $user_id)
            ->where('booking_details.status', '=', 1)
            ->select(['booking_details.*'])
            ->orderBy('booking_details.id', 'DESC')
            ->first();

        return $result;
    }

    public function insertBookingDetails($insertArr)
    {
        $result = DB::table('booking_details')->insert(
            $insertArr
        );

        return $result;
    }

    public function getBookingDetailsListRooms($booking_id, $user_id)
    {
        $language = new Language();
        $lang = $language->getLanguage(app('getLocale'));

        $result = DB::table('apartment_room_category_map')
            ->join('room_category', 'apartment_room_category_map.room_category_id', '=', 'room_category.id')
            ->join('room_category_lang', 'room_category.id', '=', 'room_category_lang.id')
            ->join('booking_details', 'apartment_room_category_map.id', '=', 'booking_details.apartment_room_category_map_id')
            ->join('booking', 'booking_details.booking_id', '=', 'booking.id')
            ->leftJoin('apartment_room_price', function ($join) {
                $join->on('apartment_room_category_map.room_category_id', '=', 'apartment_room_price.room_category_id')
                    ->where('apartment_room_price.price_status', '=', 1)
                    ->where('apartment_room_price.status', '=', 1);
            })
            ->where('booking_details.user_id', '=', $user_id)
            ->where('booking.id', '=', $booking_id)
            ->where('booking_details.status', '=', 1)
            ->where('booking.status', '=', 1)
            ->where('room_category_lang.lang_id', '=', $lang['lang_id'])
            ->where('apartment_room_category_map.status', '=', 1)
            ->where('room_category.status', '=', 1)
            ->select(['apartment_room_category_map.*'
                , 'apartment_room_price.rent_price'
                , 'room_category.area', 'room_category.type'
                , 'room_category_lang.title', 'room_category_lang.short_description'
                , 'room_category_lang.description'
                , 'booking_details.apartment_room_category_map_id'
                , 'booking_details.id as booking_details_id'
            ])
            ->orderBy('room_category.area', 'DESC')
            ->orderBy('apartment_room_price.rent_price', 'DESC')
            ->orderBy('apartment_room_category_map.id')
            ->get();
        return $result;
    }

    public function getListBookingUsersWithDetails($params = null)
    {

        $user_id = isset($params['user_id']) ? $params['user_id'] : 0;
        $booking_id = isset($params['booking_id']) ? $params['booking_id'] : 0;
        $paginateNum = isset($params['paginateNum']) ? $params['paginateNum'] : 10;

        $archive = $params['archive'];

        $date_from = $params['date_from'];
        $date_to = $params['date_to'];

        $reg_date_from = $params['reg_date_from'];
        $reg_date_to = $params['reg_date_to'];

        $first_name = $params['first_name'];
        $last_name = $params['last_name'];

        $gender = $params['gender'];

        $country_name = $params['country_name'];

        $results = DB::table('booking')
            ->join('users', 'booking.user_id', '=', 'users.id')
            ->leftjoin('info_users', 'users.id', '=', 'info_users.user_id')
            ->leftjoin('user_parent', 'users.id', '=', 'user_parent.user_id')
            ->leftjoin('countries', 'users.country_id', '=', 'countries.id')
            ->select('booking.*', 'users.firstname', 'users.lastname', 'users.mobile'
                , 'users.email', 'users.gender', 'users.birth_date', 'users.created_at as register_date_user'
                , 'countries.name as country_name'
                , 'info_users.persona', 'info_users.education', 'info_users.institution_type', 'info_users.institution', 'info_users.institution_town'
                , 'info_users.current_institution_type', 'info_users.current_institution', 'info_users.current_institution_town'
                , 'info_users.parent_guarantor', 'info_users.accuracy_information', 'info_users.personal_message'
                , 'user_parent.parent_first_name', 'user_parent.parent_last_name', 'user_parent.parent_email', 'user_parent.parent_mobile'
                , 'user_parent.parent_tenant_address', 'user_parent.parent_address', 'user_parent.parent_postal_code', 'user_parent.parent_country_id'
                , DB::raw('(SELECT name FROM countries WHERE id=user_parent.parent_country_id) as parent_country_name')
                , DB::raw('(SELECT GROUP_CONCAT(type,"=",rent_status) FROM booking_details WHERE booking_id=booking.id GROUP BY booking_id) as all_rent_status')
            )
            ->where(function ($query) use ($archive) {
                if ($archive === 'all') {
                    $query->where('booking.status', '!=', 0);
                } elseif ($archive) {
                    $query->where('booking.status', '!=', 1);
                } else {
                    $query->where('booking.status', '=', 1);
                }

            })
            ->where('users.status', '=', 1)
            ->where(function ($query) use (
                $user_id, $booking_id
                , $date_from, $date_to, $reg_date_from, $reg_date_to, $first_name, $last_name, $gender, $country_name
            ) {
                if ($user_id && $query) {
                    $query->where('users.id', '=', $user_id);
                }
                if ($booking_id && $query) {
                    $query->where('booking.id', '=', $booking_id);
                }
                if ($date_from && $query) {
                    $query->where('booking.from_date', '>=', $date_from);
                }
                if ($date_to && $query) {
                    $query->where('booking.to_date', '<=', $date_to);
                }
                if ($reg_date_from && $query) {
                    $query->where('booking.created_at', '>=', $reg_date_from . ' 00:00:00');
                }
                if ($reg_date_to && $query) {
                    $query->where('booking.created_at', '<=', $reg_date_to . ' 23:59:59');
                }
                if ($first_name && $query) {
                    $query->where('users.firstname', 'like', '%' . $first_name . '%');
                }
                if ($last_name && $query) {
                    $query->where('users.lastname', 'like', '%' . $last_name . '%');
                }
                if ($country_name && $query) {
                    $query->where('countries.name', 'like', '%' . $country_name . '%');
                }
                if ($gender && $query) {
                    $query->where('users.gender', '=', $gender);
                }
            })
            ->orderBy('booking.status_view')->orderBy('booking.id')->paginate($paginateNum);

        $bookingIds = [];
        foreach ($results as $item) {
            $bookingIds[] = $item->id;
        }

        $params['bookingIds'] = $bookingIds;
        $resultDetails = $this->getListBookingDetailsUsers($params);

        $arcmIds = [];
        $paramsRoom = [];
        foreach ($results as &$item) {
            $item->rentStatus = ['room' => 0, 'parking' => 0];
            if ($item->all_rent_status) {
                $arrRentStatus = explode(",", $item->all_rent_status);
                $arrRentStatus = array_unique($arrRentStatus);
                $rentStatus = [];
                foreach ($arrRentStatus as $itemRS) {
                    $arrRS = explode("=", $itemRS);
                    $typeRS = $arrRS[0];
                    $statusRS = $arrRS[1];
                    $rentStatus[$typeRS] = $statusRS;
                }
                $item->rentStatus = $rentStatus;
            }

            foreach ($resultDetails as $itemD) {

                if ($item->id == $itemD->booking_id) {
                    $item->details[] = $itemD;
                    $arcmIds[] = $itemD->id;

                }
            }
            //get list rooms
            $item->roomReserved = $this->getRoomReserved($item->user_id, $item->id, 'room');
            $item->parkingReserved = $this->getRoomReserved($item->user_id, $item->id, 'parking');

        }


        return $results;

    }

    public function getListBookingDetailsUsers($params = null)
    {

        $user_id = isset($params['user_id']) ? $params['user_id'] : 0;
        $bookingIds = isset($params['bookingIds']) ? $params['bookingIds'] : 0;
        $lang_id = isset($params['lang_id']) ? $params['lang_id'] : 0;
        $archive = isset($params['archive']) ? $params['archive'] : 0;

        if (!$lang_id)
            return false;


        $result = DB::table('apartment_room_category_map')
            ->join('room_category', 'apartment_room_category_map.room_category_id', '=', 'room_category.id')
            ->join('room_category_lang', 'room_category.id', '=', 'room_category_lang.id')
            ->join('booking_details', 'apartment_room_category_map.id', '=', 'booking_details.apartment_room_category_map_id')
            ->join('booking', 'booking_details.booking_id', '=', 'booking.id')
            ->leftJoin('apartment_room_price', function ($join) {
                $join->on('apartment_room_category_map.room_category_id', '=', 'apartment_room_price.room_category_id')
                    ->where('apartment_room_price.price_status', '=', 1)
                    ->where('apartment_room_price.status', '=', 1);
            })
            ->where(function ($query) use ($user_id, $bookingIds) {
                if ($user_id) {
                    $query->where('booking_details.user_id', '=', $user_id);
                }
                if ($bookingIds) {
                    $query->whereIn('booking.id', $bookingIds);
                }
            })
            ->where('booking_details.status', '=', 1)
            ->where(function ($query) use ($archive) {
                if ($archive === 'all') {
                    $query->where('booking.status', '!=', 0);
                } elseif ($archive) {
                    $query->where('booking.status', '!=', 1);
                } else {
                    $query->where('booking.status', '=', 1);
                }

            })
            ->where('room_category_lang.lang_id', '=', $lang_id)
            ->where('apartment_room_category_map.status', '=', 1)
            ->where('room_category.status', '=', 1)
            ->select(['apartment_room_category_map.*'
                , 'apartment_room_price.rent_price', 'apartment_room_price.to_rent_price'
                , 'room_category.area', 'room_category.to_area', 'room_category.type'
                , 'room_category_lang.title', 'room_category_lang.short_description'
                , 'room_category_lang.description'
                , 'booking_details.apartment_room_category_map_id'
                , 'booking_details.id as booking_details_id'
                , 'booking_details.booking_id'
                , 'booking_details.rent_status'
            ])
            ->orderBy('room_category.area', 'DESC')
            ->orderBy('apartment_room_price.rent_price', 'DESC')
            ->orderBy('apartment_room_category_map.id')
            ->get();
        return $result;

    }


    public function rejectBookingDetailsType($booking_id, $type)
    {
        $resUpdBookingDetails = DB::table('booking_details')
            ->where('booking_id', $booking_id)
            ->where('type', $type)
            ->where('status', 1)
            ->update([
                'status' => 0
                , 'updated_at' => date('Y-m-d H:i:s')
            ]);

        if (!$resUpdBookingDetails)
            return ['action' => false, 'message' => Lang::get('backend/Booking.messages.changeBookingStatusNotOk')];

        return ['action' => true, 'message' => Lang::get('backend/Booking.messages.changeBookingStatusOk')];


    }


    public function changeBookingUser($booking_id, $status = 3)
    {

        $resUpdBooking = DB::table('booking')
            ->where('id', $booking_id)
            ->update([
                'status' => $status
                , 'updated_at' => date('Y-m-d H:i:s')
            ]);

        $resUpdBookingDetails = DB::table('booking_details')
            ->where('booking_id', $booking_id)
            ->where('status', 1)
            ->update([
                'rent_status' => $status
                , 'updated_at' => date('Y-m-d H:i:s')
            ]);

        if (!$resUpdBooking)
            return ['action' => false, 'message' => Lang::get('backend/Booking.messages.changeBookingStatusNotOk')];

        return ['action' => true, 'message' => Lang::get('backend/Booking.messages.changeBookingStatusOk')];


    }

    public function getRoomReserved($user_id, $booking_id, $type = 'room')
    {

        if (!$user_id || !$booking_id || !$type)
            return false;

        $result = DB::table('rent')
            ->where('user_id', $user_id)
            ->where('booking_id', $booking_id)
            ->where('type', $type)
            ->where('status', 1)
            ->where('rent_status', '!=', 0)
            ->orderBy('id', 'DESC')
            ->first();

        if (!$result)
            return false;

        return $result;

    }

    public function getBookingWithUserData($booking_id)
    {

        $results = DB::table('booking')
            ->join('users', 'booking.user_id', '=', 'users.id')
            ->select('booking.*', 'users.firstname', 'users.lastname', 'users.mobile'
                , 'users.email', 'users.gender', 'users.birth_date'
            )
            ->where('users.status', '=', 1)
            ->where(function ($query) use ($booking_id) {
                if ($booking_id && $query) {
                    $query->where('booking.id', '=', $booking_id);
                }
            })
            ->orderBy('booking.id')->get();

        if ($booking_id) {
            $results = $results[0];
        }

        return $results;

    }

    public function getRentDataUser($booking_id, $type = 'room')
    {

        $result = DB::table('rent')
            ->join('users', 'rent.user_id', '=', 'users.id')
            ->join('rooms', 'rent.room_id', '=', 'rooms.id')
            ->join('room_category', 'rooms.room_category_id', '=', 'room_category.id')
            ->select(
                'rent.*'
                , 'users.firstname', 'users.lastname'
                , 'users.gender', 'users.birth_date'
                , 'users.code_postal', 'users.address', 'users.city'
                , 'rooms.price', 'rooms.room_number', 'room_category.area'
                , DB::raw('(SELECT name FROM countries WHERE id=users.country_id) as country_name')
                , DB::raw('(SELECT rent_price FROM apartment_room_price WHERE room_category_id=rent.room_category_id AND status=1 ORDER BY id DESC LIMIT 1) as rent_price')
            )
            ->where('rent.booking_id', '=', $booking_id)
            ->where('rent.type', '=', $type)
            ->where('rent.rent_status', '=', 3)
            ->where('room_category.status', '=', 1)
            ->where('rooms.status', '=', 1)
            ->where('rent.status', '=', 1)
            ->orderBy('rent.id', 'DESC')
            ->first();

        return $result;

    }

    public function getLastRentDataUser($user_id, $type = 'room')
    {

        if (!$user_id)
            return false;

        $result = DB::table('rent')
            ->join('users', 'rent.user_id', '=', 'users.id')
            ->join('rooms', 'rent.room_id', '=', 'rooms.id')
            ->select(
                'rent.*'
                , 'users.firstname', 'users.lastname', 'users.email'
                , 'rooms.room_number'
                , 'rooms.username_internet', 'rooms.password_internet', 'rooms.digit_electric_controller'
            )
            ->where('rent.user_id', '=', $user_id)
            ->where('rent.type', '=', $type)
            ->where('rent.rent_status', '=', 3)
            ->where('rooms.status', '=', 1)
            ->where('rooms.room_status', '=', 3)
            ->where('rent.status', '=', 1)
            ->orderBy('rent.id', 'DESC')
            ->first();

        return $result;

    }

} 