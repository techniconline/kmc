<?php

/**
 * Created by PhpStorm.
 * User: Hamid
 * Date: 13/01/2015
 * Time: 09:03 AM
 */
namespace App;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;

class AttachTypeGroup extends BaseModel
{

    protected $table = 'attach_type_group';
    protected $primaryKey = 'id';
    protected $fillable = ['alias', 'condition', 'group_status', 'user_id', 'status'];

    protected $hidden = ['remember_token'];
    public $errors;
    public $rules = ['alias' => 'required', 'user_id' => 'required'];

    /**
     * @return bool
     */
    public function isValid()
    {
        $validation = Validator::make($this->attributes, $this->rules);
        if ($validation->fails()) {
            $this->errors = $validation->messages();
            return false;
        }
        return true;
    }

    public function saveGroupLang($params)
    {
        $lang_id = $params['lang_id'];
        $id = $params['id'];
        $title = $params['title'];
        $description = $params['description'];
        $result = DB::table('attach_type_group_lang')->insert(
            [
                'id' => $id
                , 'title' => $title
                , 'description' => $description
                , 'lang_id' => $lang_id
            ]
        );

        return $result;

    }

    public function updateGroupLang($params)
    {
        $lang_id = $params['lang_id'];
        $id = $params['id'];
        $title = $params['title'];
        $description = $params['description'];
        $result = DB::table('attach_type_group_lang')
            ->where('id', '=', $id)
            ->where('lang_id', '=', $lang_id)
            ->update(
                [
                    'title' => $title
                    , 'description' => $description
                ]
            );

        return $result;

    }

    public function addMaxFileUploadToDataGroup($data, $have_items = true)
    {
        if ($have_items) {

            foreach ($data as &$item) {
                if (strpos($item->condition, 'MAX_UPLOAD_USER') !== false) {

                    $strNum1 = strcspn($item->condition, "[");
                    $subStr = substr($item->condition, $strNum1);
                    $strNum2 = strcspn($subStr, "]");
                    $strLen = $strNum2 - 1;
                    $strMaxUpload = substr($item->condition, $strNum1 + 1, $strLen);
                    $arrMaxUpload = explode("=", $strMaxUpload);
                    $item->max_upload = $max_upload = (int)$arrMaxUpload[1];

                }
            }

        } else {

            if (strpos($data->condition, 'MAX_UPLOAD_USER') !== false) {

                $strNum1 = strcspn($data->condition, "[");
                $subStr = substr($data->condition, $strNum1);
                $strNum2 = strcspn($subStr, "]");
                $strLen = $strNum2 - 1;
                $strMaxUpload = substr($data->condition, $strNum1 + 1, $strLen);
                $arrMaxUpload = explode("=", $strMaxUpload);
                $data->max_upload = $max_upload = (int)$arrMaxUpload[1];

            }

        }


        return $data;
    }

} 