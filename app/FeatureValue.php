<?php

/**
 * Created by PhpStorm.
 * User: Hamid
 * Date: 13/01/2015
 * Time: 09:03 AM
 */
namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;
use Laracasts\Flash\Flash;

class FeatureValue extends BaseModel
{

    protected $table = 'feature_value';
    protected $primaryKey = 'id';
    protected $fillable = ['feature_id'];

    protected $hidden = ['remember_token'];
    public $errors;
    public $rules = ['feature_id' => 'required'];

    public function storeFeatureValues($input)
    {
        $fill = $this->fill($input);
        $isValid = $fill->isValid();
        if (!$isValid || !isset($input['value']) || !$input['value']) {
            return ['valid' => false, 'message' => Lang::get('backend/Features.messages.errCreate'), 'err_list' => $this->errors];
        }

        $dataValue = DB::table('feature_value_lang')
            ->join('feature_value', 'feature_value.id', '=', 'feature_value_lang.feature_value_id')
            ->where('feature_value_lang.lang_id', $input['lang_id'])
            ->where('feature_id', $input['feature_id'])
            ->where('value', $input['value'])
            ->where('title', $input['title'])
            ->select('feature_value_id')->first();
        $id = isset($dataValue->feature_value_id) ? $dataValue->feature_value_id : 0;
        if (!$id) {
            $id = DB::table($this->table)->insertGetId(
                ['feature_id' => $input['feature_id']
                    , 'created_at' => date('Y-m-d H:i:s')
                    , 'updated_at' => date('Y-m-d H:i:s')
                ]);
        } else {

            return ['valid' => true, 'feature_id' => $input['feature_id'], 'feature_value_id' => $id, 'message' => Lang::get('backend/Features.messages.msgInsertOk')];

        }

        if ($id) {
            $language = new Language();
            $params['table'] = 'feature_value_lang';
            foreach (Config::get('app.locales') as $key => $value) {
                $lang = $language->getLanguage($key);

                $params['items'] = ['feature_value_id' => $id, 'value' => $input['value'], 'title' => $input['title'], 'lang_id' => $lang['lang_id']];
                $result = $this->saveItemLang($params);
            }
            return ['valid' => true, 'feature_id' => $input['feature_id'], 'feature_value_id' => $id, 'message' => Lang::get('backend/Features.messages.msgInsertOk')];

        }
        return ['valid' => false, 'message' => Lang::get('backend/Features.messages.errCreate')];
    }

    /**
     * @param $product_id
     * @param $values ['feature_id'=>'feature_value_id']
     * @return mixed
     */
    public function saveFeatureValueProduct($product_id, $values)
    {

        if(!$values)
            return false;

        DB::table('feature_product')->where('product_id', $product_id)->delete();

        $itemInsert = [];
        foreach ($values as $key => $value) {
            if (is_array($value)) {

                foreach ($value as $item) {
                    $itemInsert[] = ['product_id' => $product_id, 'feature_id' => $key, 'feature_value_id' => $item];
                }

            } else {
                $itemInsert[] = ['product_id' => $product_id, 'feature_id' => $key, 'feature_value_id' => $value];
            }
        }

        $resInsert = DB::table('feature_product')->insert($itemInsert);

        return $resInsert;

    }

} 