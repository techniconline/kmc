<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class FaqLang extends BaseModel
{

    protected $table = 'faq_lang';

    protected $fillable = ['faq_id', 'lang_id', 'question', 'answer'];

    public $errors;
    public $timestamps = false;
    public $rules = [
        'question' => 'required',
        'answer' => 'required'
    ];

    public static function updateLang($params)
    {
        $result = DB::table('faq_lang')
            ->where('faq_id', '=', $params['faq_id'])
            ->where('lang_id', '=', $params['lang_id'])
            ->update(
                [
                    'question' => $params['question'],
                    'answer' => $params['answer']
                ]
            );

        return $result;
    }
}