<?php

/**
 * Created by PhpStorm.
 * User: Hamid
 * Date: 13/01/2015
 * Time: 09:03 AM
 */
namespace App;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;

class Content extends BaseModel
{

    protected $table = 'contents';
    protected $fillable = ['url', 'author_id', 'employee_id', 'status', 'type', 'login', 'image_content', 'sort', 'extension'];
//
    public $errors;
    public $rules = [
        'url' => 'required|unique:contents,url',
        'author_id' => 'required',
        'type' => 'required',
        'browser_title' => 'required',
        'link_title' => 'required',
        'body_title' => 'required',
        'body_content' => 'required',
        'category_id' => 'required',
    ];

    public function selectJoinLang($field)
    {
        return $this->where(key($field), $field)->join('contents_lang', 'contents_id', '=', 'id')
            ->where('contents_lang.lang_id', '=', app('activeLangDetails')['lang_id']);
    }

    public function saveContentCategories($content_id, $categories = [])
    {
        $resDel = DB::table('content_categories')->where('content_id', $content_id)->delete();

        $insertItems = [];
        $result=false;
        if($categories){
            foreach ($categories as $item) {
                $insertItems[] = ['content_id' => $content_id, 'categories_id' => $item, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')];
            }

            $result = DB::table('content_categories')->insert(
                $insertItems
            );
        }
        return $result;

    }

    public function getContentCategories($content_id, $list = false)
    {
        $result = DB::table('content_categories')
            ->where('content_categories.content_id', '=', $content_id)
            ->get();

        if ($list) {

            $resultNew = [];
            foreach ($result as $item) {
                $resultNew[] = $item->categories_id;
            }

            return $resultNew;
        }

        return $result;
    }

} 