<?php namespace App;

use Baum\Node;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CategoryLang extends Model
{

    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'categories_lang';

    public static function updateLang($params)
    {
        $result = DB::table('categories_lang')
            ->where('categories_id', '=', $params['categories_id'])
            ->where('lang_id', '=', $params['lang_id'])
            ->update(
                [
                    'name' => $params['name'],
                    'desc' => $params['desc']
                ]
            );

        return $result;
    }

    public static function saveLang($params)
    {
        $result = DB::table('categories_lang')->insert(
            [
                'categories_id' => $params['categories_id'],
                'lang_id' => $params['lang_id'],
                'name' => $params['name']
            ]
        );

        return $result;
    }

}
