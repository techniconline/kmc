<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class Link extends BaseModel
{

    protected $table = 'links';
    protected $fillable = ['url', 'title', 'desc'];

    public $errors;
    public $rules = [
        'url' => 'required',
        'title' => 'required'
    ];
}