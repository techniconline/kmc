<?php namespace App;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use App\Http\Requests;
use App\User;
use App\Language;
use Illuminate\Support\Facades\Mail;
use Barryvdh\DomPDF\PDF;
use Illuminate\Support\Facades\App;

class FileGenerate extends BaseModel
{

    public $storageUrl;
    public $user_id;

    public function __construct()
    {
        $this->storageUrl = base_path() . env('PUBLIC_PATH_URL', 'public') . '/generate-pdf/';
        $this->user_id = isset(Auth::user()->get()->id) ? Auth::user()->get()->id : 0;

    }

    public function createFile($inputs)
    {
        if (!$inputs)
            return false;

        $base_storage = $this->storageUrl;
        $subFolder = $inputs['subFolder'] ? $inputs['subFolder'] : 'attach-pdf';
        $user_id = $inputs['user_id'] ? $inputs['user_id'] : 0;
        $resMediaFolder = $this->createFolder($base_storage, $user_id);
        $resSubFolder = $this->createFolder($resMediaFolder['folder'], $subFolder);
        $output_dir = $resSubFolder['folder'];

        $fileName = $inputs['fileName'] ? $inputs['fileName'] . '.pdf' : 'invoice-' . date("Y-m-d-H-i-s") . '.pdf';

        $resultDir = $this->createFileLocation($output_dir);

        $templateFile = $inputs['template'] ? $inputs['template'] : 'pdf.invoice';
        $data = $inputs['data'] ? $inputs['data'] : 0; //['name'=>'ahmad khorshidi']


        //
        $pdf = App::make('dompdf.wrapper');

//        $folder = storage_path().'/generate-pdf/'.(md5(mt_rand())).'/';
//        mkdir($folder, 0755);
        $result = $pdf->loadView($templateFile, $data)
//                        ->setOrientation('landscape')
            ->save($resultDir['folder'] . $fileName);

        if (!$result)
            return false;

        $urlStorage = 'generate-pdf/' . $user_id . '/' . $subFolder . '/' . $resultDir['uploadFolder'] . '/' . $fileName;

        return ['action' => true, 'file' => $resultDir['folder'] . $fileName, 'uploadFolder' => $resultDir['uploadFolder'], 'fileName' => $fileName, 'urlStorageFile' => $urlStorage];

//        $pdf->loadView('pdf.invoice', $this->locale);
        //return $pdf->stream();

//        $pdf = PDF::loadView('pdf.invoice', $this->locale);
//        return $pdf->download('invoice.pdf');


    }


    public function createFileLocation($folderLocation = '')
    {

        $Folder = md5(mt_rand());
        $createFolder = $folderLocation . $Folder . '/';
        mkdir($createFolder, 0755);
        return array('folder' => $createFolder, 'uploadFolder' => $Folder);


    }

    public function createFolder($folderLocation, $folderName)
    {
        $Folder = $folderName;
        $createFolder = $folderLocation . $Folder . '/';
        if (!file_exists($createFolder)) {
            mkdir($createFolder, 0755);
        }
        return ['valid' => true, 'folder' => $createFolder, 'uploadFolder' => $Folder];

    }


}
