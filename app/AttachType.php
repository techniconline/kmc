<?php

/**
 * Created by PhpStorm.
 * User: Hamid
 * Date: 13/01/2015
 * Time: 09:03 AM
 */
namespace App;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;

class AttachType extends BaseModel
{

    protected $table = 'attach_type';
    protected $primaryKey = 'id';
    protected $fillable = ['alias', 'group_id', 'type_status', 'user_id', 'status'];

    protected $hidden = ['remember_token'];
    public $errors;
    public $rules = ['alias' => 'required', 'group_id' => 'required', 'user_id' => 'required'];

    /**
     * @return bool
     */
    public function isValid()
    {
        $validation = Validator::make($this->attributes, $this->rules);
        if ($validation->fails()) {
            $this->errors = $validation->messages();
            return false;
        }
        return true;
    }

    public function saveAttachLang($params)
    {
        $lang_id = $params['lang_id'];
        $id = $params['id'];
        $title = $params['title'];
        $description = $params['description'];
        $result = DB::table('attach_type_lang')->insert(
            [
                'id' => $id
                , 'title' => $title
                , 'description' => $description
                , 'lang_id' => $lang_id
            ]
        );

        return $result;

    }

    public function updateAttachLang($params)
    {
        $lang_id = $params['lang_id'];
        $id = $params['id'];
        $title = $params['title'];
        $description = $params['description'];
        $result = DB::table('attach_type_lang')
            ->where('id', '=', $id)
            ->where('lang_id', '=', $lang_id)
            ->update(
                [
                    'title' => $title
                    , 'description' => $description
                ]
            );

        return $result;

    }

    public function getAttachTypeList($lang_id = 0, $groupId = 0, $user_id = 0, $booking_id = 0)
    {

        if (!$user_id && !$booking_id) {
            $results = DB::table($this->table)
                ->join('attach_type_lang', 'attach_type.id', '=', 'attach_type_lang.id')
                ->join('attach_type_group', 'attach_type.group_id', '=', 'attach_type_group.id')
                ->join('attach_type_group_lang', 'attach_type_group_lang.id', '=', 'attach_type_group.id')
                ->select('attach_type.*', 'attach_type_lang.*', 'attach_type_group_lang.title as title_group'
                    , 'attach_type_group.alias as alias_group', 'attach_type_group.condition')
                ->where('attach_type.status', '=', 1)
                ->where('attach_type_group.status', '=', 1)
                ->where(function ($query) use ($groupId) {
                    if ($groupId && $query) {
                        $query->where('group_id', '=', $groupId);
                    }
                })
                ->where('attach_type_group_lang.lang_id', '=', $lang_id)
                ->where('attach_type_lang.lang_id', '=', $lang_id)
                ->orderBy('group_id')
                ->get();

        } else {

            $results = DB::table($this->table)
                ->join('attach_type_lang', 'attach_type.id', '=', 'attach_type_lang.id')
                ->join('attach_type_group', 'attach_type.group_id', '=', 'attach_type_group.id')
                ->join('attach_type_group_lang', 'attach_type_group_lang.id', '=', 'attach_type_group.id')
                ->leftjoin('attach_documents', function ($leftJoin) use ($user_id, $booking_id) {
                    if ($user_id) {
                        $leftJoin->on('attach_type.id', '=', 'attach_documents.attach_type_id');
                        $leftJoin->where('attach_documents.user_id', '=', $user_id);
                        $leftJoin->where('attach_documents.booking_id', '=', $booking_id);
                        $leftJoin->where('attach_documents.status', '=', 1);
                        $leftJoin->where('attach_documents.document_status', '=', 1);
                    }
                })
                ->leftjoin('media', function ($leftJoin1) use ($user_id) {
                    if ($user_id) {
                        $leftJoin1->on('attach_documents.media_id', '=', 'media.id');
                        $leftJoin1->where('media.status', '=', 1);
                    }
                })
                ->select('attach_type.*', 'attach_type_lang.*', 'attach_type_group_lang.title as title_group'
                    , 'attach_type_group.alias as alias_group', 'attach_type_group.condition'
                    , 'attach_documents.id as attach_document_id', 'attach_documents.media_id'
                    , 'attach_documents.attach_type_id', 'attach_documents.booking_id'
                    , DB::raw('(select document_status from attach_documents at where at.attach_type_id=attach_type.id
                                            and at.user_id=' . ($user_id ? $user_id : 0) . ' and at.status=1 and at.document_status=4 AND at.booking_id=' . ($booking_id ? $booking_id : 0) . ' order by at.id desc limit 1 ) as valid_document_status')
                    , DB::raw('(select src from media where id=(select media_id from attach_documents at where at.attach_type_id=attach_type.id
                                            and at.user_id=' . ($user_id ? $user_id : 0) . ' and at.status=1 and at.document_status=4 AND at.booking_id=' . ($booking_id ? $booking_id : 0) . ' order by at.id desc limit 1 )) as valid_src')
                    , 'media.src', 'media.type')
                ->where('attach_type.status', '=', 1)
                ->where('attach_type_group.status', '=', 1)
                ->where(function ($query) use ($groupId, $user_id, $booking_id) {
                    if ($groupId && $query) {
                        $query->where('attach_type.group_id', '=', $groupId);
                    }

                })
                ->where('attach_type_group_lang.lang_id', '=', $lang_id)
                ->where('attach_type_lang.lang_id', '=', $lang_id)
                ->orderBy('attach_type.group_id')
                ->get();

        }


        return $results;
    }

    public function getAttachTypeListItem($lang_id = 0, $groupId = 0)
    {

        $results = DB::table($this->table)
            ->join('attach_type_lang', 'attach_type.id', '=', 'attach_type_lang.id')
            ->join('attach_type_group', 'attach_type.group_id', '=', 'attach_type_group.id')
            ->join('attach_type_group_lang', 'attach_type_group_lang.id', '=', 'attach_type_group.id')
            ->select('attach_type.*', 'attach_type_lang.*', 'attach_type_group_lang.title as title_group')
            ->where('attach_type.status', '=', 1)
            ->where('attach_type_group.status', '=', 1)
            ->where(function ($query) use ($groupId) {
                if ($query) {
                    $query->where('group_id', '=', $groupId);
                }
            })
            ->where('attach_type_group_lang.lang_id', '=', $lang_id)
            ->where('attach_type_lang.lang_id', '=', $lang_id)
            ->orderBy('group_id')
            ->lists('attach_type_lang.title', 'attach_type.id');

        return $results;
    }

    public function getListAttachUsers($params = null)
    {

        $user_id = isset($params['users_id']) ? $params['users_id'] : 0;
        $booking_id = isset($params['booking_id']) ? $params['booking_id'] : 0;
        $bookingIds = isset($params['bookingIds']) ? $params['bookingIds'] : 0;
        $groupId = isset($params['group_id']) ? $params['group_id'] : 0;
        $lang_id = isset($params['lang_id']) ? $params['lang_id'] : 0;

        if (!$lang_id)
            return false;

        $results = DB::table($this->table)
            ->join('attach_type_lang', 'attach_type.id', '=', 'attach_type_lang.id')
            ->join('attach_type_group', 'attach_type.group_id', '=', 'attach_type_group.id')
            ->join('attach_type_group_lang', 'attach_type_group_lang.id', '=', 'attach_type_group.id')
            ->join('attach_documents', function ($Join) use ($user_id, $booking_id, $bookingIds) {
                $Join->on('attach_type.id', '=', 'attach_documents.attach_type_id');
                if ($user_id) {
                    $Join->where('attach_documents.user_id', '=', $user_id);
                }
                if ($booking_id) {
                    $Join->where('attach_documents.booking_id', '=', $booking_id);
                }
                //$Join->where('attach_documents.document_status','=',1);
                $Join->where('attach_documents.status', '=', 1);
            })
            ->join('media', 'attach_documents.media_id', '=', 'media.id')
            ->join('users', 'attach_documents.user_id', '=', 'users.id')
            ->leftjoin('countries', 'users.country_id', '=', 'countries.id')
            ->select('attach_type.*'
                , 'attach_type_lang.title', 'attach_type_lang.description'
                , 'attach_type_group_lang.title as title_group'
                , 'attach_type_group.alias as alias_group', 'attach_type_group.condition'
                , 'attach_documents.id as attach_document_id', 'attach_documents.media_id'
                , 'attach_documents.attach_type_id', 'attach_documents.booking_id'
                , 'attach_documents.document_status'
                , 'media.src', 'media.type'
                , 'users.firstname', 'users.lastname', 'users.mobile'
                , 'users.email', 'users.gender', 'users.birth_date', 'countries.name as country_name'
            )
            ->where('attach_type.status', '=', 1)
            ->where('media.status', '=', 1)
            ->where('attach_type_group.status', '=', 1)
            ->where(function ($query) use ($groupId, $bookingIds) {
                if ($groupId && $query) {
                    $query->where('attach_type.group_id', '=', $groupId);
                }
                if ($bookingIds) {
                    $query->whereIn('attach_documents.booking_id', $bookingIds);
                }
            })
            ->where('attach_type_group_lang.lang_id', '=', $lang_id)
            ->where('attach_type_lang.lang_id', '=', $lang_id)
            ->orderBy('attach_type.group_id')
            ->get();

        return $results;

    }

    public function getListBookingUsersWithDoc($params = null)
    {

        $user_id = isset($params['user_id']) ? $params['user_id'] : 0;
        $booking_id = isset($params['booking_id']) ? $params['booking_id'] : 0;
        $paginateNum = isset($params['paginateNum']) ? $params['paginateNum'] : 10;
        $document_status_is_not_show = isset($params['document_status_is_not_show']) ? $params['document_status_is_not_show'] : 0;

        $date_from = $params['date_from'];
        $date_to = $params['date_to'];

        $reg_date_from = $params['reg_date_from'];
        $reg_date_to = $params['reg_date_to'];

        $first_name = $params['first_name'];
        $last_name = $params['last_name'];

        $gender = $params['gender'];

        $country_name = $params['country_name'];

        $results = DB::table('booking')
            ->join('users', 'booking.user_id', '=', 'users.id')
            ->leftjoin('countries', 'users.country_id', '=', 'countries.id')
            ->select('booking.*', 'users.firstname', 'users.lastname', 'users.mobile'
                , 'users.email', 'users.gender', 'users.birth_date'
                , 'countries.name as country_name'
            )
            ->where(function ($query) use ($user_id) {
                if ($user_id) {
//                    $query->where('booking.status', '!=', 1);
                } else {
                    $query->where('booking.status', '=', 1);
                }
            })
            ->where('users.status', '=', 1)
            ->where(DB::raw('(SELECT COUNT(attach_documents.id) FROM attach_documents WHERE attach_documents.booking_id=booking.id
                            AND attach_documents.status=1 AND attach_documents.document_status!=' . $document_status_is_not_show . ')'), '>', 0)
            ->where(function ($query) use (
                $user_id, $booking_id
                , $date_from, $date_to, $reg_date_from, $reg_date_to, $first_name, $last_name, $gender, $country_name
            ) {
                if ($user_id && $query) {
                    $query->where('users.id', '=', $user_id);
                }
                if ($booking_id && $query) {
                    $query->where('booking.id', '=', $booking_id);
                }
                if ($date_from && $query) {
                    $query->where('booking.from_date', '>=', $date_from);
                }
                if ($date_to && $query) {
                    $query->where('booking.to_date', '<=', $date_to);
                }
                if ($reg_date_from && $query) {
                    $query->where('booking.created_at', '>=', $reg_date_from . ' 00:00:00');
                }
                if ($reg_date_to && $query) {
                    $query->where('booking.created_at', '<=', $reg_date_to . ' 23:59:59');
                }
                if ($first_name && $query) {
                    $query->where('users.firstname', 'like', '%' . $first_name . '%');
                }
                if ($last_name && $query) {
                    $query->where('users.lastname', 'like', '%' . $last_name . '%');
                }
                if ($country_name && $query) {
                    $query->where('countries.name', 'like', '%' . $country_name . '%');
                }
                if ($gender && $query) {
                    $query->where('users.gender', '=', $gender);
                }
            })
            ->orderBy('booking.id')->paginate($paginateNum);

        $bookingIds = [];
        foreach ($results as $item) {
            $bookingIds[] = $item->id;
        }

        $params['bookingIds'] = $bookingIds;
        $resultDocs = $this->getListAttachUsers($params);


        foreach ($results as &$item) {

            foreach ($resultDocs as $itemD) {

                if ($item->id == $itemD->booking_id) {

                    $item->documents[] = $itemD;
                    $item->text_status_document = '';
                    if ($itemD->document_status == 5) {
                        $item->text_status_document = Lang::get('backend/AttachType.messages.msgStatusDocumentRejected');
                    } elseif ($itemD->document_status == 4) {
                        $item->text_status_document = Lang::get('backend/AttachType.messages.msgStatusDocumentIsOk');

                    } elseif ($itemD->document_status == 1) {
                        $profileModel = new Profile();
                        $message = $profileModel->getMessagesBooking($item->id, 3, true);//document is not valid! and not completed
                        if (isset($message->id)) {
                            $item->text_status_document = Lang::get('backend/AttachType.messages.msgStatusDocumentIsNotComplete');
                        } else {
                            $item->text_status_document = Lang::get('backend/AttachType.messages.msgStatusDocumentNotCheck');
                        }
                    }

                }
            }
        }


        return $results;

    }

    public function getListBookingDetails($booking_id, $lang_id)
    {

        if (!$booking_id || !$lang_id)
            return false;

        $results = DB::table('booking')
            ->join('users', 'booking.user_id', '=', 'users.id')
            ->leftjoin('countries', 'users.country_id', '=', 'countries.id')
            ->leftjoin('booking_details', function ($leftJoin) {
                $leftJoin->on('booking.id', '=', 'booking_details.booking_id');
                $leftJoin->where('booking_details.status', '=', 1);
            })
            ->leftjoin('apartment_room_category_map', function ($leftJoin1) {
                $leftJoin1->on('booking_details.apartment_room_category_map_id', '=', 'apartment_room_category_map.id');
                $leftJoin1->where('apartment_room_category_map.status', '=', 1);
            })
            ->leftjoin('room_category', function ($leftJoin2) {
                $leftJoin2->on('apartment_room_category_map.room_category_id', '=', 'room_category.id');
                $leftJoin2->where('room_category.status', '=', 1);
            })
            ->leftjoin('room_category_lang', function ($leftJoin3) use ($lang_id) {
                $leftJoin3->on('room_category.id', '=', 'room_category_lang.id');
                $leftJoin3->where('room_category_lang.lang_id', '=', $lang_id);
            })
            ->leftjoin('apartments', function ($leftJoin4) {
                $leftJoin4->on('apartment_room_category_map.apartment_id', '=', 'apartments.id');
                $leftJoin4->where('apartments.status', '=', 1);
            })
            ->leftjoin('apartments_lang', function ($leftJoin5) use ($lang_id) {
                $leftJoin5->on('apartments.id', '=', 'apartments_lang.id');
                $leftJoin5->where('apartments_lang.lang_id', '=', $lang_id);
            })
            ->select('booking.*', 'users.firstname', 'users.lastname', 'users.mobile'
                , 'users.email', 'users.gender', 'users.birth_date'
                , 'countries.name as country_name'
                , 'booking_details.id as booking_details_id', 'booking_details.priority'
                , 'apartments_lang.apartment_title'
                , 'room_category_lang.room_category_title'
                , 'room_category.area', 'room_category.type'
                , 'apartment_room_category_map.apartment_id'
                , 'apartment_room_category_map.room_category'
            )
            ->where('booking.status', '=', 1)
            ->where('users.status', '=', 1)
            ->where('booking.id', '=', $booking_id)
            ->orderBy('booking_details.priority')
            ->get();


    }

    public function addFeedBackForDocument($params)
    {

        if (!$params)
            return false;

        $booking_id = isset($params['booking_id']) ? $params['booking_id'] : 0;
        $user_id = isset($params['user_id']) ? $params['user_id'] : 0;
        $feedback_message = isset($params['feedback_message']) ? $params['feedback_message'] : '';
        $subject = isset($params['subject']) ? $params['subject'] : '';
        $status = isset($params['status']) ? $params['status'] : 0;
        $assessor_user_id = isset($params['assessor_user_id']) ? $params['assessor_user_id'] : 0;

        if (!$booking_id || !$user_id || !$assessor_user_id || !$status)
            return false;

        $result = DB::table('attach_documents_feedback')->insertGetId(
            [
                'booking_id' => $booking_id
                , 'user_id' => $user_id
                , 'assessor_user_id' => $assessor_user_id
                , 'subject' => $subject
                , 'feedback_message' => $feedback_message
                , 'view_status' => 0
                , 'status' => $status
                , 'created_at' => date('Y-m-d H:i:s')
            ]
        );

        return $result;

    }

    public function updateAttachDocUser($params)
    {
        $user_id = isset($params['user_id']) ? $params['user_id'] : 0;
        $document_status = isset($params['document_status']) ? $params['document_status'] : 0;
        $status = isset($params['status']) ? $params['status'] : 0;
        $booking_id = isset($params['booking_id']) ? $params['booking_id'] : 0;
        $attach_type_id = isset($params['attach_type_id']) ? $params['attach_type_id'] : 0;

        $result = DB::table('attach_documents')
            ->where('status', '!=', 0)
            ->where(function ($query) use ($user_id, $booking_id, $attach_type_id) {
                if ($user_id) {
                    $query->where('user_id', '=', $user_id);
                } elseif ($attach_type_id) {
                    $query->where('attach_type_id', '=', $attach_type_id);
                } elseif ($booking_id) {
                    $query->where('booking_id', '=', $booking_id);
                } else {
                    $query->where('id', '<', 0);
                }
            })
            ->update(
                [
                    'document_status' => $document_status
                    , 'status' => $status
                    , 'updated_at' => date('Y-m-d H:i:s')
                ]
            );

        return $result;

    }


} 