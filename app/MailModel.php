<?php namespace App;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use App\Http\Requests;
use App\User;
use App\Language;
use Illuminate\Support\Facades\Mail;

class MailModel extends BaseModel
{


    public function sendMail($inputs)
    {
        if (!$inputs)
            return false;

        $email = isset($inputs['email']) ? $inputs['email'] : '';
        $username = isset($inputs['username']) ? $inputs['username'] : '';
        $subject = isset($inputs['subject']) ? $inputs['subject'] : '';
        $text = isset($inputs['text']) ? $inputs['text'] : '';
        $text2 = isset($inputs['text2']) ? $inputs['text2'] : '';
        $page = isset($inputs['page']) ? $inputs['page'] : '';
        $attach = isset($inputs['attach']) ? $inputs['attach'] : '';
        $attachs = isset($inputs['attachs']) ? $inputs['attachs'] : '';//is Array attachment, sample:[$path1, $path2 , ...]

        if (!$page || !$text || !$subject || !$email)
            return false;

        $data = [
            'email' => $email,
            'username' => $username,
            'subject' => $subject,
            'text2' => $text2,
            'text' => $text
        ];

        $result = Mail::send($page, $data, function ($message) use ($email, $subject, $username, $attach, $attachs) {
            $message->to($email, $username)->subject($subject);
            if ($attach) {
                $message->attach($attach);
            }
            if ($attachs) {
                foreach ($attachs as $item) {
                    $message->attach($item);
                }
            }

        });

        return $result;

    }

    public function sendMailToMany($inputs)
    {
        if (!$inputs)
            return false;

        $emails = isset($inputs['emails']) ? $inputs['emails'] : ''; //['email'=>name]
        $subject = isset($inputs['subject']) ? $inputs['subject'] : '';
        $text = isset($inputs['text']) ? $inputs['text'] : '';
        $text2 = isset($inputs['text2']) ? $inputs['text2'] : '';
        $page = isset($inputs['page']) ? $inputs['page'] : '';
        $attach = isset($inputs['attach']) ? $inputs['attach'] : '';
        $attachs = isset($inputs['attachs']) ? $inputs['attachs'] : '';//is Array attachment, sample:[$path1, $path2 , ...]

        if (!$page || !$text || !$subject || !$emails)
            return false;

        $data = [
            'subject' => $subject,
            'text2' => $text2,
            'text' => $text
        ];

        $result = Mail::send($page, $data, function ($message) use ($emails, $subject, $attach, $attachs) {
            $message->to($emails)->subject($subject);
            if ($attach) {
                $message->attach($attach);
            }
            if ($attachs) {
                foreach ($attachs as $item) {
                    $message->attach($item);
                }
            }

        });

        return $result;

    }


}
