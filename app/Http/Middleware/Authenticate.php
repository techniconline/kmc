<?php namespace App\Http\Middleware;

use Auth;
use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\URL;
use App\BaseModel;
use Illuminate\Routing\Route;

class Authenticate
{

    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * The Segment One URI.
     *
     * @var String
     */
    protected $segment;

    protected $user_id;

    protected $controller;

    protected $functionCalled;

    /**
     * Create a new filter instance.
     *
     */
    public function __construct()
    {
        if (app('locale') == '') {
            $this->segment = Request::segment(1);
            $this->controller = Request::segment(2);
            $this->functionCalled = Request::segment(3);
        } else {
            $this->segment = Request::segment(2);
            $this->controller = Request::segment(3);
            $this->functionCalled = Request::segment(4);
        }

        if ($this->segment == 'backend') {
            $this->auth = Auth::employee();
            $this->user_id = isset(Auth::employee()->get()->id) ? Auth::employee()->get()->id : 0;
        } else {
            $this->segment = 'front';
            $this->auth = Auth::user();
        }

    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->auth->guest()) {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest(app('locale') . '/' . $this->segment . '/auth/login');
            }
        } else {

            $userRequest = [
                'controller' => $this->controller
                , 'functionCalled' => $this->functionCalled
                , 'method' => $request->method()
                , 'requestUri' => $request->getRequestUri()
                , 'prefix' => $this->segment
                , 'currentRoute' => \Illuminate\Support\Facades\Route::getCurrentRoute()
            ];
            $baseModel = new BaseModel();
            $result = $baseModel->checkPermission($userRequest, $this->user_id);

            if (!$result) {
                if ($request->ajax()) {
                    return response('Access Denied.', 302);
                } else {
                    return abort(302);
                }
            }


        }

        return $next($request);
    }

}