<?php namespace App\Http\Middleware;

use Auth;
use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Request;

class RedirectIfAuthenticated
{

    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * The Segment One URI.
     *
     * @var String
     */
    protected $segment;

    /**
     * Create a new filter instance.
     *
     */
    public function __construct()
    {
        if (app('locale') == '')
            $this->segment = Request::segment(1);
        else
            $this->segment = Request::segment(2);

        if ($this->segment == 'backend')
            $this->auth = Auth::employee();
        else {
            $this->segment = 'front';
            $this->auth = Auth::user();
        }
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->auth->check()) {
            return new RedirectResponse(url(app('locale') . '/' . $this->segment . '/home'));
        }

        return $next($request);
    }

}