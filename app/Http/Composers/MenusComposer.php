<?php
/**
 * Created by PhpStorm.
 * User: kharrazi
 * Date: 4/7/15
 * Time: 10:30 AM
 */

namespace App\Http\Composers;

use App\Content;
use App\Menu;
use App\Param;

class MenusComposer
{

    public function slider($view)
    {
        $sliders = Param::where('alias', '=', 'index')->where('status', 1)->where('type', 'image')->lists('value');
        foreach ($sliders as $key => $slider) {
            $sliders[$key] = asset('/uploads/images/Params/' . $slider . '.jpg');
        }
        $view->with('sliders', $sliders);
    }

    public function build($view)
    {
        $back_menus = Menu::where('root_id', '=', 1)->first()->getDescendants()->toHierarchy();
        $view->with('backend_menus', $back_menus);
    }

    public function front_menus($view)
    {
        $front_menus = Menu::where('root_id', '=', 2)->first()->getDescendants()->toHierarchy();
        $contents = Content::join('contents_lang', 'contents_id', '=', 'id')
            ->where('contents_lang.lang_id', '=', app('activeLangDetails')['lang_id'])
            ->where('type', 'blog')
            ->where('contents.status', 1)
            ->get();

        $view->with('frontend_menus', $front_menus)->with('contents', $contents);
    }
}
