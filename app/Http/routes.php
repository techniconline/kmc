<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/', 'WelcomeController@index');

//Route::get('backend/auth/login',function(){
//    return "asfasfasf";
//});
Route::controllers([
    'backend/auth' => 'Auth\AuthController',
    'backend/password' => 'Auth\ResetPasswordController',
    'front/auth' => 'Auth\FrontAuthController',
    'front/password' => 'Auth\FrontResetPasswordController',
]);
Route::get('auth/google', 'Auth\AuthController@google_redirect');
Route::get('auth/google/callback', 'Auth\AuthController@google');
Route::get('auth/github', 'Auth\AuthController@github_redirect');
Route::get('auth/github/callback', 'Auth\AuthController@github');
Route::get('sitemap', function(){

    $model = new \App\BaseModel();
    return $model->createSiteMap();

});
Route::group(['prefix' => 'backend', 'namespace' => 'backend', 'middleware' => 'auth'], function () {

    Route::post('accessGroup/search', array('as' => 'backend.accessGroup.search', 'uses' => 'AccessGroupController@index'));
    Route::get('accessGroup/search', array('as' => 'backend.accessGroup.search', 'uses' => 'AccessGroupController@index'));
    Route::get('accessGroup/accessList/{group_id}', array('as' => 'backend.accessGroup.accessList', 'uses' => 'AccessGroupController@showAccessList'));
    Route::put('accessGroup/updateAccessList/{group_id}', array('as' => 'backend.accessGroup.updateAccessList', 'uses' => 'AccessGroupController@updateAccessList'));
    Route::resource('accessGroup', 'AccessGroupController');

    Route::post('actionList/search', array('as' => 'backend.actionList.search', 'uses' => 'ActionListController@index'));
    Route::get('actionList/search', array('as' => 'backend.actionList.search', 'uses' => 'ActionListController@index'));
    Route::resource('actionList', 'ActionListController');

    Route::get('home/listRoomRentedDashboard', array('as' => 'backend.home.listRoomRentedDashboard', 'uses' => 'HomeController@listRoomRented'));
    Route::post('home/listRoomRentedDashboard', array('as' => 'backend.home.listRoomRentedDashboard', 'uses' => 'HomeController@listRoomRented'));

//    Route::post('/', 'HomeController@listRoomRentedInDashboard');
    Route::get('/', 'HomeController@index');
    Route::get('home', 'HomeController@index');
    Route::post('category/changeposition', 'CategoryController@changePosition');
    Route::resource('category', 'CategoryController');
    Route::post('menu/changeposition', 'MenuController@changePosition');
    Route::resource('menu', 'MenuController');
    Route::resource('content', 'ContentsController');
    Route::resource('weblog', 'WebLogController');
    Route::resource('news', 'NewsController');
//    Route::post('news/status', 'NewsController@ch_status');
    Route::post('mail', ['as' => 'backend.users.mail', 'uses' => 'UsersController@mail']);
    Route::get('users/showData/{user_id}', array('as' => 'backend.users.showData', 'uses' => 'UsersController@showData'));
    Route::get('showEmail', ['as' => 'backend.users.showEmail', 'uses' => 'UsersController@showEmail']);
    Route::get('users/formSendMailGroup', ['as' => 'backend.users.formSendMailGroup', 'uses' => 'UsersController@formSendMailGroup']);
    Route::post('users/sendMailGroup', ['as' => 'backend.users.sendMailGroup', 'uses' => 'UsersController@sendMailGroup']);
    Route::post('users/sendAccountInt/{user_id}', ['as' => 'backend.users.sendAccountInt', 'uses' => 'UsersController@sendAccountInt']);
    Route::resource('users', 'UsersController');
    Route::resource('employees', 'EmployeesController');
    Route::resource('profile', 'ProfileController', ['only' => ['edit', 'update']]);
    Route::resource('links', 'LinksController');
    Route::resource('faq', 'FaqController');
    Route::resource('messages', 'MessagesController');
    Route::resource('params', 'ParamsController');
    Route::resource('calendar', 'CalendarController');
    Route::resource('language', 'LanguageController');

    Route::get('feature/createFeatureValue/{feature_id}', ['as' => 'backend.feature.createFeatureValue', 'uses' => 'FeatureController@createFeatureValue']);
    Route::post('feature/storeFeatureValue/{feature_id}', ['as' => 'backend.feature.storeFeatureValue', 'uses' => 'FeatureController@storeFeatureValue']);
    Route::get('feature/editFeatureValue/{feature_id}/{feature_value_id}', ['as' => 'backend.feature.editFeatureValue', 'uses' => 'FeatureController@editFeatureValue']);
    Route::put('feature/updateFeatureValue/{feature_id}/{feature_value_id}', ['as' => 'backend.feature.updateFeatureValue', 'uses' => 'FeatureController@updateFeatureValue']);
    Route::resource('feature', 'FeatureController');
    Route::resource('featureGroup', 'FeatureGroupController');

    Route::post('attachment/rejectDocumentsUser/{user_id}/{booking_id}', array('as' => 'backend.attachment.rejectDocumentsUser', 'uses' => 'AttachTypeController@rejectDocumentsUser'));
    Route::post('attachment/isOkDocumentsUser/{user_id}/{booking_id}', array('as' => 'backend.attachment.isOkDocumentsUser', 'uses' => 'AttachTypeController@isOkDocumentsUser'));
    Route::post('attachment/sendMailToUser', array('as' => 'backend.attachment.sendMailToUser', 'uses' => 'AttachTypeController@sendMailToUser'));
    Route::put('attachment/active/{attach_type_id}', array('as' => 'backend.attachment.active', 'uses' => 'AttachTypeController@activate'));
    Route::put('attachment/createGroup/active/{group_id}', array('as' => 'backend.attachment.activeGroup', 'uses' => 'AttachTypeController@activateGroup'));
    Route::get('attachment/createGroup', array('as' => 'backend.attachment.createGroup', 'uses' => 'AttachTypeController@createGroup'));
    Route::get('attachment/createGroup/{group_id}/edit', array('as' => 'backend.attachment.editGroup', 'uses' => 'AttachTypeController@editGroup'));
    Route::put('attachment/createGroup/{group_id}', array('as' => 'backend.attachment.updateGroup', 'uses' => 'AttachTypeController@updateGroup'));
    Route::delete('attachment/createGroup/{group_id}', array('as' => 'backend.attachment.destroyGroup', 'uses' => 'AttachTypeController@destroyGroup'));
    Route::post('attachment/createGroup', array('as' => 'backend.attachment.storeGroup', 'uses' => 'AttachTypeController@storeGroup'));
    Route::get('attachment/listUploadedSingleUser/{user_id}/{booking_id}', array('as' => 'backend.attachment.listUploadedSingleUser', 'uses' => 'AttachTypeController@listUploadedSingleUser'));
    Route::get('attachment/listUploaded', array('as' => 'backend.attachment.listUploaded', 'uses' => 'AttachTypeController@listUploaded'));
    Route::post('attachment/listUploaded', array('as' => 'backend.attachment.listUploaded', 'uses' => 'AttachTypeController@listUploaded'));
    Route::get('attachment/listUploadedRender', array('as' => 'backend.attachment.listUploadedRender', 'uses' => 'AttachTypeController@listUploadedRender'));
    Route::resource('attachment', 'AttachTypeController');

    Route::resource('comments', 'CommentsController');


//    Route::post('booking/sendContract/{booking_id}', array('as' => 'backend.booking.sendContract', 'uses' => 'BookingController@sendContract'));
//    Route::post('booking/generateContract/{booking_id}', array('as' => 'backend.booking.generateContract', 'uses' => 'BookingController@generateContract'));
//    Route::post('booking/sendMailContract/{booking_id}', array('as' => 'backend.booking.sendMailContract', 'uses' => 'BookingController@sendMailContract'));
//    Route::put('booking/rejectBookingDetailsType/{booking_id}/{type}', array('as' => 'backend.booking.rejectBookingDetailsType', 'uses' => 'BookingController@rejectBookingDetailsType'));
//    Route::get('booking/listArchiveBooking', array('as' => 'backend.booking.listArchiveBooking', 'uses' => 'BookingController@listArchiveBooking'));
////    Route::post('booking/rejectDocumentsUser/{user_id}/{booking_id}',array('as' => 'backend.booking.rejectBookingUser', 'uses' =>'BookingController@rejectBookingUser'));
////    Route::post('booking/isOkDocumentsUser/{user_id}/{booking_id}',array('as' => 'backend.booking.isOkBookingUser', 'uses' =>'BookingController@isOkBookingUser'));
////    Route::post('booking/sendMailToUser',array('as' => 'backend.booking.sendMailToUser', 'uses' =>'BookingController@sendMailToUser'));
//    Route::post('booking/searchArchive', array('as' => 'backend.booking.searchArchive', 'uses' => 'BookingController@listArchiveBooking'));
//    Route::post('booking/search', array('as' => 'backend.booking.search', 'uses' => 'BookingController@index'));
//    Route::get('booking/search', array('as' => 'backend.booking.search', 'uses' => 'BookingController@index'));
//    Route::resource('booking', 'BookingController');
//
//
    Route::put('product/isDefault', array('as' => 'backend.product.isDefault', 'uses' => 'ProductController@isDefault'));
    Route::resource('product', 'ProductController');
//    Route::get('roomCategory/details/{category}', array('as' => 'backend.roomCategory.details', 'uses' => 'RoomCategoryController@details'));
//    Route::put('roomCategory/updateDetails/{category}/{apartment_map}', array('as' => 'backend.roomCategory.updateDetails', 'uses' => 'RoomCategoryController@updateDetails'));
//    Route::post('roomCategory/addDetails/{category}', array('as' => 'backend.roomCategory.addDetails', 'uses' => 'RoomCategoryController@addDetails'));
//    Route::delete('roomCategory/details/del/{apartment_map}', array('as' => 'backend.roomCategory.delDetails', 'uses' => 'RoomCategoryController@delDetails'));
//    Route::put('roomCategory/details/updatePrice/{category}', array('as' => 'backend.roomCategory.updatePrice', 'uses' => 'RoomCategoryController@updatePrice'));
//    Route::resource('roomCategory', 'RoomCategoryController');
//
//    //use in dashboard
//    Route::get('room/listRoomRentedInDashboard', array('as' => 'backend.room.listRoomRentedInDashboard', 'uses' => 'HomeController@listRoomRentedInDashboard'));
//    Route::post('room/listRoomRentedInDashboard', array('as' => 'backend.room.listRoomRentedInDashboard', 'uses' => 'HomeController@listRoomRentedInDashboard'));
//    //-----------------------------
//
//    Route::get('room/listRoomRented', array('as' => 'backend.room.listRoomRented', 'uses' => 'RoomController@listRoomRented'));
//    Route::post('room/listRoomRented', array('as' => 'backend.room.listRoomRented', 'uses' => 'RoomController@listRoomRented'));
//    Route::put('room/savePictures/{room_id}', array('as' => 'backend.room.savePictures', 'uses' => 'RoomController@savePictures'));
//    Route::delete('room/delPictures/{room_id}/{media_id}', array('as' => 'backend.room.delPictures', 'uses' => 'RoomController@delPictures'));
//    Route::post('room/reservation/{room_id}/{booking_id}/{user_id}', array('as' => 'backend.room.reservationRoom', 'uses' => 'RoomController@reservationRoom'));
//    Route::post('room/rent/{room_id}/{booking_id}/{user_id}', array('as' => 'backend.room.rentRoom', 'uses' => 'RoomController@rentRoom'));
//    Route::post('room/cancelRentRoom/{rent_id}/{user_id}', array('as' => 'backend.room.cancelRentRoom', 'uses' => 'RoomController@cancelRentRoom'));
//    Route::post('room/generate/{apartment_map}', array('as' => 'backend.room.generateRooms', 'uses' => 'RoomController@generateRooms'));
//    Route::resource('room', 'RoomController');
//
    Route::resource('advert', 'AdvertisementsController');
//
//    Route::post('billing/search', array('as' => 'backend.billing.search', 'uses' => 'BillingController@index'));
//    Route::get('billing/search', array('as' => 'backend.billing.search', 'uses' => 'BillingController@index'));
//    Route::post('billing/generateListRent/{date}', array('as' => 'backend.billing.generateListRent', 'uses' => 'BillingController@generateListRent'));
//    Route::post('billing/sendRememberInvoice/{date}', array('as' => 'backend.billing.sendRememberInvoice', 'uses' => 'BillingController@sendMailRememberDueInvoice'));
//    Route::resource('billing', 'BillingController');
//
});

Route::group(['prefix' => 'front', 'namespace' => 'front', 'middleware' => 'auth'], function () {
    Route::get('like/{system}/{id}/{status}', array('as' => 'front.like.likeAction', 'uses' => 'LikeController@likeAction'));

    Route::get('profile/messages/{id}', array('as' => 'front.profile.showMessage', 'uses' => 'ProfileController@showMessage'));
    Route::get('profile/messages', array('as' => 'front.profile.messages', 'uses' => 'ProfileController@messagesUser'));
    Route::get('profile/password', array('as' => 'front.profile.password', 'uses' => 'ProfileController@password'));
    Route::post('profile/change-password', array('as' => 'front.profile.changePassword', 'uses' => 'ProfileController@changePassword'));
    Route::get('profile/avatar', array('as' => 'front.profile.avatar', 'uses' => 'ProfileController@avatar'));
    Route::post('profile/change-avatar', array('as' => 'front.profile.changeAvatar', 'uses' => 'ProfileController@changeAvatar'));
    Route::resource('profile', 'ProfileController');
    Route::resource('booking', 'BookingController');
//    Route::resource('weblog', 'WebLogController',['only' => ['store','update','edit']]);//,['only'=>['index','show']]

});

Route::get('/sendMail', function () {
    $data = [];

    Mail::send('emails.welcome', $data, function ($message) {
        $message->to('techniconline@yahoo.com', 'Name')->subject('For test Mail');
    });

});
Route::get('/', 'HomeController@index');
Route::get('home', 'HomeController@index');
Route::post('uploadFile/delete', array('as' => 'uploadFile.delete', 'uses' => 'UploaderController@deleteFile'));
Route::post('uploadFile', array('as' => 'uploadFile', 'uses' => 'UploaderController@uploadFile'));
Route::resource('media', 'MediaController');

Route::get('products', array('as' => 'category.index', 'uses' => 'CategoryController@index'));
Route::get('category/{alias}.html', array('as' => 'category.show', 'uses' => 'CategoryController@show'));
Route::resource('category', 'CategoryController', ['except' => ['show']]);


Route::get('product/{alias}.html', array('as' => 'product.show', 'uses' => 'ProductController@show'));
Route::resource('product', 'ProductController', ['except' => ['show']]);
//Route::resource('content','ContentController',['only'=>['show']]);

Route::get('content/{alias}.html', array('as' => 'content.show', 'uses' => 'ContentController@show'));
Route::get('content/{alias}.html', array('as' => 'static.show', 'uses' => 'ContentController@show'));
Route::get('content/{alias}.html', array('as' => 'index-list.show', 'uses' => 'ContentController@show'));
Route::get('news/{alias}.html', array('as' => 'news.show', 'uses' => 'NewsController@show'));
Route::get('news/tags/{tag}', array('as' => 'news.searchTags', 'uses' => 'NewsController@searchTags'));
Route::get('news/category/{category}', array('as' => 'news.searchCategory', 'uses' => 'NewsController@searchCategory'));
Route::get('search/', array('as' => 'searchContentText', 'uses' => 'NewsController@searchContentText'));
Route::get('search/{text}', array('as' => 'searchContent', 'uses' => 'NewsController@searchText'));
Route::resource('news', 'NewsController', ['except' => ['show']]);
Route::resource('links', 'LinksController', ['only' => ['index']]);
Route::resource('faq', 'FaqController', ['only' => ['index', 'show']]);
Route::resource('contact-us', 'ContactUsController');
Route::resource('guide', 'GuideController', ['only' => ['index', 'show']]);
Route::resource('page', 'PageController', ['only' => ['show']]);
Route::resource('feed', 'RssController');
Route::resource('comment', 'CommentController');

Route::get('weblog/tags/{tag}', array('as' => 'weblog.searchTags', 'uses' => 'WebLogController@searchTags'));
Route::get('weblog/{alias}.html', array('as' => 'weblog.show', 'uses' => 'WebLogController@show'));
Route::get('weblog/category/{category}', array('as' => 'weblog.searchCategory', 'uses' => 'WebLogController@searchCategory'));
Route::resource('weblog', 'WebLogController', ['except' => ['show']]);//,['only'=>['index','show']]

//for example
Route::resource('test', 'TestController');


Route::get('Ckbrowser', array('as' => 'ck.browse', 'uses' => 'CKController@browse'));


//Event::listen('illuminate.query', function($sql, $bindings, $time){
////    echo $sql;          // select * from my_table where id=?
////    print_r($bindings); // Array ( [0] => 4 )
////    echo $time;         // 0.58
//
//    // To get the full sql query with bindings inserted
//    $sql = str_replace(array('%', '?'), array('%%', '%s'), $sql);
//    $full_sql = vsprintf($sql, $bindings);
//    echo "<pre>";print_r($full_sql);echo "/////////Time: ".$time;echo "</pre>";
//});
