<?php namespace App\Http\Controllers\Auth;

use Auth;
use Socialite;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers;

    protected $viewPath = 'backend';
    protected $redirectTo = '';
    protected $loginPath = '';

    /**
     * Create a new authentication controller instance.
     *
     * @param  \Illuminate\Contracts\Auth\Registrar $registrar
     */
    public function __construct(Registrar $registrar)
    {
        $prefix = (app('locale') == '') ? '' : '/' . app('locale');
        $this->redirectTo = $prefix . '/backend/home';
        $this->loginPath = $prefix . '/backend/auth/login';
//        var_dump(app('locale'));
//        var_dump($this->redirectTo);
//        die;

        $this->auth = Auth::employee();
        $this->registrar = $registrar;

        $this->middleware('guest', ['except' => 'getLogout']);
    }

    public function google_redirect()
    {
        return Socialite::with('google')->redirect();
    }

    public function google()
    {
        $user = Socialite::with('google')->user();

        // $user->token;
    }
    public function github_redirect()
    {
        return Socialite::with('github')->redirect();
    }

    public function github()
    {
        $user = Socialite::with('github')->user();

        // $user->token;
    }

}