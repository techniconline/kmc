<?php namespace App\Http\Controllers\Auth;


use App\Employee;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Password;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Str;

class ResetPasswordController extends Controller
{

    public $employee;

    public function getReset($id)
    {
        if ($this->employee = Employee::find($id)) {
            if ($this->generateAndMailNewPassword()) {
                return redirect()->back();
            }
        }
    }

    public function postMail()
    {
        $email = Input::get('forgotEmail');
        if ($this->employee = Employee::where('email', $email)->first()) {
            if ($this->generateAndMailNewPassword()) {
                return response()->json(TRUE);
            }
        } else {
            return response()->json(FALSE);
        }
    }

    public function generateAndMailNewPassword()
    {
        $newPass = Str::random(8);
        $this->employee->password = $newPass;
        if ($this->employee->save()) {
            Mail::send('emails.password', ['newPass' => $newPass], function ($message) {
                $message->to($this->employee->email, $this->employee->lastname)->subject('New Password!');
            });
            return TRUE;
        }
        return FALSE;
    }

}