<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;

class FrontResetPasswordController extends Controller
{

    public $user;

    public function getReset($id)
    {
        if ($this->user = User::find($id)) {
            if ($this->generateAndMailNewPassword()) {
                return redirect()->back();
            }
        }
    }

    public function getEmail()
    {
        if (Auth::user()->get()) {
            return redirect()->back();
        } else {
            return view('auth.password');
        }
    }

    public function postEmail()
    {
        $email = Input::get('email');
        if ($this->user = User::where('email', $email)->first()) {
            if ($this->generateAndMailNewPassword()) {
//                return redirect()->back()->with('message','New Password Sent To Your Mail');
                return redirect()->back()->with('status', 'Sucessfuly Sent New Password To Your Mail');
            }
        } else {
            return redirect()->back()->withErrors(['email' => 'This Email Does Not Exist']);
        }
    }

    public function generateAndMailNewPassword()
    {
        $newPass = Str::random(8);
        $this->user->password = Hash::make($newPass);
        if ($this->user->save()) {
            Mail::send('emails.password', ['newPass' => $newPass], function ($message) {
                $message->to($this->user->email, $this->user->lastname)->subject('New Password!');
            });
            return TRUE;
        }
        return FALSE;
    }


}
