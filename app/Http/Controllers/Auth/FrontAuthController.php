<?php namespace App\Http\Controllers\Auth;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class FrontAuthController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers;

    protected $redirectTo = '';
    protected $loginPath = '';

    /**
     * Create a new authentication controller instance.
     *
     * @param  \Illuminate\Contracts\Auth\Registrar $registrar
     */
    public function __construct(Registrar $registrar)
    {
        $prefix = (app('locale') == '') ? '' : '/' . app('locale');
        $this->redirectTo = $prefix . '/front/profile';
        $this->loginPath = $prefix . '/front/auth/login';
//        var_dump(app('locale'));
//        var_dump($this->redirectTo);
//        die;

        $this->auth = Auth::user();
        $this->registrar = $registrar;

        $this->middleware('guest', ['except' => 'getLogout']);
    }

}