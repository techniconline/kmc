<?php namespace App\Http\Controllers;

use App\Language;
use App\Link;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Intervention\Image\Facades\Image;
use App\Faq;
use Illuminate\Support\Facades\Lang;

class GuideController extends Controller
{

    public $faq;
    public $locale;

    public function __construct(Faq $faq)
    {
        $this->faq = $faq;
        $this->locale = app('locale') == '' ? '' : app('locale') . '.';
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $dataType = Lang::get('faq.view.index.FAQ_GUIDE_DATA_TYPE');
        $faqData = [];
        foreach ($dataType as $key => $item) {
            $faqs = $this->faq
                ->join('faq_lang', 'faq_id', '=', 'faq.id')
                ->where('faq_lang.lang_id', '=', app('activeLangDetails')['lang_id'])
                ->where('type', $key)
                ->get();
            $faqData[$key] = $faqs;
        }
        return view('guide.index', [
            'faq_type' => $dataType,
            'faqs' => $faqData,
            'locale' => $this->locale
        ]);
    }

}
