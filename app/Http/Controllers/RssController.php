<?php namespace App\Http\Controllers;

use App\Content;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class RssController extends Controller
{

    public $content;
    public $locale;
    public $user_id;

    public function __construct(Content $content)
    {
        $this->content = $content;
        $this->locale = app('locale') == '' ? '' : app('locale') . '.';
        $this->user_id = isset(Auth::user()->get()->id) ? Auth::user()->get()->id : 0;

    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $data['news'] = $this->content->selectJoinLang(['type' => 'news'])
            ->where(function ($query) {
                if (!$this->user_id) {
                    $query->where('login', '=', 0);
                }
            })
            ->orderBy('updated_at', 'desc')
            ->get();
        $data['locale'] = $this->locale;

        return Response::view('rss.index', $data, 200, [
            'Content-Type' => 'application/atom+xml; charset=UTF-8'
        ]);
    }

}
