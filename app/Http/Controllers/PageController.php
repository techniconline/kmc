<?php namespace App\Http\Controllers;

use App\Language;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Lang;

class PageController extends Controller
{

    public $locale;

    public function __construct()
    {
        $this->locale = app('locale') == '' ? '' : app('locale') . '.';
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function show($page)
    {
        if ($page) {
            return view('pages.' . $page, ['title' => $page]);
        }

    }

}
