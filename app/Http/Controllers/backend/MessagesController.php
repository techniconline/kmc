<?php namespace App\Http\Controllers\backend;

use App\Message;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Laracasts\Flash\Flash;

class MessagesController extends BackendController
{

    public $message;
    public $locale;

    public function __construct(Message $message)
    {
        $this->message = $message;
        $this->locale = app('locale') == '' ? '' : app('locale') . '.';
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $messages = $this->message->where('status', '!=', '2')->orderBy('id', 'desc')->paginate(5);
        return view('backend.messages.index', [
            'messages' => $messages,
            'locale' => $this->locale
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = Input::all();

        $input_answer['to_id'] = $input['id'];
        list($input_answer['from'], $input_answer['to']) = array($input['to'], $input['from']);
        $input_answer['name'] = isset($input['name']) ? $input['name'] : '';
        $input_answer['message'] = $input['answer'];
        $input_answer['status'] = 2;


        if (!$this->message->fill($input_answer)->isValid()) {
            Flash::error(trans('backend/Message.messages.fillError'));
            return redirect()->back()->withInput()->withErrors($this->message->errors);
        }
        $this->message->save();
        Mail::send('emails.message', ['from' => $input_answer['from'], 'pm' => $input_answer['message']], function ($message) use ($input_answer) {
            $message->to($input_answer['to'])->subject('Answer');
        });

        $message = $this->message->find($input['id']);
        $message->status = 1;
        $message->save();

        Flash::success(trans('backend/Message.messages.success', ['email' => $input['from']]));
        return $this->index();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        if (Request::ajax()) {
            $message = $this->message->where('to_id', $id)->first();
            return $message;
        } else {
            $message = $this->message->find($id);
            return view('backend.messages.form', [
                'message' => $message,
                'locale' => $this->locale
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

}
