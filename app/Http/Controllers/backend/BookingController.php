<?php namespace App\Http\Controllers\backend;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Language;
use App\MailModel;
use App\Booking;
use App\Room;
use App\AttachType;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\DB;
use App\Profile;
use App\FileGenerate;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Response;

class BookingController extends Controller
{

    public $booking;
    public $room;
    public $attach;
    public $locale;
    public $langActive;
    public $language;
    public $listLang;
    public $user_id;
    public $storageUrl;

    public function __construct(Booking $booking, Language $language, Room $room, AttachType $attach)
    {
        $this->listLang = Config::get('app.locales');
        $this->booking = $booking;
        $this->room = $room;
        $this->attach = $attach;
        $this->langActive = app('getLocale');
        $this->language = $language;
        $this->locale = app('locale') == '' ? '' : app('locale') . '.';
        $this->user_id = isset(Auth::employee()->get()->id) ? Auth::employee()->get()->id : 0;
        $this->storageUrl = base_path() . env('PUBLIC_PATH_URL', 'public');

    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $inputs = Input::all();

        $pageData = $this->listBookingRender($inputs);
        if (!$pageData['action']) {
            $pageData = '';
        }

        if ((isset($inputs['page']) && $inputs['page']) || (isset($inputs['search']) && $inputs['search'])) {
            $pageDataTable = $pageData['pageData'];
            return ['action' => $pageData['action'], 'pageData' => $pageDataTable, 'message' => $pageData['message']];
        } else {
            return view('backend.booking.listBooking')
                ->with('pageData', $pageData['pageData'])
                ->with('locale', $this->locale);
        }
    }

    public function listArchiveBooking()
    {
        $inputs = Input::all();

        $pageData = $this->listBookingRender($inputs, true);
        if (!$pageData['action']) {
            $pageData = '';
        }

        if ((isset($inputs['page']) && $inputs['page']) || (isset($inputs['search']) && $inputs['search'])) {
            $pageDataTable = $pageData['pageData'];
            return ['action' => $pageData['action'], 'pageData' => $pageDataTable, 'message' => $pageData['message']];
        } else {
            return view('backend.booking.listArchiveBooking')
                ->with('pageData', $pageData['pageData'])
                ->with('locale', $this->locale);
        }
    }

    public function listBookingRender($inputs, $archive = false)
    {

        $lang = $this->language->getLanguage($this->langActive);
        $params['lang_id'] = $lang['lang_id'];
        $params['paginateNum'] = 10;
        $params['archive'] = $archive;

        $params['date_from'] = isset($inputs['date_from']) ? $inputs['date_from'] : false;
        $params['date_to'] = isset($inputs['date_to']) ? $inputs['date_to'] : false;

        $params['reg_date_from'] = isset($inputs['reg_date_from']) ? $inputs['reg_date_from'] : false;
        $params['reg_date_to'] = isset($inputs['reg_date_to']) ? $inputs['reg_date_to'] : false;

        $params['first_name'] = isset($inputs['first_name']) ? $inputs['first_name'] : false;
        $params['last_name'] = isset($inputs['last_name']) ? $inputs['last_name'] : false;

        $params['gender'] = isset($inputs['gender']) ? $inputs['gender'] : false;

        $params['country_name'] = isset($inputs['country_name']) ? $inputs['country_name'] : false;

        $dataListBookingUsers = $this->booking->getListBookingUsersWithDetails($params);

        $paramsAttach = [];
        $paramsAttach['lang_id'] = $lang['lang_id'];

        foreach ($dataListBookingUsers as &$item) {
            $paramsAttach['booking_id'] = $item->id;
            $dataListBookingUsersAttach = $this->attach->getListAttachUsers($paramsAttach);
            $item->documents = $dataListBookingUsersAttach;
            $item->text_status_document = '';
            if (isset($dataListBookingUsersAttach[0]) && $firstRowDoc = $dataListBookingUsersAttach[0]) {
                if ($firstRowDoc->document_status == 5) {
                    $item->text_status_document = Lang::get('backend/AttachType.messages.msgStatusDocumentRejected');
                } elseif ($firstRowDoc->document_status == 4) {
                    $item->text_status_document = Lang::get('backend/AttachType.messages.msgStatusDocumentIsOk');

                } elseif ($firstRowDoc->document_status == 1) {
                    $profileModel = new Profile();
                    $message = $profileModel->getMessagesBooking($item->id, 3, true);//document is not valid! and not completed
                    if (isset($message->id)) {
                        $item->text_status_document = Lang::get('backend/AttachType.messages.msgStatusDocumentIsNotComplete');
                    } else {
                        $item->text_status_document = Lang::get('backend/AttachType.messages.msgStatusDocumentNotCheck');
                    }
                }
            }

        }

        $paramsRoom['lang_id'] = $lang['lang_id'];
        $paramsRoom['type'] = 'room';
        $dataListRooms = $this->room->getListRooms($paramsRoom);
        $paramsRoom['type'] = 'parking';
        $dataListParking = $this->room->getListRooms($paramsRoom);

        $pageData = view('backend.booking.listBookingRender')
            ->with('dataBooking', $dataListBookingUsers)
            ->with('dataListRooms', $dataListRooms)
            ->with('dataListParking', $dataListParking)
            ->with('locale', $this->locale)->render();

        if (!$dataListBookingUsers)
            return ['action' => false, 'pageData' => $pageData, 'message' => Lang::get('backend/AttachType.messages.errValid')];

        return ['action' => true, 'pageData' => $pageData, 'message' => ''];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //update for view to administrator
        $booking = $this->booking->find($id);
        $booking->status_view = 1;
        $booking->save();

        return ['action' => true];


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function rejectBookingDetailsType($booking_id, $type)
    {
        $inputs = Input::all();
        $email = isset($inputs['email']) ? $inputs['email'] : '';
        $username = isset($inputs['username']) ? $inputs['username'] : '';
        $user_id = isset($inputs['user_id']) ? $inputs['user_id'] : 0;

        if (!$email || !$username || !$booking_id || !$type || !$user_id)
            return ['action' => false, 'message' => Lang::get('backend/Booing.messages.errValid')];

        $result = $this->booking->rejectBookingDetailsType($booking_id, $type);

        if ($result['action']) {

            $data = [];
            $data['username'] = isset($inputs['username']) ? $inputs['username'] : '';
            $data['email'] = isset($inputs['email']) ? $inputs['email'] : '';
            $data['page'] = 'emails.action-rent';

            if ($type == 'parking') {
                $data['subject'] = Lang::get('backend/Booking.messages.rejectBookingDetailsForRoom');
                $data['text'] = Lang::get('backend/Booking.messages.rejectBookingDetailsTextForRoom');
            } else {
                $data['subject'] = Lang::get('backend/Booking.messages.rejectBookingDetailsForParking');
                $data['text'] = Lang::get('backend/Booking.messages.rejectBookingDetailsTextForParking');
            }

            $params = [];
            $params['assessor_user_id'] = $assessor_user_id = Auth::employee()->get()->id;
            $params['booking_id'] = $booking_id;
            $params['user_id'] = $user_id;
            $params['subject'] = $data['subject'];
            $params['feedback_message'] = $data['text'];
            $params['status'] = 10; //for reject type of booking

            $resultMsg = $this->attach->addFeedBackForDocument($params);

            if (!$data['email'] || !$data['text'])
                return false;

            $mailModel = new MailModel();
            $resultMail = $mailModel->sendMail($data);
            if (!$resultMail) {
                return ['action' => false, 'result' => $resultMail, 'message' => Lang::get('backend/AttachType.messages.SendMailNotOk')];
            }
        }

        return $result;

    }

    public function sendContract($booking_id)
    {
        $bookingData = $this->booking->getBookingWithUserData($booking_id);

        if (!$bookingData)
            return false;

        $dataRoom = $this->booking->getRentDataUser($booking_id, 'room');
        $dataParking = $this->booking->getRentDataUser($booking_id, 'parking');

        return [
            'room_price' => (isset($dataRoom->price) && $dataRoom->price) ? $dataRoom->price : 0
            , 'room_price_9' => (isset($dataRoom->price) && $dataRoom->price) ? $dataRoom->price * 9 : 0
            , 'action_room' => $dataRoom ? true : false
            , 'parking_price' => (isset($dataParking->price) && $dataParking->price) ? $dataParking->price : 0
            , 'parking_price_9' => (isset($dataParking->price) && $dataParking->price) ? $dataParking->price * 9 : 0
            , 'action_parking' => $dataParking ? true : false
            , 'message' => (!$dataRoom && !$dataParking) ? Lang::get("backend/Booking.messages.NotFindRentedData") : ''
        ];


    }

    public function generateContract($booking_id)
    {
        $inputs = Input::all();
        $price_letters_room = isset($inputs['price_letters_room']) ? $inputs['price_letters_room'] : '';
        $price_letters_room_9 = isset($inputs['price_letters_room_9']) ? $inputs['price_letters_room_9'] : '';
        $price_letters_parking = isset($inputs['price_letters_parking']) ? $inputs['price_letters_parking'] : '';
        $price_letters_parking_9 = isset($inputs['price_letters_parking_9']) ? $inputs['price_letters_parking_9'] : '';

        if (!$price_letters_parking && !$price_letters_room)
            return ['action' => false, 'message' => Lang::get('backend/Booking.messages.FillItems')];

        if ($price_letters_parking && !$price_letters_parking_9) {
            return ['action' => false, 'message' => Lang::get('backend/Booking.messages.FillItems')];
        }

        if ($price_letters_room && !$price_letters_room_9) {
            return ['action' => false, 'message' => Lang::get('backend/Booking.messages.FillItems')];
        }

        $bookingData = $this->booking->getBookingWithUserData($booking_id);

        if (!$bookingData)
            return false;

        $age = 0;
        if ($bookingData->birth_date) {

            $age = date_diff(date_create($bookingData->birth_date), date_create('today'))->y;

        }

        $dataRoom = $this->booking->getRentDataUser($booking_id, 'room');
        $dataParking = $this->booking->getRentDataUser($booking_id, 'parking');

        if (!$dataRoom)
            return ['action' => false, 'message' => Lang::get('backend/Booking.messages.NotFindRentedData')];


        $data = [];
        $file = [];
        $fileGen = new FileGenerate();
        if ($dataRoom) {
            //-------------------
            $inputAttach = [];
            $inputAttach['user_id'] = $bookingData->user_id;
            $inputAttach['subFolder'] = 'contract-attach';
            $inputAttach['data'] = [
                'ROOM_PRICE' => ($dataRoom->price) ? $dataRoom->price : $dataRoom->rent_price
                , 'ROOM_PRICE_LETTER' => ($price_letters_room) ? $price_letters_room : ''
                , 'ROOM_PRICE_LETTER_9' => ($price_letters_room_9) ? $price_letters_room_9 : ''
                , 'GENDER' => $dataRoom->gender
                , 'FIRSTNAME' => $dataRoom->firstname
                , 'LASTNAME' => $dataRoom->lastname
            ];
            $inputAttach['template'] = 'pdf.contract1';
            $inputAttach['fileName'] = 'contract-1-' . date('Y-m-d_H-i-s');
            $res = $fileGen->createFile($inputAttach);
            if ($res) {
                $data['attachs'][] = $res['file'];
                $file[] = $res['urlStorageFile'];
            }
            $inputAttach = [];
            $inputAttach['user_id'] = $bookingData->user_id;
            $inputAttach['subFolder'] = 'contract-attach';
            $inputAttach['data'] = [
                'ROOM_PRICE' => (isset($dataRoom) && $dataRoom->price) ? $dataRoom->price : 0
                , 'PARKING_PRICE' => (isset($dataParking) && $dataParking->price) ? $dataParking->price : 0
                , 'ROOM_PRICE_LETTER' => ($price_letters_room) ? $price_letters_room : ''
                , 'ROOM_PRICE_LETTER_9' => ($price_letters_room_9) ? $price_letters_room_9 : ''
                , 'PARKING_PRICE_LETTER' => ($price_letters_parking) ? $price_letters_parking : ''
                , 'PARKING_PRICE_LETTER_9' => ($price_letters_parking_9) ? $price_letters_parking_9 : ''
                , 'GENDER' => $dataRoom->gender
                , 'FIRSTNAME' => $dataRoom->firstname
                , 'LASTNAME' => $dataRoom->lastname
                , 'ADDRESS' => $dataRoom->address
                , 'BIRTH_DATE' => $dataRoom->birth_date
                , 'POSTAL_CODE' => $dataRoom->code_postal
                , 'CITY' => $dataRoom->city
                , 'ROOM_NUMBER' => $dataRoom->room_number
                , 'ROOM_AREA' => $dataRoom->area
            ];
            $inputAttach['template'] = 'pdf.contract2';
            $inputAttach['fileName'] = 'contract-2-' . date('Y-m-d_H-i-s');
            $res = $fileGen->createFile($inputAttach);
            if ($res) {
                $data['attachs'][] = $res['file'];
                $file[] = $res['urlStorageFile'];
            }

//            $entry = Fileentry::where('filename', '=', $filename)->firstOrFail();
//            $file = Storage::disk('local')->get($entry->filename);
            //-------------------
        }


        return ['action' => true
            , 'result' => $file
            , 'titleBtn' => Lang::get("backend/Booking.view.listBooking.sendContractMail")
            , 'message' => Lang::get('backend/AttachType.messages.GenerateOk')];


    }

    public function sendMailContract($booking_id)
    {
        $inputs = Input::all();
        $contract1 = isset($inputs['file_contract1']) ? $inputs['file_contract1'] : '';
        $contract2 = isset($inputs['file_contract2']) ? $inputs['file_contract2'] : '';

        $bookingData = $this->booking->getBookingWithUserData($booking_id);

        $data = [];
        $data['username'] = isset($bookingData) ? $bookingData->firstname . ' ' . $bookingData->lastname : '';
        $data['email'] = isset($bookingData->email) ? $bookingData->email : '';
        $data['page'] = 'emails.sendContract';
        $data['subject'] = Lang::get('backend/Booking.messages.sendContractSubject');
        $data['text'] = Lang::get('backend/Booking.messages.sendContractText');

        $params = [];
        $params['assessor_user_id'] = $assessor_user_id = Auth::employee()->get()->id;
        $params['booking_id'] = $booking_id;
        $params['user_id'] = $bookingData->user_id;
        $params['subject'] = $data['subject'];
        $params['feedback_message'] = $data['text'];
        $params['status'] = 12; //for send Contract

        $resultMsg = $this->attach->addFeedBackForDocument($params);

        if (!$data['email'] || !$data['text'])
            return false;

        $data['attachs'][] = $this->storageUrl . '/attach/attach-contract-1.pdf';
        $data['attachs'][] = $this->storageUrl . '/attach/attach-contract-2.pdf';
        if ($contract1) {
            $data['attachs'][] = $this->storageUrl . '/' . $contract1;
        }
        if ($contract2) {
            $data['attachs'][] = $this->storageUrl . '/' . $contract2;
        }

        if (!$contract1 || !$contract2)
            return ['action' => false, 'message' => Lang::get('backend/Booking.messages.errValid')];


        $mailModel = new MailModel();
        $resultMail = $mailModel->sendMail($data);
        if (!$resultMail) {
            return ['action' => false, 'result' => $resultMail, 'message' => Lang::get('backend/AttachType.messages.SendMailNotOk')];
        }

        $dBooking = $this->booking->find($booking_id);
        $dBooking->send_contract = 1;
        $dBooking->save();

        return ['action' => true, 'result' => $resultMail, 'message' => Lang::get('backend/AttachType.messages.SendMailOk')];


    }

}
