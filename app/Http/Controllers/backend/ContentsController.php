<?php namespace App\Http\Controllers\backend;

use App\BaseModel;
use App\Category;
use App\Content;
use App\ContentLang;
use App\Language;
use App\Media;
use App\Providers\Helpers\Category\CategoryHelper;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Laracasts\Flash\Flash;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Request;

class ContentsController extends BackendController
{

    public $content;
    public $category;

    public $locale;
    public $langActive;
    public $language;
    public $listLang;

    public function __construct(Category $category, Content $content, Language $language)
    {
        $this->content = $content;
        $this->category = $category;
        $this->listLang = Config::get('app.locales');
        $this->langActive = app('getLocale');
        $this->language = $language;
        $this->locale = app('locale') == '' ? '' : app('locale') . '.';
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $contents = $this->content
            ->join('contents_lang', 'contents_id', '=', 'contents.id')
            ->join('employee', 'contents.author_id', '=', 'employee.id')
            ->where('contents_lang.lang_id', '=', app('activeLangDetails')['lang_id'])
            ->whereNotIn('type', ['news', 'weblog'])
            ->where('contents.status', 1)
            ->orderBy('contents.id', 'DESC')
            ->get();
        return view('backend.content.index', [
            'contents' => $contents,
            'locale' => $this->locale,
            'type' => 'content'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $results = $this->category->getDataCategory(2);
        $roots = $results['roots'];
        $translatedName = $results['translatedLang'];

        return view('backend.content.form', [
            'locale' => $this->locale,
            'listLang' => $this->listLang,
            'type' => 'content'
        ])->with('roots', $roots)
            ->with('translatedName', $translatedName);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = Input::all();

        $input_content['url'] = $input['url'];
        $input['author_id'] = $input_content['employee_id'] = $input_content['author_id'] = Auth::employee()->get()->id ? Auth::employee()->get()->id : 0;
        $input_content['status'] = $input['status'];
        $input_content['type'] = $input['type'];
        $input_content['sort'] = isset($input['sort']) && $input['sort'] ? $input['sort'] : 1;
        $URL = isset($input['url']) && $input['url'] ? str_replace(" ", "-", $input['url']) : CategoryHelper::createAlias($input['link_title']);
        $input['url'] = $input_content['url'] = $URL;
        $input['category_id'] = isset($input['categories']) ? $input['categories'][0] : null;

        $fill = $this->content->fill($input_content);
        $valid = $fill->validationData($input, $this->content->rules);
        if (!$valid) {
            return redirect()->back()->withInput()->withErrors($this->content->errors);
        }
        $this->content->save();

        if ($id = $this->content->id) {
            $resCat = $this->content->saveContentCategories($id, $input['categories']);

            if (Request::hasFile('image_content')) {
                $extension = Input::file('image_content')->getClientOriginalExtension();
                $createFolder = base_path() . env('PUBLIC_PATH_URL', 'public') . '/uploads/images/image_content/' . $id . '/';
                if (!file_exists($createFolder)) {
                    mkdir($createFolder, 0755);
                }
                if (File::exists(base_path() . env('PUBLIC_PATH_URL', 'public') . '/uploads/images/image_content/' . $id . '/' . $id . '-60x60.' . $extension)) {
                    File::delete($id . '-60x60.' . $extension);
                }
                if (File::exists(base_path() . env('PUBLIC_PATH_URL', 'public') . '/uploads/images/image_content/' . $id . '/' . $id . '-160x80.' . $extension)) {
                    File::delete($id . '-160x80.' . $extension);
                }
                if (File::exists(base_path() . env('PUBLIC_PATH_URL', 'public') . '/uploads/images/image_content/' . $id . '/' . $id . '-600x400.' . $extension)) {
                    File::delete($id . '-600x400.' . $extension);
                }
                Image::make(Input::file('image_content'))->fit(60, 60)->save('uploads/images/image_content/' . $id . '/' . $id . '-60x60.' . $extension);
                Image::make(Input::file('image_content'))->fit(160, 80)->save('uploads/images/image_content/' . $id . '/' . $id . '-160x80.' . $extension);
                Image::make(Input::file('image_content'))->fit(600, 400)->save('uploads/images/image_content/' . $id . '/' . $id . '-600x400.' . $extension);
                $updateContent = $this->content->find($id);
                $updateContent->image_content = '/uploads/images/image_content/' . $id . '/';
                $updateContent->extension = $extension;
                $updateContent->save();
                $messages[] = ['level' => BaseModel::level_success, 'message' => Lang::get('backend/Content.messages.uploadFileSuccess')];
            }


            $input['contents_id'] = $id;
            foreach ($this->listLang as $key => $value) {
                $content_lang = new ContentLang();
                $lang = $this->language->getLanguage($key);
                $input['lang_id'] = $lang['lang_id'];
                $input['title_tags'] = CategoryHelper::createTags($input['link_title']);
                $input['tags_upd'] = array_unique(explode(",", $input['title_tags'] . ',' . $input['tags']));
                $input['tags'] = implode(",", $input['tags_upd']);

                if (!$content_lang->fill($input)->isValid()) {
                    return redirect()->back()->withInput()->withErrors($content_lang->errors);
                }
                $content_lang->save();
            }

            $messages[] = ['level' => BaseModel::level_success, 'message' => Lang::get('backend/Content.messages.successInsert')];
            Session::flash('messages', $messages);

            return Redirect::route($this->locale . 'backend.content.index')->with('locale', $this->locale);
        }
        Flash::error(Lang::get('backend/Content.messages.errCreate'));
        return Redirect::back()->withErrors(Lang::get('backend/Content.messages.errCreate'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $results = $this->category->getDataCategory(2);
        $roots = $results['roots'];
        $translatedName = $results['translatedLang'];
        $categoriesContent = $this->content->getContentCategories($id, true);

        $content = $this->content->where('id', $id)
            ->join('contents_lang', 'id', '=', 'contents_id')
            ->where('contents_lang.lang_id', '=', app('activeLangDetails')['lang_id'])
            ->first();

        $params=[];
        $params['ctrl']='content';
        $params['itemId']=$id;
        $params['type']='image';
        $images = $this->content->getMediaUpload($params);

        return view('backend.content.form', [
            'images' => $images,
            'content' => $content,
            'categoriesContent' => $categoriesContent,
            'locale' => $this->locale,
            'listLang' => $this->listLang,
            'type' => 'content'
        ])->with('roots', $roots)
            ->with('translatedName', $translatedName);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $input = Input::all();
        $lang = $this->language->getLanguage($input['lang']);
        $input['lang_id'] = $lang['lang_id'];

        $updateContent = $this->content->find($id);
        $URL = isset($input['url']) && $input['url'] ? str_replace(" ", "-", $input['url']) : CategoryHelper::createAlias($input['link_title']);
        $input['url'] = $URL;
        $input['title_tags'] = CategoryHelper::createTags($input['link_title']);

        $updateContent->url = $input['url'];
        $updateContent->type = $input['type'];
        $updateContent->sort = isset($input['sort']) && $input['sort'] ? $input['sort'] : 1;
        $input['tags_upd'] = array_unique(explode(",", $input['title_tags'] . ',' . $input['tags']));

        $paramsForTranslate = [
            'contents_id' => $id,
            'lang_id' => $input['lang_id'],
            'link_title' => $input['link_title'],
            'browser_title' => $input['browser_title'],
            'body_title' => $input['body_title'],
            'tags' => implode(",", $input['tags_upd']),
            'short_content' => $input['short_content'],
            'body_content' => $input['body_content']

        ];
        ContentLang::updateLang($paramsForTranslate);
        $resCat = $this->content->saveContentCategories($id, $input['categories']);

        if (Request::hasFile('image_content')) {
            $extension = Input::file('image_content')->getClientOriginalExtension();
            $createFolder = base_path() . env('PUBLIC_PATH_URL', 'public') . '/uploads/images/image_content/' . $id . '/';
            if (!file_exists($createFolder)) {
                mkdir($createFolder, 0755);
            }
            if (File::exists(base_path() . env('PUBLIC_PATH_URL', 'public') . '/uploads/images/image_content/' . $id . '/' . $id . '-60x60.' . $extension)) {
                File::delete($id . '-60x60.' . $extension);
            }
            if (File::exists(base_path() . env('PUBLIC_PATH_URL', 'public') . '/uploads/images/image_content/' . $id . '/' . $id . '-160x80.' . $extension)) {
                File::delete($id . '-160x80.' . $extension);
            }
            if (File::exists(base_path() . env('PUBLIC_PATH_URL', 'public') . '/uploads/images/image_content/' . $id . '/' . $id . '-600x400.' . $extension)) {
                File::delete($id . '-600x400.' . $extension);
            }
            Image::make(Input::file('image_content'))->fit(60, 60)->save('uploads/images/image_content/' . $id . '/' . $id . '-60x60.' . $extension);
            Image::make(Input::file('image_content'))->fit(160, 80)->save('uploads/images/image_content/' . $id . '/' . $id . '-160x80.' . $extension);
            Image::make(Input::file('image_content'))->fit(600, 400)->save('uploads/images/image_content/' . $id . '/' . $id . '-600x400.' . $extension);
//            $updateContent = $this->content->find($id);
            $updateContent->image_content = '/uploads/images/image_content/' . $id . '/';
            $updateContent->extension = $extension;
            $messages[] = ['level' => BaseModel::level_success, 'message' => Lang::get('backend/Content.messages.uploadFileSuccess')];
        }

        $updateContent->save();

        $messages[] = ['level' => BaseModel::level_success, 'message' => Lang::get('backend/Content.messages.successUpdate')];
        Session::flash('messages', $messages);

        return Redirect::back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $content = $this->content->find($id);
//        $content->status = 0;
//        $content->save();
        $content->delete();
        DB::table('contents_lang')->where('contents_id', $id)->delete();
        DB::table('content_categories')->where('content_id', $id)->delete();

        Flash::success(trans('backend/Content.messages.successDelete'));
        return Redirect::back();
    }

}
