<?php namespace App\Http\Controllers\backend;

use Illuminate\Support\Facades\App;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Billing;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Lang;
use App\Providers\Helpers\Category\CategoryHelper;
use App\Language;
use App\MailModel;
use App\Booking;
use App\Room;
use Laracasts\Flash\Flash;
use App\AttachType;

class BillingController extends BackendController
{

    public $booking;
    public $attach;
    public $room;
    public $billing;
    public $locale;
    public $langActive;
    public $language;
    public $listLang;
    public $user_id;

    public function __construct(Billing $billing, Booking $booking, Language $language, Room $room, AttachType $attach)
    {
        $this->listLang = Config::get('app.locales');
        $this->booking = $booking;
        $this->room = $room;
        $this->attach = $attach;
        $this->billing = $billing;
        $this->langActive = app('getLocale');
        $this->language = $language;
        $this->locale = app('locale') == '' ? '' : app('locale') . '.';
        $this->user_id = isset(Auth::employee()->get()->id) ? Auth::employee()->get()->id : 0;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $inputs = Input::all();

        $pageData = $this->listBillingRender($inputs);
        if (!$pageData['action']) {
            $pageData = '';
        }

        if ((isset($inputs['page']) && $inputs['page']) || (isset($inputs['search']) && $inputs['search']) || Request::ajax()) {
            $pageDataTable = $pageData['pageData'];
            return ['action' => $pageData['action'], 'pageData' => $pageDataTable, 'message' => $pageData['message']];
        } else {
            return view('backend.billing.listBilling')
                ->with('pageData', $pageData['pageData'])
                ->with('locale', $this->locale);
        }
    }

    public function listBillingRender($inputs)
    {

        $lang = $this->language->getLanguage($this->langActive);
        $params['lang_id'] = $lang['lang_id'];
        $params['paginateNum'] = 10;

        $params['date_from'] = isset($inputs['date_from']) ? $inputs['date_from'] : false;
        $params['date_to'] = isset($inputs['date_to']) ? $inputs['date_to'] : false;

        $params['reg_date_from'] = isset($inputs['reg_date_from']) ? $inputs['reg_date_from'] : false;
        $params['reg_date_to'] = isset($inputs['reg_date_to']) ? $inputs['reg_date_to'] : false;

        $params['email'] = isset($inputs['email']) ? $inputs['email'] : false;
        $params['last_name'] = isset($inputs['last_name']) ? $inputs['last_name'] : false;

        $params['price'] = isset($inputs['price']) ? $inputs['price'] : '';

        $params['room_number'] = isset($inputs['room_number']) ? $inputs['room_number'] : '';

        $dataListBillingUsers = $this->billing->getListBillingUsersWithDetails($params);


        $pageData = view('backend.billing.listBillingRender')
            ->with('dataBilling', $dataListBillingUsers)
            ->with('locale', $this->locale)->render();

        if (!$dataListBillingUsers)
            return ['action' => false, 'pageData' => $pageData, 'message' => Lang::get('backend/Billing.messages.errValid')];

        return ['action' => true, 'pageData' => $pageData, 'message' => ''];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $lang = $this->language->getLanguage($this->langActive);
        $params['lang_id'] = $lang['lang_id'];

        $listRentedRooms = $this->billing->getListRentedRooms($params);

        return view('backend.billing.form')
            ->with('title', Lang::get('backend/Billing.view.form.TitleSave'))
            ->with('btnTitle', Lang::get('backend/Billing.view.form.Save'))
            ->with('listRentedRooms', $listRentedRooms)
            ->with('locale', $this->locale);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = Input::all();
        $input['assessor_user_id'] = $this->user_id;

        if (!$rent_id = $input['rent_id']) {
            Flash::error(Lang::get('backend/Billing.messages.YouRentIsUnavailable'));
            $return = Redirect::back()->withInput();
            return $return;
        }

        $dataRent = $this->billing->getRentData($rent_id);

        if (!$dataRent) {
            Flash::error(Lang::get('backend/Billing.messages.YouRentIsUnavailable'));
            $return = Redirect::back()->withInput();
            return $return;
        }

        $input['rent_id'] = $dataRent->id;
        $input['user_id'] = $dataRent->user_id;
        $input['room_id'] = $dataRent->room_id;
        $input['booking_id'] = $dataRent->booking_id;
        $input['status'] = 1;
        $year = 0;
        $month = 0;
        if (isset($input['rent_date']) && $input['rent_date']) {
            $dateArr = explode("-", $input['rent_date'], 2);
            $year = $input['year'] = strlen($dateArr[0]) == 4 ? $dateArr[0] : 0;
            $month = $input['month'] = strlen($dateArr[1]) >= 1 || strlen($dateArr[1]) <= 2 ? $dateArr[1] : 0;
        }
//        $input['billing_status']=1;
        $input['price'] = isset($input['price']) ? $input['price'] : $dataRent->price;
        $input['pay_date'] = date('Y-m-d');

        $fill = $this->billing->fill($input);
        $isValid = $fill->isValid();
        if (!$isValid) {
            Flash::error($this->billing->errors);
            $return = Redirect::back()->withInput()->withErrors($this->billing->errors);
            return $return;
        }

        $dataRentDuplicate = $this->billing
            ->where('year', $year)
            ->where('month', $month)
            ->where('user_id', $dataRent->user_id)
            ->where('room_id', $dataRent->room_id)
            ->where('booking_id', $dataRent->booking_id)
            ->where('rent_id', $dataRent->id)
            ->where('status', '!=', 0)->first();

        if ($dataRentDuplicate) {
            Flash::error(Lang::get('backend/Billing.messages.errCreateDuplicate'));
            return Redirect::route($this->locale . 'backend.billing.index');
        }

        $this->billing->save();

        if ($id = $this->billing->id) {
            Flash::success(Lang::get('backend/Billing.messages.addNewRent'));
            $input['id'] = $id;

            //send Email

            $data = [];
            $data['username'] = $dataRent->firstname . ' ' . $dataRent->lastname;
            $data['email'] = $dataRent->email;
            $data['page'] = 'emails.invoice';


            $data['subject'] = Lang::get('backend/Billing.messages.SubjectInvoice');
            $data['text'] = Lang::get('backend/Billing.messages.TextInvoice') . ' ' . $input['rent_date'] . ' ' . Lang::get('backend/Billing.messages.Is') . ' ' . $input['price'] . ' ' . Lang::get('backend/Billing.messages.Euro') . '.';
            $data['text2'] = Lang::get('backend/Billing.messages.ThankYou');

            $params['subject'] = $data['subject'];
            $params['feedback_message'] = $data['text'];
            $params['status'] = 15; //for invoice

            $resultMsg = $this->attach->addFeedBackForDocument($params);

            if (!$data['email'] || !$data['text'])
                return false;

            $mailModel = new MailModel();
            $resultMail = $mailModel->sendMail($data);
            if (!$resultMail) {
                return ['action' => false, 'result' => $resultMail, 'message' => Lang::get('backend/AttachType.messages.SendMailNotOk')];
            }
            //---------

            return Redirect::route($this->locale . 'backend.billing.index');
        }
        Flash::error(Lang::get('backend/Billing.messages.errCreate'));
        return Redirect::back()->withErrors(Lang::get('backend/Billing.messages.errCreate'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($date)
    {
        $dateArr = explode("-", $date, 2);
        $year = $input['year'] = strlen($dateArr[0]) == 4 ? $dateArr[0] : 0;
        $month = $input['month'] = strlen($dateArr[1]) >= 1 || strlen($dateArr[1]) <= 2 ? $dateArr[1] : 0;

        if (!$year || !$month)
            return ['action' => false, 'message' => Lang::get('backend/Billing.messages.NotValidData')];

        $data = $this->billing->getListInvoice();
        foreach ($data as &$item) {
            $item->status = $item->billing_status == 2 ? 'Unpaid' : '';
        }


        \Excel::create('ExportInvoiceList-' . date('Y-m-d_H-i-s'), function ($excel) use ($data) {

            $excel->sheet('ExportInvoiceList', function ($sheet) use ($data) {

                $sheet->fromArray($data);

            });

        })->download('xls');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $lang = $this->language->getLanguage($this->langActive);
        $params['lang_id'] = $lang['lang_id'];

        $data = $this->billing->find($id);
        $listRentedRooms = $this->billing->getListRentedRooms($params);

        return view('backend.billing.form', [
            'data' => $data
        ])
            ->with('listRentedRooms', $listRentedRooms)
            ->with('title', Lang::get('backend/Billing.view.form.TitleSave'))
            ->with('btnTitle', Lang::get('backend/Billing.view.form.Save'))
            ->with('locale', $this->locale);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $data = $this->billing->find($id);

        $input = Input::all();
        $data->assessor_user_id = $input['assessor_user_id'] = $this->user_id;

        if (!$rent_id = $input['rent_id']) {
            Flash::error(Lang::get('backend/Billing.messages.YouRentIsUnavailable'));
            $return = Redirect::back()->withInput();
            return $return;
        }

        $dataRent = $this->billing->getRentData($rent_id);

        if (!$dataRent) {
            Flash::error(Lang::get('backend/Billing.messages.YouRentIsUnavailable'));
            $return = Redirect::back()->withInput();
            return $return;
        }


        $data->rent_id = $input['rent_id'] = $dataRent->id;
        $data->user_id = $input['user_id'] = $dataRent->user_id;
        $data->room_id = $input['room_id'] = $dataRent->room_id;
        $data->booking_id = $input['booking_id'] = $dataRent->booking_id;
        $data->status = 1;
        if (isset($input['rent_date']) && $input['rent_date']) {
            $dateArr = explode("-", $input['rent_date'], 2);
            $data->year = $input['year'] = strlen($dateArr[0]) == 4 ? $dateArr[0] : 0;
            $data->month = $input['month'] = strlen($dateArr[1]) >= 1 || strlen($dateArr[1]) <= 2 ? $dateArr[1] : 0;
        }
//        $input['billing_status']=1;
        $input['price'] = isset($input['price']) ? $input['price'] : $dataRent->price;
        $input['pay_date'] = date('Y-m-d');

        $fill = $data->fill($input);
        $isValid = $fill->isValid();
        if (!$isValid) {
            Flash::error($data->errors);
            $return = Redirect::back()->withInput()->withErrors($data->errors);
            return $return;
        }

        if ($data->save()) {
            Flash::success(Lang::get('backend/Billing.messages.msgUpdateOk'));
            $input['id'] = $id;

            //send Email

            $data = [];
            $data['username'] = $dataRent->firstname . ' ' . $dataRent->lastname;
            $data['email'] = $dataRent->email;
            $data['page'] = 'emails.invoice';


            $data['subject'] = Lang::get('backend/Billing.messages.SubjectInvoice');
            $data['text'] = Lang::get('backend/Billing.messages.TextInvoice') . ' ' . $input['rent_date'] . ' ' . Lang::get('backend/Billing.messages.Is') . ' ' . $input['price'] . ' ' . Lang::get('backend/Billing.messages.Euro') . '.';
            $data['text2'] = Lang::get('backend/Billing.messages.ThankYou');

            $params['subject'] = $data['subject'];
            $params['feedback_message'] = $data['text'];
            $params['status'] = 15; //for invoice
            $params['assessor_user_id'] = $this->user_id;
            $params['user_id'] = $dataRent->user_id;
            $params['booking_id'] = $dataRent->booking_id;

            $resultMsg = $this->attach->addFeedBackForDocument($params);

            if (!$data['email'] || !$data['text'])
                return false;

            $mailModel = new MailModel();
            $resultMail = $mailModel->sendMail($data);
            if (!$resultMail) {
                return ['action' => false, 'result' => $resultMail, 'message' => Lang::get('backend/AttachType.messages.SendMailNotOk')];
            }
            //---------

            return Redirect::back();
        }
        Flash::error(Lang::get('backend/Billing.messages.msgUpdateNotOk'));
        return Redirect::back()->withErrors(Lang::get('backend/Billing.messages.msgUpdateNotOk'));


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function generateListRent($date)
    {

        $dateArr = explode("-", $date, 2);
        $year = $input['year'] = strlen($dateArr[0]) == 4 ? $dateArr[0] : 0;
        $month = $input['month'] = strlen($dateArr[1]) >= 1 || strlen($dateArr[1]) <= 2 ? $dateArr[1] : 0;

        if (!$year || !$month)
            return ['action' => false, 'message' => Lang::get('backend/Billing.messages.NotValidDataForGenerate')];


        $result = $this->billing->generateRentList($year, $month, $this->user_id);

        return ['action' => true, 'result' => $result['result'], 'resultMail' => $result['resultMail'], 'message' => Lang::get('backend/Billing.messages.SuccessGenerate')];


    }

    public function sendMailRememberDueInvoice($date)
    {
        $dateArr = explode("-", $date, 2);
        $year = $input['year'] = strlen($dateArr[0]) == 4 ? $dateArr[0] : 0;
        $month = $input['month'] = strlen($dateArr[1]) >= 1 || strlen($dateArr[1]) <= 2 ? $dateArr[1] : 0;

        if (!$year || !$month)
            return ['action' => false, 'message' => Lang::get('backend/Billing.messages.NotValidData')];

        $result = $this->billing->sendMailForRememberInvoiceDueDate($year, $month, $this->user_id);

        return $result;

    }
}
