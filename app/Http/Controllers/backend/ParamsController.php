<?php namespace App\Http\Controllers\backend;

use App\ContentLang;
use App\Param;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Intervention\Image\Facades\Image;
use Laracasts\Flash\Flash;

class ParamsController extends BackendController
{

    public $param;
    public $locale;
    public $types = [
        'pay' => 'pay',
        'image' => 'image',
        'system' => 'system',
        'email' => 'email',
        'other' => 'other'
    ];

    public function __construct(Param $param)
    {
        $this->param = $param;
        $this->locale = app('locale') == '' ? '' : app('locale') . '.';
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $params = $this->param->where('status', 1)->get();
        return view('backend.param.index', [
            'params' => $params,
            'locale' => $this->locale
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.param.form', [
            'locale' => $this->locale,
            'types' => $this->types
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = Input::all();
        $input['type'] = $this->types[$input['type']];
        $input['status'] = 1;

        if (Request::hasFile('img')) {
            $randName = rand(11111111, 99999999);
            $extension = Input::file('img')->getClientOriginalExtension();

            if (File::exists(base_path() . env('PUBLIC_PATH_URL', 'public') . '/public/uploads/images/Params/' . $randName . '-' . $input['alias'] . '.' . $extension)) {
                File::delete($input['name'] . '-' . $input['alias'] . '.jpg');
            }
            Image::make(Input::file('img'))->save('uploads/images/Params/' . $randName . '-' . $input['alias'] . '.' . $extension);
            $input['value'] = $randName . '-' . $input['alias'] . '.' . $extension;
        }

        if (!$this->param->fill($input)->isValid()) {
            Flash::error(trans('backend/Param.messages.fillError'));
            return redirect()->back()->withInput()->withErrors($this->param->errors);
        }
        $this->param->save();

        Flash::success(trans('backend/Param.messages.success'));
        return $this->index();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $param = $this->param->find($id);
        return view('backend.param.form', [
            'param' => $param,
            'locale' => $this->locale,
            'types' => $this->types
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $param = $this->param->find($id);
        $input = Input::all();

        if (!$param->fill($input)->isValid()) {
            Flash::error(trans('backend/Param.messages.fillError'));
            return redirect()->back()->withInput()->withErrors($param->errors);
        }
        $param->save();

        Flash::success(trans('backend/Param.messages.successUpd'));
        return Redirect::back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $param = $this->param->find($id);
        $param->status = 0;
        $param->save();

        Flash::success(trans('backend/Param.messages.successDel'));
        return Redirect::back();
    }

}
