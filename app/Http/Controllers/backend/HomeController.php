<?php namespace App\Http\Controllers\backend;

use App\Calendar;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\backend\RoomController;
use Illuminate\Support\Facades\Request;
use App\Room;
use App\Language;
use Illuminate\Support\Facades\Config;

class HomeController extends BackendController
{

    public $room;
    public $locale;
    public $langActive;
    public $language;
    public $listLang;
    /*
    |--------------------------------------------------------------------------
    | Home Controller
    |--------------------------------------------------------------------------
    |
    | This controller renders your application's "dashboard" for users that
    | are authenticated. Of course, you are free to change or remove the
    | controller as you wish. It is just here to get your app started!
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Room $room, Language $language)
    {
        $this->listLang = Config::get('app.locales');
        $this->langActive = app('getLocale');
        $this->language = $language;
        $this->locale = app('locale') == '' ? '' : app('locale') . '.';

        $this->room = $room;
    }

    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
    public function index()
    {
        $calendars = Calendar::where('employee_id', Auth::employee()->get()->id)->get(['title', 'start', 'end', 'all_day', 'color', 'id'])->toArray();

        return view('backend.dashboard.index', [
            'reminders' => $calendars,
            'locale' => $this->locale
        ]);
    }


}
