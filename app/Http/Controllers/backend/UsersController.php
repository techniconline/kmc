<?php namespace App\Http\Controllers\backend;

use App\Content;
use App\ContentLang;
use App\Email;
use App\Language;
use App\Media;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Lang;

use App\Profile;

class UsersController extends BackendController
{

    public $user;
    public $locale;
    public $langActive;
    public $language;
    public $listLang;

    public function __construct(User $user, Language $language)
    {

        $this->user = $user;
        $this->listLang = Config::get('app.locales');
        $this->langActive = app('getLocale');
        $this->language = $language;
        $this->locale = app('locale') == '' ? '' : app('locale') . '.';
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $inputs = Input::all();
        $inputs['filtering'] = isset($inputs['filtering']) ? $inputs['filtering'] : '';
        $users = $this->user->leftJoin('countries AS c', 'c.id', '=', 'country_id')
            ->where(function ($query) use ($inputs) {
                if ($inputs['filtering'] != '') {
                    if ($inputs['lastname'] != '') {
                        $query->where('lastname', 'LIKE', '%' . $inputs['lastname'] . '%');
                    }
                    if ($inputs['email'] != '') {
                        $query->where('email', 'LIKE', '%' . $inputs['email'] . '%');
                    }
                    if ($inputs['country'] != '') {
                        $query->where('c.name', 'LIKE', '%' . $inputs['country'] . '%');
                    }
                    if ($inputs['mobile'] != '') {
                        $query->where('mobile', 'LIKE', '%' . $inputs['mobile'] . '%');
                    }
                }
            })
            ->select(['users.*', 'c.name'])
            ->paginate(10);
        $users = view('backend.users.render', [
            'users' => $users,
            'locale' => $this->locale
        ])->render();

        if ($inputs['filtering'] != '' || Request::ajax()) {
            return $users;
        } else {
            return view('backend.users.index', [
                'users' => $users,
                'locale' => $this->locale
            ]);
        }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $emails = Email::where('emails.user_id', $id)->leftJoin('media', 'media.id', '=', 'media_id')->orderBy('emails.id', 'DESC')->get(['emails.*', 'media.src']);
        return view('backend.users.email', [
            'emails' => $emails,
            'locale' => $this->locale
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $user = $this->user->where('users.id', $id)->leftJoin('countries AS c', 'c.id', '=', 'country_id')->get(['users.*', 'c.name'])->first();
        $countries = DB::table('countries')->lists('name', 'id');

        $inputs['user_id'] = $id;


        return view('backend.users.form', [
            'user' => $user,
            'countries' => $countries,
            'locale' => $this->locale
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $input = Input::except(['avatar']);

        $user = $this->user->find($id);

        if (Request::hasFile('avatar')) {
            if (File::exists(base_path() . env('PUBLIC_PATH_URL', 'public') . '/uploads/images/Users/' . $user->id . '-200x200.jpg')) {
                File::delete($user->id . '-200x200.jpg');
            }
            Image::make(Input::file('avatar'))->fit(200, 200)->save('uploads/images/Users/' . $user->id . '-200x200.jpg');
        }

        $user->fill($input);
        $user->save();

        return Redirect::back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function mail()
    {
        if (Input::get('images-docs')[0] != '') {
            $media_id = Input::get('images-docs')[0];
            $media = Media::find($media_id);
            $media_path = base_path() . env('PUBLIC_PATH_URL', 'public') . '/' . $media->src;
        } else {
            $media_path = '';
        }

        $subject = Input::get('subject');
        $txt = Input::get('message');
        $ids = Input::get('id');

        $users = User::where('status', 1)->where(function ($query) use ($ids) {
            if ($ids == 0) {
                $query->where('id', '!=', 0);
            } else {
                $query->where('id', $ids);
            }
        })->get();

        foreach ($users as $key => $user) {
            Mail::send('emails.users', ['subject' => $subject, 'txt' => $txt], function ($message) use ($media_path, $user, $subject) {
                $message->to($user->email, $user->lastname)->subject($subject);
                if ($media_path != '')
                    $message->attach($media_path);
            });

            $email = new Email();
            $email->user_id = $user->id;
            $email->subject = $subject;
            $email->media_id = isset($media_id) ? $media_id : 0;
            $email->url_attach = '';
            $email->message = $txt;
            $email->save();
        }

        echo 'true';
    }

    public function showEmail()
    {
        $mail = Email::find(Input::get('id'));
        return view('backend.users.showEmail', [
            'mail' => $mail
        ]);
    }

    public function formSendMailGroup()
    {
        $allUsers = $this->user->all(['id', 'firstname', 'lastname']);
        return view('backend.users.form-mail')
            ->with('users', $allUsers)
            ->with('locale', $this->locale);


    }

    public function sendMailGroup()
    {

        $inputs = Input::all();
        $ids = $inputs['user_list'];
        $fileAttach = '';
        $urlAttach = '';
        if (Request::hasFile('attach')) {
            $file = Request::file('attach');
            $fileName = $file->getClientOriginalName();

            $Folder = md5(mt_rand());
            $createFolder = base_path() . env('PUBLIC_PATH_URL', 'public') . '/attach/user-mail/' . $Folder . '/';
            mkdir($createFolder, 0755);
            $file->move($createFolder, $fileName);
            $fileAttach = $createFolder . $fileName;
            $urlAttach = 'attach/user-mail/' . $Folder . '/' . $fileName;
        }


        $subject = $inputs['subject'];
        $txt = $inputs['message'];

        $users = User::whereIn('id', $ids)->get();

        foreach ($users as $key => $user) {
            Mail::send('emails.users', ['subject' => $subject, 'txt' => $txt], function ($message) use ($fileAttach, $user, $subject) {
                $message->to($user->email, $user->lastname)->subject($subject);
                if ($fileAttach != '')
                    $message->attach($fileAttach);
            });

            $email = new Email();
            $email->user_id = $user->id;
            $email->subject = $subject;
            $email->media_id = 0;
            $email->url_attach = isset($urlAttach) ? $urlAttach : '';
            $email->message = $txt;
            $email->save();
        }


        return Redirect::back();


    }

}
