<?php namespace App\Http\Controllers\backend;

use App\BaseModel;
use App\Advertisement;
use App\Language;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Laracasts\Flash\Flash;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Request;

class AdvertisementsController extends BackendController
{

    public $advertisement;
    public $locale;
    public $langActive;
    public $language;
    public $listLang;

    public function __construct(Advertisement $advertisement, Language $language)
    {
        $this->advertisement = $advertisement;
        $this->listLang = Config::get('app.locales');
        $this->langActive = app('getLocale');
        $this->language = $language;
        $this->locale = app('locale') == '' ? '' : app('locale') . '.';
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $advertisements = $this->advertisement
            ->join('advertisements_lang', 'advertisements_lang.advertisement_id', '=', 'advertisements.id')
            ->where('advertisements_lang.lang_id', '=', app('activeLangDetails')['lang_id'])
            ->whereIn('advertisements.type', ['image', 'flash'])
            ->where('advertisements.status', 1)
            ->get();

        return view('backend.advertisement.index', [
            'advertisements' => $advertisements,
            'locale' => $this->locale,
            'type' => 'advertisement'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.advertisement.form', [
            'locale' => $this->locale,
            'listLang' => $this->listLang,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = Input::all();

        $input_advertisement['url'] = $input['url'];
        $input_advertisement['status'] = 1;
        $input_advertisement['type'] = $input['type'];
        $input_advertisement['group_show'] = $input['group_show'];
        $input_advertisement['slider_option'] = $input['slider_option'];
        $position = $input_advertisement['position'] = $input['position'];
        $extension = '';
        $validFile = false;
        if (Request::hasFile('file_path')) {
            $extension = Input::file('file_path')->getClientOriginalExtension();
            $validFile = true;

        }
        $input_advertisement['extension'] = $extension;

        if (!$this->advertisement->fill($input_advertisement)->isValid()) {
            return redirect()->back()->withInput()->withErrors($this->advertisement->errors);
        }
        $this->advertisement->save();

        if ($id = $this->advertisement->id) {
            if ($validFile) {
                $file = Request::file('file_path');
                $fileName = $file->getClientOriginalName();

                $createFolder = base_path() . env('PUBLIC_PATH_URL', 'public') . '/uploads/images/image_advertisement/' . $id . '/';
                mkdir($createFolder, 0755);
                $file->move($createFolder, $fileName);
                $urlFile = '/uploads/images/image_advertisement/' . $id . '/' . $fileName;

                $updateContent = $this->advertisement->find($id);
                $updateContent->file_path = $urlFile;
                $updateContent->save();
                $messages[] = ['level' => BaseModel::level_success, 'message' => Lang::get('backend/Advertisement.messages.uploadFileSuccess')];
            }


            $params['table'] = 'advertisements_lang';
            foreach ($this->listLang as $key => $value) {
                $lang = $this->language->getLanguage($key);
                $body_content = json_encode($input['body_content']);
                $params['items'] = [
                    'advertisement_id' => $id
                    , 'title' => $input['title']
                    , 'short_description' => $input['short_description']
                    , 'description' => $input['description']
                    , 'body_content' => $body_content
                    , 'lang_id' => $lang['lang_id']
                ];
                $result = $this->advertisement->saveItemLang($params);
            }

            Flash::success(Lang::get('backend/Advertisement.messages.successInsert'));
            return Redirect::route($this->locale . 'backend.advert.edit', array('advertisements_id' => $id))->with('locale', $this->locale);
        }

        Flash::error(Lang::get('backend/Advertisement.messages.errCreate'));
        return Redirect::back()->withErrors(Lang::get('backend/Advertisement.messages.errCreate'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $advertisement = $this->advertisement->where('id', $id)
            ->join('advertisements_lang', 'id', '=', 'advertisement_id')
            ->where('advertisements_lang.lang_id', '=', app('activeLangDetails')['lang_id'])
            ->first();
        return view('backend.advertisement.form', [
            'advertisement' => $advertisement,
            'locale' => $this->locale,
            'listLang' => $this->listLang,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $input = Input::all();
        $lang = $this->language->getLanguage($input['lang']);
        $params['lang_id'] = $lang['lang_id'];

        $updateContent = $this->advertisement->find($id);
        $updateContent->url = $input['url'];
        $updateContent->type = $input['type'];
        $updateContent->group_show = $input['group_show'];
        $updateContent->position = $input['position'];
        $updateContent->slider_option = $input['slider_option'];

        $extension = $updateContent->extension;
        $validFile = false;
        if (Request::hasFile('file_path')) {
            $extension = Input::file('file_path')->getClientOriginalExtension();
            $validFile = true;
        }
        $updateContent->extension = $extension;

        $params['id'] = $id;
        $params['table'] = 'advertisements_lang';
        $params['name_id_field'] = 'advertisement_id';

        $body_content = json_encode($input['body_content']);
        $params['items'] = [
            'title' => $input['title']
            , 'short_description' => $input['short_description']
            , 'description' => $input['description']
            , 'body_content' => $body_content
        ];
        $result = $this->advertisement->updateItemLang($params);
        if (!$result) {
            $messages[] = ['level' => BaseModel::level_warning, 'message' => Lang::get('backend/FeatureGroup.messages.msgUpdateNotOkWarningLang')];
        }

        if ($validFile) {

            $file = Request::file('file_path');
            $fileName = $file->getClientOriginalName();

            $createFolder = base_path() . env('PUBLIC_PATH_URL', 'public') . '/uploads/images/image_advertisement/' . $id . '/';
            if (!file_exists($createFolder)) {
                mkdir($createFolder, 0755);
            }
            $file->move($createFolder, $fileName);
            $urlFile = '/uploads/images/image_advertisement/' . $id . '/' . $fileName;

            $updateContent->file_path = $urlFile;
            $messages[] = ['level' => BaseModel::level_success, 'message' => Lang::get('backend/Advertisement.messages.uploadFileSuccess')];
        }

        $updateContent->save();

        $messages[] = ['level' => BaseModel::level_success, 'message' => Lang::get('backend/Content.messages.successUpdate')];
        Session::flash('messages', $messages);

        return Redirect::back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $advertisement = $this->advertisement->find($id);
        $advertisement->status = 0;
        $advertisement->save();

        Flash::success(trans('backend/Advertisement.messages.successDelete'));
        return Redirect::back();
    }

}
