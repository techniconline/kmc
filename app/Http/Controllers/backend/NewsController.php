<?php namespace App\Http\Controllers\backend;

use App\Category;
use App\Content;
use App\ContentLang;
use App\Language;
use App\Providers\Helpers\Category\CategoryHelper;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\BaseModel;
use Laracasts\Flash\Flash;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Request;

class NewsController extends BackendController
{

    public $content;
    public $category;

    public $locale;
    public $langActive;
    public $language;
    public $listLang;

    public function __construct(Category $category, Content $content, Language $language)
    {
        $this->content = $content;
        $this->category = $category;

        $this->listLang = Config::get('app.locales');
        $this->langActive = app('getLocale');
        $this->language = $language;
        $this->locale = app('locale') == '' ? '' : app('locale') . '.';
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $contents = $this->content
            ->join('contents_lang', 'contents_id', '=', 'contents.id')
            ->join('employee', 'contents.author_id', '=', 'employee.id')
            ->where('contents_lang.lang_id', '=', app('activeLangDetails')['lang_id'])
            ->where('type', 'news')
//            ->where('contents.status', 1)
            ->get();
        return view('backend.content.index', [
            'contents' => $contents,
            'locale' => $this->locale,
            'type' => 'news'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

        $results = $this->category->getDataCategory(3);
        $roots = $results['roots'];
        $translatedName = $results['translatedLang'];

        return view('backend.content.form', [
            'locale' => $this->locale,
            'listLang' => $this->listLang,
            'type' => 'news'
        ])->with('roots', $roots)
            ->with('translatedName', $translatedName);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = Input::all();

        $URL = isset($input['url']) && $input['url'] ? str_replace(" ", "-", $input['url']) : CategoryHelper::createAlias($input['link_title']);
        $input['url'] = $input_content['url'] = $URL;

        $input['author_id'] = $input_content['employee_id'] = $input_content['author_id'] = Auth::employee()->get()->id ? Auth::employee()->get()->id : 0;

        $input_content['status'] = $input['status'];
        $input['type'] = $input_content['type'] = 'news';
        $input_content['login'] = $input['login'];
        $input['category_id'] = isset($input['categories']) ? $input['categories'][0] : null;

        $fill = $this->content->fill($input_content);
        $valid = $fill->validationData($input, $this->content->rules);
        if (!$valid) {
            return redirect()->back()->withInput()->withErrors($this->content->errors);
        }
        $this->content->save();

        if ($id = $this->content->id) {

            $resCat = $this->content->saveContentCategories($id, $input['categories']);

            if (Request::hasFile('image_content')) {
                $createFolder = base_path() . env('PUBLIC_PATH_URL', 'public') . '/uploads/images/image_content/news/' . $id . '/';
                if (!file_exists($createFolder)) {
                    mkdir($createFolder, 0755);
                }
                if (File::exists(base_path() . env('PUBLIC_PATH_URL', 'public') . '/uploads/images/image_content/news/' . $id . '/' . $id . '-160x80.jpg')) {
                    File::delete($id . '-160x80.jpg');
                }
                Image::make(Input::file('image_content'))->fit(160, 80)->save('uploads/images/image_content/news/' . $id . '/' . $id . '-160x80.jpg');
                Image::make(Input::file('image_content'))->fit(600, 400)->save('uploads/images/image_content/news/' . $id . '/' . $id . '-600x400.jpg');
                $updateContent = $this->content->find($id);
                $updateContent->image_content = '/uploads/images/image_content/news/' . $id . '/';
                $updateContent->save();
                $messages[] = ['level' => BaseModel::level_success, 'message' => Lang::get('backend/Content.messages.uploadFileSuccess')];
            }

            $input['contents_id'] = $id;
            foreach ($this->listLang as $key => $value) {
                $lang = $this->language->getLanguage($key);
                $input['lang_id'] = $lang['lang_id'];
                $input['title_tags'] = CategoryHelper::createTags($input['link_title']);
                $input['tags_upd'] = array_unique(explode(",", $input['title_tags'] . ',' . $input['tags']));
                $input['tags'] = implode(",", $input['tags_upd']);

                $content_lang = new ContentLang();
                if (!$content_lang->fill($input)->isValid()) {
                    return redirect()->back()->withInput()->withErrors($content_lang->errors);
                }
                $content_lang->save();

            }
            $messages[] = ['level' => BaseModel::level_success, 'message' => Lang::get('backend/Content.messages.successInsert')];
            Session::flash('messages', $messages);
            return Redirect::route($this->locale . 'backend.news.index')->with('locale', $this->locale);
        }
        return Redirect::back()->withErrors(Lang::get('backend/Content.messages.errCreate'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {

        $content = $this->content->where('id', $id)
            ->join('contents_lang', 'id', '=', 'contents_id')
            ->where('contents_lang.lang_id', '=', app('activeLangDetails')['lang_id'])->first();

        $results = $this->category->getDataCategory(3);
        $roots = $results['roots'];
        $translatedName = $results['translatedLang'];
        $categoriesContent = $this->content->getContentCategories($id, true);

        return view('backend.content.form', [
            'content' => $content,
            'categoriesContent' => $categoriesContent,
            'locale' => $this->locale,
            'listLang' => $this->listLang,
            'type' => 'news'
        ])->with('roots', $roots)
            ->with('translatedName', $translatedName);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $input = Input::all();
        $lang = $this->language->getLanguage($input['lang']);
        $input['lang_id'] = $lang['lang_id'];

        $URL = isset($input['url']) && $input['url'] ? str_replace(" ", "-", $input['url']) : CategoryHelper::createAlias($input['link_title']);
        $input['url'] = $URL;
        $input['title_tags'] = CategoryHelper::createTags($input['link_title']);
        $updateContent = $this->content->find($id);
        $updateContent->url = $input['url'];
        $updateContent->login = $input['login'];
        $input['tags_upd'] = array_unique(explode(",", $input['title_tags'] . ',' . $input['tags']));

        $paramsForTranslate = [
            'contents_id' => $id,
            'lang_id' => $input['lang_id'],
            'link_title' => $input['link_title'],
            'browser_title' => $input['browser_title'],
            'body_title' => $input['body_title'],
            'tags' => implode(",", $input['tags_upd']),
            'short_content' => $input['short_content'],
            'body_content' => $input['body_content']

        ];
        ContentLang::updateLang($paramsForTranslate);
        $resCat = $this->content->saveContentCategories($id, $input['categories']);

        if (Request::hasFile('image_content')) {
            $createFolder = base_path() . env('PUBLIC_PATH_URL', 'public') . '/uploads/images/image_content/news/' . $id . '/';
            if (!file_exists($createFolder)) {
                mkdir($createFolder, 0755);
            }
            if (File::exists(base_path() . env('PUBLIC_PATH_URL', 'public') . '/uploads/images/image_content/news/' . $id . '/' . $id . '-160x80.jpg')) {
                File::delete($id . '-160x80.jpg');
            }
            if (File::exists(base_path() . env('PUBLIC_PATH_URL', 'public') . '/uploads/images/image_content/news/' . $id . '/' . $id . '-600x400.jpg')) {
                File::delete($id . '-600x400.jpg');
            }
            Image::make(Input::file('image_content'))->fit(160, 80)->save('uploads/images/image_content/news/' . $id . '/' . $id . '-160x80.jpg');
            Image::make(Input::file('image_content'))->fit(600, 400)->save('uploads/images/image_content/news/' . $id . '/' . $id . '-600x400.jpg');
            $updateContent = $this->content->find($id);
            $updateContent->image_content = '/uploads/images/image_content/news/' . $id . '/';
            $messages[] = ['level' => BaseModel::level_success, 'message' => Lang::get('backend/Content.messages.uploadFileSuccess')];
        }

        $updateContent->save();

        $messages[] = ['level' => BaseModel::level_success, 'message' => Lang::get('backend/Content.messages.successUpdate')];
        Session::flash('messages', $messages);

        return Redirect::back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $content = $this->content->find($id);

        $content->delete();
        DB::table('contents_lang')->where('contents_id', $id)->delete();
        DB::table('content_categories')->where('content_id', $id)->delete();
        DB::table('comments')->where('item_id', $id)->delete();
        DB::table('comments_media')->where('comment_id', $id)->delete();
        Flash::success(trans('backend/Content.messages.successDelete'));
        return Redirect::back();
    }

}
