<?php
namespace App\Http\Controllers\backend;

use App\RoomCategory;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use App\Room;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use App\Language;
use App\MailModel;
use App\AttachType;

class RoomController extends BackendController
{

    public $room;
    public $attach;
    public $locale;
    public $langActive;
    public $language;
    public $listLang;

    public function __construct(Room $room, Language $language, AttachType $attach)
    {
        $this->listLang = Config::get('app.locales');
        $this->langActive = app('getLocale');
        $this->language = $language;
        $this->locale = app('locale') == '' ? '' : app('locale') . '.';

        $this->room = $room;
        $this->attach = $attach;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $categoryModel = new RoomCategory();
        $data = $categoryModel->getApartmentRoomCategoryMap();
        return view('backend.room.index')->with('data', $data)->with('locale', $this->locale);
    }


    public function listRoomRented()
    {
        $inputs = Input::all();
        $inputs['showActionRoomRented'] = true;

        $pageData = $this->listRoomRentedRender($inputs);
        if (!$pageData['action']) {
            $pageData = '';
        }

        if ((isset($inputs['page']) && $inputs['page']) || (isset($inputs['search']) && $inputs['search'])) {
            $pageDataTable = $pageData['pageData'];
            return ['action' => $pageData['action'], 'pageData' => $pageDataTable, 'message' => $pageData['message']];
        } else {
            return view('backend.room.listRoom')
                ->with('pageData', $pageData['pageData'])
                ->with('locale', $this->locale);
        }
    }

    public function listRoomRentedRender($inputs)
    {
        $lang = $this->language->getLanguage($this->langActive);
        $params['lang_id'] = $lang['lang_id'];
        $params['paginateNum'] = 10;

        $showActionRoomRented = isset($inputs['showActionRoomRented']) ? $inputs['showActionRoomRented'] : false;

        $params['date_from'] = isset($inputs['date_from']) ? $inputs['date_from'] : false;
        $params['date_to'] = isset($inputs['date_to']) ? $inputs['date_to'] : false;
        $params['room_number'] = isset($inputs['room_number']) ? $inputs['room_number'] : false;
        $params['type'] = isset($inputs['type']) ? $inputs['type'] : false;

        $params['sort'] = isset($inputs['sort']) ? $inputs['sort'] : false;

        $params['first_name'] = isset($inputs['first_name']) ? $inputs['first_name'] : false;
        $params['last_name'] = isset($inputs['last_name']) ? $inputs['last_name'] : false;

        $params['gender'] = isset($inputs['gender']) ? $inputs['gender'] : false;
        $params['rent_status'] = isset($inputs['rent_status']) ? $inputs['rent_status'] : false;

        $dataListRoomRented = $this->room->getListRoomsRented($params);

        $pageData = view('backend.room.listRoomRender')
            ->with('dataRoomRented', $dataListRoomRented)
            ->with('showActionRoomRented', $showActionRoomRented)
            ->with('locale', $this->locale)->render();

        if (!$dataListRoomRented)
            return ['action' => false, 'pageData' => $pageData, 'message' => Lang::get('backend/AttachType.messages.errValid')];

        return ['action' => true, 'pageData' => $pageData, 'message' => ''];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $apartment_room_category_map_id
     * @return Response
     */
    public function edit($apartment_room_category_map_id)
    {
        $lang = $this->language->getLanguage($this->langActive);
        $data = $this->room
            ->where('room_status', '!=', 0)
            ->where('status', '=', 1)
            ->where('apartment_room_category_map_id', '=', $apartment_room_category_map_id)
            ->get();

        $base_storage = URL::to('');

        foreach ($data as &$item) {

            $item->dataRent = null;
            if ($item->room_status > 1) {
                $dataRent = $item->dataRent = $this->room->getRentRoomData($item->id, $lang['lang_id']);
            }

            $dataPic = $item->pictures = $this->room->getRoomPicturesList($item->id);
            foreach ($dataPic as &$itemP) {
                $itemP->linkMedia = $base_storage . '/' . $itemP->src;
            }
        }


        $categoryModel = new RoomCategory();
        $data_category = $categoryModel->getApartmentRoomCategoryMap($apartment_room_category_map_id)[0];
        return view('backend.room.create')->with('data', $data)->with('data_category', $data_category)->with('locale', $this->locale);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int $apartment_room_category_map_id
     * @return Response
     */
    public function update($apartment_room_category_map_id)
    {
        $input = Input::all();
        $input['user_id'] = Auth::employee()->get()->id;
        $input['apartment_room_category_map_id'] = $apartment_room_category_map_id;
        $fill = $this->room->fill($input);
        $isValid = $fill->isValid();
        if (!$isValid) {
            $return = Redirect::back()->withInput()->withErrors($this->room->errors);
            return $return;
        }

        $result = $this->room->updateRooms($apartment_room_category_map_id, $input);

        return Redirect::back()->withResult($result)->withErrors($result['message']);

    }

    public function savePictures($room_id)
    {
        $input = Input::all();
        $input['user_id'] = Auth::employee()->get()->id;
        $input['room_id'] = $room_id;
        $items = $input['items'];

        $result = $this->room->savePictures($room_id, $items);

        if (!$result)
            return ['action' => false, 'message' => Lang::get('backend/Room.messages.msgInsertNotOk')];

        return $result;

    }


    public function delPictures($room_id, $media_id)
    {

        $result = $this->room->delRoomPic($room_id, $media_id);

        return $result;

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $apartment_room_category_map_id
     * @return Response
     */
    public function destroy($apartment_room_category_map_id)
    {
        $result = $this->room->delRooms($apartment_room_category_map_id);
        if ($result['action']) {
            $result['genLink'] = URL::route('backend.room.generateRooms', ['apartment_room_category_map_id' => $apartment_room_category_map_id]);
        }

        return $result;
    }


    /**
     * @param $apartment_room_category_map_id
     * @return array
     */
    public function generateRooms($apartment_room_category_map_id)
    {
        $categoryModel = new RoomCategory();
        $data = $categoryModel->getApartmentRoomCategoryMap($apartment_room_category_map_id)[0];
        $result = $this->room->generateRooms($data->room_number, $data);
        if ($result['action']) {
            $result['editLink'] = URL::route($this->locale . 'backend.room.edit', ['apartment_room_category_map_id' => $apartment_room_category_map_id]);
            $result['delLink'] = URL::route($this->locale . 'backend.room.destroy', ['apartment_room_category_map_id' => $apartment_room_category_map_id]);
        }
        return $result;
    }

    public function reservationRoom($room_id, $booking_id, $user_id)
    {
        $inputs = Input::all();
        $email = isset($inputs['email']) ? $inputs['email'] : '';
        $username = isset($inputs['username']) ? $inputs['username'] : '';

        if (!$email || !$username || !$user_id || !$booking_id || !$room_id)
            return ['action' => false, 'message' => Lang::get('backend/Room.messages.errValid')];

        $params = [];
        $params['assessor_user_id'] = $assessor_user_id = Auth::employee()->get()->id;
        $params['booking_id'] = $booking_id;
        $params['user_id'] = $user_id;
        $params['room_id'] = $room_id;
        $params['room_status'] = $room_status = 2; //for reservation for user

        $result = $this->room->addReservationRoom($params);

        if (!$result)
            return ['action' => false, 'message' => Lang::get('backend/Room.messages.errValid')];

        if ($result['action']) {
            $type = $result['type'];

            $data = [];
            $data['username'] = isset($inputs['username']) ? $inputs['username'] : '';
            $data['email'] = isset($inputs['email']) ? $inputs['email'] : '';
            if ($type == 'parking') {
                $data['subject'] = Lang::get('backend/Room.messages.msgMailSubjectForReservationParking');
                $data['text'] = Lang::get('backend/Room.messages.msgMailTextForReservationParking');
            } else {
                $data['subject'] = Lang::get('backend/Room.messages.msgMailSubjectForReservation');
                $data['text'] = Lang::get('backend/Room.messages.msgMailTextForReservation');
            }

            $data['page'] = 'emails.action-rent';

//            $params['user_id'] = $data['user_id'];
//            $params['booking_id'] = $data['booking_id'];
//            $params['assessor_user_id'] = $this->user_id;
            $params['subject'] = $data['subject'];
            $params['feedback_message'] = $data['text'];
            $params['status'] = 6; //for reservation for user

            $resultMsg = $this->attach->addFeedBackForDocument($params);

            if (!$data['email'] || !$data['text'])
                return false;

            $mailModel = new MailModel();
            $resultMail = $mailModel->sendMail($data);
            if (!$resultMail) {
                return ['action' => false, 'result' => $resultMail, 'message' => Lang::get('backend/AttachType.messages.SendMailNotOk')];
            }
        }


        return $result;

    }

    public function rentRoom($room_id, $booking_id, $user_id)
    {
        $inputs = Input::all();
        $email = isset($inputs['email']) ? $inputs['email'] : '';
        $username = isset($inputs['username']) ? $inputs['username'] : '';

        if (!$email || !$username || !$user_id || !$booking_id || !$room_id)
            return ['action' => false, 'message' => Lang::get('backend/Room.messages.errValid')];

        $params = [];
        $params['assessor_user_id'] = $assessor_user_id = Auth::employee()->get()->id;
        $params['booking_id'] = $booking_id;
        $params['user_id'] = $user_id;
        $params['room_id'] = $room_id;
        $params['room_status'] = $room_status = 3; //for rent

        $result = $this->room->addRentRoom($params);

        if (!$result)
            return ['action' => false, 'message' => Lang::get('backend/Room.messages.errValid')];

        if ($result['action']) {
            $type = $result['type'];

            $data = [];
            $data['username'] = isset($inputs['username']) ? $inputs['username'] : '';
            $data['email'] = isset($inputs['email']) ? $inputs['email'] : '';
            $data['page'] = 'emails.action-rent';

            if ($type == 'parking') {
                $data['subject'] = Lang::get('backend/Room.messages.msgMailSubjectForRentParking');
                $data['text'] = Lang::get('backend/Room.messages.msgMailTextForRentParking');
            } else {
                $data['subject'] = Lang::get('backend/Room.messages.msgMailSubjectForRent');
                $data['text'] = Lang::get('backend/Room.messages.msgMailTextForRent');
            }

//            $params['user_id'] = $data['user_id'];
//            $params['booking_id'] = $data['booking_id'];
//            $params['assessor_user_id'] = $this->user_id;
            $params['subject'] = $data['subject'];
            $params['feedback_message'] = $data['text'];
            $params['status'] = 7; //for rent

            $resultMsg = $this->attach->addFeedBackForDocument($params);

            if (!$data['email'] || !$data['text'])
                return false;

            $mailModel = new MailModel();
            $resultMail = $mailModel->sendMail($data);
            if (!$resultMail) {
                return ['action' => false, 'result' => $resultMail, 'message' => Lang::get('backend/AttachType.messages.SendMailNotOk')];
            }
        }
        return $result;

    }

    public function cancelRentRoom($rent_id, $user_id)
    {
        $inputs = Input::all();
        $email = isset($inputs['email']) ? $inputs['email'] : '';
        $username = isset($inputs['username']) ? $inputs['username'] : '';

        if (!$email || !$username || !$user_id || !$rent_id)
            return ['action' => false, 'message' => Lang::get('backend/Room.messages.errValid')];

        $params = [];
        $params['assessor_user_id'] = $assessor_user_id = Auth::employee()->get()->id;
        $params['rent_id'] = $rent_id;
        $params['user_id'] = $user_id;
        $params['room_status'] = $room_status = 0; //for cancel
        $params['cancelBooking'] = isset($inputs['cancelBooking']) ? $inputs['cancelBooking'] : ''; //cancel Booking

        $result = $this->room->cancelRentRoom($params);

        if (!$result)
            return ['action' => false, 'message' => Lang::get('backend/Room.messages.errValid')];

        if ($result['action']) {
            $data = [];
            $data['username'] = isset($inputs['username']) ? $inputs['username'] : '';
            $data['email'] = isset($inputs['email']) ? $inputs['email'] : '';
            $data['subject'] = Lang::get('backend/Room.messages.msgMailSubjectForCancelRent');
            $data['text'] = Lang::get('backend/Room.messages.msgMailTextForCancelRent');
            $data['page'] = 'emails.action-rent';

            $params['subject'] = $data['subject'];
            $params['feedback_message'] = $data['text'];
            $params['status'] = 8; //for cancel

            $resultMsg = $this->attach->addFeedBackForDocument($params);

            if (!$data['email'] || !$data['text'])
                return false;

            $mailModel = new MailModel();
            $resultMail = $mailModel->sendMail($data);
            if (!$resultMail) {
                return ['action' => false, 'result' => $resultMail, 'message' => Lang::get('backend/AttachType.messages.SendMailNotOk')];
            }
        }

        return $result;

    }

}
