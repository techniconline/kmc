<?php namespace App\Http\Controllers\backend;

use App\Language;
use App\Link;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Intervention\Image\Facades\Image;

class LinksController extends BackendController
{

    public $link;
    public $locale;
    public $langActive;
    public $language;
    public $listLang;

    public function __construct(Link $link, Language $language)
    {
        $this->link = $link;
        $this->listLang = Config::get('app.locales');
        $this->langActive = app('getLocale');
        $this->language = $language;
        $this->locale = app('locale') == '' ? '' : app('locale') . '.';
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $links = $this->link->all();
        return view('backend.links.index', [
            'links' => $links,
            'locale' => $this->locale
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.links.form', [
            'locale' => $this->locale
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = Input::all();
        $input['desc'] = 'NULL';
        if (!$this->link->fill($input)->isValid()) {
            return redirect()->back()->withInput()->withErrors($this->link->errors);
        }
        $this->link->save();

        return $this->index();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $link = $this->link->find($id);
        return view('backend.links.form', [
            'link' => $link,
            'locale' => $this->locale
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $input = Input::all();
        $link = $this->link->find($id);

        if (!$link->fill($input)->isValid()) {
            return redirect()->back()->withInput()->withErrors($link->errors);
        }

        $link->save();
        return Redirect::back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

}
