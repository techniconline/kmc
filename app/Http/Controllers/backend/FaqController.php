<?php namespace App\Http\Controllers\backend;

use App\Faq;
use App\FaqLang;
use App\Language;
use App\Link;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Intervention\Image\Facades\Image;

class FaqController extends BackendController
{

    public $faq;
    public $locale;
    public $langActive;
    public $language;
    public $listLang;

    public function __construct(Faq $faq, Language $language)
    {
        $this->faq = $faq;
        $this->listLang = Config::get('app.locales');
        $this->langActive = app('getLocale');
        $this->language = $language;
        $this->locale = app('locale') == '' ? '' : app('locale') . '.';
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $faqs = $this->faq
            ->join('faq_lang', 'faq_id', '=', 'faq.id')
            ->where('faq_lang.lang_id', '=', app('activeLangDetails')['lang_id'])
            ->orderBy('type')
            ->get();
        return view('backend.faq.index', [
            'faqs' => $faqs,
            'locale' => $this->locale
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.faq.form', [
            'locale' => $this->locale,
            'listLang' => $this->listLang
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = Input::all();

        $fill = $this->faq->fill($input);
        $this->faq->save();

        foreach ($this->listLang as $key => $value) {
            $lang = $this->language->getLanguage($key);

            $input['faq_id'] = $this->faq->id;
            $input['lang_id'] = $lang['lang_id'];

            $faqLang = new FaqLang();
            if (!$faqLang->fill($input)->isValid()) {
                return redirect()->back()->withInput()->withErrors($faqLang->errors);
            }
            $faqLang->save();
        }

        return $this->index();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $faq = $this->faq->selectJoinLang(['id' => $id])->first();
        return view('backend.faq.form', [
            'faq' => $faq,
            'locale' => $this->locale,
            'listLang' => $this->listLang
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $input = Input::all();
        $lang = $this->language->getLanguage($input['lang']);
        $input['lang_id'] = $lang['lang_id'];

        $faqData = $this->faq->find($id);
        $faqData->type = $input['type'];
        $faqData->save();

        $paramsForTranslate = [
            'faq_id' => $id,
            'lang_id' => $input['lang_id'],
            'question' => $input['question'],
            'answer' => $input['answer']
        ];
        FaqLang::updateLang($paramsForTranslate);

        return Redirect::back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->faq->find($id)->delete();
        FaqLang::where('faq_id', $id)->delete();
        return Redirect::back();
    }

}
