<?php namespace App\Http\Controllers\backend;

use App\ActionsList;
use Illuminate\Support\Facades\App;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Lang;
use App\Providers\Helpers\Category\CategoryHelper;
use App\Language;
use App\MailModel;
use Laracasts\Flash\Flash;

class ActionListController extends BackendController
{

    public $actionList;
    public $locale;
    public $langActive;
    public $language;
    public $listLang;
    public $user_id;

    public function __construct(Language $language, ActionsList $actionList)
    {
        $this->listLang = Config::get('app.locales');
        $this->actionList = $actionList;
        $this->langActive = app('getLocale');
        $this->language = $language;
        $this->locale = app('locale') == '' ? '' : app('locale') . '.';
        $this->user_id = isset(Auth::employee()->get()->id) ? Auth::employee()->get()->id : 0;
    }

    public function index()
    {
        $inputs = Input::all();

        $pageData = $this->listRender($inputs);

        if ((isset($inputs['page']) && $inputs['page']) || (isset($inputs['search']) && $inputs['search']) || Request::ajax()) {
            $pageDataTable = $pageData['pageData'];
            return ['action' => $pageData['action'], 'pageData' => $pageDataTable, 'message' => $pageData['message']];
        } else {
            return view('backend.actionList.index')
                ->with('pageData', $pageData['pageData'])
                ->with('locale', $this->locale);
        }
    }

    public function listRender($inputs)
    {


        $inputs['paginateNum'] = isset($inputs['paginateNum']) && $inputs['paginateNum'] ? $inputs['paginateNum'] : 20;

        $data = $this->actionList->getActionList($inputs);

        $pageData = view('backend.actionList.listRender')
            ->with('data', $data)
            ->with('locale', $this->locale)->render();

        if (!$data)
            return ['action' => false, 'pageData' => $pageData, 'message' => Lang::get('backend/ActionList.messages.errValid')];

        return ['action' => true, 'pageData' => $pageData, 'message' => ''];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

        return view('backend.actionList.form')
            ->with('title', Lang::get('backend/ActionList.view.form.TitleSave'))
            ->with('btnTitle', Lang::get('backend/ActionList.view.form.Save'))
            ->with('locale', $this->locale);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = Input::all();

        $fill = $this->actionList->fill($input);
        $isValid = $fill->isValid();

        if (!$isValid) {
            Flash::error(Lang::get('backend/ActionList.messages.errValid'));
            $return = Redirect::back()->withInput()->withErrors($this->accessGroup->errors);
            return $return;
        }

        $this->actionList->save();

        if ($id = $this->actionList->id) {
            Flash::success(Lang::get('backend/ActionList.messages.added'));
            return Redirect::route($this->locale . 'backend.actionList.edit', array('id' => $id));
        }
        Flash::error(Lang::get('backend/ActionList.messages.errCreate'));
        return Redirect::back()->withErrors(Lang::get('backend/ActionList.messages.errCreate'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {

        $data = $this->actionList->find($id);

        return view('backend.actionList.form', [
            'data' => $data
        ])
            ->with('title', Lang::get('backend/ActionList.view.form.TitleSave'))
            ->with('btnTitle', Lang::get('backend/ActionList.view.form.Save'))
            ->with('locale', $this->locale);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $input = Input::all();
        $data = $this->actionList->find($id);

        $fill = $data->fill($input);

        $isValid = $fill->isValid();

        if (!$isValid) {
            Flash::error(Lang::get('backend/ActionList.messages.errValid'));
            $return = Redirect::back()->withInput()->withErrors($data->errors);
            return $return;
        }

        if ($data->save()) {
            Flash::success(Lang::get('backend/ActionList.messages.msgUpdateOk'));
            return Redirect::route($this->locale . 'backend.actionList.edit', array('id' => $id));
        }
        Flash::error(Lang::get('backend/ActionList.messages.msgUpdateNotOk'));
        return Redirect::back()->withErrors(Lang::get('backend/ActionList.messages.msgUpdateNotOk'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $data = $this->actionList->find($id);
        $data->status = 0;
        if ($data->save()) {
            Flash::success(Lang::get('backend/ActionList.messages.msgDelOk'));
            return Redirect::back();
        }
        Flash::error(Lang::get('backend/ActionList.messages.msgDelNotOk'));
        return Redirect::back()->withErrors(Lang::get('backend/ActionList.messages.msgDelNotOk'));
    }

}
