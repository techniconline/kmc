<?php namespace App\Http\Controllers\backend;

use App\Employee;
use App\Language;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Intervention\Image\Facades\Image;

class ProfileController extends BackendController
{

    public $employee;
    public $locale;

    public function __construct(Employee $employee, Language $language)
    {
        $this->employee = $employee;
        $this->locale = app('locale') == '' ? '' : app('locale') . '.';
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $employee = $this->employee->find($id);
        return view('backend.profile.form', [
            'employee' => $employee,
            'locale' => $this->locale
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $input = Input::all();
        if ($input['currentPassword'] !== '') {
            if (!Hash::check($input['currentPassword'], Auth::employee()->get()->password)) {
                return Redirect::back()->withErrors(['currentPassword' => trans('backend/User.messages.Match_Password')]);
            } else {
                $input['password'] = Hash::make($input['newPassword']);
                unset($input['currentPassword']);
                unset($input['newPassword']);
            }
        } else {
            unset($input['currentPassword']);
            unset($input['newPassword']);
        }

        $employee = $this->employee->find($id);

        if (Request::hasFile('avatar')) {
            if (File::exists(public_path() . '/uploads/images/Employees/' . $employee->id . '-200x200.jpg')) {
                File::delete($employee->id . '-200x200.jpg');
            }
            Image::make(Input::file('avatar'))->fit(200, 200)->save('uploads/images/Employees/' . $employee->id . '-200x200.jpg');
        }

        $employee->fill($input);
        $employee->save();

        return Redirect::back();
    }

}
