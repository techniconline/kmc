<?php namespace App\Http\Controllers\backend;

use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Http\Requests;

abstract class BackendController extends BaseController
{

    use DispatchesCommands, ValidatesRequests;

}
