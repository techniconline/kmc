<?php
namespace App\Http\Controllers\backend;

use App\Apartment;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Media;
use Illuminate\Support\Facades\URL;
use Illuminate\View\View;
use App\RoomCategory;
use stdClass;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\App;
use App\Language;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class RoomCategoryController extends BackendController
{

    public $roomCategory;
    public $locale;
    public $langActive;
    public $language;
    public $listLang;

    public function __construct(RoomCategory $roomCategory, Language $language)
    {
        $this->roomCategory = $roomCategory;
        $this->listLang = Config::get('app.locales');
        $this->langActive = app('getLocale');
        $this->language = $language;
        $this->locale = app('locale') == '' ? '' : app('locale') . '.';
    }

    /**
     * @return mixed
     */
    public function index()
    {
        $data = $this->roomCategory->getListOfRoomCategory();
        return view('backend.roomcategory.index')->with('data', $data)->with('locale', $this->locale);
    }


    /**
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('backend.roomcategory.create', [
            'btnTitle' => Lang::get('backend/roomCategory.controller.Insert_Room_Category')
            , 'error' => ''
            , 'title' => Lang::get('backend/roomCategory.controller.Create_Room_Category')])
            ->with('locale', $this->locale)->with('listLang', $this->listLang)
            ->with('setLang', app('getLocaleDefault'));
    }


    /**
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $input = Input::all();
        $input['user_id'] = Auth::employee()->get()->id;
        $input['status'] = 1;
        $fill = $this->roomCategory->fill($input);
        $isValid = $fill->isValid();
        if (!$isValid) {
            $return = Redirect::back()->withInput()->withErrors($this->roomCategory->errors);
            return $return;
        }
        $this->roomCategory->save();

        if ($id = $this->roomCategory->id) {
            $input['id'] = $id;
            foreach ($this->listLang as $key => $value) {
                $lang = $this->language->getLanguage($key);
                $input['lang_id'] = $lang['lang_id'];
                $result = $this->roomCategory->saveRoomCategoryLang($input);
            }
            return Redirect::route($this->locale . 'backend.roomCategory.edit', array('room_category_id' => $id));
        }
        return Redirect::back()->withErrors(Lang::get('backend/roomCategory.messages.errCreate'));

    }


    /**
     * @param $room_category_id
     * @return \Illuminate\View\View
     */
    public function show($room_category_id)
    {
//        $data = $this->roomCategory->where('status', 1)->find($room_category_id);
//        return view('backend.roomCategory.show', ['data' => $data])->with('locale',$this->locale);
    }


    /**
     * @param $room_category_id
     * @return \Illuminate\View\View
     */
    public function edit($room_category_id)
    {
        $lang = $this->language->getLanguage($this->langActive);
        $data = $this->roomCategory
            ->join('room_category_lang', 'room_category_lang.id', '=', 'room_category.id')
            ->where('room_category.id', '=', $room_category_id)
            ->where('room_category.status', '=', 1)
            ->where('room_category_lang.lang_id', '=', $lang['lang_id'])
            ->select(['room_category_lang.title', 'room_category_lang.short_description', 'room_category_lang.description', 'room_category.*'])
            ->get()->first();

        $errors = '';
        if (!$data)
            $errors[] = "This category is not valid!!!";

        $mediaModel = new Media();
        $images = $mediaModel->getMediaApartment('image', 0, $room_category_id);
        $videos = $mediaModel->getMediaApartment('video', 0, $room_category_id);
        return view('backend.roomcategory.create', [
            'data' => $data
            , 'images' => $images
            , 'videos' => $videos
            , 'title' => Lang::get('backend/roomCategory.controller.Edit_Room_Category')
            , 'btnTitle' => Lang::get('backend/roomCategory.controller.Update_Room_Category')
            , 'error' => $errors
        ])->with('locale', $this->locale)->with('listLang', $this->listLang)->with('setLang', $this->langActive);
    }


    /**
     * @param $room_category_id
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function update($room_category_id)
    {
        $input = Input::all();
        $input['user_id'] = Auth::employee()->get()->id;
        $lang = $this->language->getLanguage($input['lang']);
        $input['lang_id'] = $lang['lang_id'];
        $inputImage = Input::get('image');
        $inputImageTitle = Input::get('image_title');
        $inputVideo = Input::get('video');
        $inputVideoTitle = Input::get('video_title');
        $inputMediaDel = Input::get('media_del');

        $mediaArr = array(
            'images' => $inputImage,
            'images_title' => $inputImageTitle,
            'videos' => $inputVideo,
            'videos_title' => $inputVideoTitle
        );
        $result = $this->roomCategory->updateCategory($room_category_id, $input, $mediaArr, $inputMediaDel);
        if (!$result['action']) {
            $return = Redirect::back()->withInput()->withErrors($result['message']);
            return $return;
        }
        return Redirect::back()->with('message', $result['message']);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $room_category_id
     * @return Response
     */
    public function destroy($room_category_id)
    {
        $result = $this->roomCategory->del($room_category_id);
        //$resultLang = DB::table('room_category_lang')->where('id', '=', $room_category_id)->delete();
        return ($result);
    }


    /**
     * @param $room_category_id
     * @return $this
     */
    public function details($room_category_id)
    {
        $lang = $this->language->getLanguage($this->langActive);
        $apartmentModel = new Apartment();
        $dataApartment = $apartmentModel
            ->join('apartments_lang', 'apartments_lang.id', '=', 'apartments.id')
            ->where('apartments.status', '=', 1)
            ->where('apartments_lang.lang_id', '=', $lang['lang_id'])
            ->select(['apartments_lang.title', 'apartments.*'])
            ->lists('apartments_lang.title', 'apartments.id');

        $dataCategory = $this->roomCategory
            ->join('room_category_lang', 'room_category_lang.id', '=', 'room_category.id')
            ->where('room_category_lang.lang_id', '=', $lang['lang_id'])
            ->where('status', 1)->find($room_category_id);
        $data = $this->roomCategory->getCategoryDetails($room_category_id);
        return view('backend.roomcategory.details')->with(['dataApartment' => $dataApartment
            , 'dataCategory' => $dataCategory
            , 'dataDetails' => $data
            , 'error' => ''])->with('locale', $this->locale)->with('listLang', $this->listLang)->with('setLang', $this->langActive);
    }


    /**
     * @param $room_category_id
     * @param $apartment_room_category_map_id
     * @return $this
     */
    public function updateDetails($room_category_id, $apartment_room_category_map_id)
    {
        $input = Input::all();
        $input['room_category_id'] = $room_category_id;
        $result = $this->roomCategory->updateDetails($apartment_room_category_map_id, $input);
        if (!$result['action']) {
            $return = Redirect::back()->withInput()->withErrors($result['message']);
            return $return;
        }
        return Redirect::back()->with('message', $result['message']);
    }

    /**
     * @param $room_category_id
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function addDetails($room_category_id)
    {
        $input = Input::all();
        $input['room_category_id'] = $room_category_id;
        $input['status'] = 1;
        $fill = $this->roomCategory->fill($input);
        $isValid = $fill->isValidDetails();
        if (!$isValid) {
            $return = Redirect::back()->withInput()->withErrors($this->roomCategory->errors);
            return $return;
        }

        $data = new stdClass();
        $data->apartment_id = $input['apartment_id'];
        $data->room_category_id = $input['room_category_id'];
        $data->status = $input['status'];
        $data->room_number = $input['room_number'];
        $data->floor_number = $input['floor_number'];
        $id = $this->roomCategory->saveDetails($data);

        if ($id) {
            return Redirect::route('backend.roomCategory.details', array('room_category_id' => $room_category_id));
        }
        return Redirect::back()->withErrors(Lang::get('backend/roomCategory.messages.errCreate'));
    }

    public function updatePrice($room_category_id)
    {

        if (!$room_category_id)
            return ['valid' => false, 'message' => Lang::get('backend/roomCategory.messages.errNoCatId')];

        $input = Input::all();
        $result = $this->roomCategory->addPrice($room_category_id, $input['price'], $input['to_price']);
        return $result;

    }

    public function delDetails($apartment_room_category_map_id)
    {
        $result = $this->roomCategory->delDetails($apartment_room_category_map_id);
        return ($result);
    }

}
