<?php
namespace App\Http\Controllers\backend;

use Illuminate\Support\Facades\App;
use App\AttachType;
use App\AttachTypeGroup;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Lang;
use App\Providers\Helpers\Category\CategoryHelper;
use App\Language;
use App\MailModel;

class AttachTypeController extends BackendController
{

    public $attach;
    public $attachGroup;
    public $locale;
    public $langActive;
    public $language;
    public $listLang;
    public $user_id;

    public function __construct(AttachType $attach, AttachTypeGroup $attachGroup, Language $language)
    {
        $this->listLang = Config::get('app.locales');
        $this->attach = $attach;
        $this->attachGroup = $attachGroup;
        $this->langActive = app('getLocale');
        $this->language = $language;
        $this->locale = app('locale') == '' ? '' : app('locale') . '.';
        $this->user_id = isset(Auth::employee()->get()->id) ? Auth::employee()->get()->id : 0;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $lang = $this->language->getLanguage($this->langActive);
        $dataTypeGroup = $this->attachGroup
            ->join('attach_type_group_lang', 'attach_type_group_lang.id', '=', 'attach_type_group.id')
            ->where('attach_type_group.status', '=', 1)
            ->where('attach_type_group_lang.lang_id', '=', $lang['lang_id'])
            ->select(['attach_type_group_lang.title', 'attach_type_group_lang.description', 'attach_type_group.*'])
            ->get();
        $dataType = $this->attach->getAttachTypeList($lang['lang_id']);
        return view('backend.AttachType.index')->with('data', $dataType)->with('dataGroup', $dataTypeGroup)->with('locale', $this->locale);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $lang = $this->language->getLanguage($this->langActive);
        $dataTypeGroup = $this->attachGroup
            ->join('attach_type_group_lang', 'attach_type_group_lang.id', '=', 'attach_type_group.id')
            ->where('attach_type_group.status', '=', 1)
            ->where('attach_type_group_lang.lang_id', '=', $lang['lang_id'])
            ->select(['attach_type_group.id', 'attach_type_group_lang.title'])
            ->lists('title', 'id');
        return view('backend.AttachType.form', ['btnTitle' => Lang::get('backend/AttachType.controller.InsertAttachType')
            , 'error' => '', 'title' => Lang::get('backend/AttachType.controller.CreateAttachType')])->with('listTypeGroup', $dataTypeGroup)
            ->with('locale', $this->locale)->with('listLang', $this->listLang)->with('setLang', app('getLocaleDefault'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = Input::all();
//        $lang = $this->language->getLanguage($input['lang']);
//        $input['lang_id']=$lang['lang_id'];
        $input['user_id'] = $this->user_id;

        $input['status'] = 1;
        if (Input::get('alias') != NULL) {
            $alias = Input::get('alias');
        } else {
            $alias = CategoryHelper::createAlias(Input::get('title'));
            $input['alias'] = $alias;
        }

        $aliasDuplicate = $this->attach->where('alias', '=', $alias)->where('status', '=', 1)->get()->first();

        if ($aliasDuplicate) {
            return Redirect::back()->withErrors(Lang::get('backend/AttachType.messages.errDuplicateAlias'));
        }
        $fill = $this->attach->fill($input);
        $isValid = $fill->isValid();
        if (!$isValid) {
            $return = Redirect::back()->withInput()->withErrors($this->attach->errors);
            return $return;
        }
        $this->attach->save();

        if ($id = $this->attach->id) {
            $input['id'] = $id;
            foreach ($this->listLang as $key => $value) {
                $lang = $this->language->getLanguage($key);
                $input['lang_id'] = $lang['lang_id'];
                $result = $this->attach->saveAttachLang($input);
            }
            return Redirect::route($this->locale . 'backend.attachment.edit', array('attach_type' => $id))->with('locale', $this->locale);
        }
        return Redirect::back()->withErrors(Lang::get('backend/AttachType.messages.errCreate'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $lang = $this->language->getLanguage($this->langActive);
        $dataTypeGroup = $this->attachGroup
            ->join('attach_type_group_lang', 'attach_type_group_lang.id', '=', 'attach_type_group.id')
            ->where('attach_type_group.status', '=', 1)
            ->where('attach_type_group_lang.lang_id', '=', $lang['lang_id'])
            ->select(['attach_type_group.id', 'attach_type_group_lang.title'])
            ->lists('title', 'id');
        $data = $this->attach
            ->join('attach_type_lang', 'attach_type_lang.id', '=', 'attach_type.id')
            ->where('attach_type.status', '=', 1)
            ->where('attach_type.id', '=', $id)
            ->where('attach_type_lang.lang_id', '=', $lang['lang_id'])
            ->select(['attach_type_lang.title', 'attach_type_lang.description', 'attach_type.*'])
            ->get()->first();

        $errors = '';
        if (!$data)
            $errors[] = Lang::get('backend/AttachType.messages.errValid');

        return view('backend.AttachType.form', [
            'data' => $data
            , 'title' => Lang::get('backend/AttachType.controller.AttachTypeEdit')
            , 'btnTitle' => Lang::get('backend/AttachType.controller.UpdateAttachType')
            , 'error' => $errors
        ])->with('listTypeGroup', $dataTypeGroup)->with('locale', $this->locale)->with('listLang', $this->listLang)->with('setLang', $this->langActive);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $input = Input::all();
        $lang = $this->language->getLanguage($input['lang']);
        $input['lang_id'] = $lang['lang_id'];
        $input['user_id'] = $this->user_id;
        $input['status'] = 1;
        $input['id'] = $id;

        if (Input::get('alias') != NULL) {
            $alias = Input::get('alias');
        } else {
            $alias = CategoryHelper::createAlias(Input::get('title'));
            $input['alias'] = $alias;
        }
        $attach = $this->attach->find($id);
        $fill = $this->attach->fill($input);
        $isValid = $fill->isValid();
        if (!$isValid) {
            $return = Redirect::back()->withInput()->withErrors($this->attach->errors);
            return $return;
        }

        $input['title'];
        $attach->alias = $input['alias'];
        $input['description'];
        $attach->user_id = $input['user_id'];

        if ($attach->save() && $this->attach->updateAttachLang($input))
            return Redirect::back()->with('message', Lang::get('backend/AttachType.messages.msgUpdateOk'));

        return Redirect::back()->withInput()->withErrors($this->attach->errors);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $input = Input::all();
        $input['user_id'] = $this->user_id;
        $attach = $this->attach->find($id);
        $status = $attach->status = 0;
        $attach->user_id = $input['user_id'];

        if ($attach->save()) {
            return $result = ['action' => true, 'message' => Lang::get('backend/AttachType.messages.msgDelOk')];
        }

        return $result = ['action' => false, 'message' => Lang::get('backend/AttachType.messages.msgDelNotOk')];
    }

    public function activate($id)
    {
        $input = Input::all();
        $input['user_id'] = $this->user_id;
        $input['status'] = 1;
        $attach = $this->attach->find($id);
        $type_status = $attach->type_status = $input['type_status'] ? 0 : 1;
        $attach->user_id = $input['user_id'];

        if ($attach->save()) {
            if ($type_status) {
                $TitleActivate = Lang::get('backend/AttachType.view.index.IsEnable');
            } else {
                $TitleActivate = Lang::get('backend/AttachType.view.index.IsDisable');
            }
            return $result = ['action' => true, 'TitleActivate' => $TitleActivate, 'type_status' => $type_status, 'message' => Lang::get('backend/AttachType.messages.msgUpdateOk')];
        }

        return $result = ['action' => false, 'message' => Lang::get('backend/AttachType.messages.msgUpdateNotOk')];


    }

    public function listUploaded()
    {
        $inputs = Input::all();

        $pageData = $this->listUploadedRender($inputs);
        if (!$pageData['action']) {
            $pageData = '';
        }

        if ((isset($inputs['page']) && $inputs['page']) || (isset($inputs['search']) && $inputs['search'])) {
            $pageDataTable = $pageData['pageData'];
            return ['action' => $pageData['action'], 'pageData' => $pageDataTable, 'message' => $pageData['message']];
        } else {
            return view('backend.AttachType.listUploaded')
                ->with('pageData', $pageData['pageData'])
                ->with('locale', $this->locale);
        }

    }


    public function listUploadedSingleUser($user_id, $booking_id)
    {
        if (!$user_id)
            return Redirect::back()->withInput()->withErrors(Lang::get('backend/AttachType.messages.NotValidUser'));;

        $inputs['user_id'] = $user_id;
        $inputs['booking_id'] = $booking_id;

        $pageData = $this->listUploadedRender($inputs);
        if (!$pageData['action']) {
            $pageData = '';
        }

        if ((isset($inputs['page']) && $inputs['page']) || (isset($inputs['search']) && $inputs['search'])) {
            $pageDataTable = $pageData['pageData'];
            return ['action' => $pageData['action'], 'pageData' => $pageDataTable, 'message' => $pageData['message']];
        } else {
            return view('backend.AttachType.listUploaded')
                ->with('pageData', $pageData['pageData'])
                ->with('locale', $this->locale);
        }

    }

    public function listUploadedRender($inputs)
    {

        $lang = $this->language->getLanguage($this->langActive);
        $params['lang_id'] = $lang['lang_id'];
        $params['paginateNum'] = 10;

        //for single user doc
        $params['user_id'] = isset($inputs['user_id']) ? $inputs['user_id'] : false;
        $params['booking_id'] = isset($inputs['booking_id']) ? $inputs['booking_id'] : false;
        //----------------------

        $params['date_from'] = isset($inputs['date_from']) ? $inputs['date_from'] : false;
        $params['date_to'] = isset($inputs['date_to']) ? $inputs['date_to'] : false;

        $params['reg_date_from'] = isset($inputs['reg_date_from']) ? $inputs['reg_date_from'] : false;
        $params['reg_date_to'] = isset($inputs['reg_date_to']) ? $inputs['reg_date_to'] : false;

        $params['first_name'] = isset($inputs['first_name']) ? $inputs['first_name'] : false;
        $params['last_name'] = isset($inputs['last_name']) ? $inputs['last_name'] : false;

        $params['gender'] = isset($inputs['gender']) ? $inputs['gender'] : false;

        $params['country_name'] = isset($inputs['country_name']) ? $inputs['country_name'] : false;

        $params['document_status_is_not_show'] = 5;

        $dataListBookingUsers = $this->attach->getListBookingUsersWithDoc($params);
        $pageData = view('backend.AttachType.listUploadedRender')
            ->with('dataBooking', $dataListBookingUsers)
            ->with('locale', $this->locale)->render();

        if (!$dataListBookingUsers)
            return ['action' => false, 'pageData' => $pageData, 'message' => Lang::get('backend/AttachType.messages.errValid')];

        return ['action' => true, 'pageData' => $pageData, 'message' => ''];
    }

    public function createGroup()
    {

        return view('backend.AttachType.formGroup', ['btnTitle' => Lang::get('backend/AttachType.controller.InsertGroup')
            , 'error' => '', 'title' => Lang::get('backend/AttachType.controller.CreateGroup')])
            ->with('locale', $this->locale)->with('listLang', $this->listLang)->with('setLang', app('getLocaleDefault'));
    }

    public function storeGroup()
    {
        $input = Input::all();
        $lang = $this->language->getLanguage($input['lang']);
        $lang_id = $input['lang_id'] = $lang['lang_id'];
        $input['user_id'] = $this->user_id;
        $input['status'] = 1;
        if (Input::get('alias') != NULL) {
            $alias = Input::get('alias');
        } else {
            $alias = CategoryHelper::createAlias(Input::get('title'));
            $input['alias'] = $alias;
        }
        $fill = $this->attachGroup->fill($input);
        $isValid = $fill->isValid();
        if (!$isValid) {
            $return = Redirect::back()->withInput()->withErrors($this->attachGroup->errors);
            return $return;
        }
        $this->attachGroup->save();

        if ($id = $this->attachGroup->id) {
            $input['id'] = $id;
            foreach ($this->listLang as $key => $value) {
                $lang = $this->language->getLanguage($key);
                $input['lang_id'] = $lang['lang_id'];
                $result = $this->attachGroup->saveGroupLang($input);
            }
            return Redirect::route('backend.attachment.editGroup', array('group' => $id))->with('locale', $this->locale);
        }
        return Redirect::back()->withErrors(Lang::get('backend/AttachType.messages.errCreate'));
    }

    public function editGroup($id)
    {
        $lang = $this->language->getLanguage($this->langActive);
        $data = $this->attachGroup
            ->join('attach_type_group_lang', 'attach_type_group_lang.id', '=', 'attach_type_group.id')
            ->where('attach_type_group.status', '=', 1)
            ->where('attach_type_group.id', '=', $id)
            ->where('attach_type_group_lang.lang_id', '=', $lang['lang_id'])
            ->select(['attach_type_group_lang.title', 'attach_type_group_lang.description', 'attach_type_group.*'])
            ->get()->first();

//        $data=$this->attachGroup->where('status',1)->find($id);
        $errors = '';
        if (!$data)
            $errors[] = Lang::get('backend/AttachType.messages.errValid');

        return view('backend.AttachType.formGroup', [
            'data' => $data
            , 'title' => Lang::get('backend/AttachType.controller.AttachTypeEdit')
            , 'btnTitle' => Lang::get('backend/AttachType.controller.UpdateAttachType')
            , 'error' => $errors
        ])->with('locale', $this->locale)->with('listLang', $this->listLang)->with('setLang', $this->langActive);
    }

    public function updateGroup($id)
    {
        $input = Input::all();
        $lang = $this->language->getLanguage($input['lang']);
        $lang_id = $input['lang_id'] = $lang['lang_id'];

        $input['user_id'] = $this->user_id;
        $input['status'] = 1;
        $input['id'] = $id;
        if (Input::get('alias') != NULL) {
            $alias = Input::get('alias');
        } else {
            $alias = CategoryHelper::createAlias(Input::get('title'));
            $input['alias'] = $alias;
        }
        $attachGroup = $this->attachGroup->find($id);
        $fill = $this->attachGroup->fill($input);
        $isValid = $fill->isValid();
        if (!$isValid) {
            $return = Redirect::back()->withInput()->withErrors($this->attachGroup->errors);
            return $return;
        }

        $input['title'];
        $attachGroup->alias = $input['alias'];
        $attachGroup->condition = $input['condition'];
        $input['description'];
        $attachGroup->user_id = $input['user_id'];

        if ($attachGroup->save() && ($this->attachGroup->updateGroupLang($input)))
            return Redirect::back()->with('message', Lang::get('backend/AttachType.messages.msgUpdateOk'));

        return Redirect::back()->withInput()->withErrors($this->attachGroup->errors);

    }

    public function destroyGroup($id)
    {
        $input = Input::all();
        $input['user_id'] = $this->user_id;
        $attachGroup = $this->attachGroup->find($id);
        $status = $attachGroup->status = 0;
        $attachGroup->user_id = $input['user_id'];

        if ($attachGroup->save()) {
            return $result = ['action' => true, 'message' => Lang::get('backend/AttachType.messages.msgDelOk')];
        }

        return $result = ['action' => false, 'message' => Lang::get('backend/AttachType.messages.msgDelNotOk')];
    }

    public function activateGroup($id)
    {
        $input = Input::all();
        $input['user_id'] = $this->user_id;
        $input['status'] = 1;
        $attachGroup = $this->attachGroup->find($id);
        $group_status = $attachGroup->group_status = $input['group_status'] ? 0 : 1;
        $attachGroup->user_id = $input['user_id'];

        if ($attachGroup->save()) {
            if ($group_status) {
                $TitleActivate = Lang::get('backend/AttachType.view.index.IsEnable');
            } else {
                $TitleActivate = Lang::get('backend/AttachType.view.index.IsDisable');
            }
            return $result = ['action' => true, 'TitleActivate' => $TitleActivate, 'group_status' => $group_status, 'message' => Lang::get('backend/AttachType.messages.msgUpdateOk')];
        }

        return $result = ['action' => false, 'message' => Lang::get('backend/AttachType.messages.msgUpdateNotOk')];


    }

    public function sendMailToUser()
    {
        $inputs = Input::all();
        $data = [];
        $data['booking_id'] = isset($inputs['booking_id']) ? $inputs['booking_id'] : 0;
        $data['user_id'] = isset($inputs['user_id']) ? $inputs['user_id'] : 0;
        $data['username'] = isset($inputs['username']) ? $inputs['username'] : '';
        $data['email'] = isset($inputs['email']) ? $inputs['email'] : '';
        $data['subject'] = Lang::get('backend/AttachType.messages.YouDocIsUnavailable');
        $data['text'] = isset($inputs['text']) ? $inputs['text'] : '';
        $data['page'] = 'emails.unavailable-document';

        if (!$data['booking_id'] || !$data['user_id'] || !$data['email'] || !$data['text'])
            return false;

        $mailModel = new MailModel();
        $result = $mailModel->sendMail($data);
        if (!$result) {
            return ['action' => false, 'result' => $result, 'message' => Lang::get('backend/AttachType.messages.SendMailNotOk')];
        }

        $params = [];
        $params['user_id'] = $data['user_id'];
        $params['booking_id'] = $data['booking_id'];
        $params['assessor_user_id'] = $this->user_id;
        $params['subject'] = $data['subject'];
        $params['feedback_message'] = $data['text'];
        $params['status'] = 3; //document is not valid! and not completed

        $result = $this->attach->addFeedBackForDocument($params);
        if (!$result)
            return ['action' => false, 'result' => $result, 'message' => Lang::get('backend/AttachType.messages.errCreate')];

        return ['action' => true, 'result' => $result, 'message' => Lang::get('backend/AttachType.messages.SendMailOk')];

    }

    public function rejectDocumentsUser($user_id, $booking_id)
    {

        $inputs = Input::all();
        $data = [];
        $data['booking_id'] = $booking_id;
        $data['user_id'] = $user_id;
        $data['username'] = isset($inputs['username']) ? $inputs['username'] : '';
        $data['email'] = isset($inputs['email']) ? $inputs['email'] : '';
        $data['subject'] = Lang::get('backend/AttachType.messages.YouDocRejected');
        $data['text'] = Lang::get('backend/AttachType.messages.TextYouDocRejected');
        $data['page'] = 'emails.unavailable-document';

        if (!$data['booking_id'] || !$data['user_id'] || !$data['email'] || !$data['text'])
            return false;

        $mailModel = new MailModel();
        $result = $mailModel->sendMail($data);
        if (!$result) {
            return ['action' => false, 'result' => $result, 'message' => Lang::get('backend/AttachType.messages.SendMailNotOk')];
        }

        $params = [];
        $paramsDoc = [];
        $params['user_id'] = $data['user_id'];
        $paramsDoc['booking_id'] = $params['booking_id'] = $data['booking_id'];
        $params['assessor_user_id'] = $this->user_id;
        $params['subject'] = $data['subject'];
        $params['feedback_message'] = $data['text'];
        $params['status'] = 5; //all document is not valid! and reject

        $result = $this->attach->addFeedBackForDocument($params);
        if (!$result)
            return ['action' => false, 'result' => $result, 'message' => Lang::get('backend/AttachType.messages.errCreate')];

        $paramsDoc['status'] = 0;
        $paramsDoc['document_status'] = 5;
        $resultDoc = $this->attach->updateAttachDocUser($paramsDoc);

        return ['action' => true, 'result' => $result, 'message' => Lang::get('backend/AttachType.messages.SendMailOk')
            , 'message_status' => Lang::get('backend/AttachType.messages.msgStatusDocumentRejected')];


    }

    public function isOkDocumentsUser($user_id, $booking_id)
    {

        $inputs = Input::all();
        $data = [];
        $data['booking_id'] = $booking_id;
        $data['user_id'] = $user_id;
        $data['username'] = isset($inputs['username']) ? $inputs['username'] : '';
        $data['email'] = isset($inputs['email']) ? $inputs['email'] : '';
        $data['subject'] = Lang::get('backend/AttachType.messages.YouDocIsOk');
        $data['text'] = Lang::get('backend/AttachType.messages.TextYouDocIsOk');
        $data['page'] = 'emails.available-document';

        if (!$data['booking_id'] || !$data['user_id'] || !$data['email'] || !$data['text'])
            return false;

        $mailModel = new MailModel();
        $result = $mailModel->sendMail($data);
        if (!$result) {
            return ['action' => false, 'result' => $result, 'message' => Lang::get('backend/AttachType.messages.SendMailNotOk')];
        }

        $params = [];
        $paramsDoc = [];
        $params['user_id'] = $data['user_id'];
        $paramsDoc['booking_id'] = $params['booking_id'] = $data['booking_id'];
        $params['assessor_user_id'] = $this->user_id;
        $params['subject'] = $data['subject'];
        $params['feedback_message'] = $data['text'];
        $params['status'] = 4; //all document is valid! and ok

        $result = $this->attach->addFeedBackForDocument($params);
        if (!$result)
            return ['action' => false, 'result' => $result, 'message' => Lang::get('backend/AttachType.messages.errCreate')];

        $paramsDoc['status'] = 1;
        $paramsDoc['document_status'] = 4;
        $resultDoc = $this->attach->updateAttachDocUser($paramsDoc);

        return ['action' => true, 'result' => $result, 'message' => Lang::get('backend/AttachType.messages.SendMailOk')
            , 'message_status' => Lang::get('backend/AttachType.messages.msgStatusDocumentIsOk')];


    }

}
