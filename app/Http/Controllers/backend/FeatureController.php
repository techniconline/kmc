<?php
namespace App\Http\Controllers\backend;

use App\BaseModel;
use App\Feature;
use App\FeatureValue;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\App;
use App\Language;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Laracasts\Flash\Flash;
use Illuminate\Support\Facades\Request;

class FeatureController extends BackendController
{

    public $feature;
    public $featureValue;
    public $locale;
    public $langActive;
    public $language;
    public $listLang;

    public function __construct(Feature $feature, FeatureValue $featureValue, Language $language)
    {
        $this->feature = $feature;
        $this->featureValue = $featureValue;
        $this->listLang = Config::get('app.locales');
        $this->langActive = app('getLocale');
        $this->language = $language;
        $this->locale = app('locale') == '' ? '' : app('locale') . '.';
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $lang = $this->language->getLanguage($this->langActive);
        $data = $this->feature
            ->join('features_lang', 'features_lang.feature_id', '=', 'features.id')
            ->where('features.status', '=', 1)
            ->where('features_lang.lang_id', '=', $lang['lang_id'])
            ->select(['features_lang.name', 'features.*'])
            ->get();

        return view('backend.feature.index')->withData($data)->with('locale', $this->locale);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.feature.form', ['btnTitle' => Lang::get('backend/Features.controller.Insert')
            , 'error' => '', 'title' => Lang::get('backend/Features.controller.Create')])
            ->with('locale', $this->locale)->with('listLang', $this->listLang)
            ->with('setLang', app('getLocaleDefault'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = Input::all();
        $input['user_id'] = Auth::employee()->get()->id;
        $input['status'] = 1;
        $input['position'] = 1;
        $fill = $this->feature->fill($input);
        $isValid = $fill->isValid();
        if (!$isValid) {

            Flash::error(Lang::get('backend/Features.messages.errCreate'));
            $return = Redirect::back()->withInput()->withErrors($this->feature->errors);
            return $return;
        }
        $this->feature->save();

        if ($id = $this->feature->id) {
            $params['table'] = 'features_lang';
            foreach ($this->listLang as $key => $value) {
                $lang = $this->language->getLanguage($key);

                $params['items'] = ['feature_id' => $id, 'name' => $input['name'], 'lang_id' => $lang['lang_id']];
                $result = $this->feature->saveItemLang($params);
            }

            Flash::success(Lang::get('backend/Features.messages.msgInsertOk'));
//            return Redirect::route($this->locale . 'backend.feature.edit', array('feature_id' => $id))->with('locale', $this->locale);
            return Redirect::route($this->locale . 'backend.feature.index')->with('locale', $this->locale);

        }

        Flash::error(Lang::get('backend/Features.messages.errCreate'));
        return Redirect::back()->withErrors(Lang::get('backend/Features.messages.errCreate'));

    }


    /**
     * Display the specified resource.
     *
     * @param $feature_id
     * @return Response
     */
    public function show($feature_id)
    {

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param $feature_id
     * @return Response
     */
    public function edit($feature_id)
    {
        $lang = $this->language->getLanguage($this->langActive);

        $data = $this->feature
            ->join('features_lang', 'features_lang.feature_id', '=', 'features.id')
            ->where('features.id', '=', $feature_id)
            ->where('features.status', '=', 1)
            ->where('features_lang.lang_id', '=', $lang['lang_id'])
            ->select(['features_lang.name', 'features.*'])
            ->get()->first();

        $title = Lang::get('backend/Features.controller.Edit');
        $btnTitle = Lang::get('backend/Features.controller.Update');

        if (!$data) {
            $messages[] = ['level' => BaseModel::level_danger, 'message' => Lang::get('backend/Features.messages.errValid')];
            $title = Lang::get('backend/Features.controller.Create');
            $btnTitle = Lang::get('backend/Features.controller.Insert');
        }

        if (isset($messages))
            Session::flash('messages', $messages);

        return view('backend.feature.form', [
            'data' => $data
            , 'title' => $title
            , 'btnTitle' => $btnTitle
        ])->with('locale', $this->locale)->with('listLang', $this->listLang)->with('setLang', $this->langActive);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param $feature_id
     * @return Response
     */
    public function update($feature_id)
    {
        $messages = [];
        $input = Input::all();
        $lang = $this->language->getLanguage($input['lang']);
        $params['lang_id'] = $lang['lang_id'];
        $input['user_id'] = Auth::employee()->get()->id;
        $input['position'] = 1;
        $input['custom'] = isset($input['custom']) && $input['custom'] ? $input['custom'] : 0;

        $feature = $this->feature->find($feature_id);

        $fill = $feature->fill($input);
        $isValid = $fill->isValid();
        if (!$isValid) {
            Flash::error(Lang::get('backend/Features.messages.errValid'));
            $return = Redirect::back()->withInput()->withErrors($this->feature->errors);
            return $return;
        }

        if (!$feature->save()) {
            Flash::error(Lang::get('backend/Features.messages.msgUpdateNotOk'));
            $return = Redirect::back()->withInput();
            return $return;
        }

        $params['id'] = $feature_id;
        $params['table'] = 'features_lang';
        $params['name_id_field'] = 'feature_id';
        $params['items'] = ['name' => $input['name']];

        $result = $this->feature->updateItemLang($params);
        if (!$result) {
            $messages[] = ['level' => BaseModel::level_warning, 'message' => Lang::get('backend/Features.messages.msgUpdateNotOkWarningLang')];
        }

        $messages[] = ['level' => BaseModel::level_success, 'message' => Lang::get('backend/Features.messages.msgUpdateOk')];
        Session::flash('messages', $messages);
        //return Redirect::back();
        return Redirect::route($this->locale . 'backend.feature.index')->with('locale', $this->locale);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param $feature_id
     * @return Response
     */
    public function destroy($feature_id)
    {
        $result = $this->feature->del($feature_id);

        return ($result);
    }


    public function createFeatureValue($feature_id)
    {
        $lang = $this->language->getLanguage($this->langActive);
        $data = $this->feature
            ->join('features_lang', 'features_lang.feature_id', '=', 'features.id')
            ->where('features.id', '=', $feature_id)
            ->where('features.status', '=', 1)
            ->where('features_lang.lang_id', '=', $lang['lang_id'])
            ->first();

        $dataValue = $this->featureValue
            ->join('feature_value_lang', 'feature_value_lang.feature_value_id', '=', 'feature_value.id')
            ->join('features', 'feature_value.feature_id', '=', 'features.id')
            ->join('features_lang', 'features_lang.feature_id', '=', 'features.id')
            ->where('features.status', '=', 1)
            ->where('features_lang.lang_id', '=', $lang['lang_id'])
            ->where('feature_value_lang.lang_id', '=', $lang['lang_id'])
            ->where('feature_value.feature_id', $feature_id)
            ->select(['feature_value_lang.value', 'feature_value_lang.title', 'features.type', 'features.custom', 'feature_value.*'])
            ->get();

        return view('backend.feature.formValue', ['btnTitle' => Lang::get('backend/Features.controller.Insert')
            , 'title' => Lang::get('backend/Features.controller.Create')])
            ->withData($data)
            ->with('DataValue', $dataValue)
            ->with('locale', $this->locale)
            ->with('listLang', $this->listLang)
            ->with('setLang', app('getLocaleDefault'));

    }


    public function storeFeatureValue($feature_id)
    {

        $input = Input::all();
        $input['feature_id'] = $feature_id;
        $lang = $this->language->getLanguage($this->langActive);
        $input['lang_id'] = $lang['lang_id'];

        $result = $this->featureValue->storeFeatureValues($input);

        if (!$result['valid'] && $result['err_list']) {
            Flash::error(Lang::get('backend/Features.messages.errCreate'));
            $return = Redirect::back()->withInput()->withErrors($result['err_list']);
            return $return;
        } elseif ($result['valid']) {
            Flash::success(Lang::get('backend/Features.messages.msgInsertOk'));
            return Redirect::route('backend.feature.editFeatureValue', array('feature_id' => $feature_id, 'feature_value_id' => $result['feature_value_id']))
                ->with('locale', $this->locale);
        } else {
            Flash::error(Lang::get('backend/Features.messages.errCreate'));
            return Redirect::back()->withErrors(Lang::get('backend/Features.messages.errCreate'));
        }

    }

    public function editFeatureValue($feature_id, $feature_value_id)
    {
        $lang = $this->language->getLanguage($this->langActive);

        $dataFeatureValue = $this->featureValue
            ->join('feature_value_lang', 'feature_value_lang.feature_value_id', '=', 'feature_value.id')
            ->join('features', 'feature_value.feature_id', '=', 'features.id')
            ->join('features_lang', 'features_lang.feature_id', '=', 'features.id')
            ->where('features.status', '=', 1)
            ->where('features_lang.lang_id', '=', $lang['lang_id'])
            ->where('feature_value_lang.lang_id', '=', $lang['lang_id'])
            ->where('feature_value.feature_id', $feature_id)
            ->where('feature_value.id', $feature_value_id)
            ->select(['feature_value_lang.value', 'feature_value_lang.title', 'features_lang.name', 'features.type', 'features.custom', 'feature_value.*'])
            ->first();


        $title = Lang::get('backend/Features.controller.Edit');
        $btnTitle = Lang::get('backend/Features.controller.Update');

        if (!$dataFeatureValue) {
            $messages[] = ['level' => BaseModel::level_danger, 'message' => Lang::get('backend/Features.messages.errValid')];
            $title = Lang::get('backend/Features.controller.Create');
            $btnTitle = Lang::get('backend/Features.controller.Insert');
        }

        if (isset($messages))
            Session::flash('messages', $messages);

        return view('backend.feature.formValue', [
            'dataFeatureValue' => $dataFeatureValue
            , 'data' => $dataFeatureValue
            , 'title' => $title
            , 'btnTitle' => $btnTitle
        ])->with('locale', $this->locale)->with('listLang', $this->listLang)->with('setLang', $this->langActive);
    }

    public function updateFeatureValue($feature_id, $feature_value_id)
    {
        $messages = [];
        $input = Input::all();
        $lang = $this->language->getLanguage($input['lang']);
        $params['lang_id'] = $lang['lang_id'];
        $input['user_id'] = Auth::employee()->get()->id;
        $input['position'] = 1;

        $featureValue = $this->featureValue->find($feature_value_id);

        $fill = $featureValue->fill($input);
        $isValid = $fill->isValid();
        if (!$isValid) {
            Flash::error(Lang::get('backend/Features.messages.errValid'));
            $return = Redirect::back()->withInput()->withErrors($this->featureValue->errors);
            return $return;
        }

        if (!$featureValue->save()) {
            Flash::error(Lang::get('backend/Features.messages.msgUpdateNotOk'));
            $return = Redirect::back()->withInput();
            return $return;
        }

        $params['id'] = $feature_value_id;
        $params['table'] = 'feature_value_lang';
        $params['name_id_field'] = 'feature_value_id';
        $params['items'] = ['value' => $input['value'], 'title' => $input['title']];

        $result = $this->featureValue->updateItemLang($params);
        if (!$result) {
            $messages[] = ['level' => BaseModel::level_warning, 'message' => Lang::get('backend/Features.messages.msgUpdateNotOkWarningLang')];
        }

        $messages[] = ['level' => BaseModel::level_success, 'message' => Lang::get('backend/Features.messages.msgUpdateOk')];
        Session::flash('messages', $messages);
        return Redirect::back();
    }


}
