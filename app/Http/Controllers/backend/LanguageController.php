<?php
namespace App\Http\Controllers\backend;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Media;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\App;
use App\Language;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class LanguageController extends BackendController
{

    public $apartment;
    public $locale;
    public $langActive;
    public $language;
    public $listLang;
    public $langUrl;

    public function __construct(Language $language)
    {
        $this->listLang = Config::get('app.locales');
        $this->langActive = app('getLocale');
        $this->language = $language;
        $this->locale = app('locale') == '' ? '' : app('locale') . '.';

        $this->langUrl = base_path() . '/resources/lang/';

    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
//        Lang::get('backend/Apartment.controller.InsertApartment');
        $data = File::allFiles($this->langUrl);

        $urlPath = [];
        $typeLang = [];
        $counter = 0;
        foreach ($data as $item) {
            $urlArr = explode("lang", $item);
            $orginUrl = $urlPath[$counter]['url'] = substr($urlArr[1], 4);
            $lenOrgin = strlen($orginUrl);
            $urlPath[$counter]['url'] = substr($orginUrl, 0, $lenOrgin - 4);

            $langArr = explode("\\", $urlArr[1]);
            $urlPath[$counter]['lang'] = $langArr[1];

            $urlPath[$counter]['urlAlias'] = $urlPath[$counter]['lang'] . '-' . str_replace("\\", "-", $urlPath[$counter]['url']);


            $counter++;
        }

        return view('backend.language.index')->withData($data)->with(['dataList' => $urlPath])->with('locale', $this->locale);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = Input::all();
        $input['user_id'] = Auth::employee()->get()->id;
    }


    /**
     * Display the specified resource.
     *
     * @param  int $apartment_id
     * @return Response
     */
    public function show($url)
    {
        $contents = File::getRequire($this->langUrl . str_replace("-", "/", $url) . '.php');


        return view('backend.language.show')->withData($contents)->with('locale', $this->locale);


    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $apartment_id
     * @return Response
     */
    public function edit($apartment_id)
    {
        $lang = $this->language->getLanguage($this->langActive);

    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int $apartment_id
     * @return Response
     */
    public function update($apartment_id)
    {
        $bytes_written = File::put('$file', '$contents');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $apartment_id
     * @return Response
     */
    public function destroy($apartment_id)
    {

    }


}
