<?php namespace App\Http\Controllers\backend;

use App\Employee;
use App\Language;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Intervention\Image\Facades\Image;

class EmployeesController extends BackendController
{

    public $employee;
    public $locale;
    public $langActive;
    public $language;
    public $listLang;

    public function __construct(Employee $employee, Language $language)
    {
        $this->employee = $employee;
        $this->listLang = Config::get('app.locales');
        $this->langActive = app('getLocale');
        $this->language = $language;
        $this->locale = app('locale') == '' ? '' : app('locale') . '.';
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $employees = $this->employee->all();
        return view('backend.employees.index', [
            'employees' => $employees,
            'locale' => $this->locale
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.employees.form', [
            'locale' => $this->locale
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = Input::all();

        if (!$this->employee->fill($input)->isValid()) {
            dd($input);
            return redirect()->back()->withInput()->withErrors($this->employee->errors);
        }
        $this->employee->save();

        if (Request::hasFile('avatar')) {
            if (File::exists(base_path() . env('PUBLIC_PATH_URL', 'public') . '/uploads/images/Employees/' . $this->employee->id . '-200x200.jpg')) {
                File::delete($this->employee->id . '-200x200.jpg');
            }
            Image::make(Input::file('avatar'))->fit(200, 200)->save('uploads/images/Employees/' . $this->employee->id . '-200x200.jpg');
        }

        return $this->index();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $employee = $this->employee->find($id);
        return view('backend.employees.form', [
            'employee' => $employee,
            'locale' => $this->locale
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $input = Input::all();
        $employee = $this->employee->find($id);

        if (Request::hasFile('avatar')) {
            if (File::exists(base_path() . env('PUBLIC_PATH_URL', 'public') . '/uploads/images/Employees/' . $employee->id . '-200x200.jpg')) {
                File::delete($employee->id . '-200x200.jpg');
            }
            Image::make(Input::file('avatar'))->fit(200, 200)->save('uploads/images/Employees/' . $employee->id . '-200x200.jpg');
        }

        $employee->fill($input);
        $employee->save();

        return Redirect::back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

}
