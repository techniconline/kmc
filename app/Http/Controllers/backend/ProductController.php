<?php
namespace App\Http\Controllers\backend;

use App\Feature;
use App\FeatureGroup;
use App\FeatureValue;
use App\Product;
use App\Category;
use App\Providers\Helpers\Category\CategoryHelper;
use App\Providers\Helpers\PersianDate\PersianDate;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Media;
use Illuminate\Support\Facades\Lang;
use App\BaseModel;
use App\Language;
use Illuminate\Support\Facades\Config;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

class ProductController extends BackendController
{

    public $product;
    public $featureGroup;
    public $feature;
    public $featureValue;

    public $category;
    public $locale;
    public $langActive;
    public $language;
    public $listLang;

    public function __construct(Category $category, Product $product, Language $language, Feature $feature, FeatureGroup $featureGroup, FeatureValue $featureValue)
    {
        $this->products = $product;
        $this->category = $category;
        $this->featureGroup = $featureGroup;
        $this->feature = $feature;
        $this->featureValue = $featureValue;

        $this->listLang = Config::get('app.locales');
        $this->langActive = app('getLocale');
        $this->language = $language;
        $this->locale = app('locale') == '' ? '' : app('locale') . '.';
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $lang = $this->language->getLanguage($this->langActive);
        $data = $this->products
            ->join('products_lang', 'products_lang.id', '=', 'products.id')
            ->where('products.status', '=', 1)
            ->where('products_lang.lang_id', '=', $lang['lang_id'])
            ->select(['products_lang.title', 'products.*'])
            ->get();

        return view('backend.product.index')->withData($data)->with('locale', $this->locale);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $lang = $this->language->getLanguage($this->langActive);
        $results = $this->category->getDataCategory(1);
        $roots = $results['roots'];
        $translatedName = $results['translatedLang'];

        $resFeature = $this->products->getDataFeatures($lang['lang_id'], []);

        return view('backend.product.create', ['btnTitle' => Lang::get('backend/Product.controller.InsertProduct')
            , 'error' => ''
            , 'dataFG' => $resFeature['dataFG']
            , 'allDataFG' => $resFeature['allDataFG']
            , 'color_selected' => $resFeature['color_selected']
            , 'title' => Lang::get('backend/Product.controller.CreateProduct')])
            ->with('roots', $roots)
            ->with('translatedName', $translatedName)
            ->with('locale', $this->locale)
            ->with('listLang', $this->listLang)
            ->with('setLang', app('getLocaleDefault'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = Input::all();
        $input['user_id'] = Auth::employee()->get()->id;
        $input['status'] = 1;
        $input['address'] = isset($input['formatted_address']) ? $input['formatted_address'] : null;
        $input['category_id'] = isset($input['categories']) ? $input['categories'][0] : null;

        $alias = isset($input['alias']) && $input['alias'] ? str_replace(" ", "-", $input['alias']) : CategoryHelper::createAlias($input['title']);
        $input['alias'] = $alias;
        $input['active_date'] = isset($input['active_date']) && $input['active_date'] ? $input['active_date'] : date('Y-m-d H:i:s');
        $activeDateArr = explode(" ", $input['active_date']);
        $input['active_date'] = PersianDate::convert_date_H_to_M($activeDateArr[0]) . ' ' . $activeDateArr[1];

        $fill = $this->products->fill($input);
        $isValid = $fill->validationData($input, $this->products->rules);
        if (!$isValid) {
            $return = Redirect::back()->withInput()->withErrors($this->products->errors);
            return $return;
        }
        $this->products->save();

        if ($id = $this->products->id) {

            if (Request::hasFile('image_product')) {
                $extension = Input::file('image_product')->getClientOriginalExtension();
                $createFolder = base_path() . env('PUBLIC_PATH_URL', 'public') . '/uploads/images/product/' . $id . '/';
                if (!file_exists($createFolder)) {
                    mkdir($createFolder, 0755);
                }
                if (File::exists(base_path() . env('PUBLIC_PATH_URL', 'public') . '/uploads/images/product/' . $id . '/' . $id . '-1920x600.' . $extension)) {
                    File::delete($id . '-1920x600.' . $extension);
                }
                Image::make(Input::file('image_product'))->fit(1920, 600)->save('uploads/images/product/' . $id . '/' . $id . '-1920x600.' . $extension);
                $updateProduct = $this->products->find($id);
                $updateProduct->image = '/uploads/images/product/' . $id . '/' . $id . '-1920x600.' . $extension;
                $updateProduct->save();
                $messages[] = ['level' => BaseModel::level_success, 'message' => Lang::get('backend/Content.messages.uploadFileSuccess')];
                Session::flash('messages', $messages);
            }

            $input['id'] = $id;
            foreach ($this->listLang as $key => $value) {
                $lang = $this->language->getLanguage($key);
                $input['lang_id'] = $lang['lang_id'];
                $alias = isset($input['alias']) && $input['alias'] ? str_replace(" ", "-", $input['alias']) : CategoryHelper::createAlias($input['title']);
                $input['alias'] = $alias;

                $result = $this->products->saveProductLang($input);
            }

            $resCategories = $this->products->saveProductCategories($id, $input['categories']);

            return Redirect::route($this->locale . 'backend.product.edit', array('product_id' => $id))->with('locale', $this->locale);
        }
        return Redirect::back()->withErrors(Lang::get('backend/Product.messages.errCreate'));

    }


    /**
     * Display the specified resource.
     *
     * @param  int $product_id
     * @return Response
     */
    public function show($product_id)
    {
//        $data=$this->products->where('status',1)->find($product_id);
//		return view('backend.product.show',['data'=>$data])->with('locale',$this->locale);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $product_id
     * @return Response
     */
    public function edit($product_id)
    {
        $results = $this->category->getDataCategory(1);
        $roots = $results['roots'];
        $translatedName = $results['translatedLang'];

        $lang = $this->language->getLanguage($this->langActive);
//        $data=$this->products->where('status',1)->find($product_id);
        $data = $this->products
            ->join('products_lang', 'products_lang.id', '=', 'products.id')
            ->where('products.id', '=', $product_id)
            ->where('products.status', '=', 1)
            ->where('products_lang.lang_id', '=', $lang['lang_id'])
            ->select(['products_lang.title', 'products_lang.short_description', 'products_lang.description', 'products_lang.alias', 'products.*'])
            ->get()->first();
        $errors = '';
        if (!$data)
            $errors[] = Lang::get('backend/Product.messages.errValid');

        $mediaModel = new Media();
        $images = $mediaModel->getMediaProduct('image', $product_id);
        $videos = $mediaModel->getMediaProduct('video', $product_id);
        $categoriesProduct = $this->products->getProductCategories($product_id, true);

        $featuresProduct = $this->featureValue
            ->join('feature_value_lang', 'feature_value.id', '=', 'feature_value_lang.feature_value_id')
            ->join('feature_product', 'feature_product.feature_value_id', '=', 'feature_value.id')
            ->where('feature_product.product_id', $product_id)
            ->where('feature_value_lang.lang_id', '=', $lang['lang_id'])
            ->select(['feature_value.id', 'feature_value_lang.value'])
            ->get();
//            ->lists('feature_value.id' , 'feature_value_lang.value');

//        dd($featuresProduct);

        $featuresProduct_NEW = [];
        foreach ($featuresProduct as $item) {
            $featuresProduct_NEW[$item->value][] = $item->id;
        }
//
//        dd($featuresProduct_NEW);

        $resFeature = $this->products->getDataFeatures($lang['lang_id'], $featuresProduct_NEW);

//        dd($resFeature);

        return view('backend.product.create', [
            'data' => $data
            , 'dataFG' => $resFeature['dataFG']
            , 'allDataFG' => $resFeature['allDataFG']
            , 'color_selected' => $resFeature['color_selected']
            , 'featuresProduct' => $featuresProduct
            , 'images' => $images
            , 'videos' => $videos
            , 'categoriesProduct' => $categoriesProduct
            , 'title' => Lang::get('backend/Product.controller.ProductEdit')
            , 'btnTitle' => Lang::get('backend/Product.controller.UpdateProduct')
            , 'error' => $errors
        ])
            ->with('roots', $roots)
            ->with('translatedName', $translatedName)
            ->with('locale', $this->locale)->with('listLang', $this->listLang)->with('setLang', $this->langActive);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int $product_id
     * @return Response
     */
    public function update($product_id)
    {
        $input = Input::all();
        $lang = $this->language->getLanguage($input['lang']);
        $input['lang_id'] = $lang['lang_id'];
        $inputImageClass = isset($input['class']) ? $input['class'] : null;
        $inputImageSort = isset($input['sort']) ? $input['sort'] : null;
        $inputImageFeatureSelect = isset($input['feature_value_selected']) ? $input['feature_value_selected'] : null;

        $inputImage = Input::get('image');
        $inputImageTitle = Input::get('image_title');
        $inputVideo = Input::get('video');
        $inputVideoTitle = Input::get('video_title');
        $inputMediaDel = Input::get('media_del');
        $input['user_id'] = Auth::employee()->get()->id;
        $input['active_date'] = isset($input['active_date']) && $input['active_date'] ? $input['active_date'] : date('Y-m-d H:i:s');
        $activeDateArr = explode(" ", $input['active_date']);
        $input['active_date'] = PersianDate::convert_date_H_to_M($activeDateArr[0]) . ' ' . $activeDateArr[1];
        $input['category_id'] = isset($input['categories']) ? $input['categories'][0] : null;

        $mediaArr = array(
            'images' => $inputImage,
            'images_title' => $inputImageTitle,
            'images_class' => $inputImageClass,
            'images_sort' => $inputImageSort,
            'images_feature_select' => $inputImageFeatureSelect,
            'videos' => $inputVideo,
            'videos_title' => $inputVideoTitle
        );

        $alias = isset($input['alias']) && $input['alias'] ? str_replace(" ", "-", $input['alias']) : CategoryHelper::createAlias($input['title']);
        $input['alias'] = $alias;

        $result = $this->products->updateProduct($product_id, $input, $mediaArr, $inputMediaDel);
        $resCategories = $this->products->saveProductCategories($product_id, $input['categories']);
        $req = Request::hasFile('image_product');
        if (Request::hasFile('image_product')) {
            $extension = Input::file('image_product')->getClientOriginalExtension();
            $createFolder = base_path() . env('PUBLIC_PATH_URL', 'public') . '/uploads/images/product/' . $product_id . '/';
            if (!file_exists($createFolder)) {
                mkdir($createFolder, 0755);
            }
            if (File::exists(base_path() . env('PUBLIC_PATH_URL', 'public') . '/uploads/images/product/' . $product_id . '/' . $product_id . '-1920x600.' . $extension)) {
                File::delete($product_id . '-1920x600.' . $extension);
            }
            Image::make(Input::file('image_product'))->fit(1920, 600)->save('uploads/images/product/' . $product_id . '/' . $product_id . '-1920x600.' . $extension);
            $updateProduct = $this->products->find($product_id);
            $updateProduct->image = '/uploads/images/product/' . $product_id . '/' . $product_id . '-1920x600.' . $extension;
            $updateProduct->save();
            $messages[] = ['level' => BaseModel::level_success, 'message' => Lang::get('backend/Content.messages.uploadFileSuccess')];
            Session::flash('messages', $messages);
        }

        if (!$result['action']) {
            $return = Redirect::back()->withInput()->withErrors($result['message']);
            return $return;
        }
        return Redirect::back()->with('message', $result['message']);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $product_id
     * @return Response
     */
    public function destroy($product_id)
    {
        $result = $this->products->del($product_id);
//        $result = DB::table('products_lang')->where('id', '=', $product_id)->delete();

        return ($result);
    }

    public function isDefault()
    {
        $input = Input::all();
        $product_id = $input['product_id'];
        $media_id = $input['media_id'];
        $result = $this->products->isDefault($product_id, $media_id);

        return $result;

    }

}
