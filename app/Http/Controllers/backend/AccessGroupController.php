<?php namespace App\Http\Controllers\backend;

use App\AccessGroups;
use App\AccessList;
use App\ActionsList;
use App\Employee;
use App\User;
use App\UserAccessGroup;
use Illuminate\Support\Facades\App;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Lang;
use App\Providers\Helpers\Category\CategoryHelper;
use App\Language;
use App\MailModel;
use Laracasts\Flash\Flash;

class AccessGroupController extends BackendController
{

    public $accessGroup;
    public $accessList;
    public $userAccessGroup;
    public $locale;
    public $langActive;
    public $language;
    public $listLang;
    public $user_id;

    public function __construct(Language $language, AccessGroups $accessGroups, UserAccessGroup $userAccessGroup, AccessList $accessList)
    {
        $this->listLang = Config::get('app.locales');
        $this->accessGroup = $accessGroups;
        $this->accessList = $accessList;
        $this->userAccessGroup = $userAccessGroup;
        $this->langActive = app('getLocale');
        $this->language = $language;
        $this->locale = app('locale') == '' ? '' : app('locale') . '.';
        $this->user_id = isset(Auth::employee()->get()->id) ? Auth::employee()->get()->id : 0;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $inputs = Input::all();

        $pageData = $this->listGroupRender($inputs);

        if ((isset($inputs['page']) && $inputs['page']) || (isset($inputs['search']) && $inputs['search']) || Request::ajax()) {
            $pageDataTable = $pageData['pageData'];
            return ['action' => $pageData['action'], 'pageData' => $pageDataTable, 'message' => $pageData['message']];
        } else {
            return view('backend.accessGroup.index')
                ->with('pageData', $pageData['pageData'])
                ->with('locale', $this->locale);
        }
    }

    public function listGroupRender($inputs)
    {


        $inputs['paginateNum'] = isset($inputs['paginateNum']) && $inputs['paginateNum'] ? $inputs['paginateNum'] : 10;

        $data = $this->accessGroup->getGroupList($inputs);

        $pageData = view('backend.accessGroup.listGroupRender')
            ->with('data', $data)
            ->with('locale', $this->locale)->render();

        if (!$data)
            return ['action' => false, 'pageData' => $pageData, 'message' => Lang::get('backend/AccessGroup.messages.errValid')];

        return ['action' => true, 'pageData' => $pageData, 'message' => ''];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
//        $lang = $this->language->getLanguage($this->langActive);
//        $params['lang_id'] = $lang['lang_id'];

        return view('backend.accessGroup.form')
            ->with('title', Lang::get('backend/AccessGroup.view.form.TitleSave'))
            ->with('btnTitle', Lang::get('backend/AccessGroup.view.form.Save'))
            ->with('locale', $this->locale);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = Input::all();

        $fill = $this->accessGroup->fill($input);
        $isValid = $fill->isValid();

        if (!$isValid) {
            Flash::error(Lang::get('backend/AccessGroup.messages.errValid'));
            $return = Redirect::back()->withInput()->withErrors($this->accessGroup->errors);
            return $return;
        }

        $this->accessGroup->save();

        if ($id = $this->accessGroup->id) {
            Flash::success(Lang::get('backend/AccessGroup.messages.added'));
            return Redirect::route($this->locale . 'backend.accessGroup.edit', array('id' => $id));
        }
        Flash::error(Lang::get('backend/AccessGroup.messages.errCreate'));
        return Redirect::back()->withErrors(Lang::get('backend/AccessGroup.messages.errCreate'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $data = $this->accessGroup->find($id);

        if (!$data) {
            Flash::error(Lang::get('backend/AccessGroup.messages.NotValidData'));
            Redirect::back();
        }

        if ($data->type == 'backend') {
            $userList = Employee::where('status', 1)->get();
            $userType = 'employee_id';
        } else {
            $userList = User::where('status', 1)->get();
            $userType = 'user_id';
        }

        $userActiveList = $this->userAccessGroup->where('status', 1)
            ->where('access_group_id', $id)->lists($userType);

        return view('backend.accessGroup.form')
            ->with('title', Lang::get('backend/AccessGroup.view.form.TitleSave'))
            ->with('btnTitle', Lang::get('backend/AccessGroup.view.form.Save'))
            ->with('data', $data)
            ->with('userList', $userList)
            ->with('userActiveList', $userActiveList)
            ->with('locale', $this->locale);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {

        $data = $this->accessGroup->find($id);

        return view('backend.accessGroup.form', [
            'data' => $data
        ])
            ->with('title', Lang::get('backend/AccessGroup.view.form.TitleSave'))
            ->with('btnTitle', Lang::get('backend/AccessGroup.view.form.Save'))
            ->with('locale', $this->locale);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $input = Input::all();
        $data = $this->accessGroup->find($id);
        $beforeType = $data->type;

        $fill = $data->fill($input);

        $isValid = $fill->isValid();

        if (!$isValid) {
            Flash::error(Lang::get('backend/AccessGroup.messages.errValid'));
            $return = Redirect::back()->withInput()->withErrors($data->errors);
            return $return;
        }

        if ($data->save()) {
            $afterType = $data->type;
            $editPage = true;
            if ($beforeType != $afterType) {
                $this->accessGroup->removeUsersFromGroup($id);
            } else {
                if (isset($input['user_ids']) && $input['user_ids']) {
                    $editPage = false;
                    if ($data->type == AccessGroups::TYPE_BACKEND) {
                        $typeUsers = 'employee_id';
                    } else {
                        $typeUsers = 'user_id';
                    }
                    $this->accessGroup->removeUsersFromGroup($id);
                    $this->accessGroup->addUsersFromGroup($id, $input['user_ids'], $typeUsers);
                }
            }

            Flash::success(Lang::get('backend/AccessGroup.messages.msgUpdateOk'));
            if ($editPage) {
                $editPage = Redirect::route($this->locale . 'backend.accessGroup.edit', array('id' => $id));
            } else {
                $editPage = Redirect::route($this->locale . 'backend.accessGroup.show', array('id' => $id));
            }
            return $editPage;
        }
        Flash::error(Lang::get('backend/AccessGroup.messages.msgUpdateNotOk'));
        return Redirect::back()->withErrors(Lang::get('backend/AccessGroup.messages.msgUpdateNotOk'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $data = $this->accessGroup->find($id);
        $data->status = 0;
        if ($data->save()) {
            Flash::success(Lang::get('backend/AccessGroup.messages.msgDelOk'));
            return Redirect::back();
        }
        Flash::error(Lang::get('backend/AccessGroup.messages.msgDelNotOk'));
        return Redirect::back()->withErrors(Lang::get('backend/AccessGroup.messages.msgDelNotOk'));
    }

    public function showAccessList($id)
    {
        $data = $this->accessGroup->find($id);

        if (!$data) {
            Flash::error(Lang::get('backend/AccessGroup.messages.NotValidData'));
            Redirect::back();
        }

        if ($data->type == 'backend') {
            $actionList = ActionsList::where('status', 1)->where('prefix', AccessGroups::TYPE_BACKEND)
                ->orderBy('controller')
                ->get();
            $userType = 'employee_id';
        } else {
            $actionList = ActionsList::where('status', 1)->whereIn('prefix', [AccessGroups::TYPE_FRONTEND, AccessGroups::TYPE_PUBLIC])
                ->orderBy('controller')
                ->get();
            $userType = 'user_id';
        }

        $actionActiveList = $this->accessList->where('status', 1)->where('access', 1)
            ->where('access_group_id', $id)->lists('action_id');

        return view('backend.accessGroup.showAccessList')
            ->with('title', Lang::get('backend/AccessGroup.view.form.TitleSave'))
            ->with('btnTitle', Lang::get('backend/AccessGroup.view.form.Save'))
            ->with('data', $data)
            ->with('actionList', $actionList)
            ->with('actionActiveList', $actionActiveList)
            ->with('locale', $this->locale);

    }

    public function updateAccessList($id)
    {
        $input = Input::all();
        $data = $this->accessGroup->find($id);

        if (!$data || (!isset($input['action_ids']) || !$input['action_ids'])) {
            Flash::error(Lang::get('backend/AccessGroup.messages.errValid'));
            $return = Redirect::back();
            return $return;
        }

        $result = $this->accessList->addAccessToGroup($id, $input['action_ids'], $this->user_id);

        if ($result) {
            Flash::success(Lang::get('backend/AccessGroup.messages.msgUpdateOk'));
            $return = Redirect::back();
            return $return;
        }
        Flash::error(Lang::get('backend/AccessGroup.messages.msgUpdateNotOk'));
        return Redirect::back()->withErrors(Lang::get('backend/AccessGroup.messages.msgUpdateNotOk'));

    }


}
