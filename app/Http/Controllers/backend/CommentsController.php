<?php namespace App\Http\Controllers\backend;

use App\BaseModel;
use App\Comments;
use App\Content;
use App\ContentLang;
use App\Language;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Laracasts\Flash\Flash;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Request;

class CommentsController extends BackendController
{

    public $content;
    public $locale;
    public $langActive;
    public $language;
    public $listLang;
    public $comment;

    public function __construct(Content $content, Language $language, Comments $comments )
    {
        $this->content = $content;
        $this->comment = $comments;
        $this->listLang = Config::get('app.locales');
        $this->langActive = app('getLocale');
        $this->language = $language;
        $this->locale = app('locale') == '' ? '' : app('locale') . '.';
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $comments = $this->comment
            ->where('comments.status', 1)
            ->orderBy('id','DESC')
            ->paginate(20);

        $lang_id = app('activeLangDetails')['lang_id'];
        foreach($comments as &$item){
            $model = $this->comment->getSystemModel($item->system);
            $item->url=null;
            if($model){
                if($item->system=='product'){
                    $data = $model->join('products_lang','products.id','=','products_lang.id')
                        ->where('lang_id',$lang_id)
                        ->where('products.id',$item->item_id)
                        ->select('alias')->first();
                    $item->url=$item->item_id;
                    if($data){
                        $item->url = URL::route($this->locale.'product.show',$data->alias);
                    }
                }else{
                    $data = $model->find($item->item_id);
                    $item->url=$item->item_id;
                    if($data){
                        $item->url = URL::route($this->locale.$item->system.'.show',$data->url);
                    }
                }

            }

        }

        return view('backend.comments.index', [
            'data' => $comments,
            'locale' => $this->locale
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = Input::all();

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $comment = $this->comment->find($id)->delete();

        Flash::success(trans('backend/Content.messages.successDelete'));
        return $this->index();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $input = Input::all();
        $lang = $this->language->getLanguage($input['lang']);
        $input['lang_id'] = $lang['lang_id'];

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $comment = $this->comment->find($id)->delete();

        Flash::success(trans('backend/Content.messages.successDelete'));
        return $this->index();
    }

}
