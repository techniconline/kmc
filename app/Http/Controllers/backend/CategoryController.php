<?php namespace App\Http\Controllers\backend;

use App\Category;
use App\CategoryLang;
use App\Language;
use App\Providers\Helpers\Category\CategoryHelper;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Intervention\Image\Facades\Image;

class CategoryController extends BackendController
{


    public $category;
    public $locale;
    public $langActive;
    public $language;
    public $listLang;

    public function __construct(Category $category, Language $language)
    {
        $this->category = $category;
        $this->listLang = Config::get('app.locales');
        $this->langActive = app('getLocale');
        $this->language = $language;
        $this->locale = app('locale') == '' ? '' : app('locale') . '.';
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $results = $this->category->getDataCategory();
        $roots = $results['roots'];
        $translatedName = $results['translatedLang'];
        return view('backend.Category.index')->with('roots', $roots)->with('locale', $this->locale)->with('translatedName', $translatedName);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $grandpaId = Input::get('grandpa_id') != NULL ? Input::get('grandpa_id') : NULL;
        $parentId = Input::get('parent_id') != NULL ? Input::get('parent_id') : NULL;
        $rootId = Input::get('root_id') != NULL ? Input::get('root_id') : NULL;
        if (Input::get('alias') != NULL) {
            $alias = Input::get('alias');
        } else {
            $alias = CategoryHelper::createAlias(Input::get('newCat'));
        }

        $newRoot = CategoryLang::where('name', '=', Input::get('newCat'))->first();

        if (!isset($newRoot) && $parentId == NULL) {
            $lastRoot = $this->category->roots()->max('root_id');
            $category = $this->category->create(['root_id' => $lastRoot + 1, 'alias' => $alias]);

            // Create Translate Named For Each Language That Setup For Application
            foreach ($this->listLang as $key => $value) {
                $lang = $this->language->getLanguage($key);
                CategoryLang::saveLang(['categories_id' => $category->id, 'lang_id' => $lang['lang_id'], 'name' => Input::get('newCat')]);
            }

            return Redirect::route($this->locale . 'backend.category.index');

        } elseif (!isset($newRoot) && $parentId != NULL) {
            $newRoot = $this->category->find($parentId);
            $child = $newRoot->children()->create(['root_id' => $rootId, 'alias' => $alias]);

            // Create Translate Named For Each Language That Setup For Application
            foreach ($this->listLang as $key => $value) {
                $lang = $this->language->getLanguage($key);
                CategoryLang::saveLang(['categories_id' => $child->id, 'lang_id' => $lang['lang_id'], 'name' => Input::get('newCat')]);
            }
            return $child;

        }
        return Redirect::route($this->locale . 'backend.category.show');
    }

    /**
     * Display the specified resource.
     *
     * @param  string $urlAlias
     * @return Response
     */
    public function show($urlAlias)
    {
        $catArr = explode("-", $urlAlias);
        $url = substr(Request::url(), 0, strrpos(Request::url(), '/') + 1);
        $myRoot = $this->category->where('id', '=', $catArr[0])->first();
        $cats = $myRoot->getDescendantsAndSelf()->toHierarchy();
//        $translatedName = CategoryLang::where('lang_id',app('activeLangDetails')['lang_id'])->lists('name','categories_id');

        $translated = CategoryLang::where('lang_id', app('activeLangDetails')['lang_id'])->get();
        $translatedLang = [];
        foreach ($translated as $item) {
            $translatedLang[$item->categories_id] = [
                'name' => $item->name
                , 'desc' => $item->desc
                , 'tag' => $item->tag];
        }

        return view('backend.Category.subCats')
            ->with('subCats', $cats)
            ->with('myRoot', $myRoot)
            ->with('url', $url)
            ->with('locale', $this->locale)
            ->with('translatedName', $translatedLang);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $category = $this->category->where('id', $id)->join('categories_lang', 'id', '=', 'categories_id')->where('categories_lang.lang_id', '=', app('activeLangDetails')['lang_id'])->first();
        return view('backend.Category.form', [
            'category' => $category,
            'locale' => $this->locale,
            'listLang' => $this->listLang
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $input = Input::all();
        $lang = $this->language->getLanguage($input['lang']);
        $input['lang_id'] = $lang['lang_id'];

        $paramsForTranslate = [
            'categories_id' => $id,
            'lang_id' => $input['lang_id'],
            'name' => $input['name'],
            'desc' => $input['desc']
        ];
        CategoryLang::updateLang($paramsForTranslate);

        $category = $this->category->find($id);
        $category->alias = $input['alias'];
        $image = $category->image == 'true' ? 'true' : 'false';
        if (Request::hasFile('image')) {
            if (File::exists(base_path() . env('PUBLIC_PATH_URL', 'public') . '/uploads/images/Categories/' . $category->id . '-300x100.jpg')) {
                File::delete($category->id . '-300x100.jpg');
                File::delete($category->id . '-600x200.jpg');
                File::delete($category->id . '-40x40.jpg');
            }
            Image::make(Input::file('image'))->fit(300, 100)->save('uploads/images/Categories/' . $category->id . '-300x100.jpg');
            Image::make(Input::file('image'))->fit(600, 200)->save('uploads/images/Categories/' . $category->id . '-600x200.jpg');
            Image::make(Input::file('image'))->fit(40, 40)->save('uploads/images/Categories/' . $category->id . '-40x40.jpg');
            $image = 'true';
        }
        $category->image = $image;
        $category->thumbnail = $input['thumbnail'];
        $category->save();

        return $this->edit($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $node = $this->category->find($id);
        $node->delete();

        DB::table('categories_lang')->where('categories_id', '=', $id)->delete();
    }

    public function changePosition()
    {
        $inputs = Input::all();
        $thisNode = $this->category->find($inputs['sourceId']);
        if (isset($inputs['prevId'])) {
            $prevNode = $this->category->find($inputs['prevId']);
            $thisNode->moveToRightOf($prevNode);
        } elseif (isset($inputs['nextId'])) {
            $nextNode = $this->category->find($inputs['nextId']);
            $thisNode->moveToLeftOf($nextNode);
        } else {
            $parentNode = $this->category->find(Input::get('parentId'));
            $thisNode->makeChildOf($parentNode);
        }

        return "TRUE";
    }

}
