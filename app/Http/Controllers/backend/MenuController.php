<?php namespace App\Http\Controllers\backend;

use App\Language;
use App\Menu;
use App\MenuGroup;
use App\MenuLang;
use App\Providers\Helpers\Menus\MenusHelper;
use App\Providers\Helpers\Category\CategoryHelper;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Intervention\Image\Facades\Image;

class MenuController extends BackendController
{

    public $menu;
    public $locale;
    public $langActive;
    public $language;
    public $listLang;

    public function __construct(Menu $menu, Language $language)
    {
        $this->menu = $menu;
        $this->listLang = Config::get('app.locales');
        $this->langActive = app('getLocale');
        $this->language = $language;
        $this->locale = app('locale') == '' ? '' : app('locale') . '.';
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $roots = $this->menu->roots()
            ->join('menus_lang', 'menus.id', '=', 'menus_lang.menus_id')
            ->where('menus_lang.lang_id', '=', app('activeLangDetails')['lang_id'])
            ->get();
        $translated = MenuLang::where('lang_id', app('activeLangDetails')['lang_id'])->get();
        $translatedLang = [];
        foreach ($translated as $item) {
            $translatedLang[$item->menus_id] = [
                'name' => $item->name
                , 'desc' => $item->desc
            ];
        }


        return view('backend.Menu.index')->with('roots', $roots)->with('locale', $this->locale)->with('translatedName', $translatedLang);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $grandpaId = Input::get('grandpa_id') != NULL ? Input::get('grandpa_id') : NULL;
        $parentId = Input::get('parent_id') != NULL ? Input::get('parent_id') : NULL;
        $rootId = Input::get('root_id') != NULL ? Input::get('root_id') : NULL;
        if (Input::get('alias') != NULL) {
            $alias = Input::get('alias');
        } else {
            $alias = CategoryHelper::createAlias(Input::get('newCat'));
        }

        $newRoot = MenuLang::join('menus', 'menus.id', '=', 'menus_lang.menus_id')
            ->where('menus_lang.name', '=', Input::get('newCat'))
            ->where('menus.root_id', '=', $rootId)
            ->first();

        if (!isset($newRoot) && $parentId == NULL) {
            $lastRoot = Menu::roots()->max('root_id');
            $menu = Menu::create(['root_id' => $lastRoot + 1, 'alias' => $alias]);

            // Create Translate Named For Each Language That Setup For Application
            foreach ($this->listLang as $key => $value) {
                $lang = $this->language->getLanguage($key);
                MenuLang::saveLang(['menus_id' => $menu->id, 'lang_id' => $lang['lang_id'], 'name' => Input::get('newCat')]);
            }

            return Redirect::route($this->locale . 'backend.menu.index');

        } elseif (!isset($newRoot) && $parentId != NULL) {
            $newRoot = Menu::find($parentId);
            $child = $newRoot->children()->create(['root_id' => $rootId, 'alias' => $alias]);

            // Create Translate Named For Each Language That Setup For Application
            foreach ($this->listLang as $key => $value) {
                $lang = $this->language->getLanguage($key);
                MenuLang::saveLang(['menus_id' => $child->id, 'lang_id' => $lang['lang_id'], 'name' => Input::get('newCat')]);
            }
            return $child;

        }
        return ['action' => false, 'message' => 'this menu is already!'];
//        return Redirect::route($this->locale.'backend.menu.show');
    }

    /**
     * Display the specified resource.
     *
     * @param  string $urlAlias
     * @return Response
     */
    public function show($urlAlias)
    {
        $lang = $this->language->getLanguage($this->langActive);
        $catArr = explode("-", $urlAlias);
        $url = substr(Request::url(), 0, strrpos(Request::url(), '/') + 1);
        $myRoot = $this->menu->where('id', '=', $catArr[0])->first();
        $cats = $myRoot->getDescendantsAndSelf()->toHierarchy();
        $translated = MenuLang::where('lang_id', app('activeLangDetails')['lang_id'])->get();
        $translatedLang = [];
        foreach ($translated as $item) {
            $translatedLang[$item->menus_id] = [
                'name' => $item->name
                , 'desc' => $item->desc
            ];
        }

        return view('backend.Menu.subMenus')
            ->with('subMenus', $cats)
            ->with('myRoot', $myRoot)
            ->with('url', $url)
            ->with('locale', $this->locale)
            ->with('translatedName', $translatedLang);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $menu = $this->menu->where('id', $id)->join('menus_lang', 'id', '=', 'menus_id')->where('menus_lang.lang_id', '=', app('activeLangDetails')['lang_id'])->first();
        return view('backend.Menu.form', [
            'menu' => $menu,
            'locale' => $this->locale,
            'listLang' => $this->listLang
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $input = Input::all();
        $lang = $this->language->getLanguage($input['lang']);
        $input['lang_id'] = $lang['lang_id'];

        $paramsForTranslate = [
            'menus_id' => $id,
            'lang_id' => $input['lang_id'],
            'name' => $input['name'],
            'desc' => $input['desc']

        ];
        MenuLang::updateLang($paramsForTranslate);

        $menu = $this->menu->find($id);
        $menu->alias = Input::get('alias');
        $menu->url = Input::get('url');
        $image = $menu->image == 'true' ? 'true' : 'false';
        if (Request::hasFile('image')) {
            if (File::exists(base_path() . env('PUBLIC_PATH_URL', 'public') . '/uploads/images/Menus/' . $menu->id . '-300x100.jpg')) {
                File::delete($menu->id . '-300x100.jpg');
                File::delete($menu->id . '-600x200.jpg');
                File::delete($menu->id . '-40x40.jpg');
            }
            Image::make(Input::file('image'))->fit(300, 100)->save('uploads/images/Menus/' . $menu->id . '-300x100.jpg');
            Image::make(Input::file('image'))->fit(600, 200)->save('uploads/images/Menus/' . $menu->id . '-600x200.jpg');
            Image::make(Input::file('image'))->fit(40, 40)->save('uploads/images/Menus/' . $menu->id . '-40x40.jpg');
            $image = 'true';
        }
        $menu->image = $image;
        $menu->thumbnail = Input::get('thumbnail');
        $menu->save();

        return $this->edit($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $node = Menu::find($id);
        $node->delete();

        DB::table('menus_lang')->where('menus_id', '=', $id)->delete();
    }

    public function changePosition()
    {
        $inputs = Input::all();
        $thisNode = Menu::find($inputs['sourceId']);
        if (isset($inputs['prevId'])) {
            $prevNode = Menu::find($inputs['prevId']);
            $thisNode->moveToRightOf($prevNode);
        } elseif (isset($inputs['nextId'])) {
            $nextNode = Menu::find($inputs['nextId']);
            $thisNode->moveToLeftOf($nextNode);
        } else {
            $parentNode = Menu::find(Input::get('parentId'));
            $thisNode->makeChildOf($parentNode);
        }

        return "TRUE";
    }

}
