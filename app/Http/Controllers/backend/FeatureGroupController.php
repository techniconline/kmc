<?php
namespace App\Http\Controllers\backend;

use App\BaseModel;
use App\Feature;
use App\FeatureGroup;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Media;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\App;
use App\Language;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Laracasts\Flash\Flash;

class FeatureGroupController extends BackendController
{

    public $featureGroup;
    public $feature;
    public $locale;
    public $langActive;
    public $language;
    public $listLang;

    public function __construct(FeatureGroup $featureGroup, Language $language, Feature $feature)
    {
        $this->featureGroup = $featureGroup;
        $this->feature = $feature;
        $this->listLang = Config::get('app.locales');
        $this->langActive = app('getLocale');
        $this->language = $language;
        $this->locale = app('locale') == '' ? '' : app('locale') . '.';
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $lang = $this->language->getLanguage($this->langActive);
        $data = $this->featureGroup
            ->join('feature_group_lang', 'feature_group_lang.feature_group_id', '=', 'feature_group.id')
            ->where('feature_group.status', '=', 1)
            ->where('feature_group_lang.lang_id', '=', $lang['lang_id'])
            ->select([
                'feature_group_lang.name'
                , 'feature_group.*'
                , DB::raw('(SELECT GROUP_CONCAT(NAME) FROM features_lang
                    WHERE feature_id IN
                    (SELECT feature_id FROM feature_group_relation WHERE feature_group_id=feature_group.id)
                    AND lang_id=' . $lang['lang_id'] . ') as list_features')
            ])
            ->get();

        return view('backend.featureGroup.index')->withData($data)->with('locale', $this->locale);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.featureGroup.form', ['btnTitle' => Lang::get('backend/FeatureGroup.controller.Insert')
            , 'error' => '', 'title' => Lang::get('backend/FeatureGroup.controller.Create')])
            ->with('locale', $this->locale)->with('listLang', $this->listLang)
            ->with('setLang', app('getLocaleDefault'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = Input::all();
        $input['user_id'] = Auth::employee()->get()->id;
        $input['status'] = 1;
        $input['position'] = 1;
        $fill = $this->featureGroup->fill($input);
        $isValid = $fill->isValid();
        if (!$isValid) {

            Flash::error(Lang::get('backend/FeatureGroup.messages.errCreate'));
            $return = Redirect::back()->withInput()->withErrors($this->featureGroup->errors);
            return $return;
        }
        $this->featureGroup->save();

        if ($id = $this->featureGroup->id) {
            $params['table'] = 'feature_group_lang';
            foreach ($this->listLang as $key => $value) {
                $lang = $this->language->getLanguage($key);

                $params['items'] = ['feature_group_id' => $id, 'name' => $input['name'], 'lang_id' => $lang['lang_id']];
                $result = $this->featureGroup->saveItemLang($params);
            }

            Flash::success(Lang::get('backend/FeatureGroup.messages.msgInsertOk'));
            return Redirect::route($this->locale . 'backend.featureGroup.edit', array('feature_group_id' => $id))->with('locale', $this->locale);
        }

        Flash::error(Lang::get('backend/FeatureGroup.messages.errCreate'));
        return Redirect::back()->withErrors(Lang::get('backend/FeatureGroup.messages.errCreate'));

    }


    /**
     * Display the specified resource.
     *
     * @param $feature_group_id
     * @return Response
     */
    public function show($feature_group_id)
    {

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param $feature_group_id
     * @return Response
     */
    public function edit($feature_group_id)
    {
        $lang = $this->language->getLanguage($this->langActive);

        $data = $this->featureGroup
            ->join('feature_group_lang', 'feature_group_lang.feature_group_id', '=', 'feature_group.id')
            ->where('feature_group.id', '=', $feature_group_id)
            ->where('feature_group.status', '=', 1)
            ->where('feature_group_lang.lang_id', '=', $lang['lang_id'])
            ->select(['feature_group_lang.name', 'feature_group.*'])
            ->get()->first();

        $dataFeature = $this->feature
            ->join('features_lang', 'features_lang.feature_id', '=', 'features.id')
            ->where('features.status', '=', 1)
            ->where('features_lang.lang_id', '=', $lang['lang_id'])
            ->whereNotIn('features.id', DB::table('feature_group_relation')->where('feature_group_id', '!=', $feature_group_id)->lists('feature_id'))
            ->select(['features_lang.name', 'features.*'])
            ->get();

        $featureGroup = DB::table('feature_group_relation')->where('feature_group_id', '=', $feature_group_id)->lists('feature_id');

        $title = Lang::get('backend/FeatureGroup.controller.Edit');
        $btnTitle = Lang::get('backend/FeatureGroup.controller.Update');

        if (!$data) {
            $messages[] = ['level' => BaseModel::level_danger, 'message' => Lang::get('backend/FeatureGroup.messages.errValid')];
            $title = Lang::get('backend/FeatureGroup.controller.Create');
            $btnTitle = Lang::get('backend/FeatureGroup.controller.Insert');
        }

        if (isset($messages))
            Session::flash('messages', $messages);

        return view('backend.featureGroup.form', [
            'data' => $data
            , 'dataFeature' => $dataFeature
            , 'featureGroup' => $featureGroup
            , 'title' => $title
            , 'btnTitle' => $btnTitle
        ])->with('locale', $this->locale)
            ->with('listLang', $this->listLang)
            ->with('setLang', $this->langActive);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param $feature_group_id
     * @return Response
     */
    public function update($feature_group_id)
    {
        $messages = [];
        $input = Input::all();
        $lang = $this->language->getLanguage($input['lang']);
        $params['lang_id'] = $lang['lang_id'];
        $input['user_id'] = Auth::employee()->get()->id;
        $input['position'] = 1;

        $feature = $this->featureGroup->find($feature_group_id);

        $fill = $feature->fill($input);
        $isValid = $fill->isValid();
        if (!$isValid) {
            Flash::error(Lang::get('backend/FeatureGroup.messages.errValid'));
            $return = Redirect::back()->withInput()->withErrors($this->featureGroup->errors);
            return $return;
        }

        if (!$feature->save()) {
            Flash::error(Lang::get('backend/FeatureGroup.messages.msgUpdateNotOk'));
            $return = Redirect::back()->withInput();
            return $return;
        }

        $params['id'] = $feature_group_id;
        $params['table'] = 'feature_group_lang';
        $params['name_id_field'] = 'feature_group_id';
        $params['items'] = ['name' => $input['name']];

        $result = $this->featureGroup->updateItemLang($params);
        if (!$result) {
            $messages[] = ['level' => BaseModel::level_warning, 'message' => Lang::get('backend/FeatureGroup.messages.msgUpdateNotOkWarningLang')];
        }

        if (isset($input['features']) && $input['features']) {
            $this->featureGroup->changeFeaturesGroup($input['features'], $feature_group_id);
        }

        $messages[] = ['level' => BaseModel::level_success, 'message' => Lang::get('backend/FeatureGroup.messages.msgUpdateOk')];
        Session::flash('messages', $messages);
        return Redirect::back();
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $product_id
     * @return Response
     */
    public function destroy($feature_group_id)
    {
        $result = $this->featureGroup->del($feature_group_id);

        return ($result);
    }


}
