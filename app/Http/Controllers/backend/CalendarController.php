<?php namespace App\Http\Controllers\backend;

use App\Calendar;
use App\ContentLang;
use App\Param;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Intervention\Image\Facades\Image;
use Laracasts\Flash\Flash;

class CalendarController extends BackendController
{

    public $calendar;
    public $locale;

    public function __construct(Calendar $calendar)
    {
        $this->calendar = $calendar;
        $this->locale = app('locale') == '' ? '' : app('locale') . '.';
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $calendars = $this->calendar->where('employee_id', Auth::employee()->get()->id)->get();
        return view('backend.dashboard.index', [
            'reminders' => $calendars,
            'locale' => $this->locale
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.param.form', [
            'locale' => $this->locale,
            'types' => $this->types
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = Input::all();

        $input['employee_id'] = Auth::employee()->get()->id;

        $input['start'] .= ' ';
        $input['start'] .= ($input['startTime'] == '') ? '00' : $input['startTime'];
        $input['start'] .= ':00:00';

        $input['end'] .= ' ';
        $input['end'] .= ($input['endTime'] == '') ? '00' : $input['endTime'];
        $input['end'] .= ':00:00';

        $input['all_day'] = isset($input['all_day']) ? $input['all_day'] : 0;

        unset($input['startTime']);
        unset($input['endTime']);

        if (!$this->calendar->fill($input)->isValid()) {
            Flash::error(trans('backend/Calendar.messages.fillError'));
            return redirect()->back()->withInput()->withErrors($this->calendar->errors);
        }
        $this->calendar->save();

        Flash::success(trans('backend/Calendar.messages.success'));
        return Redirect::to('/backend');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $calendar = $this->calendar->find($id);
        return view('backend.param.form', [
            'calendar' => $calendar,
            'locale' => $this->locale,
            'types' => $this->types
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $calendar = $this->calendar->find($id);
        $input = Input::all();

        if (!$calendar->fill($input)->isValid()) {
            Flash::error(trans('backend/Param.messages.fillError'));
            return redirect()->back()->withInput()->withErrors($calendar->errors);
        }
        $calendar->save();

        Flash::success(trans('backend/Param.messages.successUpd'));
        return Redirect::back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $calendar = $this->calendar->find($id);
        $calendar->delete();

        Flash::success(trans('backend/Param.messages.successDel'));
        return Redirect::to('/backend');
    }

}
