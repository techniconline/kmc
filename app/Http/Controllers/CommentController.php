<?php namespace App\Http\Controllers;

use App\Comments;
use App\Language;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;

class CommentController extends Controller
{

    public $comment;

    public $locale;
    public $langActive;
    public $language;
    public $listLang;
    public $user_id;

    public function __construct(Comments $comments, Language $language)
    {
        $this->comment = $comments;
        $this->listLang = Config::get('app.locales');
        $this->langActive = app('getLocale');
        $this->language = $language;
        $this->locale = app('locale') == '' ? '' : app('locale') . '.';
        $this->user_id = isset(Auth::user()->get()->id) ? Auth::user()->get()->id : 0;

    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $inputs = Input::all();
        $result = $this->comment->saveComment($inputs);

        return $result;
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $inputs = Input::all();
        $dataComment = $this->comment->find($id);
        if($dataComment->user_id!=$this->user_id){
            return ['action' => false, 'message' => Lang::get('comments.messages.errNotValidData')];
        }
        $fill = $dataComment->fill($inputs);
        $isValid = $fill->validationData($inputs,$this->comment->rulesEdit);
        if(!$isValid){
            return ['action' => false, 'message' => Lang::get('comments.messages.msgUpdateNotOk')];
        }
        $dataComment->save();
        return ['action' => true, 'message' => Lang::get('comments.messages.msgUpdateOk')];
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $result = $this->comment->find($id);
        $result->status = 0;
        $res = $result->save();
        if ($res) {
            $resReply = $this->comment->where('parent_id', $id)->update(['status' => 0]);
        }

        if (!$res) {
            $action = false;
            $message = Lang::get('comments.messages.msgDelNotOk');
        } else {
            $action = true;
            $message = Lang::get('comments.messages.msgDelOk');
        }

        $return = ['action' => $action, 'message' => $message];
        return $return;

    }


}
