<?php namespace App\Http\Controllers;

use App\Category;
use App\Comments;
use App\Content;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Providers\Helpers\Category\CategoryHelper;
use Illuminate\Http\Request;
use App\Language;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use App\Media;
use App\RoomCategory;
use Illuminate\Support\Facades\Auth;

class ContentController extends Controller
{

    public $locale;
    public $content;
    public $comment;
    public $category;

    public function __construct(Content $content, Comments $comments, Category $category)
    {
        $this->content = $content;
        $this->comment = $comments;
        $this->category = $category;
        $this->locale = app('locale') == '' ? '' : app('locale') . '.';
    }

    /**
     * Display the specified resource.
     *
     * @param  string $alias
     * @return Response
     */
    public function show($alias)
    {
        $results = $this->category->getDataCategory(3);
        $roots = $results['roots'];
        $translatedName = $results['translatedLang'];
        $content = $this->content->selectJoinLang(['url' => $alias])
//            ->where('type','!=','static')
            ->where('status', 1)
            ->first();
        if (!$content)
            abort(404);

        $this->visitItem(['system' => 'content', 'item_id' => $content->id]);//visit log

        $commentsData = $this->comment->getComments('news', $content->id)->paginate(20);
        $comments = $this->comment->getCommentsDetails($commentsData,'news', $content->id);


        return view('content.index', [
            'content' => $content,
            'comments' => $comments,
            'system' => 'content',
        ])->with('locale', $this->locale)
            ->with('roots', $roots)
            ->with('translatedName', $translatedName);
    }
}
