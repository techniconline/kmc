<?php namespace App\Http\Controllers;

use App\Comments;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Providers\Helpers\Category\CategoryHelper;
use Illuminate\Http\Request;
use App\Language;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use App\Product;
use App\Media;
use App\RoomCategory;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class ProductController extends Controller
{

    public $locale;
    public $langActive;
    public $language;
    public $listLang;

    public $products;
    public $comment;
    public $user_id;

    public function __construct(Language $language, Product $products, Comments $comments)
    {

        $this->products = $products;
        $this->comment = $comments;
        $this->listLang = Config::get('app.locales');
        $this->langActive = app('getLocale');
        $this->language = $language;
        $this->locale = app('locale') == '' ? '' : app('locale') . '.';
        $this->user_id = isset(Auth::user()->get()->id) ? Auth::user()->get()->id : 0;

    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return Redirect::route($this->locale . 'category.index')->with('locale', $this->locale);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param $alias
     * @return Response
     * @internal param int $id
     */
    public function show($alias)
    {
        $lang = $this->language->getLanguage($this->langActive);

        $data = $this->products
            ->join('products_lang', 'products_lang.id', '=', 'products.id')
            ->leftjoin('product_media', function ($leftJoin) {
                $leftJoin->on('products.id', '=', 'product_media.product_id');
                $leftJoin->where('product_media.is_default', '=', 1);
                $leftJoin->where('product_media.status', '=', 1);
            })
            ->leftjoin('media', function ($leftJoinM) {
                $leftJoinM->on('product_media.media_id', '=', 'media.id');
                $leftJoinM->where('media.status', '=', 1);
            })
            ->leftjoin('categories', function ($leftJoinC) {
                $leftJoinC->on('products.category_id', '=', 'categories.id');
            })
            ->leftjoin('categories_lang', function ($leftJoinCl) use ($lang) {
                $leftJoinCl->on('categories.id', '=', 'categories_lang.categories_id');
                $leftJoinCl->where('categories_lang.lang_id', '=', $lang['lang_id']);
            })
            ->where('products_lang.alias', '=', $alias)
            ->where('products.status', '=', 1)
            ->where('products_lang.lang_id', '=', $lang['lang_id'])
            ->select(['products_lang.title'
                , 'products_lang.short_description'
                , 'products_lang.description', 'products.*'
                , 'media.src', 'product_media.feature_value_selected'
                , 'categories.alias as category_alias'
                , 'categories_lang.name as category_name'
            ])
            ->first();
        if (!$data) {
            return abort(404);
        }
        $product_id = $data->id;

        $this->visitItem(['system' => 'product', 'item_id' => $product_id]);//visit log

        $mediaModel = new Media();
        $images = $mediaModel->getMediaProduct('image', $product_id);
        $videos = $mediaModel->getMediaProduct('video', $product_id);

        $dataCategories = '';
//        $dataCategories = $this->products->getProductCategoriesDetail($product_id,$this->user_id);
        $commentsData = $this->comment->getComments('product', $product_id)->paginate(20);
        $comments = $this->comment->getCommentsDetails($commentsData,'product', $product_id);

        $features = $this->products->getFeaturesProduct($product_id, $lang['lang_id']);

        $new_features = [];
        $new_feature_color = [];
        $count = 0;
        foreach ($features as $item) {
            $new_features[$item->feature_id]['name_group'] = $item->name_group;
            $new_features[$item->feature_id]['id'] = $item->id;
            $new_features[$item->feature_id]['feature_id'] = $item->feature_id;
            $new_features[$item->feature_id]['name'] = $item->name;
            $new_features[$item->feature_id]['type'] = $item->type;
            $new_features[$item->feature_id]['feature_values'][] = $item->title ? $item->title . ': ' . ($item->type == 'color' ? '<span style="color:' . $item->value . '"><i class="fa fa-square"></i></span>' : $item->value) : $item->value;

            if ($item->type == 'color') {

                $new_feature_color[] = ['title' => $item->title, 'value' => $item->value];

            }

            $count++;
        }

//        dd($new_feature_color);

        return view('product.partials.info')
            ->with('locale', $this->locale)
            ->with(['comments' => $comments
                , 'dataProduct' => $data
                , 'features' => $new_features
                , 'feature_color' => $new_feature_color
                , 'imageProduct' => $images
                , 'videoProduct' => $videos
                , 'system' => 'product'])
            ->with('dataCategories', $dataCategories);


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

}
