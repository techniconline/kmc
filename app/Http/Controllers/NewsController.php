<?php namespace App\Http\Controllers;

use App\Category;
use App\Content;
use App\Http\Requests;
use App\Comments;
use App\Http\Controllers\Controller;

use App\Providers\Helpers\Category\CategoryHelper;
use Illuminate\Http\Request;
use App\Language;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;
use App\Media;
use App\RoomCategory;
use Illuminate\Support\Facades\Auth;
use App\Providers\Helpers\PersianDate\PersianDate;
use Illuminate\Support\Facades\Redirect;
use kcfinder\text;

class NewsController extends Controller
{

    public $locale;
    public $content;
    public $comment;
    public $category;

    public function __construct(Content $content, Comments $comments, Category $category)
    {
        $this->content = $content;
        $this->comment = $comments;
        $this->category = $category;
        $this->locale = app('locale') == '' ? '' : app('locale') . '.';
    }

    public function index($tag = null, $category = null, $text = null)
    {
        $inputs = Input::all();
        $category = isset($inputs['type']) ? $inputs['type'] : $category;

        $results = $this->category->getDataCategory(3);
        $roots = $results['roots'];
        $translatedName = $results['translatedLang'];
        $langId = app('activeLangDetails')['lang_id'];

        $news = $this->content
            ->join('contents_lang', 'contents_id', '=', 'id')
            ->where('contents_lang.lang_id', '=', $langId)
//            ->selectJoinLang(['type' => 'news'])
            ->where(function ($query) {
                if (!isset(Auth::user()->get()->id)) {
                    $query->where('login', 0);
                }
            })
            ->leftJoin('employee', function ($joinL1) {
                $joinL1->on('contents.employee_id', '=', 'employee.id');
                $joinL1->where('contents.employee_id', '>', 0);
            })
            ->leftJoin('users', function ($joinL1) {
                $joinL1->on('contents.author_id', '=', 'users.id');
                $joinL1->where('contents.employee_id', '=', 0);
            })
            ->leftJoin('content_categories', 'contents.id', '=', 'content_categories.content_id')
            ->leftJoin('categories', 'categories.id', '=', 'content_categories.categories_id')
//            ->leftJoin('categories_lang','categories.id','=','categories_lang.categories_id')
            ->leftJoin('categories_lang', function ($joinL) use ($langId) {
                $joinL->on('categories.id', '=', 'categories_lang.categories_id');
                $joinL->where('categories_lang.lang_id', '=', $langId);
            })
            ->select([
                DB::raw('(IF(contents.employee_id>0,CONCAT_WS(" ",employee.firstname,employee.lastname),CONCAT_WS(" ",users.firstname,users.lastname))) AS user_name')
                , DB::raw('(SELECT COUNT(id) FROM comments WHERE item_id=contents.id AND status=1) AS count_comments ')
                , 'categories_lang.name as name_category'
                , 'contents.*'
                , 'contents_lang.*'])
            ->where('contents.status', 1)
            ->whereIn('contents.type', ['blog', 'weblog', 'news'])
            ->where(function ($query) use ($tag, $category, $text) {
                if ($tag) {
                    $query->where('tags', 'LIKE', '%' . $tag . '%');
                }
                if ($category) {
                    $query->where('categories.alias', $category);
                }
                if (!$text) {
                    $query->where('type', 'news');
                } else {
                    $query->orWhere('link_title', 'LIKE', '%' . $text . '%');
                    $query->orWhere('browser_title', 'LIKE', '%' . $text . '%');
                    $query->orWhere('body_title', 'LIKE', '%' . $text . '%');
                    $query->orWhere('short_content', 'LIKE', '%' . $text . '%');
                    $query->orWhere('body_content', 'LIKE', '%' . $text . '%');
                    $query->orWhere('tags', 'LIKE', '%' . $text . '%"');
                }
            })
            ->orderBy('contents.id', 'DESC')
//            ->distinct()
            ->paginate(10);

        foreach ($news as &$item) {

            $item->pDayName = PersianDate::$pdateWeekNameWithKey[strtolower(date("D", strtotime($item->created_at)))];
            $item->pDate = PersianDate::convert_date_M_to_H(date("Y-m-d", strtotime($item->created_at)));
            $arrDate = explode("-", $item->pDate);
            $item->pMonthName = PersianDate::$pdateMonthName[(int)$arrDate[1]];
            $item->pMonth = $arrDate[1];
            $item->pDay = $arrDate[2];
            $item->pYear = $arrDate[0];

        }


        return view('news.index', [
            'news' => $news,
            'locale' => $this->locale
        ])->with('roots', $roots)
            ->with('systemContent', 'news')
            ->with('translatedName', $translatedName);
    }

    /**
     * Display the specified resource.
     *
     * @param  string $alias
     * @return Response
     */
    public function show($alias)
    {
        $results = $this->category->getDataCategory(3);
        $roots = $results['roots'];
        $translatedName = $results['translatedLang'];

        $content = $this->content->selectJoinLang(['url' => $alias])
//            ->where('type','news')
            ->where('contents.status', 1)
            ->join('employee', 'employee.id', '=', 'contents.author_id')
            ->select(['employee.firstname', 'employee.lastname', 'contents.*', 'contents_lang.*'])->first();
        if (!$content)
            abort(404);
        if ($content->login == 1 && !isset(Auth::user()->get()->id))
            abort(503);

        $this->visitItem(['system' => 'content', 'item_id' => $content->id]);//visit log

        $content->pDayName = PersianDate::$pdateWeekNameWithKey[strtolower(date("D", strtotime($content->created_at)))];
        $content->pDate = PersianDate::convert_date_M_to_H(date("Y-m-d", strtotime($content->created_at)));
        $arrDate = explode("-", $content->pDate);
        $content->pMonthName = PersianDate::$pdateMonthName[(int)$arrDate[1]];
        $content->pMonth = $arrDate[1];
        $content->pDay = $arrDate[2];
        $content->pYear = $arrDate[0];

        $commentsData = $this->comment->getComments('news', $content->id)->paginate(20);
        $comments = $this->comment->getCommentsDetails($commentsData,'news', $content->id);

        return view('content.index', [
            'content' => $content,
            'comments' => $comments,
            'locale' => $this->locale
        ])->with('roots', $roots)
            ->with('systemContent', 'news')
            ->with('system', 'news')
            ->with('translatedName', $translatedName);
    }



    public function searchTags($tag)
    {
        return $this->index($tag);
    }

    public function searchCategory($category)
    {
        return $this->index(null, $category);
    }

    public function searchContentText()
    {
        $input = Input::all();

        $text = isset($input['searchText']) ? $input['searchText'] : null;

        if (!$text)
            return Redirect::back();

        return Redirect::route('searchContent', ['text' => $text]);
    }

    public function searchText($text)
    {
        return $this->index(null, null, $text);
    }
}
