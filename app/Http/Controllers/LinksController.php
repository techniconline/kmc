<?php namespace App\Http\Controllers;

use App\Language;
use App\Link;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Intervention\Image\Facades\Image;

class LinksController extends Controller
{

    public $link;

    public function __construct(Link $link, Language $language)
    {
        $this->link = $link;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $links = $this->link->all();
        return view('links.index', [
            'links' => $links
        ]);
    }

}
