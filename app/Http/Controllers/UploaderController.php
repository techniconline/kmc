<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use App\Media;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Lang;

class UploaderController extends Controller
{

    public $publicUrl;
    public $user;
    public $employee;

    public function __construct()
    {
//        $this->publicUrl = base_path().'/public';
        $this->publicUrl = base_path() . env('PUBLIC_PATH_URL', 'public');
        $this->user =  Auth::user()->get();
        $this->employee =  Auth::employee()->get();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function uploadFile()
    {
        $base_storage = $this->publicUrl;
        $inputs = Input::all();
        $type = isset($inputs['type'])&&$inputs['type']?$inputs['type']:'images';
        $mediaFolder = $type;
        $subFolder = isset($inputs['sf'])&&$inputs['sf']?$inputs['sf']:null;

        $params=[];
        $params['ctrl'] = $controller = isset($inputs['ctrl'])&&$inputs['ctrl']?$inputs['ctrl']:null;
        $params['ctrlDoc'] = $controllerDoc = isset($inputs['ctrlDoc'])&&$inputs['ctrlDoc']?$inputs['ctrlDoc']:null;
        $params['itemId'] = $itemId = isset($inputs['itemId'])&&$inputs['itemId']?$inputs['itemId']:null;
        $params['prefix'] = $prefix = isset($inputs['prefix'])&&$inputs['prefix']?$inputs['prefix']:null;

        $uploadTarget = "uploads/" . $mediaFolder . "/" . $subFolder . "/";
        if(!file_exists($base_storage."/uploads/" . $mediaFolder )){
            mkdir($base_storage."/uploads/" . $mediaFolder);
        }
        if(!file_exists($base_storage."/uploads/" . $mediaFolder. "/" . $subFolder )){
            mkdir($base_storage."/uploads/" . $mediaFolder. "/" . $subFolder);
        }
        $output_dir = $base_storage . "/" . $uploadTarget;
        if (isset($_FILES["myfile"])) {
            $ret = array();
            $error = $_FILES["myfile"]["error"];

            if (!is_array($_FILES["myfile"]["name"])) //single file
            {
                $fileName = $_FILES["myfile"]["name"];
                $isValidExt = $this->validExt($type, $fileName);
                if ($isValidExt['valid']) {

                    $resMediaFolder = $this->createFolder($base_storage . "/uploads/", $isValidExt['mediaFolder']);
                    $resSubFolder = $this->createFolder($resMediaFolder['folder'], $subFolder);
                    $output_dir = $resSubFolder['folder'];
                    $uploadTarget = "uploads/" . $isValidExt['mediaFolder'] . "/" . $subFolder . "/";

                    $resultDir = $this->createFileName($fileName, $output_dir);
                    move_uploaded_file($_FILES["myfile"]["tmp_name"], $resultDir['folder'] . $resultDir['fileName']);
                    $ret['src'] = $data['src'] = $uploadTarget . $resultDir["uploadFolder"] . '/' . $resultDir["fileName"];
                    $mediaModel = new Media();
                    $resultMedia = $mediaModel->saveMedia($data, $isValidExt['type']);
                    $params['media_id'] = $ret['id'] = $resultMedia;
                    $params['media_title'] = $controller.'-'.$itemId.'-'.$resultMedia;
                    $ret['msg'] = isset($resultMedia) ? 'success upload file.' : 'no success upload file! ';
                    $ret['valid'] = isset($resultMedia) ? true : false;
                } else {
                    $ret['valid'] = false;
                    $ret['msg'] = 'this file is not valid!';
                }
            } else  //Multiple files, file[]
            {
                $fileCount = count($_FILES["myfile"]["name"]);
                for ($i = 0; $i < $fileCount; $i++) {
                    $fileName = $_FILES["myfile"]["name"][$i];
                    $isValidExt = $this->validExt($type, $fileName);
                    if ($isValidExt['valid']) {

                        $resMediaFolder = $this->createFolder($base_storage . "/uploads/", $isValidExt['mediaFolder']);
                        $resSubFolder = $this->createFolder($resMediaFolder['folder'], $subFolder);
                        $output_dir = $resSubFolder['folder'];
                        $uploadTarget = "uploads/" . $isValidExt['mediaFolder'] . "/" . $subFolder . "/";

                        $resultDir = $this->createFileName($fileName, $output_dir);
                        move_uploaded_file($_FILES["myfile"]["tmp_name"][$i], $resultDir['folder'] . $resultDir['fileName']);
                        $ret['src'] = $data['src'] = $uploadTarget . $resultDir["uploadFolder"] . '/' . $resultDir["fileName"];
                        $mediaModel = new Media();
                        $resultMedia = $mediaModel->saveMedia($data, $isValidExt['type']);
                        $params['media_ids'][$i] = $ret['id'] = $resultMedia;
                        $params['media_titles'][$i] = $controller.'-'.$itemId.'-'.$resultMedia;

                        $ret['msg'] = isset($resultMedia) ? 'success upload file.' : 'no success upload file! ';
                        $ret['valid'] = isset($resultMedia) ? true : false;
                    } else {
                        $ret['valid'] = false;
                        $ret['msg'] = 'this file is not valid!';
                    }
                }

            }

            if($ret['valid']&&$params['ctrl']&&$params['itemId']){
                $resultCtrl = $this->callSaveController($params);
            }
            return json_encode($ret);
        }
    }

    public function callDelController($params){

        $params['user_id']=0;
        $params['employee_id']=0;
        if($params['prefix']=='frontend'){
            $params['user_id'] = isset($this->user->id)?$this->user->id:0;
        }elseif($params['prefix']=='backend'){
            $params['employee_id'] = isset($this->employee->id)?$this->employee->id:0;
        }

        $model = "App\\".ucfirst($params['ctrl']);
        $objModel = new $model();
        $result = $objModel->deleteMediaUpload($params);
        return $result;
    }

    public function callSaveController($params){

        $params['user_id']=0;
        $params['employee_id']=0;
        if($params['prefix']=='frontend'){
            $params['user_id'] = isset($this->user->id)?$this->user->id:0;
        }elseif($params['prefix']=='backend'){
            $params['employee_id'] = isset($this->employee->id)?$this->employee->id:0;
        }

        $model = "App\\".ucfirst($params['ctrl']);
        $objModel = new $model();
        $result = $objModel->saveMediaUpload($params);
        return $result;
    }

    public function validExt($type = "images", $fileName = "")
    {
        $typ = '';
        if ($type == "images") {
            $extArr = array('jpg', 'gif', 'bmp', 'png', 'jpeg');
            $typ = 'image';
        } elseif ($type == "videos") {
            $extArr = array('mov', 'flv', 'wma', 'avi', 'mpeg', 'mp4');
            $typ = 'video';
        } elseif ($type == 'images-docs-zip') {
            $extArr = array('jpg', 'gif', 'bmp', 'png', 'jpeg', 'pdf', 'zip', 'rar', 'doc', 'docx');
            $typ = 'image';
        } elseif ($type == 'images-docs') {
            $extArr = array('jpg', 'gif', 'bmp', 'png', 'jpeg', 'doc', 'docx', 'xls', 'xlsx', 'pdf');
            $typ = 'image';
        } elseif ($type == 'docs') {
            $extArr = array('doc', 'docx', 'xls', 'xlsx', 'pdf');
            $typ = 'doc';
        } else {
            return array('valid' => 0, 'type' => $typ);
        }

        if (!$fileName)
            return array('valid' => 0, 'type' => $typ);

        $fileArr = explode(".", $fileName);
        $ext = end($fileArr);

        if (in_array($ext, $extArr) && $type == 'images-docs') {
            $extArrImg = array('jpg', 'gif', 'bmp', 'png', 'jpeg');
            $extArrDoc = array('doc', 'docx', 'xls', 'xlsx', 'pdf');

            if (in_array($ext, $extArrImg)) {
                $typ = 'image';
                return array('valid' => true, 'type' => $typ, 'mediaFolder' => 'images');
            } else {
                $typ = 'doc';
                return array('valid' => true, 'type' => $typ, 'mediaFolder' => 'docs');
            }

        } elseif (in_array($ext, $extArr) && $type == 'images-docs-zip') {
            $extArrImg = array('jpg', 'gif', 'bmp', 'png', 'jpeg');
            $extArrDoc = array('doc', 'docx', 'xls', 'xlsx', 'pdf', 'zip', 'rar');

            if (in_array($ext, $extArrImg)) {
                $typ = 'image';
                return array('valid' => true, 'type' => $typ, 'mediaFolder' => 'images');
            } else {
                $typ = 'doc';
                return array('valid' => true, 'type' => $typ, 'mediaFolder' => 'docs');
            }

        } elseif (in_array($ext, $extArr)) {
            return array('valid' => true, 'type' => $typ, 'mediaFolder' => $type);
        }

        return array('valid' => false, 'type' => $typ);

    }

    public function createFileName($fileName, $folderLocation = '')
    {

        if (!$fileName)
            return 0;

        $fileArr = explode(".", $fileName);
        $ext = end($fileArr);
        $file = '';
        foreach ($fileArr as $item) {
            $file .= $item;
        }
        $Folder = md5(mt_rand());
        $createFolder = $folderLocation . $Folder . '/';
        mkdir($createFolder, 0755);
        return array('fileName' => 'original.' . $ext, 'ext' => $ext, 'folder' => $createFolder, 'uploadFolder' => $Folder);


    }

    public function createFolder($folderLocation, $folderName)
    {
        $Folder = $folderName;
        $createFolder = $folderLocation . $Folder . '/';
        if (!file_exists($createFolder)) {
            mkdir($createFolder, 0755);
        }
        return ['valid' => true, 'folder' => $createFolder, 'uploadFolder' => $Folder];

    }

    public function deleteFile()
    {
        $output_dir = URL::to("uploads/");
        $inputs = Input::all();
        $base_storage = $this->publicUrl;
        $controller = isset($inputs["controller"]) ? ($inputs["controller"]) : '';
        $controllerDoc = isset($inputs["controllerDoc"]) ? ($inputs["controllerDoc"]) : '';
        if (isset($inputs["op"]) && $inputs["op"] == "delete" && isset($inputs['id']) && ($inputs['id'])) {
            $mediaModel = new Media();
            $mediaData = $mediaModel->find($inputs['id']);
            $filePath = $base_storage . "/" . $mediaData->src;
            $ret['message'] = $message = Lang::get('profile.messages.msgDelNotOk');
            $ret['icon'] = $icon = '<i class="fa fa-times font-red"></i>';
            $ret['msg'] = Lang::get('profile.messages.msgDelNotOk');
            $ret['valid'] = false;
            $ret['action'] = false;
            if (file_exists($filePath)) {
                $resultMedia = unlink($filePath);
                if ($resultMedia) {
                    $mediaData->status = 0;
                    $resultMedia = $mediaData->save();
                }
                $ret['message'] = $message = isset($resultMedia) && $resultMedia ? Lang::get('profile.messages.msgDelOk') : Lang::get('profile.messages.msgDelNotOk');
                $ret['icon'] = $icon = isset($resultMedia) && $resultMedia ? '<i class="fa fa-check font-green"></i>' : '<i class="fa fa-times font-red"></i>';
                $ret['msg'] = isset($resultMedia) ? Lang::get('profile.messages.msgDelOk') : Lang::get('profile.messages.msgDelNotOk');
                $ret['valid'] = isset($resultMedia) ? true : false;
                $ret['action'] = isset($resultMedia) ? true : false;
            } else {
                $mediaData->status = 0;
                $resultMedia = $mediaData->save();
                $ret['message'] = $message = isset($resultMedia) && $resultMedia ? Lang::get('profile.messages.msgDelOk') : Lang::get('profile.messages.msgDelNotOk');
                $ret['icon'] = $icon = isset($resultMedia) && $resultMedia ? '<i class="fa fa-warning font-yellow"></i>' : '<i class="fa fa-times font-red"></i>';
                $ret['msg'] = isset($resultMedia) && $resultMedia ? Lang::get('profile.messages.msgDelOk') : Lang::get('profile.messages.msgDelNotOk');
                $ret['valid'] = isset($resultMedia) && $resultMedia ? true : false;
                $ret['action'] = isset($resultMedia) && $resultMedia ? true : false;
            }


            $params=[];
            $params['ctrl'] = $controller = isset($inputs['controller'])&&$inputs['controller']?$inputs['controller']:null;
            $params['ctrlDoc'] = $controllerDoc = isset($inputs['controllerDoc'])&&$inputs['controllerDoc']?$inputs['controllerDoc']:null;
            $params['media_id'] = $itemId = isset($inputs['id'])&&$inputs['id']?$inputs['id']:null;
            $params['itemId'] = $itemId = isset($inputs['item_id'])&&$inputs['item_id']?$inputs['item_id']:null;
            $params['prefix'] = $itemId = isset($inputs['prefix'])&&$inputs['prefix']?$inputs['prefix']:null;
            $resultCtrl = $this->callDelController($params);
            $ret['validController'] = $this->deleteMediaFromAnotherController($inputs['id'], $controllerDoc);

            return json_encode($ret);
        }
    }

    public function deleteMediaFromAnotherController($mediaId, $controller = '')
    {

        if (!$controller || !$mediaId)
            return false;

        $result = false;
        if ($controller == 'attach') {

            $result = DB::table('attach_documents')
                ->where('media_id', '=', $mediaId)
                ->update(
                    [
                        'status' => 0
                        , 'updated_at' => date('Y-m-d H:i:s')
                    ]
                );
        }

        return $result;

    }


}
