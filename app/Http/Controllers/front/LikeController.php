<?php namespace App\Http\Controllers\front;

use App\Http\Requests;
use App\Likes;
use App\Media;
use App\Profile;
use App\Language;
use App\Providers\Helpers\PersianDate\PersianDate;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\front\FrontController;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\URL;
use App\UserParent;

use Illuminate\Support\Facades\View;
use App\MailModel;
use Intervention\Image\Facades\Image;
use Laracasts\Flash\Flash;

class LikeController extends FrontController
{
    public $profile;
    public $user;

    public $locale;
    public $langActive;
    public $language;
    public $listLang;
    public $user_id;

    public function __construct(User $user, Likes $likes, Language $language)
    {
        $this->like = $likes;
        $this->user = $user;
        $this->listLang = Config::get('app.locales');
        $this->langActive = app('getLocale');
        $this->language = $language;
        $this->locale = app('locale') == '' ? '' : app('locale') . '.';
        $this->user_id = isset(Auth::user()->get()->id) ? Auth::user()->get()->id : 0;
    }

    public function likeAction($system, $item_id, $status)
    {

        $likeData = $this->like->where('system', $system)
            ->where('item_id', $item_id)
            ->where('user_id', $this->user_id)
            ->first();

        $data['system'] = $system;
        $data['item_id'] = $item_id;
        $data['user_id'] = $this->user_id;
        $data['status'] = $status;
        if (!$likeData) {

            $fill = $this->like->fill($data);
            $valid = $fill->isValid();
            if (!$valid) {
                return ['action' => false, 'errors' => $this->like->errors, 'message' => Lang::get('profile.messages.errDate')];
            }
            if ($this->like->save()) {
                return ['action' => true, 'create' => true, 'valueAction' => $this->like->status, 'message' => Lang::get('profile.messages.msgCreate')];
            }
            return ['action' => false, 'errors' => '', 'message' => Lang::get('profile.messages.errCreate')];

        } elseif ($likeData->status != $status) {

            $fill = $likeData->fill($data);
            $valid = $likeData->isValid();
            if (!$valid) {
                return ['action' => false, 'errors' => $this->like->errors, 'message' => Lang::get('profile.messages.errDate')];
            }
            if ($likeData->save()) {
                return ['action' => true, 'create' => false, 'valueAction' => $this->like->status, 'message' => Lang::get('profile.messages.msgUpdateOk')];
            }
            return ['action' => false, 'errors' => '', 'message' => Lang::get('profile.messages.errCreate')];

        }

        return ['action' => false, 'message' => 'before liked!'];


    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }


    /**
     * @param $page
     */
    public function show($page)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

}
