<?php namespace App\Http\Controllers\front;

use App\Booking;
use App\RoomCategory;
use App\Language;
use Illuminate\Http\Request;
use App\Http\Controllers\front\FrontController;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;

class BookingController extends FrontController
{

    public $booking;
    public $roomCategory;

    public $locale;
    public $langActive;
    public $language;
    public $listLang;

    public function __construct(Booking $booking, Language $language, RoomCategory $roomCategory)
    {
        $this->booking = $booking;
        $this->roomCategory = $roomCategory;
        $this->listLang = Config::get('app.locales');
        $this->langActive = app('getLocale');
        $this->language = $language;
        $this->locale = app('locale') == '' ? '' : app('locale') . '.';

    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $inputs = Input::all();
        $user_id = $inputs['user_id'] = Auth::user()->get()->id;
        $inputs['from_date'] = date('Y-m-d');
        $inputs['to_date'] = date('Y-m-d', strtotime("+0 days"));;
        $inputs['status'] = 1;

        $message = Lang::get('profile.messages.err');
        $icon = '<i class="fa fa-times font-red"></i>';
        $action = false;
        $arcm_id = $inputs['apartment_room_category_map_id'] = $inputs['arcm_id'];
        $arcmData = $this->roomCategory->getRoomCategoryMapDetails($arcm_id);

        $bookingOld = $this->booking->where('status', '=', 1)->where('user_id', '=', $user_id)->orderBy('id', 'DESC')->get()->first();
        $modal_message = '';
        if (!$bookingOld) {
            //check booking active in duration date for this user

            $bookingActive = $this->booking
                ->join('booking_details', 'booking.id', '=', 'booking_details.booking_id')
                ->where('booking_details.status', '!=', '0')
                ->where('booking.status', '!=', 0)
                ->where('booking.to_date', '>=', date('Y-m-d'))
                ->where('booking.user_id', '=', $user_id)
                ->select('booking.*', 'booking_details.type')
                ->orderBy('booking.id', 'DESC')
                ->get();

            if ($bookingActive) {
                $typesArr = [];
                foreach ($bookingActive as $itemA) {
                    $typesArr[] = $itemA->type;

                    if ($arcmData->type == $itemA->type) {
                        $message = Lang::get('profile.messages.YouHaveBookingActive');
                        $icon = '<i class="fa fa-times font-red"></i>';
                        $action = false;
                        $return = ['action' => $action, 'icon' => $icon, 'message' => $message, 'modal_message' => $modal_message];
                        return $return;
                    }

                }
            }

            //-----------------------------------


            $fill = $this->booking->fill($inputs);
            $isValid = $fill->isValid();
            if (!$isValid) {
                $return = ['action' => $action, 'icon' => $icon, 'message' => $this->booking->errors];
                return $return;
            }
            $this->booking->save();

            if ($id = $this->booking->id) {
                $booking_id = $inputs['booking_id'] = $id;
                $priority = 1;
                $insertArray = [
                    'booking_id' => $booking_id
                    , 'apartment_room_category_map_id' => $arcmData->id
                    , 'room_category_id' => $arcmData->room_category_id
                    , 'type' => $arcmData->type
                    , 'user_id' => $user_id
                    , 'priority' => $priority
                    , 'rent_status' => 0
                    , 'status' => 1
                    , 'created_at' => date('Y-m-d H:i:s')
                ];
                $result = $this->booking->insertBookingDetails($insertArray);

                if ($result) {
                    if ($arcmData->type == 'room') {
                        $modal_message['your_selected'] = Lang::get('booking.messages.YourSelect') . ' <b> ' . (1) . ' </b> ' . Lang::get('booking.messages.Room');
                        $modal_message['your_choose'] = Lang::get('booking.messages.YouCanChoose') . ' <b> ' . (3 - 1) . ' </b> ' . Lang::get('booking.messages.Room');
                    } else {
                        $modal_message['your_selected'] = Lang::get('booking.messages.YourSelect') . ' <b> ' . (1) . ' </b> ' . Lang::get('booking.messages.Parking');
                        $modal_message['your_choose'] = Lang::get('booking.messages.YouCanChoose') . ' <b> ' . (1 - 1) . ' </b> ' . Lang::get('booking.messages.Parking');
                    }
                    $action = true;
                    $message = Lang::get('booking.messages.msgCreate');
                    $icon = '<i class="fa fa-check font-green"></i>';
                } else {
                    $action = false;
                    $message = Lang::get('booking.messages.err');
                    $icon = '<i class="fa fa-times font-red"></i>';
                }

                $return = ['action' => $action, 'icon' => $icon, 'message' => $message, 'modal_message' => $modal_message];
                return $return;
            }
        } else {
            //exist booking and update add booking details
            $booking_id = $inputs['booking_id'] = $bookingOld->id;
            $Count = $this->booking->getCountBookingDetails($booking_id, $arcmData->type);
            $priority = isset($Count) ? $Count + 1 : 1;

            $validBookingData = $this->booking->checkValidationBookingDetails($booking_id, $arcmData->id, $user_id);

            if (($priority > 3 && $arcmData->type == 'room')) {
                $message = Lang::get('booking.messages.msgMaxCreate');
                $icon = '<i class="fa fa-warning font-yellow"></i>';
                $action = false;
            } elseif (($priority > 1 && $arcmData->type == 'parking')) {
                $message = Lang::get('booking.messages.msgMaxCreateParking');
                $icon = '<i class="fa fa-warning font-yellow"></i>';
                $action = false;
            } elseif ($validBookingData) {
                $message = Lang::get('booking.messages.msgCreateDuplicate');
                $icon = '<i class="fa fa-warning font-yellow"></i>';
                $action = false;
            } else {
                $insertArray = [
                    'booking_id' => $booking_id
                    , 'apartment_room_category_map_id' => $arcmData->id
                    , 'room_category_id' => $arcmData->room_category_id
                    , 'type' => $arcmData->type
                    , 'user_id' => $user_id
                    , 'priority' => $priority
                    , 'rent_status' => 0
                    , 'status' => 1
                    , 'created_at' => date('Y-m-d H:i:s')
                ];
                $result = $this->booking->insertBookingDetails($insertArray);

                if ($result) {
                    $action = true;
                    if ($arcmData->type == 'room') {
                        $modal_message['your_selected'] = Lang::get('booking.messages.YourSelect') . ' <b> ' . ($priority) . ' </b> ' . Lang::get('booking.messages.Room');
                        $modal_message['your_choose'] = Lang::get('booking.messages.YouCanChoose') . ' <b> ' . (3 - $priority) . ' </b> ' . Lang::get('booking.messages.Room');
                    } else {
                        $modal_message['your_selected'] = Lang::get('booking.messages.YourSelect') . ' <b> ' . ($priority) . ' </b> ' . Lang::get('booking.messages.Parking');
                        $modal_message['your_choose'] = Lang::get('booking.messages.YouCanChoose') . ' <b> ' . (1 - $priority) . ' </b> ' . Lang::get('booking.messages.Parking');
                    }
                    $message = Lang::get('booking.messages.msgCreate');
                    $icon = '<i class="fa fa-check font-green"></i>';
                }


            }


        }


        $return = ['action' => $action, 'icon' => $icon, 'message' => $message, 'modal_message' => $modal_message];
        return $return;

    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $result = DB::table('booking_details')->where('id', '=', $id)->delete();

        if (!$result) {
            $action = false;
            $message = Lang::get('booking.messages.msgDelNotOk');
            $icon = '<i class="fa fa-times font-red"></i>';
        } else {
            $action = true;
            $message = Lang::get('booking.messages.msgDelOk');
            $icon = '<i class="fa fa-check font-green"></i>';
        }

        $return = ['action' => $action, 'icon' => $icon, 'message' => $message];
        return $return;

    }


}
