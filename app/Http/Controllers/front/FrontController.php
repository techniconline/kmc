<?php namespace App\Http\Controllers\front;

use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;

abstract class FrontController extends BaseController
{

    use DispatchesCommands, ValidatesRequests;

}
