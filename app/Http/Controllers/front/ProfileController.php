<?php namespace App\Http\Controllers\front;

use App\Http\Requests;
use App\Media;
use App\Profile;
use App\Language;
use App\Providers\Helpers\PersianDate\PersianDate;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\front\FrontController;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\URL;
use App\UserParent;

use Illuminate\Support\Facades\View;
use App\MailModel;
use Intervention\Image\Facades\Image;
use Laracasts\Flash\Flash;

class ProfileController extends FrontController
{
    public $profile;
    public $user;

    public $locale;
    public $langActive;
    public $language;
    public $listLang;
    public $user_id;

    public function __construct(User $user, Profile $profile, Language $language)
    {
        $this->profile = $profile;
        $this->user = $user;
        $this->listLang = Config::get('app.locales');
        $this->langActive = app('getLocale');
        $this->language = $language;
        $this->locale = app('locale') == '' ? '' : app('locale') . '.';
        $this->user_id = isset(Auth::user()->get()->id) ? Auth::user()->get()->id : 0;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $dataUser = Auth::user()->get();
        $age = 0;
        if ($dataUser->birth_date) {

            $age = date_diff(date_create($dataUser->birth_date), date_create('today'))->y;

        }

        $dataCountries = $this->profile->getCountries();
        $titlePage = Lang::get('profile.view.partials.user');

        return view('front.profile.index')
            ->with(['title' => $titlePage, 'data' => $dataUser
                , 'dataCountries' => $dataCountries, 'page' => 'user'])
            ->with('locale', $this->locale)
            ->with('age', $age);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }


    /**
     * @param $page
     */
    public function show($page)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $inputs = Input::all();
        $page = $inputs['page'];
        $user = $this->user->find($id);

        $inputs['birth_date'] = isset($inputs['birth_date']) && $inputs['birth_date'] ? PersianDate::convert_date_H_to_M($inputs['birth_date']) : '1970-03-22';

        $user->fill($inputs);

        if ($user->save()) {
            Flash::success(trans('profile.messages.msgUpdateOk'));
        } else {
            Flash::danger(trans('profile.messages.msgUpdateNotOk'));
        }

        return Redirect::back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function messagesUser()
    {

        $dataMessage = $this->profile->getMessagesUser($this->user_id);

        return view('front.profile.messagesUser')->with('locale', $this->locale)
            ->with([
                'title' => Lang::get('profile.view.messagesUser.ListMessagesUser')
                , 'dataMessage' => $dataMessage
            ]);

    }

    public function showMessage($id)
    {

        $data = $this->profile->getMessage($id);
        $Assessor = Lang::get('profile.view.messagesUser.From');
        $Status = Lang::get('profile.view.messagesUser.Read');
        if ($data) {
            $result = $this->profile->updateStatusMessage($id);
            return ['action' => true, 'subject' => $data->subject, 'status' => $Status, 'text' => $data->feedback_message, 'assessor' => $Assessor . ': ' . $data->firstname . ' ' . $data->lastname];
        } else {
            return ['action' => false];
        }

    }

    public function password()
    {
        return view('front.profile.changePassword');
    }

    public function changePassword()
    {
        $input = Input::all();
        if ($input['currentPassword'] !== '') {
            if (!Hash::check($input['currentPassword'], Auth::user()->get()->password)) {
                return Redirect::back()->withErrors(['currentPassword' => trans('profile.messages.Match_Password')]);
            } else {
                $input['password'] = Hash::make($input['newPassword']);
            }
            $user = User::find(Auth::user()->get()->id);
            $user->password = $input['password'];
            $user->save();
            return Redirect::back()->with('status', trans('profile.messages.Sucessfuly'));
        }
        return Redirect::back()->withErrors(['currentPassword' => trans('profile.messages.Wrong')]);
    }

    public function avatar()
    {
        return view('front.profile.changeAvatar');
    }

    public function changeAvatar()
    {
        if (\Illuminate\Support\Facades\Request::hasFile('avatar')) {
            if (File::exists(base_path() . env('PUBLIC_PATH_URL', 'public') . '/uploads/images/Users/' . Auth::user()->get()->id . '-200x200.jpg')) {
                File::delete(Auth::user()->get()->id . '-200x200.jpg');
            }
            Image::make(Input::file('avatar'))->fit(200, 200)->save('uploads/images/Users/' . Auth::user()->get()->id . '-200x200.jpg');
        }
        Flash::success(trans('profile.view.avatar.SaveSuceess'));
        return Redirect::back();
    }

}
