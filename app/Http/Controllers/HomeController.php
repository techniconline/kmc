<?php namespace App\Http\Controllers;

use App\Category;
use App\Menu;
use App\Content;
use App\Param;

class HomeController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public $locale;

    public function __construct()
    {
        $this->locale = app('locale') == '' ? '' : app('locale') . '.';
//		$this->middleware('auth');
    }

    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
    public function index()
    {
        $catModel = new Category();
        $results = $catModel->getDataCategory(3);
        $roots = $results['roots'];
        $translatedName = $results['translatedLang'];

        $front_menus = Menu::where('root_id', '=', 2)->first()->getDescendants()->toHierarchy();
        $contents = Content::join('contents_lang', 'contents_id', '=', 'id')
            ->where('contents_lang.lang_id', '=', app('activeLangDetails')['lang_id'])
            ->where('type', 'blog')
            ->where('contents.status', 1)
            ->get();

        return view('home')
            ->with('frontend_menus', $front_menus)
            ->with('contents', $contents)
            ->with('locale', $this->locale)
            ->with('systemContent', 'news')
            ->with('roots', $roots)
            ->with('translatedName', $translatedName);
    }

}
