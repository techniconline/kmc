<?php namespace App\Http\Controllers;

use App\Category;
use App\Comments;
use App\Content;
use App\ContentLang;
use App\Language;
use App\Providers\Helpers\Category\CategoryHelper;
use App\Providers\Helpers\PersianDate\PersianDate;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\BaseModel;
use Laracasts\Flash\Flash;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Request;

class WebLogController extends Controller
{

    public $content;
    public $comment;
    public $category;

    public $locale;
    public $langActive;
    public $language;
    public $listLang;
    public $user_id;

    public function __construct(Content $content, Language $language, Comments $comments, Category $category)
    {
        $this->content = $content;
        $this->comment = $comments;
        $this->category = $category;
        $this->listLang = Config::get('app.locales');
        $this->langActive = app('getLocale');
        $this->language = $language;
        $this->locale = app('locale') == '' ? '' : app('locale') . '.';
        $this->user_id = isset(Auth::user()->get()->id) ? Auth::user()->get()->id : 0;

    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($tag = null, $category = null, $text = null)
    {
        $inputs = Input::all();
        $category = isset($inputs['type']) ? $inputs['type'] : $category;

        $results = $this->category->getDataCategory(2);// 2 for weblog
        $roots = $results['roots'];
        $translatedName = $results['translatedLang'];
        $langId = app('activeLangDetails')['lang_id'];

        $contents = $this->content
            ->join('contents_lang', 'contents_lang.contents_id', '=', 'contents.id')
            ->leftJoin('employee', function ($joinL1) {
                $joinL1->on('contents.employee_id', '=', 'employee.id');
                $joinL1->where('contents.employee_id', '>', 0);
            })
            ->leftJoin('users', function ($joinL2) {
                $joinL2->on('contents.author_id', '=', 'users.id');
                $joinL2->where('contents.employee_id', '=', 0);
            })
//            ->leftJoin('content_categories','contents.id','=','content_categories.content_id')
//            ->leftJoin('categories','categories.id','=','content_categories.categories_id')
//            ->leftJoin('categories_lang',function($joinL)use($langId){
//                $joinL->on('categories.id','=','categories_lang.categories_id');
//                $joinL->where('categories_lang.lang_id', '=', $langId);
//            })
            ->select([
                DB::raw('(IF(contents.employee_id>0,CONCAT_WS(" ",employee.firstname,employee.lastname),CONCAT_WS(" ",users.firstname,users.lastname))) AS user_name')
                , DB::raw('(SELECT COUNT(id) FROM comments WHERE system="weblog" AND item_id=contents.id AND status=1) AS count_comments ')
//                ,'categories_lang.name as name_category'
                , DB::raw('(SELECT GROUP_CONCAT(name) FROM `categories_lang` WHERE categories_id IN (
                            SELECT categories_id FROM `content_categories` WHERE content_id=contents.id
                            ) AND lang_id=' . $langId . ') as name_category')
                , 'contents.*'
                , 'contents_lang.*'])
            ->where('contents_lang.lang_id', '=', app('activeLangDetails')['lang_id'])
            ->where('contents.status', 1)
            ->where(function ($query) use ($tag, $category, $text) {
                if ($tag) {
                    $query->where('tags', 'LIKE', '%' . $tag . '%');
                }
                if ($category) {
                    $query->where('categories.alias', $category);
                }
                if (!$text) {
                    $query->whereIn('type', ['weblog']);
                } else {
                    $query->orWhere('link_title', 'LIKE', '%' . $text . '%');
                    $query->orWhere('browser_title', 'LIKE', '%' . $text . '%');
                    $query->orWhere('body_title', 'LIKE', '%' . $text . '%');
                    $query->orWhere('short_content', 'LIKE', '%' . $text . '%');
                    $query->orWhere('body_content', 'LIKE', '%' . $text . '%');
                    $query->orWhere('tags', 'LIKE', '%' . $text . '%"');
                }
            })
            ->orderBy('contents.id', 'DESC')
            ->paginate(10);

        foreach ($contents as &$item) {

            $item->pDayName = PersianDate::$pdateWeekNameWithKey[strtolower(date("D", strtotime($item->created_at)))];
            $item->pDate = PersianDate::convert_date_M_to_H(date("Y-m-d", strtotime($item->created_at)));
            $arrDate = explode("-", $item->pDate);
            $item->pMonthName = PersianDate::$pdateMonthName[(int)$arrDate[1]];
            $item->pMonth = $arrDate[1];
            $item->pDay = $arrDate[2];
            $item->pYear = $arrDate[0];

        }

        return view('news.index', [
            'titleCategory' => trans("webLog.view.index.TitleCategory"),
            'systemContent' => 'weblog',
            'news' => $contents,
            'locale' => $this->locale,
            'type' => 'weblog'
        ])->with('roots', $roots)
            ->with('translatedName', $translatedName);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        if (!$this->user_id) {
            Flash::error(trans('webLog.messages.PleaseLogIn'));
            return Redirect::back()->withErrors([trans('webLog.messages.PleaseLogIn')]);
        }
        $root_id = 2;
        $catList = $this->category
            ->join('categories_lang', 'categories.id', '=', 'categories_lang.categories_id')
            ->where('categories_lang.lang_id', '=', app('activeLangDetails')['lang_id'])
            ->whereNotNull('parent_id')
            ->where(function ($query) use ($root_id) {
                if ($root_id) {
                    $query->where('root_id', $root_id);
                }
            })
            ->lists('name', 'id');

        return view('weblog.form', [
            'locale' => $this->locale,
            'listLang' => $this->listLang,
            'catList' => $catList,
            'type' => 'weblog'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
//        if(!$this->user_id){
//            Flash::error(trans('webLog.messages.PleaseLogIn'));
//            return Redirect::to('front/auth/login')->withInput()->withErrors([trans('webLog.messages.PleaseLogIn')]);
//        }

        $input = Input::all();
        $URL = isset($input['url']) && $input['url'] ? str_replace(" ", "-", $input['url']) : CategoryHelper::createAlias($input['link_title']);
        $input['url'] = $URL . ($URL ? date('-Y-m-d-H-i-s') : '');

        $input['body_title'] = $input['link_title'];
        $input['browser_title'] = $input['link_title'];
        $input['category_id'] = isset($input['categories']) ? $input['categories'][0] : 0;

        $input['author_id'] = $this->user_id;
        $input['employee_id'] = 0;
        $input['status'] = 2;
        $input['type'] = 'weblog';

        $fill = $this->content->fill($input);

        if (!$fill->validationData($input, $this->content->rules)) {
            return redirect()->back()->withInput()->withErrors($this->content->errors);
        }
        $this->content->save();

        if ($id = $this->content->id) {

            $resCat = $this->content->saveContentCategories($id, $input['categories']);

            if (Request::hasFile('image_content')) {
                $extension = Input::file('image_content')->getClientOriginalExtension();
                $createFolder = base_path() . env('PUBLIC_PATH_URL', 'public') . '/uploads/images/image_content/weblog/' . $id . '/';
                if (!file_exists($createFolder)) {
                    mkdir($createFolder, 0755);
                }
                if (File::exists(base_path() . env('PUBLIC_PATH_URL', 'public') . '/uploads/images/image_content/weblog/' . $id . '/' . $id . '-160x80.' . $extension)) {
                    File::delete($id . '-160x80.jpg');
                }
                if (File::exists(base_path() . env('PUBLIC_PATH_URL', 'public') . '/uploads/images/image_content/weblog/' . $id . '/' . $id . '-60x60.' . $extension)) {
                    File::delete($id . '-60x60.' . $extension);
                }
                if (File::exists(base_path() . env('PUBLIC_PATH_URL', 'public') . '/uploads/images/image_content/weblog/' . $id . '/' . $id . '-600x400.' . $extension)) {
                    File::delete($id . '-600x400.' . $extension);
                }
                Image::make(Input::file('image_content'))->fit(60, 60)->save('uploads/images/image_content/weblog/' . $id . '/' . $id . '-60x60.' . $extension);
                Image::make(Input::file('image_content'))->fit(160, 80)->save('uploads/images/image_content/weblog/' . $id . '/' . $id . '-160x80.' . $extension);
                Image::make(Input::file('image_content'))->fit(600, 400)->save('uploads/images/image_content/weblog/' . $id . '/' . $id . '-600x400.' . $extension);
                $updateContent = $this->content->find($id);
                $updateContent->image_content = '/uploads/images/image_content/weblog/' . $id . '/';
                $updateContent->extension = $extension;
                $updateContent->save();
                $messages[] = ['level' => BaseModel::level_success, 'message' => Lang::get('backend/Content.messages.uploadFileSuccess')];
            }

            $input['contents_id'] = $id;
            foreach ($this->listLang as $key => $value) {
                $lang = $this->language->getLanguage($key);
                $input['lang_id'] = $lang['lang_id'];
                $input['title_tags'] = CategoryHelper::createTags($input['link_title']);
                $input['tags_upd'] = array_unique(explode(",", $input['title_tags']));
                $input['tags'] = implode(",", $input['tags_upd']);
                $content_lang = new ContentLang();
                if (!$content_lang->fill($input)->isValid()) {
                    $this->content->delete();
                    return redirect()->back()->withInput()->withErrors($content_lang->errors);
                }
                $content_lang->save();

            }
            $messages[] = ['level' => BaseModel::level_success, 'message' => Lang::get('backend/Content.messages.successInsert')];
            Session::flash('messages', $messages);
            return Redirect::route($this->locale . 'weblog.index')->with('locale', $this->locale);
        }
        return Redirect::back()->withErrors(Lang::get('backend/Content.messages.errCreate'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $alias
     * @return Response
     */
    public function show($alias)
    {
        $results = $this->category->getDataCategory(2);
        $roots = $results['roots'];
        $translatedName = $results['translatedLang'];
        $langId = app('activeLangDetails')['lang_id'];

        $content = $this->content
            ->join('contents_lang', 'contents_lang.contents_id', '=', 'contents.id')
            ->where('contents.type', 'weblog')
            ->where('contents.status', 1)
            ->where('contents.url', $alias)
            ->leftJoin('employee', function ($joinL1) {
                $joinL1->on('contents.employee_id', '=', 'employee.id');
                $joinL1->where('contents.employee_id', '>', 0);
            })
            ->leftJoin('users', function ($joinL1) {
                $joinL1->on('contents.author_id', '=', 'users.id');
                $joinL1->where('contents.employee_id', '=', 0);
            })
            ->leftJoin('content_categories', 'contents.id', '=', 'content_categories.content_id')
            ->leftJoin('categories', 'categories.id', '=', 'content_categories.categories_id')
            ->leftJoin('categories_lang', function ($joinL) use ($langId) {
                $joinL->on('categories.id', '=', 'categories_lang.categories_id');
                $joinL->where('categories_lang.lang_id', '=', $langId);
            })
            ->select([
                DB::raw('(IF(contents.employee_id>0,CONCAT_WS(" ",employee.firstname,employee.lastname),CONCAT_WS(" ",users.firstname,users.lastname))) AS user_name')
                , 'categories_lang.name as name_category'
                , 'contents.*'
                , 'contents_lang.*'])->first();
        if (!$content)
            abort(404);
        if ($content->login == 1 && !isset(Auth::user()->get()->id))
            abort(503);

        $this->visitItem(['system' => 'content', 'item_id' => $content->id]);//visit log

        $content->pDayName = PersianDate::$pdateWeekNameWithKey[strtolower(date("D", strtotime($content->created_at)))];
        $content->pDate = PersianDate::convert_date_M_to_H(date("Y-m-d", strtotime($content->created_at)));
        $arrDate = explode("-", $content->pDate);
        $content->pMonthName = PersianDate::$pdateMonthName[(int)$arrDate[1]];
        $content->pMonth = $arrDate[1];
        $content->pDay = $arrDate[2];
        $content->pYear = $arrDate[0];

        $comments = $this->comment->getComments('weblog', $content->id);

        return view('weblog.show', [
            'content' => $content,
            'comments' => $comments,
            'locale' => $this->locale
        ])->with('roots', $roots)
            ->with('systemContent', 'weblog')
            ->with('system', 'weblog')
            ->with('translatedName', $translatedName);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $content = $this->content->where('id', $id)
            ->join('contents_lang', 'id', '=', 'contents_id')->where('contents_lang.lang_id', '=', app('activeLangDetails')['lang_id'])->first();
        return view('backend.content.form', [
            'content' => $content,
            'locale' => $this->locale,
            'listLang' => $this->listLang,
            'type' => 'news'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $input = Input::all();
        $lang = $this->language->getLanguage($input['lang']);
        $input['lang_id'] = $lang['lang_id'];

        $URL = isset($input['url']) && $input['url'] ? str_replace(" ", "-", $input['url']) : CategoryHelper::createAlias($input['link_title']);
        $input['url'] = $URL;
        $input['title_tags'] = CategoryHelper::createTags($input['link_title']);
        $updateContent = $this->content->find($id);
        $updateContent->url = $input['url'];
        $updateContent->login = $input['login'];
        $input['tags_upd'] = array_unique(explode(",", $input['title_tags'] . ',' . $input['tags']));

        $paramsForTranslate = [
            'contents_id' => $id,
            'lang_id' => $input['lang_id'],
            'link_title' => $input['link_title'],
            'browser_title' => $input['browser_title'],
            'body_title' => $input['body_title'],
            'tags' => implode(",", $input['tags_upd']),
            'short_content' => $input['short_content'],
            'body_content' => $input['body_content']

        ];
        ContentLang::updateLang($paramsForTranslate);
        $resCat = $this->content->saveContentCategories($id, $input['categories']);

        if (Request::hasFile('image_content')) {

            $extension = Input::file('image_content')->getClientOriginalExtension();
            $createFolder = base_path() . env('PUBLIC_PATH_URL', 'public') . '/uploads/images/image_content/weblog/' . $id . '/';
            if (!file_exists($createFolder)) {
                mkdir($createFolder, 0755);
            }
            if (File::exists(base_path() . env('PUBLIC_PATH_URL', 'public') . '/uploads/images/image_content/weblog/' . $id . '/' . $id . '-160x80.' . $extension)) {
                File::delete($id . '-160x80.jpg');
            }
            if (File::exists(base_path() . env('PUBLIC_PATH_URL', 'public') . '/uploads/images/image_content/weblog/' . $id . '/' . $id . '-60x60.' . $extension)) {
                File::delete($id . '-60x60.' . $extension);
            }
            if (File::exists(base_path() . env('PUBLIC_PATH_URL', 'public') . '/uploads/images/image_content/weblog/' . $id . '/' . $id . '-600x400.' . $extension)) {
                File::delete($id . '-600x400.' . $extension);
            }
            Image::make(Input::file('image_content'))->fit(60, 60)->save('uploads/images/image_content/weblog/' . $id . '/' . $id . '-60x60.' . $extension);
            Image::make(Input::file('image_content'))->fit(160, 80)->save('uploads/images/image_content/weblog/' . $id . '/' . $id . '-160x80.' . $extension);
            Image::make(Input::file('image_content'))->fit(600, 400)->save('uploads/images/image_content/weblog/' . $id . '/' . $id . '-600x400.' . $extension);

            $updateContent = $this->content->find($id);
            $updateContent->image_content = '/uploads/images/image_content/weblog/' . $id . '/';
            $updateContent->extension = $extension;

            $messages[] = ['level' => BaseModel::level_success, 'message' => Lang::get('backend/Content.messages.uploadFileSuccess')];
        }

        $updateContent->save();

        $messages[] = ['level' => BaseModel::level_success, 'message' => Lang::get('backend/Content.messages.successUpdate')];
        Session::flash('messages', $messages);

        return Redirect::back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $content = $this->content->find($id);
        $content->status = 0;
        $content->save();

        Flash::success(trans('backend/Content.messages.successDelete'));
        return Redirect::back();
    }

    public function searchTags($tag)
    {
        return $this->index($tag);
    }

    public function searchCategory($category)
    {
        return $this->index(null, $category);
    }
}
