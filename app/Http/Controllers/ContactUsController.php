<?php namespace App\Http\Controllers;

use App\Content;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Message;
use App\Providers\Helpers\Category\CategoryHelper;
use Illuminate\Http\Request;
use App\Language;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;
use App\Apartment;
use App\Media;
use App\RoomCategory;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Laracasts\Flash\Flash;

class ContactUsController extends Controller
{

    public $locale;

    public function __construct()
    {
        $this->locale = app('locale') == '' ? '' : app('locale') . '.';
    }

    public function index()
    {
        return view('contactUs.index', [
            'locale' => $this->locale
        ]);
    }

    public function store()
    {
        $input = Input::all();
        $rules = array(
            'email' => 'required|email',
            'message' => 'required',
            'recaptcha_response_field' => 'required|recaptcha'
        );
        $valid = Validator::make(Input::all(), $rules);
        if ($valid->fails()) {
            Flash::error('send message is not successfully.');
            return redirect()->back()->withInput()->withErrors($valid);
        } else {
            $message = new Message();
            $message->status = 0;
            $message->from = $input['email'];
            $message->name = $input['name'];
            $message->to = $input['to'];
            $message->message = $input['message'];

            if ($message->save()) {
                Mail::send('emails.message', ['from' => $input['email'], 'pm' => $input['message']], function ($message) use ($input) {
                    $message->to('sajjadkharrazi@gmail.com', $input['email'])->subject('New Message!');
                });
            }

            Flash::success('send your message.');
            return redirect()->back();
        }
    }
}
