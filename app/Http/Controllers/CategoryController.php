<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Category;
use App\Providers\Helpers\Category\CategoryHelper;
use Illuminate\Http\Request;
use App\Language;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use App\Product;
use App\Media;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{

    public $locale;
    public $langActive;
    public $language;
    public $listLang;

    public $category;
    public $products;
    public $user_id;

    public function __construct(Language $language, Product $products, Category $category)
    {

        $this->products = $products;
        $this->category = $category;
        $this->listLang = Config::get('app.locales');
        $this->langActive = app('getLocale');
        $this->language = $language;
        $this->locale = app('locale') == '' ? '' : app('locale') . '.';
        $this->user_id = isset(Auth::user()->get()->id) ? Auth::user()->get()->id : 0;

    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $results = $this->category->getDataCategory(3);
        $roots = $results['roots'];
        $translatedName = $results['translatedLang'];

        $lang = $this->language->getLanguage($this->langActive);
        $data = $this->products
            ->join('products_lang', 'products_lang.id', '=', 'products.id')
            ->leftjoin('product_media', function ($leftJoin) {
                $leftJoin->on('products.id', '=', 'product_media.product_id');
                $leftJoin->where('product_media.is_default', '=', 1);
                $leftJoin->where('product_media.status', '=', 1);
            })
            ->leftjoin('media', function ($leftJoinM) {
                $leftJoinM->on('product_media.media_id', '=', 'media.id');
                $leftJoinM->where('media.status', '=', 1);
            })
            ->where('products.status', '=', 1)
            ->where('products_lang.lang_id', '=', $lang['lang_id'])
            ->select(['products_lang.title'
                , 'products_lang.short_description'
                , 'products_lang.description'
                , 'products_lang.alias'
                , 'products.*'
                , 'media.src'
            ])
            ->get();

        //create alias for show in link
//        foreach($data as &$item){
//            $item->alias = CategoryHelper::createAlias($item->title);
//        }

        return view('category.index')->with('locale', $this->locale)->with('data', $data)
            ->with('roots', $roots)
            ->with('translatedName', $translatedName);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($alias)
    {
        $catModel = new Category();
        $results = $this->category->getDataCategory(3);
        $roots = $results['roots'];
        $translatedName = $results['translatedLang'];

        $lang = $this->language->getLanguage($this->langActive);

        $dataCategory = $this->category
            ->where('alias', $alias)->first();
        $parentId = $dataCategory->parent_id;

        if (!$dataCategory) {
            abort(404);
        }

        $data = $this->products
            ->join('products_lang', 'products_lang.id', '=', 'products.id')
            ->join('product_categories', 'product_categories.product_id', '=', 'products.id')
            ->join('categories', 'product_categories.categories_id', '=', 'categories.id')
            ->leftjoin('product_media', function ($leftJoin) {
                $leftJoin->on('products.id', '=', 'product_media.product_id');
                $leftJoin->where('product_media.is_default', '=', 1);
                $leftJoin->where('product_media.status', '=', 1);
            })
            ->leftjoin('media', function ($leftJoinM) {
                $leftJoinM->on('product_media.media_id', '=', 'media.id');
                $leftJoinM->where('media.status', '=', 1);
            })
            ->where('categories.alias', $alias)
            ->where('products.status', '=', 1)
            ->where('products_lang.lang_id', '=', $lang['lang_id'])
            ->select(['products_lang.title'
                , 'products_lang.short_description'
                , 'products_lang.alias'
                , 'products_lang.description', 'products.*'
                , 'media.src'
            ])
            ->get();

        $this->visitItem(['system' => 'content', 'item_id' => $dataCategory->id]);//visit log

//        $mediaModel = new Media();
//        $images=$mediaModel->getMediaProduct('image',$product_id);
//        $videos=$mediaModel->getMediaProduct('video',$product_id);

        $dataCategories = $this->category
            ->join('categories_lang', 'categories_lang.categories_id', '=', 'categories.id')
            ->where('parent_id', $parentId)
            ->where('categories_lang.lang_id', '=', $lang['lang_id'])
            ->get();

        return view('category.index')
            ->with('locale', $this->locale)
            ->with('systemContent', 'news')
            ->with(['data' => $data
//                ,'imageProduct'=>$images
//                ,'videoProduct'=>$videos
            ])
            ->with('roots', $roots)
            ->with('translatedName', $translatedName)
            ->with('dataCategories', $dataCategories);


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

}
