<?php namespace App;

use Baum\Node;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Menu
 */
class MenuLang extends Model
{

    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'menus_lang';

    public static function updateLang($params)
    {
        $result = DB::table('menus_lang')
            ->where('menus_id', '=', $params['menus_id'])
            ->where('lang_id', '=', $params['lang_id'])
            ->update(
                [
                    'name' => $params['name'],
                    'desc' => $params['desc']
                ]
            );

        return $result;
    }

    public static function saveLang($params)
    {
        $result = DB::table('menus_lang')->insert(
            [
                'menus_id' => $params['menus_id'],
                'lang_id' => $params['lang_id'],
                'name' => $params['name']
            ]
        );

        return $result;
    }
}
