<?php

/**
 * Created by PhpStorm.
 * User: Hamid
 * Date: 13/01/2015
 * Time: 09:03 AM
 */
namespace App;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use PDO;

class Profile extends BaseModel
{

    protected $table = 'info_users';
    protected $primaryKey = 'user_id';
    protected $fillable = ['user_id', 'persona', 'education', 'institution_type',
        'institution', 'institution_town', 'current_institution_type',
        'current_institution', 'current_institution_town', 'parent_guarantor', 'accuracy_information',
        'personal_message', 'status'];

    protected $hidden = ['remember_token'];
    public $errors;
    public $rules = ['user_id' => 'required|numeric'];

    public function isValid()
    {
        $validation = Validator::make($this->attributes, $this->rules);
        if ($validation->fails()) {
            $this->errors = $validation->messages();
            return false;
        }
        return true;
    }

    public function getCountries($isList = true)
    {

        if ($isList) {
            $countries = DB::table('countries')->where('status', '=', 1)->lists('name', 'id');
        } else {
            $countries = DB::table('countries')->where('status', '=', 1)->get();
        }

        return $countries;

    }


    public function updateUserProfile($params)
    {

        $result = DB::table('users')
            ->where('id', Auth::user()->get()->id)
            ->update($params);


        return $result;

    }

    public static function checkCompleted($user_id = 0, $for = 'user')
    {
        if (!$user_id || !$for)
            return false;


        if ($for == 'parent') {
            $checkBooking = self::checkCompleted($user_id, 'bookingDate');
            $checkUser = self::checkCompleted($user_id, 'user');
            $checkInfo = self::checkCompleted($user_id, 'info');
            if (!$checkBooking || !$checkUser || !$checkInfo)
                return false;

            DB::setFetchMode(PDO::FETCH_ASSOC);
            $data = DB::table('user_parent')
                ->where('user_id', $user_id)
                ->where('status', 1)
                ->select('parent_first_name', 'parent_last_name', 'parent_email', 'parent_mobile'
                    , 'parent_tenant_address', 'parent_address'
                    , 'parent_postal_code', 'parent_country_id'
                )
                ->first();

            DB::setFetchMode(PDO::FETCH_CLASS);
            if (!$data || in_array("", $data) || in_array(null, $data) || in_array('0', $data)) {
                return false;
            }

        } elseif (($for == 'bookingRoom')) {
            DB::setFetchMode(PDO::FETCH_ASSOC);
            $data = DB::table('booking')
                ->where('user_id', $user_id)
                ->where('status', 1)
                ->select('booking.user_id'
                    , 'booking.accepted_terms'
                    , 'booking.accepted_house_rules'
                )
                ->orderBy('booking.id', 'DESC')
                ->first();

            DB::setFetchMode(PDO::FETCH_CLASS);
            if (!$data || in_array("", $data) || in_array(null, $data) || in_array('0', $data)) {
                return false;
            }

        } elseif (($for == 'bookingDate')) {
            DB::setFetchMode(PDO::FETCH_ASSOC);
            $data = DB::table('booking')
                ->where('user_id', $user_id)
                ->where('status', 1)
                ->select('booking.user_id', 'booking.from_date', 'booking.to_date'
                    , 'booking.accepted_terms'
                    , 'booking.accepted_house_rules'
                )
                ->orderBy('booking.id', 'DESC')
                ->first();

            DB::setFetchMode(PDO::FETCH_CLASS);
            if (!$data || in_array("", $data) || in_array(null, $data) || in_array('0', $data)
                || $data['from_date'] < date('Y-m-d') || $data['to_date'] <= $data['from_date']
            ) {
                return false;
            }

        } elseif (($for == 'info')) {
            $checkBooking = self::checkCompleted($user_id, 'bookingDate');
            $checkUser = self::checkCompleted($user_id, 'user');
            if (!$checkBooking || !$checkUser)
                return false;

            DB::setFetchMode(PDO::FETCH_ASSOC);
            $data = DB::table('info_users')
                ->where('user_id', $user_id)
                ->select('persona', 'education', 'institution_type', 'institution'
                    , 'institution_town', 'current_institution_type'
                    , 'current_institution', 'current_institution_town'
                    , 'parent_guarantor', 'accuracy_information'
                )
                ->first();


            if (!$data || in_array("", $data) || in_array(null, $data) || in_array('0', $data)) {
                DB::setFetchMode(PDO::FETCH_CLASS);
                return false;
            }


        } elseif (($for == 'user')) {
            $checkBooking = self::checkCompleted($user_id, 'bookingDate');
            if (!$checkBooking)
                return false;

            DB::setFetchMode(PDO::FETCH_ASSOC);
            $data = DB::table('users')
                ->where('id', $user_id)
                ->where('status', 1)
                ->select('firstname', 'lastname', 'gender', 'birth_date'
                    , 'mobile', 'address', 'code_postal', 'country_id', 'city')->first();

            DB::setFetchMode(PDO::FETCH_CLASS);

            if (!$data || in_array("", $data) || in_array(null, $data) || in_array('0', $data)) {
                return false;
            }

        }

        DB::setFetchMode(PDO::FETCH_CLASS);
        return true;
    }

    public function getMessagesUser($user_id)
    {

        $data = DB::table('attach_documents_feedback')
            ->join('employee', 'employee.id', '=', 'attach_documents_feedback.assessor_user_id')
            ->where('attach_documents_feedback.status', '!=', 0)
            ->where('attach_documents_feedback.user_id', '=', $user_id)
            ->select('attach_documents_feedback.*'
                , 'employee.firstname', 'employee.lastname'
            )
            ->orderBy('attach_documents_feedback.view_status')
            ->orderBy('attach_documents_feedback.id', 'DESC')
            ->get();

        return $data;

    }

    public function getMessagesBooking($booking_id, $status = 0, $first_row = false)
    {

        $data = DB::table('attach_documents_feedback')
            ->join('employee', 'employee.id', '=', 'attach_documents_feedback.assessor_user_id')
            ->where('attach_documents_feedback.booking_id', '=', $booking_id)
            ->where(function ($query) use ($status) {
                $query->where('attach_documents_feedback.status', '=', $status);
            })
            ->select('attach_documents_feedback.*'
                , 'employee.firstname', 'employee.lastname'
            )
            ->orderBy('attach_documents_feedback.id', 'DESC')->get();

        if ($data && $first_row) {
            return $data[0];
        } elseif ($data) {
            return $data;
        } else {
            return false;
        }


    }

    public static function checkMessagesUser($user_id)
    {

        $data = DB::table('attach_documents_feedback')
            ->where('attach_documents_feedback.status', '!=', 0)
            ->where('attach_documents_feedback.view_status', '=', 0)
            ->where('attach_documents_feedback.user_id', '=', $user_id)
            ->count();
        return $data;

    }

    public function getMessage($id)
    {

        $data = DB::table('attach_documents_feedback')
            ->join('employee', 'employee.id', '=', 'attach_documents_feedback.assessor_user_id')
            ->where('attach_documents_feedback.status', '!=', 0)
            ->where('attach_documents_feedback.id', '=', $id)
            ->select('attach_documents_feedback.*'
                , 'employee.firstname', 'employee.lastname'
            )
            ->orderBy('attach_documents_feedback.id', 'DESC')
            ->first();

        return $data;

    }


    public function updateStatusMessage($id)
    {

        $result = DB::table('attach_documents_feedback')
            ->where('id', $id)
            ->update(
                [
                    'attach_documents_feedback.view_status' => 1
                    , 'attach_documents_feedback.updated_at' => date('Y-m-d H:i:s')
                ]
            );


        return $result;

    }


} 