<?php namespace App;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use App\MailModel;
use App\AttachType;

class Billing extends BaseModel
{

    protected $table = 'billings';
    protected $primaryKey = 'id';
    protected $fillable = ['user_id'
        , 'booking_id', 'room_id', 'rent_id'
        , 'pay_date', 'assessor_user_id'
        , 'billing_status', 'status'
        , 'price', 'year', 'month'];

    protected $hidden = ['remember_token'];
    public $errors;
    public $rules = [
        'user_id' => 'required|numeric'
        , 'pay_date' => 'required'
        , 'rent_id' => 'required|numeric'
        , 'booking_id' => 'required|numeric'
        , 'room_id' => 'required|numeric'
        , 'assessor_user_id' => 'required|numeric'
        , 'billing_status' => 'required|numeric'
        , 'price' => 'required|numeric'
        , 'year' => 'required|numeric'
        , 'month' => 'required|numeric'
    ];

    public function isValid()
    {
        $validation = Validator::make($this->attributes, $this->rules);
        if ($validation->fails()) {
            $this->errors = $validation->messages();
            return false;
        }
        return true;
    }

    public function getRentData($rent_id)
    {

        if (!$rent_id)
            return false;

        $result = DB::table('rent')
            ->join('users', 'rent.user_id', '=', 'users.id')
            ->join('rooms', 'rent.room_id', '=', 'rooms.id')
            ->select(
                'rent.*'
                , 'users.firstname', 'users.lastname'
                , 'users.gender', 'users.birth_date'
                , 'users.code_postal', 'users.address'
                , 'users.email'
                , 'rooms.price', 'rooms.room_number'
                , DB::raw('(SELECT rent_price FROM apartment_room_price WHERE room_category_id=rent.room_category_id AND status=1 ORDER BY id DESC LIMIT 1) as rent_price')
            )
            ->where('rent.id', '=', $rent_id)
            ->where('rent.rent_status', '!=', 0)
            ->where('rent.status', '!=', 0)
            ->orderBy('rent.id', 'DESC')
            ->first();

        return $result;

    }

    public function getListBillingUsersWithDetails($params = null)
    {

        $user_id = isset($params['user_id']) ? $params['user_id'] : 0;
        $booking_id = isset($params['booking_id']) ? $params['booking_id'] : 0;
        $paginateNum = isset($params['paginateNum']) ? $params['paginateNum'] : 10;
        $billing_status = isset($params['billing_status']) ? $params['billing_status'] : 0;
        $lang_id = isset($params['lang_id']) ? $params['lang_id'] : 0;

        $rent_month_date_from = isset($params['date_from']) ? $params['date_from'] : 0;
        $year_from = 0;
        $month_from = 0;
        $year_to = 0;
        $month_to = 0;
        if ($rent_month_date_from) {
            $dateArr = explode("-", $rent_month_date_from, 2);
            $year_from = strlen($dateArr[0]) == 4 ? $dateArr[0] : 0;
            $month_from = strlen($dateArr[1]) >= 1 || strlen($dateArr[1]) <= 2 ? $dateArr[1] : 0;
        }

        $rent_month_date_to = isset($params['date_to']) ? $params['date_to'] : 0;
        if ($rent_month_date_to) {
            $dateArr = explode("-", $rent_month_date_to, 2);
            $year_to = strlen($dateArr[0]) == 4 ? $dateArr[0] : 0;
            $month_to = strlen($dateArr[1]) >= 1 || strlen($dateArr[1]) <= 2 ? $dateArr[1] : 0;
        }

        $reg_date_from = isset($params['reg_date_from']) ? $params['reg_date_from'] : 0;
        $reg_date_to = isset($params['reg_date_to']) ? $params['reg_date_to'] : 0;

        $email = isset($params['email']) ? $params['email'] : 0;
        $last_name = isset($params['last_name']) ? $params['last_name'] : 0;

        $room_number = isset($params['room_number']) ? $params['room_number'] : 0;

        $price = isset($params['price']) ? $params['price'] : 0;

        $result = DB::table('billings')
            ->join('users', 'billings.user_id', '=', 'users.id')
            ->join('booking', 'billings.booking_id', '=', 'booking.id')
            ->join('rooms', 'billings.room_id', '=', 'rooms.id')
            ->join('apartment_room_category_map', 'rooms.apartment_room_category_map_id', '=', 'apartment_room_category_map.id')
            ->join('room_category', 'apartment_room_category_map.room_category_id', '=', 'room_category.id')
            ->join('room_category_lang', 'room_category.id', '=', 'room_category_lang.id')
            ->where(function ($query) use (
                $user_id, $year_from, $month_from, $year_to, $month_to
                , $reg_date_from, $reg_date_to
                , $email, $last_name
                , $room_number, $price, $billing_status
            ) {
                if ($billing_status) {
                    $query->where('billings.billing_status', '=', $billing_status);
                } else {
                    $query->where('billings.billing_status', '!=', 0);
                }
                if ($user_id) {
                    $query->where('billings.user_id', '=', $user_id);
                }
                if ($year_from) {
                    $query->where('billings.year', '>=', $year_from);
                }
                if ($month_from) {
                    $query->where('billings.month', '>=', $month_from);
                }
                if ($year_to) {
                    $query->where('billings.year', '<=', $year_to);
                }
                if ($month_to) {
                    $query->where('billings.month', '<=', $month_to);
                }
                if ($reg_date_from) {
                    $query->where('billings.created_at', '>=', $reg_date_from . ' 00:00:00');
                }
                if ($reg_date_to) {
                    $query->where('billings.created_at', '<=', $reg_date_to . ' 23:59:59');
                }
                if ($email) {
                    $query->where('users.email', 'like', '%' . $email . '%');
                }
                if ($last_name) {
                    $query->where('users.lastname', 'like', '%' . $last_name . '%');
                }
                if ($room_number) {
                    $query->where('rooms.room_number', 'like', '' . $room_number . '%');
                }
                if ($price) {
                    $query->where('billings.price', '>=', $price);
                }
            })
            ->where('billings.status', '!=', 0)
            ->where('room_category_lang.lang_id', '=', $lang_id)
            ->where('apartment_room_category_map.status', '=', 1)
            ->where('room_category.status', '=', 1)
            ->select(['billings.*'
                , 'rooms.room_number', 'rooms.price as room_price'
                , 'users.firstname', 'users.gender', 'users.mobile', 'users.birth_date'
                , 'users.lastname', 'users.email'
            ])
            ->orderBy('billings.id', 'DESC')
            ->paginate($paginateNum);

        return $result;
    }

    public function generateRentList($year, $month, $assessor_user_id)
    {

        $resultMail = $this->sendMailForInvoiceDueDate($year, $month, $assessor_user_id);

        $query = "
        INSERT INTO billings (rent_id,booking_id,room_id,price,YEAR,MONTH
                    ,pay_date,user_id,assessor_user_id,billing_status,STATUS,created_at)
                (
                SELECT r.id,r.booking_id,r.room_id
                ,(SELECT price FROM rooms WHERE id = r.room_id) AS price
                ," . $year . "  , " . $month . " , '" . date('Y-m-d') . "', r.user_id , " . $assessor_user_id . " , 2 , 1, NOW()
                FROM rent r
                WHERE (
                (MONTH(r.from_date) = " . $month . "
                AND YEAR(r.from_date) = " . $year . " )
                OR
                (MONTH(r.to_date) = " . $month . "
                AND YEAR(r.to_date) = " . $year . " )
                )
                AND r.rent_status!=0
                AND r.status=1

                AND r.id NOT IN (SELECT rent_id FROM billings WHERE YEAR=" . $year . "  AND MONTH=" . $month . "  AND STATUS=1)
                )
        ";
        $result = DB::statement($query);

        return ['resultMail' => $resultMail['result'], 'actionMail' => $resultMail['action'], 'result' => $result];

    }

    public function sendMailForInvoiceDueDate($year, $month, $assessor_user_id)
    {

        $query = "
                SELECT r.booking_id, r.user_id,
                  (SELECT CONCAT_WS('|',email,firstname,lastname) FROM users WHERE id = r.user_id) AS user_data
                FROM rent r
                WHERE (
                (MONTH(r.from_date) = " . $month . "
                AND YEAR(r.from_date) = " . $year . " )
                OR
                (MONTH(r.to_date) = " . $month . "
                AND YEAR(r.to_date) = " . $year . " )
                )
                AND r.rent_status!=0
                AND r.status=1

                AND r.id NOT IN (SELECT rent_id FROM billings WHERE YEAR=" . $year . "  AND MONTH=" . $month . "  AND STATUS=1)

        ";
        $result = DB::select($query);

        $data = [];
        $data['page'] = 'emails.due-invoice';
        $data['subject'] = Lang::get('backend/Billing.messages.SubjectDueInvoice');
        $data['text'] = Lang::get('backend/Billing.messages.TextDueInvoice');

        $params['subject'] = $data['subject'];
        $params['feedback_message'] = $data['text'];
        $params['status'] = 16; //for due invoice
        $params['assessor_user_id'] = $assessor_user_id;
        $attachModel = new AttachType();

        $listMail = [];
        foreach ($result as $item) {
            $arr = explode("|", $item->user_data);
            $listMail[] = $arr[0];

            $params['user_id'] = $item->user_id;
            $params['booking_id'] = $item->booking_id;
            $resultMsg = $attachModel->addFeedBackForDocument($params);

        }

        //send Email

        $data['emails'] = $listMail;


        if (!$data['emails'] || !$data['text'])
            return false;

        $mailModel = new MailModel();
        $resultMail = $mailModel->sendMailToMany($data);
        if (!$resultMail) {
            return ['action' => false, 'result' => $resultMail, 'message' => Lang::get('backend/AttachType.messages.SendMailNotOk')];
        }

        return ['action' => true, 'result' => $resultMail, 'message' => Lang::get('backend/AttachType.messages.SendMailOk')];

        //---------

    }

    public function getListInvoice($year = 0, $month = 0, $billing_status = 2)
    {
        $result = $this->join('users', 'billings.user_id', '=', 'users.id')
            ->join('rooms', 'billings.room_id', '=', 'rooms.id')
            ->where('users.status', '!=', 0)
            ->where('billings.billing_status', '=', $billing_status)
            ->where('billings.status', '!=', 0)
            ->where(function ($query) use ($year, $month) {
                if ($year) {
                    $query->where('billings.year', '=', (int)$year);
                }
                if ($month) {
                    $query->where('billings.month', '=', (int)$month);
                }
            })
            ->select('billings.year', 'billings.month'
                , 'billings.billing_status'
                , 'rooms.room_number', 'rooms.price'
                , 'users.firstname', 'users.lastname', 'users.email', 'users.mobile')->get();

        return $result;
    }

    public function sendMailForRememberInvoiceDueDate($year, $month, $assessor_user_id)
    {

        $result = $this->join('users', 'billings.user_id', '=', 'users.id')
            ->where('users.status', '!=', 0)
            ->where('billings.billing_status', '=', 2)
            ->where('billings.status', '!=', 0)
            ->where('billings.year', '=', (int)$year)
            ->where('billings.month', '=', (int)$month)
            ->select('billings.*', 'users.email')->get();

        $data = [];
        $data['page'] = 'emails.due-invoice';
        $data['subject'] = Lang::get('backend/Billing.messages.SubjectDueInvoice') . ' - ' . Lang::get('backend/Billing.messages.Remember');
        $data['text'] = Lang::get('backend/Billing.messages.TextDueInvoice');
        $params['subject'] = $data['subject'];
        $params['feedback_message'] = $data['text'];
        $params['status'] = 17; //for due invoice remember
        $params['assessor_user_id'] = $assessor_user_id;

        $attachModel = new AttachType();

        $listMail = [];
        foreach ($result as $item) {
            $listMail[] = $item->email;

            $params['user_id'] = $item->user_id;
            $params['booking_id'] = $item->booking_id;
            $resultMsg = $attachModel->addFeedBackForDocument($params);

        }

        //send Email

        $data['emails'] = $listMail;


        if (!$data['emails'] || !$data['text'])
            return false;

        $mailModel = new MailModel();
        $resultMail = $mailModel->sendMailToMany($data);
        if (!$resultMail) {
            return ['action' => false, 'result' => $resultMail, 'message' => Lang::get('backend/AttachType.messages.SendMailNotOk')];
        }

        return ['action' => true, 'result' => $resultMail, 'message' => Lang::get('backend/AttachType.messages.SendMailOk')];

        //---------

    }


    public function getListRentedRooms($params = null)
    {

        $user_id = isset($params['user_id']) ? $params['user_id'] : 0;
        $lang_id = isset($params['lang_id']) ? $params['lang_id'] : 0;

        if (!$lang_id)
            return false;

        $result = DB::table('rent')
            ->join('users', 'rent.user_id', '=', 'users.id')
            ->join('rooms', 'rent.room_id', '=', 'rooms.id')
            ->join('apartment_room_category_map', 'rooms.apartment_room_category_map_id', '=', 'apartment_room_category_map.id')
            ->join('apartments', 'apartment_room_category_map.apartment_id', '=', 'apartments.id')
            ->join('apartments_lang', 'apartments_lang.id', '=', 'apartments.id')
            ->join('room_category', 'apartment_room_category_map.room_category_id', '=', 'room_category.id')
            ->join('room_category_lang', 'room_category.id', '=', 'room_category_lang.id')
            ->leftJoin('apartment_room_price', function ($join) {
                $join->on('apartment_room_category_map.room_category_id', '=', 'apartment_room_price.room_category_id')
                    ->where('apartment_room_price.price_status', '=', 1)
                    ->where('apartment_room_price.status', '=', 1);
            })
            ->where('room_category_lang.lang_id', '=', $lang_id)
            ->where('apartments_lang.lang_id', '=', $lang_id)
            ->where('rent.status', '=', 1)
            ->where('rent.rent_status', '!=', 0)
            ->where('rooms.status', '!=', 0)
            ->where('apartments.status', '=', 1)
            ->where('apartment_room_category_map.status', '=', 1)
            ->where('room_category.status', '=', 1)
            ->select([
                'rooms.*'
                , 'rent.id'
                , 'rent.booking_id'
                , 'rent.rent_status'
                , DB::raw('CONCAT_WS(" - ",apartments_lang.title,CONCAT("( type:",room_category_lang.title," )")
                                                    ,CONCAT("( Number: ",rooms.room_number," ) ")
                                                    ,CONCAT("( Username: ",users.firstname," ",users.lastname," ) ")
                                                    ,CONCAT("( Email: ",users.email," ) ")
                                                    ,CONCAT("( Area: ",room_category.area,"~",room_category.to_area," ) ")
                                                    ,CONCAT("( Price: ",IF(rooms.price>0,rooms.price, CONCAT(apartment_room_price.rent_price,"~",apartment_room_price.to_rent_price) )," ) ")
                                                    , IF(rent.room_id>0 AND rent.rent_status=2,"<b style=\"color: #ff0000\">Reserved</b>"
                                                    ,IF(rent.room_id>0 AND rent.rent_status=3,"<b style=\"color: #00ff00\">Rented</b>" ,"")
                                                    )) as title_list')
            ])
            ->orderBy('rooms.id')
            ->orderBy('apartment_room_category_map.id')
            ->orderBy('room_category.area', 'DESC')
            ->orderBy('apartment_room_price.rent_price', 'DESC')
            ->lists('title_list', 'rent.id');
        return $result;

    }


}
