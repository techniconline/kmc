<?php

/**
 * Created by PhpStorm.
 * User: Hamid
 * Date: 13/01/2015
 * Time: 09:03 AM
 */
namespace App;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;

class UserParent extends BaseModel
{

    protected $table = 'user_parent';
    protected $primaryKey = 'user_id';
    protected $fillable = ['user_id', 'parent_first_name',
        'parent_last_name', 'parent_email', 'parent_mobile', 'parent_tenant_address',
        'parent_address', 'parent_postal_code',
        'parent_country_id', 'status'];

    protected $hidden = ['remember_token'];
    public $errors;
    public $rules = ['user_id' => 'required|numeric'];

    public function isValid()
    {
        $validation = Validator::make($this->attributes, $this->rules);
        if ($validation->fails()) {
            $this->errors = $validation->messages();
            return false;
        }
        return true;
    }


}