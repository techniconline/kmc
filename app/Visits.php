<?php

/**
 * Created by PhpStorm.
 * User: Hamid
 * Date: 13/01/2015
 * Time: 09:03 AM
 */
namespace App;

class Visits extends BaseModel
{

    protected $table = 'visits';
    protected $fillable = [
        'system',
        'item_id',
        'user_id',
        'status',
        'ip',
    ];
//
    public $errors;
    public $rules = [
        'item_id' => 'required',
        'system' => 'required',
        'status' => 'required',
    ];

    public function addVisitItem($params)
    {

        $fill = $this->fill($params);
        $valid = $fill->isValid();
        if (!$valid) {
            return false;
        }
        $this->save();
    }

    public function getVisitedItem($system = 'comment', $item_id, $count = true, $user_id = 0)
    {

        $result = $this->where('system', $system)
            ->where('item_id', $item_id)
            ->where(function ($query) use ($user_id) {
                if ($user_id) {
                    $query->where('user_id', $user_id);
                }
            });

        if (!$count) {
            $result = $result->get();
        } else {
            $result = $result->count();
        }

        return $result;

    }


} 