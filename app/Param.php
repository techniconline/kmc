<?php namespace App;

class Param extends BaseModel
{

    protected $table = 'parameters';
    protected $fillable = ['name', 'alias', 'value', 'to_value', 'type', 'status'];

    public $errors;
    public $rules = [
        'name' => 'required',
        'alias' => 'required',
        'value' => 'required',
    ];


} 