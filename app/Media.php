<?php

/**
 * Created by PhpStorm.
 * User: Hamid
 * Date: 13/01/2015
 * Time: 09:03 AM
 */
namespace App;

use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class Media extends BaseModel
{

    protected $table = 'media';
    protected $primaryKey = 'id';
    protected $fillable = ['src', 'user_id', 'employee_id', 'status', 'type'];

    protected $hidden = ['remember_token'];
    public $errors;
    public $rules = ['src' => 'required'];

    public function isValid()
    {
        $validation = Validator::make($this->attributes, $this->rules);
        if ($validation->fails()) {
            $this->errors = $validation->messages();
            return false;
        }
        return true;
    }

    public function saveMedia($data, $type = 'image')
    {

        $input['user_id'] = $user_id = 0;
        $input['employee_id'] = $employee_id = 0;

        if (isset(Auth::employee()->get()->id)) {
            $employee_id = Auth::employee()->get()->id;
            $input['employee_id'] = $employee_id;

        } elseif (isset(Auth::user()->get()->id)) {
            $user_id = Auth::user()->get()->id;
            $input['user_id'] = $user_id;
        }

        if (!$employee_id && !$user_id) {
            $input['user_id'] = 999999999;
        }

        $input['src'] = $data['src'];
        $input['type'] = $type;
        $input['status'] = 1;
        $fill = $this->fill($input);
        $isValid = $fill->isValid();
        if (!$isValid)
            return 0;

        $this->save();

        if ($id = $this->id) {
            return $id;
        }

        return false;

    }

    public function getRoomMedia($media_id = 0, $product_id = 0, $room_category_id = 0)
    {

        $result = false;
        if ($media_id) {
            $result = DB::table('product_media')->where('media_id', '=', $media_id)->first();
        } elseif ($product_id) {
            $result = DB::table('product_media')->where('product_id', '=', $product_id)->get();
        } elseif ($room_category_id) {
            $result = DB::table('product_media')->where('room_category_id', '=', $room_category_id)->get();
        }

        return $result;

    }

    public function updateRoomMedia($product_media_id, $updData)
    {

        if (!$product_media_id || !$updData)
            return 0;

        $result = DB::table('product_media')
            ->where('id', $product_media_id)
            ->update($updData);

        return $result;

    }

    public function removeMedia($mediaArr = null, $product_id = 0)
    {
        $user_id = 0;
        if (Auth::employee()->get()->id || Auth::user()->get()->id) {
            $user_id = Auth::employee()->get()->id ? Auth::employee()->get()->id : Auth::user()->get()->id;
        }
        if ((!$mediaArr && !$product_id) || !$user_id)
            return 0;

        $date = date('Y-m-d H:i:s');
        $updData = array('status' => 0, 'updated_at' => $date, 'user_id' => $user_id);
        if ($mediaArr) {
            DB::table('product_media')
                ->whereIn('media_id', $mediaArr)
                ->update($updData);

            DB::table('media')
                ->whereIn('id', $mediaArr)
                ->update($updData);

            return true;
        }

        return false;

    }

    public function getMediaProduct($type = 'image', $product_id = 0)
    {
        $base_storage = URL::to('');
        $results = DB::table('product_media')
            ->join('media', 'product_media.media_id', '=', 'media.id')
            ->select('product_media.id'
                , 'product_media.media_id'
                , 'product_media.sort'
                , 'product_media.class'
                , 'product_media.feature_value_selected'
                , 'product_media.is_default'
                , 'media.src', 'media.type', 'product_media.title')
            ->where('media.type', '=', $type)
            ->where('product_media.product_id', '=', $product_id)
            ->where('media.status', '=', 1)
            ->where('product_media.status', '=', 1)
            ->orderBy('product_media.sort')
            ->get();

        foreach ($results as &$item) {
            if ($item->type == 'video') {
                $item->video = $base_storage . '/' . $item->src;
            } else {
                $item->image = $base_storage . '/' . $item->src;
            }
        }

        return $results;
    }

    public function addMediaProduct($mediaArr, $product_id = 0)
    {

        $user_id = 0;
        if (Auth::employee()->get()->id || Auth::user()->get()->id) {
            $user_id = Auth::employee()->get()->id ? Auth::employee()->get()->id : Auth::user()->get()->id;
        }

        if (!$mediaArr['ids'] || !$user_id)
            return 0;

        $date = date('Y-m-d H:i:s');
        $insertArr = [];
        $mediaModel = new Media();
        foreach ($mediaArr['ids'] as $item) {
            $title = ($mediaArr['titles'][$item]) ? $mediaArr['titles'][$item] : '';
            $resultMedia = $this->getRoomMedia($item);
            if (!$resultMedia) {
                $insertArr[] = ['title' => $title, 'media_id' => $item, 'product_id' => $product_id
                    , 'created_at' => $date, 'user_id' => $user_id, 'status' => 1];
            } else {
                //if exists this media in product_media and update...
                $updData = array(
                    'product_id' => $product_id
                , 'title' => $title
                , 'updated_at' => $date
                , 'user_id' => $user_id
                );
                $mediaModel->updateRoomMedia($resultMedia->id, $updData);
            }

        }
        if ($insertArr) {
            DB::table('product_media')->insert($insertArr);
        }
        return true;

    }

} 