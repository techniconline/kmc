<?php namespace App;

class Message extends BaseModel
{
    protected $table = 'contact_us';
    protected $fillable = ['from', 'name', 'to', 'message', 'status', 'to_id'];

    public $errors;
    public $rules = [
        'message' => 'required',
        'from' => 'required'
    ];

    public $toAttribute;

    public function __construct()
    {
        $this->toAttribute = trans('contactUs.model.itemContactUs');
//        $this->toAttribute[2] = trans('contactUs.model.Rent');
    }

    public function getToAttribute($value)
    {
        if ($a = array_key_exists($value, $this->toAttribute)) {
            return $this->toAttribute[$value];
        } else {
            return $value;
        }
    }

    public function setFromAttribute($value)
    {
        if ($a = array_search($value, $this->toAttribute)) {
            $this->attributes['from'] = $a;
        } else {
            $this->attributes['from'] = $value;
        }
    }
}