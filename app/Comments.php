<?php

/**
 * Created by PhpStorm.
 * User: Hamid
 * Date: 13/01/2015
 * Time: 09:03 AM
 */
namespace App;

use App\Providers\Helpers\PersianDate\PersianDate;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\File;

class Comments extends BaseModel
{

    protected $table = 'comments';
    protected $fillable = [
        'system',
        'item_id',
        'email',
        'user_name',
        'text',
        'parent_id',
        'user_id',
        'employee_id',
        'assessor_user_id',
        'active',
        'show_status',
        'status',
    ];
//
    public $errors;
    public $rules = [
        'email' => 'required',
        'text' => 'required',
        'item_id' => 'required',
        'system' => 'required',
    ];

    public $rulesEdit = [
        'text' => 'required',
    ];

    public function saveComment($params)
    {
        $emailUser = '';
        $userName = '';
        if (isset($params['email']) || isset($params['user_id'])) {
            $emailUser = isset($params['email']) ? ($params['email']) : '';
            $idUser = isset($params['user_id']) ? ($params['user_id']) : 0;

            $user = $params['user_id'] = User::where('status', 1)
                ->where(function ($query) use ($idUser, $emailUser) {
                    if ($emailUser)
                        $query->where('email', $emailUser);

                    if ($idUser)
                        $query->where('id', $idUser);
                })
                ->first();
            if (!$user) {
                $params['email'] = $emailUser = $params['email'];
                $params['user_name'] = $userName = $params['user_name'];
                $params['user_id'] = $idUser = 0;

            } else {
                $params['email'] = $emailUser = $user->email;
                $params['user_id'] = $idUser = $user->id;
                $params['user_name'] = $userName = $user->firstname . ' ' . $user->lastname;
            }

            if (!$emailUser)
                return ['action' => false, 'message' => Lang::get('comments.messages.errCreate')];

        } elseif (isset($params['employee_id'])) {
            $employee = $params['employee_id'] = Auth::employee()->get();

        } else {

        }

        $params['system'] = isset($params['system']) ? $params['system'] : 'news';
        $params['text'] = isset($params['text']) ? $params['text'] : '';
        $params['item_id'] = isset($params['item_id']) ? $params['item_id'] : '';
        $params['parent_id'] = isset($params['parent_id']) ? $params['parent_id'] : 0;
        $params['status'] = 1;
        $params['active'] = 1;
        $params['show_status'] = 1;

        if (!$this->fill($params)->isValid()) {
            $message = Lang::get('comments.messages.msgNotCreate');
            return ['action' => false, 'message' => $message];
        }
        $res = $this->save();
        if (!$res)
            return ['action' => false, 'message' => Lang::get('comments.messages.errCreate')];

        $message = Lang::get('comments.messages.msgCreate');

        $data = $this;
        //for media
        $params['media_ids'] = isset($params['images-docs']) ? $params['images-docs'] : 0;
        $params['comment_id'] = $data->id ? $data->id : 0;
        $resMedia = null;
        if ($params['media_ids']) {
            $resMedia = $this->saveMediaComment($params);
        }


        $date = explode(" ", $data->created_at);
        $data->sdate = PersianDate::convert_date_M_to_H($date[0]) . ' ' . $date[1];
        $data->avatar_image = ('http://www.placehold.it/85x85&text=Image');
        if (File::exists(base_path() . env('PUBLIC_PATH_URL', 'public') . '/uploads/images/' . ($data->user_id ? 'Users' : 'Employees') . '/' . ($data->user_id ? $data->user_id : $data->employee_id) . '-200x200.jpg')) {
            $data->avatar_image = asset('uploads/images/' . ($data->user_id ? 'Users' : 'Employees') . '/' . ($data->user_id ? $data->user_id : $data->employee_id) . '-200x200.jpg');
        }


        $resMail = '';

        if($commentParentId = $params['parent_id']){
            $commentParentData = $this->find($commentParentId);

            if($commentParentData){
                //send mail to admin
                $dataMail = [];
                $dataMail['username'] = Config::get('app.webSiteName');
                $dataMail['email'] = Config::get('app.MAIL_ADMIN_INFO');
                $dataMail['page'] = 'emails.send-mail-for-reply';
                $dataMail['subject'] = Config::get('app.webSiteName').' - '. trans('comments.messages.subjectReplyComment');
                $dataMail['text'] = $params['text'];

                $mailModel = new MailModel();
                $resultMail = $mailModel->sendMail($dataMail);
                $resMail = ['action' => true, 'result' => $resultMail, 'message' => Lang::get('backend/AttachType.messages.SendMailOk')];
                if (!$resultMail) {
                    $resMail = ['action' => false, 'result' => $resultMail, 'message' => Lang::get('backend/AttachType.messages.SendMailNotOk')];
                }

                //send mail to user parent
                $dataMail = [];
                $dataMail['username'] = $commentParentData->user_name;
                $dataMail['email'] = $commentParentData->email;
                $dataMail['page'] = 'emails.send-mail-for-reply';
                $dataMail['subject'] = Config::get('app.webSiteName').' - '. trans('comments.messages.subjectReplyComment');
                $dataMail['text'] = $params['text'];

                $resultMail = $mailModel->sendMail($dataMail);
                if (!$resultMail) {
                    $resMail = ['action' => false, 'result' => $resultMail, 'message' => Lang::get('backend/AttachType.messages.SendMailNotOk')];
                }
            }

        }else{

            $dataMail = [];
            $dataMail['username'] = Config::get('app.webSiteName');
            $dataMail['email'] = Config::get('app.MAIL_ADMIN_INFO');
            $dataMail['page'] = 'emails.send-mail-for-reply';
            $dataMail['subject'] = Config::get('app.webSiteName').' - '. trans('comments.messages.subjectReplyComment');
            $dataMail['text'] = $params['text'];

            $mailModel = new MailModel();
            $resultMail = $mailModel->sendMail($dataMail);
            $resMail = ['action' => true, 'result' => $resultMail, 'message' => Lang::get('backend/AttachType.messages.SendMailOk')];
            if (!$resultMail) {
                $resMail = ['action' => false, 'result' => $resultMail, 'message' => Lang::get('backend/AttachType.messages.SendMailNotOk')];
            }

        }

        $return = ['action' => true, 'message' => $message, 'resultMail' => $resMail
            , 'id' => $params['comment_id'], 'resMedia' => $resMedia
            , 'image' => $data->avatar_image, 'date' => $data->sdate
            , 'titleReply' => Lang::get("News.addcomment.Reply")];
        return $return;

    }

    public function saveMediaComment($params)
    {

        $mediaId = $params['media_ids'];
        $comment_id = $params['comment_id'];
        $item_id = $params['item_id'];
        $system = $params['system'];

        if (!$mediaId || !$comment_id)
            return false;

        $insertData = [];
        foreach ($mediaId as $item) {
            $insertData[] = ['media_id' => $item, 'comment_id' => $comment_id, 'status' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')];
        }

        $result = DB::table('comments_media')->insert(
            $insertData
        );

        $resMedia = $this->getMediaComment($system, $item_id, $comment_id);

        return $resMedia;

    }

    public function getComments($system = 'news', $item_id = 0)
    {

        $result = $this->where('system', $system)
            ->where('item_id', $item_id)
            ->where('parent_id', 0)
            ->where('active', 1)
            ->where('status', 1)
            ->orderBy('id', 'DESC');

        return $result;
    }

    public function getCommentsDetails($data, $system = 'news', $item_id = 0)
    {
        $result = $data;
        $ids = null;
        if(!$result)
            return false;

        foreach($result as $item){
            $ids[] = $item->id;
        }

        if(!$ids)
            return false;

        $resultReply = $this->where('system', $system)
            ->whereIn('id', $ids)
            ->where('item_id', $item_id)
            ->where('parent_id', '!=', 0)
            ->where('active', 1)
            ->where('status', 1)
            ->get();

        if ($resultReply) {
            $resReply = [];
            foreach ($resultReply as &$itemR) {

                $date = explode(" ", $itemR->created_at);
                $itemR->sdate = PersianDate::convert_date_M_to_H($date[0]) . ' ' . $date[1];
                $itemR->avatar_image = (asset('images/85x85.png'));
                if (File::exists(base_path() . env('PUBLIC_PATH_URL', 'public') . '/uploads/images/' . ($itemR->user_id ? 'Users' : 'Employees') . '/' . ($itemR->user_id ? $itemR->user_id : $itemR->employee_id) . '-200x200.jpg')) {
                    $itemR->avatar_image = asset('uploads/images/' . ($itemR->user_id ? 'Users' : 'Employees') . '/' . ($itemR->user_id ? $itemR->user_id : $itemR->employee_id) . '-200x200.jpg');
                }

                $resReply[$itemR->parent_id][] = $itemR;
            }

        }

        $resMedia = $this->getMediaComment($system, $item_id,0,$ids);
        $likeModel = new Likes();

        foreach ($result as &$item) {

            if (isset($resReply[$item->id])) {
                $item->replyComments = $resReply[$item->id];
            }

            if (isset($resMedia[$item->id])) {
                $item->mediaComment = $resMedia[$item->id];
            }
            $item->like_value = $likeModel->getLikesItem('comment', $item->id);

            $date = explode(" ", $item->created_at);
            $item->sdate = PersianDate::convert_date_M_to_H($date[0]) . ' ' . $date[1];
            $item->avatar_image = (asset('images/85x85.png'));
            if (File::exists(base_path() . env('PUBLIC_PATH_URL', 'public') . '/uploads/images/' . ($item->user_id ? 'Users' : 'Employees') . '/' . ($item->user_id ? $item->user_id : $item->employee_id) . '-200x200.jpg')) {
                $item->avatar_image = asset('uploads/images/' . ($item->user_id ? 'Users' : 'Employees') . '/' . ($item->user_id ? $item->user_id : $item->employee_id) . '-200x200.jpg');
            }

        }

        return $result;


    }

    public function getMediaComment($system = 'news', $item_id = 0, $comment_id = 0,$commentIds=0)
    {
        $resultMedia = DB::table('comments_media')
            ->join('comments', 'comments_media.comment_id', '=', 'comments.id')
            ->join('media', 'comments_media.media_id', '=', 'media.id')
            ->where('comments.system', $system)
            ->where('comments.item_id', $item_id)
            ->where(function ($query) use ($comment_id,$commentIds) {
                if ($comment_id) {
                    $query->where('comments_media.comment_id', $comment_id);
                }
                if($commentIds){
                    $query->whereIn('comments_media.comment_id',$commentIds);
                }
            })
            ->where('comments_media.status', 1)
            ->where('media.status', 1)
            ->select(['media.src', 'media.type', 'comments_media.*'])
            ->get();

        $resMedia = [];
        if ($resultMedia) {
            $resMedia = [];
            foreach ($resultMedia as &$itemM) {
                $itemM->src_media = '';
                if (File::exists(base_path() . env('PUBLIC_PATH_URL', 'public') . '/' . $itemM->src)) {
                    $itemM->src_media = asset($itemM->src);
                }
                $resMedia[$itemM->comment_id][] = $itemM;
            }

        }

        return $resMedia;

    }


} 