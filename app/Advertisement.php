<?php

/**
 * Created by PhpStorm.
 * User: Hamid
 * Date: 13/01/2015
 * Time: 09:03 AM
 */
namespace App;


class Advertisement extends BaseModel
{

    protected $table = 'advertisements';
    protected $fillable = ['url', 'position', 'status', 'status_show', 'slider_option', 'extension', 'type', 'group_show', 'file_path'];
//
    public $errors;
    public $rules = [
//        'url' => 'required|unique:contents,url'
    ];

    public function getAdverts($position = '', $type = 'image')
    {

        $advertisements = $this->join('advertisements_lang', 'advertisements_lang.advertisement_id', '=', 'advertisements.id')
            ->where('advertisements_lang.lang_id', '=', app('activeLangDetails')['lang_id'])
            ->where(function ($query) use ($position, $type) {
                if ($position) {
                    $query->where('advertisements.position', $position);
                }
                if ($type) {
                    $query->where('advertisements.type', $type);
                }
            })
            ->where('advertisements.status', 1)
            ->get();

        return $advertisements;
    }


} 