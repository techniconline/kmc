<?php

/**
 * Created by PhpStorm.
 * User: Hamid
 * Date: 13/01/2015
 * Time: 09:03 AM
 */
namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;

class FeatureGroup extends BaseModel
{

    protected $table = 'feature_group';
    protected $primaryKey = 'id';
    protected $fillable = ['position', 'status'];

    protected $hidden = ['remember_token'];
    public $errors;
    public $rules = ['status' => 'required'];

    public function changeFeaturesGroup($features, $feature_group_id)
    {

        if (!$features || !$feature_group_id)
            return false;

        DB::table('feature_group_relation')->where('feature_group_id', $feature_group_id)->delete();
        $insertData = [];
        foreach ($features as $item) {
            $insertData[] = ['feature_id' => $item, 'feature_group_id' => $feature_group_id];
        }

        $result = DB::table('feature_group_relation')->insert($insertData);

        return $result;

    }

    public function del($feature_group_id)
    {

        if (!$feature_group_id)
            return 0;

        $Input['user_id'] = Auth::employee()->get()->id;
        $Input['updated_at'] = date('Y-m-d H:i:s');
        $Input['status'] = 0;

        $result = DB::table($this->table)
            ->where('id', $feature_group_id)
            ->where('status', 1)
            ->update($Input);


        if ($result) {
            return array('action' => true, 'message' => Lang::get('backend/FeatureGroup.messages.msgDelOk'));
        } else {
            return array('action' => false, 'message' => Lang::get('backend/FeatureGroup.messages.msgDelNotOk'));
        }

    }

} 