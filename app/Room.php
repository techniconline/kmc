<?php

/**
 * Created by PhpStorm.
 * User: Hamid
 * Date: 13/01/2015
 * Time: 09:03 AM
 */
namespace App;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;

class Room extends BaseModel
{

    protected $table = 'rooms';
    protected $primaryKey = 'id';
    protected $fillable = ['apartment_room_category_map_id', 'room_category_id', 'room_number', 'room_status'
        , 'user_id', 'status'
    ];

    protected $hidden = ['remember_token'];
    public $errors;
    public $rules = ['apartment_room_category_map_id' => 'required|numeric'
        , 'room_number' => 'required|array'];

    /**
     * @return bool
     */
    public function isValid()
    {
        $validation = Validator::make($this->attributes, $this->rules);
        if ($validation->fails()) {
            $this->errors = $validation->messages();
            return false;
        }
        return true;
    }

    /**
     * @param $room_number
     * @param $data
     * @return array
     */
    public function generateRooms($room_number, $data)
    {
        $user_id = Auth::employee()->get()->id;
        if (!$room_number || !$data || !$user_id)
            return 0;

        $count_room = $data->Count_Room;
        $floor_number = $data->floor_number;
        if (!$floor_number)
            $floor_number = 1;

        $room_category_id = $data->room_category_id;
        $apartment_room_category_map_id = $data->id;
        $date = date('Y-m-d H:i:s');
        $insertArr = [];
        $count = 0;
        if (!$count_room) {

            for ($i = 1; $room_number >= $i; $i++) {
                $insertArr[] = ['apartment_room_category_map_id' => $apartment_room_category_map_id, 'room_category_id' => $room_category_id
                    , 'room_number' => $floor_number . $i, 'room_status' => 1
                    , 'price' => 0, 'description' => ''
                    , 'created_at' => $date, 'user_id' => $user_id, 'status' => 1];
            }

        } elseif ($room_number > $count_room) {
            $room_number = $room_number - $count_room;
            for ($i = 1; $room_number >= $i; $i++) {
                $insertArr[] = ['apartment_room_category_map_id' => $apartment_room_category_map_id, 'room_category_id' => $room_category_id
                    , 'room_number' => $floor_number . ($i + $count_room), 'room_status' => 1
                    , 'price' => 0, 'description' => ''
                    , 'created_at' => $date, 'user_id' => $user_id, 'status' => 1];
            }

        } else {
            $room_number = $count_room - $room_number;
            $count = $room_number;
            $result = DB::table('rooms')
                ->select('id')
                ->where('apartment_room_category_map_id', '=', $apartment_room_category_map_id)
                ->orderBy('id', 'DESC')
                ->limit($count)
                ->get();
            $ids = '';
            foreach ($result as $item) {
                $ids[] = $item->id;
            }
            if ($ids) {
                DB::table('rooms')
                    ->whereIn('id', $ids)
                    ->where('room_status', '=', 1)
                    ->where('status', '=', 1)
                    ->update(['room_status' => 0, 'updated_at' => $date, 'user_id' => $user_id]);

                return ['action' => true, 'count' => count($result), 'message' => Lang::get('backend/Room.messages.msgDelOk')];
            }

        }

        if ($insertArr) {
            $count = count($insertArr);
            DB::table('rooms')->insert($insertArr);
            return ['action' => true, 'count' => $count, 'message' => Lang::get('backend/Room.messages.msgInsertOk')];
        }

        return ['action' => false, 'count' => $count, 'message' => Lang::get('backend/Room.messages.errMsg')];

    }

    /**
     * @param $apartment_room_category_map_id
     * @return array
     */
    public function delRooms($apartment_room_category_map_id)
    {

        $user_id = Auth::employee()->get()->id;
        if (!$apartment_room_category_map_id || !$user_id)
            return 0;
        $date = date('Y-m-d H:i:s');

        $result = DB::table('rooms')
            ->where('apartment_room_category_map_id', '=', $apartment_room_category_map_id)
            ->where('room_status', '=', 1)
            ->where('status', '=', 1)
            ->update(['room_status' => 0, 'status' => 0, 'updated_at' => $date, 'user_id' => $user_id]);

        $resDelPic = $this->delRoomPic(0, 0, $apartment_room_category_map_id);

        if ($result) {
            return ['action' => true, 'result' => $result, 'message' => Lang::get('backend/Room.messages.msgDelOk')];
        }

        return ['action' => false, 'result' => $result, 'message' => Lang::get('backend/Room.messages.msgDelNotOk')];

    }


    public function delRoomPic($room_id = 0, $media_id = 0, $apartment_room_category_map_id = 0)
    {

        $date = date('Y-m-d H:i:s');
        $user_id = Auth::employee()->get()->id;

        if (!$user_id)
            return ['action' => false, 'is' => 'user not valid.', 'message' => Lang::get('backend/Room.messages.msgDelNotOk')];

        if ($apartment_room_category_map_id) {

            $resRoomId = DB::table('rooms')->where('apartment_room_category_map_id', $apartment_room_category_map_id)
                ->where('status', 1)
                ->lists('id');

            $result = DB::table('room_pics')
                ->where('status', 1)
                ->whereIn('room_id', $resRoomId)
                ->update(['status' => 0, 'updated_at' => $date, 'user_id' => $user_id]);

            return ['action' => true, 'result' => $result, 'message' => Lang::get('backend/Room.messages.msgDelOk')];

        } elseif ($room_id && $media_id) {


            $result = DB::table('room_pics')
                ->where('status', 1)
                ->where('room_id', $room_id)
                ->where('media_id', $media_id)
                ->update(['status' => 0, 'updated_at' => $date, 'user_id' => $user_id]);

            return ['action' => true, 'result' => $result, 'message' => Lang::get('backend/Room.messages.msgDelOk')];

        } elseif ($room_id) {

            $result = DB::table('room_pics')
                ->where('status', 1)
                ->where('room_id', $room_id)
                ->update(['status' => 0, 'updated_at' => $date, 'user_id' => $user_id]);

            return ['action' => true, 'result' => $result, 'message' => Lang::get('backend/Room.messages.msgDelOk')];

        } else {

            return ['action' => false, 'message' => Lang::get('backend/Room.messages.msgDelNotOk')];

        }


    }

    public function getRentRoomData($room_id, $lang_id)
    {
        if (!$room_id || !$lang_id)
            return false;

        $resultRoomData = DB::table('rent')
            ->join('users', 'rent.user_id', '=', 'users.id')
            ->join('apartment_room_category_map', 'rent.apartment_room_category_map_id', '=', 'apartment_room_category_map.id')
            ->join('apartments_lang', 'apartment_room_category_map.apartment_id', '=', 'apartments_lang.id')
            ->join('room_category_lang', 'apartment_room_category_map.room_category_id', '=', 'room_category_lang.id')
            ->select('rent.*'
                , 'users.firstname', 'users.lastname', 'users.gender', 'users.email'
                , 'apartments_lang.title as apartment_title'
                , 'room_category_lang.title as category_title'
            )
            ->where('apartments_lang.lang_id', '=', $lang_id)
            ->where('room_category_lang.lang_id', '=', $lang_id)
            ->where('rent.status', '!=', 0)
            ->where('rent.rent_status', '!=', 0)
            ->where('rent.room_id', $room_id)
            ->orderBy('rent.id', 'DESC')
            ->first();

        return $resultRoomData;
    }

    public function getRoomPictures($room_id)
    {
        if (!$room_id)
            return false;

        $resultPictures = DB::table('room_pics')
            ->where('status', 1)
            ->where('room_id', $room_id)
            ->get();

        return $resultPictures;
    }

    public function getRoomPicturesList($room_id)
    {
        if (!$room_id)
            return false;

        $resultPictures = DB::table('room_pics')
            ->join('media', 'room_pics.media_id', '=', 'media.id')
            ->where('room_pics.status', 1)
            ->where('media.status', 1)
            ->where('room_pics.room_id', $room_id)
            ->select(['room_pics.*', 'media.src', 'media.type'])
            ->get();

        return $resultPictures;
    }

    public function savePictures($room_id, $inputs)
    {

        $user_id = Auth::employee()->get()->id;
        if (!$room_id || !$inputs || !$user_id)
            return false;

        $resPic = $this->getRoomPictures($room_id);
        $countPic = count($resPic);
        $countPicNew = count($inputs);
        $countIsOk = 15;

        $totalCount = $countIsOk - $countPic;

        if ($totalCount >= $countPicNew) {

            $counter = 0;
            $insertArr = [];
            foreach ($inputs as $item) {
                $countPic++;
                $counter++;
                $insertArr[] = [
                    'media_id' => $item['media_id']
                    , 'room_id' => $item['room_id']
                    , 'title' => 'Picture ' . $countPic
                    , 'is_default' => 0
                    , 'status' => 1
                    , 'user_id' => $user_id
                    , 'created_at' => date('Y-m-d H:i:s')
                ];
            }

            $resInsert = DB::table('room_pics')->insert($insertArr);

        } else {
            return ['action' => false, 'message' => Lang::get('backend/Room.messages.msgInsertPicsCountNotOk'), 'countIsOkPics' => $totalCount];
        }

        return ['action' => true, 'message' => Lang::get('backend/Room.messages.msgInsertOk'), 'countInsertedPics' => $counter, 'result' => $resInsert];


    }


    public function updateRooms($apartment_room_category_map_id, $input)
    {
        $user_id = Auth::employee()->get()->id;
        $data = $this->where('room_status', '!=', 0)
            ->where('status', '=', 1)
            ->where('apartment_room_category_map_id', '=', $apartment_room_category_map_id)
            ->select('id')
            ->get();

        if (!$apartment_room_category_map_id || !$user_id || !$input || !$data)
            return 0;

        $date = date('Y-m-d H:i:s');

        $counter = 0;
        foreach ($data as $item) {
            $roomValue = isset($input['room_number'][$item->id]) ? $input['room_number'][$item->id] : 0;
            $roomValuePrice = isset($input['room_price'][$item->id]) ? $input['room_price'][$item->id] : 0;
            $roomValueDesc = isset($input['room_desc'][$item->id]) ? $input['room_desc'][$item->id] : '';
            $roomValueWall_status = isset($input['wall_status'][$item->id]) ? $input['wall_status'][$item->id] : 'good';
            $roomValueFacilities_status = isset($input['facilities_status'][$item->id]) ? $input['facilities_status'][$item->id] : 'good';
            $roomValueDoors_status = isset($input['doors_status'][$item->id]) ? $input['doors_status'][$item->id] : 'good';
            $roomValueMedia_tools_status = isset($input['media_tools_status'][$item->id]) ? $input['media_tools_status'][$item->id] : 'good';
            $roomValueFloor_status = isset($input['floor_status'][$item->id]) ? $input['floor_status'][$item->id] : 'good';
            $roomValueTotal_status = isset($input['total_status'][$item->id]) ? $input['total_status'][$item->id] : 'good';

            $roomValue_digit_electric_controller = isset($input['digit_electric_controller'][$item->id]) ? $input['digit_electric_controller'][$item->id] : 0;
            $roomValue_password_internet = isset($input['password_internet'][$item->id]) ? $input['password_internet'][$item->id] : 0;
            $roomValue_username_internet = isset($input['username_internet'][$item->id]) ? $input['username_internet'][$item->id] : 0;
            $result = DB::table('rooms')
                ->where('room_status', '!=', 0)
                ->where('status', '=', 1)
                ->where('id', '=', $item->id)
//                ->where('room_number', '!=', $roomValue)
                ->update(['room_number' => $roomValue, 'price' => $roomValuePrice
                    , 'description' => $roomValueDesc
                    , 'wall_status' => $roomValueWall_status
                    , 'facilities_status' => $roomValueFacilities_status
                    , 'doors_status' => $roomValueDoors_status
                    , 'media_tools_status' => $roomValueMedia_tools_status
                    , 'floor_status' => $roomValueFloor_status
                    , 'total_status' => $roomValueTotal_status
                    , 'username_internet' => $roomValue_username_internet
                    , 'password_internet' => $roomValue_password_internet
                    , 'digit_electric_controller' => $roomValue_digit_electric_controller
                    , 'updated_at' => $date, 'user_id' => $user_id]);

            if ($result)
                $counter++;
        }

        if ($counter) {
            return ['action' => true, 'count' => $counter, 'message' => Lang::get('backend/Room.messages.msgUpdateOk')];
        } else {
            return ['action' => false, 'count' => $counter, 'message' => Lang::get('backend/Room.messages.msgUpdateNotOk')];
        }

    }

    public function addReservationRoom($params)
    {

        $room_id = isset($params['room_id']) ? $params['room_id'] : 0;
        $booking_id = isset($params['booking_id']) ? $params['booking_id'] : 0;
        $user_id = isset($params['user_id']) ? $params['user_id'] : 0;
        $assessor_user_id = isset($params['assessor_user_id']) ? $params['assessor_user_id'] : 0;
        $room_status = isset($params['room_status']) ? $params['room_status'] : 2;

        if (!$user_id || !$booking_id || !$room_id || !$assessor_user_id)
            return false;

        $resRoom = DB::table('rooms')
            ->join('room_category', 'room_category.id', '=', 'rooms.room_category_id')
            ->select([
                'rooms.*'
                , 'room_category.type'
            ])
            ->where('rooms.id', '=', $room_id)
            ->where('room_category.status', '=', 1)
            ->whereNotIn('rooms.room_status', [0, 2, 3])// 0 for remove , 2 for reservation , 3 for rent
            ->first();

        $resBooking = DB::table('booking')
            ->where('id', $booking_id)
            ->where('user_id', $user_id)
            ->where('status', 1)
            ->first();

        if (!$resRoom || !$resBooking)
            return false;

        $apartment_room_category_map_id = $resRoom->apartment_room_category_map_id;
        $room_category_id = $resRoom->room_category_id;
        $type = $resRoom->type;

        $fromDate = $resBooking->from_date;
        $toDate = $resBooking->to_date;

        $result = DB::table('rent')
            ->where('room_id', $room_id)
            ->where('status', 1)
            ->whereIn('rent_status', [2, 3])// 0 for free , 2 for reservation , 3 for rent
            ->first();

        if ($result)
            return ['action' => false, 'message' => Lang::get('backend/Room.messages.thisRoomNotValid')];


        $resultReservation = DB::table('rent')
            ->where('booking_id', $booking_id)
            ->where('type', $type)
            ->where('user_id', $user_id)
            ->where('status', 1)
            ->whereIn('rent_status', [1, 2, 3])// 1 for active , 2 for reservation , 3 for rent
            ->first();

        if ($resultReservation)
            return ['action' => false, 'message' => Lang::get('backend/Room.messages.thisRoomReservedForUser')];


        $idRent = DB::table('rent')->insertGetId(
            [
                'room_category_id' => $room_category_id
                , 'booking_id' => $booking_id
                , 'apartment_room_category_map_id' => $apartment_room_category_map_id
                , 'room_id' => $room_id
                , 'type' => $type
                , 'from_date' => $fromDate
                , 'to_date' => $toDate
                , 'user_id' => $user_id
                , 'assessor_user_id' => $assessor_user_id
                , 'rent_status' => $room_status
                , 'status' => 1
                , 'created_at' => date('Y-m-d H:i:s')
            ]
        );

        if (!$idRent)
            return ['action' => false, 'message' => Lang::get('backend/Room.messages.msgInsertRentNotOk')];

        $resUpd = DB::table('rooms')
            ->where('id', $room_id)
            ->update([
                'room_status' => $room_status
                , 'user_id' => $assessor_user_id
                , 'updated_at' => date('Y-m-d H:i:s')
            ]);

        if (!$resUpd)
            return ['action' => false, 'message' => Lang::get('backend/Room.messages.msgUpdateNotOk')];

        return ['action' => true, 'message' => Lang::get('backend/Room.messages.msgInsertReservationOk'), 'type' => $type];

    }

    public function addRentRoom($params)
    {


        $room_id = isset($params['room_id']) ? $params['room_id'] : 0;
        $booking_id = isset($params['booking_id']) ? $params['booking_id'] : 0;
        $user_id = isset($params['user_id']) ? $params['user_id'] : 0;
        $assessor_user_id = isset($params['assessor_user_id']) ? $params['assessor_user_id'] : 0;
        $room_status = isset($params['room_status']) ? $params['room_status'] : 3;

        if (!$user_id || !$booking_id || !$room_id || !$assessor_user_id)
            return false;

        $resRoom = DB::table('rooms')
            ->join('room_category', 'room_category.id', '=', 'rooms.room_category_id')
            ->select([
                'rooms.*'
                , 'room_category.type'
            ])
            ->where('rooms.id', '=', $room_id)
            ->where('room_category.status', '=', 1)
            ->whereNotIn('rooms.room_status', [0, 3])// 0 for remove , 3 for rent
            ->first();

        $resBooking = DB::table('booking')
            ->where('id', $booking_id)
            ->where('user_id', $user_id)
            ->where('status', 1)
            ->first();

        if (!$resRoom || !$resBooking)
            return false;

        $apartment_room_category_map_id = $resRoom->apartment_room_category_map_id;
        $room_category_id = $resRoom->room_category_id;
        $type = $resRoom->type;

        $fromDate = $resBooking->from_date;
        $toDate = $resBooking->to_date;

        //find reservation room
        $result = DB::table('rent')
            ->where('room_id', $room_id)
            ->where('user_id', $user_id)
            ->where('status', 1)
            ->where('rent_status', 2)
            ->first();

        if (!$result)
            return ['action' => false, 'message' => Lang::get('backend/Room.messages.thisRoomInNotReserved')];

        $rent_id = $result->id;

        $resUpdRent = DB::table('rent')
            ->where('id', $rent_id)
            ->where('user_id', $user_id)
            ->update([
                'room_category_id' => $room_category_id
                , 'booking_id' => $booking_id
                , 'apartment_room_category_map_id' => $apartment_room_category_map_id
                , 'rent_status' => $room_status
                , 'room_id' => $room_id
                , 'type' => $type
                , 'from_date' => $fromDate
                , 'to_date' => $toDate
                , 'assessor_user_id' => $assessor_user_id
                , 'updated_at' => date('Y-m-d H:i:s')
            ]);


        if (!$resUpdRent)
            return ['action' => false, 'message' => Lang::get('backend/Room.messages.msgUpdateReservationNotOk')];

        $resUpd = DB::table('rooms')
            ->where('id', $room_id)
            ->update([
                'room_status' => $room_status
                , 'user_id' => $assessor_user_id
                , 'updated_at' => date('Y-m-d H:i:s')
            ]);

        //After add rent for update booking_details OR booking
        $resUpdBookingDetails = DB::table('booking_details')
            ->where('booking_id', $booking_id)
            ->where('type', $type)
            ->where('status', 1)
            ->update([
                'rent_status' => $room_status
                , 'updated_at' => date('Y-m-d H:i:s')
            ]);

        $resBookingDetails = DB::table('booking_details')
            ->where('booking_id', $booking_id)
            ->where('status', 1)
            ->where('rent_status', 0)
            ->get();

        if (!count($resBookingDetails)) {
            $resUpdBooking = DB::table('booking')
                ->where('id', $booking_id)
                ->update([
                    'status' => $room_status
                    , 'updated_at' => date('Y-m-d H:i:s')
                ]);
        }


        if (!$resUpd)
            return ['action' => false, 'message' => Lang::get('backend/Room.messages.msgUpdateNotOk')];

        return ['action' => true, 'message' => Lang::get('backend/Room.messages.msgInsertRentOk'), 'type' => $type];

    }

    public function cancelRentRoom($params)
    {


        $rent_id = isset($params['rent_id']) ? $params['rent_id'] : 0;
        $user_id = isset($params['user_id']) ? $params['user_id'] : 0;
        $assessor_user_id = isset($params['assessor_user_id']) ? $params['assessor_user_id'] : 0;
        $room_status = isset($params['room_status']) ? $params['room_status'] : 0;
        $cancelBooking = isset($params['cancelBooking']) ? $params['cancelBooking'] : 0;

        if (!$user_id || !$rent_id || !$assessor_user_id)
            return false;

        //find reservation room
        $result = DB::table('rent')
            ->where('id', $rent_id)
            ->where('status', 1)
            ->first();

        if (!$result)
            return ['action' => false, 'message' => Lang::get('backend/Room.messages.thisRoomInNotReserved')];

        $room_id = $result->room_id;
        $booking_id = $result->booking_id;
        $cur_rent_status = $result->rent_status;
        $type = $result->type;

        $resUpdRent = DB::table('rent')
            ->where('id', $rent_id)
            ->update([
                'status' => $room_status
                , 'rent_status' => $room_status
                , 'assessor_user_id' => $assessor_user_id
                , 'updated_at' => date('Y-m-d H:i:s')
            ]);


        if (!$resUpdRent)
            return ['action' => false, 'message' => Lang::get('backend/Room.messages.msgUpdateReservationNotOk')];

        $resUpd = DB::table('rooms')
            ->where('id', $room_id)
            ->update([
                'room_status' => 1
                , 'user_id' => $assessor_user_id
                , 'updated_at' => date('Y-m-d H:i:s')
            ]);


        $booking_status = 1; //for cancel reserved room
        $bookingD_status = 0; //for cancel reserved room


        if ($cancelBooking) {
            $resUpdBookingDetails = DB::table('booking_details')
                ->where('booking_id', $booking_id)
                ->where('type', $type)
                ->where('status', 1)
                ->update([
                    'status' => 0
                    , 'rent_status' => 10
                    , 'updated_at' => date('Y-m-d H:i:s')
                ]);

            $resultBookingDetails = DB::table('booking_details')
                ->where('booking_id', $booking_id)
                ->where('status', 1)
                ->get();

            if (!$resultBookingDetails) {
                $resUpdBooking = DB::table('booking')
                    ->where('id', $booking_id)
                    ->update([
                        'status' => 10
                        , 'updated_at' => date('Y-m-d H:i:s')
                    ]);
            }

        } else {
            $resUpdBookingDetails = DB::table('booking_details')
                ->where('booking_id', $booking_id)
                ->where('type', $type)
                ->where('status', 1)
                ->update([
                    'rent_status' => $bookingD_status
                    , 'updated_at' => date('Y-m-d H:i:s')
                ]);

            $resUpdBooking = DB::table('booking')
                ->where('id', $booking_id)
                ->update([
                    'status' => $booking_status
                    , 'updated_at' => date('Y-m-d H:i:s')
                ]);

        }


        if (!$resUpd)
            return ['action' => false, 'message' => Lang::get('backend/Room.messages.msgUpdateNotOk')];

        return ['action' => true, 'message' => Lang::get('backend/Room.messages.msgCancelRentOk')];

    }

    public function getListRooms($params = null)
    {

        $lang_id = isset($params['lang_id']) ? $params['lang_id'] : 0;
        $apartment_room_category_map_id = isset($params['apartment_room_category_map_id']) ? $params['apartment_room_category_map_id'] : 0;
        $not_apartment_room_category_map_ids = isset($params['not_apartment_room_category_map_ids']) ? $params['not_apartment_room_category_map_ids'] : 0;
        $apartment_room_category_map_ids = isset($params['apartment_room_category_map_ids']) ? $params['apartment_room_category_map_ids'] : 0;
        $type = isset($params['type']) ? $params['type'] : 0;

        if (!$lang_id)
            return false;

        $result = DB::table('rooms')
            ->join('apartment_room_category_map', 'rooms.apartment_room_category_map_id', '=', 'apartment_room_category_map.id')
            ->join('apartments', 'apartment_room_category_map.apartment_id', '=', 'apartments.id')
            ->join('apartments_lang', 'apartments_lang.id', '=', 'apartments.id')
            ->join('room_category', 'apartment_room_category_map.room_category_id', '=', 'room_category.id')
            ->join('room_category_lang', 'room_category.id', '=', 'room_category_lang.id')
            ->leftJoin('apartment_room_price', function ($join) {
                $join->on('apartment_room_category_map.room_category_id', '=', 'apartment_room_price.room_category_id')
                    ->where('apartment_room_price.price_status', '=', 1)
                    ->where('apartment_room_price.status', '=', 1);
            })
            ->leftJoin('rent', function ($joinL) {

                $joinL->on('rent.room_id', '=', 'rooms.id')
                    ->where('rent.to_date', '>', date('Y-m-d'))
                    ->where('rent.rent_status', '!=', 0)
                    ->where('rent.status', '!=', 0);

            })
            ->where(function ($query) use ($type, $apartment_room_category_map_id, $not_apartment_room_category_map_ids, $apartment_room_category_map_ids) {
                if ($apartment_room_category_map_id) {
                    $query->where('apartment_room_category_map.id', $apartment_room_category_map_id);
                }
                if ($type) {
                    $query->where('room_category.type', $type);
                }
                if ($not_apartment_room_category_map_ids) {
                    $query->whereNotIn('apartment_room_category_map.id', $not_apartment_room_category_map_ids);
                }
                if ($apartment_room_category_map_ids) {
                    $query->whereIn('apartment_room_category_map.id', $apartment_room_category_map_ids);
                }
            })
            ->where('room_category_lang.lang_id', '=', $lang_id)
            ->where('apartments_lang.lang_id', '=', $lang_id)
            ->where('apartments.status', '=', 1)
            ->where('apartment_room_category_map.status', '=', 1)
            ->where('room_category.status', '=', 1)
            ->select([
//                'apartment_room_category_map.room_number'
//                ,'apartment_room_category_map.floor_number'
//                ,'apartment_room_price.rent_price'
//                ,'room_category.area','room_category.type'
//                ,'apartments_lang.title as apartment_title'
//                ,'room_category_lang.title as category_title'
//                ,'room_category_lang.short_description'
//                ,'room_category_lang.description'
                'rooms.*'
                , 'rent.room_id'
                , 'rent.rent_status'
                , DB::raw('CONCAT_WS(" - ",apartments_lang.title,CONCAT("( type:",room_category_lang.title," )")
                                                    ,CONCAT("( Number: ",rooms.room_number," ) ")
                                                    ,CONCAT("( Area: ",room_category.area,"~",room_category.to_area," ) ")
                                                    ,CONCAT("( Price: ",IF(rooms.price>0,rooms.price, CONCAT(apartment_room_price.rent_price,"~",apartment_room_price.to_rent_price) )," ) ")
                                                    , IF(rent.room_id>0 AND rent.rent_status=2,"<b style=\"color: #ff0000\">Reserved</b>"
                                                    ,IF(rent.room_id>0 AND rent.rent_status=3,"<b style=\"color: #00ff00\">Rented</b>" ,"")
                                                    )) as title_list')
            ])
            ->orderBy('rooms.id')
            ->orderBy('apartment_room_category_map.id')
            ->orderBy('room_category.area', 'DESC')
            ->orderBy('apartment_room_price.rent_price', 'DESC')
//            ->get();
            ->lists('title_list', 'rooms.id');

        return $result;

    }

    public function getListRoomsRented($params = null)
    {

        $lang_id = isset($params['lang_id']) ? $params['lang_id'] : 0;
        $type = isset($params['type']) ? $params['type'] : 0;
        $user_id = isset($params['user_id']) ? $params['user_id'] : 0;
        $booking_id = isset($params['booking_id']) ? $params['booking_id'] : 0;
        $paginateNum = isset($params['paginateNum']) ? $params['paginateNum'] : 10;

        $sort = $params['sort'];
        $room_number = $params['room_number'];
        $date_from = $params['date_from'];
        $date_to = $params['date_to'];
        $rent_status = $params['rent_status'];

        $first_name = $params['first_name'];
        $last_name = $params['last_name'];

        $gender = $params['gender'];

        if (!$lang_id)
            return false;

        $result = DB::table('rooms')
            ->join('apartment_room_category_map', 'rooms.apartment_room_category_map_id', '=', 'apartment_room_category_map.id')
            ->join('apartments', 'apartment_room_category_map.apartment_id', '=', 'apartments.id')
            ->join('apartments_lang', 'apartments_lang.id', '=', 'apartments.id')
            ->join('room_category', 'apartment_room_category_map.room_category_id', '=', 'room_category.id')
            ->join('room_category_lang', 'room_category.id', '=', 'room_category_lang.id')
            ->leftJoin('apartment_room_price', function ($join) {
                $join->on('apartment_room_category_map.room_category_id', '=', 'apartment_room_price.room_category_id')
                    ->where('apartment_room_price.price_status', '=', 1)
                    ->where('apartment_room_price.status', '=', 1);
            })
            ->leftJoin('rent', function ($join2) {
                $join2->on('rooms.id', '=', 'rent.room_id')
                    ->where('rent.status', '!=', 0)
                    ->where('rent.rent_status', '!=', 0);
            })
            ->leftJoin('users', function ($join1) {
                $join1->on('rent.user_id', '=', 'users.id')->where('users.status', '!=', 0);
            })
            ->where(function ($query) use ($type, $first_name, $last_name, $gender, $room_number, $user_id, $booking_id, $date_from, $date_to, $rent_status) {
                if ($rent_status) {
                    $query->where('rent.rent_status', '=', $rent_status);
                }
                if ($user_id) {
                    $query->where('rent.user_id', $user_id);
                }
                if ($booking_id) {
                    $query->where('rent.booking_id', $booking_id);
                }
                if ($date_from) {
                    $query->where('rent.from_date', '>=', $date_from);
                }
                if ($date_to) {
                    $query->where('rent.to_date', '<=', $date_to);
                }
                if ($type) {
                    $query->where('room_category.type', $type);
                }
                if ($room_number) {
                    $query->where('rooms.room_number', 'like', '%' . $room_number . '%');
                }

                if ($first_name) {
                    $query->where('users.firstname', 'like', '%' . $first_name . '%');
                }
                if ($last_name) {
                    $query->where('users.lastname', 'like', '%' . $last_name . '%');
                }
                if ($gender) {
                    $query->where('users.gender', '=', $gender);
                }
            })
            ->where('room_category_lang.lang_id', '=', $lang_id)
            ->where('apartments_lang.lang_id', '=', $lang_id)
            ->where('apartments.status', '=', 1)
            ->where('apartment_room_category_map.status', '=', 1)
            ->where('room_category.status', '=', 1)
            ->where('rooms.status', '!=', 0)
            ->select([
                'apartment_room_category_map.floor_number'
                , 'apartment_room_price.rent_price'
                , 'room_category.area', 'room_category.to_area', 'room_category.type'
                , 'apartments_lang.title as apartment_title'
                , 'room_category_lang.title as category_title'
                , 'room_category_lang.short_description'
                , 'room_category_lang.description'
                , 'rent.booking_id', 'rent.room_id', 'rent.user_id', 'rent.from_date', 'rent.to_date'
                , 'rent.rent_status', 'rent.status', 'rent.created_at', 'rent.id'
                , 'users.firstname', 'users.lastname', 'users.gender', 'users.email', 'users.mobile', 'users.created_at as register_date_user'
                , 'rooms.room_number', 'rooms.price', 'rooms.id as id_room'
//                ,DB::raw('CONCAT_WS(" - ",apartments_lang.title,CONCAT("( type:",room_category_lang.title," )")
//                                                    ,CONCAT("( Number: ",rooms.room_number," ) ")
//                                                    ,CONCAT("( Area: ",room_category.area,"~",room_category.to_area," ) ")
//                                                    ,CONCAT("( Price: ",IF(rooms.price>0,rooms.price, CONCAT(apartment_room_price.rent_price,"~",apartment_room_price.to_rent_price) )," ) ")
//                                                    , IF(rent.room_id>0 AND rent.rent_status=2,"<b style=\"color: #ff0000\">Reserved</b>"
//                                                    ,IF(rent.room_id>0 AND rent.rent_status=3,"<b style=\"color: #00ff00\">Rented</b>" ,"")
//                                                    )) as title_list')
            ])
            ->orderBy('rent.rent_status', 'DESC')
            ->orderBy('room_category.type')
            ->orderBy('rent.id')
            ->orderBy('rooms.id')
            ->orderBy('rooms.price', 'DESC')
            ->orderBy('rent.type')
            ->paginate($paginateNum);

        return $result;

    }


} 