var UINestable = function () {

    var updateOutput = function (e) {
        //var list = e.length ? e : $(e.target),
        //    output = list.data('output');
        //if (window.JSON) {
        //    output.val(window.JSON.stringify(list.nestable('serialize'))); //, null, 2));
        //} else {
        //    output.val('JSON browser support required for this demo.');
        //}
    };


    return {
        //main function to initiate the module
        init: function () {

            $('#nestable_list').nestable({
                dropCallback: function (details) {
                    var parent = details.sourceEl.parents('.dd-item').first();
                    var next = details.sourceEl.next('.dd-item').first().data('id');
                    var prev = details.sourceEl.prev('.dd-item').first().data('id');
                    var parentId = parent.data('id');

                    if (details.destId != null) {
                        $.ajax({
                            url: 'changeposition',
                            method: 'POST',
                            data: {
                                prevId: prev,
                                parentId: parentId,
                                nextId: next,
                                sourceId: details.sourceId,
                                destId: details.destId
                            },
                            success: function (msg) {
                                console.log(msg);
                            }
                        });
                    }
                }
            });

            var form = $("form").clone();
            $("form").hide();
            $("a#newCat").on('click', function () {
                $(this).after(form);
                $("form").append("<input type='hidden' name='grandpa_id' value='" + $(this).data('parentid') + "'>")
                $("form").append("<input type='hidden' name='parent_id' value='" + $(this).data('id') + "'>")
                $("form").append("<input type='hidden' name='root_id' value='" + $(this).data('rootid') + "'>")
            });

            // Removing Category
            $('body').on('click', '.catremove', function () {
                var thisdom = $(this);
                $.ajax({
                    url: thisdom.siblings('.catedit').attr('data-id'),
                    data: {
                        id: thisdom.siblings('.catedit').attr('data-id')
                    },
                    method: 'DELETE',
                    success: function () {
                        thisdom.closest('li[class="dd-item dd3-item"]').hide('slow', function () {
                            thisdom.closest('li[class="dd-item dd3-item"]').remove();
                        });
                    }
                });
            });

            // Button Adding Category
            $('body').on('click', '.catadd', function () {

                var thisdom = $(this);
                var thisdompar = $(this).closest('.dd3-content');
                var parentId = thisdompar.parent().data('id');
                var next = thisdompar.next();

                if (next.length == 1) {
                    next.prepend('\
                    <li class="dd-item dd3-item" id="newCat" data-id="8">\
                        <div class="dd-handle dd3-handle">\
                        </div>\
                        <div class="dd3-content">\
                            <form action="" method="post" id="addForm">\
                                <input type="text" name="name" placeholder="Name"/>\
                                <input type="text" name="alias" placeholder="Alias"/>\
                                <input type="hidden" name="parentid" value="' + parentId + '" />\
                                <input type="submit" class="btn btn-x green">\
                            </form>\
                        </div>\
                    </li>'
                    );
                } else {
                    thisdompar.parent().append('\
                        <ol class="dd-list">\
                            <li class="dd-item dd3-item" id="newCat" data-id="4">\
                                <div class="dd-handle dd3-handle"></div>\
                                <div class="dd3-content">\
                                    <form action="" method="post" id="addForm">\
                                        <input type="text" name="name" placeholder="Name"/>\
                                        <input type="text" name="alias" placeholder="Alias"/>\
                                        <input type="hidden" name="parentid" value="' + parentId + '" />\
                                        <input type="submit" class="btn btn-x green">\
                                    </form>\
                                </div>\
                            </li>\
                        </ol>'
                    );
                }
                $('.dd3-content').find('input[name="name"]').focus();
            });

            // Adding Category
            $('#nestable_list').on('submit', function (e) {
                e.preventDefault();
                var name = $(this).find('input[name="name"]').val();
                var route = $(this).data('route');
                var route = route.split('.');
                var newId = $(this).find('#newCat');
                console.log('/backend/' + route[route.length - 1]);
                //var mythis = $(this);
                //var myparent = mythis.parent().parent().parent();
                //var mygrandpa = myparent.parent();
                //var name = $(this).parent().find($('[name=name]')).val();
                //var thisdom = $(this);
                //$(".cats_holder").sortable('enable');
                $.ajax({
                    url: '/backend/' + route[route.length - 1],
                    method: 'POST',
                    data: {
                        newCat: $(this).find('input[name="name"]').val(),
                        alias: $(this).find('input[name="alias"]').val(),
                        root_id: $(this).data('root'),
                        parent_id: $(this).find('input[name="parentid"]').val()
                    },
                    success: function (data) {
                        //$(this).find('#newCat').attr('data-id',data.id);
                        newId.data('id', data.id);
                        $('#addForm').parent().empty().html('' +
                            '<a href="/backend/' + route[route.length - 1] + '/' + data.id + '-' + data.alias + '">' + name + '</a>' +
                                //'<div style="margin-top:-2px;display: inline;float: right;">' +
                            '<a class="catLeft catadd btn btn-xs green-meadow"><i class="fa fa-plus"></i> Add Child</a>' +
                            '<a href="/backend/' + route[route.length - 1] + '/' + data.id + '/edit" class="catLeft catedit btn btn-xs purple-plum " data-id="' + data.id + '"><i class="fa fa-edit"></i> Edit</a>' +
                            '<a class="catRight catremove btn btn-xs red-sunglo"><i class="fa fa-times"></i> Deleted</a>'
                            //'</div>'
                        )

                        console.log(newId.data('id'));
                    }
                });
            });

            // activate Nestable for list 1
            //$('#nestable_list_1').nestable({
            //    group: 1
            //})
            //    .on('change', updateOutput);
            //

            // output initial serialised data
            //updateOutput($('#nestable_list_1').data('output', $('#nestable_list_1_output')));

            $('#nestable_list_menu').on('click', function (e) {
                var target = $(e.target),
                    action = target.data('action');
                if (action === 'expand-all') {
                    $('.dd').nestable('expandAll');
                }
                if (action === 'collapse-all') {
                    $('.dd').nestable('collapseAll');
                }
            });

            //$('#nestable_list_3').nestable();

        }

    };

}();