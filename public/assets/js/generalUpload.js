$(document).ready(function () {
    var subFolder = direct;
    var inputs = [];
    $("#msgUploaderImage").hide();
    $("#msgUploaderVideo").hide();
    //-----Image Upload
    var uploadObjImg = $("#imageUploader").uploadFile({
        url: baseUrl + '?type=images',
        method: 'POST',
        //allowedTypes:"png,gif,jpg,jpeg",
        formData: {sf: subFolder},
        fileName: "myfile",
        autoSubmit: false,
        maxFileSize: maxFileSizeImg,
        dragDropStr: "<span><b>Upload Image files, with Drag & Drop Files</b></span>",
        onSuccess: function (files, data, xhr) {
            data = $.parseJSON(data);
            if (data.valid) {
                inputs = ['<label><input class="icheck" data-checkbox="icheckbox_square-grey" type="checkbox" checked value="' + data.id + '" name="image[]"></label>'];
                inputsTitle = ['<input placeholder="Title, example:' + files + '" type="text" class="form-control input-medium" value="" name="image_title[' + data.id + ']">'];
                $("#msgUploaderImage").append('<p>' + data.msg + ' ' + inputs + ' (save) ' + inputsTitle + ' </p>').show();
            } else {
                $("#msgUploaderImage").append('<p>' + data.msg + '</p>').show();
            }
        }
    });
    $("#startUploadImage").click(function () {
        uploadObjImg.startUpload();
    });
    //-----Video Upload
    var uploadObjVideo = $("#videoUploader").uploadFile({
        url: baseUrl + '?type=videos',
        method: 'POST',
        formData: {sf: subFolder},
        fileName: "myfile",
        autoSubmit: false,
        maxFileSize: maxFileSizeVideo,
        dragDropStr: "<span><b>Upload Video files, with Drag & Drop Files</b></span>",
        onSuccess: function (files, data, xhr) {
            data = $.parseJSON(data);
            if (data.valid) {
                inputs = ['<label><input class="icheck" data-checkbox="icheckbox_square-grey" type="checkbox" checked value="' + data.id + '" name="video[]"></label>'];
                inputsTitle = ['<input placeholder="Title, example:' + files + '" type="text" class="form-control input-medium" value="" name="video_title[' + data.id + ']">'];
                $("#msgUploaderVideo").append('<p>' + data.msg + ' ' + inputs + ' (save) ' + inputsTitle + ' ' + files + '</p>').show();
            } else {
                $("#msgUploaderVideo").append('<p>' + data.msg + '</p>').show();
            }
        }
    });
    $("#startUploadVideo").click(function () {
        uploadObjVideo.startUpload();
    });

    $('.table').on('click', 'input.media-default', function (event) {
        //event.preventDefault();
        var clicked = $(this);
        var mediaId = clicked.val();
        var productId = $('#product-id').val();
        var parent = clicked.parents('.table').first();
        var url = parent.attr('data-url');
        $.ajax({
            url: url,
            method: 'PUT',
            data: {
                product_id: productId,
                media_id: mediaId
            },
            success: function (data) {
                var tmpdata = (data);
                if (tmpdata.action == true) {
                    alert(tmpdata.message);
                } else {
                    alert(tmpdata.message);
                }

            }
        });

    });

});
