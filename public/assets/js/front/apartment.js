jQuery(document).ready(function () {
    if ($("#map-canvas").length > 0) {
        google.maps.event.addDomListener(window, 'load', initialize);
    }

    $('div.item-roomCategoryMap').on('click', 'a.linkAddRoomMap', function (event) {
        event.preventDefault();
        var clicked = $(this);
        var parent = clicked.parents('div.item-roomCategoryMap').first();
        var loading = parent.find('span.loading').first();
        var arcmId = clicked.attr('data-id');
        loading.fadeIn();
        $.ajax({
            url: clicked.attr('href'),
            method: 'POST',
            data: {
                arcm_id: arcmId
            },
            success: function (data) {
                var tmpdata = (data);
                loading.fadeOut();
                if (tmpdata.action == true) {
                    var modal = $('div#small');
                    modal.find('modal-body').html("");
                    parent.find('a.show-modal').click();
                    modal.find('div.modal-body').html("<p>" + tmpdata.modal_message['your_selected'] + "</p><p>" + tmpdata.modal_message['your_choose'] + "</p>");
                    clicked.html("");
                    clicked.html(tmpdata.message + ' ' + tmpdata.icon);
                } else {
                    alert(tmpdata.message);
                }

            }, error: function (ts) {
                loading.fadeOut();
                alert(ts.responseText);
                window.location.href = '/front/auth/login';
                //window.location.reload();
            }
        });

    });

    $('div.item-roomCategoryMap').on('click', 'a.delete', function (event) {
        event.preventDefault();
        var clicked = $(this);
        var id = clicked.attr('data-id');
        var parent = clicked.parents('dive.dashboard-stat').first();
        var loading = parent.find('span.loading');
        console.log(id);

        if (!id) {
            return;
        }
        var action = clicked.attr('href');
        loading.fadeIn();
        $.ajax({
            url: action,
            method: 'DELETE',
            data: {
                id: id
            },
            success: function (data) {
                var tmpdata = (data);
                loading.fadeOut();
                if (tmpdata.action == true) {
                    clicked.html(tmpdata.icon + '' + tmpdata.message);
                    window.location.reload();
                } else {
                    clicked.html(tmpdata.icon + '' + tmpdata.message);
                }

            }, error: function (ts) {
                loading.fadeOut();
                alert(ts.responseText);
                window.location.href = '/front/auth/login';
            }
        });

    });


    $('div#small').on('click', 'a.default', function (event) {
        event.preventDefault();
        var clicked = $(this);
        window.location.reload();

    });


});

