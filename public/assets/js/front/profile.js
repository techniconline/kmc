jQuery(document).ready(function () {


    $('table.table-plans').on('click', 'a.remove-register', function (event) {
        event.preventDefault();
        var clicked = $(this);
        var parent = clicked.parents('tr.tr-row').first();
        var $id = clicked.attr('data-id');
        if (!$id) {
            return;
        }

        $.ajax({
            url: clicked.attr('href'),
            method: 'DELETE',
            success: function (data) {
                var tmpdata = (data);
                if (tmpdata.action == true) {
                    alert(tmpdata.message);
                    parent.fadeOut();
                } else {
                    alert(tmpdata.message);
                }

            }
        });

    });

    $('div.row').on('click', 'a.remove-image', function (event) {
        event.preventDefault();
        var clicked = $(this);
        var parent = clicked.parents('div.item-image').first();

        $.ajax({
            url: clicked.attr('href'),
            method: 'DELETE',
            success: function (data) {
                var tmpdata = (data);
                if (tmpdata.action == true) {
                    alert(tmpdata.message);
                    parent.fadeOut();
                    parent.remove();
                } else {
                    alert(tmpdata.message);
                }

            }
        });

    });

    $('div.row').on('click', 'input#btnSaveInfo', function (event) {
        event.preventDefault();
        var clicked = $(this);
        var parent = clicked.parents('div').first();
        var $form = clicked.parents('form').first();
        var $loading = parent.find('span.loading').first();
        var objInputs = $form.find('input').not(':input[name=fax]').not(':input[type="file"]');
        var $resInput = checkInputs(objInputs);
        if ($resInput) {
            return;
        }
        $loading.fadeIn();
        $.ajax({
            url: $form.attr('action'),
            method: 'POST',
            data: $form.serialize(),
            success: function (data) {
                var tmpdata = (data);
                if (tmpdata.action == true) {
                    alert(tmpdata.message);
                    $loading.fadeOut();
                } else {
                    alert(tmpdata.message);
                }

            }
        });

    });

    //
    ////var currentUrl = window.location;
    ////var $strHash = currentUrl['hash'];
    ////var $for = $strHash.replace('#', '');
    ////console.log(currentUrl);
    ////console.log(currentUrl['hash']);
    //var isValid = false;
    ////var $label = $('ul.tabTitles').find('li[for="'+$for+'"]');
    //var divBookingDate = $('#bookingDate');
    //if(!divBookingDate.hasClass('disable')){
    //    var $label = $('ul.tabTitles').find('li.do').first();
    //    //console.log($label);
    //    if(typeof $label && $label.length>0){
    //        isValid = true;
    //    }else{
    //        isValid = true;
    //        $label = $('ul.tabTitles').find('li[last-item="true"]').first();
    //        //console.log($label);
    //    }
    //    //console.log($label,isValid);
    //}
    //
    //
    //$('div.list-app').on('click', 'li a', function (event) {
    //    event.preventDefault();
    //    var clicked = $(this);
    //    if(clicked.hasClass('disable')){
    //        var msgDisable = clicked.parents('span').first().find('span.message').text();
    //        alert(msgDisable);
    //        return;
    //    }
    //    var parent = clicked.parents('div.tabContainer').first();
    //    var parentHead = clicked.parents('div.contentRow').first();
    //    var liParent = clicked.parents('li').first();
    //    var lastitem = liParent.attr('last-item');
    //    //var panelBody = parent.find('.panel-body');
    //    var targetResult = parent.find('div.tabContents');
    //    var loading = parentHead.find('div.rgbxTitle').find('span.loading').first();
    //    var pageData = liParent.attr('data-id');
    //    loading.fadeIn();
    //    $.ajax({
    //        url: clicked.attr('href'),
    //        method: 'GET',
    //        data:{
    //            pageData: pageData,
    //            lastItem:lastitem
    //        },
    //        success: function (data) {
    //            var tmpdata = (data);
    //            parent.find('li.active').removeClass('active');
    //            liParent.addClass('active');
    //            loading.fadeOut();
    //            if (tmpdata.action == true) {
    //                targetResult.html(tmpdata.page);
    //            } else {
    //                alert(tmpdata.message);
    //            }
    //
    //        },error: function(ts) {
    //            loading.fadeOut();
    //            alert(ts.responseText);
    //            window.location.href = '/front/auth/login';
    //            //window.location.reload();
    //        }
    //    });
    //
    //});
    //
    //$('div.booking-data').on('click', '#btnSave-room', function (event) {
    //    event.preventDefault();
    //    var clicked = $(this);
    //    var form = clicked.parents('form').first();
    //    var inputs = form.serialize();
    //    var action = form.attr('action');
    //    //var $page = form.find('input[name="page"]').val();
    //    var objInputs;
    //
    //    objInputs = form.find('input');
    //
    //    var $resInput = checkInputs(objInputs);
    //    var parent = clicked.parents('div.half').first();
    //    var loading = parent.find('span.loading');
    //    var targetResult = parent.find('span.result');
    //    console.log(targetResult);
    //
    //    if($resInput){
    //        return ;
    //    }
    //    loading.fadeIn();
    //    $.ajax({
    //        url:action,
    //        method: 'PUT',
    //        data:inputs,
    //        success: function (data) {
    //            var tmpdata = (data);
    //            loading.fadeOut();
    //            if (tmpdata.action == true) {
    //
    //                targetResult.html(tmpdata.icon+''+tmpdata.message);
    //                window.location.href = window.location.href;
    //                window.location.reload();
    //            } else {
    //                targetResult.html(tmpdata.icon);
    //                if(tmpdata.messages.length>1){
    //                    $.each(tmpdata.messages, function(i, val) {
    //                        targetResult.append(val+'</br>');
    //                    });
    //                }else{
    //                    targetResult.html(tmpdata.icon+''+tmpdata.message);
    //                }
    //            }
    //
    //        },error: function(ts) {
    //            loading.fadeOut();
    //            alert(ts.responseText);
    //            window.location.href = '/front/profile';
    //            //window.location.reload();
    //        }
    //    });
    //
    //});
    //
    //$('div.booking-data').on('click', '#btnSave-date', function (event) {
    //    event.preventDefault();
    //    var clicked = $(this);
    //    var form = clicked.parents('form').first();
    //    var inputs = form.serialize();
    //    var action = form.attr('action');
    //    //var $page = form.find('input[name="page"]').val();
    //    var objInputs;
    //
    //    objInputs = form.find('input');
    //
    //    var $resInput = checkInputs(objInputs);
    //    var parent = clicked.parents('div.half').first();
    //    var loading = parent.find('span.loading');
    //    var targetResult = parent.find('span.result');
    //
    //    if($resInput){
    //        return ;
    //    }
    //    loading.fadeIn();
    //    $.ajax({
    //        url:action,
    //        method: 'PUT',
    //        data:inputs,
    //        success: function (data) {
    //            var tmpdata = (data);
    //            loading.fadeOut();
    //            if (tmpdata.action == true) {
    //
    //                targetResult.html(tmpdata.icon+''+tmpdata.message);
    //                window.location.href = window.location.href;
    //                window.location.reload();
    //            } else {
    //                targetResult.html(tmpdata.icon);
    //                if(tmpdata.messages.length>1){
    //                    $.each(tmpdata.messages, function(i, val) {
    //                        targetResult.append(val+'</br>');
    //                    });
    //                }else{
    //                    targetResult.html(tmpdata.icon+''+tmpdata.message);
    //                }
    //            }
    //
    //        },error: function(ts) {
    //            loading.fadeOut();
    //            alert(ts.responseText);
    //            window.location.href = '/front/profile';
    //            //window.location.reload();
    //        }
    //    });
    //
    //});
    //
    //$('div.tabContents').on('click', 'input#btnSave', function (event) {
    //    event.preventDefault();
    //    var clicked = $(this);
    //    var form = clicked.parents('form').first();
    //    var inputs = form.serialize();
    //    var action = form.attr('action');
    //    var $page = form.find('input[name="page"]').val();
    //    var objInputs;
    //    if($page=='user'){
    //        objInputs = form.find('input').not(':input[name=telephone]');
    //    }else if($page=='info'){
    //        objInputs = form.find('input');
    //    }else if($page=='parent'){
    //        objInputs = form.find('input');
    //    }
    //    var $resInput = checkInputs(objInputs);
    //    var parent = clicked.parents('div.tabContainer').first();
    //    var parentHead = parent.find('ul.tabTitles').first();
    //    var loading = parent.find('span.loading');
    //    var targetResult = parent.find('span.result');
    //    if($page=='info'){
    //        var accuracyInfo = form.find('#accuracy_information').prop('checked');
    //        if(!accuracyInfo){
    //            targetResult.html("Please checked accuracy information!");
    //            return;
    //        }
    //    }
    //    if($resInput){
    //        return ;
    //    }
    //    loading.fadeIn();
    //    $.ajax({
    //        url:action,
    //        method: 'PUT',
    //        data:inputs,
    //        success: function (data) {
    //            var tmpdata = (data);
    //            loading.fadeOut();
    //            if (tmpdata.action == true) {
    //                if(tmpdata.messageCompleted!=""){
    //                    alert(tmpdata.messageCompleted);
    //                    targetResult.append('<br>'+tmpdata.messageCompleted);
    //                    window.location.href = window.location.href;
    //                    window.location.reload();
    //                    return;
    //                }
    //                //var urlReload = (window.location);
    //                //var newUrl;
    //                ////console.log(parentHead,'head');
    //                //newUrl = urlReload['origin']+urlReload['pathname'];
    //                //var nextPage = parentHead.find('li[for="'+$page+'"]').nextAll('li:first');
    //                ////console.log($page,nextPage.attr('for'));
    //                //var $dataID = 0;
    //                //$page = nextPage.attr('for');
    //                //
    //                //    newUrl = newUrl + '#'+$page;
    //                //    window.location.href=newUrl;
    //
    //                    window.location.href = window.location.href;
    //                    //console.log(urlReload,'url');
    //                    //return;
    //                    window.location.reload();
    //                    //location.reload();
    //                //}else{
    //                //    window.location.href=newUrl;
    //                //}
    //                targetResult.html(tmpdata.icon+''+tmpdata.message);
    //
    //            } else {
    //                targetResult.html(tmpdata.icon+''+tmpdata.message);
    //            }
    //
    //        },error: function(ts) {
    //            loading.fadeOut();
    //            alert(ts.responseText);
    //            window.location.href = '/front/profile';
    //            //window.location.reload();
    //        }
    //    });
    //
    //});
    //
    //$('div.tabContents').on('click','div.ajax-file-upload-red', function(event){
    //    event.preventDefault();
    //    var clicked = $(this);
    //    console.log(clicked);
    //    return;
    //
    //});
    //
    //$('div.tabContents').on('click', 'input#btnSaveDoc', function (event) {
    //    event.preventDefault();
    //    var clicked = $(this);
    //
    //    var form = clicked.parents('form#formDocuments').first();
    //    var inputsMedia = form.find('input.value-media');
    //
    //    var inputArray = [];
    //    var inputItemArray = [];
    //    var $this;
    //    inputsMedia.each(function(){
    //        $this = $(this);
    //        inputArray.push({media_id:$this.val(),attach_id:$this.attr('data-item')});
    //        inputItemArray.push([$this.val(),$this.attr('data-item')]);
    //    });
    //    //console.log(inputArray);
    //    //return;
    //    var action = form.attr('action');
    //    var _token = form.find('input[name="_token"]').val();
    //    var $page = form.find('input[name="page"]').val();
    //    var $lastDocument = form.find('input[name="lastDocument"]').val();
    //
    //    var parent = clicked.parents('div.tabContainer').first();
    //    var parentHead = parent.find('ul.tabTitles').first();
    //    var loading = parent.find('span.loading').last();;
    //    var targetResult = parent.find('span.result');
    //
    //    if(!inputsMedia.length){
    //        return ;
    //    }
    //    loading.fadeIn();
    //    $.ajax({
    //        url:action,
    //        method: 'PUT',
    //        data:{
    //            items:inputItemArray,
    //            _token:_token,
    //            lastDocument:$lastDocument,
    //            page:$page
    //        },
    //        success: function (data) {
    //            var tmpdata = (data);
    //            loading.fadeOut();
    //            if (tmpdata.action == true) {
    //                targetResult.html(tmpdata.icon+''+tmpdata.message);
    //                if(tmpdata.messageCompleted!=""){
    //                    alert(tmpdata.messageCompleted);
    //                    targetResult.append('<br>'+tmpdata.messageCompleted);
    //                    window.location.href = window.location.href;
    //                    window.location.reload();
    //                    return;
    //                }
    //                //var urlReload = (window.location);
    //                //var newUrl;
    //                ////console.log(parentHead,'head');
    //                //newUrl = urlReload['origin']+urlReload['pathname'];
    //                //var nextPage = parentHead.find('li[for="'+$page+'"]').nextAll('li:first');
    //                ////console.log($page,nextPage.attr('for'));
    //                //
    //                //if (typeof nextPage.attr('for') != 'undefined'){
    //                //    $page = nextPage.attr('for');
    //                //    //console.log($page,nextPage.attr('for'),'sss');
    //                //}
    //                //
    //                //newUrl = newUrl + '#'+$page;
    //                //window.location.href=newUrl;
    //                window.location.href = window.location.href;
    //                //console.log(urlReload,'url');
    //                //return;
    //                window.location.reload();
    //            } else {
    //                targetResult.html(tmpdata.icon+''+tmpdata.message);
    //            }
    //
    //        },error: function(ts) {
    //            loading.fadeOut();
    //            alert(ts.responseText);
    //            window.location.href = '/front/profile';
    //            //window.location.reload();
    //        }
    //    });
    //
    //});
    //
    //$('div.tabContents').on('click', 'a.delete', function (event) {
    //    event.preventDefault();
    //    var clicked = $(this);
    //    var id = clicked.attr('data-id');
    //
    //    var parent = clicked.parents('span.link').first();
    //    var loading = parent.find('span.loading');
    //    //console.log(id);
    //
    //    if(!id){
    //        return ;
    //    }
    //    var action = $('input#deleteUrl').val();
    //    loading.fadeIn();
    //    $.ajax({
    //        url:action,
    //        method: 'POST',
    //        data:{
    //            op: "delete",
    //            controller: "profile",
    //            controllerDoc: "attach",
    //            id: id
    //        },
    //        success: function (data) {
    //            var tmpdata = $.parseJSON(data);
    //            loading.fadeOut();
    //            if (tmpdata.action == true) {
    //                parent.html(tmpdata.icon+''+tmpdata.message);
    //                window.location.href = window.location.href;
    //                window.location.reload();
    //            } else {
    //                parent.html(tmpdata.icon+''+tmpdata.message);
    //            }
    //
    //        },error: function(ts) {
    //            loading.fadeOut();
    //            alert(ts.responseText);
    //            window.location.href = '/front/profile';
    //            //window.location.reload();
    //        }
    //    });
    //
    //});
    //
    //$('div.booking-data').on('click', 'a.deleteBooking', function (event) {
    //    event.preventDefault();
    //    var clicked = $(this);
    //    var id = clicked.attr('data-id');
    //    var parent = clicked.parents('div.booking-list').first();
    //    var loading = clicked.find('span.loading');
    //    //console.log(id);
    //
    //    if(!id){
    //        return ;
    //    }
    //    var action = clicked.attr('href');
    //    loading.fadeIn();
    //    $.ajax({
    //        url:action,
    //        method: 'DELETE',
    //        data:{
    //            id: id
    //        },
    //        success: function (data) {
    //            var tmpdata = (data);
    //            loading.fadeOut();
    //            if (tmpdata.action == true) {
    //                clicked.html(tmpdata.icon+''+tmpdata.message);
    //                parent.find('div.srItem[data-id="'+id+'"]').remove()
    //                //
    //                //
    //                //var urlReload = (window.location);
    //                //var newUrl;
    //                //newUrl = urlReload['origin']+urlReload['pathname'];
    //                //newUrl = newUrl + '#bookingDate';
    //                //window.location.href=newUrl;
    //                window.location.href = window.location.href;
    //                //window.location.reload();
    //
    //            } else {
    //                clicked.html(tmpdata.icon+''+tmpdata.message);
    //            }
    //
    //        },error: function(ts) {
    //            loading.fadeOut();
    //            alert(ts.responseText);
    //            window.location.href = '/front/auth/login';
    //            //window.location.reload();
    //        }
    //    });
    //
    //});
    //
    //$('div.panel-body').on('click', 'a.load-msg', function (event) {
    //    event.preventDefault();
    //    var clicked = $(this);
    //    var parent = clicked.parents('tr.tr-row').first();
    //
    //    var action = clicked.attr('href');
    //    action = action.replace('/#basic', '');
    //    var modal = $('#basic');
    //    var loading = modal.find('span.loading');
    //    loading.fadeIn();
    //    var head = modal.find('h4.modal-title');
    //    var assessor = modal.find('h6.modal-assessor');
    //    var body = modal.find('div.modal-body');
    //    var showStatus = parent.find('span.label-mini b');
    //
    //    //console.log(action,modal);
    //
    //    $.ajax({
    //        url:action,
    //        method: 'GET',
    //        success: function (data) {
    //            var tmpdata = (data);
    //            loading.fadeOut();
    //            if (tmpdata.action == true) {
    //                head.html(tmpdata.subject);
    //                assessor.html(tmpdata.assessor);
    //                body.html(tmpdata.text);
    //                showStatus.html(tmpdata.status);
    //            } else {
    //
    //            }
    //
    //        },error: function(ts) {
    //            loading.fadeOut();
    //            alert(ts.responseText);
    //
    //        }
    //    });
    //
    //});

    function checkInputs(inputs) {

        var counter = 0;
        inputs.each(function () {
            var input = $(this);
            //var iInput = input.parents('div.input-group').find('i.fa');
            var iInput = input;
            if (input.val() == '') {
                iInput.css("border-color", "#FF0000");
                //input.addClass('bg-red');
                counter++;
            } else {
                iInput.removeClass('font-red');
            }
        });

        return counter;

    }

    //handleDatePickers();

    //if(isValid){
    //    $label.find('a').click();
    //}


});

var handleDatePickers = function () {

    if (jQuery().datepicker) {
        $('.date-picker').datepicker({
            //format: 'MM/DD/YYYY',
            separator: '-',
            minDate: '01/01/1900',
            maxDate: '01/01/2030',
            orientation: "left",
            autoclose: true
        });
        //$('body').removeClass("modal-open"); // fix bug when inline picker is used in modal
    }

    /* Workaround to restrict daterange past date select: http://stackoverflow.com/questions/11933173/how-to-restrict-the-selectable-date-ranges-in-bootstrap-datepicker */
}