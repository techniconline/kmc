//jQuery(document).ready(function () {

var subFolder = direct;
var inputs = [];
var targetInput;
$("#msgUploaderImage").hide();
//-----Image Upload
var uploadObjImg = $("#imageUploader").uploadFile({
    url: baseUrl + '?type=images',
    method: 'POST',
    allowedTypes: "png,gif,jpg,jpeg",
    formData: {sf: subFolder},
    fileName: "myfile",
    autoSubmit: true,
    maxFileSize: maxFileSizeImg,
    dragDropStr: "<span><b>" + titleUploader + "</b></span>",
    //abortStr:"abandonner",
    //cancelStr:"résilier",
    //doneStr:"fait",
    showDelete: true,
    showDone: false,
    onSuccess: function (files, data, xhr) {
        data = $.parseJSON(data);
        if (data.valid) {
            //inputs=['<label><input class="icheck" data-checkbox="icheckbox_square-grey" type="checkbox" checked value="'+data.id+'" name="image[]"></label>'];
            inputs = $('#list-attach').clone();
            //console.log(inputs);
            //inputsTitle=['<input placeholder="Title, example:'+files+'" type="text" class="form-control input-medium" value="" name="image_title['+data.id+']">'];
            targetInput = $('#form-uploader').find('div.ajax-file-upload-statusbar').first();
            targetInput.css('height', '150px');
            inputs.find('label.control-label').css('margin-top', '10px');
            //targetInput.append('<p>'+data.msg+' '+inputs.html()+' (save) '+inputsTitle+' </p>').show();
            targetInput.append('<p> ' + inputs.html() + ' </p>').show();
        } else {
            $("#msgUploaderImage").append('<p>' + data.msg + '</p>').show();
        }
    }
});
$("#startUploadImage").click(function () {
    uploadObjImg.startUpload();
});

//});
