jQuery(document).ready(function () {

    jQuery('ul#comments-list').on('click', 'a._like , a._dislike', function (event) {

        event.preventDefault();
        var clicked = jQuery(this);
        var $item_id = clicked.attr('data-id');
        var $valueAction = 0;
        if (clicked.hasClass('_like')) {
            $valueAction = 1;
        } else {
            $valueAction = 2;
        }
        var $params = [];
        $params['result'] = clicked.find('i');
        $params['loading'] = clicked.parents('div').first().find('span.loading-like').first();
        $params['url'] = '/front/like/comment/' + $item_id + '/' + $valueAction;
        $params['valueAction'] = $valueAction;
        myLike.init(clicked, $params);
    });

    jQuery('div#create-comment').on('click', '#btnSave-Comment', function (event) {
        event.preventDefault();
        var clicked = jQuery(this);
        var locResult = jQuery('ul#comments-list');
        var form = clicked.parents('form').first();
        var jQueryresulPatern = jQuery('<li>'
            + '<div class="comment">'
            + '<header class="comment-author author-image">'
            + '<img src="" alt=""/>'
            + '</header>'
            + '<div class="comment-details">'
            + '<div class="author-name" data-email="' + form.find('input[name="email"]').val() + '">'
            + '<a href="#">' + form.find('input[name="user_name"]').val() + '</a>'
            + '</div>'
            + '<div class="commentmetadata comment-date"> </div>'
            + '    <div class="comment-body">'
            + '        <div class="comment-content" data-id="">'
            + '        <p> ' + form.find('textarea[name="text"]').val() + ' </p>'
            + '        </div></div>'
            + '    <div class="reply">'
            + '    <a href="#commentform" class="dt-sc-button"> ' + '...' + '</a>'
            + '    </div></div></div>'
            + '</li>');
        var $params = [];

        myComment.init(clicked, locResult, jQueryresulPatern, 'false', 'false', $params);
    });

});