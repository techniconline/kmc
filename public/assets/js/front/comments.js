jQuery(document).ready(function () {

    jQuery('div.list-app').on('click', 'li a', function (event) {
        event.preventDefault();
        var clicked = jQuery(this);
        if (clicked.hasClass('disable')) {
            var msgDisable = clicked.parents('span').first().find('span.message').text();
            alert(msgDisable);
            return;
        }
        var parent = clicked.parents('div.tabContainer').first();
        var parentHead = clicked.parents('div.contentRow').first();
        var liParent = clicked.parents('li').first();
        var lastitem = liParent.attr('last-item');
        //var panelBody = parent.find('.panel-body');
        var targetResult = parent.find('div.tabContents');
        var loading = parentHead.find('div.rgbxTitle').find('span.loading').first();
        var pageData = liParent.attr('data-id');
        loading.fadeIn();
        jQuery.ajax({
            url: clicked.attr('href'),
            method: 'GET',
            data: {
                pageData: pageData,
                lastItem: lastitem
            },
            success: function (data) {
                var tmpdata = (data);
                parent.find('li.active').removeClass('active');
                liParent.addClass('active');
                loading.fadeOut();
                if (tmpdata.action == true) {
                    targetResult.html(tmpdata.page);
                } else {
                    alert(tmpdata.message);
                }

            }, error: function (ts) {
                loading.fadeOut();
                alert(ts.responseText);
                window.location.href = '/front/auth/login';
                //window.location.reload();
            }
        });

    });

});
var myComment = function () {

    return {
        init: function (clicked, locResult, jQueryresulPatern, $reply, $fileUploader, $params) {

            var form = clicked.parents('form').first();
            var inputs = form.serialize();
            var action = form.attr('action');
            var objInputs, objInputUploader;
            if ($fileUploader != 'false') {
                objInputUploader = form.find('.' + $fileUploader).find('input');
            }
            //console.log(objInputUploader)
            objInputs = form.find('input,textarea').not(objInputUploader);

            var jQueryresInput = this.checkValid(objInputs);

            //var parent = clicked.parents('div.half').first();
            var loading = form.find('span.loading');
            if ($reply == 'true') {
                loading = form.find('span.loading-reply');
            }
            var targetResult = parent.find('span.result');

            if (jQueryresInput) {
                return;
            }

            loading.fadeIn();
            jQuery.ajax({
                url: action,
                method: 'POST',
                data: inputs,
                success: function (data) {
                    var tmpdata = (data);
                    loading.fadeOut();
                    if (tmpdata.action == true) {

                        alert(tmpdata.message);
                        jQueryresulPatern.find('.author-image img').attr('src', tmpdata.image);
                        jQueryresulPatern.find('.comment-date').text(tmpdata.date);
                        jQueryresulPatern.find('div.comment-content').attr('data-id', tmpdata.id);
                        jQueryresulPatern.find('a.reply-comment').attr('data-id', tmpdata.id);
                        jQueryresulPatern.find('div.media-comment').attr('data-id', tmpdata.id);
                        jQueryresulPatern.find('ul.reply-list').attr('data-id', tmpdata.id);
                        jQueryresulPatern.find('a.dt-sc-button').text(tmpdata.titleReply);

                        if (tmpdata.resMedia) {
                            var Images = '';
                            jQuery.each(tmpdata.resMedia, function (i, val) {
                                Images = '';
                                jQuery.each(val, function (k, valItem) {
                                    Images += '<li style="border-style: none !important;"><div class="comment">';
                                    if (valItem.type == 'image') {
                                        Images += '<img src="' + valItem.src_media + '" width="100px">';
                                    } else {
                                        Images += '<a class="btn download" target="_blank" href="' + valItem.src_media + '"> Download /a>';
                                    }
                                    Images += '</div></li>';
                                });
                                jQueryresulPatern.find('ul.media-list').html(Images);
                            });


                        }

                        if ($reply == 'true') {
                            locResult.append(jQueryresulPatern);
                        } else {
                            locResult.prepend(jQueryresulPatern);
                        }
                        if ($reply == 'true') {
                            var btnClose = form.find('input.btn-close');
                            if (btnClose.length > 0) {
                                btnClose.click();
                            } else if ($params['hide_element'].length) {
                                $params['hide_element'].fadeOut();
                            }
                        }

                    } else {
                        //if(tmpdata.messages.length>1){
                        //    jQuery.each(tmpdata.messages, function(i, val) {
                        //        targetResult.append(val+'</br>');
                        //    });
                        //}else{
                        alert(tmpdata.message);
                        //}
                    }

                }, error: function (ts) {
                    loading.fadeOut();
                    alert(ts.responseText);
                    window.location.href = '/front/profile';
                    //window.location.reload();
                }
            });
        },updateEdit: function ($params){

            jQuery('div#edit-panel').on('click', 'input#btnSave-Edit-Comment', function (event) {
                event.preventDefault();
                var clicked = jQuery(this);
                var $parent = clicked.parents('div#edit-panel').first();
                var $commentId = clicked.parents('div.comment-content').first().attr('data-id');
                var loading = $parent.find('span.loading');
                var $url = $parent.attr('data-url');
                jQuery.ajax({
                    url: $url+'/'+$commentId,
                    method: 'PUT',
                    data: $parent.find('input, textarea').serialize(),
                    success: function (data) {
                        loading.fadeOut();
                        if (data.action == true) {
                            alert(data.message);
                            window.location.href = window.location.href;
                        } else {
                            alert(data.message);
                        }

                    }, error: function (ts) {
                        loading.fadeOut();
                        alert(ts.responseText);
                        window.location.href = '/front/profile';
                    }
                });


            });

        },edit: function ($params){

            var clicked = $params['clicked'];
            var item_id = $params['item_id'];
            var parent = $params['parent'];
            var editPanel = $params['edit-panel'];

            var $edit = clicked.parents('div.comment-details').first().find('div.comment-content[data-id="'+item_id+'"]').first();
            var editData = $edit.find('p').first().text();
            var resEdit = $edit.find('div.res-edit-panel').first();
            editData = jQuery.trim(editData);
            console.log(editData);
            editPanel.find('textarea').val(editData);
            resEdit.html(editPanel);
            resEdit.fadeIn();
            editPanel.fadeIn();

        },closeEditForm: function(){

            jQuery('div#edit-panel').on('click', 'input.close', function (event) {
                event.preventDefault();
                var clicked = jQuery(this);
                clicked.parents('div#edit-panel').first().fadeOut();
                clicked.parents('div.res-edit-panel').first().fadeOut();

            });
        },
        checkValid: function (inputs) {

            var counter = 0;
            inputs.each(function () {
                var input = jQuery(this);
                var iInput = input;
                if (input.val() == '') {
                    iInput.css("border-color", "#FF0000");
                    counter++;
                } else {
                    iInput.removeClass('font-red');
                }
            });

            return counter;

        }
    }
}();
