jQuery(document).ready(function () {


    jQuery('ul#comments-list').on('click', 'a._like , a._dislike', function (event) {

        event.preventDefault();
        var clicked = jQuery(this);
        var $item_id = clicked.attr('data-id');
        var $valueAction = 0;
        if (clicked.hasClass('_like')) {
            $valueAction = 1;
        } else {
            $valueAction = 2;
        }
        var $params = [];
        $params['result'] = clicked.find('i');
        $params['loading'] = clicked.parents('div').first().find('span.loading-like').first();
        $params['url'] = '/front/like/comment/' + $item_id + '/' + $valueAction;
        $params['valueAction'] = $valueAction;
        myLike.init(clicked, $params);
    });

    jQuery('ul#comments-list').on('click', 'a.edit-comment', function (event) {
        event.preventDefault();
        var clicked = jQuery(this);
        var $item_id = clicked.attr('data-id');
        var $parent = clicked.parents('li.item[data-id="'+$item_id+'"]').first();
        var $editPanel = jQuery('div#edit-panel');
        var $params = [];
        $params['clicked'] = clicked;
        $params['item_id'] = $item_id;
        $params['parent'] = $parent;
        $params['edit-panel'] = $editPanel;

        myComment.edit($params);

    });

    myComment.updateEdit();
    myComment.closeEditForm();

    jQuery('div#create-comment').on('click', '#btnSave-Comment', function (event) {
        event.preventDefault();
        var clicked = jQuery(this);
        var locResult = jQuery('ul#comments-list');
        //var $replyTitle = locResult.find('a.dt-sc-button').first().text();
        var form = clicked.parents('form').first();
        var jQueryresulPatern = jQuery('<li>'
            + '<div class="comment">'
            + '<header class="comment-author author-image">'
            + '<img src="" alt=""/>'
            + '</header>'
            + '<div class="comment-details">'
            + '<div class="author-name" data-email="' + form.find('input[name="email"]').val() + '">'
            + '<a href="#">' + form.find('input[name="user_name"]').val() + '</a>'
            + '</div>'
            + '<div class="commentmetadata comment-date"> </div>'
            + '    <div class="comment-body">'
            + '        <div class="comment-content" data-id="">'
            + '        <p> ' + form.find('textarea[name="text"]').val() + ' </p>'
            + '        </div></div>'
            + '    <div class="reply">'
            + '    <a data-id="" class="dt-sc-button reply-comment"> ' + '...' + '</a>'
            + '    </div></div>'
            + '    <div class="media-comment" data-id="">'
            + '      <ul class="media-list">'

            + '      </ul></div>'
            + '      <ul class="reply-list" data-id=""></ul>'
            + '</div></li>');
        var $params = [];
        myComment.init(clicked, locResult, jQueryresulPatern, 'false', 'file-uploader', $params);
    });

    jQuery('ul#comments-list').on('click', '#btnSave-ReplyComment', function (event) {
        event.preventDefault();
        var clicked = jQuery(this);

        var form = clicked.parents('form').first();
        var $id = form.find('input.parent-id').val();
        var locResult = jQuery('ul#comments-list').find('ul.reply-list[data-id="' + $id + '"]');
        var jQueryresulPatern = jQuery('<li>'
            + '<div class="comment">'
            + '<header class="comment-author author-image">'
            + '<img src="" alt=""/>'
            + '</header>'
            + '<div class="comment-details">'
            + '<div class="author-name" data-email="' + form.find('input[name="email"]').val() + '">'
            + '<a href="#">' + form.find('input[name="user_name"]').val() + '</a>'
            + '</div>'
            + '<div class="commentmetadata comment-date"> </div>'
            + '    <div class="comment-body">'
            + '        <div class="comment-content">'
            + '        <p> ' + form.find('textarea[name="text"]').val() + ' </p>'
            + '        </div></div>'
            + '    </div></div>'
            + '</li>');

        var $params = [];
        $params['hide_element'] = jQuery('#replyComment');

        myComment.init(clicked, locResult, jQueryresulPatern, 'true', 'false', $params);
    });

    jQuery('ul#comments-list').on('click', 'a.reply-comment', function (event) {
        event.preventDefault();
        var clicked = jQuery(this);
        var $id = clicked.attr('data-id');
        var $replyForm = jQuery('#replyComment');
        var locResult = jQuery('ul#comments-list').find('li.item[data-id="' + $id + '"]');

        if ($replyForm.length == 0) {
            alert('Not find reply form!');
            return;
        }

        $replyForm.find('input.parent-id').val($id);
        $replyForm.appendTo(locResult).fadeIn();

        setTimeout(function () {
            jQuery('html, body').animate({
                scrollTop: jQuery("#replyComment").offset().top
            }, 500);
        }, 200);

    });

    jQuery('ul#comments-list').on('click', 'a.del-comment', function (event) {
        event.preventDefault();
        var clicked = jQuery(this);
        var $id = clicked.attr('data-id');
        var parent = clicked.parents('li.item[data-id="' + $id + '"]').first();
        var $url = jQuery('ul#comments-list').attr('data-url');
        if (!parent) {
            return;
        }

        if (window.confirm("آیا برای حذف مطمئن هستید؟")) {
            jQuery.ajax({
                url: $url + '/' + $id,
                method: 'DELETE',
                success: function (data) {
                    var tmpdata = (data);

                    if (tmpdata.action == true) {
                        alert(tmpdata.message);
                        parent.fadeOut();
                        setTimeout(function () {
                            parent.remove();
                        }, 1000);
                    } else {
                        alert(tmpdata.message);
                    }

                }, error: function (ts) {
                    alert('خطا در سیستم!');
                }
            });
        } else {
            alert("انصراف.");
        }

    });

    jQuery('div.color-product').on('click', 'a.item-color', function (event) {
        event.preventDefault();
        var clicked = jQuery(this);
        var parent = clicked.parents('div.image-product').first();
        var f_value = clicked.attr('data-value');
        var default_image = parent.find('#default-product-image');
        parent.find('img.item-image-gallery').remove();
        if (f_value == default_image.attr('data-feature-value')) {
            default_image.fadeIn();
        } else {
            default_image.hide();
            var section_images = jQuery('section#portfolio').first();
            var image_product = section_images.find('img.item-image-gallery[data-feature-value="' + f_value + '"]').first();
            if (image_product.length > 0) {
                var cloneImage = image_product.clone().attr('data-feature-value', f_value);
                parent.find('div.recent-gallery-container').append(cloneImage);
            } else {
                default_image.fadeIn();
            }


        }


    })

});