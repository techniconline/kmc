//--google map
function initialize() {
    var lPlus = 1.5;
    if (dbLat) {
        lPlus = 0.0003;
    }
    var $lat0 = (dbLat) ? dbLat : 45.82, $lng0 = (dbLng) ? dbLng : 1.8137;
    var $lat1 = $lat0 + lPlus, $lng1 = $lng0 + lPlus;
    var markers = [];
    var googleMapOptions =
    {
        mapTypeId: google.maps.MapTypeId.ROADMAP // google map type
    };
    var map = new google.maps.Map(document.getElementById('map-canvas'), googleMapOptions);
    //console.log(map);
    var defaultBounds = new google.maps.LatLngBounds(
        new google.maps.LatLng($lat0, $lng0),
        new google.maps.LatLng($lat1, $lng1));
    map.fitBounds(defaultBounds);

    // Create the search box and link it to the UI element.
    var input = /** @type {HTMLInputElement} */(
        document.getElementById('pac-input'));
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    var searchBox = new google.maps.places.SearchBox(
        /** @type {HTMLInputElement} */(input));

    // [START region_getplaces]
    // Listen for the event fired when the user selects an item from the
    // pick list. Retrieve the matching places for that item.
    google.maps.event.addListener(searchBox, 'places_changed', function () {
        var places = searchBox.getPlaces();

        if (places.length == 0) {
            return;
        }
        for (var i = 0, marker; marker = markers[i]; i++) {
            marker.setMap(null);
        }

        // For each place, get the icon, place name, and location.
        markers = [];
        var bounds = new google.maps.LatLngBounds();
        for (var i = 0, place; place = places[i]; i++) {
            // Create a marker for each place.
            var marker = new google.maps.Marker({
                map: map,
                //icon: image,
                title: place.name,
                position: place.geometry.location
            });

            markers.push(marker);
            bounds.extend(place.geometry.location);
            //console.log(place.geometry.location.lat(), place.geometry.location.lng(), place);
            var elmLat = $('.lat-apt');
            var elmLng = $('.lng-apt');
            var elmAdd = $('.address-apt');
            elmLat.val(place.geometry.location.lat());
            elmLng.val(place.geometry.location.lng());
            elmAdd.val(place.formatted_address);

        }

        map.fitBounds(bounds);
    });
    // [END region_getplaces]

    // Bias the SearchBox results towards places that are within the bounds of the
    // current map's viewport.
    google.maps.event.addListener(map, 'bounds_changed', function () {
        var bounds = map.getBounds();
        searchBox.setBounds(bounds);
    });
}

google.maps.event.addDomListener(window, 'load', initialize);
//-- google map
