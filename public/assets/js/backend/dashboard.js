jQuery(document).ready(function () {

    $('#result-data-room-table').on('click', 'ul.pagination a', function (event) {
        event.preventDefault();
        var clicked = $(this);
        var page = clicked.text();
        var url = clicked.parents('div.row').first().attr('data-url');
        var parent = clicked.parents('tr.paginate').first();
        var loading = parent.find('span.loading');
        var resultDataTable = clicked.parents('#result-data-room-table');
        //var filterInput = clicked.parents('table#table-advance').first().find('tr.filter');
        //console.log(filterInput);
        //var inputs = filterInput.find('input,select');
        //console.log(inputs);

        loading.fadeIn();
        $.ajax({
            url: url + "/?page=" + page,
            method: 'POST',
            //data:inputs,
            success: function (data) {
                var tmpdata = (data);
                loading.fadeOut();
                if (tmpdata.action == true) {
                    resultDataTable.html(tmpdata.pageData);
                } else {
                    alert(tmpdata.message);
                }

            }
        });

    });

    $('#table-advance').on('click', 'button.btn-search', function (event) {
        event.preventDefault();
        var clicked = $(this);
        var parent = clicked.parents('tr.filter').first();
        var url = clicked.parents('div.search').first().attr('data-url');
        var loading = clicked.parents('div.search').first().find('span.loading');
        var resultDataTable = clicked.parents('#table-advance').first().find('#result-data-table');
        var inputs = parent.find('input,select');
        ////console.log(inputs.serialize(),parent);
        //return;

        loading.fadeIn();
        $.ajax({
            url: url,
            method: 'POST',
            data: inputs.serialize(),
            success: function (data) {
                var tmpdata = (data);
                loading.fadeOut();
                if (tmpdata.action == true) {
                    resultDataTable.html(tmpdata.pageData);
                } else {
                    alert(tmpdata.message);
                }

            }
        });

    });

});
