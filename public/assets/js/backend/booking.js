jQuery(document).ready(function () {


    $('#table-advance').on('click', 'a.row-details', function (event) {
        event.preventDefault();
        var clicked = $(this);
        var iElem = clicked.find('i');
        var parent = clicked.parents('td.row-action');
        var tr_parent = clicked.parents('tr.row-item').first();
        var dataId = tr_parent.attr('data-id');
        var tr_details = tr_parent.siblings('tr.details[data-id="' + dataId + '"]');

        if (iElem.hasClass('fa-minus-square-o')) {
            iElem.removeClass('fa-minus-square-o');
            iElem.addClass('fa-plus-square-o');
            tr_details.fadeOut();
        } else if (iElem.hasClass('fa-plus-square-o')) {
            iElem.removeClass('fa-plus-square-o');
            iElem.addClass('fa-minus-square-o');
            tr_details.fadeIn();
        }

    });

    $('#result-data-table').on('click', 'ul.pagination a', function (event) {
        event.preventDefault();
        var clicked = $(this);
        var url = clicked.attr('href');
        var parent = clicked.parents('tr.paginate').first();
        var loading = parent.find('span.loading');
        var resultDataTable = clicked.parents('#result-data-table');

        loading.fadeIn();
        $.ajax({
            url: clicked.attr('href'),
            method: 'GET',
            //data:{type_status:status},
            success: function (data) {
                var tmpdata = (data);
                loading.fadeOut();
                if (tmpdata.action == true) {
                    resultDataTable.html(tmpdata.pageData);
                } else {
                    alert(tmpdata.message);
                }

            }
        });

    });

    $('#table-advance').on('click', 'button.btn-search', function (event) {
        event.preventDefault();
        var clicked = $(this);
        var parent = clicked.parents('tr.filter').first();
        var url = clicked.parents('div.search').first().attr('data-url');
        var loading = clicked.parents('div.search').first().find('span.loading');
        var resultDataTable = clicked.parents('#table-advance').first().find('#result-data-table');
        var inputs = parent.find('input,select');
        ////console.log(inputs.serialize(),parent);
        //return;

        loading.fadeIn();
        $.ajax({
            url: url,
            method: 'POST',
            data: inputs.serialize(),
            success: function (data) {
                var tmpdata = (data);
                loading.fadeOut();
                if (tmpdata.action == true) {
                    resultDataTable.html(tmpdata.pageData);
                } else {
                    alert(tmpdata.message);
                }

            }
        });

    });

    $('#result-data-table').on('click', 'a.send-contract', function (event) {
        event.preventDefault();
        var clicked = $(this);
        var url = clicked.attr('href');
        var parentTD = clicked.parents('td.row-action').first();
        var loading = parentTD.find('span.loading');
        var resultEmail = parentTD.find('span.result');

        loading.fadeIn();
        //console.log(user_id,email,username,booking_id,url);

        $.ajax({
            url: url,
            method: 'POST',
            success: function (data) {
                var tmpdata = (data);
                loading.fadeOut();
                if (tmpdata.action_room == true || tmpdata.action_parking == true) {

                    var linkShowModal = parentTD.find('a.show-modal-contract');
                    var modal = parentTD.find('div.modal').first();
                    var btnPdf = parentTD.find('div.modal-footer a.pdf-modal-generate-contract');
                    btnPdf.fadeIn();

                    var inputPriceRoom = parentTD.find('input.price_letters_room');
                    var divPriceRoom = parentTD.find('div.price-room');
                    var targetPriceRoom = divPriceRoom.find('span.price b');

                    var inputPriceRoom9 = parentTD.find('input.price_letters_room_9');
                    var divPriceRoom9 = parentTD.find('div.price-room-9');
                    var targetPriceRoom9 = divPriceRoom9.find('span.price b');

                    var inputPriceParking = parentTD.find('input.price_letters_parking');
                    var divPriceParking = parentTD.find('div.price-parking');
                    var targetPriceParking = divPriceParking.find('span.price b');

                    var inputPriceParking9 = parentTD.find('input.price_letters_parking_9');
                    var divPriceParking9 = parentTD.find('div.price-parking-9');
                    var targetPriceParking9 = divPriceParking9.find('span.price b');

                    if (tmpdata.action_room == false) {
                        inputPriceRoom.prop('disabled', true);
                        inputPriceRoom9.prop('disabled', true);
                        divPriceRoom.fadeOut();
                        divPriceRoom9.fadeOut();
                        //console.log(inputPriceRoom);
                    } else {
                        //console.log(inputPriceRoom);
                        inputPriceRoom.prop('disabled', false);
                        inputPriceRoom9.prop('disabled', false);
                        divPriceRoom.fadeIn();
                        divPriceRoom9.fadeIn();
                        targetPriceRoom.text(tmpdata.room_price);
                        targetPriceRoom9.text(tmpdata.room_price_9);
                    }
                    if (tmpdata.action_parking == false) {
                        inputPriceParking.prop('disabled', true);
                        inputPriceParking9.prop('disabled', true);
                        divPriceParking.fadeOut();
                        divPriceParking9.fadeOut();
                        //console.log(inputPriceParking);
                    } else {
                        //console.log(inputPriceParking);
                        divPriceParking.fadeIn();
                        divPriceParking9.fadeIn();
                        inputPriceParking.prop('disabled', false);
                        inputPriceParking9.prop('disabled', false);
                        targetPriceParking.text(tmpdata.parking_price);
                        targetPriceParking9.text(tmpdata.parking_price_9);
                    }
                    linkShowModal.click();

                } else {
                    alert(tmpdata.message);
                }

            }, error: function (ts) {
                alert('You have error!');
                loading.fadeOut();
            }
        });


    });

    $('#result-data-table').on('click', 'a.pdf-modal-generate-contract', function (event) {
        event.preventDefault();
        var clicked = $(this);
        var url = clicked.attr('href');
        var booking_id = clicked.parents('tr.details').first().attr('data-id');
        var parentTD = clicked.parents('div.modal').first();
        var loading = parentTD.find('span.loading');
        var resultEmail = parentTD.find('span.result');

        loading.fadeIn();
        //console.log(user_id,email,username,booking_id,url);

        $.ajax({
            url: url,
            method: 'POST',
            data: parentTD.find('input').serialize(),
            success: function (data) {
                var tmpdata = (data);
                loading.fadeOut();
                if (tmpdata.action == true) {
                    var btnPdf = parentTD.find('div.modal-footer a.pdf-modal-generate-contract');
                    btnPdf.fadeOut();
                    btnPdf = parentTD.find('div.modal-footer a.pdf-modal-send-mail-contract');
                    btnPdf.fadeIn();
                    var inputContract1 = parentTD.find('div.modal-footer input.file_contract1');
                    var inputContract2 = parentTD.find('div.modal-footer input.file_contract2');

                    resultEmail.html("");
                    if (tmpdata.result.length > 0) {
                        inputContract1.val(tmpdata.result[0]);
                        inputContract2.val(tmpdata.result[1]);
                        resultEmail.append('<a target="_blank" href="/' + tmpdata.result[0] + '" class="btn green" >Link 1 <i class="fa fa-link"></i></a>');
                        resultEmail.append('<a target="_blank" href="/' + tmpdata.result[1] + '" class="btn green" >Link 2 <i class="fa fa-link"></i></a>');
                        //console.log(tmpdata.result,tmpdata.result.length);
                        //$.each(tmpdata.result, function(i, val) {
                        //    resultEmail.append('<p> '+val+' </p>')
                        //});
                    }

                    //resultEmail.html(tmpdata.message);
                } else {
                    alert(tmpdata.message);
                }

            }, error: function (ts) {
                alert('You have error!');
                loading.fadeOut();
            }
        });
    });

    $('#result-data-table').on('click', 'a.pdf-modal-send-mail-contract', function (event) {
        event.preventDefault();
        var clicked = $(this);
        var url = clicked.attr('href');
        var booking_id = clicked.parents('tr.details').first().attr('data-id');
        var parentTD = clicked.parents('div.modal').first();
        var loading = parentTD.find('span.loading');
        var resultEmail = parentTD.find('span.result');

        loading.fadeIn();
        //console.log(user_id,email,username,booking_id,url);
        bootbox.confirm("Are you sure , for send contract?", function (result) {
            if (result === false) {
                alert("Cancel send contract.");
                loading.fadeOut();
            } else {

                $.ajax({
                    url: url,
                    method: 'POST',
                    data: parentTD.find('input').serialize(),
                    success: function (data) {
                        var tmpdata = (data);
                        loading.fadeOut();
                        if (tmpdata.action == true) {
                            resultEmail.html(tmpdata.message);
                        } else {
                            alert(tmpdata.message);
                        }

                    }, error: function (ts) {
                        alert('You have error!');
                        loading.fadeOut();
                    }
                });
            }
        });

    });

    $('#result-data-table').on('click', 'a.send-mail', function (event) {
        event.preventDefault();
        var clicked = $(this);
        var url = clicked.attr('href');
        var booking_id = clicked.parents('tr.details').first().attr('data-id');
        var parent = clicked.parents('#result-data-table').find('tr.row-item[data-id="' + booking_id + '"]').first();
        var parentTD = clicked.parents('td.row-action').first();
        var loading = parentTD.find('span.loading');
        var user_id = parent.find('span.email').attr('data-user');
        var email = $.trim(parent.find('span.email').text());
        var username = $.trim(parent.find('span.username').text());
        var resultEmail = parentTD.find('span.result');

        loading.fadeIn();
        //console.log(user_id,email,username,booking_id,url);
        bootbox.prompt("Text Mail:", function (result) {
            if (result === null) {
                alert("Cancel send mail!");
                loading.fadeOut();
            } else {

                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {
                        booking_id: booking_id,
                        user_id: user_id,
                        email: email,
                        text: result,
                        username: username
                    },
                    success: function (data) {
                        var tmpdata = (data);
                        loading.fadeOut();
                        if (tmpdata.action == true) {
                            resultEmail.html(tmpdata.message);
                        } else {
                            alert(tmpdata.message);
                        }

                    }, error: function (ts) {
                        alert('You have error!');
                        loading.fadeOut();
                    }
                });
            }
        });

    });

    $('#result-data-table').on('click', 'a.reject', function (event) {
        event.preventDefault();
        var clicked = $(this);
        var url = clicked.attr('href');

        var booking_id = clicked.parents('tr.details').first().attr('data-id');
        var parent = clicked.parents('#result-data-table').find('tr.row-item[data-id="' + booking_id + '"]').first();

        var parentTD = clicked.parents('td.row-action').first();
        var loading = parentTD.find('span.loading');
        var email = $.trim(parent.find('span.email').text());
        var username = $.trim(parent.find('span.username').text());
        var resultEmail = parentTD.find('span.result');
        loading.fadeIn();
        //console.log(user_id,email,username,booking_id,url);
        bootbox.confirm("Are you sure , for reject?", function (result) {
            if (result === false) {
                alert("Cancel reject.");
                loading.fadeOut();
            } else {

                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {
                        email: email,
                        username: username
                    },
                    success: function (data) {
                        var tmpdata = (data);
                        loading.fadeOut();
                        if (tmpdata.action == true) {
                            resultEmail.html(tmpdata.message);
                        } else {
                            alert(tmpdata.message);
                        }

                    }, error: function (ts) {
                        alert('You have error!');
                        loading.fadeOut();
                    }
                });
            }
        });

    });

    $('#result-data-table').on('click', 'a.is-ok', function (event) {
        event.preventDefault();
        var clicked = $(this);
        var url = clicked.attr('href');

        var booking_id = clicked.parents('tr.details').first().attr('data-id');
        var parent = clicked.parents('#result-data-table').find('tr.row-item[data-id="' + booking_id + '"]').first();

        var parentTD = clicked.parents('td.row-action').first();
        var loading = parentTD.find('span.loading');
        var email = $.trim(parent.find('span.email').text());
        var username = $.trim(parent.find('span.username').text());
        var resultEmail = parentTD.find('span.result');
        loading.fadeIn();
        //console.log(user_id,email,username,booking_id,url);
        bootbox.confirm("All documents are okay?", function (result) {
            if (result === false) {
                alert("Cancel Ok.");
                loading.fadeOut();
            } else {

                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {
                        email: email,
                        username: username
                    },
                    success: function (data) {
                        var tmpdata = (data);
                        loading.fadeOut();
                        if (tmpdata.action == true) {
                            resultEmail.html(tmpdata.message);
                        } else {
                            alert(tmpdata.message);
                        }

                    }, error: function (ts) {
                        alert('You have error!');
                        loading.fadeOut();
                    }
                });
            }
        });

    });

    $('#result-data-table').on('click', 'a.reservation', function (event) {
        event.preventDefault();
        var clicked = $(this);
        var url = clicked.attr('href');
        var parentTD = clicked.parents('td').first();
        var parent = clicked.parents('tr.details').first();
        var booking_id = parent.attr('data-id');
        var cupParent = parent.parents('#result-data-table').find('tr.row-item[data-id="' + booking_id + '"]');

        var parentAction = clicked.parents('tr.details-action').first();
        var room_id = parentAction.find('select.item-room-select option:selected').val();
        var loading = parentAction.find('span.loading');
        var email = $.trim(cupParent.find('span.email').text());
        var user_id = cupParent.find('span.email').attr('data-user');
        var username = $.trim(cupParent.find('span.username').text());
        var resultEmail = parentAction.find('span.result');
        loading.fadeIn();
        //console.log(room_id,url,email,username);
        if (!room_id) {
            return;
        }

        bootbox.confirm("Reservation room?", function (result) {
            if (result === false) {
                alert("Cancel Ok.");
                loading.fadeOut();
            } else {

                $.ajax({
                    url: url + '/reservation/' + room_id + '/' + booking_id + '/' + user_id,
                    method: 'POST',
                    data: {
                        email: email,
                        username: username
                    },
                    success: function (data) {
                        var tmpdata = (data);
                        loading.fadeOut();
                        if (tmpdata.action == true) {
                            parentTD.html(tmpdata.message);

                            var searchBtn = $('table#table-advance').find('tr.filter').find('button.btn-search');
                            if (searchBtn) {
                                searchBtn.click();
                            }

                        } else {
                            alert(tmpdata.message);
                        }

                    }, error: function (ts) {
                        alert('You have error!');
                        loading.fadeOut();
                    }
                });
            }
        });

    });

    $('#result-data-table').on('click', 'a.reject-booking-details', function (event) {
        event.preventDefault();
        var clicked = $(this);
        var url = clicked.attr('href');
        var parent = clicked.parents('tr.details').first();
        //var parentTD = clicked.parents('td.details').first();
        var parentTD = clicked.parents('td').first();
        var booking_id = parent.attr('data-id');
        var cupParent = parent.parents('#result-data-table').find('tr.row-item[data-id="' + booking_id + '"]');

        var parentAction = clicked.parents('tr.details-action').first();
        var loading = parentAction.find('span.loading');
        var email = $.trim(cupParent.find('span.email').text());
        var user_id = cupParent.find('span.email').attr('data-user');

        var username = $.trim(cupParent.find('span.username').text());
        loading.fadeIn();
        //console.log(room_id,url,email,username);
        if (!email) {
            return;
        }

        bootbox.confirm("Reject this type reservation?", function (result) {
            if (result === false) {
                alert("Cancel Ok.");
                loading.fadeOut();
            } else {

                $.ajax({
                    url: url,
                    method: 'PUT',
                    data: {
                        email: email,
                        user_id: user_id,
                        username: username
                    },
                    success: function (data) {
                        var tmpdata = (data);
                        loading.fadeOut();
                        if (tmpdata.action == true) {
                            parentAction.html(tmpdata.message);

                            var searchBtn = $('table#table-advance').find('tr.filter').find('button.btn-search');
                            if (searchBtn) {
                                searchBtn.click();
                            }

                        } else {
                            alert(tmpdata.message);
                        }

                    }, error: function (ts) {
                        alert('You have error!');
                        loading.fadeOut();
                    }
                });
            }
        });

    });

    $('#result-data-table').on('click', 'a.rent', function (event) {
        event.preventDefault();
        var clicked = $(this);
        var url = clicked.attr('href');
        var parent = clicked.parents('tr.details').first();
        //var parentTD = clicked.parents('td.details').first();
        var parentTD = clicked.parents('td').first();
        var booking_id = parent.attr('data-id');
        var cupParent = parent.parents('#result-data-table').find('tr.row-item[data-id="' + booking_id + '"]');

        var parentAction = clicked.parents('tr.details-action').first();
        var room_id = parentAction.find('select.item-room-select option:selected').val();
        var loading = parentAction.find('span.loading');
        var email = $.trim(cupParent.find('span.email').text());
        var user_id = cupParent.find('span.email').attr('data-user');

        var username = $.trim(cupParent.find('span.username').text());
        var resultEmail = parentAction.find('span.result');
        loading.fadeIn();
        //console.log(room_id,url,email,username);
        if (!room_id) {
            return;
        }

        bootbox.confirm("Rent room?", function (result) {
            if (result === false) {
                alert("Cancel Ok.");
                loading.fadeOut();
            } else {

                $.ajax({
                    url: url + '/rent/' + room_id + '/' + booking_id + '/' + user_id,
                    method: 'POST',
                    data: {
                        email: email,
                        username: username
                    },
                    success: function (data) {
                        var tmpdata = (data);
                        loading.fadeOut();
                        if (tmpdata.action == true) {
                            parentTD.html(tmpdata.message);

                            var searchBtn = $('table#table-advance').find('tr.filter').find('button.btn-search');
                            if (searchBtn) {
                                searchBtn.click();
                            }

                        } else {
                            alert(tmpdata.message);
                        }

                    }, error: function (ts) {
                        alert('You have error!');
                        loading.fadeOut();
                    }
                });
            }
        });

    });

    $('#result-data-table').on('click', 'a.cancel-rent', function (event) {
        event.preventDefault();
        var clicked = $(this);
        var url = clicked.attr('href');
        var parent = clicked.parents('tr.details').first();
        var parentTD = clicked.parents('td').first();
        var booking_id = parent.attr('data-id');
        //var room_id = parent.find('select.item-room-select option:selected').val();
        var cupParent = parent.parents('#result-data-table').find('tr.row-item[data-id="' + booking_id + '"]');

        var parentAction = clicked.parents('tr.details-action').first();
        var loading = parentAction.find('span.loading');
        var email = $.trim(cupParent.find('span.email').text());
        //var user_id =cupParent.find('span.email').attr('data-user');

        var username = $.trim(cupParent.find('span.username').text());
        var resultEmail = parentAction.find('span.result');
        loading.fadeIn();
        //console.log(url,email,username);
        if (!booking_id) {
            return;
        }

        bootbox.confirm("Cancel rent room?", function (result) {
            if (result === false) {
                alert("Cancel Ok.");
                loading.fadeOut();
            } else {

                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {
                        email: email,
                        username: username
                    },
                    success: function (data) {
                        var tmpdata = (data);
                        loading.fadeOut();
                        if (tmpdata.action == true) {
                            parentTD.html(tmpdata.message);

                            var searchBtn = $('table#table-advance').find('tr.filter').find('button.btn-search');
                            if (searchBtn) {
                                searchBtn.click();
                            }

                        } else {
                            alert(tmpdata.message);
                        }

                    }, error: function (ts) {
                        alert('You have error!');
                        loading.fadeOut();
                    }
                });
            }
        });

    });

});

var fnSelect2 = function () {
    if (arrayId.length > 0) {
        for (var i = 0; i < arrayId.length; i++) {
            //console.log(arrayId[i]);

            $("#select2-" + arrayId[i] + "").select2({
                placeholder: "Select...",
                allowClear: true,
                escapeMarkup: function (m) {
                    return m;
                }
            });
        }
        arrayId = [];
    }
}