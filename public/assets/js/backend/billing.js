jQuery(document).ready(function () {


    $('div.list-button').on('click', 'a#remember-list', function (event) {
        event.preventDefault();
        var clicked = $(this);
        var parent = clicked.parents('div.list-button').first();
        var url = clicked.attr('href');
        var loading = parent.find('span.loading');
        var resultData = parent.find('span.result');

        loading.fadeIn();
        bootbox.confirm("Are you sure , for send mail of invoice - remember ?", function (result) {
            if (result === false) {
                alert("Cancel generate.");
                loading.fadeOut();
            } else {

                $.ajax({
                    url: url,
                    method: 'POST',
                    success: function (data) {
                        var tmpdata = (data);
                        loading.fadeOut();
                        if (tmpdata.action == true) {
                            resultData.html(tmpdata.message);
                            resultData.append(tmpdata.result + ' send mail successful.');
                            window.location.href = window.location.href;
                            window.location.reload();
                        } else {
                            alert(tmpdata.message);
                        }

                    }, error: function (ts) {
                        alert('You have error!');
                        loading.fadeOut();
                    }
                });
            }
        });

    });

    $('div.list-button').on('click', 'a#generate-list', function (event) {
        event.preventDefault();
        var clicked = $(this);
        var parent = clicked.parents('div.list-button').first();
        var url = clicked.attr('href');
        var loading = parent.find('span.loading');
        var resultData = parent.find('span.result');

        loading.fadeIn();
        bootbox.confirm("Are you sure , for generate list?", function (result) {
            if (result === false) {
                alert("Cancel generate.");
                loading.fadeOut();
            } else {

                $.ajax({
                    url: url,
                    method: 'POST',
                    success: function (data) {
                        var tmpdata = (data);
                        loading.fadeOut();
                        if (tmpdata.action == true) {
                            resultData.html(tmpdata.message);
                            resultData.append(tmpdata.resultMail + ' send mail successful.');
                            window.location.href = window.location.href;
                            window.location.reload();
                        } else {
                            alert(tmpdata.message);
                        }

                    }, error: function (ts) {
                        alert('You have error!');
                        loading.fadeOut();
                    }
                });
            }
        });

    });

    $('#result-data-table').on('click', 'ul.pagination a', function (event) {
        event.preventDefault();
        var clicked = $(this);
        var url = clicked.attr('href');
        var parent = clicked.parents('tr.paginate').first();
        var loading = parent.find('span.loading');
        var resultDataTable = clicked.parents('#result-data-table');

        loading.fadeIn();
        $.ajax({
            url: clicked.attr('href'),
            method: 'GET',
            //data:{type_status:status},
            success: function (data) {
                var tmpdata = (data);
                loading.fadeOut();
                if (tmpdata.action == true) {
                    resultDataTable.html(tmpdata.pageData);
                } else {
                    alert(tmpdata.message);
                }

            }
        });

    });

    $('#table-advance').on('click', 'button.btn-search', function (event) {
        event.preventDefault();
        var clicked = $(this);
        var parent = clicked.parents('tr.filter').first();
        var url = clicked.parents('div.search').first().attr('data-url');
        var loading = clicked.parents('div.search').first().find('span.loading');
        var resultDataTable = clicked.parents('#table-advance').first().find('#result-data-table');
        var inputs = parent.find('input,select');
        ////console.log(inputs.serialize(),parent);
        //return;

        loading.fadeIn();
        $.ajax({
            url: url,
            method: 'POST',
            data: inputs.serialize(),
            success: function (data) {
                var tmpdata = (data);
                loading.fadeOut();
                if (tmpdata.action == true) {
                    resultDataTable.html(tmpdata.pageData);
                } else {
                    alert(tmpdata.message);
                }

            }
        });

    });


});

var fnSelect2 = function () {

    $("#select2-rooms").select2({
        placeholder: "Select...",
        allowClear: true,
        escapeMarkup: function (m) {
            return m;
        }
    });

};