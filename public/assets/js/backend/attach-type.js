jQuery(document).ready(function () {

    $('#table-advance').on('click', 'a.row-details', function (event) {
        event.preventDefault();
        var clicked = $(this);
        var iElem = clicked.find('i');
        var parent = clicked.parents('td.row-action');
        var tr_parent = clicked.parents('tr.row-item').first();
        var dataId = tr_parent.attr('data-id');
        var tr_details = tr_parent.siblings('tr.details[data-id="' + dataId + '"]');

        if (iElem.hasClass('fa-minus-square-o')) {
            iElem.removeClass('fa-minus-square-o');
            iElem.addClass('fa-plus-square-o');
            tr_details.fadeOut();
        } else if (iElem.hasClass('fa-plus-square-o')) {
            iElem.removeClass('fa-plus-square-o');
            iElem.addClass('fa-minus-square-o');
            tr_details.fadeIn();
        }

    });
    $('.row-item').on('click', 'a.activate', function (event) {
        event.preventDefault();
        var clicked = $(this);
        var status = clicked.attr('data-status');
        var parent = clicked.parents('td.row-action');
        var tr_parent = clicked.parents('tr.row-item').first();

        $.ajax({
            url: clicked.attr('href'),
            method: 'PUT',
            data: {type_status: status},
            success: function (data) {
                var tmpdata = (data);
                if (tmpdata.action == true) {
                    alert(tmpdata.message);
                    clicked.attr('data-status', tmpdata.type_status);
                    clicked.html('<i class="fa fa-link"></i> ' + tmpdata.TitleActivate);
                } else {
                    alert(tmpdata.message);
                }

            }
        });

    });
    $('.row-item').on('click', 'a.delete', function (event) {
        event.preventDefault();
        var clicked = $(this);
        var parent = clicked.parents('td.row-action');
        var tr_parent = clicked.parents('tr.row-item').first();
        $.ajax({
            url: clicked.attr('href'),
            method: 'DELETE',
            success: function (data) {
                var tmpdata = (data);
                if (tmpdata.action == true) {
                    alert(tmpdata.message);
                    tr_parent.remove();
                } else {
                    alert(tmpdata.message);
                }

            }
        });

    });

    $('.row-item-group').on('click', 'a.activateGroup', function (event) {
        event.preventDefault();
        var clicked = $(this);
        var status = clicked.attr('data-status');
        var parent = clicked.parents('td.row-action');
        var tr_parent = clicked.parents('tr.row-item').first();

        $.ajax({
            url: clicked.attr('href'),
            method: 'PUT',
            data: {group_status: status},
            success: function (data) {
                var tmpdata = (data);
                if (tmpdata.action == true) {
                    alert(tmpdata.message);
                    clicked.attr('data-status', tmpdata.group_status);
                    clicked.html('<i class="fa fa-link"></i> ' + tmpdata.TitleActivate);
                } else {
                    alert(tmpdata.message);
                }

            }
        });

    });
    $('.row-item-group').on('click', 'a.deleteGroup', function (event) {
        event.preventDefault();
        var clicked = $(this);
        var parent = clicked.parents('td.row-action');
        var tr_parent = clicked.parents('tr.row-item').first();
        $.ajax({
            url: clicked.attr('href'),
            method: 'DELETE',
            success: function (data) {
                var tmpdata = (data);
                if (tmpdata.action == true) {
                    alert(tmpdata.message);
                    tr_parent.remove();
                } else {
                    alert(tmpdata.message);
                }

            }
        });

    });

    $('#result-data-table').on('click', 'ul.pagination a', function (event) {
        event.preventDefault();
        var clicked = $(this);
        var url = clicked.attr('href');
        var parent = clicked.parents('tr.paginate').first();
        var loading = parent.find('span.loading');
        var resultDataTable = clicked.parents('#result-data-table');

        loading.fadeIn();
        $.ajax({
            url: clicked.attr('href'),
            method: 'GET',
            //data:{type_status:status},
            success: function (data) {
                var tmpdata = (data);
                loading.fadeOut();
                if (tmpdata.action == true) {
                    resultDataTable.html(tmpdata.pageData);
                } else {
                    alert(tmpdata.message);
                }

            }
        });

    });

    $('#table-advance').on('click', 'button.btn-search', function (event) {
        event.preventDefault();
        var clicked = $(this);
        var parent = clicked.parents('tr.filter').first();
        var url = clicked.parents('div.search').first().attr('data-url');
        var loading = clicked.parents('div.search').first().find('span.loading');
        var resultDataTable = clicked.parents('#table-advance').first().find('#result-data-table');
        var inputs = parent.find('input,select');
        ////console.log(inputs.serialize(),parent);
        //return;

        loading.fadeIn();
        $.ajax({
            url: url,
            method: 'POST',
            data: inputs.serialize(),
            success: function (data) {
                var tmpdata = (data);
                loading.fadeOut();
                if (tmpdata.action == true) {
                    resultDataTable.html(tmpdata.pageData);
                } else {
                    alert(tmpdata.message);
                }

            }
        });

    });

    /*
     $('#result-data-table').on('click', 'a.pagination a', function (event) {
     event.preventDefault();
     var clicked = $(this);
     var url = clicked.attr('href');
     var parent = clicked.parents('tr.paginate').first();
     var loading = parent.find('span.loading');
     var resultDataTable = clicked.parents('#result-data-table');

     loading.fadeIn();
     $.ajax({
     url: clicked.attr('href'),
     method: 'GET',
     //data:{type_status:status},
     success: function (data) {
     var tmpdata = (data);
     loading.fadeOut();
     if (tmpdata.action == true) {
     resultDataTable.html(tmpdata.pageData);
     } else {
     alert(tmpdata.message);
     }

     }
     });

     });
     */

    $('#result-data-table').on('click', 'a.send-mail', function (event) {
        event.preventDefault();
        var clicked = $(this);
        var url = clicked.attr('href');
        var parent = clicked.parents('tr.row-item').first();
        var parentTD = clicked.parents('td.row-action').first();
        var loading = parentTD.find('span.loading');
        var user_id = parent.find('span.email').attr('data-user');
        var booking_id = parent.attr('data-id');
        var email = $.trim(parent.find('span.email').text());
        var username = $.trim(parent.find('span.username').text());
        var resultEmail = parentTD.find('span.result');
        loading.fadeIn();
        //console.log(user_id,email,username,booking_id,url);
        bootbox.prompt("Text Mail:", function (result) {
            if (result === null) {
                alert("Cancel send mail!");
                loading.fadeOut();
            } else {

                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {
                        booking_id: booking_id,
                        user_id: user_id,
                        email: email,
                        text: result,
                        username: username
                    },
                    success: function (data) {
                        var tmpdata = (data);
                        loading.fadeOut();
                        if (tmpdata.action == true) {
                            resultEmail.html(tmpdata.message);
                        } else {
                            alert(tmpdata.message);
                        }

                    }, error: function (ts) {
                        alert('You have error!');
                        loading.fadeOut();
                    }
                });
            }
        });

    });

    $('#result-data-table').on('click', 'a.reject', function (event) {
        event.preventDefault();
        var clicked = $(this);
        var url = clicked.attr('href');
        var parent = clicked.parents('tr.row-item').first();
        var parentTD = clicked.parents('td.row-action').first();
        var loading = parentTD.find('span.loading');
        var email = $.trim(parent.find('span.email').text());
        var username = $.trim(parent.find('span.username').text());
        var resultEmail = parentTD.find('span.result');
        loading.fadeIn();
        //console.log(user_id,email,username,booking_id,url);
        bootbox.confirm("Are you sure , for reject?", function (result) {
            if (result === false) {
                alert("Cancel reject.");
                loading.fadeOut();
            } else {

                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {
                        email: email,
                        username: username
                    },
                    success: function (data) {
                        var tmpdata = (data);
                        loading.fadeOut();
                        if (tmpdata.action == true) {
                            resultEmail.html(tmpdata.message);
                        } else {
                            alert(tmpdata.message);
                        }

                    }, error: function (ts) {
                        alert('You have error!');
                        loading.fadeOut();
                    }
                });
            }
        });

    });


    $('#result-data-table').on('click', 'a.is-ok', function (event) {
        event.preventDefault();
        var clicked = $(this);
        var url = clicked.attr('href');
        var parent = clicked.parents('tr.row-item').first();
        var parentTD = clicked.parents('td.row-action').first();
        var loading = parentTD.find('span.loading');
        var email = $.trim(parent.find('span.email').text());
        var username = $.trim(parent.find('span.username').text());
        var resultEmail = parentTD.find('span.result');
        loading.fadeIn();
        //console.log(user_id,email,username,booking_id,url);
        bootbox.confirm("All documents are okay?", function (result) {
            if (result === false) {
                alert("Cancel Ok.");
                loading.fadeOut();
            } else {

                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {
                        email: email,
                        username: username
                    },
                    success: function (data) {
                        var tmpdata = (data);
                        loading.fadeOut();
                        if (tmpdata.action == true) {
                            resultEmail.html(tmpdata.message);
                        } else {
                            alert(tmpdata.message);
                        }

                    }, error: function (ts) {
                        alert('You have error!');
                        loading.fadeOut();
                    }
                });
            }
        });

    });


});