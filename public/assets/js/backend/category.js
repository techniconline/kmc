jQuery(document).ready(function () {
    $('.row-category').on('click', 'a.delete', function (event) {
        event.preventDefault();
        var clicked = $(this);
        var parent = clicked.parents('tr.row-category').first();
        bootbox.confirm("Are you sure , for delete room category?", function (result) {
            if (result === false) {
                alert("Cancel delete.");
            } else {
                $.ajax({
                    url: clicked.attr('href'),
                    method: 'DELETE',
                    success: function (data) {
                        var tmpdata = (data);
                        if (tmpdata.action == true) {
                            alert(tmpdata.message);
                            parent.remove();
                        } else {
                            alert(tmpdata.message);
                        }

                    }
                });
            }

        });

    });
    $('.row-details').on('click', '.btn-delete', function (event) {
        event.preventDefault();
        var clicked = $(this);
        var parent = clicked.parents('.item-details');
        var arcm_id = parent.attr('data-id');
        $.ajax({
            url: 'del/' + arcm_id,
            method: 'DELETE',
            data: {
                arcm_id: arcm_id
            },
            success: function (data) {
                var tmpdata = (data);

                if (tmpdata.action == true) {
                    alert(tmpdata.message);
                    parent.parents('.row-details').remove();
                } else {
                    alert(tmpdata.message);
                }

            }
        });

    });
    $('.price-item').on('click', '.btn-update-price', function (event) {
        event.preventDefault();
        var clicked = $(this);
        var parent = clicked.parents('.price-item');
        var c_id = parent.attr('data-id');
        var price = parent.find('.price-cat').val();
        var to_price = parent.find('.to-price-cat').val();
        $.ajax({
            url: 'updatePrice/' + c_id,
            method: 'PUT',
            data: {
                c_id: c_id,
                price: price,
                to_price: to_price
            },
            success: function (data) {
                var tmpdata = (data);

                if (tmpdata.action == true) {
                    alert(tmpdata.message);
                    //parent.parents('.row-details').remove();
                } else {
                    alert(tmpdata.message);
                }

            }
        });

    });
});

var CategoriesTree = function () {


    var handleSample2 = function () {
        $('#tree_2').jstree({
            'plugins': ["wholerow", "checkbox", "types"],
            'core': {
                "themes": {
                    "responsive": true
                },
                'data': [{
                    "text": "Same but with checkboxes",
                    "children": [{
                        "text": "initially selected",
                        "state": {
                            "selected": true
                        }
                    }, {
                        "text": "custom icon",
                        "icon": "fa fa-warning icon-state-danger"
                    }, {
                        "text": "initially open",
                        "icon": "fa fa-folder icon-state-default",
                        "state": {
                            "opened": true
                        },
                        "children": ["Another node"]
                    }, {
                        "text": "custom icon",
                        "icon": "fa fa-warning icon-state-warning"
                    }, {
                        "text": "disabled node",
                        "icon": "fa fa-check icon-state-success",
                        "state": {
                            "disabled": true
                        }
                    }]
                },
                    "And wholerow selection"
                ]
            },
            "types": {
                "default": {
                    "icon": "fa fa-folder icon-state-warning icon-lg"
                },
                "file": {
                    "icon": "fa fa-file icon-state-warning icon-lg"
                }
            }
        });
    }

    return {
        //main function to initiate the module
        init: function () {

            handleSample2();

        }

    };

}();
var CategoriesList = function () {

    return {
        //main function to initiate the module
        init: function () {

            $('#nestable_list').nestable({
                dropCallback: function (details) {
                    var parent = details.sourceEl.parents('.dd-item').first();
                    var next = details.sourceEl.next('.dd-item').first().data('id');
                    var prev = details.sourceEl.prev('.dd-item').first().data('id');
                    var parentId = parent.data('id');

                    //if(details.destId != null){
                    //    $.ajax({
                    //        url : 'changeposition',
                    //        method : 'POST',
                    //        data : {
                    //            prevId : prev,
                    //            parentId : parentId,
                    //            nextId : next,
                    //            sourceId : details.sourceId,
                    //            destId : details.destId
                    //        },
                    //        success : function(msg){
                    //            console.log(msg);
                    //        }
                    //    });
                    //}
                }
            });

        }

    };

}();
