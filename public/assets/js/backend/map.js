//--google map
var map, latlng, i;
var markersArray = [];
jQuery(document).ready(function () {

    initialize();
    placeMarker(latlng);

});


function initialize() {
    latlng = new google.maps.LatLng(dbLat, dbLng);
    var myOptions = {
        zoom: zoom,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById("map-canvas"), myOptions);

    //var input = /** @type {HTMLInputElement} */(
    //    document.getElementById('pac-input'));
    //map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
    //
    //var searchBox = new google.maps.places.SearchBox(
    //    /** @type {HTMLInputElement} */(input));
    //
    //google.maps.event.addListener(searchBox, 'places_changed', function() {
    //    var places = searchBox.getPlaces();
    //
    //    if (places.length == 0) {
    //        return;
    //    }
    //
    //    // For each place, get the icon, place name, and location.
    //    var bounds = new google.maps.LatLngBounds();
    //    console.log(places.geometry.location);
    //    bounds.extend(places.geometry.location);
    //
    //    map.fitBounds(bounds);
    //});


    // add a click event handler to the map object
    google.maps.event.addListener(map, "click", function (event) {
        // place a marker
        //console.log(event.latLng);
        placeMarker(event.latLng);

        var elmLat = $('.lat-apt');
        var elmLng = $('.lng-apt');
        var elmAdd = $('.address-apt');
        // display the lat/lng in your form's lat/lng fields
        elmLat.val(event.latLng.lat());
        elmLng.val(event.latLng.lng());
        //console.log(event.latLng);
    });
    //
    //google.maps.event.addListener(map, 'bounds_changed', function() {
    //    var bounds = map.getBounds();
    //    searchBox.setBounds(bounds);
    //});

}
function placeMarker(location) {
    // first remove all markers if there are any
    //console.log(markersArray);
    deleteOverlays();

    var marker = new google.maps.Marker({
        position: location,
        map: map
    });

    // add marker in markers array
    markersArray.push(marker);

    //map.setCenter(location);
}

// Deletes all markers in the array by removing references to them
function deleteOverlays() {
    //console.log(markersArray.length);
    if (markersArray.length) {
        for (i = 0; markersArray.length > i; i++) {
            //console.log(i);
            markersArray[i].setMap(null);
        }
        markersArray.length = 0;
    }
}

//google.maps.event.addDomListener(window, 'load', initialize);
//-- google map
