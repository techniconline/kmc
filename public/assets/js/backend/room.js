jQuery(document).ready(function () {
    $('.row-item').on('click', 'a.generate', function (event) {
        event.preventDefault();
        var clicked = $(this);
        var parent = clicked.parents('td.row-action');
        $.ajax({
            url: clicked.attr('href'),
            method: 'POST',
            success: function (data) {
                var tmpdata = (data);
                if (tmpdata.action == true) {
                    alert(tmpdata.message);
                    parent.html('<a href="' + tmpdata.editLink + '" class="btn default btn-xs purple edit">' +
                        '<i class="fa fa-edit"></i> Edit </a>' +
                        '<a href="' + tmpdata.delLink + '" class="btn default btn-xs red delete">' +
                        '<i class="fa fa-times"></i> Delete </a>');
                } else {
                    alert(tmpdata.message);
                }

            }
        });

    });

    $('.row-item').on('click', 'a.delete', function (event) {
        event.preventDefault();
        var clicked = $(this);
        var parent = clicked.parents('td.row-action');
        bootbox.confirm("Are you sure , for delete room?", function (result) {
            if (result === false) {
                alert("Cancel delete.");
            } else {

                $.ajax({
                    url: clicked.attr('href'),
                    method: 'DELETE',
                    success: function (data) {
                        var tmpdata = (data);
                        if (tmpdata.action == true) {
                            alert(tmpdata.message);
                            parent.html('<a href="' + tmpdata.genLink + '" class="btn default btn-xs green generate">' +
                                '<i class="fa fa-cubes"></i> Generate Rooms </a>');
                        } else {
                            alert(tmpdata.message);
                        }

                    }
                });

            }

        });

    });

    $('.row-pic').on('click', 'a.delete-pic', function (event) {
        event.preventDefault();
        var clicked = $(this);
        var parent = clicked.parents('label.link').first();
        $.ajax({
            url: clicked.attr('href'),
            method: 'DELETE',
            success: function (data) {
                var tmpdata = (data);
                if (tmpdata.action == true) {
                    alert(tmpdata.message);
                    parent.fadeOut();
                } else {
                    alert(tmpdata.message);
                }

            }
        });

    });

    $('tr.row-item').on('click', 'button.save-pictures', function (event) {
        event.preventDefault();
        var clicked = $(this);

        var form = clicked.parents('tr.row-item').first();
        var inputsMedia = form.find('input.value-media');

        var inputArray = [];
        var inputItemArray = [];
        var $this;
        inputsMedia.each(function () {
            $this = $(this);
            inputArray.push({media_id: $this.val(), room_id: $this.attr('data-item')});
            inputItemArray.push([$this.val(), $this.attr('data-item')]);
        });
        //console.log(inputArray);
        //return;
        var action = clicked.attr('data-url');
        var _token = form.parents('table').first().find('input[name="_token"]').val();

        var parent = clicked.parents('td.row-action').first();
        var loading = parent.find('span.loading');
        var targetResult = parent.find('span.result');

        if (!inputsMedia.length) {
            return;
        }
        loading.fadeIn();
        $.ajax({
            url: action,
            method: 'PUT',
            data: {
                items: inputArray,
                _token: _token
            },
            success: function (data) {
                var tmpdata = (data);
                loading.fadeOut();
                if (tmpdata.action == true) {
                    targetResult.html(tmpdata.message);
                    window.location.href = window.location.href;

                } else {
                    targetResult.html(tmpdata.message);
                }

            }, error: function (ts) {
                loading.fadeOut();
                alert(ts.responseText);
                //window.location.reload();
            }
        });

    });

    $('#result-data-table').on('click', 'ul.pagination a', function (event) {
        event.preventDefault();
        var clicked = $(this);
        var url = clicked.attr('href');
        var parent = clicked.parents('tr.paginate').first();
        var loading = parent.find('span.loading');
        var resultDataTable = clicked.parents('#result-data-table');

        loading.fadeIn();
        $.ajax({
            url: clicked.attr('href'),
            method: 'GET',
            //data:{type_status:status},
            success: function (data) {
                var tmpdata = (data);
                loading.fadeOut();
                if (tmpdata.action == true) {
                    resultDataTable.html(tmpdata.pageData);
                } else {
                    alert(tmpdata.message);
                }

            }
        });

    });

    $('#result-data-table').on('click', 'a.cancelRent', function (event) {
        event.preventDefault();
        var clicked = $(this);
        var url = clicked.attr('href');
        var parent = clicked.parents('tr.row-item').first();
        var parentTD = clicked.parents('td').first();
        var booking_id = parent.attr('data-id');
        //var room_id = parent.find('select.item-room-select option:selected').val();
        //var cupParent = parent.parents('#result-data-table').find('tr.row-item[data-id="'+booking_id+'"]');

        //var parentAction = clicked.parents('tr.details-action').first();
        var loading = parentTD.find('span.loading');
        var email = $.trim(parent.find('span.email').text());
        //var user_id =cupParent.find('span.email').attr('data-user');

        var username = $.trim(parent.find('span.username').text());
        //var resultEmail = parentTD.find('span.result');
        loading.fadeIn();
        //console.log(url,email,username);
        if (!booking_id) {
            return;
        }

        bootbox.confirm("Cancel rent / reserve room with booking?", function (result) {
            if (result === false) {
                alert("Cancel Ok.");
                loading.fadeOut();
            } else {

                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {
                        email: email,
                        cancelBooking: 1,
                        username: username
                    },
                    success: function (data) {
                        var tmpdata = (data);
                        loading.fadeOut();
                        if (tmpdata.action == true) {
                            parentTD.html(tmpdata.message);
                            parent.fadeOut();
                            var searchBtn = $('table#table-advance').find('tr.filter').find('button.btn-search');
                            if (searchBtn) {
                                searchBtn.click();
                            }

                        } else {
                            alert(tmpdata.message);
                        }

                    }, error: function (ts) {
                        alert('You have error!');
                        loading.fadeOut();
                    }
                });
            }
        });
    });

    $('#table-advance').on('click', 'button.btn-search', function (event) {
        event.preventDefault();
        var clicked = $(this);
        var parent = clicked.parents('tr.filter').first();
        var url = clicked.parents('div.search').first().attr('data-url');
        var loading = clicked.parents('div.search').first().find('span.loading');
        var resultDataTable = clicked.parents('#table-advance').first().find('#result-data-table');
        var inputs = parent.find('input,select');
        ////console.log(inputs.serialize(),parent);
        //return;

        loading.fadeIn();
        $.ajax({
            url: url,
            method: 'POST',
            data: inputs.serialize(),
            success: function (data) {
                var tmpdata = (data);
                loading.fadeOut();
                if (tmpdata.action == true) {
                    resultDataTable.html(tmpdata.pageData);
                } else {
                    alert(tmpdata.message);
                }

            }
        });

    });


});