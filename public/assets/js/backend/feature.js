jQuery(document).ready(function () {
    $('.row-item').on('click', 'a.delete', function (event) {
        event.preventDefault();
        var clicked = $(this);
        var parent = clicked.parents('.row-item');
        bootbox.confirm("Are you sure , for delete item?", function (result) {
            if (result === false) {
                alert("Cancel delete.");
            } else {
                $.ajax({
                    url: clicked.attr('href'),
                    method: 'DELETE',
                    success: function (data) {
                        var tmpdata = (data);
                        if (tmpdata.action == true) {
                            alert(tmpdata.message);
                            parent.remove();
                        } else {
                            alert(tmpdata.message);
                        }

                    }
                });
            }
        });


    });
});