/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function (config) {
    // Define changes to default configuration here. For example:
    // config.language = 'fr';
    // config.uiColor = '#AADC6E';
    config.allowedContent = true;
    CKEDITOR.dtd.$removeEmpty.i = 0;
    config.filebrowserBrowseUrl = 'http://' + window.location.host + '/ck/browser/browse.php';


};
CKEDITOR.replace('editor1',
    {
        allowedContent: true,
        filebrowserUploadUrl: 'uploader/upload.php'
    });
