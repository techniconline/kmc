<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldToAreaInRoomCategory extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('room_category', function (Blueprint $table) {
            $table->double('to_area', 8, 2)->after('area');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('room_category', function (Blueprint $table) {
            $table->dropColumn('to_area');
        });
    }

}
