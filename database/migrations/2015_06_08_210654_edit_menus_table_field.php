<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditMenusTableField extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('menus', function (Blueprint $table) {
            $table->dropColumn('attr');
            $table->dropColumn('order');
            $table->dropColumn('status');
            $table->text('desc')->after('alias')->nullable();
            $table->string('image', 255)->after('desc')->nullable();
            $table->string('thumbnail', 255)->after('image')->nullable();
            $table->string('tag', 255)->after('thumbnail')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('menus', function (Blueprint $table) {
            $table->string('attr');
            $table->integer('order');
            $table->tinyInteger('status');
            $table->dropColumn('desc');
            $table->dropColumn('image', 255);
            $table->dropColumn('thumbnail', 255);
            $table->dropColumn('tag', 255);
        });
    }

}
