<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApartmentsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apartments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 255);
            $table->text('short_description');
            $table->text('description');
            $table->text('formatted_address');
            $table->text('address');
            $table->integer('floor_number');
            $table->integer('room_number');
            $table->integer('parking_number');
            $table->string('lat', 30);
            $table->string('lng', 30);
            $table->integer('user_id');
            $table->tinyInteger('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('apartments');
    }

}
