<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddShortContentFieldForContentLang extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contents_lang', function (Blueprint $table) {
            $table->text("short_content")->after("body_title")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contents_lang', function (Blueprint $table) {
            $table->dropColumn("short_content");
        });
    }

}
