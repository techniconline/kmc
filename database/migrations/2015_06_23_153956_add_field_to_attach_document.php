<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldToAttachDocument extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('attach_documents', function (Blueprint $table) {
            $table->integer('attach_type_id')->after('attach_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('attach_documents', function (Blueprint $table) {
            $table->dropColumn('attach_type_id');
        });
    }

}
