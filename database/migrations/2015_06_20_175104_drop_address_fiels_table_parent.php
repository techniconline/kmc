<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropAddressFielsTableParent extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_parent', function (Blueprint $table) {
            $table->text('parent_address')->after('parent_tenant_address');
            $table->dropColumn('parent_address_l1');
            $table->dropColumn('parent_address_l2');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_parent', function (Blueprint $table) {
            $table->dropColumn('parent_address');
            $table->string('parent_address_l1')->after('parent_tenant_address');
            $table->string('parent_address_l2')->after('parent_address_l1');
        });
    }

}
