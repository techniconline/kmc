<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropAndAddFieldToAttachDocument extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('attach_documents', function (Blueprint $table) {
            $table->dropColumn('attach_id');
            $table->integer('media_id')->after('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('attach_documents', function (Blueprint $table) {
            $table->dropColumn('media_id');
            $table->integer('attach_id')->after('id');
        });
    }

}
