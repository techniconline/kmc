<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropFieldRoomCategoryTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('room_category', function (Blueprint $table) {
            $table->dropColumn('title');
            $table->dropColumn('short_description');
            $table->dropColumn('description');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('room_category', function (Blueprint $table) {
            $table->string('title')->after('id');
            $table->text('short_description')->after('title');
            $table->text('description')->after('short_description');
        });
    }

}
