<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLanguageTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('language', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('is_code', 3)->default('en');
            $table->string('language_code', 10)->default('en-us');
            $table->string('date_format', 10)->default('Y-m-d');
            $table->string('date_format_full', 20)->default('Y-m-d H:i:s');
            $table->tinyInteger('active')->default(0);
            $table->tinyInteger('is_rtl')->default(0);
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('language');
    }

}
