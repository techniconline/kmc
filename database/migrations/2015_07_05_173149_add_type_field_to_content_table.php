<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeFieldToContentTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contents', function (Blueprint $t) {
            $t->enum('type', ['blog', 'news'])->after('id');
            $t->tinyInteger('login')->after('author_id')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contents', function (Blueprint $t) {
            $t->dropColumn('type');
            $t->dropColumn('login');
        });
    }

}
