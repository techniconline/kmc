<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillingsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billings', function (Blueprint $table) {
            Schema::drop('billing');

            $table->increments('id');
            $table->integer('booking_id');
            $table->integer('room_id');
            $table->decimal('price');
            $table->integer('year');
            $table->integer('month');
            $table->date('pay_date');
            $table->integer('user_id');
            $table->smallInteger('billing_status');
            $table->smallInteger('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('billings');
    }

}
