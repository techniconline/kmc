<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubjectMessageFieldToAttachDocFeedback extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('attach_documents_feedback', function (Blueprint $table) {
            $table->string('subject', 255)->after('assessor_user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('attach_documents_feedback', function (Blueprint $table) {
            $table->dropColumn('subject');
        });
    }

}
