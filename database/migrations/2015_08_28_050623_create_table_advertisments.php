<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAdvertisments extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advertisements', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('type', ['image', 'flash'])->default('image');
            $table->string('position')->default('home_slider');
            $table->string('slider_option')->nullable();
            $table->string('url')->nullable();
            $table->string('file_path')->nullable();
            $table->string('extension')->nullable();
            $table->string('group_show')->default('public');
            $table->boolean('status_show')->default(0);
            $table->boolean('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('advertisements');
    }

}
