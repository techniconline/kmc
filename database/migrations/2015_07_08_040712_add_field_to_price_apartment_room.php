<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldToPriceApartmentRoom extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('apartment_room_price', function (Blueprint $table) {
            $table->double('to_rent_price', 8, 2)->after('rent_price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('apartment_room_price', function (Blueprint $table) {
            $table->dropColumn('to_rent_price');
        });
    }

}
