<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenusLangTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus_lang', function (Blueprint $table) {
            $table->integer('menus_id');
            $table->integer('lang_id');
            $table->string('name', '255');
            $table->text('desc')->nullable();
            $table->string('tag', '255')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('menus_lang');
    }

}
