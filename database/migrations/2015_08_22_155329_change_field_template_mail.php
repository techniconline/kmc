<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeFieldTemplateMail extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('template_mail', function (Blueprint $table) {
            $table->dropColumn('url');
            $table->dropColumn('params');
            $table->text('text')->after('title');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('template_mail', function (Blueprint $table) {
            $table->string('url')->after('title');
            $table->text('params')->after('url');
            $table->dropColumn('text');
        });
    }

}
