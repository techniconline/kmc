<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRenewRentLogTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('renew_rent_log', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('rent_id');
            $table->date('from_date');
            $table->date('to_date');
            $table->integer('user_id');
            $table->tinyInteger('renew_status');
            $table->tinyInteger('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('renew_rent_log');
    }

}
