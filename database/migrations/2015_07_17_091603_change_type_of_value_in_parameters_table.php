<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeTypeOfValueInParametersTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('parameters', function (Blueprint $table) {
            $table->dropColumn('value');
            $table->dropColumn('to_value');
        });
        Schema::table('parameters', function (Blueprint $table) {
            $table->text('value')->after('alias');
            $table->text('to_value')->after('value');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('parameters', function (Blueprint $table) {
            $table->dropColumn('value');
            $table->dropColumn('to_value');
        });
    }

}
