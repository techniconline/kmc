<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableComments extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('system')->default('news');
            $table->integer('item_id')->default(0);
            $table->string('user_name')->nullable();
            $table->text('text')->nullable();
            $table->integer('parent_id')->default(0);
            $table->integer('user_id')->default(0);
            $table->integer('employee_id')->default(0);
            $table->integer('assessor_user_id')->default(0);
            $table->boolean('active')->default(1);
            $table->boolean('show_status')->default(0);
            $table->boolean('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('comments');
    }

}
