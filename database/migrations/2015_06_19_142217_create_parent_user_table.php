<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParentUserTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_parent', function (Blueprint $table) {
            $table->integer('user_id');
            $table->string('parent_first_name', 150)->nullable();
            $table->string('parent_last_name', 150)->nullable();
            $table->string('parent_email', 255)->nullable();
            $table->string('parent_mobile', 150)->nullable();
            $table->enum('parent_tenant_address', ['Oui', 'Non'])->nullable();
            $table->string('parent_address_l1', 255)->nullable();
            $table->string('parent_address_l2', 255)->nullable();
            $table->string('parent_postal_code', 50)->nullable();
            $table->integer('parent_country_id')->nullable();
            $table->tinyInteger('status');
            $table->timestamps();
            $table->primary(['user_id']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_parent');
    }

}
