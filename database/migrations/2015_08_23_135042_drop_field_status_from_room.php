<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropFieldStatusFromRoom extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rooms', function (Blueprint $table) {
            $table->dropColumn('wall_status');
            $table->dropColumn('facilities_status');
            $table->dropColumn('doors_status');
            $table->dropColumn('media_tools_status');
            $table->dropColumn('floor_status');
            $table->dropColumn('total_status');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rooms', function (Blueprint $table) {
            $table->enum('wall_status', ['good', 'dirty', 'damaged', 'bad', 'very-bad'])->after('description')->default('good');
            $table->enum('facilities_status', ['good', 'dirty', 'damaged', 'bad', 'very-bad'])->after('wall_status')->default('good');
            $table->enum('doors_status', ['good', 'dirty', 'damaged', 'bad', 'very-bad'])->after('facilities_status')->default('good');
            $table->enum('media_tools_status', ['good', 'dirty', 'damaged', 'bad', 'very-bad'])->after('doors_status')->default('good');
            $table->enum('floor_status', ['good', 'dirty', 'damaged', 'bad', 'very-bad'])->after('media_tools_status')->default('good');
            $table->enum('total_status', ['good', 'dirty', 'damaged', 'bad', 'very-bad'])->after('floor_status')->default('good');

        });
    }

}
