<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInfoUsersTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('info_users', function (Blueprint $table) {
            $table->integer('user_id', false, true)->unique();
            $table->enum('persona', ['student', 'trainee', 'another']);
            $table->enum('education', ['Terminale', 'Bac Pro', 'Bac Bac+1', 'Bac+2', 'Bac+3', 'Bac+4', 'Bac+5', 'Stagiaire', 'Autre']);
            $table->enum('institution_type', ['BTS IUT', 'Prépa Commerce', 'Prépa Ingénieur', 'Université', 'Ecole de Commerce', 'Ecole d`Ingénieur', 'Entreprise', 'Lycée', 'Autre']);
            $table->string('institution', 150);
            $table->string('institution_town', 150);
            $table->enum('current_institution_type', ['BTS IUT', 'Prépa Commerce', 'Prépa Ingénieur', 'Université', 'Ecole de Commerce', 'Ecole d`Ingénieur', 'Entreprise', 'Lycée', 'Autre']);
            $table->string('current_institution', 150);
            $table->string('current_institution_town', 150);
            $table->string('parent_first_name', 150);
            $table->string('parent_last_name', 150);
            $table->string('parent_email', 255);
            $table->string('parent_mobile', 150);
            $table->enum('parent_tenant_address', ['Oui', 'Non']);
            $table->string('parent_address_l1', 255);
            $table->string('parent_address_l2', 255);
            $table->string('parent_postal_code', 50);
            $table->integer('parent_country_id');
            $table->enum('parent_guarantor', ['OuiA', 'OuiB', 'Non']);
            $table->tinyInteger('accuracy_information');
            $table->text('personal_message');
            $table->tinyInteger('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('info_users');
    }

}
