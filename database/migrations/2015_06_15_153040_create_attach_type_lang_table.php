<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttachTypeLangTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attach_type_lang', function (Blueprint $table) {
            $table->integer('id');
            $table->integer('lang_id');
            $table->string('title');
            $table->text('description');
            $table->primary(['id', 'lang_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('attach_type_lang');
    }

}
