<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingDetailsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('booking_id');
            $table->integer('apartment_room_category_map_id');
            $table->integer('room_category_id');
            $table->enum('type', ['room', 'parking']);
            $table->integer('user_id');
            $table->tinyInteger('priority');
            $table->tinyInteger('rent_status');
            $table->tinyInteger('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('booking_details');
    }

}
