<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldDefaultImageInRoomMedia extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('room_media', function (Blueprint $table) {
            $table->tinyInteger('is_default')->after('title');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('room_media', function (Blueprint $table) {
            $table->dropColumn('is_default');
        });
    }

}
