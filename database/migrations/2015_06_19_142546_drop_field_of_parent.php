<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropFieldOfParent extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('info_users', function (Blueprint $table) {
            $table->dropColumn('parent_first_name');
            $table->dropColumn('parent_last_name');
            $table->dropColumn('parent_email');
            $table->dropColumn('parent_mobile');
            $table->dropColumn('parent_tenant_address');
            $table->dropColumn('parent_address_l1');
            $table->dropColumn('parent_address_l2');
            $table->dropColumn('parent_postal_code');
            $table->dropColumn('parent_country_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('info_users', function (Blueprint $table) {
            $table->string('parent_first_name', 150)->after('current_institution_town')->nullable();
            $table->string('parent_last_name', 150)->after('parent_first_name')->nullable();
            $table->string('parent_email', 255)->after('parent_last_name')->nullable();
            $table->string('parent_mobile', 150)->after('parent_email')->nullable();
            $table->enum('parent_tenant_address', ['Oui', 'Non'])->after('parent_mobile')->nullable();
            $table->string('parent_address_l1', 255)->after('parent_tenant_address')->nullable();
            $table->string('parent_address_l2', 255)->after('parent_address_l1')->nullable();
            $table->string('parent_postal_code', 50)->after('parent_address_l2')->nullable();
            $table->integer('parent_country_id')->after('parent_postal_code')->nullable();
        });
    }

}
