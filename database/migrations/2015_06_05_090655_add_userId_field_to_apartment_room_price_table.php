<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserIdFieldToApartmentRoomPriceTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('apartment_room_price', function (Blueprint $table) {
            $table->integer('user_id')->after('price_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('apartment_room_price', function (Blueprint $table) {
            $table->dropColumn('user_id');
        });
    }

}
