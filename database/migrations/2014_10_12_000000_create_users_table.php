<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('email')->unique();
            $table->string('password', 60);
            $table->enum('gender', ['male', 'female']);
            $table->string('avatar', 255);
            $table->string('id_number', 32);
            $table->string('mobile', 32);
            $table->string('telephone', 32);
            $table->string('fax', 32);
            $table->dateTime('birth_date');
            $table->text('address');
            $table->string('code_postal', 32);
            $table->string('company', 200);
            $table->string('paypal', 200);
            $table->integer('country_id');
            $table->integer('zone_id');
            $table->tinyInteger('status');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }

}
