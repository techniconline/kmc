<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentsLangTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contents_lang', function (Blueprint $table) {
            $table->integer('contents_id');
            $table->integer('lang_id');
            $table->string('browser_title');
            $table->text('body_title');
            $table->text('body_content');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contents_lang');
    }

}
