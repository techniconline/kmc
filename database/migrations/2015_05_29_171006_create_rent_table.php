<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRentTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rent', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('booking_details_id');
            $table->integer('apartment_room_category_map_id');
            $table->integer('room_category_id');
            $table->integer('room_id');
            $table->date('from_date');
            $table->date('to_date');
            $table->enum('type', ['room', 'parking']);
            $table->integer('user_id');
            $table->tinyInteger('rent_status');
            $table->tinyInteger('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rent');
    }

}
