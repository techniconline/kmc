<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApartmentRoomCategoryMapTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apartment_room_category_map', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('apartment_id');
            $table->integer('room_category_id');
            $table->integer('room_number');
            $table->integer('floor_number');
            $table->integer('user_id');
            $table->tinyInteger('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('apartment_room_category_map');
    }

}
